//
//  RSAKeyManager.swift
//  SalesCloud
//
//  Created by mhuertao on 16/08/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit
import Foundation
import SwiftyRSA

public class RSAKeyManager {

    class func encrypt(text: String, key: String = statics.ACCESO_PUBLICO) -> String {
        let privateKeyData =  Data(base64Encoded: key, options: .ignoreUnknownCharacters)
        if privateKeyData != nil {
            do {
            let publicKey = try PublicKey(data: privateKeyData!)
            let clear = try ClearMessage(string: text, using: .utf8)
            let encrypted = try clear.encrypted(with: publicKey, padding: .PKCS1)
                return  encrypted.base64String
            } catch let error {
                return ""
            }
        } else {
            return ""
        }
    }

    class func desencrypt(text: String, key: String = statics.ACCESO_PRIVADO) -> String {
        let privateKeyData =  Data(base64Encoded: key, options: .ignoreUnknownCharacters)
        if privateKeyData != nil {
            do {
            let privateKey = try PrivateKey(data: privateKeyData!)
            let encrypted = try EncryptedMessage(base64Encoded: text)
            let clear = try encrypted.decrypted(with: privateKey, padding: .PKCS1)
            let data = clear.data
            let result = String(data: data, encoding: .utf8) ?? ""
                return  result
            } catch let error {
                return ""
            }
        } else {
            return ""
        }
    }
}
