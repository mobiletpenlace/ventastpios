//
//  BaseTableViewCell.swift
//  EstrategiaDigital
//
//  Created by Jorge Hdez Villa on 05/08/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit

public class BaseTableViewCell: UITableViewCell {

    public var delegate: NSObjectProtocol!
    public var itemObject: NSObject?
    public var delegateCell: BaseTableDelegate?

    override public func awakeFromNib() {
        super.awakeFromNib()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapEdit(_:)))
        addGestureRecognizer(tapGesture)
    }

    @objc public func tapEdit(_ sender: UITapGestureRecognizer) {
        delegateCell?.baseTableDelegate(sender: sender)
    }

    public func pupulate(object: NSObject) {
        preconditionFailure("This method must be overridden")
    }

    public func pupulateSelected(object: NSObject) {

    }

    public func executeAction() {
        preconditionFailure("This method must be overridden")
    }

    public func toString() -> String {
        preconditionFailure("This method must be overridden")
    }

}

public protocol BaseTableDelegate {
    func baseTableDelegate(sender: UITapGestureRecognizer)
}
