//
//  GeneratorRFCMoral.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 12/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class GeneratorRFCMoral: NSObject {

    let arraySociedad = ["SA DE CV", "SA", "S DE RL", "SAS"]
    let arrayCompania = ["COMPAÑIA", "CIA", "CÍA", "SOCIEDAD", "SOC"]
    let arrayArticulos = ["EL", "LA", "LOS", "LAS", "UN", "UNA", "UNOS", "UNAS", "AL", "DEL", "LO", "A", "DE"]
    let arrayPreposiciones = ["A", "ANTE", "BAJO", "CABE", "CON", "CONTRA", "DE", "DESDE", "EN", "ENTRE", "HACIA", "HASTA", "PARA", "POR", "SEGUN", "SIN", "SO", "SOBRE", "TRAS"]
    let arrayConjunciones = ["Y", "E", "NI", "O", "U", "PERO", "MAS", "SINO", "LUEGO", "CONQUE", "ASI QUE", "PORQUE", "PUES", "SI", "QUE", "COMO", "PARA QUE", "A FIN DE QUE", "AUNQUE", "YA", "BIEN", "AUN CUANDO", "SI BIEN", "CUANDO"]
    let arraySignos = ["@", "'", "%", "#", "!", ".", "$", "-", "/", "+", "(", ")"]
    let arraySignosNombres = ["ARROBA", "APOSTROFE", "PORCIENTO", "NUMERO", "ADMIRACION", "PUNTO", "PESOS", "GUION", "DIAGONAL", "SUMA", "ABRE PARENTESIS", "CIERRA PARENTESIS"]
    let arrayNumNaturales = ["UNO", "DOS", "TRES", "CUATRO", "CINCO", "SEIS", "SIETE", "OCHO", "NUEVE"]
    let arrayNumExcepciones = ["ONCE", "DOCE", "TRECE", "CATORCE", "QUINCE", "DIESCISEIS", "DIESCISIETE", "DIESCIOCHO", "DIESCINUEVE"]
    let arrayNumDecenas = ["DIEZ", "VEINTE", "TREINTA", "CUARENTA", "CINCUENTA", "SESENTA", "SETENTA", "OCHENTA", "NOVENTA"]
    let arrayNumCentenas = ["CIENTO", "DOSCIENTOS", "TRESCIENTOS", "CUATROCIENTOS", "QUINIENTOS", "SEISCIENTOS", "SETECIENTOS", "OCHOCIENTOS", "NOVECIENTOS"]
    let arrayMiles = ["MIL", "MILLONES", "MIL", "BILLONES", "MIL", "TRILLONES", "MIL"]
    let arrayHomonimo = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "&", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "Ñ"]
    let arrayHomoRC = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
    let arrayContrib = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "&", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "BLANCO", "Ñ"]

    func Moral(razonSocial: String, fecha: String) -> String {

        let nameComplete = razonSocial.uppercased()
        print(nameComplete)
        // Eliminar los puntos y comas.
        let StringWhithoutPoint = nameComplete.replacingOccurrences(of: ".", with: ""+"")
        var StringWhithoutComas = StringWhithoutPoint.replacingOccurrences(of: ",", with: ""+"")

        // Eliminar los tipos de sociedad
        for i in 0..<arraySociedad.count {
            if StringWhithoutComas.contains(string: arraySociedad[i].uppercased()) {
                StringWhithoutComas = StringWhithoutComas.replacingOccurrences(of: arraySociedad[i], with: ""+"")
            }
        }
        // Eliminar la palabra Sociedad o Compañia y sus abreviaciones
        for i in 0..<arrayCompania.count {
            if StringWhithoutComas.contains(string: arrayCompania[i].uppercased()) {
                StringWhithoutComas = StringWhithoutComas.replacingOccurrences(of: arrayCompania[i], with: ""+"")
            }
        }
        // Separar las palabras por los espacios en blanco. Y eliminar los elementos que esten en vacios.
        var newString = StringWhithoutComas.components(separatedBy: [" "])
        newString = newString.filter { $0 != "" }

        // Si solo hay un elemento en el arreglo y es un articulo, preposicion o conjuncion se queda , sino se elimina.
        if newString.count > 1 {
            for j in 0..<arrayArticulos.count {
                newString = newString.filter { $0 != arrayArticulos[j].uppercased() }
            }

            for k in 0..<arrayConjunciones.count {
                newString = newString.filter { $0 != arrayConjunciones[k].uppercased() }
            }

            for l in 0..<arrayPreposiciones.count {
                newString = newString.filter { $0 != arrayPreposiciones[l].uppercased() }
            }
        }

        // Si hay un signo dentro de una palabra en el arreglo se elimina el signo y el signo esta solo se reemplaza por su nombre
        for m in 0..<newString.count {
            for n in 0..<arraySignos.count {
                if newString[m].count == 1 && newString[m].contains(string: arraySignos[n]) {
                    // se sustituye el signo por el nombre
                    newString[m] = newString[m].replace(search: arraySignos[n], replace: arraySignosNombres[n])
                } else if newString[m].count > 1 && newString[m].contains(string: arraySignos[n]) {
                    // se elimina el signo.
                    newString[m] = newString[m].replacingOccurrences(of: arraySignos[n], with: ""+"")
                }
            }
        }

        // Si hay un numero en los primeros tres elementos del arreglo debe ser sustituido por una letra
        // Si hay mas de un elemento numerico en el arreglo juntarlos en caso que esten en posiciones juntas
        // ejemplo : "400 234 SA de CV"     de lo contrario se queda igual  "432 Sonora 12 SA DE CV"
        var arrPositionNum: [Int] = []
        for z in 0..<newString.count {
            let num = Int(newString[z])
            if num != nil {
                arrPositionNum.append(z)
                // print("Es numerico")
            }
        }
        if arrPositionNum.count > 1 {
            var arrDelete: [Int] = []
            for y in 1..<arrPositionNum.count {

               // saber si las posiciones estan juntas  o separadas
                if arrPositionNum[y] - arrPositionNum[y-1] == 1 {
                    let newNum = newString[arrPositionNum[y-1] ] + newString[arrPositionNum[y]]  // nuevo numero conformado
                    newString[arrPositionNum[y] ] = newNum                       // se actualiza el numero en el arreglo
                    arrDelete.append(arrPositionNum[y-1])                          // se agregar al arreglo la posicion que se eliminara
                    // print("estan juntas")
                } else {
                   // print("estan separadas")
                }
            }
                // eliminar las posiciones de los elementos numericos que ya no se ocuparan

            arrDelete.reverse()
            for h in 0..<arrDelete.count {
                newString.remove(at: arrDelete[h])
            }
        }
        // Cambiar el digito por el nombre del numero.
        var arrPosNumber: [Int] = []
        for z in 0..<newString.count {
            let num = Int(newString[z])
            if num != nil {
                arrPosNumber.append(z)
            }
        }

        for j in 0..<arrPosNumber.count {
            if newString[arrPosNumber[j]].length > 3 {
                // print("es mayor que 3")
                // formar grupos de 3 de derecha a izquierda
                let residuo = (newString[arrPosNumber[j]].length % 3)
                let result = (newString[arrPosNumber[j]].length / 3)
                let nombreComp1 = agrupar(num: newString[arrPosNumber[j]], residuo: residuo, result: result) // nombre del numero
                newString[arrPosNumber[j]] = nombreComp1

            } else {
               // print("es menor que 3")
                let nombreComp = nombreNum(num: newString[arrPosNumber[j]]) // nombre del numero
                newString[arrPosNumber[j]] = nombreComp
            }
        }
        // verificar que haya un palabra por posicion. (Incluyendo los nombres de los numeros que se agregaron anteriormente)
        var final = ""
        var rfcComplete = ""
        for k in 0..<newString.count {
            final = final + newString[k] + " "
        }
        var finalArray = final.components(separatedBy: [" "])
        finalArray = finalArray.filter { $0 != "" }
        print(finalArray) // array completo
        rfcComplete = inicialesRFC(arrName: finalArray)
        // acomodar la fecha ingresada de (yyyy-mm-dd) a (yy-mm-dd)
        var fechaComplete = ""
        let arrFecha = fecha.components(separatedBy: ["-"])
        if arrFecha.count == 3 {
           fechaComplete = String(arrFecha[0].suffix(2))
           fechaComplete = fechaComplete + arrFecha[1] + arrFecha[2]
            rfcComplete = rfcComplete + fechaComplete
        } else {
            print("ocurrio un error")
        }

        print(rfcComplete)
        return nameComplete
    }

    func nombreNum(num: String) -> String {
        var num1 = num
        var num2 = num
        let num3 = num
        var cadena = ""
        let cantidad = num.length
        if cantidad == 3 {
            cadena =  arrayNumCentenas[(Int(num1)!/100) - 1]
            num1.remove(at: num1.startIndex)
            if Int(num1)!>10 && Int(num1)!<20 {
                num1.remove(at: num1.startIndex)
                cadena = cadena + " " + arrayNumExcepciones[Int(num1)! - 1]
            } else {
                 if (Int(num1)!/10) != 0 {
                cadena =  cadena + " " + arrayNumDecenas[(Int(num1)!/10) - 1]
                    num1.remove(at: num1.startIndex)
                }
                if Int(num1) != 0 {
                    cadena =  cadena + " " + arrayNumNaturales[Int(num1)! - 1]
                }
            }
        } else if cantidad == 2 {
            if Int(num2)!>10 && Int(num2)!<20 {
                num2.remove(at: num2.startIndex)
                cadena = arrayNumExcepciones[Int(num2)! - 1]
            } else {
                if (Int(num2)!/10) != 0 {
                cadena = arrayNumDecenas[(Int(num2)!/10) - 1]
                    num2.remove(at: num2.startIndex)
                }
                if Int(num2) != 0 {
                    cadena =  cadena + " " + arrayNumNaturales[Int(num2)! - 1]
                }
            }
        } else if cantidad == 1 {
          cadena =  arrayNumNaturales[Int(num3)! - 1]

        }

        return cadena
    }

    func agrupar(num: String, residuo: Int, result: Int) -> String {
       var arrGroup: [String] = []
       var numero = num
        if residuo != 0 {
            let mySubstring = numero.prefix(residuo)
            arrGroup.append(String(mySubstring))
            numero = String(numero.dropFirst(residuo))
        }
        // quitar de 3 en 3
        for _ in 0..<result {
           arrGroup.append(String(numero.prefix(3)))
           numero = String(numero.dropFirst(3))
        }
        // Arreglo con las conexiones mil , millones , etc
        var arrayTemp: [String] = []
        if arrGroup.count > 1 {
            arrayTemp =  Array(arrayMiles.prefix(arrGroup.count - 1))
            arrayTemp.reverse()
        }

        // mandar cada grupo a la funcion de leer nombre
        var nameCom = ""
        for i in 0..<arrGroup.count {
            nameCom = nameCom + nombreNum(num: arrGroup[i])
            if arrayTemp.indices.contains(i) {
                nameCom = nameCom + " " + arrayTemp[i] + " "
            }
        }
        return nameCom
    }

    func inicialesRFC(arrName: [String]) -> String {
        // Verificar el numero de letras en las primeras 3 palabras
        // Verificar el numero de palabras
        var iniciales = ""
        if arrName.count > 2 {
            if arrName.indices.contains(0) {
              iniciales = String(arrName[0].prefix(1))
            }
            if arrName.indices.contains(1) {
              iniciales = iniciales + String(arrName[1].prefix(1))
            }
            if arrName.indices.contains(2) {
              iniciales = iniciales + String(arrName[2].prefix(1))
            }
        } else if arrName.count == 2 {
            if arrName.indices.contains(0) {
                iniciales = String(arrName[0].prefix(1))
            }
            if arrName.indices.contains(1) {
                if arrName[1].length>1 {
                 iniciales = iniciales + String(arrName[1].prefix(2))
                } else {
                 iniciales = iniciales + String(arrName[1].prefix(1)) + "X"
                }
            }
        } else if arrName.count == 1 {
            if arrName[0].length > 2 {
                iniciales = String(arrName[0].prefix(3))
            } else if arrName[0].length == 2 {
                iniciales = String(arrName[0].prefix(2)) + "X"
            } else if arrName[0].length == 1 {
                iniciales = String(arrName[0].prefix(2)) + "XX"
            }
        }
        return iniciales
    }

    func getHomoclave(name: String) {
        // Cambiar cada letra del nombre por su valor numero.
        var arrHomo = Array(repeating: 00, count: name.length)
        let nameH = name.uppercased()
        // arrHomo[i] = Int(String(format: "%02d", j))!
        for i in 0..<nameH.length {
            for j in 0..<arrayHomonimo.count {
                let index = nameH.index(nameH.startIndex, offsetBy: i)
                if String(nameH[index]) == arrayHomonimo[j] {
                    if j > 19 {
                        if j > 29 {
                            arrHomo[i] = j + 3
                        } else {
                            arrHomo[i] = j + 1
                        }
                    } else {
                        arrHomo[i] = j
                    }
                }
            }
        }
        print(arrHomo)
        var cadena = "0"
        for k in 0..<arrHomo.count {
           cadena = cadena + String(format: "%02d", arrHomo[k])
        }

        var sum = 0
        // var arrTemp :[Int] = []
        for _ in 0..<cadena.length - 1 {
            // los primeros dos digitos se toman y se multiplan por el segundo digito.
            // el resultado se acumula en una suma
            let operador1 = cadena.prefix(2)
            let operador2 = operador1.suffix(1)
            sum = sum + (Int(operador1)! * Int(operador2)!)
           // arrTemp.append((Int(operador1)! * Int(operador2)!)) // temporal para ver el resultado de las sumas
            cadena.remove(at: cadena.startIndex)
        }

        print(sum)
        // Obtener el coeficiente y residuo de los tres ultimos digitos de la suma entre el factor 34
        let tresDigitos = String(sum).suffix(3)
        let coeficiente =  Int(tresDigitos)! / 34
        let residuo =  Int(tresDigitos)! % 34

        print(coeficiente)
        print(residuo)
        var homoclave = ""
        // print(cadena)
        if arrayHomoRC.indices.contains(coeficiente) {
            homoclave = arrayHomoRC[coeficiente]
        }
        if arrayHomoRC.indices.contains(residuo) {
            homoclave = homoclave + arrayHomoRC[residuo]
        }
        print(homoclave)

    }

}
