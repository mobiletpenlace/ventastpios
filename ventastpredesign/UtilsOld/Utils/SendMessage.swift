//
//  SendMessage.swift
//  SalesCloud
//
//  Created by mhuertao on 16/08/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import UIKit

public class SendMessage {

        class func whatsp(withPhoneNumber phoneNumber: String) {
            let appURL = URL(string: "https://api.whatsapp.com/send?phone=\(phoneNumber)")!
            let webURL = URL(string: "https://wa.me/\(phoneNumber)")!

            if UIApplication.shared.canOpenURL(appURL) {
                openURL(appURL)
            } else {
                openURL(webURL)
            }
        }

        class func openURL(_ url: URL) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }

    }
