//
//  FactoryValidatorLimit.swift
//  VentasTPEmpresarial
//
//  Created by eduardo mancilla on 2/27/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import Foundation
import Validator
import EasyTipView

struct ValidationError: Error {
    public let message: String
    public init(message m: String) {
        message = m
    }
}

enum RegexDataType: String {
    case email = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
    case ip = "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$"
    case ipSepateComa = "((([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]),)*((([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))+$"
    case hostName = "^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9])$"
    case hostNameSeparateComa = "((([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9]),)*((([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9]))+$"
    case phone = "^\\d{3}-\\d{3}-\\d{4}$"
    case phone2 =  "^\\d{10}$"
    case rfc = "[A-Z,a-z,ñ,Ñ,&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,a-z,ñ,Ñ,0-9][A-Z,a-z,Ñ,ñ,0-9][0-9,A-Z,a-z,Ñ,ñ]"
}

enum TypeLimit {
    case none
    case required// solo validar que no esten vacio slos campos
    case max(Int)// limitar a un maximo valor
    case min(Int)// limitar a un minimo vaor
    case exact(Int)// el valor debe ser exacto
    case between(Int, Int)// un rango de valores
    case opcional(Int)// debe tener esa longitud o 0
    case regex(RegexDataType)// validar personalizado
    case regexWithLimit(RegexDataType, Int)// validar personalizado
}

enum TipoField: String {
    case all = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyz 1234567890.-/@_+*/,"
    case correo = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyz 1234567890.-/@_"
    case nombre = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyz "
    case apellido = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyz"
    case rfc = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyz1234567890"
    case calleNumero = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyz1234567890 "
    case numero = "1234567890"
    case numeroDecimal = "1234567890."
    case ipComa = "1234567890.,"
    case birthdate = "0123456789/"

    case numeroComa = "1234567890,"
}

enum FieldValidator {
    case textField(UITextField, TypeLimit, TipoField, String)
    case textView(UITextView, TypeLimit, TipoField, String)
}

class ValidatorManager: NSObject {
    typealias TipoValidator = ((UITextField) -> Void)?
    typealias TipoValidatorTextView = ((UITextView) -> Void)?

    private var listaValidator = [FieldValidator]()

    private var listaToolTips: [UIView: (EasyTipView, Bool)] = [:]

    private var listaActionEndEditing: [UITextField: TipoValidator] = [:]
    private var listaActionBeginEditing: [UITextField: TipoValidator] = [:]
    private var listaChangeField: [UITextField: UITextField] = [:]

    private var listaActionBeginEditingTextView: [UITextView: TipoValidatorTextView] = [:]
    private var listaActionEndEditingTextView: [UITextView: TipoValidatorTextView] = [:]

    private var preferences = EasyTipView.Preferences()
    private let borderColorBien = UIColor.lightGray
    private let borderColorMal = UIColor.red
    private let tiempoToolTip = 2000
    private let widthBorderBien: CGFloat = 0.5
    private let widthBorderMal: CGFloat = 2

    override init() {
        preferences.drawing.font = UIFont(name: "Futura-Medium", size: 13)!
        preferences.drawing.foregroundColor = UIColor.white
        preferences.drawing.backgroundColor = UIColor(netHex: 0x384356)
        preferences.drawing.arrowPosition = EasyTipView.ArrowPosition.bottom
        preferences.animating.dismissDuration = 0
        EasyTipView.globalPreferences = preferences
    }

    func addBeginEditingFor(_ textView: UITextView, closure: TipoValidatorTextView) {
        listaActionBeginEditingTextView[textView] = closure
    }

    func addBeginEditingFor(_ textField: UITextField, closure: TipoValidator) {
        listaActionBeginEditing[textField] = closure
    }

    func addEndEditingFor(_ textView: UITextView, closure: TipoValidatorTextView) {
        listaActionEndEditingTextView[textView] = closure
    }

    func addEndEditingFor(_ textField: UITextField, closure: TipoValidator) {
        listaActionEndEditing[textField] = closure
    }

    func addChangeEnterField(from textFieldFrom: UITextField, to textFieldTo: UITextField) {
        listaChangeField[textFieldFrom] = textFieldTo
    }

    func addValidator(_ fields: FieldValidator ...) {
        for field in fields {
            listaValidator.append(field)
            switch field {
            case .textField(let textField, _, _, let mensajeError):
                textField.addTarget(
                    self,
                    action: #selector(textFieldEditingDidChange(_:)),
                    for: UIControlEvents.editingChanged
                )
                listaToolTips[textField] = (EasyTipView(text: mensajeError), false)
            case .textView(let textView, _, _, let mensajeError):
                listaToolTips[textView] = (EasyTipView(text: mensajeError), false)
            }
        }
    }

    func setUpTextfields() {
        for validator in listaValidator {
            switch validator {
            case .textField(let textField, _, _, _):
                bien(textField)
            case .textView(let textView, _, _, _):
                bien(textView)
            }
        }
    }

    func validateRegexOnce(textField: UITextField, regexType: TypeLimit, mensajeError: String) -> Bool {
        let texto = textField.text ?? ""
        return validateRegexOnce(texto: texto, field: textField, regexType: regexType, mensajeError: mensajeError)
    }

    func validateRegexOnce(textView: UITextView, regexType: TypeLimit, mensajeError: String) -> Bool {
        let texto = textView.text ?? ""
        return validateRegexOnce(texto: texto, field: textView, regexType: regexType, mensajeError: mensajeError)
    }

    private func validateRegexOnce(texto: String, field: UIView, regexType: TypeLimit, mensajeError: String) -> Bool {
        let res = validarTexField(texto: texto, view: field, regexType: regexType)
        if res {
            addLineBotom(field, color: borderColorBien, width: widthBorderBien)
        } else {
            addLineBotom(field, color: borderColorMal, width: widthBorderMal)
            let tipView = EasyTipView(text: mensajeError)
            tipView.show(forView: field)
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(tiempoToolTip)) {
                tipView.dismiss()
            }
        }
        return res
    }

    func validate() -> Bool {
        var res = true
        for validator in listaValidator {
            switch validator {
            case .textField(let textField, let regexType, _, _):
                let texto = textField.text ?? ""
                res = res && validarTexField(texto: texto, view: textField, regexType: regexType)
            case .textView(let textView, let regexType, _, _):
                let texto = textView.text ?? ""
                res = res && validarTexField(texto: texto, view: textView, regexType: regexType)
            }
        }
        return res
    }

    @objc func textFieldEditingDidChange(_ sender: UITextField) {
        for validator in listaValidator {
            switch validator {
            case .textField(let textField, let regexType, _, _):
                if sender == textField {
                    let texto = textField.text ?? ""
                    _ = validarTexField(texto: texto, view: textField, regexType: regexType)
                }
            case .textView:
                break
            }
        }
    }

    private func validarTexField(texto: String, view: UIView, regexType: TypeLimit) -> Bool {
        switch regexType {
        case .max(let value):
            return validarMax(text: texto, value: value, view)
        case .min(let value):
            return validarMin(text: texto, value: value, view)
        case .exact(let value):
            return validarBetween(text: texto, valueMax: value, valueMin: value, view)
        case .between(let valueMin, let valueMax):
            return validarBetween(text: texto, valueMax: valueMax, valueMin: valueMin, view)
        case .regex(let regex):
            return validarRegex(text: texto, regex: regex.rawValue, view)
        case .required:
            return validateRequired(text: texto, view)
        case .none:
            return validateNone(view)
        case .regexWithLimit(let regex, _):
            return validarRegex(text: texto, regex: regex.rawValue, view)
        case .opcional(let value):
            return validateOpcional(text: texto, value: value, view)
        }
    }

    private func validateNone(_ view: UIView) -> Bool {
        return true
    }

    private func validarRegex(text: String, regex: String, _ view: UIView) -> Bool {
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        let result = predicate.evaluate(with: text)
        if result {
            bien(view)
        } else {
            mal(view)
        }
        return result
    }

    private func validarMax(text: String, value: Int, _ view: UIView) -> Bool {
        let rule = ValidationRuleLength(max: value, error: ValidationError(message: ""))
        return validateRule(rule: rule, text: text, view)
    }

    private func validarMin(text: String, value: Int, _ view: UIView) -> Bool {
        let rule = ValidationRuleLength(min: value, error: ValidationError(message: ""))
        return validateRule(rule: rule, text: text, view)
    }

    private func validarBetween(text: String, valueMax: Int, valueMin: Int, _ view: UIView) -> Bool {
        let rule = ValidationRuleLength(min: valueMin, max: valueMax, error: ValidationError(message: ""))
        return validateRule(rule: rule, text: text, view)
    }

    private func validateRule(rule: ValidationRuleLength, text: String, _ view: UIView) -> Bool {
        let result = text.validate(rule: rule)
        switch result {
        case .valid:
            bien(view)
            return true
        case .invalid:
            mal(view)
            return false
        }
    }

    private func validateRequired(text: String, _ view: UIView) -> Bool {
        if text.trim().isEmpty {
            mal(view)
            return false
        } else {
            bien(view)
            return true
        }
    }

    private func validateOpcional(text: String, value: Int, _ view: UIView) -> Bool {
        if text.trim().isEmpty || validarBetween(text: text, valueMax: value, valueMin: value, view) {
            bien(view)
            return true
        } else {
            mal(view)
            return false
        }
    }

    private func bien(_ view: UIView) {
        addLineBotom(view, color: borderColorBien, width: widthBorderBien)
        guard let (tipView, _) = listaToolTips[view] else {
            Logger.println("No se ha encontrado el tooltip con esa clave.")
            return
        }
        tipView.dismiss()
    }

    private func mal(_ view: UIView) {
        addLineBotom(view, color: borderColorMal, width: widthBorderMal)
        guard let (tipView, isShow) = listaToolTips[view] else {
            Logger.println("No se ha encontrado el tooltip con esa clave.")
            return
        }
        if isShow == false {
            listaToolTips[view]?.1 = true
            tipView.show(forView: view)
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(tiempoToolTip)) {
                tipView.dismiss {
                    self.listaToolTips[view]?.1 = false
                }
            }
        }
    }

    private func addLineBotom(_ view: UIView, color: UIColor, width: CGFloat) {
        let tag = 1000
        if let viewWithTag = view.viewWithTag(tag) {
            viewWithTag.removeFromSuperview()
        }

        let underlineFrame = CGRect(
            x: 0,
            y: view.bounds.size.height - width,
            width: view.frame.size.width,
            height: width
        )

        let underline = UIView(frame: underlineFrame)
        underline.backgroundColor = color
        underline.tag = tag
        view.addSubview(underline)
        view.layer.masksToBounds = true
    }
}

extension ValidatorManager {

    private func validateField(_ field: Any, _ range: NSRange, _ string: String) -> Bool {
        for validator in listaValidator {
            switch validator {
            case .textField(let textField, let regexType, let regexAllowChar, _):
                if let field = field as? UITextField {
                    if field == textField {
                        let text = textField.text ?? ""
                        return validateTextfield(text, regexType, regexAllowChar, range, string)
                    }
                }
            case .textView(let textView, let regexType, let regexAllowChar, _):
                if let field = field as? UITextView {
                    if field == textView {
                        let text = textView.text ?? ""
                        return validateTextfield(text, regexType, regexAllowChar, range, string)
                    }
                }
            }
        }
        return true
    }

    private func validateTextfield(_ currentText: String, _ regexType: TypeLimit, _ regexAllowChar: TipoField, _ range: NSRange, _ string: String) -> Bool {
        var maximo: Int?
        switch regexType {
        case .max(let value):
            maximo = value
        case .min:
            break
        case .exact(let value):
            maximo = value
        case .between(_, let valueMax):
            maximo = valueMax
        case .regex:
            break
        case .required:
            break
        case .none:
            break
        case .regexWithLimit(_, let max):
            maximo = max
        case .opcional(let max):
            maximo = max
        }
        let allowedChars = regexAllowChar.rawValue
        return restrict(currentText, range, string, allowedChars, maximo)
    }

    private func validateMax(_ currentText: String, allowedChars: String, maximo: Int?, _ range: NSRange, _ string: String) -> Bool {
        return restrict(currentText, range, string, allowedChars, maximo)
    }
}

// limit manager
extension ValidatorManager {

    private func restrict(_ currentText: String, _ range: NSRange, _ string: String, _ allowedChars: String? = nil, _ maxLength: Int? = nil) -> Bool {
        let prospectiveText = (currentText as NSString).replacingCharacters(in: range, with: string)

        let containOnly = (allowedChars != nil) ? containsOnly(prospectiveText, allowedChars!) : true
        let limit = (maxLength != nil) ? isLimit(prospectiveText, max: maxLength!) : true
        return containOnly && limit
    }

    private func containsOnly(_ string: String, _ matchCharacters: String) -> Bool {
        let disallowedCharacterSet = NSCharacterSet(charactersIn: matchCharacters).inverted
        return string.rangeOfCharacter(from: disallowedCharacterSet) == nil
    }

    private func isLimit(_ string: String, max: Int) -> Bool {
        return string.count <= max
    }
}

extension ValidatorManager: UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard string.count > 0 else { return true }
        return validateField(textField, range, string)
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.text = textField.text?.uppercased()
        if let closure = listaActionEndEditing[textField] {
            if let closure = closure {
                closure(textField)
            }
        }
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let closure = listaActionBeginEditing[textField] {
            if let closure = closure {
                closure(textField)
            }
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let textFieldTo = listaChangeField[textField] {
            textFieldTo.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
}

extension ValidatorManager: UITextViewDelegate {

    func textViewDidEndEditing(_ textView: UITextView) {
        textView.text = textView.text?.uppercased()
        if let closure = listaActionEndEditingTextView[textView] {
            if let closure = closure {
                closure(textView)
            }
        }
    }

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        guard text.count > 0 else { return true }
        return validateField(textView, range, text)
    }

    func textViewDidChange(_ textView: UITextView) {
        for validator in listaValidator {
            switch validator {
            case .textField:
                break
            case .textView(let textview2, let regexType, _, _):
                if textview2 == textView {
                    let texto = textView.text ?? ""
                    _ = validarTexField(texto: texto, view: textView, regexType: regexType)
                }
            }
        }
    }

}
