//
//  UIAlertControllerExtension.swift
//  VentasTotalPlayiOS
//
//  Created by Mario Hernandez on 11/01/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import UIKit

let CancelTitle     =   "Cancel"
let OKTitle         =   "OK"
typealias AlertViewController = UIAlertController

struct AlertAction {

    var title: String = ""
    var type: UIAlertActionStyle? = .default
    var enable: Bool? = true
    var selected: Bool? = false

    init(title: String, type: UIAlertActionStyle? = .default, enable: Bool? = true, selected: Bool? = false) {
        self.title = title
        self.type = type
        self.enable = enable
        self.selected = selected
    }
}

extension UIViewController {

    // Show Alert or Action sheet
    func getAlertViewController(type: UIAlertControllerStyle, with title: String?, message: String?, actions: [AlertAction], showCancel: Bool, actionHandler: @escaping ((_ title: String) -> Void)) -> AlertViewController {

        let alertController = UIAlertController(title: title, message: message, preferredStyle: type)

        // items
        var actionItems: [UIAlertAction] = []

        // add actions
        for (index, action) in actions.enumerated() {

            let actionButton = UIAlertAction(title: action.title, style: action.type!, handler: { (actionButton) in
                actionHandler(actionButton.title ?? "")
            })

            actionButton.isEnabled = action.enable!
            if type == .actionSheet { actionButton.setValue(action.selected, forKey: "checked") }
            actionButton.setAssociated(object: index)

            actionItems.append(actionButton)
            alertController.addAction(actionButton)
        }

        // add cancel button
        if showCancel {
            let cancelAction = UIAlertAction(title: CancelTitle, style: .cancel, handler: { (action) in
                actionHandler(action.title!)
            })
            alertController.addAction(cancelAction)
        }
        return alertController
    }
}

extension UIAlertController {
}
