//
//  SlidingContainerViewController.swift
//  SlidingContainerViewController
//
//  Created by Cem Olcay on 10/04/15.
//  Copyright (c) 2015 Cem Olcay. All rights reserved.
//

import UIKit

@objc protocol SlidingContainerViewControllerDelegate {
    @objc optional func slidingContainerViewControllerDidMoveToViewController (slidingContainerViewController: SlidingContainerViewController, viewController: UIViewController, atIndex: Int)
    @objc optional func slidingContainerViewControllerDidHideSliderView (slidingContainerViewController: SlidingContainerViewController)
    @objc optional func slidingContainerViewControllerDidShowSliderView (slidingContainerViewController: SlidingContainerViewController)
}

class SlidingContainerViewController: UIViewController, UIScrollViewDelegate, SlidingContainerSliderViewDelegate {

    @objc dynamic var contentViewControllers: [UIViewController]!
    @objc dynamic var contentScrollView: UIScrollView!
    @objc dynamic var titles: [String]!

    @objc dynamic var sliderView: SlidingContainerSliderView!
    @objc dynamic var sliderViewShown: Bool = true

    @objc dynamic var delegate: SlidingContainerViewControllerDelegate?

    var index: Int = 0

    // MARK: Init

    init (parent: UIViewController, contentViewControllers: [UIViewController], titles: [String]) {
        super.init(nibName: nil, bundle: nil)
        self.contentViewControllers = contentViewControllers
        self.titles = titles

        // Move to parent

        willMove(toParentViewController: parent)
        parent.addChildViewController(self)
        didMove(toParentViewController: parent)

        // Setup Views

        sliderView = SlidingContainerSliderView(width: view.frame.size.width, titles: titles)
        sliderView.frame.origin.y = parent.topLayoutGuide.length
        sliderView.setShadow(color: .black, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
        sliderView.sliderDelegate = self

        contentScrollView = UIScrollView(frame: view.frame)
        contentScrollView.showsHorizontalScrollIndicator = false
        contentScrollView.showsVerticalScrollIndicator = false
        contentScrollView.isPagingEnabled = false
        contentScrollView.scrollsToTop = false
        contentScrollView.delegate = self
        contentScrollView.contentSize.width = contentScrollView.frame.size.width * CGFloat(contentViewControllers.count)

        view.addSubview(contentScrollView)
        view.addSubview(sliderView)

        // Add Child View Controllers

        var currentX: CGFloat = 0
        for vc in contentViewControllers {
            vc.view.frame = CGRect(
                x: currentX,
                y: parent.topLayoutGuide.length,
                width: view.frame.size.width,
                height: view.frame.size.height - parent.topLayoutGuide.length - parent.bottomLayoutGuide.length)
            contentScrollView.addSubview(vc.view)

            currentX += contentScrollView.frame.size.width
        }

        // Move First Item

        setCurrentViewControllerAtIndex(index: 0)
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }

    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: ChildViewController Management

    func setCurrentViewControllerAtIndex (index: Int) {

        for i in 0..<self.contentViewControllers.count {
            let vc = contentViewControllers[i]

            if i == index {

                vc.willMove(toParentViewController: self)
                addChildViewController(vc)
                vc.didMove(toParentViewController: self)

                delegate?.slidingContainerViewControllerDidMoveToViewController?(slidingContainerViewController: self, viewController: vc, atIndex: index)
            } else {

                vc.willMove(toParentViewController: self)
                vc.removeFromParentViewController()
                vc.didMove(toParentViewController: self)
            }
        }

        self.index = index
        sliderView.selectItemAtIndex(index: index)
        contentScrollView.setContentOffset(
            CGPoint(x: contentScrollView.frame.size.width * CGFloat(index), y: 0),
            animated: true)

        navigationController?.navigationItem.title = titles[index]
    }

    // MARK: SlidingContainerSliderViewDelegate

    func slidingContainerSliderViewDidPressed(slidingtContainerSliderView slidingContainerSliderView: SlidingContainerSliderView, atIndex: Int) {
        sliderView.shouldSlide = false
        setCurrentViewControllerAtIndex(index: atIndex)
    }

    // MARK: SliderView

    func hideSlider () {

        if !sliderViewShown {
            return
        }

        UIView.animate(withDuration: 0.001,
            animations: {[unowned self] in
                self.sliderView.frame.origin.y += self.parent!.topLayoutGuide.length + self.sliderView.frame.size.height
            },
            completion: {[unowned self] _ in
                self.sliderViewShown = false
                self.delegate?.slidingContainerViewControllerDidHideSliderView?(slidingContainerViewController: self)
            })
    }

    func showSlider () {

        if sliderViewShown {
            return
        }

        UIView.animate(withDuration: 0.001,
            animations: {[unowned self] in
                self.sliderView.frame.origin.y -= self.parent!.topLayoutGuide.length + self.sliderView.frame.size.height
            },
            completion: {[unowned self] _ in
                self.sliderViewShown = true
                self.delegate?.slidingContainerViewControllerDidShowSliderView?(slidingContainerViewController: self)
            })
    }

    // MARK: UIScrollViewDelegate

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.panGestureRecognizer.state == .began {
                sliderView.shouldSlide = true
        }

        let r = contentScrollView.contentSize.width - contentScrollView.frame.size.width
        let s = sliderView.contentSize.width - sliderView.frame.size.width
        let c = contentScrollView.contentOffset.x
        let a = c / r

        if sliderView.contentSize.width > sliderView.frame.size.width && sliderView.shouldSlide == true {
            sliderView.contentOffset = CGPoint(x: s * a, y: 0)
        }

    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let index = scrollView.contentOffset.x / contentScrollView.frame.size.width
        setCurrentViewControllerAtIndex(index: Int(index))
    }
}

extension UIGestureRecognizerState {
    public var description: String {
        get {
            switch self {
            case .began:
                return "Began"
            case .cancelled:
                return "Cancelled"
            case .changed:
                return "Changed"
            case .ended:
                return "Ended"
            case .failed:
                return "Failed"
            case .possible:
                return "Possible"
            }
        }
    }
}
