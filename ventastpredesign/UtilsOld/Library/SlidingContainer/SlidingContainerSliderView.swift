//
//  SlidingContainerSliderView.swift
//  SlidingContainerViewController
//
//  Created by Cem Olcay on 10/04/15.
//  Copyright (c) 2015 Cem Olcay. All rights reserved.
//

import UIKit

struct SlidingContainerSliderViewAppearance {

    var backgroundColor: UIColor

    var font: UIFont
    var selectedFont: UIFont

    var textColor: UIColor
    var selectedTextColor: UIColor

    var outerPadding: CGFloat
    var innerPadding: CGFloat

    var selectorColor: UIColor
    var selectorHeight: CGFloat

    var fixedWidth: Bool
}

protocol SlidingContainerSliderViewDelegate {
    func slidingContainerSliderViewDidPressed (slidingtContainerSliderView: SlidingContainerSliderView, atIndex: Int)
}

class SlidingContainerSliderView: UIScrollView, UIScrollViewDelegate {

    // MARK: Properties

    var appearance: SlidingContainerSliderViewAppearance! {
        didSet {
            draw()
        }
    }

    @objc dynamic var shouldSlide: Bool = true

    let sliderHeight: CGFloat = 60
    @objc dynamic var titles: [String]!

    @objc dynamic var labels: [UILabel] = []
    @objc dynamic var selector: UIView!

    var sliderDelegate: SlidingContainerSliderViewDelegate?

    // MARK: Init

    init (width: CGFloat, titles: [String]) {
        super.init(frame: CGRect(x: 0, y: 0, width: width, height: sliderHeight))
        self.titles = titles

        delegate = self
        showsHorizontalScrollIndicator = false
        showsVerticalScrollIndicator = false
        scrollsToTop = false

        appearance = SlidingContainerSliderViewAppearance(
            backgroundColor: UIColor(white: 0, alpha: 0.3),

            font: UIFont(name: "Montserrat-Regular", size: 16)!,
            selectedFont: UIFont(name: "Montserrat-Regular", size: 16)!,

            textColor: UIColor.darkGray,
            selectedTextColor: UIColor.white,

            outerPadding: 10,
            innerPadding: 10,

            selectorColor: UIColor.red,
            selectorHeight: 0,
            fixedWidth: false)

        draw()
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }

    // MARK: Draw

    func draw () {

        // clean
        if labels.count > 0 {
            for label in labels {

                label.removeFromSuperview()

                if selector != nil {
                    selector.removeFromSuperview()
                    selector = nil
                }
            }
        }

        labels = []
        backgroundColor = appearance.backgroundColor

        if appearance.fixedWidth {
            var labelTag = 0
            let width = CGFloat(frame.size.width) / CGFloat(titles.count)

            for title in titles {
                let label = labelWithTitle(title: title)
                label.frame.origin.x = (width * CGFloat(labelTag))
                label.frame.size = CGSize(width: width, height: label.frame.size.height)
                label.center.y = frame.size.height/2
                labelTag += 1
                label.tag = labelTag

                addSubview(label)
                labels.append(label)
            }

            let selectorH = appearance.selectorHeight
            selector = UIView(frame: CGRect(x: 0, y: frame.size.height - selectorH, width: width, height: selectorH))
            selector.backgroundColor = appearance.selectorColor
            addSubview(selector)

            contentSize = CGSize(width: frame.size.width, height: frame.size.height)
        } else {
            var labelTag = 0
            var currentX = appearance.outerPadding

            for title in titles {
                let label = labelWithTitle(title: title)
                label.frame.origin.x = currentX
                label.center.y = frame.size.height/2
                labelTag += 1
                label.tag = labelTag

                addSubview(label)
                labels.append(label)
                currentX += label.frame.size.width + appearance.outerPadding
            }

            let selectorH = appearance.selectorHeight
            selector = UIView(frame: CGRect(x: 0, y: frame.size.height - selectorH, width: 100, height: selectorH))
            selector.backgroundColor = appearance.selectorColor
            addSubview(selector)

            contentSize = CGSize(width: currentX, height: frame.size.height)
        }

    }

    func labelWithTitle (title: String) -> UILabel {

        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        label.text = title
        label.font = appearance.font
        label.textColor = appearance.textColor
        label.textAlignment = .center

        label.sizeToFit()
        label.frame.size.width += appearance.innerPadding * 2

        label.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(SlidingContainerSliderView.didTap(_:))))
        label.isUserInteractionEnabled = true

        return label
    }

    // MARK: Actions

    @objc func didTap (_ tap: UITapGestureRecognizer) {
        self.sliderDelegate?.slidingContainerSliderViewDidPressed(slidingtContainerSliderView: self, atIndex: tap.view!.tag - 1)
    }

    // MARK: Menu

    func selectItemAtIndex (index: Int) {

        // Set Labels

        for i in 0..<self.labels.count {
            let label = labels[i]

            if i == index {

                // label.textColor = appearance.selectorColor
                label.font = UIFont.boldSystemFont(ofSize: 16.0)
                // label.font = appearance.selectedFont

                if !appearance.fixedWidth {
                    label.sizeToFit()
                    label.frame.size.width += appearance.innerPadding * 2
                }

                // Set selector

                UIView.animate(withDuration: 0.0015, animations: {[unowned self] in

                    if i == 1 {
                        self.selector.frame = CGRect(
                            x: label.frame.origin.x,
                            y: self.selector.frame.origin.y,
                            width: label.frame.size.width/2,
                            height: self.appearance.selectorHeight)

                        self.selector.frame.centerX = (label.frame.size.width * 3)/2
                    } else {
                        self.selector.frame = CGRect(
                            x: label.frame.origin.x,
                            y: self.selector.frame.origin.y,
                            width: label.frame.size.width/2,
                            height: self.appearance.selectorHeight)

                        self.selector.frame.centerX = label.frame.size.width/2
                    }

                })

            } else {

                label.textColor = appearance.textColor
                label.font = appearance.font
                if !appearance.fixedWidth {
                    label.sizeToFit()
                    label.frame.size.width += appearance.innerPadding * 2
                }
            }
        }
    }

}

extension CGRect {
    /** Creates a rectangle with the given center and dimensions
     - parameter center: The center of the new rectangle
     - parameter size: The dimensions of the new rectangle
     */
    init(center: CGPoint, size: CGSize) {
        self.init(x: center.x - size.width / 2, y: center.y - size.height / 2, width: size.width, height: size.height)
    }

    /** the coordinates of this rectangles center */
    var center: CGPoint {
        get { return CGPoint(x: centerX, y: centerY) }
        set { centerX = newValue.x; centerY = newValue.y }
    }

    /** the x-coordinate of this rectangles center
     - note: Acts as a settable midX
     - returns: The x-coordinate of the center
     */
    var centerX: CGFloat {
        get { return midX }
        set { origin.x = newValue - width * 0.5 }
    }

    /** the y-coordinate of this rectangles center
     - note: Acts as a settable midY
     - returns: The y-coordinate of the center
     */
    var centerY: CGFloat {
        get { return midY }
        set { origin.y = newValue - height * 0.5 }
    }

    // MARK: - "with" convenience functions

    /** Same-sized rectangle with a new center
     - parameter center: The new center, ignored if nil
     - returns: A new rectangle with the same size and a new center
     */
    func with(center: CGPoint?) -> CGRect {
        return CGRect(center: center ?? self.center, size: size)
    }

    /** Same-sized rectangle with a new center-x
     - parameter centerX: The new center-x, ignored if nil
     - returns: A new rectangle with the same size and a new center
     */
    func with(centerX: CGFloat?) -> CGRect {
        return CGRect(center: CGPoint(x: centerX ?? self.centerX, y: centerY), size: size)
    }

    /** Same-sized rectangle with a new center-y
     - parameter centerY: The new center-y, ignored if nil
     - returns: A new rectangle with the same size and a new center
     */
    func with(centerY: CGFloat?) -> CGRect {
        return CGRect(center: CGPoint(x: centerX, y: centerY ?? self.centerY), size: size)
    }

    /** Same-sized rectangle with a new center-x and center-y
     - parameter centerX: The new center-x, ignored if nil
     - parameter centerY: The new center-y, ignored if nil
     - returns: A new rectangle with the same size and a new center
     */
    func with(centerX: CGFloat?, centerY: CGFloat?) -> CGRect {
        return CGRect(center: CGPoint(x: centerX ?? self.centerX, y: centerY ?? self.centerY), size: size)
    }
}
