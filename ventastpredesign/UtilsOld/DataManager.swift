//
//  DataManager.swift
//  ventastpredesign
//
//  Created by Arturo Ceron Epigmenio on 27/06/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import UIKit
import RealmSwift

public  protocol TX: NSObjectProtocol {
    func execute(transactions: DataManager.Transactions)
}

open  class DataManager: NSObject {

    open  var mRealm: Realm?
    open  var mTransactions: Transactions?

    open  func getInstance() -> DataManager {
        if mRealm == nil {
            mRealm = try! Realm()
        }
        if mTransactions == nil {
            mTransactions = Transactions(mRealm: mRealm!)
        }

        return self
    }

    open  func deleteFormalityEntity() {
        mRealm?.deleteAll()
    }

    open  func queryWhere<T: Object>(object: T.Type) -> Query<T> {
        return Query<T>().create(object: object, mRealm: mRealm!)
    }

    open  func tx(execute: (DataManager.Transactions) -> Void) {
        try! mRealm?.write({ () -> Void in
            execute(mTransactions!)
        })
    }

    open  class Query<T: Object> {

        open var mRealm: Realm!
        open var results: Results<T>!

        open   func create(object: T.Type, mRealm: Realm) -> Query {
            self.mRealm = mRealm
            results = mRealm.objects(object)
            return self
        }

        open  func findFirst() -> T? {
            return results.first
        }

        open  func list() -> [T] {
            return Array(results)
        }

        open  func remove() {
            try! mRealm.write({ () -> Void in
                mRealm.delete(results)
            })
        }

    }

    open  class Transactions: NSObject {

        open  var mRealm: Realm!

        public init(mRealm: Realm) {
            self.mRealm = mRealm
        }

        open    func save<T: Object>(object: T) {
            mRealm.add(object, update: true)
        }
    }

    //    func insert<T: Object>(object: T) -> T!{
    //        let realm = try! Realm()
    //        realm.beginWrite()
    //        realm.add(object, update: true)
    //        try! realm.commitWrite()
    //        return object
    //    }

}
