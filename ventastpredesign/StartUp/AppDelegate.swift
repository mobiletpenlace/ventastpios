//
//  AppDelegate.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 13/03/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import RealmSwift
import Fabric
import GoogleMaps
import GooglePlaces
import UserNotifications
import Firebase
// import FirebaseInstanceID
import FirebaseMessaging
import Amplitude
import IQKeyboardManagerSwift
import AppAuth
import FirebaseCore
import FirebaseFirestore
import FirebaseAuth
import FirebaseAnalytics

let credentials = (
    appId: "KNW9xa0fnPE5sJvR46Pf",
    appCode: "f53pWKrdfUpTDeG-U4DwqA",
    licenseKey: "GDNyIoiNPYs5yg5uAQe7twRY5Tzw15IM5ieDohD3/fWI9HSjaoI6AHCO4evXPEzYSFWxjvxQcVYMMVbSUF0mMM6/bo3QpPQCHhSxyjxaR7lO3dhsrJQ/VANGzrIT5nNBLnUmKedLtMy7IRAJxLHnK//j+tM8NGn78CQEz9gVWFF7o8hzZDATaRVROnAzNKZsPOuJQyicdarAvsUZNs6xgbZgRiSqsYn0o6N+CJrI25oUpqM1XZlkxn2P3se4YU1emBLys5iehyUL82Z+K3guQPOpaU4SSrWGR3XKe4vaqUp+lAgxdIfyOmvfHyCWrb/d8nZrGh6cUl2oKEDasz3gDV1kmU2zkJudz4gAcip+uwnlHWSWHZD1ZjZ5NCinwdA4SqAcjthb+pRuE9fuuyPT/PPZ2c8xzXLxJJEFIxCdwS8Tcy0Pe9QanGwjCbbFoD4Urn2dpgsMTn5ngxJk9QVZzKPk9gctJdzp0TD3ejEW+ISJ+TsaAapDwkdzN48LNRSVGqQv+w+8FVWQZClrmF8bmXbld3sExsDKiBUeggh/WwHo3hrMFFZSpSQnAJOu90/j1C571Xs8Y0Klbpg+tde1fvtVnnMSDBhyOR17aKQwI568phLT1Guo+U8z969si bebs2yw5f1WUbV3XlrMtCuZlz4iJetscIOKKCTg+XZe046yYN70="
)

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    // property of the app's AppDelegate
    var currentAuthorizationFlow: OIDExternalUserAgentSession?
    var window: UIWindow?
    static var isProd = false
    static var isCol = false
    static var menu_bool = true
    static var mServiceStatus = false
    static var isValidateCoverageFromMenu = false
    static var API_SYSTEMSTP = "https://apiservice.sistemastp.com.mx"
    static var API_SYSTEMSTP_TEST = "https://apiservice-qa.sistemastp.com.mx"
    static var API_CARIAI = "https://cariai.com/hsmapi/createtoken"
    static var API_SEND_HSM = "https://cariai.com/hsmapi/sendhsm"
    static var API_TOKEN_CARIAI = "https://cariai.com"
    static var API_CLOUD_TOKEN = "https://mc16y3xk-dkjxnm8kw0nwjxl9xb4.auth.marketingcloudapis.com/v2/token"
    static var API_CLOUD_EVENTS = "https://mc16y3xk-dkjxnm8kw0nwjxl9xb4.rest.marketingcloudapis.com/interaction/v1/events"
    static var API_TOTAL_TEST = "https://msstest.totalplay.com.mx"
    static var API_TOTAL = "https://mss.totalplay.com.mx"
    //    static var API_TOTAL = "https://mssqa.totalplay.com.mx"
    //    static var API_SALESFORCE = "https://totalplay--Pruebas.my.salesforce.com/services/"
    static var API_SALESFORCE = "https://totalplay--pruebas.sandbox.my.salesforce.com/services/"
    static var API_GOOGLE_COORDINATES = "https://addressvalidation.googleapis.com/v1:validateAddress?key=AIzaSyBeHJpR37gbG_MWrtk5qNeaYeQ7IeoSi_w"

    // "https://totalplay.my.salesforce.com/services/"//"https://totalplay--Pruebas.my.salesforce.com/services/"
    //    static let API_TOTAL_PRUEBAS = "https://totalplay--pruebas.my.salesforce.com/services/"
    static let API_TOTAL_PRUEBAS = "https://totalplay--pruebas.sandbox.my.salesforce.com/services/"
    static var GRANT = "grant_type=password&client_id=3MVG9oZtFCVWuSwOvwwAVjUooDcuXh_jhkAkE5gY5U9gj92CtWtoPdYLlfQCdIrdWoWjg7puk0yOiu5B.srv.&client_secret=47A8074CEBF6718C127C4726B0CB2D001605D79E82C16A5E839CBEAAF8A9A863&username=appresidencial@totalplay.com.mx.pruebas&password=Totalplay123456TmXRrPxqwft5MLVGw4X7cNVW" // "grant_type=password&client_id=3MVG9KI2HHAq33RxdFES5KXE7S33bVP0UfROooHlOdvmRoaGjvZuU3SkV_HL2JwVF.DEzXLnq39uKPYUbhIWM&client_secret=975959C56FCF9F6A916A39D172E7E519933FECA6B1A78A3EC8B4078D26C80887&username=appresidencial@totalplay.com.mx&password=totalplay1234564up20pvZSyYibYOeEPFDNaRYJ"
    static var TOKEN_SYSTEM_USER: String = "qxAq5yDnMu8YkfiNtmCqwwZGEP5v2BxihT9VZhGGuvBz51qy"
    static var TOKEN_SYSTEM_PASS: String = "medGjgTlWWSScYWACLihGXFyoHS8CMXtRmiYi8KJ5IURODhz3TWKGlmRoKZsRjix"
    static var TOKEN = "token"
    static var Gmaps = true
    let date = Date()
    let formatter = DateFormatter()
    let calendar = Calendar.current
    let componentes = DateComponents()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        // Enable sending automatic session events
        //Amplitude.instance().defaultTracking.sessions = true
        Amplitude.instance().trackingSessionEvents = true
        // Initialize SDK
        Amplitude.instance().initializeApiKey("5c1abd02e2de482d56c48c9f184923a1")
        // Log an event
        Amplitude.instance().logEvent("App_Launch")

        // MARK: Configure for firebase StreeTP
        /*let filePathInfoPlistST = Bundle.main.path(forResource: "GoogleService-Info", ofType: "plist")
        let optionInfoPlistST = FirebaseOptions(contentsOfFile: filePathInfoPlistST!)
        FirebaseApp.configure(options: optionInfoPlistST!)*/

        // Secondary firebase configuration
        /*   let secondaryOptions = FirebaseOptions(googleAppID: "1:507220461440:ios:15aa0271f1fb570ccea365",
         gcmSenderID: "507220461440")
         secondaryOptions.apiKey = "AIzaSyBcfjgd3ASYt7fliftXWhoIAFZz-CMvhEU"
         secondaryOptions.projectID = "ventasresidencial-155ca"
         FirebaseApp.configure(name: "secondary", options: secondaryOptions)*/

        GMSServices.provideAPIKey("AIzaSyAsKFmeJMPGlJGjFcx4_30Yb5ugKFz0IEE")
        GMSPlacesClient.provideAPIKey("AIzaSyBX3feB3Lzeg-7wr9zrNEbJj2jBFKfLTS0")
        // Fabric
        IQKeyboardManager.shared.enable = true
        //  Fabric.with([Crashlytics.self])
        Fabric.sharedSDK().debug = true

        UNUserNotificationCenter.current().delegate = self

        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(
          options: authOptions,
          completionHandler: { _, _ in }
        )
        application.registerForRemoteNotifications()

        //UIModalPresentationStyle.fullScreen

        let config = Realm.Configuration(deleteRealmIfMigrationNeeded: true)
        Realm.Configuration.defaultConfiguration = config

        // MARK: Configure for Firebase ventasRecidencial info.plist
        let filePathInfoPlistVR = Bundle.main.path(forResource: "GoogleService-Info-ventas", ofType: "plist")
        let optionsInfoPlistVR = FirebaseOptions(contentsOfFile: filePathInfoPlistVR!)
        FirebaseApp.configure(options: optionsInfoPlistVR!)

        Messaging.messaging().delegate = self

        if let storyboardName = Bundle.main.object(forInfoDictionaryKey: "MainStoryboardName") as? String {
            let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
            window = UIWindow(frame: UIScreen.main.bounds)
            let initialViewController = storyboard.instantiateInitialViewController()
            window?.rootViewController = initialViewController
            window?.makeKeyAndVisible()
        }

        return true
    }

    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("Firebase registration token: \(fcmToken ?? String.empty)")
        debugPrint("FCM Token \(fcmToken ?? "NO HAY TOKEN" )")
    }

    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {

        if self.window?.rootViewController?.presentedViewController is PlayerViewController {
            return UIInterfaceOrientationMask.landscapeLeft
        } else {
            return UIInterfaceOrientationMask.portrait
        }
    }

    /*func applicationReceivedRemoteMessage(_ remoteMessage: MessagingRemoteMessage) {
     print(remoteMessage.appData)
     let token = Messaging.messaging().fcmToken
     print("FCM token: \(token ?? "")")
     }*/

    //    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
    //        var token = ""
    //        for i in 0..<deviceToken.count {
    //            token = token + String(format: "%02.2hhx", arguments: [deviceToken[i]])
    //        }
    //        print("Registration succeeded! Token: ", token)
    //        let a = Messaging.messaging().fcmToken
    //        print("FCM token: \(a ?? "")")
    //
    //        if let refreshedToken = InstanceID.instanceID().token() {
    //            print("InstanceID token: \(refreshedToken)")
    //        }
    //            Messaging.messaging().apnsToken = deviceToken
    //    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Registration failed!")
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("Background info")
        print(userInfo)
    }

    // Firebase notification received
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler   completionHandler: @escaping (_ options: UNNotificationPresentationOptions) -> Void) {

        // custom code to handle push while app is in the foreground
        print("Handle push from foreground \(notification.request.content.userInfo)")

        // Reading message body
        let dict = notification.request.content.userInfo["aps"] as! NSDictionary
        var messageBody: String?
        var messageTitle: String = "Alert"

        if let alertDict = dict["alert"] as? [String: String] {
            messageBody = alertDict["body"]!
            if alertDict["title"] != nil {
                messageTitle  = alertDict["title"]!
            }
        } else {
            messageBody = dict["alert"] as? String
        }

        print("Message body is \(messageBody!) ")
        print("Message messageTitle is \(messageTitle) ")
        let notificacion = Notificaciones()
        notificacion.Titulo = String(messageBody!)
        notificacion.Mensaje = String(messageTitle)
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        let fecha = calendar.date(byAdding: componentes, to: date)
        let fechaString = formatter.string(from: fecha!)
        notificacion.Fecha = fechaString
        _ = RealManager.insert(object: notificacion)

        // Let iOS to display message
        completionHandler([.alert, .sound, .badge])
    }
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let gcmLabel = "google.c.a.c_l"
        print("Message \(response.notification.request.content.userInfo)")
        print("MENSAJE DE PRUEBA BODY \(response.notification.request.content.body)")
        print("MENSAJE DE PRUEBA TITLE \(response.notification.request.content.title)")
        let notificacion = Notificaciones()
        notificacion.Titulo = String(response.notification.request.content.title)
        notificacion.Mensaje = String(response.notification.request.content.body)
        // formatter.dateFormat = "yyyy-MM-dd"
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        let fecha = calendar.date(byAdding: componentes, to: date)
        let fechaString = formatter.string(from: fecha!)
        notificacion.Fecha = fechaString
        _ = RealManager.insert(object: notificacion)
        completionHandler()
    }

    func showAlertAppDelegate(title: String, message: String, buttonTitle: String, window: UIWindow) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: buttonTitle, style: .default, handler: nil))
        window.rootViewController?.present(alert, animated: false, completion: nil)
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


    // MARK: Remote Notifications
    func getFCMToken() {
        Messaging.messaging().token { token, error in
            if let error = error {
                debugPrint("ERROR -> \(error.localizedDescription)")
            } else if let token = token {
                debugPrint("TOKEN -> \(token)")
            }
        }
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
        getFCMToken()
    }
}

extension AppDelegate: MessagingDelegate {
}
