//
//  ImageView+Extension.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta O. on 03/12/19.
//  Copyright © 2019 Juan Reynaldo Escobar Miron. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {

    func downloaded(from url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFill) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async {
                self.image = image
            }
        }.resume()
    }

    func downloaded(from link: String, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}

extension UIImage {

    public class func URLimage(url: String, completion: @escaping (UIImage?) -> Void) {
        guard let bundleURL = URL(string: url) else {
            print("SwiftGif: This image named \"\(url)\" does not exist")
            completion(nil)
            return
        }

        let task = URLSession.shared.dataTask(with: bundleURL) { data, response, error in
            if let error = error {
                print("SwiftGif: Cannot download image from \"\(url)\". Error: \(error)")
                completion(nil)
                return
            }

            guard let imageData = data else {
                print("SwiftGif: Data is nil for image named \"\(url)\"")
                completion(nil)
                return
            }

            let image = UIImage(data: imageData)
            completion(image)
        }

        task.resume()
    }

}
