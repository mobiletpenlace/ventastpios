//
//  String+Extension.swift
//  StreetTpIOS
//
//  Created by Juan Reynaldo Escobar Miron on 9/25/19.
//  Copyright © 2019 Juan Reynaldo Escobar Miron. All rights reserved.
//

import Foundation

extension String {

    func urlEncoded() -> String? {
        var allowedCharacterSet = CharacterSet.urlQueryAllowed
        allowedCharacterSet.remove("+")
        if let partiallyEncodedString = self.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet) {
            let encodedString = partiallyEncodedString.replacingOccurrences(of: "+", with: "%2B")
            return encodedString
        }
        return nil
    }

    func trim() -> String {
        return self.trimmingCharacters(in: NSCharacterSet.whitespaces)
    }

    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + self.lowercased().dropFirst()
    }

    func currencyFormatting() -> String {
        if let value = Double(self) {
            let formatter = NumberFormatter()
            formatter.numberStyle = .currency
            formatter.maximumFractionDigits = 2
            formatter.minimumFractionDigits = 2
            if let str = formatter.string(for: value) {
                return str
            }
        }
        return ""
    }

    func base64Encoded() -> String? {
          data(using: .utf8)?.base64EncodedString()
      }

}

public extension String {
    static var emptyString: String {
        get {
            return ""
        }
    }

    static var empty: String {
        get {
            return ""
        }
    }

    static var space: String {
        get {
            return " "
        }
    }
}
