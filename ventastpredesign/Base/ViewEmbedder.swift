//
//  ViewEmbedder.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta O. on 24/10/19.
//  Copyright © 2019 Juan Reynaldo Escobar Miron. All rights reserved.
//

import UIKit

class ViewEmbedder {
    class func embed(
        parent: UIViewController,
        container: UIView,
        child: UIViewController,
        previous: UIViewController?) {

        if let previous = previous {
            removeFromParent(vc: previous)
        }
        child.willMove(toParentViewController: parent)
        parent.addChildViewController(child)
        container.addSubview(child.view)
        child.didMove(toParentViewController: parent)
        let w = container.frame.size.width
        let h = container.frame.size.height
        child.view.frame = CGRect(x: 0, y: 0, width: w, height: h)
    }

    class func removeFromParent(vc: UIViewController) {
        print("VIEW CONTROLLER: \(vc.restorationIdentifier ?? "Sin identificador")")
        vc.willMove(toParentViewController: nil)
        vc.view.removeFromSuperview()
        vc.removeFromParentViewController()

    }

    class func embed(withIdentifier id: String, parent: UIViewController, container: UIView, _ completion: ((UIViewController) -> Void)? = nil) {
        let viewAlert = UIStoryboard(name: id, bundle: nil)
        let vc = viewAlert.instantiateViewController(withIdentifier: id)

        print("EMBED: id = \(id), parent = \(parent.restorationIdentifier), container = \(container.subviews.count) ")

//        lazy var secondVC: SecondViewController = {
//            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("SecondVC") as! SecondViewController
//            return vc
//          }()
        embed(
            parent: parent,
            container: container,
            child: vc,
            previous: parent.childViewControllers.first
        )
        completion?(vc)
    }
}
