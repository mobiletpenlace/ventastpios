//
//  BaseFormatDate.swift
//  StreetTpIOS
//
//  Created by Juan Reynaldo Escobar Miron on 9/25/19.
//  Copyright © 2019 Juan Reynaldo Escobar Miron. All rights reserved.
//

import Foundation

protocol BaseFormatDate {
    func formatDate(_ date: Date, format: String) -> String
    func formatDate(_ dates: [Date], format: String) -> [String]
    func createDate(_ date: String, format: String) -> Date
    func diference(_ fecha1: Date, _ fecha2: Date, _ tipo: Calendar.Component) -> DateComponents
    func incrementDate(_ date: Date, value: Int, byAdding: Calendar.Component) -> Date
}

extension BaseFormatDate {

    func formatDate(_ date: Date, format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: date)
    }

    func formatDate(_ dates: [Date], format: String) -> [String] {
        var lista = [String]()
        for date in dates {
            lista.append(formatDate(date, format: format))
        }
        return lista
    }

    func createDate(_ date: String, format: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        guard let dateN = dateFormatter.date(from: date) else {
            fatalError()
        }
        return dateN
    }

    func diference(_ fecha1: Date, _ fecha2: Date, _ tipo: Calendar.Component) -> DateComponents {
        let components = Calendar.current.dateComponents(
            [tipo],
            from: fecha1,
            to: fecha2
        )
        return components
    }

    func incrementDate(_ date: Date, value: Int, byAdding: Calendar.Component) -> Date {
        return Calendar.current.date(byAdding: byAdding, value: value, to: date)!
    }
}
