//
//  BaseAlamofireManager.swift
//  StreetTpIOS
//
//  Created by Juan Reynaldo Escobar Miron on 9/27/19.
//  Copyright © 2019 Juan Reynaldo Escobar Miron. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import SwiftJWT
import Foundation

class BaseServerDataManager<Res: BaseResponse> {

    open var requestUrl: String
    open var delegate: AlamofireResponseDelegate
    open var request: Alamofire.Request?
    let URL = "https://mssoqa.totalplay.com.mx/oauth/logintoken"
    let URLREFRESH = "https://mssoqa.totalplay.com.mx/oauth/refreshtoken"
    let obtainToken: ObtainToken = ObtainToken()

    public init(requestUrl: String, delegate: AlamofireResponseDelegate) {
        self.delegate = delegate
        self.requestUrl = requestUrl
    }

    open func getJsonDebug(requestUrl: String) -> String {
        return ""
    }

    open func getDebugEnabled() -> Bool {
        return false
    }

    open var Manager: Alamofire.SessionManager = {
        // Create the server trust policies
        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            "https://189.203.181.233": .disableEvaluation,
            "189.203.181.233": .disableEvaluation
        ]

        // Create custom manager
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        let manager = Alamofire.SessionManager(
            configuration: URLSessionConfiguration.default,
            serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
        )

        return manager
    }()

    open func request(requestModel: AlfaRequest) {

        petition(requestModel: requestModel)

    }

    func petition(requestModel: AlfaRequest) {
        if getDebugEnabled() {
            let json: String = getJsonDebug(requestUrl: self.requestUrl)
            if json != "" {
                let response = Res(JSONString: json)
                self.delegate.onSuccessLoadResponse(requestUrl: self.requestUrl, response: response!)
            }
            return
        }
        if ConnectionUtils.isConnectedToNetwork() {
            delegate.onRequestWs()
            Logger.blockSeparator()
            let paramsPrint  = Mapper().toJSONString(requestModel, prettyPrint: true)
            let params = Mapper().toJSON(requestModel)
            Logger.println(String("Request (\(requestUrl))= \(paramsPrint)"))
            Logger.blockSeparator()

            request = Manager.request(self.requestUrl, method: HTTPMethod.post, parameters: params, encoding: JSONEncoding.default, headers: /*[
                "Content-Type": "application/json",
                "Authorization": "Bearer " + statics.TOKEN,
                "Accept": "application/json"
                 ]*/ApiDefinition.headersMiddle).responseObject {(response: DataResponse<Res>) in
                    self.processResponse(requestModel: requestModel, response: response)
            }
        } else {
            delegate.onErrorConnection()
        }
    }

    func processResponse(requestModel: AlfaRequest, response: DataResponse<Res>) {
        switch response.result {
        case .success:

            let object = Mapper().toJSON(response.result.value! as Res)
            let objectPrint = Mapper().toJSONString(response.result.value! as Res, prettyPrint: true)
            Logger.println("Response (\(requestUrl))\(String(describing: objectPrint))")
            if let error = object["error_message"] {
                Logger.println("error: El token ha expirado")
                /*self.obtainToken({(res) in
                    if(res){
                        self.request(requestModel: requestModel)
                    }
                })*/
            } else {
                self.delegate.onSuccessLoadResponse(requestUrl: self.requestUrl, response: response.result.value! as Res)
                Logger.blockSeparator()
            }

        case .failure:
            let statusCode = response.response?.statusCode ?? -1
            if statusCode == 401 {
                /*self.obtainToken({(res) in
                    if(res){
                        self.request(requestModel: requestModel)
                    }
                })*/
            } else {
                self.delegate.onErrorLoadResponse(requestUrl: self.requestUrl, messageError: "")
            }
        }
    }

    /*func obtainToken(_ callBack:@escaping ((Bool)) -> Void) {
        TPCipher.shared.setKey(b64Key: "VG90YWxwbGF5TW9iaWxlU2VjdXJpdHlMYXllcjIwMTk=")
        guard let subEncrypt = TPCipher.shared.code(plainText: "15006306") else {
            return
        }
        guard let prnEncrypt = TPCipher.shared.code(plainText: "GrupoSalinas2019") else {
            return
        }
        Logger.println(subEncrypt)
        Logger.println(prnEncrypt)
        
        
        let header = Header(alg: "RS256")
        let claims = TokenClaim(iss: "8ac11dfc-3203-477a-b815-368cbddbef6f", aud: "http://apigateway/api/oauth/token", exp: Date(timeIntervalSinceNow: 60), iat: Date(timeIntervalSinceNow: 0), sub: subEncrypt, prn: prnEncrypt)
        let resultexp = String("\(claims.exp.timeIntervalSince1970)").split(separator: ".")
        let resultiat = String("\(claims.iat.timeIntervalSince1970)").split(separator: ".")
        statics.dataiat = String(resultiat[0])
        let claimsString = String("{\"iss\": \"\(claims.iss)\", \"aud\": \"\(claims.aud)\", \"exp\": \"\(resultexp[0])\", \"iat\": \"\(resultiat[0])\", \"sub\": \"\(claims.sub)\", \"prn\": \"\(claims.prn)\"} ")
        var myJWT = JWT(header: header, claims: (claimsString.data(using: .utf8)?.base64EncodedString())!.replacingOccurrences(of: "+", with: "-")
            .replacingOccurrences(of: "/", with: "_")
            .replacingOccurrences(of: "=", with: ""))
        
        let privateKeyPath = Bundle.main.url(forResource: "TotalPlay360", withExtension: "pem")
        guard let privateKey = try? Data(contentsOf: privateKeyPath! , options: .alwaysMapped) else {
            callBack((false))
            return
        }
        
        let jwtSigner = JWTSigner.rs256(privateKey: privateKey)
        guard let signedJWT = try? myJWT.sign(using: jwtSigner) else {
            callBack((false))
            return }
        
        TokenService.tokenJWT = signedJWT
        
        var token: String = ""
        var res: TokenResponse!
        let request = String("grant_type=urn:ietf:params:oauth:grant-type:jwt-bearer&assertion=")
        let delayTime = DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            self.obtainToken.postToken(model: request, url: self.URL ){ data in
                guard let data = data else{
                    callBack((false))
                    return
                }
                do{
                    let decoder = JSONDecoder()
                    res = try decoder.decode(TokenResponse.self, from: data)
                    //statics.refreshToken = res.refresh_token ?? ""
                    //statics.expiresIn = res.expires_in ?? ""
                    statics.TOKEN = res.access_token ?? ""
                    callBack((true))
                    
                }catch let err{
                    print(err)
                    callBack((false))
                    
                }
            }
            
        }
    }*/

}
