//
//  LoginRequest.swift
//  EstrategiaDigital
//
//  Created by Jorge Hdez Villa on 09/08/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import ObjectMapper

open  class LoginRequestBase: NSObject, Mappable {

    open  var user: String?
    open  var secretWord: String?
    open  var ip: String?
    open  var versionAPP: String?

    override public init() {
        super.init()
    }

    public init(user: String, secretWord: String, ip: String) {
        self.user = user
        self.secretWord = secretWord
        self.ip = ip
    }

    public required init?(map: Map) {

    }

    public func mapping(map: Map) {
        user <- map["userId"]
        user <- map["User"]
        user <- map["UserId"]
        secretWord <- map["password"]
        secretWord <- map["Password"]
        ip <- map["ip"]
        ip <- map["Ip"]
        versionAPP <- map["VersionAPP"]
    }

}
