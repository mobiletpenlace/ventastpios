//
//  TokenClaim.swift
//  TotalPlay360IOS
//
//  Created by Juan Reynaldo Escobar Miron on 7/26/19.
//  Copyright © 2019 Developers Mobile. All rights reserved.
//
import Foundation
import SwiftJWT

struct TokenClaim: Claims {

    public init(iss: String, aud: String, exp: Date, iat: Date, sub: String, prn: String) {
        self.iss = iss
        self.aud = aud
        self.exp = exp
        self.iat = iat
        self.sub = sub
        self.prn = prn
    }

    public var iss: String
    public var aud: String
    public var exp: Date
    public var iat: Date
    public var sub: String
    public var prn: String
}
