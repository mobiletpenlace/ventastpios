import Foundation
struct TokenResponseOAUTH: Codable {

    let access_token: String?
    let token_type: String?
    let expires_in: String?
    let refresh_token: String?
    let refresh_token_expires_in: String?
    let error_code: String?
    let error_message: String?

    enum CodingKeys: String, CodingKey {

        case access_token
        case token_type
        case expires_in
        case refresh_token
        case refresh_token_expires_in
        case error_code
        case error_message
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        access_token = try values.decodeIfPresent(String.self, forKey: .access_token)
        token_type = try values.decodeIfPresent(String.self, forKey: .token_type)
        expires_in = try values.decodeIfPresent(String.self, forKey: .expires_in)
        refresh_token = try values.decodeIfPresent(String.self, forKey: .refresh_token)
        refresh_token_expires_in = try values.decodeIfPresent(String.self, forKey: .refresh_token_expires_in)
        error_code = try values.decodeIfPresent(String.self, forKey: .error_code)
        error_message =  try values.decodeIfPresent(String.self, forKey: .error_message)
    }

}
