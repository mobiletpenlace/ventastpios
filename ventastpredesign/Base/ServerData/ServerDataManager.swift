//
//  RetrofitManager.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 04/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class ServerDataManager<Res: BaseResponse>: BaseServerDataManager<Res> {

    override func getDebugEnabled() -> Bool {
        return false
    }

    override func getJsonDebug(requestUrl: String) -> String {
        var json = ""
        return json
    }

}
