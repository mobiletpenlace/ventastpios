//
//  ObtainToken.swift
//  StreetTpIOS
//
//  Created by Juan Reynaldo Escobar Miron on 10/4/19.
//  Copyright © 2019 Juan Reynaldo Escobar Miron. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import SwiftJWT
import Foundation

class ObtainToken {

    let jsonEncoder = JSONEncoder()
    let jsonDecoder = JSONDecoder()

    var manager: Alamofire.SessionManager = {
        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            "https://189.203.181.233": .disableEvaluation,
            "189.203.181.233": .disableEvaluation,

            "https://cs3.salesforce.com": .disableEvaluation,
            "cs3.salesforce.com": .disableEvaluation

        ]
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        let manager = Alamofire.SessionManager(
            configuration: URLSessionConfiguration.default,
            serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
        )
        return manager
    }()

    func objToJSON<A: Codable>(model: A) -> String {
        let jsonData = try? jsonEncoder.encode(model)
        let jsonString = String(data: jsonData!, encoding: .utf8)
        return jsonString ?? ""
    }

    func postToken(model: String, url path: String, closure: @escaping (Data?) -> Void) {
        postToken(model: model, url: path, headers: ApiDefinition.headersGetToken, closure: closure)
       }

    private func postToken(model: String, url path: String, headers: [String: String], closure: @escaping (Data?) -> Void) {
        if ConnectionUtils.isConnectedToNetwork() {
            Logger.blockSeparator()
            Logger.println(path)
            var request = createRequest(url: path, headers: headers)
            request.httpBody = model.data(using: .utf8)
            requestManager(request, closure: closure)
        } else {
            closure(nil)
            Logger.println("no hay conexion a internet.")
        }
    }

    private func requestManager(_ request: URLRequest, closure: @escaping (Data?) -> Void) {
        manager.request(request).responseJSON {response in
            switch response.result {
            case .success:
                Logger.blockSeparator()
                Logger.println("response status:  \(String(describing: response.response?.statusCode))")
                closure(response.data)
            case .failure(let error):
                Logger.blockSeparator()
                Logger.println("error: \(error)")
                closure(nil)
            }
            if let result = response.result.value {
                if let JSON = result as? NSDictionary {
                    Logger.blockSeparator()
                    Logger.println("response: \(JSON)")
                }
            }
        }
    }

    private func createRequest(url path: String, headers: [String: String]) -> URLRequest {
        var request = URLRequest(url: URL(string: path)!)
        request.httpMethod = HTTPMethod.post.rawValue
        for header in headers {
            request.setValue(header.value, forHTTPHeaderField: header.key)
        }
        return request
    }
}
