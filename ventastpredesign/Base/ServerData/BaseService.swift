//
//  BaseService.swift
//  StreetTpIOS
//
//  Created by Juan Reynaldo Escobar Miron on 10/9/19.
//  Copyright © 2019 Juan Reynaldo Escobar Miron. All rights reserved.
//

import Foundation

class BaseService: NSObject, AlamofireResponseDelegate {

    func onRequestWs() {
    }

    func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {

    }

    func onSuccessLoadResponse(requestUrl: String, response: [BaseResponse]) {
    }

    func onErrorLoadResponse(requestUrl: String, messageError: String) {

    }

    func onErrorConnection() {

    }

}
