//
//  RestFaultElementBean.swift
//  EstrategiaDigital
//
//  Created by Jorge Hdez VIlla on 24/08/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import ObjectMapper

open  class RestFaultElementBean: NSObject, Mappable {

    open  var summary: String = ""
    open  var code: String = ""
    open  var detail: String = ""

    public  required convenience init?(map: Map) {
        self.init()
    }

    open  func mapping(map: Map) {
        summary		<- map["summary"]
        code		<- map["code"]
        detail		<- map["detail"]
    }

}
