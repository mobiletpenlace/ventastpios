//
//  ApiDefinition.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 04/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class ApiDefinition: NSObject {
    static let username = "admin"
    static let secretWord = "1234"
    static let credentialData = "\(username):\(secretWord)".data(using: String.Encoding.utf8)!
    static let base64Credentials = credentialData.base64EncodedString(options: [])
    static let headers = ["Authorization": base64Credentials]
    static let CUSTOM_HEADERS = [
        "Content-Type": "application/json",
        "Authorization": "Basic ZmZtYXBwOjRnZW5kNG1pM250bw==",
        "Accept": "application/json"
    ]
    static let headersINE = [
        "Content-Type": "application/json",
        "Authorization": "Basic a2FyYWx1bmRpOl9XOGN2MzMyLlJzcQ==",
        "Accept": "application/json"
    ]
    static var headersToken = [
        "Accept": "application/json; charset=UTF-8",
        "Content-Type": "application/json; charset=UTF-8",
        "Authorization": "Bearer " + statics.TOKEN
    ]
    static var headersCloudToken = [
        "Content-Type": "application/json",
        "Accept": "application/json",
        "version": "3.0.0.24"
    ]
    static var headersCloudEvent = [
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Authorization": "Bearer " + statics.CLOUD_TOKEN
    ]
    static var headersOkta = [
        "Content-Type": "application/json",
        "Authorization": "Bearer ",
        "version": "3.0.0.26"
    ]
    static let headersMiddle = [
        "Accept": "application/json; charset=UTF-8",
        "Content-Type": "application/json; charset=UTF-8",
        "Authorization": "Basic ZmZtYXBwOjRnZW5kNG1pM250bw=="
    ]
    static let headersFfmapp = [
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": "Basic ZmZtYXBwOjRnZW5kNG1pM250bw== "
    ]
    static let headersGetToken = [
        "Content-Type": "application/x-www-form-urlencoded"
    ]
    static let headersModeloToken = [
        "Content-Type": "application/json",
        "Accept": "application/json",
        "version": "3.0.0.24"
    ]
    static var oAuthSystemToken = [
        "Version": "1.0.0.0",
        "Authorization": "Basic " + statics.TOKEN_SYSTEM_AUTHORIZATION_PROD.toBase64()!
    ]
    static var headersSystemToken = [
        "Content-Type": "application/json",
        "Version": "3.0.0.38",
        "Authorization": "Bearer "
    ]
    static var headersCariaiToken = [
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Version": "3.0.0.38",
        "Authorization": "cVhlaTdqekZaZkkyL1VZRDc5VjFiUkI0dmc0NXRnZ2tmenl1d3pPWk1FeldPc0ZVWER0L2NiNGx5NitFOUdjemh2cWZ2M1pPWmErOXFET3M="
    ]
    static var headersSendPDF = [
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Version": "3.0.0.38",
        "CariSec": statics.TOKEN_CARIAI
    ]
    static var headersFlyers = [
        "Content-Type": "application/Json",
        "Version": "3.0.0.38",
        "Authorization": "Bearer " + statics.TOKEN_SYSTEMS_TP
    ]
    static var headersModeloMessage = [
        "Content-Type": "application/json",
        "Authorization": "Bearer ",
        "version": "3.0.0.24"
    ]
    static let bodyModeloToken = [
        "client_secret": "SUe9O0LuUvarwqi7mj3p7EDe",
        "account_id": "7295536",
        "client_id": "hhle7x22kj4upzblgnr1zkdu",
        "grant_type": "client_credentials"
    ]
    static let headersGetQR = [
        "Accept": "image/png",
        "Content-Type": "application/json",
        "Authorization": "Bearer " + statics.TOKEN_SYSTEMS_TP
    ]
    static var headerLlave = [
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": "Bearer GqiruqoGktwjx1ve2FT2mDmLerFv"
    ]
    static var headersTOKENService = [
        "Authorization": "Basic " + statics.TOKEN_SISTEM_AUTHORIZATION
    ]
    static var headersKeyApiService = [
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Authorization": "Bearer " +  statics.TOKEN_SERVICE_API
    ]
    static var headersApiService = [
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Authorization": "Bearer " +  statics.TOKEN_SERVICE_API,
        "id-Acceso": statics.ID_ACCESO
    ]
    // DESARROLLO Ó PRODUCCION
    static var PRODUCTION = false
    // MAPS
    static let GEO_CODING_BASE_URL = "https://maps.googleapis.com/maps/api/geocode/json?"
    static let GEO_CODING_API_KEY = "AIzaSyBvPtCfXHLfAtgHmfdNJBOiRLMVnkQdAOQ"
    // API
    static let API = "http://appstotalplay.com"
    // static let API = "http://totalvideos.dflabs.io"
    static let API_TOTAL_QA = "https://mss.totalplay.com.mx"
    static let API_TOTAL_TEST="https://msstest.totalplay.com.mx"
    static let API_INE_NUBARIUM = "https://ine.nubarium.com"
    // Pruebas Nuevos servicios
    static let API_PLAN_SUGERIDO = "apexrest/WS_InfoPerfilCliente"
    static let API_PLAN_RECOMENDACION = "apexrest/WS_RecomendacionesEstrategia"
    static let API_PLAN_PAQUETE = "apexrest/WS_ConsultaDetallePaquete"
    // https://www.appstotalplay.com/api/app-version/?app_code=VentasIOS
    // static let API_VIDEOS = API + "/api/videos/"
    static let API_FLAGS = "https://cmsapp.totalplay.dev/sales-Cloud-ios-flags"
    static let API_VIDEOS = "https://cmsapp.totalplay.dev/videos"
    static let API_VERSION = API + "/api/app-version/"
    static let api = "https://apiservice.sistemastp.com.mx/v1/iptv/getFlyers"
    static let API_CP = "/ventasmovil/ConsultaCP"
    static let API_LOGIN = "/ventasmovil/LoginUserAPPJson"
    static let API_LOGIN_FFM = "/ventasmovil/LoginUsuarioVentasAPP"
    static let API_QUERY_EMPLOYEES_BY_TICKET = "/ventasmovil/ConsultaEmpleadosJson"
    static let API_QUERY_UPLOAD_DOCUMENT = "/ventasmovil/AgregarDocumento"
    static let API_ACD = "/ventasmovil/EnviaACD"
    static let API_SALES_POINT = "/ventasmovil/ConsultaPuntosVentaJson"
    static let API_COVERAGE = "/ventasmovil/Cobertura"
    static let API_FAMILY_PACKAGESVM = "/ventasmovil/FamilyPackagesVM"
    static let API_CONSULT_STATUS_REQUEST = "/ventasmovil/ConsultaStatusSolicitudJson"
    static let API_DETAIL_REQUEST = "/ventasmovil/DetalleSolicitudesJson"
    static let API_DOWNLOAD_OPPORTUNITY = "/ventasmovil/ConsultaOportunidadSF"
    static let API_CONSULT_PLAN_SALES = "/ventasmovil/ConsultaPlanVenta"
    static let API_ADD_OPPORTUNITY = "/ventasmovil/AgregarOportunidad"
    static let API_SEND_ACD = "/ventasmovil/EnviaACD"
    // Appis SALESFORCE
    static let API_TOKEN_SALESFORCE = "oauth2/token"
    static let API_EMPLEADO_PAIS = "apexrest/WS_ConsultaEmpleadoPais"
    static let API_INICIO_EMPLEADO = "apexrest/WS_LoginInicioSesionEmpleado"
    static let API_PLANES_TP = "apexrest/WSPlanesTP"
    static let API_ADDONS_VENTA = "apexrest/WS_ConsultaAddonsVenta"
    static let API_CONVIVENCIA_PROMOCIONES = "apexrest/WS_ValidaConvivenciaPromociones"
    static let API_CREA_VENTA = "apexrest/WS_CreacionVenta_TP"
    static let API_CONSULTA_VENTA = "apexrest/WS_ConsultaModificar_VentaTP"
    static let API_CONSULTA_OPORTUNIDADES = "apexrest/WS_ConsultaOportunidadesEmpleado"
    static let API_CONSULTA_OS = "apexrest/WS_ConsultaOportunidadOS"
    static let API_CONSULTA_CLUSTERS = "apexrest/WS_ConsultaCatalogos"
    static let API_ENVIA_MARKETING = "apexrest/WS_EnvioPorMarketingCloud"
    static let API_INFO_EMPLEADO = "apexrest/WS_InfoEmpleado"
    // LEAD SALESFORCE
    static let API_CREATE_LEAD = "apexrest/WS_CreacionLEADRest"
    static let API_STATUS_LEAD = "apexrest/WS_VentasRecomendados_Detalles_Rest"
    static let API_COMMENTS_LEAD = "apexrest/WS_ConsultaComentariosObject"
    static let API_CREATE_COMMENTS_LEAD = "apexrest/WS_ComentariosChatterRest"
    static let API_DEBTOR = "/SalesForce/PrevencionCobranza"
    // SETTINGS PACKAGE
    static let API_QUERY_TYPE_PLAIN = "/ventasmovil/TipoPlanJson"
    static let API_QUERY_PLANES = "/ventasmovil/PlanesJson"
    static let API_QUERY_SERVICES_PRODUCT_DETAIL = "/ventasmovil/ServicesProductsJson"
    // COMISIONES EMBAJADOR - MINISTRO
    static let API_DETAIL_ACCOUNT_EMBAJADOR = "/Comisiones/getEmbajador"
    static let API_DETAIL_MINISTRO = "/Comisiones/getMinistro"
    // STREET TP
    static let API_LOGIN_STREETTP =  "/StreetTP/LoginSt"
    static let API_EVENT_LIST = "/StreetTP/SearchEvent"
    static let API_EVENT_DETAIL = "/StreetTP/SearchEventDetail"
    static let API_ASSIGNED_HOSTESS = "/StreetTP/AssignmentHostess"
    static let API_HOSTESS_SUPERVISE =  "/StreetTP/SearchHostessBoss"
    static let API_TREE_CATEGORY = "/StreetTP/getTreeCategory"
    static let API_DOWNLOAD_PHOTO =  "/StreetTP/getURLImages"
    static let API_UPLOAD_PHOTO =  "/StreetTP/AddURLImageBD"
    static let API_EVENT_EVALUATION =  "/StreetTP/EventEvaluation"
    static let API_EMPLOYEE_EVALUATION =  "/StreetTP/EmployeeEvaluation"
    static let API_QUERY_EVALUATION =  "/StreetTP/RestQueryEvaluation"
    static let API_REASON_MISSING =  "/StreetTP/RestReasonMissing"
    static let API_CHECK_OUT =  "/StreetTP/RestQueryCheckOUT"
    static let API_UPDATE_CHECK_OUT =  "/StreetTP/RestUpdateCheckOUT"
    static let API_CREATE_EVENT =  "/StreetTP/createEvent"
    static let API_EDIT_EVENT =  "/StreetTP/editEvent"
    static let API_DELETE_HOSTESS =  "/StreetTP/deleteHostess"
    static let API_DELETE_EVENT =  "/StreetTP/deleteEvent"
    // FFMTpe
    static let API_SALES_AVAILABILITY = "/FFMTpe/VentasDisponibilidad"
    static let API_SALES_RESCHEDULE = "/FFMTpe/VentasReagendamientoOT"
    // AVAILABILITY
    static let availability = "/FFM/ConsultaDisponibilidadAPP"
    // INR
    static let ine = "/ocr/obtener_datos"
    // CUPON
    static let API_PROMO_CUPON = "/ventasmovil/rstConsultaPromoCode"
    /// FAMILY PACKAGE IMAGES
    static let API_FAMILY_PACKAGES = "/Estrategia/FamilyPackages"
    // COMISSIONS
    static let API_COMISSIONS_SALE = "/ComisionesBack/GetComisionesByVendedor"
    static let API_FACT_NO_PAYMENT = "/ComisionesBack/GetFacturasNoPagadas"
    static let API_FACT_RANKING = "/ComisionesBack/GetRankingByVendedor"
    static let API_GET_PROD_MIN = "/ComisionesBack/GetProdMin"
    static let API_GET_TICKETS = "/Aclaraciones/Historial"
    // Comissions Details
    static let API_DETAIL_FACT =  "/ComisionesBack/GetDetalleFactura"
    static let API_DETAIL_FACT_PRODMIN =  "/ComisionesBack/GetProdMinDet"
    static let API_DETAIL_FACT2 = "/ComisionesBack/GetDetailFact2"
    // TICKETS
    static let API_CREATE_TICKET = "/Aclaraciones/Crear"
    // Consulta Paquetes y promociones ESTOS YA NO SE OCUPAN
    static let dobleplay = API + "/planes/dobleplay/"
    static let dobleplayTV = API + "/planes/dobleplaytv/"
    static let tripleplay = API + "/planes/tripleplay/"
    static let API_PROMOTIONS = API + "/planes/promociones/"
    static let API_RELEASES = API + "/planes/lanzamientos/"
    static let API_DIFERENTS = API + "/planes/quenoshacediferentes/"
    static let businessmen = API + "/planes/empresarios/"
    // Redes sociales
    static let API_FACEBOOK = "https://www.facebook.com/"
    static let API_TWITTER = "https://www.twitter.com/"
    static let API_LINKED = "https://www.linkedin.com/"
    // Grupo modelo
    static let API_MODELO_COVERAGE = "/GrupoModelo/checkCoverage"
    static let API_MODELO_PRODUCTS = "/GrupoModelo/getProducts"
    static let API_MODELO_TOKEN = "https://mc16y3xk-dkjxnm8kw0nwjxl9xb4.auth.marketingcloudapis.com/v2/token"
    static let API_MODELO_MESSAGE = "https://mc16y3xk-dkjxnm8kw0nwjxl9xb4.rest.marketingcloudapis.com/interaction/v1/events"
    // Verificar NIP del telefono
    static let API_NIP = "/ventasmovil/rstGeneraNIP"
    // Bandeja venta digital
    static let APISERVICE_PROD = "https://apiservice.sistemastp.com.mx"
    static let API_LLAVE_PATERNIDAD = APISERVICE_PROD + "/v1/pci/aplicacion/llaves"
    static let API_PATERNIDAD = APISERVICE_PROD + "/v2/digitalizacion/getPaternidades"
    // Domicilio Deudor
    // Okta
    static let API_OKTA = "https://gruposalinas-oie.oktapreview.com/oauth2/v1/userinfo"
    // Existing Accounts
    static let API_SEARCH = "apexrest/WS_InformacionCuenta_Nombre"
    // Reportes
    static let API_TOKEN_REPORTES = "http://10.216.48.99:9010/ventas/authTP"
    static let API_REPORTES_VENTAS = "http://10.216.48.99:9010/ventas/ConsultaVentas/"
    static let API_REPORTES_INSTALADAS = "http://10.216.48.99:9010/ventas/ConsultaReportes/"
    static let API_REPORTS = "apexrest/WS_ConsultaVentasCoach"
    //    Flyers
    static let API_SYSTEMASTP_TOKEN = "/oauth/client_credential/accesstoken?grant_type=client_credentials"
    static let API_SYSTEMS_GET_ALL_STATES = "/v1/digitalizacion/v1/Get_All_States"
    static let API_SYSTEMS_GET_OFFERS = "/v1/iptv/getFlyers"
    static let API_SYSTEMS_DOWNLOAD_FLYER = "/v1/iptv/Flyer?name="
    static let API_SYSTEMS_DOWNLOAD_QR = "/v1/iptv/QrCode?name="
    static let API_COMPLETION = "/v1/verify_identity/verification-data/"
    static let API_PLAZAS = "https://cmsapp.totalplay.dev/plazas"
    static let API_CONSULTA_CREACION_EMPLEADO = "apexrest/WS_CreacionUsuarioEmpleadoTP"
    static let API_GETDISTRO = "https://cmsapp.totalplay.dev/distritos"
    // Bandeja de venta digital
    static let API_SERVICE_PROD = "https://apiservice.sistemastp.com.mx"
    static let API_SERVICE_QA = "https://apiservice-qa.sistemastp.com.mx"
    static let API_SERVICE_DEV = "https://apiservice-dev.sistemastp.com.mx"
    static let API_SERVICE_CURRENT = API_SERVICE_PROD
    static let API_TOKEN_SERVICE =  API_SERVICE_CURRENT + "/oauth/client_credential/accesstoken?grant_type=client_credentials"
    static let API_KEY_PATERNITY =  API_SERVICE_CURRENT + "/v1/pci/aplicacion/llaves"
    static let API_PATERNITY =  API_SERVICE_CURRENT + "/v2/digitalizacion/getPaternidades"
    // MARK: leads params
    static let LEADS_CANAL = "call center"
    static let LEADS_SUBCANAL = "ejecutivos ekt"
}
