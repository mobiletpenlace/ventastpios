//
//  TokenService.swift
//  TotalPlay360IOS
//
//  Created by Juan Reynaldo Escobar Miron on 14/01/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import SwiftJWT
import CryptoSwift

class TokenServiceOAUTH {
    let obtainToken: ObtainToken = ObtainToken()
    let URL = "https://mssoqa.totalplay.com.mx/oauth/logintoken"
    static var tokenJWT = ""

//    var timer = Timer()
//    var counter: Int = 0
    // https://na34.salesforce.com/services/oauth2/token"

    init() {
        // serverManager = ServerDataManager()
    }

    /*func getTokenS(_ callBack:@escaping ((Bool)) -> Void) {
        TPCipher.shared.setKey(b64Key: "VG90YWxwbGF5TW9iaWxlU2VjdXJpdHlMYXllcjIwMTk=")
        guard let subEncrypt = TPCipher.shared.code(plainText: "15006306") else {
            return
        }
        guard let prnEncrypt = TPCipher.shared.code(plainText: "GrupoSalinas2019") else {
            return
        }
        Logger.println(subEncrypt)
        Logger.println(prnEncrypt)
        

        let header = Header()
        let claims = TokenClaim(iss: "8ac11dfc-3203-477a-b815-368cbddbef6f", aud: "http://apigateway/api/oauth/token", exp: Date(timeIntervalSinceNow: 60), iat: Date(timeIntervalSinceNow: 0), sub: subEncrypt, prn: prnEncrypt)
        let resultexp = String("\(claims.exp.timeIntervalSince1970)").split(separator: ".")
        let resultiat = String("\(claims.iat.timeIntervalSince1970)").split(separator: ".")
        statics.dataiat = String(resultiat[0])
        let claimsString = String("{\"iss\": \"\(claims.iss)\", \"aud\": \"\(claims.aud)\", \"exp\": \"\(resultexp[0])\", \"iat\": \"\(resultiat[0])\", \"sub\": \"\(claims.sub)\", \"prn\": \"\(claims.prn)\"} ")
        var myJWT = JWT(header: header, claims: (claimsString.data(using: .utf8)?.base64EncodedString())!.replacingOccurrences(of: "+", with: "-")
            .replacingOccurrences(of: "/", with: "_")
            .replacingOccurrences(of: "=", with: ""))

        let privateKeyPath = Bundle.main.url(forResource: "TotalPlay360", withExtension: "pem")
        guard let privateKey = try? Data(contentsOf: privateKeyPath! , options: .alwaysMapped) else {
            callBack((false))
            return
        }

        let jwtSigner = JWTSigner.rs256(privateKey: privateKey)
        guard let signedJWT = try? myJWT.sign(using: jwtSigner) else {
            callBack((false))
            return }

        statics.TOKEN = signedJWT
        
        callBack((true))
    }*/

    func getToken(_ callBack: @escaping ((Bool, String)) -> Void) {
        var token: String = ""
        var res: TokenResponse!
        if statics.TOKEN != "" {
            var request = String("grant_type=urn:ietf:params:oauth:grant-type:jwt-bearer&assertion=\(statics.TOKEN)")
            Logger.println(request)
            let delayTime = DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                self.obtainToken.postToken(model: request, url: self.URL ) { data in
                    guard let data = data else {
                        callBack((false, ""))
                        return
                    }
                    do {
                        let decoder = JSONDecoder()
                        res = try decoder.decode(TokenResponse.self, from: data)
                        token = res.access_token ?? ""
//                        statics.refreshToken = res.refresh_token ?? ""
//                        statics.expiresIn = res.expires_in ?? ""
                        callBack((true, token))

                    } catch let err {
                        print(err)
                        callBack((false, ""))

                    }
                }

            }
        }
    }
}

extension String {

    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self, options: Data.Base64DecodingOptions(rawValue: 0)) else {
            return nil
        }

        return String(data: data as Data, encoding: String.Encoding.utf8)
    }

    func toBase64() -> String? {
        guard let data = self.data(using: String.Encoding.utf8) else {
            return nil
        }

        return data.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))
    }
}
