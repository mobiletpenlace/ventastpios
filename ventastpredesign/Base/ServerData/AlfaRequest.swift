//
//  AlfaRequest.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 08/04/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

open  class AlfaRequest: NSObject, Mappable {

    override public init() {
    }

   public required init?(map: Map) {

    }

    open func mapping(map: Map) {

    }

}
