//
//  BasicRequest.swift
//  EstrategiaDigital
//
//  Created by Jorge Hdez Villa on 09/08/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import ObjectMapper

open  class BaseRequest: AlfaRequest {

    open var login: LoginRequestBase?

    override public init() {
        super.init()
        login = LoginRequestBase()
        login?.user = Strings.LOGIN_REQUEST_USER
        login?.secretWord = Strings.LOGIN_REQUEST_SECRETWORD
        login?.ip = Strings.LOGIN_REQUEST_IP
    }

    public init(user: String, secretWord: String) {
        super.init()
        login = LoginRequestBase()
        login?.user = user
        login?.secretWord = secretWord
        login?.ip = "Ip11"
    }

    public init(user: String, secretWord: String, ip: String) {
        super.init()
        login = LoginRequestBase()
        login?.user = user
        login?.secretWord = secretWord
        login?.ip = ip
    }

    public required init?(map: Map) {
        super.init()

    }

    override open func mapping(map: Map) {
        login <- map["Login"]
    }

}
