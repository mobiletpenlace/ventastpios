//
//  ImagesTake.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta O. on 16/12/19.
//  Copyright © 2019 Juan Reynaldo Escobar Miron. All rights reserved.
//

import Foundation
import UIKit
class ImagesTake {

    class func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }

   class func downloadImage(from url: URL) -> UIImage {
    var imageResult = UIImage(named: "image_picture_cam.png")
        print("Download Started")
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("Download Finished")
            DispatchQueue.main.async {
                imageResult =  UIImage(data: data)
            }
        }
    return imageResult!
    }
}
