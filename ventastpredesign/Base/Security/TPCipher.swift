//
//  TPCipher.swift
//  StreetTpIOS
//
//  Created by Alejandro Acosta on 9/2/19.
//  Copyright © 2019 Alejandro Acosta. All rights reserved.
//

/*
 How to use:
 
 TPCipher.shared.setKey(b64Key: "VG90YWxwbGF5TW9iaWxlU2VjdXJpdHlMYXllcjIwMTk=")
 
 guard let cipherText = TPCipher.shared.code(plainText: "15006303") else {
 return
 }
 print(cipherText)
 
 guard let decryptedText = TPCipher.shared.decode(coded: cipherText) else {
 return
 }
 print(decryptedText)
 */

import Foundation
import CryptoSwift

class TPCipher {

    public static let shared = TPCipher()

    private let loggerMessage: String = "TPCipher - Error "

    private var keyString: String? {
        didSet {
            initialize()
        }
    }
    private var ivArray: [UInt8] = [8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9]
    private var aes: AES?

    fileprivate func initialize() {
        guard let key = keyString else {
            print("\(loggerMessage) Warning: There is no key.")
            return
        }

        let keyBytes: [UInt8] = Array(base64: key)
        let blockMode = CBC(iv: ivArray)
        do {
            aes = try AES(key: keyBytes, blockMode: blockMode, padding: .pkcs5)

        } catch let error {
            print(error)
        }
    }

    func setKey(b64Key: String) {
        self.keyString = b64Key
    }

    func setKey(key: String) {
        let b64Key = key.data(using: .utf8)?.base64EncodedString()
        setKey(b64Key: b64Key!)
    }

    func code(plainText text: String) -> String? {
        guard let cipher = aes else {
            print("\(loggerMessage) Warning: There is no cipher instance")
            return nil
        }

        do {
            let codedText = try cipher.encrypt(Array(text.utf8))
            return codedText.toBase64()
        } catch let error {
            print("\(loggerMessage) coding: \(error.localizedDescription)")
            return nil
        }
    }

    func decode(coded: String) -> String? {
        guard let cipher = aes else {
            print("\(loggerMessage) Warning: There is no cipher instance")
            return nil
        }

        do {
            let decodedText = try cipher.decrypt(Array(base64: coded))
            return String(bytes: decodedText, encoding: .utf8)
        } catch let error {
            print("\(loggerMessage) decoding: \(error.localizedDescription)")
            return nil
        }
    }
}
