//
//  AES128.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 22/04/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//
//
/*
 
AES128.shared.setKey(key: Strings.AES128_KEY)
print( AES128.shared.code(plainText: "CIUDAD DE MEXICO") )
print( AES128.shared.decode(coded: "6R2blUddm5zoMlfjPvfSWQGCFnRwB0QtR7GJmf5DyKg=") )
 
 */
import Foundation
import CryptoSwift

class AES128 {

    public static let shared = AES128()

    private let loggerMessage: String = "TPCipher - Error "

    private var keyString: String? {
        didSet {
            initialize()
        }
    }

    private var ivArray = "380db1011o611fa9"
    private var aes: AES?

    fileprivate func initialize() {
        guard let key = keyString else {
            print("\(loggerMessage) Warning: There is no key.")
            return
        }

        let keyBytes: [UInt8] = Array(base64: key)

        let blockMode = CBC(iv: [UInt8](ivArray.utf8))
        do {
            aes = try AES(key: keyBytes, blockMode: blockMode, padding: .pkcs5)

        } catch let error {
            print(error)
        }
    }

    func setKey(b64Key: String) {
        self.keyString = b64Key
    }

    func setKey(key: String) {
        let b64Key = key.data(using: .utf8)?.base64EncodedString()
        setKey(b64Key: b64Key!)
    }

    func code(plainText text: String) -> String? {
        guard let cipher = aes else {
            print("\(loggerMessage) Warning: There is no cipher instance")
            return nil
        }

        do {
            let codedText = try cipher.encrypt(Array(text.utf8))
            return codedText.toBase64()
        } catch let error {
            print("\(loggerMessage) coding: \(error.localizedDescription)")
            return nil
        }
    }

    func decode(coded: String) -> String? {
        guard let cipher = aes else {
            print("\(loggerMessage) Warning: There is no cipher instance")
            return nil
        }

        do {
            let decodedText = try cipher.decrypt(Array(base64: coded))
            return String(bytes: decodedText, encoding: .utf8)
        } catch let error {
            print("\(loggerMessage) decoding: \(error.localizedDescription)")
            return nil
        }
    }
}
