//
//  AES256.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera  on 18/07/24.
//  Copyright © 2024 TotalPlay. All rights reserved.
//

import Foundation
import CryptoSwift

class AES256 {

   public static let shared = AES256()
   private let loggerMessage: String = "TPCipher - Error "
   private var keyString: String? {
       didSet {
           initialize()
       }
   }

   private var ivArray = "380db1011o611fa9"
   private var aes: AES?

   fileprivate func initialize() {
       guard let key = keyString else {
           print("\(loggerMessage) Warning: There is no key.")
           return
       }

       let keyBytes: [UInt8] = Array(base64: key)

       let blockMode = CBC(iv: [UInt8](ivArray.utf8))
       do {
           aes = try AES(key: keyBytes, blockMode: blockMode, padding: .pkcs5)

       } catch let error {
           print(error)
       }
   }

   func setKey(b64Key: String) {
       self.keyString = b64Key
   }

   func setKey(key: String) {
       let b64Key = key.data(using: .utf8)?.base64EncodedString()
       setKey(b64Key: b64Key!)
   }

   func code(plainText text: String) -> String? {
       guard let cipher = aes else {
           print("\(loggerMessage) Warning: There is no cipher instance")
           return nil
       }

       do {
           let codedText = try cipher.encrypt(Array(text.utf8))
           return codedText.toBase64()
       } catch let error {
           print("\(loggerMessage) coding: \(error.localizedDescription)")
           return nil
       }
   }

   func decode(coded: String) -> String? {
       guard let cipher = aes else {
           print("\(loggerMessage) Warning: There is no cipher instance")
           return nil
       }

       do {
           let decodedText = try cipher.decrypt(Array(base64: coded))
           return String(bytes: decodedText, encoding: .utf8)
       } catch let error {
           print("\(loggerMessage) decoding: \(error.localizedDescription)")
           return nil
       }
   }
}
