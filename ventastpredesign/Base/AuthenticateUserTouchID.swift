//
//  AuthenticateUserTouchID.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta Ortega on 10/4/19.
//  Copyright © 2019 Juan Reynaldo Escobar Miron. All rights reserved.
//

import Foundation
import LocalAuthentication
import UIKit

class AuthenticateUserTouchID {

    class func touchFaceID(vc: UIViewController, completion: ((UIViewController) -> Void)? = nil) {
        let context: LAContext = LAContext()
          if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil) {
              var localizedReason = "Por favor verifíquese"

              if #available(iOS 11.0, *) {
                  switch context.biometryType {

                  case .faceID: localizedReason = "Desbloquear usando Face ID"
                  case .touchID: localizedReason = "Desbloquear usando Touch ID"
                  case .opticID: localizedReason = "Desbloquear usando Optic ID"
                  case .none: print("Sin soporte biométrico")

                  }
              } else {}
              context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: localizedReason, reply: { (wasSuccesful, _) -> Void in
                  if wasSuccesful {
                      DispatchQueue.main.sync {
                         completion?(vc)
                      }
                  }
              })
          }

    }

}
