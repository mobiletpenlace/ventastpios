//
//  TPConnectUtilities.swift
//  SalesCloud
//
//  Created by Axlestevez on 01/02/24.
//  Copyright © 2024 TotalPlay. All rights reserved.
//

import Foundation

public func checkForError(httpUrlResponse: HTTPURLResponse) -> Bool {

    if httpUrlResponse.statusCode >= 400 {
        return true
    }
    return false
}
