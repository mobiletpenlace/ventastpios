//
//  Logger.swift
//  StreetTpIOS
//
//  Created by Juan Reynaldo Escobar Miron on 9/25/19.
//  Copyright © 2019 Juan Reynaldo Escobar Miron. All rights reserved.
//

import Foundation

class Logger {

    static func println(_ mensaje: String) {
        print(" Log: --> "+mensaje)
    }

    static func blockSeparator() {
        println("////////////////////////////////////////////////////////////////////////////")
    }

}
