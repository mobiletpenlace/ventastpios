//
//  Dates.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta O. on 25/10/19.
//  Copyright © 2019 Juan Reynaldo Escobar Miron. All rights reserved.
//

import Foundation

class Dates {

    static let currentDayToolBar: String = {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        formatter.locale = Locale(identifier: "es_ES")
        let current = formatter.string(from: date)
        var currentDay = current.replacingOccurrences(of: ".", with: " ")
        currentDay = currentDay.capitalizingFirstLetter()
        return currentDay
    }()

    static let currentDate: String = {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let currents = dateFormatter.string(from: date)
        return currents
    }()

    class func currentDatePhoto() -> String {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd-HH:mm:ss:SSS"
        let currents = dateFormatter.string(from: date)
        return currents
    }

    class func futureDate(startDate: String, dayF: Int, _ monthF: Int = 0, _ yearF: Int = 0 ) -> String {
        var dateComponent = DateComponents()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        let newDate  = formatter.date(from: startDate)
        dateComponent.day = dayF
        dateComponent.month = monthF
        dateComponent.year = yearF
        let futureDate = Calendar.current.date(byAdding: dateComponent, to: newDate!)
        let current = formatter.string(from: futureDate!)
        return current
    }

    class func dayOfYear(date: Date) -> Int {
        let dayOfYear = Calendar.current.ordinality(of: .day, in: .year, for: date)

        return dayOfYear ?? 0
    }

    class func numberDay(date: String) -> String {
        var day = ""
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        let newDate  = formatter.date(from: date)
        let calendar = Calendar.current
        day = String(format: "%02d", calendar.component(.day, from: newDate!))
        return day

    }

    class func nameDay(date: String, long: Bool) -> String {
        var day = ""
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        formatter.locale = Locale(identifier: "es_ES")
        let newDate  = formatter.date(from: date)
        if long {
             formatter.dateFormat = "EEEE"
        } else {
             formatter.dateFormat = "EE"
        }
        day = formatter.string(from: newDate!).capitalizingFirstLetter()
        return day
    }

}
