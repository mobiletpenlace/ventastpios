//
//  RegistrationError.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta Ortega on 10/10/19.
//  Copyright © 2019 Juan Reynaldo Escobar Miron. All rights reserved.
//

import Foundation

enum RegistrationError: Error {
    case invalidEmail
    case invalidSecretWord
    case invalidPhoneNumber
}
