//
//  ResultStatus.swift
//  ventastpredesign
//
//  Created by Claudia Isamar Delgado Vasquez on 13/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class ResultStatus: NSObject, Mappable {

    required init?(map: Map) {

    }
    override init() {

    }

    func mapping(map: Map) {
        // Enlazar con propiedades de objetos
        Aprobadas <- map["Aprobadas"]
        Pendientes <- map["Pendientes"]
        Rechazada <- map["Rechazada"]
        InstaladasAprobadas <- map["InstaladasAprobadas"]
        InstaladasPendientes <- map["InstaladasPendientes"]
        InstaladasRechazadas <- map["InstaladasRechazadas"]
    }

    var Aprobadas: String?
    var Pendientes: String?
    var Rechazada: String?
    var InstaladasAprobadas: String?
    var InstaladasPendientes: String?
    var InstaladasRechazadas: String?
}
