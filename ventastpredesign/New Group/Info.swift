//
//  Info.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 12/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class Info: NSObject, Mappable {
    required init?(map: Map) {
    }

    func mapping(map: Map) {
        FolioEmpleado <- map["FolioEmpleado"]
    }

    override init() {

    }
    var FolioEmpleado: String?

}
