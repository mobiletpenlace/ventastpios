//
//  Response.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 27/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class Response: NSObject, Mappable {
    required init?(map: Map) {

    }

    func mapping(map: Map) {
        IdResult <- map["IdResult"]
        Result <- map["Result"]
        ResultDescription <- map["ResultDescription"]
        Code <- map["code"]
        Description <- map["description"]
    }

    var IdResult: String?
    var Result: String?
    var ResultDescription: String?
    var Code: String?
    var Description: String?
}
