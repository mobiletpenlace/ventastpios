//
//  Result.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 12/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class Result: NSObject, Mappable {
    required init?(map: Map) {

    }

    func mapping(map: Map) {
        IdResult <- map["IdResult"]
        Result <- map["Result"]
        Description <- map["Description"]
    }

    var IdResult: String?
    var Result: String?
    var Description: String?

}
