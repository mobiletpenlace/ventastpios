//
//  Login.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 12/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import UIKit

import ObjectMapper
import RealmSwift

class Login: Object, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        User <- map["User"]
        SecretWord <- map["Password"]
        Ip <- map["Ip"]
        Pasword <- map["Pasword"]// Se añade "Pasword" por que en el servicio devuelve el campo escrito de este modo.
    }

    override static func primaryKey() -> String? {
        return "uui"
    }
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  User: String = ""
    @objc dynamic var  SecretWord: String = ""
    @objc dynamic var  Ip: String = ""
    @objc dynamic var  Pasword: String = ""

}
