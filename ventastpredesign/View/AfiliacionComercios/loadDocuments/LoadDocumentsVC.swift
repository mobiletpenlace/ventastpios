//
//  LoadDocumentsVC.swift
//  SalesCloud
//
//  Created by aestevezn on 25/04/24.
//  Copyright © 2024 TotalPlay. All rights reserved.
//

import UIKit

class LoadDocumentsVC: BaseVentasView {

    @IBOutlet var actaConstitutivaImage: UIImageView!
    @IBOutlet var comprobanteDeDomicilioImage: UIImageView!
    @IBOutlet var rfcImage: UIImageView!
    @IBOutlet var poderRepresentanteLegalImage: UIImageView!
    @IBOutlet var representanteLegalIDImage: UIImageView!
    @IBOutlet var estadoDeCuentaImage: UIImageView!

    @IBOutlet var actaConstitutivaBUtton: UIButton!
    @IBOutlet var comprobanteDomicilioButton: UIButton!
    @IBOutlet var rfcButton: UIButton!
    @IBOutlet var poderRepresentanteLegalButton: UIButton!
    @IBOutlet var identificacionRepresentanteLegalButton: UIButton!
    @IBOutlet var estadoDeCuentaButton: UIButton!

    let documentManager = DocumentManager.sharedInstance

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationController()
        setTagButton()
    }

    func setupNavigationController () {
        let imageButtonNav = UIImage(named: "Back_Arrow_Black")
        title = "Afiliación de Comercios"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: imageButtonNav, style: .plain, target: self, action: #selector(backActionNav))
    }

    @IBAction func loadDocumentImage(_ sender: UIButton) {
        TPDocumentsShared.shared.documentSelected = TPDocuments(rawValue: sender.tag)
        documentManager.showTypeSourceMedia(view: self.view, delegate: self)
    }

    @objc func backActionNav() {
        navigationController?.popViewController(animated: true)
    }

    func setTagButton() {
        actaConstitutivaBUtton.tag = TPDocumentsValues.acta_constitutiva
        comprobanteDomicilioButton.tag = TPDocumentsValues.comprobante_de_domicilio
        rfcButton.tag = TPDocumentsValues.RFC
        poderRepresentanteLegalButton.tag = TPDocumentsValues.poder_representante_legal
        identificacionRepresentanteLegalButton.tag = TPDocumentsValues.id_representante_legal
        estadoDeCuentaButton.tag = TPDocumentsValues.estado_de_cuenta
    }
}

extension LoadDocumentsVC: TPSourceTypeDocumentProtocol {
    func reacToCompleteFlow(action: Int) {
        documentManager.hideTypeSourceMedia()
        DispatchQueue.main.async {
            if let actionSelected = TPDocumentType(rawValue: action) {
                TPDocumentsShared.shared.documentTypeSelected = actionSelected
                switch actionSelected {
                case .DOCUMENT_TYPE:
                    self.documentManager.openFinder(view: self, pickerDelegate: self)
                    break
                case .IMAGE_TYPE:
                    self.documentManager.showMediaSourceView(delegate: self, viewC: self)
                }
            } else {
                Constants.showErrorGenericMessage(viewCPresent: self)
            }
        }
    }

    func closeView() {
        documentManager.hideTypeSourceMedia()
    }
}

// MARK: MediaDocumentsSourceProtocol implementation
extension LoadDocumentsVC: TPMediaDocumentsSourceProtocol {

    func receiveActaConstitutiva(_ doc: String, _ image: UIImage) {
        guard let optionTypeDocumentSelected = TPDocumentType(rawValue: TPDocumentsShared.shared.documentTypeSelected?.rawValue ?? 0) else {
            return
        }
        switch optionTypeDocumentSelected {
        case .DOCUMENT_TYPE:
            TPAfiliacionComerciosShared.shared.actaConstitutivaString = doc
        case .IMAGE_TYPE:
            TPAfiliacionComerciosShared.shared.actaConstitutivaImage = image
        }
    }

    func receiveComprobanteDomicilio(_ doc: String, _ image: UIImage) {
        guard let optionTypeDocumentSelected = TPDocumentType(rawValue: TPDocumentsShared.shared.documentTypeSelected?.rawValue ?? 0) else {
            return
        }
        switch optionTypeDocumentSelected {
        case .DOCUMENT_TYPE:
            TPAfiliacionComerciosShared.shared.comprobanteDeDomicilioString = doc
        case .IMAGE_TYPE:
            TPAfiliacionComerciosShared.shared.comprobanteDeDomicilioImage = image
        }
    }

    func receiveRFCAfiliacion(_ doc: String, _ image: UIImage) {
        guard let optionTypeDocumentSelected = TPDocumentType(rawValue: TPDocumentsShared.shared.documentTypeSelected?.rawValue ?? 0) else {
            return
        }
        switch optionTypeDocumentSelected {
        case .DOCUMENT_TYPE:
            TPAfiliacionComerciosShared.shared.rfcString = doc
        case .IMAGE_TYPE:
            TPAfiliacionComerciosShared.shared.rfcImage = image
        }
    }

    func receivePoderRepresentante(_ doc: String, _ image: UIImage) {
        guard let optionTypeDocumentSelected = TPDocumentType(rawValue: TPDocumentsShared.shared.documentTypeSelected?.rawValue ?? 0) else {
            return
        }
        switch optionTypeDocumentSelected {
        case .DOCUMENT_TYPE:
            TPAfiliacionComerciosShared.shared.poderRepresentanteLegalString = doc
        case .IMAGE_TYPE:
            TPAfiliacionComerciosShared.shared.poderRepresentanteLegalImage = image
        }
    }

    func receiveIDRepresentanteLegal(_ doc: String, _ image: UIImage) {
        guard let optionTypeDocumentSelected = TPDocumentType(rawValue: TPDocumentsShared.shared.documentTypeSelected?.rawValue ?? 0) else {
            return
        }
        switch optionTypeDocumentSelected {
        case .DOCUMENT_TYPE:
            TPAfiliacionComerciosShared.shared.idRepresentanteLegalString = doc
        case .IMAGE_TYPE:
            TPAfiliacionComerciosShared.shared.idReprentanteLegalImage = image
        }
    }

    func receiveEstadoDeCuenta(_ doc: String, _ image: UIImage) {
        guard let optionTypeDocumentSelected = TPDocumentType(rawValue: TPDocumentsShared.shared.documentTypeSelected?.rawValue ?? 0) else {
            return
        }
        switch optionTypeDocumentSelected {
        case .DOCUMENT_TYPE:
            TPAfiliacionComerciosShared.shared.estadoDeCuentaString = doc
        case .IMAGE_TYPE:
            TPAfiliacionComerciosShared.shared.estadoDeCuentaImage = image
        }
    }
}

// MARK: PickerDelegate implementation
extension LoadDocumentsVC: UIDocumentPickerDelegate {
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        if let selectedDocument = urls.first {
            if let document = try? Data(contentsOf: selectedDocument) {
                let base64String = document.base64EncodedString()
                print("PDF en formato base64: \(base64String)")
                DispatchQueue.main.async {
                    Constants.removeSpinnerView()
                }
            } else {
                Constants.removeSpinnerView()
                Constants.showErrorGenericMessage(viewCPresent: self)
            }
        } else {
            Constants.removeSpinnerView()
            Constants.showErrorGenericMessage(viewCPresent: self)
        }
    }

    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        Constants.removeSpinnerView()
        controller.dismiss(animated: true)
    }
}
