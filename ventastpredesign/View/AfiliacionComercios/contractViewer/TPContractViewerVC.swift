//
//  TPContractViewerVC.swift
//  SalesCloud
//
//  Created by aestevezn on 26/04/24.
//  Copyright © 2024 TotalPlay. All rights reserved.
//

import UIKit
import PDFKit

class ContractViewerVC: BaseVentasView {

    @IBOutlet weak var iconCheckImage: UIImageView!
    @IBOutlet weak var pdfViewViewer: UIView!
    @IBOutlet weak var leyendTextAccepted: UILabel!
    
    private var isCheckAcepted: Bool = false
    private var checkImage = UIImage(named: "Image_rede_blue_check")
    private var unCheckImage = UIImage(named: "icon_radiobutton_off")
    private var pdfDocument: PDFDocument!
    private let finalView = TPAfiliationCompleteVC()
    private var segueGuard: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationController()
    }

    init(pdf: PDFDocument) {
        self.pdfDocument = pdf
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    func createPDFView() {
        let pdfView = PDFView(frame: self.view.bounds)
        pdfView.translatesAutoresizingMaskIntoConstraints = true
        pdfView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        pdfView.autoScales = true
        pdfView.displayMode = .singlePageContinuous
        pdfView.displaysPageBreaks = true
        pdfView.height = self.pdfViewViewer.height - 35
        self.pdfViewViewer.addSubview(pdfView)
        pdfView.document = pdfDocument
    }

    @IBAction func checkAceptedAction(_ sender: Any) {
        if !isCheckAcepted {
            segueGuard = true
            isCheckAcepted = true
            iconCheckImage.image = checkImage
        } else {
            segueGuard = false
            isCheckAcepted = false
            iconCheckImage.image = unCheckImage
        }
    }

    @IBAction func nextViewAction(_ sender: Any) {
        if !isCheckAcepted {
            leyendTextAccepted.textColor = .red
        }
    }

    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "finalProcess" {
            return isCheckAcepted
        }
        return true
    }

    func setupNavigationController () {
        let imageButtonNav = UIImage(named: "Back_Arrow_Black")
        title = "Afiliación de Comercios"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: imageButtonNav, style: .plain, target: self, action: #selector(backActionNav))
    }

    @objc func backActionNav() {
        navigationController?.popViewController(animated: true)
    }
}
