//
//  FormViewControllerExtension.swift
//  SalesCloud
//
//  Created by aestevezn on 16/04/24.
//  Copyright © 2024 TotalPlay. All rights reserved.
//

import UIKit

extension FormViewController {

    func showPickerTypeDeposit() {
        isGiroComercial = false
        isTipoDeposito = true
        let pickerView = PickerViewReusable( frame: CGRect(
            x: 0,
            y: 0,
            width: self.view.bounds.width,
            height: self.view.bounds.height
        ))
        pickerView.observer = self
        pickerView.dataOptions = TypeDeposit.OPTIONS
        self.view.addSubview(pickerView)
    }

    func showPickerGirosComerciales () {
        isGiroComercial = true
        isTipoDeposito = false
        let pickerView = PickerViewReusable( frame: CGRect(
            x: 0,
            y: 0,
            width: self.view.bounds.width,
            height: self.view.bounds.height
        ))
        pickerView.observer = self
        pickerView.dataOptions = GirosComerciales.ARRAY_COMERCIOS
        self.view.addSubview(pickerView)
    }
}
