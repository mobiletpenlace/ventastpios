//
//  TPAfiliacionComerciosShared.swift
//  SalesCloud
//
//  Created by aestevezn on 16/04/24.
//  Copyright © 2024 TotalPlay. All rights reserved.
//

import UIKit

public class TPAfiliacionComerciosShared {

    public static let shared = TPAfiliacionComerciosShared()

    private init() {}

    // MARK: FormData
    var nombreComercial: String = String.emptyString
    var rfc: String = String.emptyString
    var razonSocial: String = String.emptyString
    var responsableAlt: String = String.emptyString
    var emai: String = String.emptyString
    var phoneNumbe: String = String.emptyString
    var cpEntregas: String = String.emptyString
    var direccionEntregas: String = String.emptyString
    var cluste: String = String.emptyString
    var descripció: String = String.emptyString
    var almacenDeSalid: String = String.emptyString
    var grioComercia: String = String.emptyString
    var cuentaBancari: String = String.emptyString
    var claveInterbancari: String = String.emptyString
    var porcentajeDeComicio: String = String.emptyString
    var tipoDeposit: String = String.emptyString
    var horari: [String] = []
    
    // MARK: Load Documents Data images
    var actaConstitutivaImage: UIImage!
    var comprobanteDeDomicilioImage: UIImage!
    var rfcImage: UIImage!
    var poderRepresentanteLegalImage: UIImage!
    var idReprentanteLegalImage: UIImage!
    var estadoDeCuentaImage: UIImage!
    
    // MARK: Load Documents data String
    var actaConstitutivaString: String = String.emptyString
    var comprobanteDeDomicilioString: String = String.emptyString
    var rfcString: String = String.emptyString
    var poderRepresentanteLegalString: String = String.emptyString
    var idRepresentanteLegalString: String = String.emptyString
    var estadoDeCuentaString: String = String.emptyString
    
    // MARK: DirectionViewData
    var cpFiscal: String = String.emptyString
    var direccionFiscal: String = String.emptyString
    
    // MARK: DocumentSelected
    var documentSelected: TPDocuments?
    
}
