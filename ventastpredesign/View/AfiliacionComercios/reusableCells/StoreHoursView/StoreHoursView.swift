//
//  StoreHoursCell.swift
//  SalesCloud
//
//  Created by Axl Estevez on 09/05/24.
//  Copyright © 2024 TotalPlay. All rights reserved.
//

import UIKit

class StoreHoursView: UIView {

    @IBOutlet var contentView: UIView!

    init() {
        super.init(frame: CGRect.zero)
        commonInit()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func commonInit() {
        Bundle.main.loadNibNamed("\(StoreHoursView.self)", owner: self)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
}
