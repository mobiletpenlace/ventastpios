//
//  FormViewControllerDelegate.swift
//  SalesCloud
//
//  Created by aestevezn on 16/04/24.
//  Copyright © 2024 TotalPlay. All rights reserved.
//

import UIKit

extension FormViewController: sendDataSelectedObserver {
    func receiveData(optionSelected: Any) {
        var optionSelectedString = optionSelected as! String
        if isGiroComercial {
            giroComercialTextField.text = optionSelectedString
        } else if isTipoDeposito {
            depositoTypeTextField.text = optionSelectedString
        }
    }
}
