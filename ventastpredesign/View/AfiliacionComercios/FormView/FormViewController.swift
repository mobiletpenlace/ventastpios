//
//  FormViewController.swift
//  SalesCloud
//
//  Created by aestevezn on 15/04/24.
//  Copyright © 2024 TotalPlay. All rights reserved.
//

import UIKit
import DTTextField
import TextFieldEffects

class FormViewController: BaseVentasView {

    @IBOutlet var nombreComercialTextField: DTTextField!
    @IBOutlet var rfcTextField: DTTextField!
    @IBOutlet var razonSocialTextField: DTTextField!
    @IBOutlet var responsableAltaTextField: DTTextField!
    @IBOutlet var correoTextField: DTTextField!
    @IBOutlet var telefonoTextField: DTTextField!
    @IBOutlet var cpEntregasTextField: DTTextField!
    @IBOutlet var direccionEntregasTextField: DTTextField!
    @IBOutlet var clusterTextField: DTTextField!
    @IBOutlet var descripcionRestaurante: DTTextField!
    @IBOutlet var almacenSalidaPedidosTextField: DTTextField!
    @IBOutlet var giroComercialTextField: DTTextField!
    @IBOutlet var cuentaBancariaTextField: DTTextField!
    @IBOutlet var CLABETextField: DTTextField!
    @IBOutlet var porcentajeTextField: DTTextField!
    @IBOutlet var depositoTypeTextField: DTTextField!

    var isGiroComercial: Bool = false
    var isTipoDeposito: Bool = false
    var forceData: [UITextField]!
    var isDataComplete: Bool = false

    let messageCompleteError    = NSLocalizedString("Campo es requerido *", comment: String.empty)

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationController()
        setForceDataFields()
    }

    @IBAction func changeDepositType(_ sender: Any) {
        showPickerTypeDeposit()
    }

    @IBAction func setGiroComercial(_ sender: Any) {
        showPickerGirosComerciales()
    }

    @IBAction func nextViewAction(_ sender: Any) {
        if !isDataComplete {
            updateEmptyTextField()
        }
    }

    func loadDataShared() {
        TPAfiliacionComerciosShared.shared.nombreComercial = nombreComercialTextField.text ?? String.empty
        TPAfiliacionComerciosShared.shared.rfc = rfcTextField.text ?? String.empty
        TPAfiliacionComerciosShared.shared.razonSocial = razonSocialTextField.text ?? String.empty
        TPAfiliacionComerciosShared.shared.cpEntregas = cpEntregasTextField.text ?? String.empty
        TPAfiliacionComerciosShared.shared.direccionEntregas = direccionEntregasTextField.text ?? String.empty
    }

    func updateEmptyTextField() {
        for textFieldData in forceData {
            if textFieldData.text == String.empty {
                let textData = textFieldData as! DTTextField
                textData.showError(message: messageCompleteError)
            }
        }
    }

    func setForceDataFields() {
        self.forceData = [
            nombreComercialTextField,
            rfcTextField,
            responsableAltaTextField,
            telefonoTextField,
            cpEntregasTextField,
            direccionEntregasTextField,
            almacenSalidaPedidosTextField,
            giroComercialTextField,
            cuentaBancariaTextField,
            CLABETextField,
            porcentajeTextField,
            depositoTypeTextField
        ]
    }

}

// MARK: NavigationController setup
extension FormViewController {

    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == AfiliacionSeguesName.DIRECTION_DELIVERY_SEGUE {
            return validateData()
        }
        return true
    }

    func setupNavigationController () {
        let imageButtonNav = UIImage(named: "Back_Arrow_Black")
        title = "Afiliación de Comercios"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: imageButtonNav, style: .plain, target: self, action: #selector(backActionNav))
    }

    @objc func backActionNav() {
        self.dismiss(animated: true)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == AfiliacionSeguesName.DIRECTION_DELIVERY_SEGUE {
            loadDataShared()
        }
    }
}

// MARK: User define methods user
extension FormViewController {
    func validateData() -> Bool {
        guard !nombreComercialTextField.text!.isEmptyStr else {
            return false
        }

        guard !rfcTextField.text!.isEmptyStr else {
            return false
        }

        guard !responsableAltaTextField.text!.isEmptyStr else {
            return false
        }

        guard !telefonoTextField.text!.isEmptyStr else {
            return false
        }

        guard !cpEntregasTextField.text!.isEmptyStr else {
            return false
        }

        guard !direccionEntregasTextField.text!.isEmptyStr else {
            return false
        }

        guard !almacenSalidaPedidosTextField.text!.isEmptyStr else {
            return false
        }

        guard !giroComercialTextField.text!.isEmptyStr else {
            return false
        }

        guard !cuentaBancariaTextField.text!.isEmptyStr else {
            return false
        }

        guard !CLABETextField.text!.isEmptyStr else {
            return false
        }

        guard !porcentajeTextField.text!.isEmptyStr else {
            return false
        }

        guard !depositoTypeTextField.text!.isEmptyStr else {
            return false
        }

        return true
    }
}
