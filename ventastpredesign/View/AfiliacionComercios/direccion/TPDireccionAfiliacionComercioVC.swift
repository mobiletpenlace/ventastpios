//
//  TPDireccionAfiliacionComercioVC.swift
//  SalesCloud
//
//  Created by aestevezn on 16/04/24.
//  Copyright © 2024 TotalPlay. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import TextFieldEffects

class TPDireccionAfiliacionComercioVC: BaseVentasView {

    @IBOutlet weak var viewMap: UIView!
    @IBOutlet var codigoPostalTextField: UITextField!
    @IBOutlet var streetAndNumberTextField: UITextField!
    @IBOutlet var cpFiscal: UITextField!
    @IBOutlet var direccionFiscal: UITextField!

    let locationManager = CLLocationManager()
    var mapView: GMSMapView!

    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        codigoPostalTextField.delegate = self
        codigoPostalTextField.keyboardType = .numberPad
        setupCameraMaps()
        onlyNumberInTextField()
        setupNavigationController()
        getDataFromShared()
    }

}

// MARK: SingletonLogic
extension TPDireccionAfiliacionComercioVC {
    func getDataFromShared() {
        self.codigoPostalTextField.text = TPAfiliacionComerciosShared.shared.cpEntregas
        self.streetAndNumberTextField.text = TPAfiliacionComerciosShared.shared.direccionEntregas
    }
}

// MARK: navigationCotroller setup
extension TPDireccionAfiliacionComercioVC {
    func setupNavigationController () {
        let imageButtonNav = UIImage(named: "Back_Arrow_Black")
        title = "Afiliación de Comercios"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: imageButtonNav, style: .plain, target: self, action: #selector(backActionNav))
    }

    @objc func backActionNav() {
        navigationController?.popViewController(animated: true)
    }
}

// MARK: camera maps setup
extension TPDireccionAfiliacionComercioVC {

    func setupCameraMaps() {
        let camera = GMSCameraPosition.camera(withLatitude: 0, longitude: 0, zoom: 15.0)
        mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        mapView.translatesAutoresizingMaskIntoConstraints = false
        viewMap.addSubview(mapView)

        NSLayoutConstraint.activate([
            mapView.topAnchor.constraint(equalTo: viewMap.topAnchor),
            mapView.leadingAnchor.constraint(equalTo: viewMap.leadingAnchor),
            mapView.trailingAnchor.constraint(equalTo: viewMap.trailingAnchor),
            mapView.bottomAnchor.constraint(equalTo: viewMap.bottomAnchor)
        ])
    }
}

// MARK: SetupTextFields
extension TPDireccionAfiliacionComercioVC {

    func onlyNumberInTextField() {
        self.codigoPostalTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
    }

    @objc func textFieldDidChange(_ textField: UITextField) {
        if let text = textField.text, !text.isEmpty {
            let filteredText = text.filter { $0.isNumber }
            textField.text = filteredText
        }
    }
}

// MARK:
extension TPDireccionAfiliacionComercioVC {

    func getDirectionFromCoordinates(direction: String, completion: @escaping (CLLocationCoordinate2D?) -> Void) {

        let geocoder = CLGeocoder()

        geocoder.geocodeAddressString(direction) { ( placemarks, error ) in
            if let error = error {
                print("Error obteniendo coordenadas -> \(error.localizedDescription)")
                completion(nil)
                return
            }

            if let placeMark = placemarks?.first {
                let coordenadas = placeMark.location?.coordinate
                completion(coordenadas)
            } else {
                print("no se pudieron obtener las coordenadas de la dirección")
                completion(nil)
            }
        }
    }
}
