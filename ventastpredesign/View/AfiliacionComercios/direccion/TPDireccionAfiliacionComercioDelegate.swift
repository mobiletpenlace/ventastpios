//
//  TPDireccionAfiliacionComercioDelegate.swift
//  SalesCloud
//
//  Created by aestevezn on 26/04/24.
//  Copyright © 2024 TotalPlay. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation

// MARK: LocationManagerDelegate
extension TPDireccionAfiliacionComercioVC: CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.startUpdatingLocation()
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        let direction = TPAfiliacionComerciosShared.shared.cpEntregas + ", " + TPAfiliacionComerciosShared.shared.direccionEntregas
        guard let location = locations.last else { return }

        if direction != String.emptyString {
            getDirectionFromCoordinates(direction: direction) { coordinate in
                if let coord = coordinate {
                    let camera = GMSCameraPosition.camera(withLatitude: coord.latitude, longitude: coord.longitude, zoom: 15.0)
                    self.mapView.animate(to: camera)
                    let coordinate = CLLocationCoordinate2D(latitude: coord.latitude, longitude: coord.longitude)
                    let market = GMSMarker(position: coordinate)
                    market.icon = GMSMarker.markerImage(with: .blue)
                    market.map = self.mapView
                } else {
                    let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 15.0)
                    self.mapView.animate(to: camera)
                    let coordinate = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
                    let market = GMSMarker(position: coordinate)
                    market.icon = GMSMarker.markerImage(with: .blue)
                    market.map = self.mapView
                }
            }
        } else {
            locationManager.stopUpdatingLocation()

            let camera  = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 15.0)

            mapView.animate(to: camera)

            let coordinate = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            let market = GMSMarker(position: coordinate)
            market.icon = GMSMarker.markerImage(with: .blue)
            market.map = mapView
        }
    }
}
