//
//  TPAfiliationCompleteVC.swift
//  SalesCloud
//
//  Created by aestevezn on 26/04/24.
//  Copyright © 2024 TotalPlay. All rights reserved.
//

import UIKit

class TPAfiliationCompleteVC: BaseVentasView {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func closeProcessAction(_ sender: Any) {
        self.dismiss(animated: true)
    }

}
