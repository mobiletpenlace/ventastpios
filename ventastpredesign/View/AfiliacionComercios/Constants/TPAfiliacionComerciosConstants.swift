//
//  TPAfiliacionComerciosConstants.swift
//  SalesCloud
//
//  Created by aestevezn on 16/04/24.
//  Copyright © 2024 TotalPlay. All rights reserved.
//

import Foundation

public enum TypeDeposit {
    static let SEMANAL   =        "Semanal"
    static let QUINCENAL =        "Quincelnal"
    static let MENSUAL   =        "Mensual"
    static let OPTIONS   =        [SEMANAL, QUINCENAL, MENSUAL]
}

public enum AfiliacionSeguesName {
    static let DIRECTION_DELIVERY_SEGUE = "loadDirectionSegue"
}

public enum GirosComerciales {
    static let EDITORAS_DE_MUSICA               = "Editoras de música"
    static let DISENIO_INDUSTRIAL               = "Diseño Insdustrial"
    static let COMERCIAO_AL_PORMENOR_DE_BLANCOS = "Comercio al por menor de blancos"
    static let ARRAY_COMERCIOS = [EDITORAS_DE_MUSICA, DISENIO_INDUSTRIAL, COMERCIAO_AL_PORMENOR_DE_BLANCOS]
}
