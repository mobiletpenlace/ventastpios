//
//  StepContainerDelegate.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 24/05/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

extension StepContainer: RecommendedPlanModelObserver, UIScrollViewDelegate, RecommendedPlanObserver {

    func enviarData() {
        setDataView()
    }

    func actualizaPrecio() {
        setResumeLabels(totalProductos: mRecommendedPlanViewModel.getTotalProductos(), Totaldescuentos: mRecommendedPlanViewModel.getTotalDiscount(), TotalCostoInstalacion: mRecommendedPlanViewModel.getCosto())
    }

    func succesAddons() {
        selectModule(index: 1)
//        mRecommendedPlanViewModel.onSuccesConvivencia()
    }

    func succesGetDetailPlans() {
    }

    func succesGetDetailPlansBestFit() {
        if !faseConfigurarPlan.adivinaQuien {
            mRecommendedPlanViewModel.getDetallePlanes()
        }
    }

    func succesGetRecomendacionesIDs() {
        if !faseConfigurarPlan.adivinaQuien {
            mRecommendedPlanViewModel.getDetallePlanes()
        }
    }

    func onErrorEmploye() {
        print("error")
    }

    func succesGetProspects() {
        mRecommendedPlanViewModel.getRecomendacionesIDs()
    }

    func onSuccesConvivencia() {
        print("succes convivencia")
    }

    func onNotConvivencia(message: String) {
        print("succes not convivencia")
    }

    func succesAddons(response: ConsultaAddonsResponse) {
    }

    func successProspect(response: InfoProfileClientResponse) {
    }

    func succcessRecommendations(response: RecommendationsResponse) {
    }

    func successConsultDetail(response: ConsultaDetalleResponse) {
        print("succes consult detail")
    }

    func actualizaTotalPrecio(precio: Float) {
    }

    func actualizaTotalProductos(total: Int) {
    }

    func actualizaServiciosResumen(servicios: [Servicio]) {
    }

    func actualizaProductosResumen(productos: [Adicional]) {
    }

    func actualizaPromocionesResumen(promociones: [Promocion]) {
    }

    func ocultaServicios() {
    }

    func ocultaProductos() {
    }

    func ocultaPromociones() {
    }

    func retiraResumen() {
    }

    func setPlanSecundario(objeto: LlenadoObjeto) {
        print(objeto)
    }
}

extension StepContainer: RecommendedPlanSetRequestAddons {
    func sendRequestAddons(request: ConsultaAddonsRequest) {
        self.addonsRequest = request
    }
}
