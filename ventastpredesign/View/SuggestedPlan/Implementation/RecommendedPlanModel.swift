//
//  RecommendedPlanModel.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 12/05/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import RealmSwift
import SwiftEventBus

protocol RecommendedPlanModelObserver {
    func successProspect(response: InfoProfileClientResponse)
    func succcessRecommendations(response: RecommendationsResponse)
    func successConsultDetail(response: ConsultaDetalleResponse)
    func succesAddons(response: ConsultaAddonsResponse)
    func onSuccesConvivencia()
    func onNotConvivencia(message: String)
}

class RecommendedPlanModel: BaseModel {

    var mPlan: Plan!
    var mSitio: Sitio!
    var mProspecto: NProspecto!

    var mServicios: [Servicio] = []
    var mProductos: [Adicional] = []
    var mPromociones: [Promocion] = []
    var CostoInstalacion: Float = 0.0

    var viewModel: RecommendedPlanModelObserver?
    var server: ServerDataManager2<InfoProfileClientResponse>?
    var server2: ServerDataManager2<RecommendationsResponse>?
    var server3: ServerDataManager2<ConsultaDetalleResponse>?
    var server4: ServerDataManager2<ConsultaAddonsResponse>?
    var server5: ServerDataManager2<ConsultaConvivenciaResponse>?

    var url = AppDelegate.API_SALESFORCE + ApiDefinition.API_PLAN_SUGERIDO
    var url2 = AppDelegate.API_SALESFORCE + ApiDefinition.API_PLAN_RECOMENDACION
    var url3 = AppDelegate.API_SALESFORCE + ApiDefinition.API_PLAN_PAQUETE
    var url4 = AppDelegate.API_SALESFORCE + ApiDefinition.API_ADDONS_VENTA
    var url5 = AppDelegate.API_SALESFORCE + ApiDefinition.API_CONVIVENCIA_PROMOCIONES
    init (observer: BaseViewModelObserver) {
        server = ServerDataManager2<InfoProfileClientResponse>()
        server2 = ServerDataManager2<RecommendationsResponse>()
        server3 = ServerDataManager2<ConsultaDetalleResponse>()
        server4 = ServerDataManager2<ConsultaAddonsResponse>()
        server5 = ServerDataManager2<ConsultaConvivenciaResponse>()
        super.init(observerVM: observer)
    }

    func atachModel(viewModel: RecommendedPlanModelObserver) {
        self.viewModel = viewModel
    }

    func getInfoCliente(_ request: InfoProfileClientRequest) {
        url = AppDelegate.API_SALESFORCE + ApiDefinition.API_PLAN_SUGERIDO
        server?.setValues(requestUrl: url, delegate: self, headerType: .headersSalesforce)
        server?.request(requestModel: request)
    }

    func getRecomendacionesIDs(_ request: RecommendationsRequest) {
        url2 = AppDelegate.API_SALESFORCE + ApiDefinition.API_PLAN_RECOMENDACION
        server2?.setValues(requestUrl: url2, delegate: self, headerType: .headersSalesforce)
        server2?.request(requestModel: request)
    }

    func getDetallePlanes(_ request: ConsultaDetalleRequest) {
        url3 = AppDelegate.API_SALESFORCE + ApiDefinition.API_PLAN_PAQUETE
        server3?.setValues(requestUrl: url3, delegate: self, headerType: .headersSalesforce)
        server3?.request(requestModel: request)
    }

    func chargeAddons(_ request: ConsultaAddonsRequest) {
        url4 = AppDelegate.API_SALESFORCE + ApiDefinition.API_ADDONS_VENTA
        server4?.setValues(requestUrl: url4, delegate: self, headerType: .headersSalesforce)
        server4?.request(requestModel: request)
    }

    func getConvivencia(_ request: ConsultaConvivenciaRequest) {
        url5 = AppDelegate.API_SALESFORCE + ApiDefinition.API_CONVIVENCIA_PROMOCIONES
        server5?.setValues(requestUrl: url5, delegate: self, headerType: .headersSalesforce)
        server5?.request(requestModel: request)
    }

    func getDetallePlanesBestFit(_ request: ConsultaDetalleRequest) {
        url3 = AppDelegate.API_SALESFORCE + ApiDefinition.API_PLAN_PAQUETE
        server3?.setValues(requestUrl: url3, delegate: self, headerType: .headersSalesforce)
        server3?.request(requestModel: request)
    }

    func getSitio() -> Sitio {
        var mSitio = Sitio()
        if RealManager.findFirst(object: Sitio.self) != nil {
            mSitio = Sitio(value: RealManager.findFirst(object: Sitio.self)!)
        }
        return mSitio
    }

    func getProspecto() -> NProspecto {
        var mProspecto = NProspecto()
        if RealManager.findFirst(object: NProspecto.self) != nil {
            mProspecto = NProspecto(value: RealManager.findFirst(object: NProspecto.self)!)
        }
        return mProspecto
    }

    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if requestUrl == url {
            let mInfoProfileClientResponse = response as! InfoProfileClientResponse
            if mInfoProfileClientResponse.result == "0" {
                SwiftEventBus.post("envia_IDCliente", sender: mInfoProfileClientResponse.infoClte?.idPerfilClte)
                viewModel?.successProspect(response: mInfoProfileClientResponse)
            } else {

            }
        }

        if requestUrl == url2 {
            let mRecommendationsResponse = response as! RecommendationsResponse
            if mRecommendationsResponse.response == "0" {
                viewModel?.succcessRecommendations(response: mRecommendationsResponse)
            } else {
                let omitirButton: OmitirButton = UIStoryboard(name: "OmitirButton", bundle: nil).instantiateViewController(withIdentifier: "OmitirButton") as! OmitirButton
                omitirButton.providesPresentationContextTransitionStyle = true
                omitirButton.definesPresentationContext = true
                omitirButton.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                omitirButton.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                UIApplication.topViewController()?.present(omitirButton, animated: false, completion: nil)
                omitirButton.popUpView.isHidden = false
                omitirButton.cuestionsView.isHidden = true
                omitirButton.configurePlanView.isHidden = true
            }
        }

        if requestUrl == url3 {
            let mConsultaDetalleResponse = response as! ConsultaDetalleResponse
            if mConsultaDetalleResponse.result == "0" {
                viewModel?.successConsultDetail(response: mConsultaDetalleResponse)
            } else {

            }
        }

        if requestUrl == url4 {
            print("Obtener Addons")
            let mConsultaAddonsResponse = response as! ConsultaAddonsResponse
            if mConsultaAddonsResponse.result == "0" {
                viewModel?.succesAddons(response: mConsultaAddonsResponse)
            } else {

            }
        }

        if requestUrl == url5 {
            let mConsultaConvivenciaResponse = response as! ConsultaConvivenciaResponse
            if mConsultaConvivenciaResponse.result == "0" {
                viewModel?.onSuccesConvivencia()
            } else {
                viewModel?.onNotConvivencia(message: mConsultaConvivenciaResponse.resultDescription)
            }
        }
    }

}
