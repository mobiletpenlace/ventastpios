//
//  RecommendedPlanViewModel.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 12/05/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus
import Amplitude

protocol RecommendedPlanSetRequestAddons {
    func sendRequestAddons(request: ConsultaAddonsRequest)
}

protocol RecommendedPlanObserver {
    func succesGetProspects()
    func succesGetRecomendacionesIDs()
    func succesGetDetailPlans()
    func succesGetDetailPlansBestFit()
    func succesAddons()
    func onErrorEmploye()
    func actualizaTotalPrecio(precio: Float)
    func actualizaTotalProductos(total: Int)
    func actualizaServiciosResumen(servicios: [Servicio])
    func actualizaProductosResumen(productos: [Adicional])
    func actualizaPromocionesResumen(promociones: [Promocion])
    func ocultaServicios()
    func ocultaProductos()
    func ocultaPromociones()
    func retiraResumen()
    func actualizaPrecio()
    func enviarData()
}

extension RecommendedPlanObserver {
    func succesGetProspects() {}
    func succesGetRecomendacionesIDs() {}
    func succesGetDetailPlans() {}
    func succesGetDetailPlansBestFit() {}
    func succesAddons() {}
    func onErrorEmploye() {}
    func actualizaTotalPrecio(precio: Float) {}
    func actualizaTotalProductos(total: Int) {}
    func actualizaServiciosResumen(servicios: [Servicio]) {}
    func actualizaProductosResumen(productos: [Adicional]) {}
    func actualizaPromocionesResumen(promociones: [Promocion]) {}
    func ocultaServicios() {}
    func ocultaProductos() {}
    func ocultaPromociones() {}
    func retiraResumen() {}
    func actualizaPrecio() {}
    func enviarData() {}
}

class RecommendedPlanViewModel: BaseViewModel, RecommendedPlanModelObserver {
    var idPerfilClte = ""
    var arrayIdPlanesRecomendados: [String] = []
    var arrayidServiciosAdd: [String] = []
    var arrayidpromocionesAdd: [String] = []
    var nombre = ""
    var mPlan: String!
    var mSitio: Sitio!
    var idPlan1 = ""
    var familiaPaquetePlan1 = ""
    var idPlan2 = ""
    var familiaPaquetePlan2 = ""
    var mProspecto: NProspecto!
    var mServicios: [Servicio] = []
    var mOtherProducts: [Adicional] = []
    var mProductosPremium: Producto = Producto()
    var mProductosParrillas: Producto = Producto()
    var mProductos: [Adicional] = []
    var mPromociones: [Promocion] = []
    var selectedServicios: [Servicio] = []
    var selectedProductos: [Adicional] = []
    var selectedPromociones: [Promocion] = []
    var CostoInstalacion: Float = 0.0
    var promoValidacion: Promocion!
    var infoAddons: InfoAdicionalesCreacion = InfoAdicionalesCreacion()
    var totalPrice: Float = 0.0
    var totalDiscount: Int = 0
    var totalProductos: Int = 0
    var mProductosAdd = ProductosAdd()
    var mPromocionesAdd = PromocionesAdd()
    var mServiciosAdd = ServiciosAdd()
    var i: Int!
    var j: Int!
    var planBestFit = ""
    let objetoPlan1: LlenadoObjeto = LlenadoObjeto()
    let objetoPlan2: LlenadoObjeto = LlenadoObjeto()
    var recomendaciones: RecommendationsResponse = RecommendationsResponse()
    var observer: RecommendedPlanObserver!
    var model: RecommendedPlanModel!

    var delegateAddonsRequest: RecommendedPlanSetRequestAddons?

    override init(view: BaseVentasView) {
        super.init(view: view)
        model = RecommendedPlanModel(observer: self)
        model.atachModel(viewModel: self)
    }

    func atachView(_ observer: RecommendedPlanObserver) {
        self.observer = observer
        SwiftEventBus.onMainThread(self, name: "Agrega_servicio") { [weak self] result in
            if let servicio = result?.object as? Servicio {
                self?.selectedServicios.append(servicio)
                dump(self!.selectedServicios)
                self?.actualiza()
            }
        }

        SwiftEventBus.onMainThread(self, name: "setTotal") { [weak self] result in
            if let total = result?.object as? Int {
                self?.totalProductos = total
            }
        }
        SwiftEventBus.onMainThread(self, name: "setDiscount") { [weak self] result in
            if let discount = result?.object as? Int {
                self?.totalDiscount = discount
            }
        }
        SwiftEventBus.onMainThread(self, name: "actualizarPrecio") { [weak self] _ in
            self?.observer.actualizaPrecio()
        }

        SwiftEventBus.onMainThread(self, name: "Retira_servicio") { [weak self] result in
            if let servicio = result?.object as? Servicio {
                self?.eliminaServicio(servicio)
            }
        }

        SwiftEventBus.onMainThread(self, name: "Agrega_Addon") { [weak self] result in
            if let addon = result?.object as? Adicional {
                self?.selectedProductos.append(addon)
                self?.actualiza()
            }
            dump(self!.selectedProductos)
        }
        SwiftEventBus.onMainThread(self, name: "Retira_Addon") { [weak self] result in
            if let addon = result?.object as? Adicional {
                self?.eliminaAddon(addon)
            }
            dump(self!.selectedProductos)
        }

        SwiftEventBus.onMainThread(self, name: "valida_convivencia_Promocion") { [weak self] result in
            if let promocion = result?.object as? Promocion {
                self?.validaConvivencia(promocion: promocion)
            }
        }

        SwiftEventBus.onMainThread(self, name: "Retira_Promocion") { [weak self] result in
            if let promocion = result?.object as? Promocion {
                self?.eliminaPromo(promocion)
            }
        }

        SwiftEventBus.onMainThread(self, name: "RetiraResumen") { [weak self] _ in
            self?.observer.retiraResumen()
        }

        SwiftEventBus.onMainThread(self, name: "Addons_cargados") { [weak self] _ in
            self?.observer.enviarData()
        }

        SwiftEventBus.onMainThread(self, name: "RemoveEventBus") { [weak self] _ in
            self?.removeEventBus()
        }
    }

    func getInfoCliente(typePlan: String, action: String) {
        view.showLoading(message: "")
        let mInfoProfileClientRequest: InfoProfileClientRequest = InfoProfileClientRequest()
        let objetoInfo = Info2()
        let objetoRespuestas = Respuestas()
        mSitio = model.getSitio()
        objetoInfo.cluster = mSitio.cluster
        objetoInfo.distrito = mSitio.distrito
        objetoRespuestas.appsFavoritas = fasePreguntas.favoriteApps
        objetoRespuestas.proveedorInternet = fasePreguntas.internetProvider
        objetoRespuestas.numHabitaciones = String(fasePreguntas.numberOfRooms)
        objetoRespuestas.personasConectadas = String(fasePreguntas.numberOfPeople)
        objetoRespuestas.proveedorTelefonia = fasePreguntas.phoneProvider
        objetoRespuestas.proveedorTV = fasePreguntas.tvProvider
        objetoRespuestas.UsosRed = fasePreguntas.usesInternet

        if objetoRespuestas.personasConectadas?.contains("OMITIR") == false || objetoRespuestas.numHabitaciones?.contains("OMITIR") == false || objetoRespuestas.proveedorTV?.contains("OMITIR") == false || objetoRespuestas.proveedorInternet?.contains("OMITIR") == false || objetoRespuestas.proveedorTelefonia?.contains("OMITIR") == false || objetoRespuestas.appsFavoritas?.contains("OMITIR") == false || objetoRespuestas.UsosRed?.contains("OMITIR") == false {
        }

        objetoInfo.Respuestas = objetoRespuestas
        objetoInfo.plaza = mSitio.plaza
        objetoInfo.tipoPlan = typePlan
        mInfoProfileClientRequest.accion = action
        mInfoProfileClientRequest.info = objetoInfo
        model.getInfoCliente(mInfoProfileClientRequest)
    }

    func getRecomendacionesIDs() {
        let mRecommendationsRequest: RecommendationsRequest = RecommendationsRequest()
        let objetoInData = InData()
        mRecommendationsRequest.accion = "ejecutarE"
        objetoInData.canalFront = "AppVentasBestFitPiloto"
        objetoInData.contextObjName = "Perfil_Cliente"
        objetoInData.estimuloFiscal = false
        objetoInData.esPostVenta = false
        objetoInData.maxRecomm = "4"
        objetoInData.nombreCanal = "MC"
        objetoInData.plaza = "CIUDAD DE MEXICO"
        objetoInData.contextObjId = idPerfilClte
        mRecommendationsRequest.inData = objetoInData
        model.getRecomendacionesIDs(mRecommendationsRequest)
        observer?.succesGetDetailPlans()
    }

    func getDetallePlanes() {
        let mConsultaDetalleRequest: ConsultaDetalleRequest = ConsultaDetalleRequest()
        let mInfoConsultaDetallePaquetes = InfoConsultaDetallePaquetes()
        let adicionales = Adicionales()
        let planes = Plans()

        planes.idPlanes = arrayIdPlanesRecomendados
        adicionales.idServiciosAdd = arrayidServiciosAdd
        mInfoConsultaDetallePaquetes.adicionales = adicionales
        mInfoConsultaDetallePaquetes.estimuloFiscal = false
        mInfoConsultaDetallePaquetes.planes = planes
        idPlan1 = objetoPlan1.idPlan
        idPlan2 = objetoPlan2.idPlan
      //  mInfoConsultaDetallePaquetes.planes?.idPlanes[1]
        mConsultaDetalleRequest.accion = "Consulta"
        mConsultaDetalleRequest.info = mInfoConsultaDetallePaquetes
        model.getDetallePlanes(mConsultaDetalleRequest)
        observer?.succesGetDetailPlans()
    }

    func getDetallePlanesBestFit(plan: String) {
        let mConsultaDetalleRequest: ConsultaDetalleRequest = ConsultaDetalleRequest()
        let mInfoConsultaDetallePaquetes = InfoConsultaDetallePaquetes()
        let objetoAdicionales = Adicionales()
        let objetoPlanes = Plans()
        objetoPlanes.idPlanes.append(plan)
        mInfoConsultaDetallePaquetes.adicionales = objetoAdicionales
        mInfoConsultaDetallePaquetes.estimuloFiscal = false
        mInfoConsultaDetallePaquetes.planes = objetoPlanes
        idPlan1 = (mInfoConsultaDetallePaquetes.planes?.idPlanes[0])!
        mConsultaDetalleRequest.accion = "Consulta"
        mConsultaDetalleRequest.info = mInfoConsultaDetallePaquetes
        model.getDetallePlanesBestFit(mConsultaDetalleRequest)
        observer?.succesGetDetailPlansBestFit()
    }

    // MARK: - devoluciones al view

    func getServicios() -> [Servicio] {
        return mServicios
    }

    func getPremium() -> Producto {
        return mProductosPremium
    }

    func getParrillas() -> Producto {
        return mProductosParrillas
    }

    func getProductos() -> [Adicional] {
        return mProductos
    }

    func getOtherProductos() -> [Adicional] {
        return mOtherProducts
    }

    func getPromociones() -> [Promocion] {
        return mPromociones
    }

    func getCosto() -> Float {
        return CostoInstalacion
    }
    func getTotalProductos() -> Int {
        return totalProductos
    }

    func getTotalDiscount() -> Int {
        return totalDiscount
    }

    func getAddonRecommended(first: Bool) -> LlenadoObjeto {
        if first {
            return objetoPlan1
        } else {
            return objetoPlan2
        }
    }

    func successConsultDetail(response: ConsultaDetalleResponse) {
        dump(response.info)
        if faseConfigurarPlan.adivinaQuien == true {
            response.info?.infoPlanes?.detallePlanes[0].servIncSinAgregar?.serviciosIncSinAgregar.forEach { element in
                if element.nombre?.contains(string: "WIFI") != false {
                    objetoPlan1.wifi = "WIFI"
                    objetoPlan1.maximoAgregarWIFI = element.maximoAgregar ?? 0
                }
            }
            response.info?.infoPlanes?.detallePlanes[0].servIncSinAgregar?.serviciosIncSinAgregar.forEach { element in
                if element.nombre?.contains(string: "Television") != false {
                    objetoPlan1.tv = "TV"
                    objetoPlan1.maximoAgregarTV = element.maximoAgregar ?? 0
                }
            }
            objetoPlan1.precioProntoPago = (response.info?.infoPlanes?.detallePlanes[0].precioProntoPago) as! Int
            objetoPlan1.arregloProductos = (response.info?.infoPlanes?.detallePlanes[0].productosIncluidos?.productos) ?? [ProductosIncluidosChild]()
            objetoPlan1.streaming = (response.info?.infoPlanes?.detallePlanes[0].servicioStreaming) ?? "0"
            objetoPlan1.Titulo = (response.info?.infoPlanes?.detallePlanes[0].nombrePaquete)!
            objetoPlan1.Megas = (response.info?.infoPlanes?.detallePlanes[0].megas)!
            objetoPlan1.opcionPlan = ""
            if (response.info?.infoPlanes?.detallePlanes[0].tvIncluidas.isEmpty)! {
                objetoPlan1.numeroTVs = "0"
            } else {
            objetoPlan1.numeroTVs = (response.info?.infoPlanes?.detallePlanes[0].tvIncluidas)!
            }

            objetoPlan1.familiaPaquete = (response.info?.infoPlanes?.detallePlanes[0].familiaPaquete)!
            objetoPlan1.recomendacionTv = 0
            SwiftEventBus.post("SetPlanPrimario", sender: objetoPlan1)
            faseSelectedPlan.plan1 = objetoPlan1
            faseSelectedPlan.selectedPlan = objetoPlan1
        } else {
            for detallePlan in response.info!.infoPlanes!.detallePlanes {
                if detallePlan.id == objetoPlan1.idPlan {
                    if detallePlan.servIncSinAgregar != nil {
                        for servicioIncluido in detallePlan.servIncSinAgregar!.serviciosIncSinAgregar {
                            if servicioIncluido.nombre?.uppercased().contains(string: "WIFI") != false {
                                objetoPlan1.wifi = "WIFI"
                            }
                            if servicioIncluido.nombre?.uppercased().contains(string: "TELEVISION") != false {
                                objetoPlan1.wifi = "TV"
                            }
                        }
                    }
                    objetoPlan1.servIncSinAgregar = (response.info?.infoPlanes?.detallePlanes[0].servIncSinAgregar)
                    objetoPlan1.arregloProductos = detallePlan.productosIncluidos!.productos
                    objetoPlan1.Megas = detallePlan.megas ?? ""
                    objetoPlan1.Titulo = detallePlan.nombrePaquete ?? ""
                    objetoPlan1.streaming = detallePlan.servicioStreaming ?? ""
                    objetoPlan1.numeroTVs = detallePlan.tvIncluidas ?? "0"
                    objetoPlan1.precioProntoPago = detallePlan.precioProntoPago ?? 0
                    objetoPlan1.familiaPaquete = detallePlan.familiaPaquete!
                    objetoPlan1.precioLista = detallePlan.precioLista ?? 0

                } else if detallePlan.id == objetoPlan2.idPlan {
                    if detallePlan.servIncSinAgregar != nil {
                        for servicioIncluido in detallePlan.servIncSinAgregar!.serviciosIncSinAgregar {
                            if servicioIncluido.nombre?.uppercased().contains(string: "WIFI") != false {
                                objetoPlan2.wifi = "WIFI"
                            }
                            if servicioIncluido.nombre?.uppercased().contains(string: "TELEVISION") != false {
                                objetoPlan2.wifi = "TV"
                            }
                        }
                    }
                    objetoPlan2.arregloProductos = detallePlan.productosIncluidos?.productos ?? []
                    objetoPlan2.Megas = detallePlan.megas ?? ""
                    objetoPlan2.Titulo = detallePlan.nombrePaquete ?? ""
                    objetoPlan2.streaming = detallePlan.servicioStreaming ?? ""
                    objetoPlan2.numeroTVs = detallePlan.tvIncluidas ?? "0"
                    objetoPlan2.precioProntoPago = detallePlan.precioProntoPago ?? 0
                    objetoPlan2.familiaPaquete = detallePlan.familiaPaquete!
                    objetoPlan2.precioLista = detallePlan.precioLista ?? 0
                }
            }

            if response.info?.infoAddons?.serviciosAdd != nil {
                for addon in response.info!.infoAddons!.serviciosAdd!.servicios {
                    if addon.Id == objetoPlan1.idServicioRecomendado {
                        objetoPlan1.precioAdicionales = addon.precio ?? 0
                        objetoPlan1.precioTotal = objetoPlan1.precioProntoPago + objetoPlan1.precioAdicionales
                    }
                    if addon.Id == objetoPlan2.idServicioRecomendado {
                        objetoPlan2.precioAdicionales = addon.precio ?? 0
                        objetoPlan2.precioTotal = objetoPlan2.precioProntoPago + objetoPlan2.precioAdicionales
                    }
                }
            }
            SwiftEventBus.post("SetPlanPrimario", sender: objetoPlan1)
            SwiftEventBus.post("SetPlanSecundario", sender: objetoPlan2)
            faseSelectedPlan.plan1 = objetoPlan1
            faseSelectedPlan.plan2 = objetoPlan2
        }

        Amplitude.instance().logEvent("Show_SuggestedPlanScreen", withEventProperties: ["SuggestedPlan_Principal": objetoPlan1.Titulo, "SuggestedPlan_Secondary": objetoPlan2.Titulo])
        view.hideLoading()
    }

    func BuscaAdoons() {
//        mPlan = plan
        var request = ConsultaAddonsRequest()
        if faseSelectedPlan.firstPlan == true {
            request.idPlan = idPlan1
            SwiftEventBus.post("receiveIDPlan", sender: idPlan1)
        } else {
            request.idPlan = idPlan2
            SwiftEventBus.post("receiveIDPlan", sender: idPlan2)
        }
//        request.idPlan = plan
        mSitio = model.getSitio()
        mProspecto = model.getProspecto()
        request.info.canal = mProspecto.canalEmpleado
        request.info.canalFront = "AppVentasBestFitPiloto"
        request.info.cluster = mSitio.cluster
        request.info.colonia = mProspecto.colonia
        request.info.distrito = mSitio.distrito
        request.info.metodoPago = "Efectivo"
        request.info.plaza = mSitio.plaza
        request.info.codigoPostal = mProspecto.codigoPostal
        request.info.subcanal = mProspecto.subCanalEmpleado
        request.info.subcanalVenta = "Cambaceo"
        request.info.consultaAddons = true
        request.info.consultaServ = true
        request.info.consultaPromo = true
        if mSitio.isEstimulo == "true" {
            request.info.estimulo = true
        } else {
            request.info.estimulo = false
        }
        delegateAddonsRequest?.sendRequestAddons(request: request)
        model.chargeAddons(request)
    }

    func succesAddons(response: ConsultaAddonsResponse) {
        mServicios = response.infoAddons.serviciosAdd?.servicios as! [Servicio]
        response.infoAddons.productosAdd?.productos.forEach { result in
            if !result.Agrupacion.contains("HOGAR SEGURO") {
                result.adicional.forEach { i in
                    if !i.nombre.contains("HBO"),
                       !i.nombre.contains("FOX"),
                       i.tipoProducto != "Incluido sin agregar" { // Temporal
                        let containsBundles = i.grupoycategoria.contains { category in
                            category.categoria == "Bundles" // Temporal
                        }
                        if !containsBundles {
                            mOtherProducts.append(i)
                        }
                    }
                }
            }
        }

//        response.infoAddons.productosAdd!.productos.forEach(){Result in
//            if Result.Agrupacion != "HOGAR SEGURO"{
//                mOtherProducts.append(Result)
//            }
//        }

//        for agrupacion in response.infoAddons.productosAdd!.productos{
//            if agrupacion.Agrupacion == "SIN AGRUPACION" || agrupacion.Agrupacion == "Canales" || agrupacion.Agrupacion == "Suite Incluida"{
//                mOtherProducts = agrupacion
//            }
//
//            if agrupacion.Agrupacion == "Suite Incluida"{
//                mOtherProducts = agrupacion
//            }
//
//        }

        for agrupacion in response.infoAddons.productosAdd!.productos {
            if agrupacion.Agrupacion == "Canales" {
                mProductosPremium = agrupacion
            }
            if agrupacion.Agrupacion == "Parrillas" {
                mProductosParrillas = agrupacion
            }
        }
        CostoInstalacion = 0.0
        for agrupacion in response.infoAddons.costoInstalacionAdd!.productos {
            if agrupacion.Agrupacion != "HOGAR SEGURO" {
                for adicional in agrupacion.adicional {
                    CostoInstalacion = CostoInstalacion + adicional.precio
                }
            }
        }
        mProductos = mOtherProducts
        mPromociones = response.infoAddons.promocionesAdd?.promociones as! [Promocion]
        observer?.succesAddons()
        view.hideLoading()
    }

    func actualiza() {
        dump(selectedProductos)
        totalPrice = 0.0
        totalProductos = 0
        totalDiscount = 0

        for servicio in selectedServicios {
            totalPrice += servicio.precio.rounded()
        }

        for producto in selectedProductos {
            totalPrice += producto.precio.rounded()
            print(totalPrice)
            print(producto.precio.rounded())
        }

        for promociones in selectedPromociones {
//            totalDiscount += promociones.montoDescuento.rounded()
        }

//        totalPrice += mPlan.precioProntoPago.rounded()
        // totalPrice = (totalPrice + CostoInstalacion) - totalDiscount
        totalProductos = selectedServicios.count + selectedProductos.count + selectedPromociones.count + 1

        if selectedServicios.count > 0 {
            var acumulatedServicios: [Servicio] = []
            for servicio in selectedServicios {
                var conteo: Int = 0
                for serv in selectedServicios {
                    if servicio.Id == serv.Id {
                        conteo += 1
                    }
                }
                if conteo > 1 {
                    if acumulatedServicios.count == 0 {
                        let auxServ = servicio
                        auxServ.cantidad = conteo
                        acumulatedServicios.append(auxServ)
                    } else {
                        var exist: Bool = false
                        for serv in acumulatedServicios {
                            if serv.Id == servicio.Id {
                                exist = true
                            }
                        }
                        if !exist {
                            let auxServ = servicio
                            auxServ.cantidad = conteo
                            acumulatedServicios.append(auxServ)
                        }
                    }
                } else {
                    let auxServ = servicio
                    auxServ.cantidad = 1
                    acumulatedServicios.append(auxServ)
                }

            }
            observer.actualizaServiciosResumen(servicios: acumulatedServicios)
        } else {
            observer.ocultaServicios()
        }

        if selectedProductos.count > 0 {
            observer.actualizaProductosResumen(productos: selectedProductos)
        } else {
            observer.ocultaProductos()
        }

        if selectedPromociones.count > 0 {
            observer.actualizaPromocionesResumen(promociones: selectedPromociones)
        } else {
            observer!.ocultaPromociones()
        }
        observer.actualizaTotalPrecio(precio: totalPrice)
        observer.actualizaTotalProductos(total: totalProductos)
    }

    func eliminaServicio(_ servicio: Servicio) {
        var count = 1
        let aux = selectedServicios
        selectedServicios = []
        for mServicio in aux {
            if count > 0 {
                if mServicio.Id == servicio.Id {
                    count = 0
                } else {
                    selectedServicios.append(mServicio)
                }
            } else {
                selectedServicios.append(mServicio)
            }
        }
//        actualiza()
    }

    func eliminaAddon(_ addon: Adicional) {
        let aux = selectedProductos
        selectedProductos = []
        for mAddon in aux {
            if mAddon.Id == addon.Id {
            } else {
                selectedProductos.append(mAddon)
            }
        }
        actualiza()
    }

    func eliminaPromo(_ promo: Promocion) {
        let aux = selectedPromociones
        selectedPromociones = []
        for mAddon in aux {
            if mAddon.Id == promo.Id {
                if promo.adicionalProductoId != "" {
                    retiraProductoIncluido(promo.adicionalProductoId)
                }
            } else {
                selectedPromociones.append(mAddon)
            }
        }
//        actualiza()
    }

    func successProspect(response: InfoProfileClientResponse) {
        idPerfilClte = (response.infoClte?.idPerfilClte)!
        SwiftEventBus.post("receiveIDProfileCliente", sender: idPerfilClte)
        observer?.succesGetProspects()
    }

    func succcessRecommendations(response: RecommendationsResponse) {
        // AQUI DEBO TRAER DOBLE DE MEGAS Y DE CANALES
        recomendaciones = response
        for recomendacion in response.infoSalidaRecomendaciones!.recomendaciones {
            if recomendacion.opcionPlan == "Opcion 1" {
                if recomendacion.promocionesRecomendadas != nil {
                    for recomendacionPromo in recomendacion.promocionesRecomendadas!.promociones {
                        if ((recomendacionPromo.nombre?.uppercased().contains(string: "MEGAS")) == true) && recomendacionPromo.esAutomatica == true {
                            objetoPlan1.hasPromoMegas = true
                            objetoPlan1.idPromoMegas = recomendacionPromo.Id ?? ""
                        }
                    }
                }
                if recomendacion.serviciosRecomendados != nil {
                    if (recomendacion.serviciosRecomendados?.servicios.count)! > 0 {
                        objetoPlan1.hasRecomendation = true
                        objetoPlan1.idServicioRecomendado = recomendacion.serviciosRecomendados?.servicios[0].Id ?? ""
                    }
                }
                objetoPlan1.idPlan = recomendacion.PlanRecomendado ?? ""
            } else if recomendacion.opcionPlan == "Opcion 2" {
                if (response.infoSalidaRecomendaciones?.recomendaciones.count)! > 1 {
                    if recomendacion.promocionesRecomendadas != nil {
                        for recomendacionPromo in recomendacion.promocionesRecomendadas!.promociones {
                            if ((recomendacionPromo.nombre?.uppercased().contains(string: "MEGAS")) == true) && recomendacionPromo.esAutomatica == true {
                                objetoPlan2.hasPromoMegas = true
                                objetoPlan2.idPromoMegas = recomendacionPromo.Id ?? ""
                            }
                        }
                    }
                    if recomendacion.serviciosRecomendados != nil {
                        if (recomendacion.serviciosRecomendados?.servicios.count)! > 0 {
                            objetoPlan2.hasRecomendation = true
                            objetoPlan2.idServicioRecomendado = recomendacion.serviciosRecomendados?.servicios[0].Id ?? ""
                        }
                    }
                    objetoPlan2.idPlan = recomendacion.PlanRecomendado ?? ""
                }
            }
        }

        // PLANES ID PLANES
        for i in 0..<response.infoSalidaRecomendaciones!.recomendaciones.count {
            arrayIdPlanesRecomendados.append((response.infoSalidaRecomendaciones?.recomendaciones[i].PlanRecomendado)!)
        }

//      ID SERVICIOS ADD
//      AQUI HAY UN BUG :v
        for i in 0..<response.infoSalidaRecomendaciones!.recomendaciones.count {
            if response.infoSalidaRecomendaciones!.recomendaciones[i].serviciosRecomendados != nil {
                for j in 0..<(response.infoSalidaRecomendaciones!.recomendaciones[i].serviciosRecomendados?.servicios.count)! {
                    arrayidServiciosAdd.append((response.infoSalidaRecomendaciones?.recomendaciones[i].serviciosRecomendados?.servicios[j].Id)!)
                }
            }
        }

        for i in 0..<(response.infoSalidaRecomendaciones?.recomendaciones.count)! {
            if response.infoSalidaRecomendaciones?.recomendaciones[i].promocionesRecomendadas != nil {
                for j in 0..<(response.infoSalidaRecomendaciones?.recomendaciones[i].promocionesRecomendadas?.promociones.count)! {
                    arrayidpromocionesAdd.append((response.infoSalidaRecomendaciones?.recomendaciones[i].promocionesRecomendadas?.promociones[j].Id)!)
                }
            }
        }

        observer?.succesGetRecomendacionesIDs()
    }

    func validaConvivencia(promocion: Promocion) {
        promoValidacion = promocion
        var request: ConsultaConvivenciaRequest = ConsultaConvivenciaRequest()
        request.idPlan = idPlan1
        request.promocionesNew.append(promocion.Id)
        for promo in selectedPromociones {
            request.promocionesOld.append(promo.Id)
        }
        model.getConvivencia(request)
    }

    func removeEventBus() {
        SwiftEventBus.unregister(self)
    }

    func onSuccesConvivencia() {
        view.hideLoading()
        selectedPromociones.append(promoValidacion)
        SwiftEventBus.post("Agrega_Promocion", sender: promoValidacion)
        if promoValidacion.adicionalProductoId != "" {
            agregaProductoIncluido(promoValidacion.adicionalProductoId)
        }
//        actualiza()
    }

    func onNotConvivencia(message: String) {
        view.hideLoading()
        Constants.Alert(title: "Aviso", body: message, type: type.Error, viewC: view)
        SwiftEventBus.post("RetiraPromoCelda", sender: promoValidacion.Id)
    }

    func agregaProductoIncluido(_ producto: String) {
        SwiftEventBus.post("AgregaProductoCelda", sender: producto)
    }

    func retiraProductoIncluido(_ producto: String) {
        SwiftEventBus.post("RetiraProductoCelda", sender: producto)
    }

}
