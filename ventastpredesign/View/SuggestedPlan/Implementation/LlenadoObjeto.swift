//
//  LlenadoObjeto.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 19/05/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation

class LlenadoObjeto: NSObject {
    var hasRecomendation: Bool = false
    var hasPromoMegas: Bool = false
    var hasPrmoCanales: Bool = false
    var hasProducto: [Bool] = []
    var idServicioRecomendado: String = ""
    var idPromoMegas: String = ""
    var idPromoCanales: String =  ""
    var idPromosProducto: [String] = []
    var opcionPlan: String = ""
    var idPlan: String =  ""
    var familiaPaquete: String = ""
    var Titulo: String = ""
    var Megas: String = ""
    var precioProntoPago: Int = 0
    var precioAdicionales: Int = 0
    var precioTotal: Int = 0
    var precioLista: Int = 0
    var numeroTVs: String = "0"
    var wifi: String = ""
    var maximoAgregarWIFI: Int = 0
    var maximoAgregarTV: Int = 0
    var tv: String = ""
    var recomendacionTv: Int = 0
    var arregloProductos: [ProductosIncluidosChild] = []
    var arregloPromocionesIncluidas: [Promociones]  = []
    var servIncSinAgregar: ServIncSinAgregar?
    var streaming: String = ""
}
