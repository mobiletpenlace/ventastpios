//
//  BeerPresenter.swift
//  ventastp
//
//  Created by Ulises Ortega on 09/03/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//
import UIKit
import ObjectMapper
import Alamofire
import SwiftyJSON
import SwiftEventBus

protocol BeersDelegate: NSObjectProtocol {
    func OnSuccesBeers(mBeers: BeersResponse )
    func OnSuccessCoverage(mCoverage: BeerCoverageResponse)
}

class BeerPresenter: BaseVentastpredesignPresenter {
    weak var mBeersDelegate: BeersDelegate?
    var request: Alamofire.Request?

    override func onRequestWs() {}

    func validateCoverage(mBeersDelegate: BeersDelegate?) {
        self.mBeersDelegate = mBeersDelegate
        let latitud = String(faseDataConsult.direction.Latitude)
        let longitud = String(faseDataConsult.direction.Longitude)
        let parameters: Parameters = [
            "latitude": latitud,
            "longitude": longitud
        ]
        if ConnectionUtils.isConnectedToNetwork() {
            Alamofire.request(AppDelegate.API_TOTAL + ApiDefinition.API_MODELO_COVERAGE, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: ApiDefinition.CUSTOM_HEADERS).responseJSON {[weak self] response in
                guard let self = self else {return}
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    if let coverage = BeerCoverageResponse(JSONString: json.rawString() ?? "") {
                        self.OnSuccessCoverage(coverageResponse: coverage)
                    }
                case .failure(let error):
                    print(error)
                }
            }
        } else {
            self.onErrorConnection()
        }
    }

    func getBeers(mBeersDelegate: BeersDelegate) {
//        self.mBeersDelegate = mBeersDelegate
        if ConnectionUtils.isConnectedToNetwork() {

            // Connection success fetching data
            Alamofire.request(AppDelegate.API_TOTAL + ApiDefinition.API_MODELO_PRODUCTS, method: .post, headers: ApiDefinition.CUSTOM_HEADERS).responseJSON { [weak self] response in
                guard let self = self else {return}
                print(response)
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    if let respuestaBeer = BeersResponse(JSONString: json.rawString() ?? "") {
                        self.OnSuccessBeers(beerResponse: respuestaBeer)
                    }

                case .failure(let error):
                    print("Error: \(error)")
                }
            }
        } else {
            self.onErrorConnection()
        }
    }

    func OnSuccessCoverage(coverageResponse: BeerCoverageResponse) {
        if coverageResponse.checkCoverage == "true" {
            mBeersDelegate?.OnSuccessCoverage(mCoverage: coverageResponse)
        } else {
            view.hideLoading()
            // Constants.Alert(title: "", body: dialogs.Error.InternService, type: type.Error, viewC: mViewController)
        }
    }

    func OnSuccessBeers(beerResponse: BeersResponse) {
        if beerResponse.getProducts?.count ?? 0 > 0 {
            mBeersDelegate?.OnSuccesBeers(mBeers: beerResponse)
        } else {
            view.hideLoading()
        }
    }
}
