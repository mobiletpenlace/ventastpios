//
//  StepContainer.swift
//  ventastpredesign
//
//  Createda by Armando Isais Olguin Cabrera on 30/04/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

    import UIKit
    import SwiftEventBus
    import PKHUD
    import Amplitude

    class StepContainer: BaseVentasView {

    @IBOutlet weak var mMainContainerView: UIView!
    @IBOutlet weak var line1View: UIView!
    @IBOutlet weak var line2View: UIView!
    @IBOutlet weak var numberOneView: UIView!
    @IBOutlet weak var numberOneLabel: UILabel!
    @IBOutlet weak var numberTwoView: UIView!
    @IBOutlet weak var numberTwoLabel: UILabel!
    @IBOutlet weak var numberThreeView: UIView!
    @IBOutlet weak var numberThreeLabel: UILabel!
    @IBOutlet weak var separatorFooterView: UIView!
    @IBOutlet weak var resumeView: UIView!
    @IBOutlet weak var textButonLabel: UILabel!
    @IBOutlet weak var middleButtonView: UIView!
    @IBOutlet weak var middleButton: UIButton!
    @IBOutlet weak var rightButtonView: UIView!
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var hiddenViewBottomConstraint: NSLayoutConstraint!

    // RESUME
    @IBOutlet weak var titlePlanLabel: UILabel!
    @IBOutlet weak var priceResumeLabel: UILabel!
    @IBOutlet weak var closeResumeButtton: UIButton!
    @IBOutlet weak var showResumeButton: UIButton!
    @IBOutlet weak var megasLabel: UILabel!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var primerPagoLabel: UILabel!
    @IBOutlet weak var prontoPagoLabel: UILabel!
    @IBOutlet weak var precioPlanLabel: UILabel!
    @IBOutlet weak var precioAdicionalesLabel: UILabel!
    @IBOutlet weak var precioListaLabel: UILabel!

    var addonsRequest: ConsultaAddonsRequest!
    var isCheck = false
    var isCheckPlan = false
    var mRecommendedPlanViewModel: RecommendedPlanViewModel!
    var cosas: Info2!
    var planLocal: String!
    var bandera = false
    var megas: String = ""
    var planBestFit: String = ""
    let checkImage = UIImage(named: "Image_rede_blue_check.png")
    let myImageView: UIImageView = UIImageView()

    override func viewDidLoad() {
        hideResume()
        super.viewDidLoad()
        super.setView(view: view)
        mRecommendedPlanViewModel = RecommendedPlanViewModel(view: self)
        mRecommendedPlanViewModel.atachView(self)
        mRecommendedPlanViewModel.delegateAddonsRequest = self
        if faseConfigurarPlan.isNegocio == true {
            hideSecondPlan()
        } else {
            if faseConfigurarPlan.adivinaQuien == false {
                mRecommendedPlanViewModel.getInfoCliente(typePlan: "Residencial", action: "Creacion")
            }
        }
        numberOneView.layer.cornerRadius = numberOneView.frame.size.width/2
        numberTwoView.layer.cornerRadius = numberTwoView.frame.size.width/2
        numberThreeView.layer.cornerRadius = numberThreeView.frame.size.width/2
        // Set de pantalla inicial en stepcontainer
        selectModule(index: 0)

        if faseConfigurarPlan.isNegocio == false {
        }

        SwiftEventBus.onMainThread(self, name: "receiveIDPlanbBestFit") { [weak self] result in
            self?.mRecommendedPlanViewModel.getDetallePlanesBestFit(plan: (result?.object as? String)!)
            self?.unregister()
        }

}
        override func viewWillDisappear(_ animated: Bool) {
            mRecommendedPlanViewModel.removeEventBus()
        }

        func unregister() {
            SwiftEventBus.unregister(self)
        }

        func hideRightButton(show: Bool) {
            if show == true {
                rightButtonView.isHidden = true
                rightButton.isHidden = true
            } else {
                rightButtonView.isHidden = false
                rightButton.isHidden = false
            }
        }

        @IBAction func gestoUPAction(_ sender: Any) {
            showResume()
        }

        @IBAction func gestoDownAction(_ sender: Any) {
            if isCheck {
                hideResume()
            } else {
                showResume()
            }
        }

        @IBAction func onClickShowresume(_ sender: Any) {
            showResume()
        }

        @IBAction func closeResumeAction(_ sender: Any) {
            hideResume()
        }

        func showResume() {
            showResumeButton.isHidden = true
            UIView.animate(withDuration: 0.5) {() -> Void in
                self.hiddenViewBottomConstraint.constant = 0
                self.megasLabel.isHidden = false
                self.separatorView.isHidden = false
                self.priceResumeLabel.isHidden = true
                self.closeResumeButtton.isHidden = false
                self.titlePlanLabel.frame = CGRect(origin: CGPoint(), size: CGSize(width: 300, height: 50))
                self.titlePlanLabel.frame = CGRect(origin: CGPoint(x: 100, y: 100), size: CGSize(width: 400, height: 300))
    //            (self.titlePlanLabel.width * 0.2) + self.titlePlanLabel.width
                self.view.layoutIfNeeded()
            }
            isCheck = !isCheck
        }

    func hideResume() {
            showResumeButton.isHidden = false
            UIView.animate(withDuration: 0.5) {() -> Void in
                self.hiddenViewBottomConstraint.constant = -315// self.resumeView.frame.height * 1.2
                self.separatorView.isHidden = true
                self.megasLabel.isHidden = false
                self.priceResumeLabel.isHidden = false
                self.closeResumeButtton.isHidden = true

                self.view.layoutIfNeeded()
            }
            isCheck = !isCheck
        }

    func hideMiddleButton(show: Bool) {
            if show == true {
                middleButtonView.isHidden = true
                middleButton.isHidden = true
            } else {
                middleButtonView.isHidden = false
                middleButton.isHidden = false
            }

        }
    func hideSecondPlan() {
                separatorFooterView.isHidden = true
                rightButtonView.frame.origin.y = 100
                rightButton.frame.origin.y = 100
            }

        override func viewDidAppear(_ animated: Bool) {
            if faseConfigurarPlan.adivinaQuien == true {
                hideSecondPlan()
            }

        }

        func setResumeLabels(totalProductos: Int, Totaldescuentos: Int, TotalCostoInstalacion: Float) {
        let numberFormatter = NumberFormatter()
            if faseSelectedPlan.firstPlan == true {
                        titlePlanLabel.text = faseSelectedPlan.plan1.Titulo
                        let currencyFormatter = NumberFormatter()
                        currencyFormatter.usesGroupingSeparator = true
                        currencyFormatter.numberStyle = .currency
                        currencyFormatter.locale = Locale.current
                        priceResumeLabel.text = currencyFormatter.string(from: NSNumber(value: (faseSelectedPlan.plan1.precioProntoPago + totalProductos - Totaldescuentos + Int(TotalCostoInstalacion))))! + " /mes"
                        megasLabel.text = faseSelectedPlan.plan1.Megas + " Megas"
                        primerPagoLabel.text = currencyFormatter.string(from: NSNumber(value: Int(TotalCostoInstalacion) + faseSelectedPlan.plan1.precioProntoPago + totalProductos - Totaldescuentos))! + " /mes"
                        prontoPagoLabel.text = currencyFormatter.string(from: NSNumber(value: faseSelectedPlan.plan1.precioProntoPago + totalProductos - Totaldescuentos))
                        precioPlanLabel.text = currencyFormatter.string(from: NSNumber(value: faseSelectedPlan.plan1.precioProntoPago))
                        precioAdicionalesLabel.text  = currencyFormatter.string(from: NSNumber(value: totalProductos - Totaldescuentos))!
                precioListaLabel.text = "El precio ''pronto pago'' es el total a pagar con un descuento aplicado mensualmente por pago puntual, generalmente 10 días antes de la fecha límite de pago.\nSi pagas después de tu periodo pronto pago, el precio de lista para este paquete es de " + currencyFormatter.string(from: NSNumber(value: faseSelectedPlan.plan2.precioLista))!
                    } else {
                        titlePlanLabel.text = faseSelectedPlan.plan2.Titulo
                        let currencyFormatter = NumberFormatter()
                        currencyFormatter.usesGroupingSeparator = true
                        currencyFormatter.numberStyle = .currency
                        currencyFormatter.locale = Locale.current
                        priceResumeLabel.text = currencyFormatter.string(from: NSNumber(value: (faseSelectedPlan.plan2.precioProntoPago + totalProductos - Totaldescuentos + Int(TotalCostoInstalacion))))! + " /mes"
                        megasLabel.text = faseSelectedPlan.plan2.Megas + " Megas"
                        primerPagoLabel.text = currencyFormatter.string(from: NSNumber(value: Int(TotalCostoInstalacion) + faseSelectedPlan.plan2.precioProntoPago + totalProductos - Totaldescuentos))! + " /mes"
                        prontoPagoLabel.text = currencyFormatter.string(from: NSNumber(value: faseSelectedPlan.plan2.precioProntoPago + totalProductos - Totaldescuentos))
                        precioPlanLabel.text = currencyFormatter.string(from: NSNumber(value: faseSelectedPlan.plan2.precioProntoPago))
                        precioAdicionalesLabel.text  = currencyFormatter.string(from: NSNumber(value: totalProductos - Totaldescuentos))!
                        precioListaLabel.text = String(faseSelectedPlan.plan2.precioLista)
                    }
    }

    @IBAction func Actionback(_ sender: Any) {
        switch fasePostPreguntas.Index {
        case 0:
            if fase.TypeHome == true {
                SwiftEventBus.post("Cambia_paso_container", sender: 3)
                SwiftEventBus.post("OmitirPregunta", sender: 6)
            } else {
                SwiftEventBus.post("Cambia_paso_container", sender: 4)
            }
        case 1:
            faseConfigurarPlan.vengodeBack = true
            if faseConfigurarPlan.adivinaQuien == true {
//                mRecommendedPlanViewModel.getDetallePlanesBestFit(plan: faseSelectedPlan.planNegocioID)
            } else {
                mRecommendedPlanViewModel.getDetallePlanes()
            }
            selectModule(index: 0)
        case 2:
            selectModule(index: 1)
        default:
            Constants.Back(viewC: self)
        }
    }

    func selectModule(index: Int) {
        switch index {
        case 0:
            if faseConfigurarPlan.isNegocio == true {
                hideRightButton(show: false)
                separatorFooterView.isHidden = true
                resumeView.isHidden = true
                hideMiddleButton(show: true)
            } else {
                hideRightButton(show: false)
                separatorFooterView.isHidden = false
                resumeView.isHidden = true
                hideMiddleButton(show: true)
            }
            ViewEmbedder.embed(withIdentifier: "NewRecommendedPlanViewController", parent: self, container: mMainContainerView)
            fasePostPreguntas.Index = 0
            numberOneView.backgroundColor = UIColor(named: "Base_rede_blue_textfields_wait")
            numberOneLabel.text = "1"
            numberTwoView.backgroundColor = UIColor(named: "Base_rede_semiclear_gray")
            numberTwoLabel.textColor = UIColor.white
            line1View.backgroundColor = UIColor(named: "Base_rede_semiclear_gray")
        case 1:
            ViewEmbedder.embed(withIdentifier: "AddonsScrollView", parent: self, container: mMainContainerView)
            fasePostPreguntas.Index = 1
            numberOneView.backgroundColor = UIColor(named: "Base_rede_exit_green")
            numberOneLabel.text = "✓"
            line1View.backgroundColor = UIColor(named: "Base_rede_exit_green")
            numberTwoView.backgroundColor = UIColor(named: "Base_rede_blue_textfields_wait")
            numberTwoLabel.text = "2"
            numberThreeView.backgroundColor = UIColor(named: "Base_rede_semiclear_gray")
            numberThreeLabel.textColor = UIColor.white
            line2View.backgroundColor = UIColor(named: "Base_rede_semiclear_gray")
            separatorFooterView.isHidden = true
            resumeView.isHidden = false
            textButonLabel.text = "¡Ultimo paso!"
            hideMiddleButton(show: false)
            hideRightButton(show: true)
            SwiftEventBus.post("sendRequest", sender: addonsRequest)
        case 2:
            SwiftEventBus.post("GuardaAddons")
        default:
            break
        }
    }

    func setDataView() {
        ViewEmbedder.embed(withIdentifier: "DataRedesignView", parent: self, container: mMainContainerView)
        fasePostPreguntas.Index = 2
        numberTwoView.backgroundColor = UIColor(named: "Base_rede_exit_green")
        numberTwoLabel.text = "✓"
        line2View.backgroundColor = UIColor(named: "Base_rede_exit_green")
        separatorFooterView.isHidden = true
        textButonLabel.text = "Finalizar venta"
    }

    @IBAction func nextViewAction(_ sender: Any) {
        faseSelectedPlan.firstPlan = true
        setResumeLabels(totalProductos: mRecommendedPlanViewModel.getTotalProductos(), Totaldescuentos: mRecommendedPlanViewModel.getTotalDiscount(), TotalCostoInstalacion: mRecommendedPlanViewModel.getCosto())
        switch fasePostPreguntas.Index {
        case 0:
            mRecommendedPlanViewModel.BuscaAdoons()
        case 1:
            selectModule(index: 2)
        case 2:
            if faseDataOK.isOK == true {
                SwiftEventBus.post("EnviaData_DataVC")
                SwiftEventBus.unregister(self)
                SwiftEventBus.post("RemoveEventBus")
            } else {
                Constants.Alert(title: "Alerta", body: "Llena todos los campos para continuar", type: "normal", viewC: self)
            }

            SwiftEventBus.post("CheckFields")
        default:
            break
        }
    }

    @IBAction func closeSellAction(_ sender: Any) {
        let closeButton: PopUPClose = UIStoryboard(name: "PopUPClose", bundle: nil).instantiateViewController(withIdentifier: "PopUPClose") as! PopUPClose
        closeButton.providesPresentationContextTransitionStyle = true
        closeButton.definesPresentationContext = true
        closeButton.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        closeButton.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        UIApplication.topViewController()?.present(closeButton, animated: false, completion: nil)
    }

    override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
    }

        override func viewDidDisappear(_ animated: Bool) {
            faseConfigurarPlan.vengodeBack = false
        }

}
