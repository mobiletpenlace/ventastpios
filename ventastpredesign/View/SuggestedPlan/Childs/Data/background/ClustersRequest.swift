//
//  File.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 23/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

public class ClustersRequest: AlfaRequest {
    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override public func mapping(map: Map) {
        accion <- map["accion"]
    }

    var accion: String?
}
