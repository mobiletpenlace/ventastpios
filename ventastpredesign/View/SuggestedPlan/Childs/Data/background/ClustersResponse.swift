//
//  ClustersResponse.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 23/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper
import RealmSwift

class ClustersResponse: BaseResponse {

    public required convenience init?(map: Map) {
        self.init()
    }

    override func mapping(map: Map) {
        info <- map["info"]
    }

    var  info: InfoClusters?
}
