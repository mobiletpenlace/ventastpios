//
//  ClusterObject.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 23/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper
import RealmSwift

class ClusterObject: NSObject, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        documentosRequeridos <- map["documentosRequeridos"]
        cluster <- map["cluster"]
    }

    var  documentosRequeridos: [String] = []
    var  cluster: String?

}
