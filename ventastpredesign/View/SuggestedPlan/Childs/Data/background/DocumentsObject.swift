//
//  DocumentsObject.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 02/07/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation

class DocumentosObject: NSObject {

    var ineFront: String!
    var ineBack: String!
    var proofDirection: String!
}
