//
//  DataCDFIViewController.swift
//  ventastp
//
//  Created by aestevezn on 22/02/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import UIKit

protocol dataCDFIObserver {
    func setValuesCDFI(rfc: String, typePerson: String)
}

class DataCDFIViewController: BaseVentasView {

    var modelToken = TokenRFC()
    var token: String!
    var typePerson: String = "F"
    var usosCDFIlist: [String] = []
    var validateRFCStatus: DataResponseRFCStruct!
    var modelValidateRFC = ValidateRFCModel()
    var userRFCStruct: UserRFCStruct!
    var observer: dataCDFIObserver!
    var isValidateRFC = false

    @IBOutlet weak var cortinaView: UIView!
    @IBOutlet weak var personaFisicaButton: UIButton!
    @IBOutlet weak var personaMoralButton: UIButton!
    @IBOutlet weak var RazonName: UILabel!
    @IBOutlet weak var razonSocialView: UIView!
    @IBOutlet weak var separatorRazonSocial: UIView!
    @IBOutlet weak var guardarButton: UIButton!
    @IBOutlet weak var regimenButtonOptions: UIButton!
    @IBOutlet weak var usoOptionsButton: UIButton!

    @IBOutlet weak var regimenValue: UILabel!
    @IBOutlet weak var usoCDFIValue: UILabel!
    @IBOutlet weak var razonNameValue: UITextField!
    @IBOutlet weak var razonSocialValue: UITextField!
    @IBOutlet weak var rfcValue: UITextField!
    @IBOutlet weak var codigoPostalValue: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        self.setUpView()
        modelToken.delegate = self
        modelValidateRFC.delegate = self
        modelToken.getToken()
        userRFCStruct = UserRFCStruct()
    }

    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true)
    }

    @IBAction func showOptionsRegimen(_ sender: Any) {
        let controller = RegimensOptionViewController(token: token, type: typePerson)
        controller.delegate = self
        self.present(controller, animated: true)
    }

    @IBAction func showOptionsUsos(_ sender: Any) {
        if usosCDFIlist.count == 0 {
            let alert = UIAlertController(title: "!Cuidado!", message: "Debes de seleccionar un regimen para continuar", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Aceptar", style: .cancel))
            self.present(alert, animated: true)
        } else {
            let controller = UsosCDFIOptionsViewController(usos: usosCDFIlist)
            controller.observer = self
            self.present(controller, animated: true)
        }
    }

    @IBAction func ChangePersonaFisicaData(_ sender: Any) {
        self.RazonName.text = "Nombre completo del cliente *"
        self.typePerson = "F"
        self.personaMoralButton.layer.borderWidth = 0
        self.updateButtonDeselected(button: personaMoralButton)
        self.updateButtonSelected(button: personaFisicaButton)
        self.resetValues()
        if(!self.razonSocialView.isHidden) && (!self.separatorRazonSocial.isHidden) {
            self.razonSocialView.isHidden = true
            self.separatorRazonSocial.isHidden = true
        }
    }

    @IBAction func changePersonaMoralData(_ sender: Any) {
        self.RazonName.text = "Régimen de capital *"
        self.typePerson = "M"
        self.personaFisicaButton.layer.borderWidth = 0
        self.updateButtonDeselected(button: personaFisicaButton)
        self.updateButtonSelected(button: personaMoralButton)
        self.resetValues()
        if(self.razonSocialView.isHidden) && (self.separatorRazonSocial.isHidden) {
            self.razonSocialView.isHidden = false
            self.separatorRazonSocial.isHidden = false
        }
    }

    @IBAction func sendValidateRFCAction(_ sender: Any) {
        if self.validateTextField() {
            userRFCStruct.rfc = rfcValue.text?.uppercased()
            userRFCStruct.nombre = razonNameValue.text?.uppercased()
            userRFCStruct.cp = codigoPostalValue.text
            cortinaView.isHidden = false
            modelValidateRFC.makeResponse(rfcUser: userRFCStruct, token: token)
        } else {
            let alert = UIAlertController(
                title: "¡Advertencia!",
                message: "Por favor ingresa todos los datos para continuar",
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "Aceptar", style: .cancel))
            self.present(alert, animated: true)
        }
    }
}

extension DataCDFIViewController: GetTokenProtocol {

    func onSucess(response: TokenCDFI) {
        self.token = response.access_token
    }

    func onError() {
        print("🚨 Ha ocurrido un error obteniendo el token")
    }
}

extension DataCDFIViewController: optionCDFISelectedObserver {

    func setRegimenValue(regimen: String) {
        regimenValue.text = regimen
    }

    func setUsosCDFIList(usos: [String]) {
        usosCDFIlist = usos
        if usosCDFIlist.count != 0 {
            self.usoCDFIValue.text = usosCDFIlist[0]
        }
    }
}

extension DataCDFIViewController: usosCDFIObserver {
    func setValue(usoCDFI: String) {
        self.usoCDFIValue.text = usoCDFI
    }
}

extension DataCDFIViewController: validateRFCProtocol {
    func onSucess(response: DataListRFCResponse) {
        isValidateRFC = response.data[0].statusRFC
        let rfcResponse = response.data[0].rfc!
        if isValidateRFC {
            observer?.setValuesCDFI(rfc: rfcResponse, typePerson: self.typePerson)
            DispatchQueue.main.async {
                self.cortinaView.isHidden = true
                self.makeSussesAlert()
            }
        } else {
            self.makeErrorAlert(
                message: "Ha ocurrdo un error al validar tus datos\n Por favor intenta mas tarde"
            )
        }
    }

    func onError(error: String) {
        print(error)
    }

}
