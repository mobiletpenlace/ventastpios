//
//  optionCFDITableViewCell.swift
//  ventastp
//
//  Created by aestevezn on 01/02/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

protocol optionCDFISelectedObserver {
    func setRegimenValue(regimen: String)
    func setUsosCDFIList(usos: [String])
}

protocol optionCellCDFIObserver {
    func updateValue(valueSelected: String)
    func cellUpdate(cell: OptionCFDITableViewCell)
}

class OptionCFDITableViewCell: UITableViewCell {

    @IBOutlet weak var titleCDFILabel: UILabel!
    @IBOutlet weak var buttonCheck: UIButton!

    var delegate: optionCDFISelectedObserver!
    var managerObserver: optionCellCDFIObserver!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setData(regimen: String) {
        self.titleCDFILabel.text? = regimen
    }

    @IBAction func selectRegimenAction(_ sender: Any) {
        let newValue = self.titleCDFILabel.text
        buttonCheck.setImage(UIImage(named: "icon_radiobutton_on"), for: .normal)
        managerObserver?.cellUpdate(cell: self)
        managerObserver?.updateValue(valueSelected: newValue!)
    }

}
