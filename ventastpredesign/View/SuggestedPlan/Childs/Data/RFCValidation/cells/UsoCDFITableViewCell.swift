//
//  UsoCDFITableViewCell.swift
//  ventastp
//
//  Created by aestevezn on 09/03/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

protocol usoCDFICellObserver {
    func setValue(uso: String)
    func cellUpdate(cell: UsoCDFITableViewCell)
}

class UsoCDFITableViewCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var button: UIButton!

    var observer: usoCDFICellObserver!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setData(uso: String) {
        self.label.text = uso
    }

    @IBAction func selectedValueAction(_ sender: Any) {
        let newValue = self.label.text
        self.button.setImage(UIImage(named: "icon_radiobutton_on"), for: .normal)
        observer?.cellUpdate(cell: self)
        observer?.setValue(uso: newValue!)
    }

}
