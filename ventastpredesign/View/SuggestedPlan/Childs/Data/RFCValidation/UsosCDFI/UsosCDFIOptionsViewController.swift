//
//  UsosCDFIOptionsViewController.swift
//  ventastp
//
//  Created by aestevezn on 09/03/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

protocol usosCDFIObserver {
    func setValue(usoCDFI: String)
}

class UsosCDFIOptionsViewController: UIViewController {

    var usosList: [String]!
    var observer: usosCDFIObserver!
    var isSelect = false
    var cellSelected: UsoCDFITableViewCell!

    @IBOutlet weak var buttonSelect: UIButton!
    @IBOutlet weak var cortina: UIView!
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        tableView.delegate = self
        tableView.dataSource = self
        configuereTable()
        cortina.isHidden = true
    }

    init(usos: [String]) {
        self.usosList = usos
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    @IBAction func selectedButtonAction(_ sender: Any) {
        self.dismiss(animated: true)
    }

}

extension UsosCDFIOptionsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return usosList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "usoCDFICell", for: indexPath) as? UsoCDFITableViewCell else {
            return UITableViewCell()
        }
        let uso = usosList[indexPath.row]
        cell.setData(uso: uso)
        cell.observer = self
        return cell
    }

    func configuereTable() {
        let nibName = UINib(nibName: "UsoCDFITableViewCell", bundle: nil)
        self.tableView.rowHeight = 65
        self.tableView.register(nibName, forCellReuseIdentifier: "usoCDFICell")
        self.tableView.reloadData()
    }

}

extension UsosCDFIOptionsViewController: usoCDFICellObserver {
    func setValue(uso: String) {
        observer.setValue(usoCDFI: uso)
    }

    func cellUpdate(cell: UsoCDFITableViewCell) {
        if isSelect {
            cellSelected.button.setImage(UIImage(named: "icon_radiobutton_off"), for: .normal)
        }
        isSelect = true
        cellSelected = cell
    }

}
