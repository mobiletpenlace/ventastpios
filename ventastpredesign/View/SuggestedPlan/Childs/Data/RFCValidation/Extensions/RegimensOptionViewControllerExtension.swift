//
//  RegimensOptionViewControllerExtension.swift
//  ventastp
//
//  Created by aestevezn on 23/02/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import UIKit

extension RegimensOptionViewController {

    func getDescriptionList(array: [ConsultRegime]) -> [String] {
        var aux: [String] = []
        array.forEach { description in
            aux.append(description.Description)
        }
        return aux
    }

    func setUpView() {
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
        self.container.layer.cornerRadius = 15
        self.buttonSelect.layer.cornerRadius = 15
        self.buttonSelect.setTitle("Seleccionar", for: .normal)
        self.buttonSelect.setTitleColor(UIColor.white, for: .normal)

    }

    func getUsosCDFI(regimenString: String) -> [String] {
        var aux: [String] = []
        regimens.forEach { regimen in
            if regimen.Description.contains(string: regimenString) {
                regimen.ConsultCase.forEach { description in
                    aux.append(description.Description)
                }
            }
        }
        return aux
    }
}
