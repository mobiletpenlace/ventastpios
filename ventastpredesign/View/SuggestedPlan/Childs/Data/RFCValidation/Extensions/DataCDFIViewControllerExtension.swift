//
//  DataCDFIViewControllerExtension.swift
//  ventastp
//
//  Created by aestevezn on 22/02/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import UIKit

extension DataCDFIViewController {

    func setUpView() {
        self.personaFisicaButton.layer.cornerRadius = 15
        self.personaMoralButton.layer.cornerRadius = 15
        self.guardarButton.layer.cornerRadius = 15

        self.regimenButtonOptions.setTitle("", for: .normal)
        self.regimenButtonOptions.setTitle("", for: .selected)

        self.usoOptionsButton.setTitle("", for: .normal)
        self.usoOptionsButton.setTitle("", for: .selected)

        self.razonSocialView.isHidden = true
        self.separatorRazonSocial.isHidden = true

        self.cortinaView.isHidden = true
    }

    func updateButtonSelected(button: UIButton) {
        button.backgroundColor = UIColor(named: "Base_rede_blue_textfields_wait")
        button.setTitleColor(.white, for: .normal)
    }

    func updateButtonDeselected(button: UIButton) {
        button.backgroundColor = UIColor(named: "base_gray_lines")
        button.setTitleColor(UIColor(named: "Base_rede_blue_textfields_wait"), for: .normal)
    }

    func validateTextField() -> Bool {
        var isOK = false
        if typePerson == "M" {
            if (regimenValue.text != "") && (usoCDFIValue.text != "") && (razonNameValue.text != "")
                && (codigoPostalValue.text != "") && (rfcValue.text != "" ) && (razonSocialValue.text != "") {
                isOK = true
            }
        } else {
            if (regimenValue.text != "") && (usoCDFIValue.text != "") && (razonNameValue.text != "")
                && (codigoPostalValue.text != "") && (rfcValue.text != "" ) {
                isOK = true
            }
        }
        return isOK
    }

    func resetValues() {
        self.razonNameValue.text = ""
        self.codigoPostalValue.text = ""
        self.rfcValue.text = ""
        self.razonSocialValue.text = ""
    }

    func makeSussesAlert() {
        let alert = UIAlertController(
            title: "¡Exito!",
            message: "Los datos han sido guardados",
            preferredStyle: .alert
        )
        alert.addAction(UIAlertAction(title: "Aceptar", style: .cancel, handler: {(_) in
            self.dismiss(animated: true)
        }))
        self.present(alert, animated: true)
    }

    func makeErrorAlert(message: String) {
        let alert = UIAlertController(
            title: "¡Error!",
            message: message,
            preferredStyle: .alert
        )
        alert.addAction(UIAlertAction(title: "Aceptar", style: .cancel))
        self.present(alert, animated: true)
    }
}
