//
//  UsosCDFIOptionsViewControllerExtension.swift
//  ventastp
//
//  Created by aestevezn on 09/03/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation

extension UsosCDFIOptionsViewController {

    func setUpView() {

        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }

        self.container.layer.cornerRadius = 15

        self.buttonSelect.setTitle("Seleccionar", for: .normal)
        self.buttonSelect.layer.cornerRadius = 15
        self.buttonSelect.setTitleColor(.white, for: .normal)
    }
}
