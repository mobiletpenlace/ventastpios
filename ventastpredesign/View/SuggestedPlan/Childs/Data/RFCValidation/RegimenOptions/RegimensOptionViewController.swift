//
//  RegimensOptionViewController.swift
//  ventastp
//
//  Created by aestevezn on 06/03/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

class RegimensOptionViewController: UIViewController {

    var token: String!
    var type: String!
    var regimenList: [String] = []
    var modelRegimenList: ConsultaRegimen!
    var delegate: optionCDFISelectedObserver!
    var cellSelected: OptionCFDITableViewCell!
    var isSelect = false
    var regimens: [ConsultRegime] = []
    var usosCDFIList: [String] = []

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var cortina: UIView!
    @IBOutlet weak var buttonSelect: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpView()
        modelRegimenList = ConsultaRegimen(token: token, type: type)
        modelRegimenList.delagate = self
        modelRegimenList.makeResponse()
        tableView.delegate = self
        tableView.dataSource = self
    }

    init(token: String, type: String) {
        self.token = token
        self.type = type
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    @IBAction func closeViewAction(_ sender: Any) {
        delegate.setUsosCDFIList(usos: usosCDFIList)
        self.dismiss(animated: true)
    }

}

extension RegimensOptionViewController: consultRegimenProtocol {

    func onSucess(response: ConsultRegimeResponse) {
        regimenList = self.getDescriptionList(array: response.ConsultRegime)
        regimens = response.ConsultRegime

        DispatchQueue.main.async {
            self.configureTable()
            self.cortina.isHidden = true
        }
    }

    func onError(error: String) {
        print("🚨 Ha ocurrido un error al obtener datos")
        print(error)
    }
}

extension RegimensOptionViewController: optionCellCDFIObserver {

    func updateValue(valueSelected: String) {
        delegate.setRegimenValue(regimen: valueSelected)
        usosCDFIList = getUsosCDFI(regimenString: valueSelected)
    }

    func cellUpdate(cell: OptionCFDITableViewCell) {
        if isSelect {
            cellSelected.buttonCheck.setImage(UIImage(named: "icon_radiobutton_off"), for: .normal)
        }
        isSelect = true
        cellSelected = cell
    }
}

extension RegimensOptionViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return regimenList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "regimenCell", for: indexPath) as? OptionCFDITableViewCell else {
            return UITableViewCell()
        }
        let regimen = regimenList[indexPath.row]
        cell.setData(regimen: regimen)
        cell.managerObserver = self
        return cell
    }

    func configureTable() {
        let nibName = UINib(nibName: "optionCFDITableViewCell", bundle: nil)
        self.tableView.rowHeight = CGFloat(65)
        self.tableView.register(nibName, forCellReuseIdentifier: "regimenCell")
        self.tableView.reloadData()
    }

}
