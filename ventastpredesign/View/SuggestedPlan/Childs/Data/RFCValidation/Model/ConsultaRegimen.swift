//
//  ConsultRegimen.swift
//  ventastp
//
//  Created by aestevezn on 23/02/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation

protocol consultRegimenProtocol {
    func onSucess(response: ConsultRegimeResponse)
    func onError(error: String)
}

struct RegimenCase: Decodable {
    var Case: String!
    var Description: String!

    enum CodeKeysRegimenCase: String, CodingKey {
        case case_option = "Case"
        case description = "Description"
    }
}

struct ConsultRegime: Decodable {
    var Regime: String!
    var Description: String!
    var ConsultCase: [RegimenCase]!

    enum CodekeysConsultRegime: String, CodingKey {
        case regime = "Regime"
        case description = "Description"
        case consultCase = "ConsultCase"
    }
}

struct ResultsStruct: Decodable {
    var Results: String!
    var ResultIdentifier: String!
    var ResultDescription: String!

    enum CodeKeysResults: String, CodingKey {
        case results = "Result"
        case resultIdentifier = "ResultIdentifier"
        case resultDescription = "ResultDescription"
    }
}

struct ConsultRegimeResponse: Decodable {
    var ConsultRegime: [ConsultRegime]

    enum CodeKeysConsultRegimeResponse: String, CodingKey {
        case consultRegime = "ConsultRegime"
    }
}

struct ErrorRegimenResponse: Decodable {
    var fault: ErrorDescribeResponse
}

struct ErrorDescribeResponse: Decodable {
    var faultstring: String!
}

class ConsultaRegimen {
    var delagate: consultRegimenProtocol!
    private var token: String!
    private var type: String!

    init(token: String, type: String) {
        self.token = token
        self.type = type
    }

    func setToken(token: String) {
        self.token = token
    }

    func setType(type: String) {
        self.type = type
    }

    func makeResponse() {
        let urlString = "https://apiservice-dev.sistemastp.com.mx/soakio/regimenFiscal"
        var url = URLComponents(string: urlString)!
        let queryItemType = URLQueryItem(name: "type", value: self.type)
        let queryItemStatus = URLQueryItem(name: "status", value: "1")
        url.queryItems = [queryItemType, queryItemStatus]

        var request = URLRequest(url: url.url!)
        request.httpMethod = "GET"
        request.setValue("Bearer \(token!)", forHTTPHeaderField: "Authorization")

        let task = URLSession.shared.dataTask(with: request) { data, _, error in
            if error != nil {
                print("🚨 Ha ocuerrido un error: \(error!)")
                self.delagate?.onError(error: "errorString")
            }

            if let data = data {
                if let responseData = try? JSONDecoder().decode(ConsultRegimeResponse.self, from: data) {
                    self.delagate?.onSucess(response: responseData)
                } else {
                    self.delagate?.onError(error: "Error decodificando los datos")
                }
            }
        }

        task.resume()

    }
}
