//
//  TokenRFC.swift
//  ventastp
//
//  Created by aestevezn on 22/02/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation

struct TokenCDFI: Decodable {
    var access_token: String!
}

protocol GetTokenProtocol {
    func onSucess(response: TokenCDFI)
    func onError()
}

class TokenRFC {

    var delegate: GetTokenProtocol!

    func getToken() {

        let user = "CqoHBZOGnysq5WPhTX2G34YX95VfyCH9mYxUkrnlxsoaZicy"
        let pass = "D9QefV0gcmKXOGgAdxqtQSA0QmA7wNKA5hNDnxsJcaqiNTjPiqQHSOF0ELQWpKJe"
        let basicCredentials = "\(user):\(pass)".data(using: .utf8)!.base64EncodedString()
        let headers = [
            "Authorization": "Basic \(basicCredentials)",
             "Content-Type": "application/json"
        ]
        let urlString = "https://apiservice-dev.sistemastp.com.mx/oauth/client_credential/accesstoken?grant_type=client_credentials"
        let url = URL(string: urlString)!

        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers

        let task = URLSession.shared.dataTask(with: request) { data, _, error in

            if error != nil {
                print("🚨 Error \(String(describing: error))")

            }

            if let data = data {
                if let token = try? JSONDecoder().decode(TokenCDFI.self, from: data) {
                    self.delegate?.onSucess(response: token)
                } else {
                    self.delegate?.onError()
                }
            }
        }

        task.resume()

    }
}
