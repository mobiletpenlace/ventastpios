//
//  ValidateRFCModel.swift
//  ventastp
//
//  Created by aestevezn on 10/03/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation

struct UserRFCStruct: Codable {
    var rfc: String!
    var nombre: String!
    var cp: String!
}

struct ListaRFCStruct: Codable {
    var listaRFC: [UserRFCStruct] = []
}

struct DataResponseRFCStruct: Decodable {
    var rfc: String!
    var statusRFC: Bool!
    var statusNombre: Bool!
    var statusCP: Bool!
}

struct DataListRFCResponse: Decodable {
    var status: Int!
    var description: String!
    var data: [DataResponseRFCStruct]!
}

protocol validateRFCProtocol {
    func onSucess(response: DataListRFCResponse)
    func onError(error: String)
}

class ValidateRFCModel {

    var delegate: validateRFCProtocol!

    func makeResponse(rfcUser: UserRFCStruct, token: String) {

        let urlString = "https://apiservice-dev.sistemastp.com.mx/v1/landings/validaRFC"
        let url = URL(string: urlString)!
        var dataRequest = ListaRFCStruct()
        dataRequest.listaRFC.append(rfcUser)
        let data = try? JSONEncoder().encode(dataRequest)

        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = data

        let task = URLSession.shared.dataTask(with: request) {data, response, error in
            if error != nil {
                print("🚨 Error \(String(describing: error))")
            }

            if let data = data {
                let dataStr = String(decoding: data, as: UTF8.self)
                print(dataStr)
                if let response = try? JSONDecoder().decode(DataListRFCResponse.self, from: data) {
                    self.delegate?.onSucess(response: response)
                } else {
                    self.delegate.onError(error: "Ha ocurrido un error consultando status")
                }
            }
        }

        task.resume()
    }
}
