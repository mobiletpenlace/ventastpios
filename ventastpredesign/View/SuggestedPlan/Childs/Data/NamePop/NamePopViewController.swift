//
//  NamePopViewController.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 01/07/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus
import MaterialComponents.MDCFilledTextField

protocol sendDataToFormFromCardName {
    func receiveDataName()
}

class NamePopViewController: BaseVentasView {

    @IBOutlet weak var firstNameTextField: MDCFilledTextField!
    @IBOutlet weak var middleNameTextField: MDCFilledTextField!
    @IBOutlet weak var lastNameTextField: MDCFilledTextField!
    var mNameObject: nameObject!
    var manager: sendDataToFormFromCardName?

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        configTextFields()
    }

    override func viewDidDisappear(_ animated: Bool) {
        SwiftEventBus.unregister(self, name: "SetName_Data")
    }

    @IBAction func saveNameAction(_ sender: Any) {
        if firstNameTextField.text != "" && middleNameTextField.text != "" && lastNameTextField.text != "" {
            mNameObject = nameObject()
            mNameObject.name = firstNameTextField.text
            mNameObject.middleName = middleNameTextField.text
            mNameObject.lastName = lastNameTextField.text
            TPDataDocumentsShared.shared.mNameObject = mNameObject
            manager?.receiveDataName()
            self.dismiss(animated: false, completion: nil)
        } else {
            if firstNameTextField.text == "" {
                firstNameTextField.errorTheme(message: "*Obligatorio")
            }
            if middleNameTextField.text == "" {
                middleNameTextField.errorTheme(message: "*Obligatorio")
            }
            if lastNameTextField.text == "" {
                lastNameTextField.errorTheme(message: "*Obligatorio")
            }
        }
    }

    @IBAction func endEditName(_ sender: Any) {
        firstNameTextField.text = firstNameTextField.text?.removeExtraSpaces()
    }

    @IBAction func endEditMiddleName(_ sender: Any) {
        middleNameTextField.text = middleNameTextField.text?.removeExtraSpaces()
    }

    @IBAction func endEditLastName(_ sender: Any) {
        lastNameTextField.text = lastNameTextField.text?.removeExtraSpaces()
    }

    func configTextFields() {
        firstNameTextField.normalTheme(type: 2)
        middleNameTextField.normalTheme(type: 2)
        lastNameTextField.normalTheme(type: 2)
        if (TPDataDocumentsShared.shared.mNameObject != nil) {
            firstNameTextField.text = TPDataDocumentsShared.shared.mNameObject?.name
            lastNameTextField.text = TPDataDocumentsShared.shared.mNameObject?.lastName
            middleNameTextField.text = TPDataDocumentsShared.shared.mNameObject?.middleName
        }
    }

    @IBAction func exitButton(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }

    @IBAction func nameAction(_ sender: Any) {
        middleNameTextField.becomeFirstResponder()
    }

}
