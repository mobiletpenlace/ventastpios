//
//  DataDelegate.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 23/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus
import MaterialComponents.MaterialTextControls_FilledTextAreas
import MaterialComponents.MaterialTextControls_FilledTextFields

extension DataViewController: DataObserver {
    func showSpinnerLoader() {
    }

    func hideSpinnerLoader() {
    }

    func EnviaDataVC() {
        SwiftEventBus.post("Envia_data_toStart")
        SwiftEventBus.unregister(self, name: "Envia_data_toStart")
        SwiftEventBus.post("RemoveEventBus")
    }

    func setProof(image: String) {
        mDocs.proofDirection = image
        if image != "" {
            IDView.borderColor = UIColor(named: "Base_rede_exit_green")
            leadingProofImage.image = UIImage(named: "image_rede_ok")
            labelProof.text  = "¡Exito!"
            addProofButton.isUserInteractionEnabled = false
        }
    }

    func setName(nombre: nameObject) {
        mNameObject = nombre
        nameTextField.text = String("\(mNameObject.name!) \(mNameObject.middleName!) \(mNameObject.lastName!)")
        nameTextField.normalTheme(type: 2)
        faseDataConsult.isPaymentRiskConsulted = false
        checkFields()
    }

    func checkFields() {

        if phoneNumberValidated && emailValidated && !(nameTextField.text?.isEmpty ?? true) && !(bornDateTextField.text?.isEmpty ?? true) {
            if !isUploaded {
                idUploaded(isUploaded: false)
                IDView.borderColor = UIColor.red
                addIDLabel.textColor = UIColor.red
                self.contentScrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
                faseDataOK.isOK = false
            } else {
                debtorCheck()
                faseDataOK.isOK = true
            }
        } else {
            faseDataOK.isOK = false
        }
    }

    func enviaData() {
        mDocs.proofDirection = ""
        var fecha = bornDateTextField.text!
        let date = fecha.components(separatedBy: "/")
        let dia = date[0]
        let mes = date[1]
        let año = date[2]
        fecha = año + "-" + mes + "-" + dia
        mProspecto = NProspecto()
        mProspecto.firstName = mNameObject.name
        mProspecto.midleName = mNameObject.middleName
        mProspecto.lastName = mNameObject.lastName
        mProspecto.rfc = rfc
        mProspecto.fechaNacimiento = fecha
        mProspecto.telefono = firstNumberWithoutPrefix
        mProspecto.celular = firstNumberWithoutPrefix
        mProspecto.otroTelefono = extraNumberWithoutPrefix
        mProspecto.correo = emailTextField.text!
        mProspecto.otroCoreo = ""
        mProspecto.tipoPersona  = "Fisica"
        mProspecto.company = razonSocialTextField.text!
        SwiftEventBus.post("CargaInfoStep3", sender: mProspecto)
        SwiftEventBus.post("CargaDocumentos", sender: mDocs)
        SwiftEventBus.post("Valida_Oportunidad", sender: nil)
        faseDataOK.isSended = true
    }

    func message(message: String) {
        Constants.Alert(title: "Alerta", body: message, type: "normal", viewC: self, dimissParent: false)
    }

    func requiresComprobante(Comprobante: Bool) {
        if !Comprobante {
            requiresProf()
        }
    }

    func configTextFields() {
        typePersonTextField.normalTheme(type: 2)
        typeSectorTextField.normalTheme(type: 2)
        typeSubsectorTextField.normalTheme(type: 2)
        razonSocialTextField.normalTheme(type: 2)
        nameTextField.normalTheme(type: 2)
        bornDateTextField.normalTheme(type: 2)
        emailTextField.normalTheme(type: 2)
        phoneNumberTextField.normalTheme(type: 2)
        extraPhoneNumberTextField.normalTheme(type: 2)
    }

    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string) as? NSString

        let arrayOfString = newString?.components(separatedBy: ".")
        if arrayOfString!.count > 1 {
                return false
            }
            return true
        }

    @IBAction func optionsTypePeopleAction(_ sender: UIButton) {
        Constants.OptionCombo(origen: "Set_TipoPersona", array: Array(arrTipos), viewC: self)
    }

    @IBAction func optionsSectorAction(_ sender: UIButton) {
        Constants.OptionCombo(origen: "Set_Sector", array: Array(arrSector), viewC: self)
    }

    @IBAction func optionsSubsectorAction(_ sender: UIButton) {
        switch arrSector {
        case _ where ((typeSectorTextField.text!.contains("Animales y mascotas")) == true):
                Constants.OptionCombo(origen: "Set_Subsector", array: Array(arrSubsector1), viewC: self)
        case _ where ((typeSectorTextField.text!.contains("Asociación Civil")) == true):
                Constants.OptionCombo(origen: "Set_Subsector", array: Array(arrSubsector2), viewC: self)
        case _ where ((typeSectorTextField.text!.contains("Autos y transportes")) == true):
                Constants.OptionCombo(origen: "Set_Subsector", array: Array(arrSubsector3), viewC: self)
        case _ where ((typeSectorTextField.text!.contains("Belleza")) == true):
                Constants.OptionCombo(origen: "Set_Subsector", array: Array(arrSubsector4), viewC: self)
        case _ where ((typeSectorTextField.text!.contains("Bienes raíces")) == true):
                Constants.OptionCombo(origen: "Set_Subsector", array: Array(arrSubsector5), viewC: self)
        case _ where ((typeSectorTextField.text!.contains("Construcción")) == true):
                Constants.OptionCombo(origen: "Set_Subsector", array: Array(arrSubsector6), viewC: self)
        case _ where ((typeSectorTextField.text!.contains("Deportes")) == true):
                Constants.OptionCombo(origen: "Set_Subsector", array: Array(arrSubsector7), viewC: self)
        case _ where ((typeSectorTextField.text!.contains("Educación")) == true):
                Constants.OptionCombo(origen: "Set_Subsector", array: Array(arrSubsector8), viewC: self)
        case _ where ((typeSectorTextField.text!.contains("Fiestas y eventos")) == true):
                Constants.OptionCombo(origen: "Set_Subsector", array: Array(arrSubsector9), viewC: self)
        case _ where ((typeSectorTextField.text!.contains("Finanzas")) == true):
                Constants.OptionCombo(origen: "Set_Subsector", array: Array(arrSubsector10), viewC: self)
        case _ where ((typeSectorTextField.text!.contains("Jurídico")) == true):
                Constants.OptionCombo(origen: "Set_Subsector", array: Array(arrSubsector11), viewC: self)
        case _ where ((typeSectorTextField.text!.contains("Manufactura")) == true):
                Constants.OptionCombo(origen: "Set_Subsector", array: Array(arrSubsector12), viewC: self)
        case _ where ((typeSectorTextField.text!.contains("Materias Primas")) == true):
                Constants.OptionCombo(origen: "Set_Subsector", array: Array(arrSubsector13), viewC: self)
        case _ where ((typeSectorTextField.text!.contains("Otros Servicios")) == true):
                Constants.OptionCombo(origen: "Set_Subsector", array: Array(arrSubsector14), viewC: self)
        case _ where ((typeSectorTextField.text!.contains("Publicidad")) == true):
                Constants.OptionCombo(origen: "Set_Subsector", array: Array(arrSubsector15), viewC: self)
        case _ where ((typeSectorTextField.text!.contains("Restaurantes y Alimentos")) == true):
                Constants.OptionCombo(origen: "Set_Subsector", array: Array(arrSubsector16), viewC: self)
        case _ where ((typeSectorTextField.text!.contains("Ropa, zapatos y accesorios")) == true):
                Constants.OptionCombo(origen: "Set_Subsector", array: Array(arrSubsector17), viewC: self)
        case _ where ((typeSectorTextField.text!.contains("Salud")) == true):
                Constants.OptionCombo(origen: "Set_Subsector", array: Array(arrSubsector18), viewC: self)
        case _ where ((typeSectorTextField.text!.contains("Seguridad")) == true):
                Constants.OptionCombo(origen: "Set_Subsector", array: Array(arrSubsector19), viewC: self)
        case _ where ((typeSectorTextField.text!.contains("Servicios del hogar")) == true):
                Constants.OptionCombo(origen: "Set_Subsector", array: Array(arrSubsector20), viewC: self)
        case _ where ((typeSectorTextField.text!.contains("Tecnología")) == true):
                Constants.OptionCombo(origen: "Set_Subsector", array: Array(arrSubsector21), viewC: self)
        case _ where ((typeSectorTextField.text!.contains("Viajes y turismo")) == true):
                Constants.OptionCombo(origen: "Set_Subsector", array: Array(arrSubsector22), viewC: self)
        default:
                break
        }

    }

    func setTipoPersona(tipo: String) {
        typePersonTextField.text = tipo
        switch tipo {
        case "Fisica":
            Fisica()
        case "Moral":
            Moral()
        case "Fisica con actividad empresarial":
            Fisica()
        default:
            Fisica()
        }
    }

    func setSector(tipo: String) {
        typeSectorTextField.text = tipo
    }

    func setSubsector(tipo: String) {
        typeSubsectorTextField.text = tipo
    }

    func idUploaded(isUploaded: Bool) {
        if isUploaded == true {
            leadingImage.image = UIImage(named: "image_rede_ok")
            addIDLabel.text  = "¡Exito!"
            IDView.borderColor = UIColor(named: "Base_rede_exit_green")
            trailingImage.image = UIImage(named: "Image_rede_scanIDOk")
            addIDButton.isUserInteractionEnabled = false
            nameTextField.normalTheme(type: 2)
            bornDateTextField.normalTheme(type: 2)
            addIDLabel.textColor = UIColor(named: "Base_rede_blue_textfields_wait")
            self.isUploaded = true
        } else {
            leadingImage.image = UIImage(named: "icon_rede_camera")
            addIDLabel.text  = "Agrega INE, IFE o pasaporte"
            trailingImage.image = UIImage(named: "Image_rede_scanIDWait")
            addIDButton.isUserInteractionEnabled = true
            self.isUploaded = false
        }
    }

    func Fisica() {
        nameCalendar = "Fecha de nacimiento"
        bornDateTextField.label.text = nameCalendar
        bornDateTextField.sizeToFit()
        UIView.animate(withDuration: 0.25, animations: { [weak self] () -> Void in
            guard let self = self else {return}
            self.razonSocialConstraint.constant = 0
            self.razonSocialTop.constant = 0
            self.nombreTopConstraint.constant = 0
            self.razonSocialTextField.isHidden = false
            self.alturaNegocio = 165
            self.alturaTotal = self.alturaConstante + self.alturaNegocio + self.alturaNumeroAddon + self.alturaComprobante
            self.containerHeight.constant = self.alturaTotal
            self.view.layoutIfNeeded()
        })
    }

    func Moral() {
        nameCalendar = "Fecha de constitucion"
        bornDateTextField.label.text = nameCalendar
        bornDateTextField.sizeToFit()
        UIView.animate(withDuration: 0.25, animations: { [weak self] () -> Void in
            guard let self = self else {return}
            self.razonSocialConstraint.constant = 50
            self.razonSocialTop.constant = 15
            self.nombreTopConstraint.constant = 15
            self.razonSocialTextField.isHidden = false
            self.alturaNegocio = 275
            self.alturaTotal = self.alturaConstante + self.alturaNegocio + self.alturaNumeroAddon + self.alturaComprobante
            self.containerHeight.constant = self.alturaTotal
            self.view.layoutIfNeeded()
        })
    }

    func mostrarRazonSocial() {
        UIView.animate(withDuration: 0.25, animations: { [weak self] () -> Void in
            guard let self = self else {return}
            self.typePersonHeight.constant = 50
            self.typeTopConstraint.constant = 15
            self.sectorHeight.constant = 50
            self.sectorTopConstraint.constant = 15
            self.subsectorHeight.constant = 50
            self.subSectorTopConstraint.constant = 15
            self.razonSocialConstraint.constant = 50
            self.razonSocialTop.constant = 15
            self.nombreTopConstraint.constant = 15
            self.razonSocialTextField.isHidden = false
            self.typePersonTextField.isHidden = false
            self.typeSectorButton.isHidden = false
            self.typeSectorTextField.isHidden = false
            self.typeSubsectorButton.isHidden = false
            self.typeSubsectorTextField.isHidden = false
            self.alturaNegocio = 275
            self.alturaTotal = self.alturaConstante + self.alturaNegocio + self.alturaNumeroAddon + self.alturaComprobante
            self.containerHeight.constant = self.alturaTotal
            self.view.layoutIfNeeded()
        })
    }

    func ocultarRazonSocial() {
        UIView.animate(withDuration: 0.25, animations: { [weak self] () -> Void in
            guard let self = self else {return}
            self.typePersonHeight.constant = 0
            self.typeTopConstraint.constant = 0
            self.sectorHeight.constant = 0
            self.sectorTopConstraint.constant = 0
            self.subsectorHeight.constant = 0
            self.subSectorTopConstraint.constant = 0
            self.razonSocialConstraint.constant = 0
            self.razonSocialTop.constant = 0
            self.nombreTopConstraint.constant = 0
            self.razonSocialTextField.isHidden = true
            self.typePersonTextField.isHidden = true
            self.typePersonButton.isHidden = true
            self.typeSectorButton.isHidden = true
            self.typeSectorTextField.isHidden = true
            self.typeSubsectorButton.isHidden = true
            self.typeSubsectorTextField.isHidden = true
            self.alturaNegocio = 0
            self.alturaTotal = self.alturaConstante + self.alturaNegocio + self.alturaNumeroAddon + self.alturaComprobante
            self.containerHeight.constant = self.alturaTotal
            self.view.layoutIfNeeded()
        })
    }

    func requiresProf() {
        UIView.animate(withDuration: 0.25, animations: { [weak self] () -> Void in
            guard let self = self else {return}
            self.viewComprobante.isHidden = true
            self.comprobanteHeight.constant = 0
            self.comprobanteTop.constant = 0
            self.alturaTotal = self.alturaConstante + self.alturaNegocio + self.alturaNumeroAddon + self.alturaComprobante
            self.containerHeight.constant = self.alturaTotal
            self.view.layoutIfNeeded()
        })

    }

    func setDatePicker() {
        let datePicker = UIDatePicker()
        var components = DateComponents()
        var components2 = DateComponents()
        let loc = Locale(identifier: "es_MX")
        datePicker.locale = loc
        components.year = -18
        components2.year = -90
        let maxDate = Calendar.current.date(byAdding: components, to: Date())
        let minDate = Calendar.current.date(byAdding: components2, to: Date())
        datePicker.maximumDate = maxDate
        datePicker.minimumDate = minDate
        datePicker.datePickerMode = .date
        datePicker.frame = CGRect(x: -25, y: 0, width: 300, height: 300)
        datePicker.timeZone = NSTimeZone.local
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        } else {
        }
        let alert = UIAlertController(title: "\(nameCalendar) \n\n\n\n\n\n\n", message: "", preferredStyle: .alert)
        alert.view.addSubview(datePicker)
        let ok = UIAlertAction(title: "Aceptar", style: .default) { [weak self] (_) in
            guard let self = self else {return}
            let dateString = Constants.DateString(formater: Formate.dd_MM_yyyy, day: datePicker.date)
            self.bornDateTextField.text = dateString
            self.bornDateTextField.leadingAssistiveLabel.text = ""
            self.bornDateTextField.normalTheme(type: 2)
        }
        alert.addAction(ok)
        self.present(alert, animated: true, completion: { [weak self] in
            guard let self = self else {return}
            alert.view.superview?.isUserInteractionEnabled = true
            alert.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertControllerBackgroundTapped)))
        })
    }

}

// MARK: - Phone number extension picker implementation

extension DataViewController: UIPickerViewDelegate, UIPickerViewDataSource {

    struct Country {
        let name: String
        let image: UIImage
        let ext: String
        let numberMinLength: Int
        let numberMaxLength: Int
    }

    func initializeCountriesList() {

        let country0 = Country(name: "México", image: UIImage(named: "mxFlag")!, ext: "+52 ", numberMinLength: 10, numberMaxLength: 10)
        let country1 = Country(name: "EE.UU", image: UIImage(named: "usFlag")!, ext: "+1 ", numberMinLength: 10, numberMaxLength: 10)
        let country2 = Country(name: "Colombia", image: UIImage(named: "coFlag")!, ext: "+57 ", numberMinLength: 10, numberMaxLength: 10)
        let country3 = Country(name: "China", image: UIImage(named: "cnFlag")!, ext: "+86 ", numberMinLength: 10, numberMaxLength: 10)
        let country4 = Country(name: "Brasil", image: UIImage(named: "brFlag")!, ext: "+55 ", numberMinLength: 10, numberMaxLength: 10)
        let country5 = Country(name: "Guatemala", image: UIImage(named: "gtFlag")!, ext: "+502 ", numberMinLength: 10, numberMaxLength: 10)
        let country6 = Country(name: "Belice", image: UIImage(named: "bzFlag")!, ext: "+501 ", numberMinLength: 10, numberMaxLength: 10)

        countries.append(country0)
        countries.append(country1)
        countries.append(country2)
        countries.append(country3)
        countries.append(country4)
        countries.append(country5)
        countries.append(country6)

    }

    func configurePhoneTextFields() {
        // TODO: Sacar del bloque available cuando se tengan los assets de banderas para inicializar el imageView, configurar el textfield de número secundario
        let imageViewFlag = UIImageView(image: UIImage(named: "mxFlag"))
        let tap = UITapGestureRecognizer(target: self, action: #selector(flagTapped))
        imageViewFlag.addGestureRecognizer(tap)
        imageViewFlag.isUserInteractionEnabled = true

        let imageViewFlag2 = UIImageView(image: UIImage(named: "mxFlag"))
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(flagTapped))
        imageViewFlag2.addGestureRecognizer(tap2)
        imageViewFlag2.isUserInteractionEnabled = true

//        phoneNumberTextField.leadingView = imageViewFlag
        phoneNumberTextField.leadingView?.width = 27
        phoneNumberTextField.leadingView?.height = 17
        phoneNumberTextField.leadingViewMode = .always
        phoneNumberTextField.text = firstNumberPrefix

//        extraPhoneNumberTextField.leadingView = imageViewFlag2
        extraPhoneNumberTextField.leadingView?.width = 27
        extraPhoneNumberTextField.leadingView?.height = 17
        extraPhoneNumberTextField.leadingViewMode = .always
        extraPhoneNumberTextField.text = extraNumberPrefix
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countries.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return countries[row].name + " " + countries[row].ext
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if currentPhoneNumberTextField == 1 {
            firstNumberMaxLength = countries[row].numberMaxLength
            firstNumberMinLength = countries[row].numberMinLength
            firstNumberPrefix = countries[row].ext
            let leadingView = phoneNumberTextField.leadingView as! UIImageView
            leadingView.image = countries[row].image
            phoneNumberTextField.text = firstNumberPrefix
        } else if currentPhoneNumberTextField == 2 {
            extraNumberMaxLength = countries[row].numberMaxLength
            extraNumberMinLength = countries[row].numberMinLength
            extraNumberPrefix = countries[row].ext
            let leadingView = extraPhoneNumberTextField.leadingView as! UIImageView
            leadingView.image = countries[row].image
            extraPhoneNumberTextField.text = extraNumberPrefix
        }

    }

    @objc func didSelectCountry() {
        self.dismiss(animated: true, completion: nil)
        if currentPhoneNumberTextField == 1 {
            phoneNumberTextField.becomeFirstResponder()
        } else {
            extraPhoneNumberTextField.becomeFirstResponder()
        }
    }

    @objc func flagTapped(_ sender: UITapGestureRecognizer) {
        if sender.view == phoneNumberTextField.leadingView {
            firstNumberMaxLength = 10
            firstNumberPrefix = "+52 "
            currentPhoneNumberTextField = 1
            // phoneNumberTextField.text = firstNumberPrefix
            let leadingView = phoneNumberTextField.leadingView as! UIImageView
            leadingView.image = UIImage(named: "mxFlag")
            phoneNumberTextField.text = firstNumberPrefix
            presentCountryPicker()
        } else if sender.view == extraPhoneNumberTextField.leadingView {
            extraNumberMaxLength = 10
            extraNumberPrefix = "+52 "
            currentPhoneNumberTextField = 2
            // extraPhoneNumberTextField.text = extraNumberPrefix
            let leadingView = extraPhoneNumberTextField.leadingView as! UIImageView
            leadingView.image = UIImage(named: "mxFlag")
            extraPhoneNumberTextField.text = extraNumberPrefix
            presentCountryPicker()
        }

    }

    func presentCountryPicker() {

        // CONFIGURE PICKER VIEW CONTROLLER

        let background = UIView()
        background.backgroundColor = .white
        background.layer.cornerRadius = 10
        background.frame = CGRect(x: (self.view.width-300)/2, y: (self.view.height - 400), width: 300, height: 400)

        let picker = UIPickerView()
        picker.delegate = self
        picker.dataSource = self
        picker.frame = CGRect(x: (self.view.width-300)/2, y: background.frame.minY + 50, width: 300, height: 300)

        let buttonOkPicker = UIButton(type: .system)
        // buttonOkPicker.backgroundColor = .brown
        buttonOkPicker.addTarget(self, action: #selector(didSelectCountry), for: .touchUpInside)
        buttonOkPicker.setTitle("Seleccionar", for: .normal)
        buttonOkPicker.frame = CGRect(x: (self.view.width-300)/2, y: picker.frame.maxY, width: 300, height: 50)

        let labelTitle = UILabel()
        labelTitle.text = "Elige tu pais"
        labelTitle.textAlignment = .center
        // labelTitle.backgroundColor = .blue
        labelTitle.textColor = .black
        labelTitle.font = .systemFont(ofSize: 17, weight: .medium)
        labelTitle.frame = CGRect(x: (self.view.width-300)/2, y: picker.frame.minY-30, width: 300, height: 50)

        let viewPicker = UIViewController()
        if #available(iOS 13.0, *) {
            viewPicker.overrideUserInterfaceStyle = .light
        }
        viewPicker.view.backgroundColor = .black.withAlphaComponent(0.3)
        viewPicker.view.addSubview(background)
        viewPicker.view.addSubview(picker)
        viewPicker.view.addSubview(buttonOkPicker)
        viewPicker.view.addSubview(labelTitle)

        viewPicker.modalPresentationStyle = .overFullScreen
        viewPicker.modalTransitionStyle = .crossDissolve
        self.present(viewPicker, animated: true, completion: nil)
    }

    func validatePhoneNumber(phone: String, number: Int) -> Bool {
        if number == 1 {
            if phone.length < firstNumberPrefix.length+firstNumberMinLength {
                firstNumberWithoutPrefix = ""
                firstNumberCountryCode = ""
                return false
            } else {
                let substrings = phone.split(character: " ")
                firstNumberWithoutPrefix = substrings.last ?? ""
                firstNumberCountryCode = substrings.first?.westernArabicNumeralsOnly ?? ""
                return true
            }
        } else if number == 2 {
            if phone.length < extraNumberPrefix.length+extraNumberMinLength {
                extraNumberWithoutPrefix = ""
                extraNumberCountryCode = ""
                return false
            } else {
                let substrings = phone.split(character: " ")
                extraNumberWithoutPrefix = substrings.last ?? ""
                extraNumberCountryCode = substrings.first?.westernArabicNumeralsOnly ?? ""
                return true
            }
        }
        return false
    }

}

extension DataViewController: dataCDFIObserver {

    func setValuesCDFI(rfc: String, typePerson: String) {
        self.mProspecto.rfc = rfc
        self.mProspecto.tipoPersona = typePerson
    }
}
