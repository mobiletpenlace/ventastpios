//
//  DataRedesignViewController.swift
//  SalesCloud
//
//  Created by aestevezn on 31/10/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit
//import CountryPicker
import TextFieldEffects
import SwiftEventBus

class DataRedesignViewController: BaseVentasView {
    @IBOutlet weak var formularioContentView: UIView!
    @IBOutlet weak var formularioStack: UIStackView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var imageAditionalNumberButton: UIImageView!
    @IBOutlet weak var eliminarNumeroAdicionalLabel: UILabel!
    // Outlets para negocio
    @IBOutlet weak var subsectorLabel: UILabel!
    @IBOutlet weak var sectorLabel: UILabel!
    @IBOutlet weak var isNegocioCheckButton: UIButton!
    @IBOutlet weak var sectorView: UIView!
    // MARK: Datos en formulario junto a sus view
    // Nombre del negocio
    @IBOutlet weak var nombreDeNegocioView: UIView!
    @IBOutlet weak var nombreDeNegocioTextfield: HoshiTextField!
    // Aforo
    @IBOutlet weak var aforoView: UIView!
    @IBOutlet weak var aforoTextField: HoshiTextField!
    // Nombre del cliente
    @IBOutlet weak var nombreDeClienteView: UIView!
    @IBOutlet weak var nombreDeClienteTextField: HoshiTextField!
    // Fecha de nacimiento
    @IBOutlet weak var fechaDeNacimientoView: UIView!
    @IBOutlet weak var fechaDeNacimientoTextField: HoshiTextField!
    // Correo Electrónico
    @IBOutlet weak var correoElectronicoView: UIView!
    @IBOutlet weak var correoElectronicoTextField: HoshiTextField!
    // número teléfonico principal
    @IBOutlet weak var numeroTelefonicoPrincipalView: UIView!
    @IBOutlet weak var numeroPrincipalTextField: HoshiTextField!
    @IBOutlet weak var ladaTextField: HoshiTextField!
    // número teléfonico adicional
    @IBOutlet weak var numeroAdicionalView: UIView!
    @IBOutlet weak var numeroAdicionalTextField: HoshiTextField!
    // MARK: datos de facturación
    @IBOutlet weak var facturacionIsHideButton: UIButton!
    @IBOutlet weak var formFacturacionView: UIView!
    @IBOutlet weak var codigoPostalTextField: AkiraTextField!
    @IBOutlet weak var coloniaTextField: AkiraTextField!
    @IBOutlet weak var delegacionTextField: AkiraTextField!
    @IBOutlet weak var ciudadTextField: AkiraTextField!
    @IBOutlet weak var estadoTextField: AkiraTextField!
    @IBOutlet weak var calleTextField: AkiraTextField!
    @IBOutlet weak var noExteriorTextField: AkiraTextField!
    @IBOutlet weak var noInteriorTextField: AkiraTextField!
    // MARK: Instalación express outlets
    @IBOutlet weak var instalacionExpressLabel: UILabel!
    @IBOutlet weak var instalacionExpressImage: UIImageView!
    @IBOutlet weak var instalacionExpressButton: UIButton!
    var isNegocioIsCheck: Bool = faseConfigurarPlan.isNegocio
    fileprivate var spinnerView: WaitingView!
    fileprivate var aditionalNumerisHide: Bool = true
    fileprivate var facturacionIsHide: Bool = true
    fileprivate let imageUncheck = UIImage(named: "icon_uncheck_addons")
    fileprivate let imageCheck = UIImage(named: "Image_rede_blue_check")
    fileprivate var mConsultAvailabilityPresenter: ConsultAvailabilityPresenter!
    fileprivate var nameCalendar = ""
    fileprivate var phoneNumberValidated: Bool = false
    fileprivate var extraPhoneNumberValidated = true
    fileprivate var isUploaded = false
    fileprivate var mDataViewModel: DataViewModel!
    fileprivate var phoneIsValidate = false
    fileprivate var emailIsValidate: Bool = false
    fileprivate var extraPhoneIsValidate = false
    fileprivate let numberMinLength = 10
    var mProspecto: NProspecto!
    var prefixPhoneSelected: String = "52"
    var rfc: String = String.emptyString
    var tipoPerdona: String = String.emptyString
    var mDocs: DocumentosObject!
    var dataSubsector: [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        initialitationEventBust()
        verifySaleType()
        setupFormulariofacturacion()
        createCountryPicker()
        mDataViewModel = DataViewModel(view: self)
        mDataViewModel.atachView(observer: self)
        mDataViewModel.consultaClusters()
        mConsultAvailabilityPresenter = ConsultAvailabilityPresenter(view: self)
        mDocs = DocumentosObject()
        mProspecto = NProspecto()
        numeroPrincipalTextField.delegate = self
        numeroAdicionalTextField.delegate = self
        isNegocioIsCheck = faseConfigurarPlan.isNegocio
        Logger.println("El estado de la venta es: \(isNegocioIsCheck)")
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    deinit {
        SwiftEventBus.unregister(self)
        print("DEINIT DATAREDESIGN VIEW CONTROLLER")
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        fasePostPreguntas.Index = 2
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func checkIsNegocioAction(_ sender: Any) {
        updateIsCheckButton(check: isNegocioIsCheck)
        print("se oprime el boton")
    }

    @IBAction func showCDFIView(_ sender: Any) {
        let view = UIStoryboard(name: "DataCDFI", bundle: nil)
        let controller = view.instantiateViewController(withIdentifier: "DataCDFIViewController")
        controller.modalPresentationStyle = .overFullScreen
        self.present(controller, animated: true)
    }

    @IBAction func showOrHideFacturationAction(_ sender: Any) {
        showOrHideFacturacion()
    }

    @IBAction func addAditionalNumberAction(_ sender: Any) {
        showOrHideAditionalNumberView()
    }

    @IBAction func instalacionExpressAction(_ sender: Any) {
        ConsultAvailability()
    }

    @IBAction func showDocumentAddViewAction(_ sender: Any) {
        let view = UIStoryboard(name: "DocumentsDataView", bundle: nil)
        let controller = view.instantiateViewController(withIdentifier: "DocumentsDataViewController") as! TPDocumentsDataViewController
        controller.documentsDataManager = self
        controller.modalPresentationStyle = .overFullScreen
        self.present(controller, animated: true)
    }

    @IBAction func showPickerBirthday(_ sender: Any) {
        setDatePicker()
    }

    @IBAction func selectSectorAction(_ sender: Any) {
        Constants.OptionCombo(origen: "set_sector", array: Array(TPSectorSocialReason.arrSector), viewC: self)
    }

    @IBAction func selectSubsector(_ sender: Any) {
        Constants.OptionCombo(origen: "set_subsector", array: dataSubsector, viewC: self)
    }

    @IBAction func nameAction(_ sender: Any) {
        let viewV = UIStoryboard(name: "NamePopViewController", bundle: nil)
        let controller = viewV.instantiateViewController(withIdentifier: "NamePopViewController") as! NamePopViewController
        controller.providesPresentationContextTransitionStyle = true
        controller.definesPresentationContext = true
        controller.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        controller.mNameObject = TPDataDocumentsShared.shared.mNameObject
        controller.manager = self
        UIApplication.topViewController()?.present(controller, animated: false, completion: nil)
    }

    @IBAction func textFieldDidEnd(_ sender: Any) {
        let senderTextField = sender as! HoshiTextField
        if senderTextField == numeroPrincipalTextField {
            let phoneComplete = "\(prefixPhoneSelected)\(senderTextField.text ?? "")"
            if validatePhoneNumber(phone: phoneComplete, number: 1) {
                phoneIsValidate = true
            } else {
                phoneIsValidate = false
                numeroPrincipalTextField.borderActiveColor = .red
            }
        } else if senderTextField == numeroAdicionalTextField {
            let phoneComplete = "\(prefixPhoneSelected)\(numeroAdicionalTextField.text ?? "")"
            if validatePhoneNumber(phone: phoneComplete, number: 2) {
                extraPhoneIsValidate = true
            } else {
                extraPhoneIsValidate = false
                numeroAdicionalTextField.borderActiveColor = .red
            }
        } else if senderTextField == correoElectronicoTextField {
            if isValidEmail(email: senderTextField.text ?? "") {
                emailIsValidate = true
            } else {
                emailIsValidate = false
                correoElectronicoTextField.borderActiveColor = .red
            }
        }
        faseDataConsult.isPaymentRiskConsulted = false
        checkFields()
    }
}

// MARK: functions for User Interface
extension DataRedesignViewController {

    @objc func alertControllerBackgroundTapped() {
        self.dismiss(animated: true, completion: nil)
    }

    fileprivate func setDatePicker() {
        let datePicker = UIDatePicker()
        var components = DateComponents()
        var components2 = DateComponents()
        let loc = Locale(identifier: "es_MX")
        datePicker.locale = loc
        components.year = -18
        components2.year = -90
        let maxDate = Calendar.current.date(byAdding: components, to: Date())
        let minDate = Calendar.current.date(byAdding: components2, to: Date())
        datePicker.maximumDate = maxDate
        datePicker.minimumDate = minDate
        datePicker.datePickerMode = .date
        datePicker.frame = CGRect(x: -25, y: 0, width: 300, height: 300)
        datePicker.timeZone = NSTimeZone.local
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        } else {
        }
        let alert = UIAlertController(title: "\(nameCalendar) \n\n\n\n\n\n\n", message: "", preferredStyle: .alert)
        alert.view.addSubview(datePicker)
        let ok = UIAlertAction(title: "Aceptar", style: .default) { [weak self] (_) in
            guard let self = self else {return}
            let dateString = Constants.DateString(formater: Formate.dd_MM_yyyy, day: datePicker.date)
            self.fechaDeNacimientoTextField.text = dateString
        }
        alert.addAction(ok)
        self.present(alert, animated: true, completion: { [weak self] in
            guard let self = self else {return}
            alert.view.superview?.isUserInteractionEnabled = true
            alert.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertControllerBackgroundTapped)))
        })
    }

    fileprivate func setupView() {
        numeroAdicionalView.isHidden = true
        formFacturacionView.isHidden = true
        nombreDeNegocioView.isHidden = true
        aforoView.isHidden = true
        sectorView.isHidden = true
        updateStack(stack: self.stackView)
    }

    fileprivate func checkIsNegocio(check: Bool) {
        if !check {
            self.isNegocioIsCheck = true
            sectorView.isHidden = false
            nombreDeNegocioView.isHidden = false
            aforoView.isHidden = false
        } else {
            self.isNegocioIsCheck = false
            sectorView.isHidden = true
            nombreDeNegocioView.isHidden = true
            aforoView.isHidden = true
        }
    }

    fileprivate func updateIsCheckButton(check: Bool) {
        if !check {
            self.checkIsNegocio(check: check)
            self.isNegocioCheckButton.setImage(UIImage(named: "Image_rede_blue_check"), for: .normal)
        } else {
            self.checkIsNegocio(check: check)
            self.isNegocioCheckButton.setImage(UIImage(named: "icon_uncheck_addons"), for: .normal)
        }
    }

    fileprivate func updateStack(stack: UIStackView) {
        UIView.animate(withDuration: 0.3) {
            stack.layoutIfNeeded()
        }
    }

    fileprivate func showOrHideAditionalNumberView() {
        let agregarText = "Agregar número adicional"
        let deleteText = "Eliminar número adicional"
        if aditionalNumerisHide {
            aditionalNumerisHide = false
            numeroAdicionalView.isHidden = false
            eliminarNumeroAdicionalLabel.text = deleteText
            updateStack(stack: formularioStack)
            updateImageAddNunberButton()
        } else {
            aditionalNumerisHide = true
            numeroAdicionalView.isHidden = true
            eliminarNumeroAdicionalLabel.text = agregarText
            updateStack(stack: formularioStack)
            updateImageAddNunberButton()
        }
    }

    func showSpinner(_ title: String = String.empty) {
        spinnerView = WaitingView(frame: CGRect(
            x: 0,
            y: 0,
            width: self.view.bounds.width,
            height: self.view.bounds.height
        ))
        spinnerView.titleLabel!.text = title
        self.view.addSubview(spinnerView)
    }

    func removeSpinnerView() {
        spinnerView.removeFromSuperview()
    }

    fileprivate func setupFormulariofacturacion() {
        codigoPostalTextField.text = faseDataConsult.direction.CodigoPostal
        delegacionTextField.text = faseDataConsult.direction.Delegacion
        ciudadTextField.text = faseDataConsult.direction.Ciudad
        estadoTextField.text = faseDataConsult.direction.Estado
        calleTextField.text = faseDataConsult.direction.Calle
        noExteriorTextField.text = faseDataConsult.direction.NumeroExterior
        noInteriorTextField.text = faseDataConsult.direction.NumeroInterior
    }

    fileprivate func showOrHideFacturacion() {
        if facturacionIsHide {
            facturacionIsHide = false
            formFacturacionView.isHidden = false
            updateButtonFacturacion()
        } else {
            facturacionIsHide = true
            formFacturacionView.isHidden = true
            updateButtonFacturacion()
        }
    }

    fileprivate func updateButtonFacturacion() {
        if facturacionIsHide {
            facturacionIsHideButton.setImage(imageUncheck, for: .normal)
        } else {
            facturacionIsHideButton.setImage(imageCheck, for: .normal)
        }
    }

    fileprivate func updateImageAddNunberButton() {
        let addImage = UIImage(named: "Image_rede_add")
        let deleteImage = UIImage(named: "icon_delete_gray")
        if aditionalNumerisHide {
            imageAditionalNumberButton.image = addImage
        } else {
            imageAditionalNumberButton.image = deleteImage
        }
    }

    fileprivate func createCountryPicker() {
//        let picker = CountryPicker()
//        picker.countryPickerDelegate = self
//        picker.showPhoneNumbers = true
//        picker.displayOnlyCountriesWithCodes = ["MX", "US", "CO", "BR", "GT", "BZ"]
//        ladaTextField.inputView = picker
//        picker.selectRow(0, inComponent: 0, animated: true)
    }
}

// MARK: Logical functions
extension DataRedesignViewController {
    func validatePhoneNumber(phone: String, number: Int) -> Bool {
        if number == 1 {
            if phone.length < prefixPhoneSelected.length+numberMinLength {
                return false
            } else {
                _ = phone.split(character: String.space)
                return true
            }
        } else if number == 2 {
            if phone.length < prefixPhoneSelected.length+numberMinLength {
                return false
            } else {
                _ = phone.split(character: String.space)
                return true
            }
        }
        return false
    }

    func checkFields() {
        if (phoneIsValidate && emailIsValidate) {
            if !nombreDeClienteTextField.text!.isEmpty && !fechaDeNacimientoTextField.text!.isEmpty {
                if !TPDataDocumentsShared.shared.uploadedDocumentIDCard {
                    Constants.Alert(
                        title: "Advertencia",
                        body: "Favor de agregar la documentación necesaria",
                        type: type.Info,
                        viewC: self
                    )
                    faseDataOK.isOK = false
                } else {
                    debtorCheck()
                    faseDataOK.isOK = true
                }
            } else {
                faseDataOK.isOK = false
            }
        } else {
            faseDataOK.isOK = false
        }
    }

    func isValidEmail(email: String) -> Bool {
        let emailRegex = "(?:[\\p{L}0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[\\p{L}0-9!#$%\\&'*+/=?\\^_`{|}" +
        "~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\" +
        "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[\\p{L}0-9](?:[a-" +
        "z0-9-]*[\\p{L}0-9])?\\.)+[\\p{L}0-9](?:[\\p{L}0-9-]*[\\p{L}0-9])?|\\[(?:(?:25[0-5" +
        "]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-" +
        "9][0-9]?|[\\p{L}0-9-]*[\\p{L}0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21" +
        "-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: email)
    }

    fileprivate func initialitationEventBust() {
        SwiftEventBus.onMainThread(self, name: "CheckFields") { [weak self] _ in
            self?.checkFields()
        }

        SwiftEventBus.onMainThread(self, name: "showSpinner") { [weak self ] result in
            guard let titleSprint = result?.object as? String else {
                return
            }
            self?.showSpinner(titleSprint)
        }

        SwiftEventBus.onMainThread(self, name: "hideSpinnerLoader") { [weak self] _ in
            self?.removeSpinnerView()
        }

        SwiftEventBus.onMainThread(self, name: "set_sectorPicker") { [weak self] result in
            guard let sectorValue = result?.object as? String else { return }
            self?.setSectorValue(sectorValue: sectorValue)
        }

        SwiftEventBus.onMainThread(self, name: "set_subsectorPicker") { [weak self] result in
            guard let subSectorValue = result?.object as? String else { return }
            self?.setSubsectorValue(subsectorValue: subSectorValue)
        }
    }

    func debtorCheck() {
        if faseDataConsult.isPaymentRiskConsulted == false {
            rfc  = UtilsTp().generatRFC(name: TPDataDocumentsShared.shared.mNameObject?.name ?? String.emptyString, lastName: TPDataDocumentsShared.shared.mNameObject?.middleName ?? String.emptyString, motherLastName: TPDataDocumentsShared.shared.mNameObject?.lastName ?? String.emptyString, date: fechaDeNacimientoTextField.text!)
            mDataViewModel.consultaPaymentRisk(name: nombreDeClienteTextField.text!, bornDate: fechaDeNacimientoTextField.text!, email: correoElectronicoTextField.text!, rfc: rfc, telefono: numeroPrincipalTextField.text!)
        }
    }
}

// instalación express
extension DataRedesignViewController {
    func ConsultAvailability() {
        showSpinner("Consultando información")
        mConsultAvailabilityPresenter.GetConsultAvailability(mCiudad: "", mDistrito: faseDataConsult.direction.Distrito, mRegion: faseDataConsult.direction.Region, mZona: faseDataConsult.direction.zona, mCluster: faseDataConsult.direction.Cluster, mConsultAvailabilityDelegate: self)
    }
}

// MARK: venta negocio
extension DataRedesignViewController {
    func verifySaleType() {
        if isNegocioIsCheck {
            self.setupViewIsNegocio()
        } else {
            self.setupView()
        }
    }

    func setupViewIsNegocio() {
        numeroAdicionalView.isHidden = true
        formFacturacionView.isHidden = true
        updateIsCheckButton(check: isNegocioIsCheck)
        sectorView.isHidden = false
        nombreDeNegocioView.isHidden = false
        aforoView.isHidden = false
        updateStack(stack: self.formularioStack)
        sectorLabel.text = TPSectorSocialReason.arrSector.first
        subsectorLabel.text = TPSubsectorsSocialReason.arrSubsector1.first
    }

    func setSectorValue(sectorValue: String) {
        sectorLabel.text = sectorValue
        setSubsectorArray(sectorSelected: sectorValue)
        setSubsectorValue(subsectorValue: dataSubsector[0])
    }

    func setSubsectorValue(subsectorValue: String) {
        subsectorLabel.text = subsectorValue
    }

    func setSubsectorArray(sectorSelected: String) {
        if let opSelected = TPSectorValue(rawValue: sectorSelected) {
            switch opSelected {
            case .ANIMALES_MASCOTAS:
                dataSubsector = Array(TPSubsectorsSocialReason.arrSubsector1)
            case .ASOCIACION_CIVIL:
                dataSubsector = Array(TPSubsectorsSocialReason.arrSubsector2)
            case .AUTOS_TRANSPORTES:
                dataSubsector = Array(TPSubsectorsSocialReason.arrSubsector3)
            case .BELLEZA:
                dataSubsector = Array(TPSubsectorsSocialReason.arrSubsector4)
            case .BIENES_RAICES:
                dataSubsector = Array(TPSubsectorsSocialReason.arrSubsector5)
            case .CONSTRUCCION:
                dataSubsector = Array(TPSubsectorsSocialReason.arrSubsector6)
            case .DEPORTES:
                dataSubsector = Array(TPSubsectorsSocialReason.arrSubsector7)
            case .EDUCACION:
                dataSubsector = Array(TPSubsectorsSocialReason.arrSubsector8)
            case .FIESTAS_EVENTOS:
                dataSubsector = Array(TPSubsectorsSocialReason.arrSubsector9)
            case .FINANZAS:
                dataSubsector = Array(TPSubsectorsSocialReason.arrSubsector10)
            case .JURIDICO:
                dataSubsector = Array(TPSubsectorsSocialReason.arrSubsector11)
            case .MANUFACTURA:
                dataSubsector = Array(TPSubsectorsSocialReason.arrSubsector12)
            case .MATERIAS_PRIMAS:
                dataSubsector = Array(TPSubsectorsSocialReason.arrSubsector13)
            case .OTROS_SERVICIOS:
                dataSubsector = Array(TPSubsectorsSocialReason.arrSubsector14)
            case .PUBLICIDAD:
                dataSubsector = Array(TPSubsectorsSocialReason.arrSubsector15)
            case .RESTAURANTES_ALIMENTOS:
                dataSubsector = Array(TPSubsectorsSocialReason.arrSubsector16)
            case .ROPA_ZAPATOS_ACCESORIOS:
                dataSubsector = Array(TPSubsectorsSocialReason.arrSubsector17)
            case .SALUD:
                dataSubsector = Array(TPSubsectorsSocialReason.arrSubsector18)
            case .SEGURIDAD:
                dataSubsector = Array(TPSubsectorsSocialReason.arrSubsector19)
            case .SERVICIOS_DEL_HOGAR:
                dataSubsector = Array(TPSubsectorsSocialReason.arrSubsector20)
            case .TECNOLOGIA:
                dataSubsector = Array(TPSubsectorsSocialReason.arrSubsector21)
            case .VIAJES_TURISMO:
                dataSubsector = Array(TPSubsectorsSocialReason.arrSubsector22)
            }
        }
    }
}
