//
//  MediaDocumentSourceTypeProtocol.swift
//  SalesCloud
//
//  Created by Axlestevez-trabajo on 29/11/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

protocol TPMediaDocumentsSourceProtocol {
    func receiveImageToIDCardFront(image: UIImage)
    func receiveImageToIDCardBack(image: UIImage)
    func receiveResidencyDocument(image: UIImage)
    func receiveRFCDocuemnt(_ doc: String, _ image: UIImage)
    func reciveBankDocument(image: UIImage)
    // MARK: afiliacion de comercios
    func receiveActaConstitutiva(_ doc: String, _ image: UIImage)
    func receiveComprobanteDomicilio(_ doc: String, _ image: UIImage)
    func receiveRFCAfiliacion(_ doc: String, _ image: UIImage)
    func receivePoderRepresentante(_ doc: String, _ image: UIImage)
    func receiveIDRepresentanteLegal(_ doc: String, _ image: UIImage)
    func receiveEstadoDeCuenta(_ doc: String, _ image: UIImage)
}


extension TPMediaDocumentsSourceProtocol {
    func receiveImageToIDCardFront(image: UIImage) { }
    func receiveImageToIDCardBack(image: UIImage) { }
    func receiveResidencyDocument(image: UIImage) { }
    func receiveRFCDocuemnt( _ doc: String = String(), _ image: UIImage = UIImage()) { }
    func reciveBankDocument(image: UIImage) { }
    // MARK: afiliacion de comercios
    func receiveActaConstitutiva(_ doc: String = String(),_ image: UIImage = UIImage()) { }
    func receiveComprobanteDomicilio(_ doc: String = String(),_ image: UIImage = UIImage()) { }
    func receiveRFCAfiliacion(_ doc: String = String(),_ image: UIImage = UIImage()) { }
    func receivePoderRepresentante(_ doc: String = String(), _ image: UIImage = UIImage()) { }
    func receiveIDRepresentanteLegal(_ doc: String = String(), _ image: UIImage = UIImage()) { }
    func receiveEstadoDeCuenta(_ doc: String = String(), _ image: UIImage = UIImage()) { }
}
