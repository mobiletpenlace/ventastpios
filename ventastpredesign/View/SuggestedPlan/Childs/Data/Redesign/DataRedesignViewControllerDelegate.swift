//
//  DataRedesignViewControllerDelegate.swift
//  SalesCloud
//
//  Created by aestevezn on 01/11/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit
//import CountryPicker
import SwiftEventBus

// MARK: CountryPickerDelegate
//extension DataRedesignViewController: CountryPickerDelegate {
//    func countryPhoneCodePicker(_ picker: CountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
//        ladaTextField.placeholder = phoneCode
//        prefixPhoneSelected = phoneCode
//    }
//}

// MARK: Instalación express delegate
extension DataRedesignViewController: ConsultAvailabilityDelegate {
    func OnSuccessConsultAvailability(mConsultAvailabilityResponse: ConsultAvailabilityResponse) {
        DispatchQueue.main.async {
            self.removeSpinnerView()
            self.instalacionExpressImage.image = UIImage(named: "image_rede_ok")
            self.instalacionExpressLabel.text = "Instalación express disponible"
            self.instalacionExpressButton.isUserInteractionEnabled = false
            faseDataConsult.isExpressAcepted = true
        }
    }

    func notAvailability() {
        DispatchQueue.main.async {
            self.removeSpinnerView()
            self.instalacionExpressImage.image = UIImage(named: "Img_error")
            self.instalacionExpressLabel.text = "Instalación express NO disponible"
            self.instalacionExpressButton.isUserInteractionEnabled = false
            faseDataConsult.isExpressAcepted = false
        }
    }
}

extension DataRedesignViewController: documentsDataManagerProtocol {
    func updatedata() {
        DispatchQueue.main.async {
            let fullName = "\(TPDataDocumentsShared.shared.mNameObject?.name ?? "name") \(TPDataDocumentsShared.shared.mNameObject?.middleName ?? "ap") \(TPDataDocumentsShared.shared.mNameObject?.lastName ?? "am")"
            self.nombreDeClienteTextField.text = fullName.uppercased()
            self.fechaDeNacimientoTextField.text = TPDataDocumentsShared.shared.birthday ?? ""
            self.checkFields()
            self.nombreDeClienteTextField.isEnabled = false
        }
    }
}

extension DataRedesignViewController: DataObserver {
    func showSpinnerLoader() {
        showSpinner("Validando información")
    }

    func hideSpinnerLoader() {
        removeSpinnerView()
    }

    func setSector(tipo: String) {
        sectorLabel.text = tipo
    }

    func setTipoPersona(tipo: String) {
    }

    func setSubsector(tipo: String) {
    }

    func message(message: String) {

    }

    func requiresComprobante(Comprobante: Bool) {
        if Comprobante {
            Constants.Alert(
                title: "Cuidado",
                body: "porfavor ingresa tu compobante en la seccion de documentos ",
                type: type.Info,
                viewC: self
            )
        }
    }

    func enviaData() {
        TPDataDocumentsShared.shared.mDocs?.proofDirection = ""
        var fecha = fechaDeNacimientoTextField.text!
        let date = fecha.components(separatedBy: "/")
        let dia = date[0]
        let mes = date[1]
        let año = date[2]
        fecha = año + "-" + mes + "-" + dia
        if isNegocioIsCheck {
            mProspecto.esNegocio = isNegocioIsCheck
            mProspecto.aforo = aforoTextField.text ?? String.emptyString
            mProspecto.nombreNegocio = nombreDeNegocioTextfield.text ?? String.emptyString
            mProspecto.sector = sectorLabel.text ?? String.emptyString
            mProspecto.subsector = subsectorLabel.text ?? String.emptyString
        } else {
            mProspecto.esNegocio = isNegocioIsCheck
        }
        mProspecto.firstName = TPDataDocumentsShared.shared.mNameObject?.name ?? String.empty
        mProspecto.midleName = TPDataDocumentsShared.shared.mNameObject?.middleName ?? String.empty
        mProspecto.lastName = TPDataDocumentsShared.shared.mNameObject?.lastName ?? String.empty
        mProspecto.rfc = rfc
        mProspecto.fechaNacimiento = fecha
        mProspecto.telefono = numeroPrincipalTextField.text ?? String.empty
        mProspecto.celular = numeroPrincipalTextField.text ?? String.empty
        mProspecto.otroTelefono = numeroAdicionalTextField.text ?? String.empty
        mProspecto.correo = correoElectronicoTextField.text ?? String.empty
        mProspecto.otroCoreo = String.empty
        mProspecto.tipoPersona  = "Fisica"
        mProspecto.idioma = "Español"
        SwiftEventBus.post("CargaInfoStep3", sender: mProspecto)
        SwiftEventBus.post("CargaDocumentos", sender: TPDataDocumentsShared.shared.mDocs!)
        SwiftEventBus.post("Valida_Oportunidad", sender: nil)
        faseDataOK.isSended = true
    }

    func setName(nombre: nameObject) {

    }

    func setProof(image: String) {

    }

    func EnviaDataVC() {
        SwiftEventBus.post("Envia_data_toStart")
        SwiftEventBus.unregister(self, name: "Envia_data_toStart")
        SwiftEventBus.post("RemoveEventBus")
    }
}

extension DataRedesignViewController: sendDataToFormFromCardName {
    func receiveDataName() {
        let nameComplete = "\(TPDataDocumentsShared.shared.mNameObject?.name ?? "") \(TPDataDocumentsShared.shared.mNameObject?.middleName ?? "") \(TPDataDocumentsShared.shared.mNameObject?.lastName ?? "")"
        nombreDeClienteTextField.text = nameComplete.uppercased()
        nombreDeClienteTextField.isEnabled = false
    }
}

extension DataRedesignViewController: dataCDFIObserver {
    func setValuesCDFI(rfc: String, typePerson: String) {
        self.rfc = rfc
        self.tipoPerdona = typePerson
    }
}
