//
//  DataManagerSingleton.swift
//  SalesCloud
//
//  Created by Axlestevez on 29/11/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

public class TPDataDocumentsShared {

    // MARK: flujo de venta
    public static let shared = TPDataDocumentsShared()
    var documentTypeSharedData: TPDocumentType?  // type Document RFC (image o PDF)
    var rfcDocumentType: TPDocumentType?
    var mDocs: DocumentosObject?
    var imageIDCardFrint: UIImage?
    var imageIDCardBack: UIImage?
    var imageRecidency: UIImage?
    var imageRFC: UIImage?
    var documentRFC: String?
    var mNameObject: nameObject?
    var birthday: String?
    var uploadedDocumentIDCard: Bool = false
    var isRedesingFlow: Bool? // redesing flow
    var mProspecto: NProspecto?

    // MARK: Afiliacion de comercios
    var actaCostitutiva: UIImage?
    var comprobanteDeDomicilio: UIImage?
    var rfcAfiiacionComercios: UIImage?
    var poderRepresentanteLegal: UIImage?
    var idRepresentanteLegal: UIImage?
    var estadoDeCuenta: UIImage?

    private init() {
        mNameObject = nameObject()
        mDocs = DocumentosObject()
        mProspecto = NProspecto()
    }
}
