//
//  DocumentsDataViewController.swift
//  SalesCloud
//
//  Created by aestevezn on 23/11/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

protocol documentsDataManagerProtocol {
    func updatedata()
}

class TPDocumentsDataViewController: BaseVentasView {
    // Buttons
    @IBOutlet weak var rfcButton: UIButton!
    @IBOutlet weak var idCardBackButton: UIButton!
    @IBOutlet weak var ineFrontbutton: UIButton!
    @IBOutlet weak var recidencyProofButton: UIButton!
    @IBOutlet weak var ineFrontImage: UIImageView!
    @IBOutlet weak var labelINEFront: UILabel!
    @IBOutlet weak var idCardBackLabel: UILabel!
    @IBOutlet weak var idCardBackImage: UIImageView!
    @IBOutlet weak var recidencyProofImage: UIImageView!
    @IBOutlet weak var recidencyProofLabel: UILabel!
    @IBOutlet weak var rfcDocumentLabel: UILabel!
    @IBOutlet weak var rfcDocumentImage: UIImageView!
    fileprivate var rfcTypeDocumentSourceView: TPSourceTypeDocumentView!
    fileprivate var waitingView: WaitingView!
    fileprivate var imageIneFrontString: String?
    fileprivate var imageIneBackString: String?
    fileprivate var mConsultINEDataPresenter: ConsultINEDataPresenter!
    var documentsDataManager: documentsDataManagerProtocol?
    override func viewDidLoad() {
        super.viewDidLoad()
        mConsultINEDataPresenter = ConsultINEDataPresenter(view: self)
        TPDataDocumentsShared.shared.isRedesingFlow = true
    }

    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true)
    }

    @IBAction func showCameraController(_ sender: UIButton) {
        TPDocumentsShared.shared.documentSelected = TPDocuments(rawValue: sender.tag)
        evalButtonType(sender)
    }

    @IBAction func upLoadDocumentsAction(_ sender: UIButton) {
        showSppinnerView("Consultando información ...")
        DispatchQueue.main.async {
            self.getInfoIne()
        }
    }
}

// MARK: logical function for show media type source
extension TPDocumentsDataViewController {
    fileprivate func getInfoIne() {
        if imageIneFrontString != "" && (imageIneBackString != nil) {
            TPDataDocumentsShared.shared.mDocs?.ineFront = imageIneFrontString
            TPDataDocumentsShared.shared.mDocs?.ineBack = imageIneBackString
            mConsultINEDataPresenter.GetConsultINEData(mId: imageIneFrontString!, mIdReverso: imageIneBackString!, mConsultINEDataDelegate: self)
        } else {
            removeSpinnerView()
            showErrorGenericMessage(controller: self)
        }
    }

    @objc func evalButtonType(_ sender: UIButton) {
        if let event = documentIDType(rawValue: sender.tag) {
            switch event {
            case .ID_CARD_FRONT:
                showMediaSourceTypeController()
                break
            case .ID_CARD_BACK:
                showMediaSourceTypeController()
                break
            case .RESIDENCY_PROOF:
                showMediaSourceTypeController()
                break
            case .RFC_DOCUMENT:
                showRFCSourceSelectedView()
                break
            case .BANK_DOCUMENT:
                print("")
            }
        } else {
            Constants.Alert(title: "Error",
                            body: "Ha ocurrido un error inesperado",
                            type: type.Error,
                            viewC: self
            )
        }
    }

    fileprivate func showMediaSourceTypeController() {
        let view = UIStoryboard(name: "MediaSourceViewController", bundle: nil)
        let controller = view.instantiateViewController(withIdentifier: "MediaSourceViewController") as! MediaSourceViewController
        controller.delegate = self
        controller.modalPresentationStyle = .overFullScreen
        self.present(controller, animated: true)
    }

    fileprivate func showRFCSourceSelectedView() {
        rfcTypeDocumentSourceView = TPSourceTypeDocumentView(frame: CGRect(
            x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height
        ))
        rfcTypeDocumentSourceView.manager = self
        self.view.addSubview(rfcTypeDocumentSourceView)
    }

    func removeViewRFCDocument() {
        rfcTypeDocumentSourceView.removeFromSuperview()
    }

    func showSppinnerView( _ title: String = "") {
        waitingView = WaitingView(frame: CGRect(
            x: 0,
            y: 0,
            width: self.view.bounds.width,
            height: self.view.bounds.height
        ))
        waitingView.titleLabel!.text = title
        self.view.addSubview(waitingView)
    }

    func removeSpinnerView() {
        waitingView.removeFromSuperview()
    }

    fileprivate func showErrorGenericMessage(controller: UIViewController) {
        Constants.Alert(title: "Error",
                        body: "Ha ocurrido un error inesperado",
                        type: type.Error,
                        viewC: controller
        )
    }

    fileprivate func convertImageTobase64(image: UIImage) -> String? {
        var imageData: Data?
        var imageDataantes: Data?
        imageDataantes  = UIImageJPEGRepresentation(image, 1)
        let widthInPixels = image.size.width * image.scale
        let heightInPixels = image.size.height * image.scale
        print("tamaño de la imagen original en pixeles \(widthInPixels) x \(heightInPixels)")
        let imageSize1: Int = imageDataantes!.count
        let heightTemp = (300 * heightInPixels) / widthInPixels
        print(heightTemp)
        print("size of image antes de in KB: %f ", Double(imageSize1) / 1024.0)
        imageData  = UIImageJPEGRepresentation(imageWithSize(image: image, size: CGSize(width: 300, height: heightTemp)), 1)
        // size image
        let imageSize: Int = imageData!.count
        print("size of image despues de in KB: %f ", Double(imageSize) / 1024.0)
        //
        return imageData?.base64EncodedString()
    }

    fileprivate func imageWithSize(image: UIImage, size: CGSize) -> UIImage {
        var scaledImageRect = CGRect.zero
        let aspectWidth: CGFloat = size.width / image.size.width
        let aspectHeight: CGFloat = size.height / image.size.height
        let aspectRatio: CGFloat = min(aspectWidth, aspectHeight)
        scaledImageRect.size.width = image.size.width * aspectRatio
        scaledImageRect.size.height = image.size.height * aspectRatio
        scaledImageRect.origin.x = (size.width - scaledImageRect.size.width) / 2.0
        scaledImageRect.origin.y = (size.height - scaledImageRect.size.height) / 2.0
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        image.draw(in: scaledImageRect)
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return scaledImage!
    }

    fileprivate func updateRFCStatus() {
        rfcDocumentImage.image = UIImage(named: "pdf")
        rfcDocumentLabel.text = "Cambiar"
    }
}

// MARK: implementación de UIDocumentPickerDelegate
extension TPDocumentsDataViewController: UIDocumentPickerDelegate {
    func openFinder() {
        showSppinnerView("Validando información")
        let documentPicker = UIDocumentPickerViewController(documentTypes: ["public.content"], in: .import)
        documentPicker.delegate = self
        self.present(documentPicker, animated: true)
    }

    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        if let selectedDocument = urls.first {
            if let pdfDocument = try? Data(contentsOf: selectedDocument) {
                let base64String = pdfDocument.base64EncodedString()
                print("PDF en formato base64: \(base64String)")
                DispatchQueue.main.async {
                    self.updateRFCStatus()
                    self.removeSpinnerView()
                }
            } else {
                removeSpinnerView()
                showErrorGenericMessage(controller: self)
            }
        } else {
            removeSpinnerView()
            showErrorGenericMessage(controller: self)
        }
    }

    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        controller.dismiss(animated: true)
        DispatchQueue.main.async {
            self.removeSpinnerView()
        }
    }
}

// MARK: mediaDocumentsDelegate
extension TPDocumentsDataViewController: TPMediaDocumentsSourceProtocol {
    func receiveImageToIDCardFront(image: UIImage) {
        showSppinnerView("Cargando información")
        DispatchQueue.main.async {
            self.imageIneFrontString = self.convertImageTobase64(image: image)
        }
        ineFrontImage.image = image
        labelINEFront.text = "Cambiar"
        removeSpinnerView()
    }

    func receiveImageToIDCardBack(image: UIImage) {
        showSppinnerView()
        DispatchQueue.main.async {
            self.imageIneBackString = self.convertImageTobase64(image: image)
        }
        idCardBackImage.image = image
        idCardBackLabel.text = "Cambiar"
        removeSpinnerView()
    }

    func receiveResidencyDocument(image: UIImage) {
        recidencyProofImage.image = image
        recidencyProofLabel.text = "Cambiar"
    }

    func receiveRFCDocuemnt(_ doc: String, _ image: UIImage) {
        if let optionSelectedSource = TPDocumentType(rawValue: (TPDataDocumentsShared.shared.rfcDocumentType)?.rawValue ?? 0) {
            switch optionSelectedSource {
            case .IMAGE_TYPE:
                rfcDocumentImage.image = image
                break
            case .DOCUMENT_TYPE:
                break
            }
        } else {
            showErrorGenericMessage(controller: self)
        }
        rfcDocumentLabel.text = "Cambiar"
    }
}

// MARK: TPRFCSourceTypeDelegate
extension TPDocumentsDataViewController: TPSourceTypeDocumentProtocol {

    func reacToCompleteFlow(action: Int) {
        self.removeViewRFCDocument()
        DispatchQueue.main.async {
            if let event = TPDocumentType(rawValue: action) {
                switch event {
                case .IMAGE_TYPE:
                    self.showMediaSourceTypeController()
                    break
                case .DOCUMENT_TYPE:
                    self.openFinder()
                    break
                }
            } else {
                self.showErrorGenericMessage(controller: self)
            }
        }
    }

    func closeView() {
        DispatchQueue.main.async {
            self.rfcTypeDocumentSourceView.removeFromSuperview()
        }
    }
}

// MARK: ConsultINEDataDelegate protocol
extension TPDocumentsDataViewController: ConsultINEDataDelegate {

    func OnSuccessConsultINEData(mConsultINEDataResponse: ConsultINEDataResponse) {
        if mConsultINEDataResponse.nombres != "" {
            TPDataDocumentsShared.shared.mNameObject?.name = mConsultINEDataResponse.nombres ?? ""
        }

        if mConsultINEDataResponse.primerApellido != "" {
            TPDataDocumentsShared.shared.mNameObject?.middleName = mConsultINEDataResponse.primerApellido ?? ""
        }

        if mConsultINEDataResponse.segundoApellido != "" {
            TPDataDocumentsShared.shared.mNameObject?.lastName = mConsultINEDataResponse.segundoApellido ?? ""
        }

        if mConsultINEDataResponse.fechaNacimiento != "" {
            TPDataDocumentsShared.shared.birthday = mConsultINEDataResponse.fechaNacimiento
        }

        SwiftEventBus.post("DatosINE", sender: mConsultINEDataResponse)
        documentsDataManager?.updatedata()
        removeSpinnerView()
        TPDataDocumentsShared.shared.uploadedDocumentIDCard = true
        self.dismiss(animated: true)
    }
}
