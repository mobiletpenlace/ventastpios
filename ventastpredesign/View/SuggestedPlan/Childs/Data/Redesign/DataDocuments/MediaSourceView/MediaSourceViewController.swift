//
//  MediaSourceViewController.swift
//  SalesCloud
//
//  Created by Axlestevez-trabajo on 27/11/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import PhotosUI
import Photos
import UIKit

class MediaSourceViewController: BaseVentasView {
    private var image = UIImageView()
    private var imageSelected = UIImage()
    private var imagePickerController: UIImagePickerController!
    var delegate: TPMediaDocumentsSourceProtocol?
    var documentType: documentIDType!

    //MARK: String Contants Utils
    let cameraError     = NSLocalizedString("La cámara no esta disponible en este dispositivo", comment: "")
    let galeryError     = NSLocalizedString("Ha ocurrido un error al momento de accesar a la galeria", comment: "")
    let errorAlertTitle = NSLocalizedString("Error", comment: "")

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func mediaSourcceFromCameraAction(_ sender: Any) {
        cameraAction()
    }

    @IBAction func mediaSourceFromGalery(_ sender: Any) {
        galeryAction()
    }

    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true)
    }

    private func cameraAction() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.sourceType = .camera
            self.present(imagePickerController, animated: true)
        } else {
            self.showAlert(message: cameraError)
        }
    }

    private func showAlert(message: String) {
        let alertController = UIAlertController(title: errorAlertTitle, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }

    private func galeryAction () {
        if #available(iOS 14.0, *) {
            var config = PHPickerConfiguration(photoLibrary: .shared())
            config.selectionLimit = 1
            config.filter = PHPickerFilter.any(of: [.livePhotos, .images])

            let pickerController = PHPickerViewController(configuration: config)
            pickerController.delegate = self
            self.present(pickerController, animated: true)
        } else {
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                imagePickerController = UIImagePickerController()
                imagePickerController.delegate = self
                imagePickerController.sourceType = UIImagePickerController.SourceType.photoLibrary
                self.present(imagePickerController, animated: true)
            } else {
                self.showAlert(message: galeryError)
            }
        }
    }

    fileprivate func hideView() {
        self.dismiss(animated: true)
    }

    fileprivate func evalOptionSelected(sendingImage: UIImage) {
        if let optionSelected = TPDocumentsShared.shared.documentSelected {
            if let eval = TPDocuments(rawValue: optionSelected.rawValue) {
                switch eval {
                case .ID_CARD_FRONT:
                    delegate?.receiveImageToIDCardFront(image: sendingImage)
                    break
                case .ID_CARD_BACK:
                    delegate?.receiveImageToIDCardBack(image: sendingImage)
                    break
                case .RESIDENCY_PROOF:
                    delegate?.receiveResidencyDocument(image: sendingImage)
                    break
                case .RFC_DOCUMENT:
                    reactToOptionPreviousSelected()
                    break
                case .BANK_DOCUMENT:
                    delegate?.reciveBankDocument(image: sendingImage)
                case .ACTA_CONSTITUTIVA:
                    delegate?.receiveActaConstitutiva(String.emptyString, sendingImage)
                case .COMPROBANTE_DE_DOMICILIO:
                    delegate?.receiveComprobanteDomicilio(String.emptyString, sendingImage)
                case .RFC:
                    delegate?.receiveRFCAfiliacion(String.emptyString, sendingImage)
                case .PODER_REPRESENTANTE_LEGAL:
                    delegate?.receivePoderRepresentante(String.emptyString, sendingImage)
                case .ID_REPRESENTANTE_LEGAL:
                    delegate?.receiveIDRepresentanteLegal(String.emptyString, sendingImage)
                case .ESTADO_DE_CUENTA:
                    delegate?.receiveEstadoDeCuenta(String.emptyString, sendingImage)
                }
            }
        } else {
            Constants.Alert(title: "Error", body: "Ha ocurrido un error", type: type.Error, viewC: self)
            Logger.println("NO SE HA PODIDO OBTENER EL OPTIONCODE")
        }
    }

    fileprivate func reactToOptionPreviousSelected() {
        let opSelected = TPDataDocumentsShared.shared.rfcDocumentType
        let documentString = TPDataDocumentsShared.shared.documentRFC ?? ""
        if let event = TPDocumentType(rawValue: opSelected?.rawValue ?? 0) {
            switch event {
            case .IMAGE_TYPE:
                delegate?.receiveRFCDocuemnt("", imageSelected)
            case .DOCUMENT_TYPE:
                delegate?.receiveRFCDocuemnt(documentString)
            }
        } else {
            Logger.println("NO SE HA PODIDO OBTENER EL OPTIONCODE")
        }
    }
}

extension MediaSourceViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String: Any]) {
        picker.dismiss(animated: true)
        imageSelected = (info[UIImagePickerControllerOriginalImage] as! UIImage)
        self.evalOptionSelected(sendingImage: imageSelected)
        self.hideView()
    }

       func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
           picker.dismiss(animated: true)
       }
}

extension MediaSourceViewController: PHPickerViewControllerDelegate {
    @available(iOS 14, *)
    func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
        picker.dismiss(animated: true)
        for result in results {
            if result.itemProvider.canLoadObject(ofClass: UIImage.self) {
                result.itemProvider.loadObject(ofClass: UIImage.self, completionHandler: { (object, error) in
                    if let image = object as? UIImage {
                        DispatchQueue.main.async {
                            self.imageSelected = image
                            self.evalOptionSelected(sendingImage: self.imageSelected)
                            self.hideView()
                        }
                    }
                })
            }
        }
    }

    @available(iOS 14, *)
    func pickerDidCancel(_ picker: PHPickerViewController) {
        picker.dismiss(animated: true)
    }
}
