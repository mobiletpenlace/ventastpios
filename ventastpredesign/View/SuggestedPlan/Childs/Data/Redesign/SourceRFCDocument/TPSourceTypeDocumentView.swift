//
//  TPSourceRFCDocumentView.swift
//  SalesCloud
//
//  Created by Axlestevez-trabajo on 29/11/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

protocol TPSourceTypeDocumentProtocol {
    func reacToCompleteFlow(action: Int)
    func closeView()
}

class TPSourceTypeDocumentView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var photoButton: UIButton!
    @IBOutlet weak var findeButton: UIButton!
    var manager: TPSourceTypeDocumentProtocol?

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    @IBAction func reactToButtonSelectedAction(_ sender: UIButton) {
        TPDataDocumentsShared.shared.rfcDocumentType = TPDocumentType(rawValue: sender.tag)
        self.manager?.reacToCompleteFlow(action: sender.tag)
    }

    @IBAction func closeView(_ sender: UIButton) {
        self.manager?.closeView()
    }

    private func commonInit() {
        Bundle.main.loadNibNamed("\(TPSourceTypeDocumentView.self)", owner: self)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
}
