//
//  TPDataRedesingConstants.swift
//  SalesCloud
//
//  Created by Axlestevez-trabajo on 29/11/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation

public enum documentIDType: Int {
    case ID_CARD_FRONT      = 1
    case ID_CARD_BACK       = 2
    case RESIDENCY_PROOF    = 3
    case RFC_DOCUMENT       = 4
    case BANK_DOCUMENT      = 5

}

public enum TPDocTypeAfiliacion: Int {
    case ACTA_CONSTITUTIVA  = 1
    case COMPROBANTE_DOM    = 2
    case RFC_COMERCIOS      = 3
    case REPRESENT_LEGAL    = 4
    case ID_LAGAL_REP       = 5
    case ESTADO_CUENTA      = 6
}

public enum TPDocumentType: Int {
    case DOCUMENT_TYPE      = 0
    case IMAGE_TYPE         = 1
}

public enum TPTypeSocialReason {
    static let arrTipos: Set<String> = ["Física", "Moral", "Física con actividad empresarial"]
}

public enum TPSocialReasonType {
    static let FISICA = "Fisica"
    static let MORAL = "Moral"
    static let FISICA_EMPRESARIAL = "Fisica"
}

public enum TPSectorValue: String {
    case ANIMALES_MASCOTAS          = "Animales y mascotas"
    case ASOCIACION_CIVIL           = "Asociación Civil"
    case AUTOS_TRANSPORTES          = "Autos y transportes"
    case BELLEZA                    = "Belleza"
    case BIENES_RAICES              = "Bienes raíces"
    case CONSTRUCCION               = "Construcción"
    case DEPORTES                   = "Deportes"
    case EDUCACION                  = "Educación"
    case FIESTAS_EVENTOS            = "Fiestas y eventos"
    case FINANZAS                   = "Finanzas"
    case JURIDICO                   = "Jurídico"
    case MANUFACTURA                = "Manufactura"
    case MATERIAS_PRIMAS            = "Materias Primas"
    case OTROS_SERVICIOS            = "Otros Servicios"
    case PUBLICIDAD                 = "Publicidad"
    case RESTAURANTES_ALIMENTOS     = "Restaurantes y Alimentos"
    case ROPA_ZAPATOS_ACCESORIOS    = "Ropa, zapatos y accesorios"
    case SALUD                      = "Salud"
    case SEGURIDAD                  = "Seguridad"
    case SERVICIOS_DEL_HOGAR        = "Servicios del hogar"
    case TECNOLOGIA                 = "Tecnología"
    case VIAJES_TURISMO             = "Viajes y turismo"
}

public enum TPSectorSocialReason {
    static let arrSector: Set<String> = ["Animales y mascotas", "Asociación Civil", "Autos y transportes", "Belleza", "Bienes raíces", "Construcción", "Deportes", "Educación", "Fiestas y eventos", "Finanzas", "Jurídico", "Manufactura", "Materias Primas", "Otros Servicios", "Publicidad", "Restaurantes y Alimentos", "Ropa, zapatos y accesorios", "Salud", "Seguridad", "Servicios del hogar", "Tecnología", "Viajes y turismo"]
}

public enum TPSubsectorsSocialReason {
    static let arrSubsector1: Set<String> = ["Escuela de adiestramiento", "Operadores Logísticos", "Otros", "Venta de alimentos y accesorios", "Veterinaria", "Viajes y turismo"]
    static let arrSubsector2: Set<String> = ["Asociación Civil"]
    static let arrSubsector3: Set<String> = ["Fletes y Mudanzas", "Operadores Logísticos", "Otros", "Refaccionarias", "Transporte Escolar", "Trasporte de carga en general"]
    static let arrSubsector4: Set<String> = ["Centro de masajes", "Clínicas dermatológicas", "Estéticas", "Maquillaje y Cosmética", "Otros", "Spa"]
    static let arrSubsector5: Set<String> = ["Academias de defensa personal", "Asesores Inmobiliarios", "Oficinas virtuales", "Otros", "Renta de Oficinas, locales comerciales y casa habitación"]
    static let arrSubsector6: Set<String> = ["Arquitectos", "Desarrollos habitacionales", "Electricidad e iluminación", "Ferretería", "Materiales para construcción", "Otros", "Servicios de Instalación y Mantenimiento", "Venta de equipo Industrial"]
    static let arrSubsector7: Set<String> = ["Academias de defensa personal", "Casa Club", "Deportes Extremos", "Gym", "Otros", "Venta de Artículos Deportivos"]
    static let arrSubsector8: Set<String> = ["Academias de Danza", "Centros de Idiomas", "Coaching", "Entretenimiento", "Escuelas de Manejo", "Escuelas y universidades", "Otros"]
    static let arrSubsector9: Set<String> = ["Alquiler de Salones para de eventos", "Fotógrafos", "Otros", "Productoras de video", "Venta de artículos Promocionales"]
    static let arrSubsector10: Set<String> = ["Agencias de Seguros", "Banca", "Casa de Empeño", "Consultoría en Negocios", "Despacho de Contadores", "Joyerías"]
    static let  arrSubsector11: Set<String> = ["Agencia Aduanal", "Despacho de Abogados", "Despacho de Cobranza", "Notarias", "Otros"]
    static let arrSubsector12: Set<String> = ["Electricistas", "Herrería", "Hule", "Muebles", "Otros", "Productos de cartón", "Vidrios"]
    static let arrSubsector13: Set<String> =  ["Harinas y derivados", "Parques Temáticos", "Tienda de abarrotes"]
    static let arrSubsector14: Set<String> = ["Agricultura", "Asociación Civil", "Entretenimiento", "Venta de equipo Industrial", "Veterinaria"]
    static let arrSubsector15: Set<String> = ["Agencias de publicidad", "Campañas Digitales", "Imprenta y serigrafía", "Material POP", "Periódicos y Revistas", "Productoras de video", "Renta de espectaculares"]
    static let arrSubsector16: Set<String> = ["Bares", "Cafeterías", "Comida para llevar", "Comida rápida", "Fondas", "Loncherías", "Pastelerías", "Restaurantes"]
    static let arrSubsector17: Set<String> = ["Entretenimiento", "Fábricas de Ropa", "Joyerías", "Lencería", "Manufactura de uniformes", "Otros", "Zapaterías"]
    static let arrSubsector18: Set<String> = ["Artículos ortopédicos", "Centros de rehabilitación", "Clínicas", "Consultorios", "Entretenimiento", "Farmacias", "Ópticas", "Otros"]
    static let arrSubsector19: Set<String> = ["Academias de capacitación en seguridad industrial", "Análisis del riesgo", "CCTV", "Equipos de Protección", "Otros", "Seguridad Privada"]
    static let arrSubsector20: Set<String> = ["Agencias de personal de limpieza", "Cocinas Integrales", "Electricistas", "Jardinería", "Otros", "Papelería", "Persianas", "Pisos y laminados", "Plomería", "Reparación de electrodomésticos"]
    static let arrSubsector21: Set<String> = ["Campañas Digitales", "Desarrollo de software y aplicaciones", "Equipo de cómputo", "Factura electrónica", "Hosting de pagina web", "Instalación, Soporte y Venta de Equipos de Telecomunicaciones", "Otros"]
    static let arrSubsector22: Set<String> = ["Agencia de viajes", "Hoteles", "Parques Temáticos", "Parques Turísticos", "Transporte Turístico"]
}
