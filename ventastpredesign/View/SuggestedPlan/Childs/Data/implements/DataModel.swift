//
//  DataModel.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 23/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import RealmSwift

protocol DataModelObserver: NSObjectProtocol {
    func onSuccesClusters(clusters: ClustersResponse)
    func onSuccesPaymentRisk(response: PaymentRiskResponse)
    func onErrorClusters()
    func onErrorPaymentRisk()
}

class DataModel: BaseModel {

    weak var viewModel: DataModelObserver?
    var server1: ServerDataManager2<ClustersResponse>?
    var server2: ServerDataManager2<PaymentRiskResponse>?
    var urlClusters =  ""
    var urlPaymentRisk = AppDelegate.API_SALESFORCE + ApiDefinition.API_DEBTOR

    init(observer: BaseViewModelObserver) {
        super.init(observerVM: observer)
        server1 = ServerDataManager2<ClustersResponse>()
        server2 = ServerDataManager2<PaymentRiskResponse>()
    }

    func atachModel(viewModel: DataModelObserver) {
        self.viewModel = viewModel
    }

    func consultaClusters(request: ClustersRequest) {
        urlClusters = AppDelegate.API_SALESFORCE + ApiDefinition.API_CONSULTA_CLUSTERS
        server1?.setValues(requestUrl: urlClusters, delegate: self, headerType: .headersSalesforce)
        server1?.request(requestModel: request)
    }

    func consultaPaymetRisk(request: PaymentRiskRequest) {
        urlPaymentRisk = AppDelegate.API_TOTAL + ApiDefinition.API_DEBTOR
        server2?.setValues(requestUrl: urlPaymentRisk, delegate: self, headerType: .headersFfmap)
        server2?.request(requestModel: request)
    }

    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if requestUrl == urlClusters {
            let clusterResponse = response as! ClustersResponse
            if clusterResponse.info != nil {
                viewModel?.onSuccesClusters(clusters: clusterResponse)
            } else {
                viewModel?.onErrorClusters()
            }
        }

        if requestUrl == urlPaymentRisk {
            let paymentRiskResponse = response as! PaymentRiskResponse
            if paymentRiskResponse.Results?.ResultDescription == "Success" {
                if faseDataConsult.isPaymentRiskConsulted == false {
                    viewModel?.onSuccesPaymentRisk(response: paymentRiskResponse)
                }
            } else {
                viewModel?.onErrorPaymentRisk()
            }
        }

    }

    override func onErrorLoadResponse(requestUrl: String, messageError: String) {
        viewModel?.onErrorClusters()
    }

}
