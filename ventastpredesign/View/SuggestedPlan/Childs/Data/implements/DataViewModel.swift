//
//  DataViewModel.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 23/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus

// TEST :NSObjectProtocol
protocol DataObserver: NSObjectProtocol {
    func setSector(tipo: String)
    func setTipoPersona(tipo: String)
    func setSubsector(tipo: String)
    func message(message: String)
    func requiresComprobante(Comprobante: Bool)
    func enviaData()
    func checkFields()
    func setName(nombre: nameObject)
    func setProof(image: String)
    func EnviaDataVC()
    func showSpinnerLoader()
    func hideSpinnerLoader()
}

class DataViewModel: BaseViewModel, DataModelObserver {

    // TEST weak
    weak var observer: DataObserver?
    var model: DataModel!
    var mSitio = Sitio()
    var isCapturaComprobante: Bool = false

    deinit {
        SwiftEventBus.unregister(self)
    }

    override init(view: BaseVentasView) {
        super.init(view: view)
        model = DataModel(observer: self)
        model.atachModel(viewModel: self)
        getSitio()
    }

    func atachView(observer: DataObserver) {
        self.observer = observer
        SwiftEventBus.onMainThread(self, name: "Set_TipoPersonaPicker") { [weak self] result in
            if let value = result?.object as? String {
                self?.observer?.setTipoPersona(tipo: value)
            }
        }

        SwiftEventBus.onMainThread(self, name: "Set_SectorPicker") { [weak self] result in
            if let value = result?.object as? String {
                self?.observer?.setSector(tipo: value)
            }
        }

        SwiftEventBus.onMainThread(self, name: "Set_SubsectorPicker") { [weak self] result in
            if let value = result?.object as? String {
                self?.observer?.setSubsector(tipo: value)
            }
        }
        SwiftEventBus.onMainThread(self, name: "Envia_data_toStart") { [weak self] _ in
            // FIXIT: ESTE EVENTBUS SE REENVIA
            print("EVENTBUS DE ENVIADATA $$$$$")
            if faseDataOK.isSended == false {
                self?.observer!.enviaData()
            }
        }

        SwiftEventBus.onMainThread(self, name: "SetName_Data") { [weak self] result in
            if let value = result?.object as? nameObject {
                self?.observer?.setName(nombre: value)
            }
        }

        SwiftEventBus.onMainThread(self, name: "EnviaComprobante") { [weak self] result in
            if let value = result?.object as? String {
                self?.observer?.setProof(image: value)
            }
        }
        SwiftEventBus.onMainThread(self, name: "EnviaData_DataVC") { [weak self] _ in
                self?.observer?.EnviaDataVC()
        }

        SwiftEventBus.onMainThread(self, name: "CheckFields") { [weak self] _ in
            self?.observer?.checkFields()
        }
    }

    func consultaClusters() {
        let clustersRequest = ClustersRequest()
        clustersRequest.accion = "ConsultaClusterDocumentos"
        model.consultaClusters(request: clustersRequest)
    }

    func consultaPaymentRisk(name: String, bornDate: String, email: String, rfc: String, telefono: String) {
        SwiftEventBus.post("showSpinner", sender: "Consultando información de domicilio")
        let paymentRiskRequest = PaymentRiskRequest()
        paymentRiskRequest.MLogin = Login()
        paymentRiskRequest.mData = Client()
        paymentRiskRequest.MLogin?.Ip = "127.2.1.0"
        paymentRiskRequest.MLogin?.User = "26351"
        paymentRiskRequest.MLogin?.Pasword = "Middle100$"

        paymentRiskRequest.mData!.NombreCliente = name
        paymentRiskRequest.mData!.Calle = faseDataConsult.direction.Calle
        paymentRiskRequest.mData!.Numero = faseDataConsult.direction.NumeroExterior
        paymentRiskRequest.mData!.CodigoPostal = faseDataConsult.direction.CodigoPostal
        paymentRiskRequest.mData!.Colonia = faseDataConsult.direction.Colonia
        paymentRiskRequest.mData!.CorreoElectronico = email
        paymentRiskRequest.mData!.Telefono = telefono
        paymentRiskRequest.mData!.RFC = rfc
        paymentRiskRequest.mData!.Delegacion = faseDataConsult.direction.Delegacion
        model.consultaPaymetRisk(request: paymentRiskRequest)
    }

    func getSitio() {
//        if case let sitio : Sitio? = Sitio(value: RealManager.findFirst(object: Sitio.self)!){
//            mSitio = sitio!
//        }else{
//            observer?.message(message: "Fallo la carga del sitio")
//        }
    }

    func onSuccesClusters(clusters: ClustersResponse) {
        view.hideLoading()
        for cluster in clusters.info!.infoclusters!.clusters {
            if cluster.cluster == mSitio.cluster {
                for doc in cluster.documentosRequeridos {
                    if doc.uppercased().contains(string: "COMPROBANTE") {
                        isCapturaComprobante = true
                    }
                }
            }
        }
        observer?.requiresComprobante(Comprobante: isCapturaComprobante)
    }

    func onSuccesPaymentRisk(response: PaymentRiskResponse) {
        var name: String?
        var debt: String?
        var promo: String?

        faseDataConsult.isPaymentRiskConsulted = true
        if response.resultsCobranza?.count != 0 {
            SwiftEventBus.post("hideSpinnerLoader")
            let popUPDebtor = PopUPDebtorCard(nibName: "PopUPDebtorCard", bundle: nil)
            popUPDebtor.modalPresentationStyle = .pageSheet
            if #available(iOS 15.0, *) {
                if let presentationController = popUPDebtor.presentationController as? UISheetPresentationController {
                    presentationController.detents = [.medium()]
                }
            } else {
                // TODO: Half scren present on 14- ios versions
            }

            response.resultsCobranza?.forEach {result in
                if (result.Coincidencia?.contains(string: "NombreCliente")) == true {
                    name = result.NombreCliente
                    let debtSubStrings = result.SaldoCuenta?.components(separatedBy: "|")
                    debt = debtSubStrings?[0]
                    promo = debtSubStrings?[1]
                } else {
                    // DO Something
                }
            }
            UIApplication.topViewController()?.present(popUPDebtor, animated: false, completion: nil)

            if name?.count ?? 0 > 0 {
                if debt?.count ?? 0 > 0 {
                    popUPDebtor.debtLabel.text = "Este domicilio presenta un adeudo por $ \(debt ?? "__.__") del cliente \(name ?? "Cliente")."
                } else {
                    // FIXME: A VECES MANDA UN NIL EN DEBTLABEL cuando en el simulador se llena y le das un enter
                    popUPDebtor.debtLabel.text = "Este domicilio presenta un adeudo del cliente \(name ?? "Cliente")."
                }
                if promo?.count ?? 0 > 0 {
                    popUPDebtor.promoLabel.text = "Tiene una promoción de \(promo ?? "Promo")."
                } else {
                    popUPDebtor.promoLabel.isHidden = true
                }
            } else {
//                FIXME: debtLabel nil, cuando se da enter directo
                popUPDebtor.debtLabel.text = "Este domicilio presenta un adeudo."
                popUPDebtor.promoLabel.isHidden = true
            }

        } else {
            print("letters not found")
        }

    }

    func onErrorPaymentRisk() {
        Logger.println("On error payment risk")
    }

    func onErrorClusters() {
        view.hideLoading()
        observer?.message(message: "No se ha podido consultar la información")
    }

}
