//  DataViewController.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 05/05/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus
import MaterialComponents
import Amplitude

protocol GetDataIneDelegate {
    func getInfoINE(_ ineFront: String, _ frontINEImage: UIImage, _ ineBack: String, _ backINEImage: UIImage)
}

class DataViewController: BaseVentasView, ConsultINEDataDelegate, GetDataIneDelegate, ConsultAvailabilityDelegate {

    deinit {
        print("DEINIT DATA VIEW CONTROLLER")
    }

    @IBOutlet weak var contentScrollView: UIScrollView!
    @IBOutlet weak var leadingProofImage: UIImageView!
    @IBOutlet weak var labelProof: UILabel!
    @IBOutlet weak var addProofButton: UIButton!
    @IBOutlet weak var IDView: UIView!
    @IBOutlet weak var leadingImage: UIImageView!
    @IBOutlet weak var addIDLabel: UILabel!
    @IBOutlet weak var trailingImage: UIImageView!
    @IBOutlet weak var addIDButton: UIButton!
    @IBOutlet weak var typePersonTextField: MDCFilledTextField!
    @IBOutlet weak var typePersonButton: UIButton!
    @IBOutlet weak var typeSectorTextField: MDCFilledTextField!
    @IBOutlet weak var typeSectorButton: UIButton!
    @IBOutlet weak var typeSubsectorTextField: MDCFilledTextField!
    @IBOutlet weak var typeSubsectorButton: UIButton!
    @IBOutlet weak var razonSocialTextField: MDCFilledTextField!
    @IBOutlet weak var nameTextField: MDCFilledTextField!
    @IBOutlet weak var bornDateTextField: MDCFilledTextField!
    @IBOutlet weak var emailTextField: MDCFilledTextField!
    @IBOutlet weak var phoneNumberTextField: MDCFilledTextField!
    @IBOutlet weak var extraPhoneNumberTextField: MDCFilledTextField!
    @IBOutlet weak var addPhoneButton: UIButton!
    @IBOutlet weak var addPhoneLabel: UILabel!
    @IBOutlet weak var addPhoneImageView: UIImageView!
    @IBOutlet weak var viewComprobante: UIView!
    @IBOutlet weak var expressSellLabel: UILabel!
    @IBOutlet weak var expressSellImageView: UIImageView!
    @IBOutlet weak var expressSellButton: UIButton!

    // constraints
    @IBOutlet weak var typePersonHeight: NSLayoutConstraint!
    @IBOutlet weak var sectorHeight: NSLayoutConstraint!
    @IBOutlet weak var subsectorHeight: NSLayoutConstraint!
    @IBOutlet weak var razonSocialConstraint: NSLayoutConstraint!
    @IBOutlet weak var typeTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var sectorTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var subSectorTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var razonSocialTop: NSLayoutConstraint!
    @IBOutlet weak var nombreTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var numberAditionalContraint: NSLayoutConstraint!
    @IBOutlet weak var numberAdditionalBootom: NSLayoutConstraint!
    @IBOutlet weak var containerHeight: NSLayoutConstraint!
    @IBOutlet weak var comprobanteHeight: NSLayoutConstraint!
    @IBOutlet weak var comprobanteTop: NSLayoutConstraint!

    var arrTipos: Set<String> = ["Física", "Moral", "Física con actividad empresarial"]
    var arrSector: Set<String> = ["Animales y mascotas", "Asociación Civil", "Autos y transportes", "Belleza", "Bienes raíces", "Construcción", "Deportes", "Educación", "Fiestas y eventos", "Finanzas", "Jurídico", "Manufactura", "Materias Primas", "Otros Servicios", "Publicidad", "Restaurantes y Alimentos", "Ropa, zapatos y accesorios", "Salud", "Seguridad", "Servicios del hogar", "Tecnología", "Viajes y turismo"]
    var arrSubsector1: Set<String> = ["Escuela de adiestramiento", "Operadores Logísticos", "Otros", "Venta de alimentos y accesorios", "Veterinaria", "Viajes y turismo"]
    var arrSubsector2: Set<String> = ["Asociación Civil"]
    var arrSubsector3: Set<String> = ["Fletes y Mudanzas", "Operadores Logísticos", "Otros", "Refaccionarias", "Transporte Escolar", "Trasporte de carga en general"]
    var arrSubsector4: Set<String> = ["Centro de masajes", "Clínicas dermatológicas", "Estéticas", "Maquillaje y Cosmética", "Otros", "Spa"]
    var arrSubsector5: Set<String> = ["Academias de defensa personal", "Asesores Inmobiliarios", "Oficinas virtuales", "Otros", "Renta de Oficinas, locales comerciales y casa habitación"]
    var arrSubsector6: Set<String> = ["Arquitectos", "Desarrollos habitacionales", "Electricidad e iluminación", "Ferretería", "Materiales para construcción", "Otros", "Servicios de Instalación y Mantenimiento", "Venta de equipo Industrial"]
    var arrSubsector7: Set<String> = ["Academias de defensa personal", "Casa Club", "Deportes Extremos", "Gym", "Otros", "Venta de Artículos Deportivos"]
    var arrSubsector8: Set<String> = ["Academias de Danza", "Centros de Idiomas", "Coaching", "Entretenimiento", "Escuelas de Manejo", "Escuelas y universidades", "Otros"]
    var arrSubsector9: Set<String> = ["Alquiler de Salones para de eventos", "Fotógrafos", "Otros", "Productoras de video", "Venta de artículos Promocionales"]
    var arrSubsector10: Set<String> = ["Agencias de Seguros", "Banca", "Casa de Empeño", "Consultoría en Negocios", "Despacho de Contadores", "Joyerías"]
    var arrSubsector11: Set<String> = ["Agencia Aduanal", "Despacho de Abogados", "Despacho de Cobranza", "Notarias", "Otros"]
    var arrSubsector12: Set<String> = ["Electricistas", "Herrería", "Hule", "Muebles", "Otros", "Productos de cartón", "Vidrios"]
    var arrSubsector13: Set<String> =  ["Harinas y derivados", "Parques Temáticos", "Tienda de abarrotes"]
    var arrSubsector14: Set<String> = ["Agricultura", "Asociación Civil", "Entretenimiento", "Venta de equipo Industrial", "Veterinaria"]
    var arrSubsector15: Set<String> = ["Agencias de publicidad", "Campañas Digitales", "Imprenta y serigrafía", "Material POP", "Periódicos y Revistas", "Productoras de video", "Renta de espectaculares"]
    var arrSubsector16: Set<String> = ["Bares", "Cafeterías", "Comida para llevar", "Comida rápida", "Fondas", "Loncherías", "Pastelerías", "Restaurantes"]
    var arrSubsector17: Set<String> = ["Entretenimiento", "Fábricas de Ropa", "Joyerías", "Lencería", "Manufactura de uniformes", "Otros", "Zapaterías"]
    var arrSubsector18: Set<String> = ["Artículos ortopédicos", "Centros de rehabilitación", "Clínicas", "Consultorios", "Entretenimiento", "Farmacias", "Ópticas", "Otros"]
    var arrSubsector19: Set<String> = ["Academias de capacitación en seguridad industrial", "CAnálisis del riesgo", "CCTV", "Equipos de Protección", "Otros", "Seguridad Privada"]
    var arrSubsector20: Set<String> = ["Agencias de personal de limpieza", "Cocinas Integrales", "Electricistas", "Jardinería", "Otros", "Papelería", "Persianas", "Pisos y laminados", "Plomería", "Reparación de electrodomésticos"]
    var arrSubsector21: Set<String> = ["Campañas Digitales", "Desarrollo de software y aplicaciones", "Equipo de cómputo", "Factura electrónica", "Hosting de pagina web", "Instalación, Soporte y Venta de Equipos de Telecomunicaciones", "Otros"]
    var arrSubsector22: Set<String> = ["Agencia de viajes", "Hoteles", "Parques Temáticos", "Parques Turísticos", "Transporte Turístico"]
    var mConsultINEDataPresenter: ConsultINEDataPresenter!
    var mDataViewModel: DataViewModel!
    var mProspecto: NProspecto!
    var mNameObject: nameObject!
    let validatorManager = ValidatorManager()
    var alturaConstante: CGFloat = 445
    var alturaNegocio: CGFloat = 0
    var alturaNumeroAddon: CGFloat = 0
    var alturaTotal: CGFloat = 445
    var alturaComprobante: CGFloat = 105
    var mDocs: DocumentosObject!
    var nameCalendar = ""
    var isUploaded = false
    var isChecked = false
    var rfc = ""
    var isSended = false

    // Phone ext
    var countries: [Country] = []
    var currentPhoneNumberTextField = 1
    var firstNumberPrefix = ""
    var firstNumberMaxLength = 10
    var firstNumberMinLength = 10
    var firstNumberWithoutPrefix = ""
    var firstNumberCountryCode = ""
    var extraNumberPrefix = ""
    var extraNumberMaxLength = 10
    var extraNumberMinLength = 10
    var extraNumberWithoutPrefix = ""
    var extraNumberCountryCode = ""

    // Validation
    var phoneNumberValidated = false
    // var extraPhoneNumberValidated = true
    var emailValidated = false

    var mConsultAvailabilityPresenter: ConsultAvailabilityPresenter!
    var mVerifyNIPPresenter: VerifyNIPPresenter!

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        configTextFields()
        extraPhoneNumberTextField.isHidden = true
        idUploaded(isUploaded: false)
        idUploaded(isUploaded: false)
        phoneNumberTextField.delegate = self
        extraPhoneNumberTextField.delegate = self
        phoneNumberTextField.delegate = self

        // TEST
        bornDateTextField.isUserInteractionEnabled = false
        nameTextField.isUserInteractionEnabled = false

        // TEXTFIELD PhoneNumber
        initializeCountriesList()
        configurePhoneTextFields()

        mNameObject = nameObject()
        mDocs = DocumentosObject()
        mNameObject.name = ""
        mNameObject.middleName = ""
        mNameObject.lastName = ""
        nameCalendar = "Fecha de nacimiento"
        mDataViewModel = DataViewModel(view: self)
        mDataViewModel.atachView(observer: self)
        mDataViewModel.consultaClusters()
        numberAditionalContraint.constant = 15
        numberAdditionalBootom.constant = 0
        alturaTotal = alturaConstante + alturaComprobante + CGFloat(50)
        containerHeight.constant = alturaTotal
        ocultarRazonSocial()
        // if fase.TypeHome == true { ocultarRazonSocial() }
        // else{ mostrarRazonSocial() }

    }

    @IBAction func addIDAction(_ sender: Any) {
        let scanCard: ScanCard = UIStoryboard(name: "ScanCard", bundle: nil).instantiateViewController(withIdentifier: "ScanCard") as! ScanCard
        scanCard.providesPresentationContextTransitionStyle = true
        scanCard.definesPresentationContext = true
        scanCard.getDataIneDelegate = self
        scanCard.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        scanCard.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        UIApplication.topViewController()?.present(scanCard, animated: false, completion: nil)
        self.view.layoutIfNeeded()
    }

    @IBAction func addDocAction(_ sender: Any) {
        let scanProof: ProofDireccionViewController = UIStoryboard(name: "ProofDireccionViewController", bundle: nil).instantiateViewController(withIdentifier: "ProofDireccionViewController") as! ProofDireccionViewController
        scanProof.providesPresentationContextTransitionStyle = true
        scanProof.definesPresentationContext = true
        scanProof.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        scanProof.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        UIApplication.topViewController()?.present(scanProof, animated: false, completion: nil)
        self.view.layoutIfNeeded()
    }

    @IBAction func nameAction(_ sender: Any) {
        let viewV = UIStoryboard(name: "NamePopViewController", bundle: nil)
        let controller = viewV.instantiateViewController(withIdentifier: "NamePopViewController") as! NamePopViewController
        controller.providesPresentationContextTransitionStyle = true
        controller.definesPresentationContext = true
        controller.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        controller.mNameObject = TPDataDocumentsShared.shared.mNameObject
        UIApplication.topViewController()?.present(controller, animated: false, completion: nil)
    }

    @IBAction func fechaNacimientoAction(_ sender: UIButton) {
        setDatePicker()
    }

    @IBAction func numberAdittionalAction(_ sender: Any) {
        if self.addPhoneButton.isSelected == false {
            extraPhoneNumberTextField.text = extraNumberPrefix
            extraPhoneNumberTextField.isHidden = false
            addPhoneImageView.image = UIImage(named: "ImageClose")
            addPhoneLabel.text = "Eliminar número adicional"
            numberAditionalContraint.constant = 50
            numberAdditionalBootom.constant = 30
            alturaNumeroAddon = 80
            alturaTotal = alturaConstante + alturaNegocio + alturaNumeroAddon + alturaComprobante
            containerHeight.constant = alturaTotal
            addPhoneButton.isSelected = true
        } else {
            extraPhoneNumberTextField.text = ""
            extraPhoneNumberTextField.isHidden = true
            addPhoneImageView.image = UIImage(named: "Image_rede_add")
            addPhoneLabel.text = "Agregar número adicional"
            numberAditionalContraint.constant = 15
            numberAdditionalBootom.constant = 0
            alturaNumeroAddon = 0
            alturaTotal = alturaConstante + alturaNegocio + alturaNumeroAddon  + alturaComprobante
            containerHeight.constant = alturaTotal
            self.addPhoneButton.isSelected = false
        }
    }

    @IBAction func expressInstallAction(_ sender: Any) {
        ConsultAvailability()
    }

    @objc func alertControllerBackgroundTapped() {
        self.dismiss(animated: true, completion: nil)
    }

    override func viewDidAppear(_ animated: Bool) {
        fasePostPreguntas.Index = 2
        Amplitude.instance().logEvent("Show_DataClientScreen")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func OnSuccessConsultINEData(mConsultINEDataResponse: ConsultINEDataResponse) {
        if mConsultINEDataResponse.nombres != "" {
            mNameObject.name = mConsultINEDataResponse.nombres ?? ""
        }
        if mConsultINEDataResponse.primerApellido != "" {
            mNameObject.middleName = mConsultINEDataResponse.primerApellido ?? ""
        }
        if mConsultINEDataResponse.segundoApellido != "" {
            mNameObject.lastName = mConsultINEDataResponse.segundoApellido ?? ""
        }

        nameTextField.text = String("\(mNameObject.name!) \(mNameObject.middleName!) \(mNameObject.lastName!)")
        if mConsultINEDataResponse.fechaNacimiento != "" {
            bornDateTextField.text = mConsultINEDataResponse.fechaNacimiento
        }
        SwiftEventBus.post("DatosINE", sender: mConsultINEDataResponse)
        isUploaded = true
        idUploaded(isUploaded: true)
        checkFields()
    }

    func OnSuccessConsultAvailability(mConsultAvailabilityResponse: ConsultAvailabilityResponse) {
        expressSellImageView.image = UIImage(named: "image_rede_ok")
        expressSellLabel.text  = "Instalacion express disponible"
        expressSellButton.isUserInteractionEnabled = false
        faseDataConsult.isExpressAcepted = true
    }

    func notAvailability() {
        expressSellImageView.image = UIImage(named: "Img_error")
        expressSellLabel.text  = "Instalacion express NO disponible"
        expressSellButton.isUserInteractionEnabled = false
        faseDataConsult.isExpressAcepted = false
    }

    func ConsultAvailability() {
        mConsultAvailabilityPresenter.GetConsultAvailability(mCiudad: "", mDistrito: faseDataConsult.direction.Distrito, mRegion: faseDataConsult.direction.Region, mZona: faseDataConsult.direction.zona, mCluster: faseDataConsult.direction.Cluster, mConsultAvailabilityDelegate: self)
    }

    func getInfoINE(_ ineFront: String, _ frontINEImage: UIImage, _ ineBack: String, _ backINEImage: UIImage) {
        mDocs.ineFront = ineFront
        mDocs.ineBack = ineBack
        mConsultINEDataPresenter.GetConsultINEData(mId: ineFront, mIdReverso: ineBack, mConsultINEDataDelegate: self)
    }

    override func getPresenters() -> [BasePresenter]? {
        mConsultINEDataPresenter = ConsultINEDataPresenter(view: self)
        mConsultAvailabilityPresenter = ConsultAvailabilityPresenter(view: self)
        return [mConsultINEDataPresenter, mConsultAvailabilityPresenter]
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if textField == phoneNumberTextField {
            let protectedRange = NSRange(location: 0, length: firstNumberPrefix.length)
            let intersection = NSIntersectionRange(protectedRange, range)
            if intersection.length > 0 {
                return false
            }
            if range.location == firstNumberMaxLength + firstNumberPrefix.length-1 {
                return true
            }
            if range.location + range.length > firstNumberMaxLength + firstNumberPrefix.length-1 {
                return false
            }
            if range.location < firstNumberPrefix.length-1 {
                return false
            }
            return true
        } else if textField == extraPhoneNumberTextField {
            let protectedRange = NSRange(location: 0, length: extraNumberPrefix.length)
            let intersection = NSIntersectionRange(protectedRange, range)
            if intersection.length > 0 {
                return false
            }
            if range.location == extraNumberMaxLength + extraNumberPrefix.length-1 {
                return true
            }
            if range.location + range.length > extraNumberMaxLength + extraNumberPrefix.length-1 {
                return false
            }
            if range.location < extraNumberPrefix.length-1 {
                return false
            }
            return true
        }

        // All other textfields

        return true
    }

    func debtorCheck() {
        if faseDataConsult.isPaymentRiskConsulted == false {
            rfc  = UtilsTp().generatRFC(name: mNameObject.name, lastName: mNameObject.middleName, motherLastName: mNameObject.lastName, date: bornDateTextField.text!)
            mDataViewModel.consultaPaymentRisk(name: nameTextField.text!, bornDate: bornDateTextField.text!, email: emailTextField.text!, rfc: rfc, telefono: phoneNumberTextField.text!)
        }
    }

    @IBAction func textFieldDidEnd(_ sender: Any) {
        let senderTextField = sender as! MDCFilledTextField
        // VALIDATIONS
        if senderTextField == phoneNumberTextField {
            if validatePhoneNumber(phone: senderTextField.text ?? "", number: 1) {
                phoneNumberValidated = true
                senderTextField.normalTheme(type: 2)

            } else {
                phoneNumberValidated = false
                senderTextField.errorTheme(message: "Ingresa un número válido")
            }
        } else if senderTextField == extraPhoneNumberTextField {
            if validatePhoneNumber(phone: senderTextField.text ?? "", number: 2) {
                senderTextField.normalTheme(type: 2)
            } else {
                senderTextField.errorTheme(message: "Ingresa un número válido")
            }
        } else if senderTextField == emailTextField {
            if isValidEmail(email: emailTextField.text ?? " ") {
                emailValidated = true
                senderTextField.normalTheme(type: 2)
            } else {
                emailValidated = false
                senderTextField.errorTheme(message: "Ingresa un correo válido")
            }
        }

        if senderTextField.text?.isEmpty ?? true {
            senderTextField.errorTheme(message: "*Obligatorio")
        }

        faseDataConsult.isPaymentRiskConsulted = false
        checkFields()
    }

    func isValidEmail(email: String) -> Bool {
        let emailRegex = "(?:[\\p{L}0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[\\p{L}0-9!#$%\\&'*+/=?\\^_`{|}" +
        "~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\" +
        "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[\\p{L}0-9](?:[a-" +
        "z0-9-]*[\\p{L}0-9])?\\.)+[\\p{L}0-9](?:[\\p{L}0-9-]*[\\p{L}0-9])?|\\[(?:(?:25[0-5" +
        "]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-" +
        "9][0-9]?|[\\p{L}0-9-]*[\\p{L}0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21" +
        "-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: email)
    }

    // Validation RFC view show
    @IBAction func showRFCView(_ sender: Any) {
        let view = UIStoryboard(name: "DataCDFI", bundle: nil)
        let controller = view.instantiateViewController(withIdentifier: "DataCDFIViewController")
        controller.modalPresentationStyle = .overFullScreen
        self.present(controller, animated: true)
    }

}
