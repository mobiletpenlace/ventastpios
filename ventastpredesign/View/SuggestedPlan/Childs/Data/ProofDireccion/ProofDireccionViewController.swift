//
//  ProofDireccionViewController.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 05/07/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class ProofDireccionViewController: BaseVentasView, UIImagePickerControllerDelegate & UINavigationControllerDelegate {

    @IBOutlet weak var previewImage: UIImageView!
    var imageProofPickerController: UIImagePickerController!
    var imageBase64: String = ""
    var isFirst: Bool = true

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        if isFirst {
            take()
        }
    }

    @IBAction func AcceptAction(_ sender: Any) {
        SwiftEventBus.post("EnviaComprobante", sender: imageBase64)
        self.dismiss(animated: false, completion: nil)
    }

    @IBAction func tomarAction(_ sender: Any) {
        take()
    }

    func take() {
        isFirst = false
        let alert = UIAlertController(title: "Elige una opción", message: "", preferredStyle: .actionSheet)

        let action1 = UIAlertAction(title: "Tomar foto", style: .default, handler: { (_) -> Void in
        self.takePhoto()
        })
        let action2 = UIAlertAction(title: "Abrir de Galeria", style: .default, handler: { (_) -> Void in
        self.takeGalery()
        })

        let action3 = UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.cancel, handler: nil)
        action3.setValue(UIColor.red, forKey: "titleTextColor")

        alert.view.tintColor = UIColor.black
        alert.addAction(action1)
        alert.addAction(action2)
        alert.addAction(action3)

        present(alert, animated: true, completion: {
            alert.view.superview?.isUserInteractionEnabled = true
            alert.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertControllerBackgroundTapped)))
            alert.view.layoutIfNeeded()
        })
    }

    @objc func alertControllerBackgroundTapped() {
        self.dismiss(animated: true, completion: nil)
    }

    func takePhoto() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imageProofPickerController = UIImagePickerController()
            imageProofPickerController.delegate = self
            imageProofPickerController.sourceType = .camera
            self.present(imageProofPickerController, animated: true, completion: nil)
        }
    }

    func takeGalery() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            imageProofPickerController = UIImagePickerController()
            imageProofPickerController.delegate = self
            imageProofPickerController.sourceType = .photoLibrary
            self.present(imageProofPickerController, animated: true, completion: nil)
        }
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String: Any]) {
        imageProofPickerController.dismiss(animated: true, completion: nil)
        previewImage.alpha = 1
        previewImage.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        imageBase64 = convertImageTobase64(image: (info[UIImagePickerControllerOriginalImage] as? UIImage)!)!
    }

    func convertImageTobase64(image: UIImage) -> String? {
        var imageData: Data?
        var imageDataantes: Data?
        imageDataantes  = UIImageJPEGRepresentation(image, 1)
        let widthInPixels = image.size.width * image.scale
        let heightInPixels = image.size.height * image.scale
        let imageSize1: Int = imageDataantes!.count
        let heightTemp = (300 * heightInPixels) / widthInPixels
        imageData  = UIImageJPEGRepresentation(imageWithSize(image: image, size: CGSize(width: 300, height: heightTemp)), 1)
        // size image
        let imageSize: Int = imageData!.count
        return imageData?.base64EncodedString()
    }

    public func imageWithSize(image: UIImage, size: CGSize) -> UIImage {
        var scaledImageRect = CGRect.zero
        let aspectWidth: CGFloat = size.width / image.size.width
        let aspectHeight: CGFloat = size.height / image.size.height
        let aspectRatio: CGFloat = min(aspectWidth, aspectHeight)
        scaledImageRect.size.width = image.size.width * aspectRatio
        scaledImageRect.size.height = image.size.height * aspectRatio
        scaledImageRect.origin.x = (size.width - scaledImageRect.size.width) / 2.0
        scaledImageRect.origin.y = (size.height - scaledImageRect.size.height) / 2.0
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        image.draw(in: scaledImageRect)
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return scaledImage!
    }

}
