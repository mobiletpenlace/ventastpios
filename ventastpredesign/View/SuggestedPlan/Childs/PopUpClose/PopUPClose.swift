//
//  PopUPClose.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin cabrera on 21/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import SwiftEventBus
import Amplitude

class PopUPClose: BaseVentasView {

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
    }

    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }

    @IBAction func closeSellAction(_ sender: Any) {
        self.presentingViewController?.presentingViewController?.dismiss(animated: false, completion: nil)
    }

}
