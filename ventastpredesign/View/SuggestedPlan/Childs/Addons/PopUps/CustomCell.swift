//
//  CustomCell.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 11/05/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//
import UIKit
import Foundation
import SwiftEventBus

class CustomCell: UITableViewController {

    @IBOutlet var ButtonsCell: [UIButton]!
    var numberWiFi = 0
    var priceWiFi = 0

    var numberTV = 0
    var priceTV = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }

    @IBAction func SelectedAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        switch sender.tag {
        case 0:
            SwiftEventBus.post("WiFi", sender: 1)
        case 1:
            SwiftEventBus.post("WiFi", sender: 2)
        case 2:
            SwiftEventBus.post("WiFi", sender: 3)
        case 3:
            SwiftEventBus.post("WiFi", sender: 4)
        case 4:
            SwiftEventBus.post("WiFi", sender: 5)
        case 5:
            SwiftEventBus.post("WiFi", sender: 6)
        case 6:
            SwiftEventBus.post("TV", sender: 1)
        case 7:
            SwiftEventBus.post("TV", sender: 2)
        case 8:
            SwiftEventBus.post("TV", sender: 3)
        case 9:
            SwiftEventBus.post("TV", sender: 4)
        case 10:
           SwiftEventBus.post("TV", sender: 5)
        case 11:
            SwiftEventBus.post("TV", sender: 6)
        default:
            break
        }

    }

}
