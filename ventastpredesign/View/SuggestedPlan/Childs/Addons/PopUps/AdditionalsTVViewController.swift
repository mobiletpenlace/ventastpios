//
//  AdditionalsTVViewController.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 03/05/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import Foundation

class AdditionalsTVViewController: BaseVentasView {

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
    }

    @IBAction func closeAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}
