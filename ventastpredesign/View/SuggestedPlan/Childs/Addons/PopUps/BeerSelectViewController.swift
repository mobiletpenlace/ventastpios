//
//  BeerSelectViewController.swift
//  ventastp
//
//  Created by Branchbit on 02/12/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus
import Darwin

class BeerSelectViewController: UIViewController, UIScrollViewDelegate {

    deinit {
        print("PRUEBA DEINIT BEER")
    }

    @IBOutlet weak var promosStack: UIStackView!
    @IBOutlet weak var buttonClose: UIButton!

    // Placeholder to avoid autoresizing constraint conflicts
    @IBOutlet weak var auxView: UIView!

    var selectedPromoID: Int = -1

    var beers: [Beer]?
    var selectedBeer: Beer?

    var imagesDictionary: [String: UIImage] = [:]

    override func viewDidLoad() {
        super.viewDidLoad()

        // FORCE LIGHT MODE ////////////////////////////////////
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
        // FORCE LIGHT MODE ////////////////////////////////////

        // UI CONFIG /////////////////////////////////////////

        // hide placeholderview
        auxView.isHidden = true
        buttonClose.setTitle("", for: .normal)
        // UI CONFIG /////////////////////////////////////////

        scrollFill()

        // SET PREVIOUSLY SELECTED PROMO /////////////
        if let beer = selectedBeer {
            for subview in promosStack.subviews {
                if let cell = subview as? BeerTableViewCell {
                    if cell.beerID == Int(beer.id ?? -1) {
                        cell.setActive()
                    }
                }
            }
        }
        // SET PREVIOUSLY SELECTED PROMO /////////////

    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        SwiftEventBus.post("Beer", sender: selectedBeer)
        SwiftEventBus.unregister(self, name: "fillBeerList")
    }

    @IBAction func closeAction(_ sender: Any) {
        SwiftEventBus.unregister(self, name: "fillBeerList")
        self.dismiss(animated: true, completion: nil)
    }

    func scrollFill() {
        guard let beerArray = beers else {return}
        var aux = 0
        for beer in beerArray {
            if let cell = Bundle.main.loadNibNamed("BeerTableViewCell", owner: self, options: nil)?.first as? BeerTableViewCell {
                cell.index = aux
                cell.beerID = beer.id
                cell.beerImage.sd_setImage(with: URL(string: beer.image ?? ""), completed: .none)
                cell.nameLabel.text = beer.descripcion
                cell.priceLabel.text = "$\(beer.price ?? 0) /mes"

                cell.heightAnchor.constraint(equalToConstant: 112).isActive = true
                let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapped(recognizer:)))

                cell.addGestureRecognizer(tapGestureRecognizer)

                promosStack.addArrangedSubview(cell)
                cell.leadingAnchor.constraint(equalTo: cell.superview!.leadingAnchor).isActive = true
                cell.trailingAnchor.constraint(equalTo: cell.superview!.trailingAnchor).isActive = true
                aux += 1
            }
        }

    }

    @objc
    func tapped( recognizer: UITapGestureRecognizer) {
        if let view = recognizer.view as? BeerTableViewCell {
            let currentID = view.beerID!
            print("\(view.beerID ?? 0)")
            if view.isActive {
                view.setInactive()
                selectedPromoID = -1
                selectedBeer = nil
            } else {
                view.setActive()
            }

            for subview in promosStack.subviews {
                if let cell = subview as? BeerTableViewCell {
                    if cell.beerID != currentID {
                        cell.setInactive()
                    }
                }
            }

            for subview in promosStack.subviews {
                if let cell = subview as? BeerTableViewCell {
                    if cell.isActive {
                        selectedPromoID = cell.beerID ?? -1
                        if let beerArray = beers {
                            selectedBeer = beerArray[cell.index ?? 0]
                        }
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.15, execute: {
                            self.dismiss(animated: true)
                        })
                    }
                }
            }
        }
    }

}
