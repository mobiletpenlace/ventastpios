//
//  AddonsScrollViewModel.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin cabrera on 05/07/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus

protocol RecommendedPlanObserver {
    func succesGetProspects()
    func succesGetRecomendacionesIDs()
    func succesGetDetailPlans()
    func succesAddons()
    func onErrorEmploye()
    func actualizaTotalPrecio(precio: Float)
    func actualizaTotalProductos(total: Int)
    func actualizaServiciosResumen(servicios: [Servicio])
    func actualizaProductosResumen(productos: [Adicional])
    func actualizaPromocionesResumen(promociones: [Promocion])
    func ocultaServicios()
    func ocultaProductos()
    func ocultaPromociones()
    func retiraResumen()

}

class AddonsScrollViewModel: BaseViewModel, AddonsScrollModelObserver {
    func onSuccesConvivencia() {
        view.hideLoading()
        selectedPromociones.append(promoValidacion)

        if promoValidacion.adicionalProductoId != "" {
            agregaProductoIncluido(promoValidacion.adicionalProductoId)
        }
//        actualiza()
    }

    func onNotConvivencia(message: String) {
        view.hideLoading()
        Constants.Alert(title: "Aviso", body: message, type: type.Error, viewC: view)
        SwiftEventBus.post("RetiraPromoCelda", sender: promoValidacion.Id)
    }

    func agregaProductoIncluido(_ producto: String) {
        SwiftEventBus.post("AgregaProductoCelda", sender: producto)
    }

    func retiraProductoIncluido(_ producto: String) {
        SwiftEventBus.post("RetiraProductoCelda", sender: producto)
    }

    var prueba1 = ""
    var prueba2 = ""
    var idPerfilClte = ""
    var arrayIdServiciosRecomendados: [String] = []
    var arrayidServiciosAdd: [String] = []
    var arrayidpromocionesAdd: [String] = []

    var nombre = ""

    var mPlan: String!
    var mSitio: Sitio!
    var idPlan1 = ""
    var idPlan2 = ""
    var mProspecto: NProspecto!
    var mServicios: [Servicio] = []
    var mProductos: [Adicional] = []
    var mPromociones: [Promocion] = []
    var selectedServicios: [Servicio] = []
    var selectedProductos: [Adicional] = []
    var selectedPromociones: [Promocion] = []
    var CostoInstalacion: Float = 0.0
    var promoValidacion: Promocion!
    var infoAddons: InfoAdicionalesCreacion = InfoAdicionalesCreacion()
    var totalPrice: Float = 0.0
    var totalDiscount: Float = 0.0
    var totalProductos: Int = 0
    var mProductosAdd = ProductosAdd()
    var mPromocionesAdd = PromocionesAdd()
    var mServiciosAdd = ServiciosAdd()
    var i: Int!
    var j: Int!
    var planBestFit = ""

    let objetodeprueba1: LlenadoObjeto = LlenadoObjeto()
    let objetodeprueba2: LlenadoObjeto = LlenadoObjeto()

    func successProspect(response: InfoProfileClientResponse) {
        idPerfilClte = (response.infoClte?.idPerfilClte)!
        SwiftEventBus.post("receiveIDProfileCliente", sender: idPerfilClte)
        observer?.succesGetProspects()
    }

    func succcessRecommendations(response: RecommendationsResponse) {

        // PLANES ID PLANES
        for i in 0..<response.infoSalidaRecomendaciones!.recomendaciones.count {
            arrayIdServiciosRecomendados.append((response.infoSalidaRecomendaciones?.recomendaciones[i].PlanRecomendado)!)
        }

//         ID SERVICIOS ADD
//        AQUI HAY UN BUG
        for i in 0..<response.infoSalidaRecomendaciones!.recomendaciones.count {
            for j in 0..<(response.infoSalidaRecomendaciones!.recomendaciones[i].serviciosRecomendados?.servicios.count)! {
                arrayidServiciosAdd.append((response.infoSalidaRecomendaciones?.recomendaciones[i].serviciosRecomendados?.servicios[j].Id)!)
            }
        }

        // ID PROMOCIONES ADD
        response.infoSalidaRecomendaciones?.recomendaciones[0].promocionesRecomendadas?.promociones.forEach {element in
            print(element)
        }

        for i in 0..<(response.infoSalidaRecomendaciones?.recomendaciones.count)! {
            for j in 0..<(response.infoSalidaRecomendaciones?.recomendaciones[i].promocionesRecomendadas?.promociones.count)! {
                arrayidpromocionesAdd.append((response.infoSalidaRecomendaciones?.recomendaciones[i].promocionesRecomendadas?.promociones[j].Id)!)
            }
        }

        observer?.succesGetRecomendacionesIDs()
    }

    var observer: RecommendedPlanObserver!
    var model: RecommendedPlanModel!

    override init(view: BaseVentasView) {
        super.init(view: view)
        model = RecommendedPlanModel(observer: self)
        model.atachModel(viewModel: self)
    }

    func atachView(_ observer: RecommendedPlanObserver) {
        self.observer = observer
        SwiftEventBus.onMainThread(self, name: "Agrega_servicio") { [weak self] result in
            if let servicio = result?.object as? Servicio {
                self?.selectedServicios.append(servicio)
                self?.actualiza()
            }
        }

        SwiftEventBus.onMainThread(self, name: "Retira_servicio") { [weak self] result in
            if let servicio = result?.object as? Servicio {
                self?.eliminaServicio(servicio)
            }
        }

        SwiftEventBus.onMainThread(self, name: "Agrega_Addon") { [weak self] result in
            if let addon = result?.object as? Adicional {
                self?.selectedProductos.append(addon)
                self?.actualiza()
            }
            dump(self!.selectedProductos)
        }
        SwiftEventBus.onMainThread(self, name: "Retira_Addon") { [weak self] result in
            if let addon = result?.object as? Adicional {
                self?.eliminaAddon(addon)
            }
            dump(self!.selectedProductos)
        }

        SwiftEventBus.onMainThread(self, name: "Agrega_Promocion") { [weak self] result in
            if let promocion = result?.object as? Promocion {
                self?.validaConvivencia(promocion: promocion)
            }
        }

        SwiftEventBus.onMainThread(self, name: "Retira_Promocion") { [weak self] result in
            if let promocion = result?.object as? Promocion {
                self?.eliminaPromo(promocion)
            }
        }

        SwiftEventBus.onMainThread(self, name: "RetiraResumen") { [weak self] _ in
            self?.observer.retiraResumen()
        }

        SwiftEventBus.onMainThread(self, name: "RemoveEventBus") { [weak self] _ in
            self?.removeEventBus()
        }

    }

    func getInfoCliente() {
        view.showLoading()
        let mInfoProfileClientRequest: InfoProfileClientRequest = InfoProfileClientRequest()
        let pruebaInfo = Info2()
        let pruebaRespuestas = Respuestas()
        pruebaInfo.cluster = "PEDREGAL"
        pruebaRespuestas.appsFavoritas = fasePreguntas.favoriteApps
        pruebaRespuestas.proveedorInternet = fasePreguntas.internetProvider
        pruebaRespuestas.numHabitaciones = String(fasePreguntas.numberOfRooms)
        pruebaRespuestas.personasConectadas = String(fasePreguntas.numberOfPeople)
        pruebaRespuestas.proveedorTelefonia = fasePreguntas.phoneProvider
        pruebaRespuestas.proveedorTV = fasePreguntas.tvProvider
        pruebaRespuestas.UsosRed = fasePreguntas.usesInternet
        pruebaInfo.Respuestas = pruebaRespuestas
        mInfoProfileClientRequest.accion = "Creacion"
        mInfoProfileClientRequest.info = pruebaInfo
        model.getInfoCliente(mInfoProfileClientRequest)
    }

    func getRecomendacionesIDs() {
        let mRecommendationsRequest: RecommendationsRequest = RecommendationsRequest()
        let pruebasInData = InData()
        mRecommendationsRequest.accion = "ejecutarE"
        pruebasInData.canalFront = "AppVentas"
        pruebasInData.contextObjName = "Perfil_Cliente"
        pruebasInData.estimuloFiscal = false
        pruebasInData.esPostVenta = false
        pruebasInData.maxRecomm = "4"
        pruebasInData.nombreCanal = "MC"
        pruebasInData.plaza = "CIUDAD DE MEXICO"
        pruebasInData.contextObjId = idPerfilClte
        mRecommendationsRequest.inData = pruebasInData
        model.getRecomendacionesIDs(mRecommendationsRequest)
        observer?.succesGetDetailPlans()
    }

    // MARK: - devoluciones al view

    func getServicios() -> [Servicio] {
        return mServicios
    }

    func getProductos() -> [Adicional] {
        return mProductos
    }

    func getPromociones() -> [Promocion] {
        return mPromociones
    }

    func getCosto() -> Float {
        return CostoInstalacion
    }

    func successConsultDetail(response: ConsultaDetalleResponse) {

        if faseConfigurarPlan.adivinaQuien == true {
            response.info?.infoPlanes!.detallePlanes[0].servIncSinAgregar?.serviciosIncSinAgregar.forEach { element in
                if element.nombre?.contains(string: "WIFI") != false {
                    objetodeprueba1.wifi = "WIFI"
                }
            }

            response.info?.infoPlanes!.detallePlanes[0].servIncSinAgregar?.serviciosIncSinAgregar.forEach { element in
                if element.nombre?.contains(string: "Television") != false {
                    objetodeprueba1.tv = "TV"
                }
            }

             // Enviar Plan 1
            objetodeprueba1.precioProntoPago = (response.info?.infoPlanes?.detallePlanes[0].precioProntoPago) as! Int
            objetodeprueba1.arregloProductos = (response.info?.infoPlanes?.detallePlanes[0].productosIncluidos!.productos)!
            objetodeprueba1.streaming = (response.info?.infoPlanes?.detallePlanes[0].servicioStreaming) ?? "0"
            objetodeprueba1.Titulo = (response.info?.infoPlanes?.detallePlanes[0].nombrePaquete)!
            objetodeprueba1.Megas = (response.info?.infoPlanes?.detallePlanes[0].megas)!
            if (response.info?.infoPlanes?.detallePlanes[0].tvIncluidas.isEmpty)! {
                objetodeprueba1.numeroTVs = "0"
            } else {
            objetodeprueba1.numeroTVs = (response.info?.infoPlanes?.detallePlanes[0].tvIncluidas)!
            }

            SwiftEventBus.post("SetPlanPrimario", sender: objetodeprueba1)
            SwiftEventBus.post("receivePlan1", sender: response.info?.infoPlanes?.detallePlanes[0])
            faseSelectedPlan.plan1 = objetodeprueba1
        } else {

            response.info?.infoPlanes!.detallePlanes[0].servIncSinAgregar?.serviciosIncSinAgregar.forEach { element in
                if element.nombre?.contains(string: "WIFI") != false {
                    objetodeprueba1.wifi = "WIFI"
                }
            }

            response.info?.infoPlanes!.detallePlanes[0].servIncSinAgregar?.serviciosIncSinAgregar.forEach { element in
                if element.nombre?.contains(string: "Television") != false {
                    objetodeprueba1.tv = "TV"
                }
            }

            response.info?.infoPlanes!.detallePlanes[1].servIncSinAgregar?.serviciosIncSinAgregar.forEach { element in
                if element.nombre?.contains(string: "WIFI") != false {
                    objetodeprueba2.wifi = "WIFI"
                }
            }

            response.info?.infoPlanes!.detallePlanes[1].servIncSinAgregar?.serviciosIncSinAgregar.forEach { element in
                if element.nombre?.contains(string: "Television") != false {
                    objetodeprueba2.tv = "TV"
                }
            }

            // Enviar Plan 1
            objetodeprueba1.precioProntoPago = (response.info?.infoPlanes?.detallePlanes[0].precioProntoPago) as! Int
            objetodeprueba1.arregloProductos = (response.info?.infoPlanes?.detallePlanes[0].productosIncluidos!.productos)!
            objetodeprueba1.streaming = (response.info?.infoPlanes?.detallePlanes[0].servicioStreaming) ?? ""
            objetodeprueba1.Titulo = (response.info?.infoPlanes?.detallePlanes[0].nombrePaquete)!
            objetodeprueba1.Megas = (response.info?.infoPlanes?.detallePlanes[0].megas)!
            objetodeprueba1.numeroTVs = (response.info?.infoPlanes?.detallePlanes[0].tvIncluidas) ?? "0"

            // Enviar Plan 2
            objetodeprueba2.precioProntoPago = (response.info?.infoPlanes?.detallePlanes[1].precioProntoPago) as! Int
            objetodeprueba2.arregloProductos = (response.info?.infoPlanes?.detallePlanes[1].productosIncluidos!.productos)!
            objetodeprueba2.streaming = (response.info?.infoPlanes?.detallePlanes[1].servicioStreaming) ?? ""
            objetodeprueba2.Titulo = (response.info?.infoPlanes?.detallePlanes[1].nombrePaquete)!
            objetodeprueba2.Megas = (response.info?.infoPlanes?.detallePlanes[1].megas)!
            objetodeprueba2.numeroTVs = (response.info?.infoPlanes?.detallePlanes[1].tvIncluidas) ?? "0"

            if objetodeprueba1.precioProntoPago < objetodeprueba2.precioProntoPago {
                SwiftEventBus.post("SetPlanPrimario", sender: objetodeprueba1)
                SwiftEventBus.post("SetPlanSecundario", sender: objetodeprueba2)
                SwiftEventBus.post("receivePlan1", sender: response.info?.infoPlanes?.detallePlanes[0])
                SwiftEventBus.post("receivePlan2", sender: response.info?.infoPlanes?.detallePlanes[1])
                faseSelectedPlan.plan1 = objetodeprueba1
                faseSelectedPlan.plan2 = objetodeprueba2
            } else {
                SwiftEventBus.post("SetPlanPrimario", sender: objetodeprueba2)
                SwiftEventBus.post("SetPlanSecundario", sender: objetodeprueba1)
                SwiftEventBus.post("receivePlan1", sender: response.info?.infoPlanes?.detallePlanes[1])
                SwiftEventBus.post("receivePlan2", sender: response.info?.infoPlanes?.detallePlanes[0])
                faseSelectedPlan.plan2 = objetodeprueba1
                faseSelectedPlan.plan1 = objetodeprueba2
            }

        }

        view.hideLoading()
    }

    func getDetallePlanes() {
        let mConsultaDetalleRequest: ConsultaDetalleRequest = ConsultaDetalleRequest()
        let pruebasInfoConsultaDetallePaquetes = InfoConsultaDetallePaquetes()
        let pruebasAdicionales = Adicionales()
        let pruebasPlanes = Plans()

        pruebasPlanes.idPlanes = arrayIdServiciosRecomendados
        pruebasAdicionales.idServiciosAdd = arrayidServiciosAdd
        // Automatico
        pruebasAdicionales.idpromocionesAdd = arrayidpromocionesAdd

        // Manual
//        pruebasAdicionales.idpromocionesAdd.append("a0r3C000001mH4UQAU")
//        pruebasAdicionales.idpromocionesAdd.append("a0r3C000001lzGYQAY")
//        pruebasAdicionales.idpromocionesAdd.append("a0r3C000001mDUeQAM")
//        pruebasAdicionales.idpromocionesAdd.append("a0r3C000001mH5GQAU")
//        pruebasAdicionales.idpromocionesAdd.append("a0r3C000001mH4BQAU")
//        pruebasAdicionales.idpromocionesAdd.append("a0r3C000001mH5aQAE")
//
//
        pruebasInfoConsultaDetallePaquetes.adicionales = pruebasAdicionales
        pruebasInfoConsultaDetallePaquetes.estimuloFiscal = false
        pruebasInfoConsultaDetallePaquetes.planes = pruebasPlanes

        idPlan1 = (pruebasInfoConsultaDetallePaquetes.planes?.idPlanes[0])!
        idPlan2 = (pruebasInfoConsultaDetallePaquetes.planes?.idPlanes[1])!
        pruebasInfoConsultaDetallePaquetes.planes?.idPlanes[1]
        mConsultaDetalleRequest.accion = "Consulta"
        mConsultaDetalleRequest.info = pruebasInfoConsultaDetallePaquetes
        model.getDetallePlanes(mConsultaDetalleRequest)
        observer?.succesGetDetailPlans()

    }

    func getDetallePlanesBestFit(plan: String) {
        let mConsultaDetalleRequest: ConsultaDetalleRequest = ConsultaDetalleRequest()
        let pruebasInfoConsultaDetallePaquetes = InfoConsultaDetallePaquetes()
        let pruebasAdicionales = Adicionales()
        let pruebasPlanes = Plans()
        pruebasPlanes.idPlanes.append(plan)
//        pruebasPlanes.idPlanes.append("a0k3C000001sI1RQAU")
//        dump(pruebasPlanes.idPlanes)
//        print("aqui")
//        pruebasAdicionales.idServiciosAdd = arrayidServiciosAdd
//        pruebasAdicionales.idServiciosAdd.append("a0j3C000003OVsJQAW")
//        pruebasAdicionales.idServiciosAdd.append("a0j3C000003OVuXQAW")
//        pruebasAdicionales.idpromocionesAdd.append("a0r3C000001mH4AQAU")
//        pruebasAdicionales.idpromocionesAdd.append("a0r3C000001mH5ZQAU")
//        pruebasAdicionales.idpromocionesAdd.append("a0r3C000001lzGtQAI")

        pruebasInfoConsultaDetallePaquetes.adicionales = pruebasAdicionales
        pruebasInfoConsultaDetallePaquetes.estimuloFiscal = false
        pruebasInfoConsultaDetallePaquetes.planes = pruebasPlanes
        idPlan1 = (pruebasInfoConsultaDetallePaquetes.planes?.idPlanes[0])!
        mConsultaDetalleRequest.accion = "Consulta"
        mConsultaDetalleRequest.info = pruebasInfoConsultaDetallePaquetes
        model.getDetallePlanes(mConsultaDetalleRequest)
        observer?.succesGetDetailPlans()

    }

    func BuscaAdoons() {
//        mPlan = plan
        var request = ConsultaAddonsRequest()

        if faseSelectedPlan.firstPlan == true {
            request.idPlan = idPlan1
            SwiftEventBus.post("receiveIDPlan", sender: idPlan1)
        } else {
            request.idPlan = idPlan2
            SwiftEventBus.post("receiveIDPlan", sender: idPlan2)
        }
//        request.idPlan = plan
        mSitio = model.getSitio()
        mProspecto = model.getProspecto()
        request.info.canal = mProspecto.canalEmpleado
        request.info.canalFront = "AppVentasBestFitPiloto"
        request.info.cluster = mSitio.cluster
        request.info.colonia = mProspecto.colonia
        request.info.distrito = mSitio.distrito
        request.info.metodoPago = "Efectivo"
        request.info.plaza = mSitio.plaza
        request.info.codigoPostal = mProspecto.codigoPostal
        request.info.subcanal = mProspecto.subCanalEmpleado
        request.info.subcanalVenta = "Cambaceo"
        request.info.consultaAddons = false
        request.info.consultaServ = false
        request.info.consultaPromo = true
        if mSitio.isEstimulo == "true" {
            request.info.estimulo = true
        } else {
            request.info.estimulo = false
        }
        model.chargeAddons(request)
    }

    func succesAddons(response: ConsultaAddonsResponse) {
//        mServicios = response.infoAddons.serviciosAdd?.servicios as! [Servicio]

//        for agrupacion in response.infoAddons.productosAdd!.productos{
//            if agrupacion.Agrupacion != "HOGAR SEGURO"{
//                for adicional in agrupacion.adicional{
//                    mProductos.append(adicional)
//                }
//            }
//        }
//
//        CostoInstalacion = 0.0
//
//        for agrupacion in response.infoAddons.costoInstalacionAdd!.productos{
//            if agrupacion.Agrupacion != "HOGAR SEGURO"{
//                for adicional in agrupacion.adicional{
//                    CostoInstalacion = CostoInstalacion + adicional.precio
//                }
//            }
//        }

        mPromociones = response.infoAddons.promocionesAdd?.promociones as! [Promocion]
        dump(mPromociones)
        observer?.succesAddons()
        view.hideLoading()

    }

    func actualiza() {
        totalPrice = 0.0
        totalProductos = 0
        totalDiscount = 0.0

        for servicio in selectedServicios {
            totalPrice += servicio.precio.rounded()
        }

        for producto in selectedProductos {
            totalPrice += producto.precio.rounded()
        }

        for promociones in selectedPromociones {
            totalDiscount += promociones.montoDescuento.rounded()
        }

//        totalPrice += mPlan.precioProntoPago.rounded()
        totalPrice = (totalPrice + CostoInstalacion) - totalDiscount
        totalProductos = selectedServicios.count + selectedProductos.count + selectedPromociones.count + 1

        if selectedServicios.count > 0 {
            var acumulatedServicios: [Servicio] = []
            for servicio in selectedServicios {
                var conteo: Int = 0
                for serv in selectedServicios {
                    if servicio.Id == serv.Id {
                        conteo += 1
                    }
                }
                if conteo > 1 {
                    if acumulatedServicios.count == 0 {
                        let auxServ = servicio
                        auxServ.cantidad = conteo
                        acumulatedServicios.append(auxServ)
                    } else {
                        var exist: Bool = false
                        for serv in acumulatedServicios {
                            if serv.Id == servicio.Id {
                                exist = true
                            }
                        }
                        if !exist {
                            let auxServ = servicio
                            auxServ.cantidad = conteo
                            acumulatedServicios.append(auxServ)
                        }
                    }
                } else {
                    let auxServ = servicio
                    auxServ.cantidad = 1
                    acumulatedServicios.append(auxServ)
                }

            }
            observer.actualizaServiciosResumen(servicios: acumulatedServicios)
        } else {
            observer.ocultaServicios()
        }

        if selectedProductos.count > 0 {
            observer.actualizaProductosResumen(productos: selectedProductos)
        } else {
            observer.ocultaProductos()
        }

        if selectedPromociones.count > 0 {
            observer.actualizaPromocionesResumen(promociones: selectedPromociones)
        } else {
            observer!.ocultaPromociones()
        }

        observer.actualizaTotalPrecio(precio: totalPrice)
        observer.actualizaTotalProductos(total: totalProductos)
    }

    func eliminaServicio(_ servicio: Servicio) {
        var count = 1
        let aux = selectedServicios
        selectedServicios = []
        for mServicio in aux {
            if count > 0 {
                if mServicio.Id == servicio.Id {
                    count = 0
                } else {
                    selectedServicios.append(mServicio)
                }
            } else {
                selectedServicios.append(mServicio)
            }
        }
//        actualiza()
    }

    func eliminaAddon(_ addon: Adicional) {
        let aux = selectedProductos
        selectedProductos = []
        for mAddon in aux {
            if mAddon.Id == addon.Id {
            } else {
                selectedProductos.append(mAddon)
            }
        }
//        actualiza()
    }

    func eliminaPromo(_ promo: Promocion) {
        let aux = selectedPromociones
        selectedPromociones = []
        for mAddon in aux {
            if mAddon.Id == promo.Id {
                if promo.adicionalProductoId != "" {
                    retiraProductoIncluido(promo.adicionalProductoId)
                }
            } else {
                selectedPromociones.append(mAddon)
            }
        }
//        actualiza()
    }

    func validaConvivencia(promocion: Promocion) {
        promoValidacion = promocion
        var request: ConsultaConvivenciaRequest = ConsultaConvivenciaRequest()
        request.idPlan = idPlan1
        request.promocionesNew.append(promocion.Id)
        for promo in selectedPromociones {
            request.promocionesOld.append(promo.Id)
        }
        dump(request)
        model.getConvivencia(request)
    }

    func removeEventBus() {
        SwiftEventBus.unregister(self)
    }

}
