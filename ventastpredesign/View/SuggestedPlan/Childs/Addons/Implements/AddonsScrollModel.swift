//
//  AddonsScrollModel.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin cabrera on 05/07/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import RealmSwift

protocol AddonsScrollModelObserver {
    func succesAddons(response: ConsultaAddonsResponse)
    func onSuccesConvivencia()
    func onNotConvivencia(message: String)
}

class AddonsScrollModel: BaseModel {

    var mPlan: Plan!
    var mSitio: Sitio!
    var mProspecto: NProspecto!

    var mServicios: [Servicio] = []
    var mProductos: [Adicional] = []
    var mPromociones: [Promocion] = []
    var CostoInstalacion: Float = 0.0

    var viewModel: RecommendedPlanModelObserver?
    var server: ServerDataManager2<ConsultaAddonsResponse>?
    var server2: ServerDataManager2<ConsultaConvivenciaResponse>?

    var url = AppDelegate.API_SALESFORCE + ApiDefinition.API_ADDONS_VENTA
    var url2 = AppDelegate.API_SALESFORCE + ApiDefinition.API_CONVIVENCIA_PROMOCIONES
    init (observer: BaseViewModelObserver) {
        server = ServerDataManager2<ConsultaAddonsResponse>()
        server2 = ServerDataManager2<ConsultaConvivenciaResponse>()
        super.init(observerVM: observer)
    }

    func atachModel(viewModel: RecommendedPlanModelObserver) {
        self.viewModel = viewModel
    }

    func chargeAddons(_ request: ConsultaAddonsRequest) {
        url = AppDelegate.API_SALESFORCE + ApiDefinition.API_ADDONS_VENTA
        server?.setValues(requestUrl: url, delegate: self, headerType: .headersSalesforce)
        server?.request(requestModel: request)
    }

    func getConvivencia(_ request: ConsultaConvivenciaRequest) {
        url2 = AppDelegate.API_SALESFORCE + ApiDefinition.API_CONVIVENCIA_PROMOCIONES
        server2?.setValues(requestUrl: url2, delegate: self, headerType: .headersSalesforce)
        server2?.request(requestModel: request)
    }

    func getSitio() -> Sitio {
        var mSitio = Sitio()
        if RealManager.findFirst(object: Sitio.self) != nil {
            mSitio = Sitio(value: RealManager.findFirst(object: Sitio.self)!)
        }
        return mSitio
    }

    func getProspecto() -> NProspecto {
        var mProspecto = NProspecto()
        if RealManager.findFirst(object: NProspecto.self) != nil {
            mProspecto = NProspecto(value: RealManager.findFirst(object: NProspecto.self)!)
        }
        return mProspecto
    }

    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {

        if requestUrl == url {
            print("Obtener Addons")
            let mConsultaAddonsResponse = response as! ConsultaAddonsResponse
            if mConsultaAddonsResponse.result == "0" {
                viewModel?.succesAddons(response: mConsultaAddonsResponse)
            }
        }

        if requestUrl == url2 {
            print("Obtener convivencia")
            let mConsultaConvivenciaResponse = response as! ConsultaConvivenciaResponse
            if mConsultaConvivenciaResponse.result == "0" {
                viewModel?.onSuccesConvivencia()
            } else {
                viewModel?.onNotConvivencia(message: mConsultaConvivenciaResponse.resultDescription)
            }
        }

    }

}
