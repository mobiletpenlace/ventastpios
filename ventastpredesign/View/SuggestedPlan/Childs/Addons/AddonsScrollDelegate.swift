//
//  AddonsScrollDelegate.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 24/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import SwiftEventBus

extension AddonsScroll: RecommendedPlanObserver, BeersDelegate, UIScrollViewDelegate {
    func OnSuccesBeers(mBeers: BeersResponse) {
        self.mBeers = mBeers
    }

    func OnSuccessCoverage(mCoverage: BeerCoverageResponse) {
        // Valida si modeloID existe, si se recibió con la lista de servicios de salesforce
        if modeloID != nil {
            isBeerCoverageValid = true
            // HACER VISIBLE EL BOTON DE CERVEZA
            self.beerDetailLabel.isHidden = true
            self.beerPromoView.isHidden = false
            self.contentHeigth.constant += self.beerPromoView.height + 20
            self.topSpecialDiscounts.constant += self.beerPromoView.height + 20
            // CArgar lista de cervezas
            mBeersPresenter.getBeers(mBeersDelegate: self as BeersDelegate)
        }
    }

    // Función para setear el numero de WiFi Extender elegidos
    func setNumberOfWiFi(numero: Int) {
        if numero != 1 {
            devicesWiFiLabel.text = String(numero) + " Equipos agregados"
        } else {
            devicesWiFiLabel.text = String(numero) + " Equipo agregado"
        }
        agregaServicio(servicio: ExtenderAddicional, cantidad: numero)
    }

    // Función para setear el numero de TV's elegidas
    func setNumberOfTV(numero: Int) {
        if numero != 1 {
            devicesTVLabel.text = String(numero) + " Equipos agregados"
        } else {
            devicesTVLabel.text = String(numero) + " Equipo agregado"
        }
        agregaServicio(servicio: TvAddicional, cantidad: numero)
    }

    // Función para setear el numero de TV's elegidas
    func devicesWiFiSelected(selected: Bool) {
        if selected == true {
            devicesWiFiLabel.textColor = UIColor(named: "Base_rede_blue_textfields_wait")
            deleteWiFiButton.isHidden = false
        } else {
            devicesWiFiLabel.textColor = UIColor(named: "Base_rede_button_dark")
            deleteWiFiButton.isHidden = true
        }
    }

    // Función para setear el numero de TV's elegidas
    func devicesTVSelected(selected: Bool) {
        if selected == true {
            devicesTVLabel.textColor = UIColor(named: "Base_rede_blue_textfields_wait")
            deleteTVButton.isHidden = false
        } else {
            devicesTVLabel.textColor = UIColor(named: "Base_rede_button_dark")
            deleteTVButton.isHidden = true
        }
    }

    func setServicios() {
        for servicio in mServicios {
            if servicio.nombre.contains(string: "TV ADICIONAL") || servicio.nombre.contains(string: "Television Adicional") {
                TvAddicional.Id = servicio.Id
                TvAddicional.precio = servicio.precio
                TvAddicional.maximoAgregar = servicio.maximoAgregar
            }
            if servicio.nombre.contains(string: "EXTENDER") {
                ExtenderAddicional.Id = servicio.Id
                ExtenderAddicional.precio = servicio.precio
                ExtenderAddicional.maximoAgregar = servicio.maximoAgregar
            }
            if servicio.nombre.contains(string: "Modelo") {
                modeloID = servicio.Id
            }

        }
    }

    func setAditionals() {
        for premium in mProductosPremium.adicional {
            if premium.nombre.contains(string: "HBO") {
                CanalHBO.Id = premium.Id
                CanalHBO.precio = premium.precio
                CanalHBO.nombre = premium.nombre
                CanalHBO.maximoAgregar = premium.maximoAgregar
                CanalHBO.imagen = premium.imagen
            }
            if premium.nombre.contains(string: "STAR") {
                CanalSTAR.Id = premium.Id
                CanalSTAR.precio = premium.precio
                CanalSTAR.nombre = premium.nombre
                CanalSTAR.maximoAgregar = premium.maximoAgregar
                CanalSTAR.imagen = premium.imagen
            }
            if premium.nombre.contains(string: "Universal") {
                CanalUNIVERSAL.Id = premium.Id
                CanalUNIVERSAL.precio = premium.precio
                CanalUNIVERSAL.nombre = premium.nombre
                CanalUNIVERSAL.maximoAgregar = premium.maximoAgregar
                CanalUNIVERSAL.imagen = premium.imagen
            }
        }

        for parrilla in mProductosParrillas.adicional {
            dump(parrilla.Id)
            if parrilla.nombre.contains(string: "TV BÁSICA") {
                PromocionCanalesIncluidos.Id = parrilla.Id
                PromocionCanalesIncluidos.precio = parrilla.precio
                PromocionCanalesIncluidos.nombre = parrilla.nombre
                PromocionCanalesIncluidos.maximoAgregar = parrilla.maximoAgregar
                PromocionCanalesIncluidos.imagen = parrilla.imagen
            }
            if parrilla.nombre.contains(string: "TV AVANZADA") {
                PromocionCanalesAvanzada.Id = parrilla.Id
                PromocionCanalesAvanzada.precio = parrilla.precio
                PromocionCanalesAvanzada.nombre = parrilla.nombre
                PromocionCanalesAvanzada.maximoAgregar = parrilla.maximoAgregar
                PromocionCanalesAvanzada.imagen = parrilla.imagen
            }
            if parrilla.nombre.contains(string: "TV PREMIUM") {
                PromocionCanalesPremium.Id = parrilla.Id
                PromocionCanalesPremium.precio = parrilla.precio
                PromocionCanalesPremium.nombre = parrilla.nombre
                PromocionCanalesPremium.maximoAgregar = parrilla.maximoAgregar
                PromocionCanalesPremium.imagen = parrilla.imagen
            }
        }
    }

    func llenaScroll(arreglo: [NSObject], identifier: String, anchoCelda: CGFloat, altoCelda: CGFloat, anchoScroll: CGFloat, altoScroll: CGFloat, scroll: UIScrollView) {
        var contador = 0
        contador =  arreglo.count
        scroll.contentSize = CGSize(width: anchoCelda, height: CGFloat(altoCelda) * CGFloat(contador))
        for index in 0 ..< contador {
            if let view: CuentaCollectionReusableView = Bundle.main.loadNibNamed(identifier, owner: self, options: nil)?.first as? CuentaCollectionReusableView {
//                view.setData(arreglo[index])
                scroll.addSubview(view)
                view.frame.size.height = CGFloat(altoCelda)
                view.frame.size.width = CGFloat(anchoCelda)
                view.frame.origin.y = CGFloat(index) * CGFloat(altoCelda)
            }
            if let view: CuentaCollectionReusableView = Bundle.main.loadNibNamed(identifier, owner: self, options: nil)?.first as? CuentaCollectionReusableView {
//                view.setData(arreglo[index])
                scroll.addSubview(view)
                view.frame.size.height = CGFloat(altoCelda)
                view.frame.size.width = CGFloat(anchoCelda)
                view.frame.origin.y = CGFloat(index) * CGFloat(altoCelda)
            }
        }
    }

/*    Scroll Horizontal, para elegir secciones
    Función para darle apariencia al botón de que está seleccionado y mover el scroll a la posición de la sección deseada
*/
    func selectSectionScroll(button: UIButton, positionX: Int, positionY: Int) {
        self.scrollView.setContentOffset(CGPoint(x: positionX, y: positionY), animated: true)
        button.backgroundColor = UIColor(named: "Base_rede_blue_ligth")
        button.isSelected = true
    }

    // Función para quitar el diseño de los botones seleccionados en el scroll horizontal
    func deselectAll() {
        self.wifiExtenderButton.isSelected = false
        self.tvButton.isSelected = false
        self.moreChannelsButton.isSelected = false
        self.tvPremiumButton.isSelected = false
        self.specialDisscountsButton.isSelected = false
        self.wifiExtenderButton.backgroundColor = UIColor(named: "Base_rede_semiclear_gray")
        self.tvButton.backgroundColor = UIColor(named: "Base_rede_semiclear_gray")
        self.moreChannelsButton.backgroundColor = UIColor(named: "Base_rede_semiclear_gray")
        self.tvPremiumButton.backgroundColor = UIColor(named: "Base_rede_semiclear_gray")
        self.specialDisscountsButton.backgroundColor = UIColor(named: "Base_rede_semiclear_gray")
    }

    // Función para aplicar el diseño del paquete de canales incluido
    func onChannelsInclude() {
    self.includeChanelsButton.isSelected = true
    self.includeChannelsView.borderColor = UIColor(named: "Base_rede_button_dark")
    self.includeChannelsView.borderWidth = 3
        addCheck(viewParent: channelsView, viewChild: includeChannelsView)
    }

    // Función para aplicar el diseño del pack1 de canales
    func onChanelsPack1() {
        self.pack1ChanelsView.borderColor = UIColor(named: "Base_rede_button_dark")
        self.pack1ChanelsView.borderWidth = 3
        addCheck(viewParent: channelsView, viewChild: pack1ChanelsView)
        self.pack1ChanelsButton.isSelected = true
    }

    // Función para aplicar el diseño del pack2 de canales
    func onChanelsPack2() {
        self.pack2ChanelsView.borderColor = UIColor(named: "Base_rede_button_dark")
        self.pack2ChanelsView.borderWidth = 3
        addCheck(viewParent: channelsView, viewChild: pack2ChanelsView)
        self.pack2ChanelsButton.isSelected = true
    }

    // Función para limpiar el diseño de los canales seleccionados
    func offChannels() {
        eliminaAddon(PromocionCanalesIncluidos)
        eliminaAddon(PromocionCanalesAvanzada)
        eliminaAddon(PromocionCanalesPremium)
        self.pack1ChanelsButton.isSelected = false
        self.pack2ChanelsButton.isSelected = false
        self.includeChanelsButton.isSelected = false
        self.includeChannelsView.borderColor = UIColor(named: "Base_rede_semiclear_gray")
        self.includeChannelsView.borderWidth = 2
        self.pack1ChanelsView.borderColor = UIColor(named: "Base_rede_semiclear_gray")
        self.pack1ChanelsView.borderWidth = 2
        self.pack2ChanelsView.borderColor = UIColor(named: "Base_rede_semiclear_gray")
        self.pack2ChanelsView.borderWidth = 2
        myImageView.removeFromSuperview()
    }

    // Función para limpiar el diseño de los canales premium
    func offPremiums(name: String) {

        switch name {
        case "hbo":
            self.hboPremiumButton.isSelected = false
            self.hboPremiumButton.borderColor = UIColor(named: "Base_rede_semiclear_gray")
            self.hboPremiumButton.borderWidth = 2
        case "star":
            self.starPremiumButton.isSelected = false
            self.starPremiumButton.borderColor = UIColor(named: "Base_rede_semiclear_gray")
            self.starPremiumButton.borderWidth = 2
        case "universal":
            self.universalPremiumButton.isSelected = false
            self.universalPremiumButton.borderColor = UIColor(named: "Base_rede_semiclear_gray")
            self.universalPremiumButton.borderWidth = 2
        default:
            break
        }
//        if(hbo){
//            self.hboPremiumButton.isSelected = false
//            self.hboPremiumButton.borderColor = UIColor(named: "Base_rede_semiclear_gray")
//            self.hboPremiumButton.borderWidth = 2
//        }else{
//            self.starPremiumButton.isSelected = false
//            self.starPremiumButton.borderColor = UIColor(named: "Base_rede_semiclear_gray")
//            self.starPremiumButton.borderWidth = 2
//        }
    }
    // Función para llenar el view y tome un tamaño dinámico
    func llenaViewSpecialDiscounts(arreglo: [NSObject], identifier: String, anchoCelda: CGFloat, altoCelda: CGFloat, container: UIView, clase: Int) {
        var contador = 0
        contador =  arreglo.count
        for index in 0 ..< contador {
            if let view: BaseCollectionViewCell = Bundle.main.loadNibNamed(identifier, owner: self, options: nil)?.first as? BaseCollectionViewCell {
                view.setData(arreglo[index])
                container.addSubview(view)
                view.frame.size.height = CGFloat(altoCelda)
                view.frame.size.width = CGFloat(anchoCelda)
                view.frame.origin.y = CGFloat(index) * CGFloat(altoCelda)
            }
        }
        insideHeigth.constant = CGFloat(contador) * altoCelda
        promocionesheigth.constant = promocionesheigth.constant + insideHeigth.constant
    }

    func llenaViewOtherProducts(arreglo: [NSObject], identifier: String, anchoCelda: CGFloat, altoCelda: CGFloat, container: UIView, clase: Int) {
        var contador = 0
        contador =  arreglo.count
        for index in 0 ..< contador {
            if let view: BaseCollectionViewCell = Bundle.main.loadNibNamed(identifier, owner: self, options: nil)?.first as? BaseCollectionViewCell {
                view.setData(arreglo[index])
                container.addSubview(view)
                view.frame.size.height = CGFloat(altoCelda)
                view.frame.size.width = CGFloat(anchoCelda)
                view.frame.origin.y = CGFloat(index) * CGFloat(altoCelda)
            }
        }
        insideOtherProductsHeigth.constant = CGFloat(contador) * altoCelda
        otherProductsHeigth.constant = otherProductsHeigth.constant + insideOtherProductsHeigth.constant
    }

    func dimensionar() {

        if faseSelectedPlan.firstPlan == true {
            if faseSelectedPlan.plan1.familiaPaquete.contains(string: "Doble") == true {
                doublePlayPlan()
            }
        } else {
            if faseSelectedPlan.plan2.familiaPaquete.contains(string: "Doble") == true {
                doublePlayPlan()
            }
        }

        if faseConfigurarPlan.isNegocio == true {
            otherProductsView.isHidden = false
            contentHeigth.constant = wifiExtenderView.height + tvAditionalView.height + channelsView.height + tvPremiumView.height + promocionesheigth.constant + otherProductsHeigth.constant + 130
//            contentHeigth.constant += beerPromoView.height + 20

        } else {
            otherProductsView.isHidden = true
            otherProductsHeigth.constant = 0
            contentHeigth.constant = wifiExtenderView.height + tvAditionalView.height + channelsView.height + tvPremiumView.height + promocionesheigth.constant + otherProductsHeigth.constant + 130
//            contentHeigth.constant += beerPromoView.height + 20

        }
    }

    func agregaPromo(promocion: Promocion) {
        let promo: PromocionesCreacion = PromocionesCreacion()
        priceDiscount = priceDiscount + Int(promocion.montoDescuento)
        promo.id = promocion.Id
        if promocion.esAutomatica == "true" {
            promo.esAutomatica = true
        } else {
            promo.esAutomatica = false
        }
        selectedPromociones.append(promo)
        total()
    }

    func eliminaPromo(promocion: Promocion) {
        let aux = selectedPromociones
        selectedPromociones =  []
        for promo in aux {
            if promo.id == promocion.Id {
                priceDiscount = priceDiscount - Int(promocion.montoDescuento)
            } else {
                selectedPromociones.append(promo)
            }
        }
        total()
    }

    func agregaServicio(servicio: Servicio, cantidad: Int) {
        let serv: ServiciosCreacion = ServiciosCreacion()
        priceServicios = priceServicios + (Int(servicio.precio) * cantidad)
        serv.id = servicio.Id
        serv.cantidad = String("\(cantidad)")
        Logger.blockSeparator()
        Logger.println(serv.cantidad ?? "")
        selectedServicios.append(serv)
        total()
    }

    func eliminaServicio(servicio: Servicio) {
        let aux = selectedServicios
        selectedServicios = []
        for mServ in aux {
            if servicio.Id == mServ.id {
                priceServicios = priceServicios - (Int(servicio.precio) * (mServ.cantidad! as NSString).integerValue)
            } else {
                selectedServicios.append(mServ)
            }
        }
        total()
    }

    func eliminaAddon(_ addon: Adicional) {
        let aux = selectedProductos
        selectedProductos = []
        for mAddon in aux {
            if mAddon.id == addon.Id {
                priceProductos = priceProductos - Int(addon.precio)
            } else {
                selectedProductos.append(mAddon)
            }
        }
        total()
    }

    func agregaAddon(adicional: Adicional) {
        priceProductos = priceProductos + Int(adicional.precio)
        let producto: ProductosCreacion = ProductosCreacion()
        producto.id = adicional.Id
        selectedProductos.append(producto)
        total()
    }

    func total() {
        SwiftEventBus.post("setTotal", sender: priceServicios + priceProductos)
        SwiftEventBus.post("setDiscount", sender: priceDiscount)
        SwiftEventBus.post("actualizarPrecio")
    }

    func guardaAddons() {
        if RealManager.findFirst(object: InfoAdicionalesCreacion.self) != nil {
            RealManager.deleteAll(object: InfoAdicionalesCreacion.self)
        }
        mServiciosAdd = ServiciosAdd()
        mProductosAdd = ProductosAdd()
        mPromocionesAdd = PromocionesAdd()
        infoAddons = InfoAdicionalesCreacion()
        for servicio in selectedServicios {
            let servicioCrea: ServiciosCreacion = ServiciosCreacion()
            servicioCrea.cantidad = String("\(servicio.cantidad!)")
            servicioCrea.id = servicio.id
            servicioCrea.idExternoGM = servicio.idExternoGM
            mServiciosAdd.servicios.append(servicioCrea)
        }
        for producto in selectedProductos {
            let productoCrea: ProductosCreacion =  ProductosCreacion()
            productoCrea.id = producto.id
            mProductosAdd.productos.append(productoCrea)
        }
        for promocion in selectedPromociones {
            let promocionCrea: PromocionesCreacion = PromocionesCreacion()
            promocionCrea.id = promocion.id
            promocionCrea.requiereAutomatica = false
            if promocion.esAutomatica {
                promocionCrea.esAutomatica = true
            } else {
                promocionCrea.esAutomatica = false
            }
            mPromocionesAdd.promociones.append(promocionCrea)
        }
        if let beer = selectedBeer {
            let beerService: ServiciosCreacion = ServiciosCreacion()
            beerService.id = modeloID
            beerService.cantidad = String(1)
            beerService.idExternoGM = String(beer.productID!)
            beerExtId.idExternoGM = String(beer.productID!)
            beerExtId.idModelo = modeloID ?? ""
            mServiciosAdd.servicios.append(beerService)
        }
        infoAddons.productosAdd = mProductosAdd
        infoAddons.serviciosAdd = mServiciosAdd
        infoAddons.promocionesAdd = mPromocionesAdd
        // print("INFOAADDDONS: \(infoAddons.description)")
        setInfoAddons(infoAddons)
    }

    func setInfoAddons(_ infoAddons: InfoAdicionalesCreacion) {
        mInfo = InfoAdicionalesCreacion()
        mInfo = infoAddons
        for servicio in mInfo!.serviciosAdd!.servicios {
            servicio.idExternoGM = beerExtId.idExternoGM
        }
        if RealManager.findFirst(object: InfoAdicionalesCreacion.self) != nil {
            RealManager.deleteAll(object: InfoAdicionalesCreacion.self)
            mInfo?.uui = "1"
            _ = RealManager.insert(object: mInfo!)
        } else {
            mInfo?.uui = "1"
            _ = RealManager.insert(object: mInfo!)
        }
        SwiftEventBus.post("Addons_cargados", sender: mInfo)
    }

    func beerPromoSelect(selectedBeer: Beer ) {
        beerExtId.isSelected = true
        self.changeBeerLabel.text = "Cambiar"
        self.beerPromoButtonView.borderColor = UIColor(named: "Base_rede_button_dark")
        self.beerPromoButtonView.borderWidth = 3
        self.beerDetailLabel.isHidden = false
        self.beerDiscountLabel.isHidden = false
        self.beerStackPrice.isHidden = false
        self.beerDetailLabel.text = selectedBeer.descripcion
        self.beerPriceLabel.text = "$\(selectedBeer.price!)"
        self.beerBrandImageView.isHidden = true
        self.beerPromoImageView.sd_setImage(with: URL(string: selectedBeer.image ?? "urlinvalida"), completed: .none)
    }

    func beerPromoDeselect() {
        beerExtId.isSelected = false
        self.beerPromoButtonView.borderColor = UIColor(named: "Base_rede_semiclear_gray")
        self.beerPromoButtonView.borderWidth = 2
        self.beerPromoImageView.image = UIImage(named: "modeloDoble")
        self.beerDetailLabel.isHidden = true
        self.beerBrandImageView.isHidden = false
        self.beerStackPrice.isHidden = true
        self.beerDiscountLabel.text = "Sin costo x 2 meses"
        self.beerDiscountLabel.isHidden = true
        self.changeBeerLabel.text = "Agregar"
    }

}
