//
//  TPAddonsSelectedShared.swift
//  SalesCloud
//
//  Created by Axl Estevez on 12/01/24.
//  Copyright © 2024 TotalPlay. All rights reserved.
//

import Foundation

public class TPAddonsSelectedShared {
    public static let shared = TPAddonsSelectedShared()

    var addonServiceCreationSelected: [ServiciosCreacion] = []
    var addonsProductSelected: [ProductosCreacion] = []
    var addonsPromoSelected: [PromocionesCreacion] = []
    var priceAddonsService: Int = 0
    var priceDiscount: Int = 0
    var infoAddonsCreacion: InfoAdicionalesCreacion?

    private init() {}

    func serviceNoEmpty() -> Bool {
        if self.addonServiceCreationSelected.count > 0 {
            return false
        } else {
            return true
        }
    }

    func productsNoEmpty() -> Bool {
        if self.addonsProductSelected.count > 0 {
            return false
        } else {
            return true
        }
    }

    func promosNoEmpty() -> Bool {
        if self.addonsPromoSelected.count > 0 {
            return false
        } else {
            return true
        }
    }

    func findAddonsPrev() -> InfoAdicionalesCreacion? {
        if self.infoAddonsCreacion != nil {
            return self.infoAddonsCreacion
        } else {
            return nil
        }
    }

    func cleanAddonsCreacion() {
        self.infoAddonsCreacion = nil
    }
}
