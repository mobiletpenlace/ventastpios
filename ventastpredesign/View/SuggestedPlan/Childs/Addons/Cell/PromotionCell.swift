//
//  PromotionCell.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 24/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class PromotionCell: BaseCollectionViewCell {
    @IBOutlet weak var nombreLabel: UILabel!
    @IBOutlet weak var cellButton: UIButton!
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var selectedImage: UIImageView!
    var isCheck: Bool = false
    var mPromocion: Promocion!
    let checkImage = UIImage(named: "Image_rede_blue_check.png")
    let myImageView2: UIImageView = UIImageView()
    override func awakeFromNib() {
        super.awakeFromNib()
        selectedImage.isHidden = true
        SwiftEventBus.onMainThread(self, name: "RemoveEventBus") { [weak self] _ in
            self?.removeEventBus()
        }

        SwiftEventBus.onMainThread(self, name: "AgregaPromoCelda") { [weak self] result in
            if let adicional = result?.object as? String {
                if self?.mPromocion.Id == adicional {
                    if !self!.isCheck {
                        self?.isCheck = !self!.isCheck
                        self?.agregapromo()
                    }
                }
            }
        }

        SwiftEventBus.onMainThread(self, name: "RetiraPromoCelda") { [weak self] result in
            if let promo = result?.object as? String {
                if self?.mPromocion.Id == promo {
                    if self!.isCheck {
                        self?.isCheck = !self!.isCheck
                        self?.retiraPromo()
                    }
                }
            }
        }
    }

    override func setData(_ arreglo: NSObject) {
        mPromocion = arreglo as? Promocion
        nombreLabel.text = mPromocion.nombre
    }

    @IBAction func selectAction(_ sender: Any) {
        if isCheck {
            isCheck = !isCheck
            retiraPromo()
        } else {
            isCheck = !isCheck
            agregapromo()
        }
    }

    func removeEventBus() {
        SwiftEventBus.unregister(self)
    }

    func retiraPromo() {
        self.cellButton.isSelected = false
        self.cellView.borderColor = UIColor(named: "Base_rede_semiclear_gray")
        self.cellView.borderWidth = 2
        selectedImage.isHidden = true
        SwiftEventBus.post("Retira_Promocion", sender: mPromocion)
    }

    func agregapromo() {
        self.cellView.borderColor = UIColor(named: "Base_rede_button_dark")
        self.cellView.borderWidth = 3
        self.cellButton.isSelected = true
        selectedImage.isHidden = false
        SwiftEventBus.post("valida_convivencia_Promocion", sender: mPromocion)

    }

}
