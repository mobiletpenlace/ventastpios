//
//  ChargeBackCollectionViewCell.swift
//  ventastp
//
//  Created by Armando Isais Olguin Cabrera  on 9/9/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class AditionalsCell: BaseCollectionViewCell {

    @IBOutlet weak var chargeBack: UILabel!
    @IBOutlet weak var mCuenta: UILabel!

    var listaArrCuentas: ListCuentaComisionVO!

    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func setData(_ arreglo: NSObject) {
        listaArrCuentas = arreglo as? ListCuentaComisionVO
        mCuenta.text = listaArrCuentas.cuenta
        chargeBack.text = listaArrCuentas.pago1
        // m2Factura.text = listaArrCuentas.pago2
    }

}
