//
//  PromotionCell.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 24/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class DevicesCell: BaseCollectionViewCell {

    @IBOutlet weak var cellButton: UIButton!
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setData(_ arreglo: NSObject) {
        titleLabel.text = "TITULO"
        titleLabel.text = "126.99"
    }

    @IBAction func selectAction(_ sender: Any) {
    }

    func removeEventBus() {
        SwiftEventBus.unregister(self)
    }

}
