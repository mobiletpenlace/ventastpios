//
//  BeerTableViewCell.swift
//  ventastp
//
//  Created by Branchbit on 03/12/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

class BeerTableViewCell: UITableViewCell {

    var index: Int?
    var isActive: Bool = false

    var beerID: Int?
    var beerProductID: Int?

    // var beerObj : Adicional
    @IBOutlet weak var visibleCell: UIView!
    @IBOutlet weak var beerImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var checkedImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()

        // Initialization code
    }

    func setActive() {
        self.isActive = true
        self.visibleCell.borderColor = UIColor(named: "Base_rede_button_dark")
        self.visibleCell.borderWidth = 3
        self.checkedImage.isHidden = false

    }

    func setInactive() {
        self.isActive = false
        self.visibleCell.borderColor = UIColor(named: "Base_rede_semiclear_gray")
        self.visibleCell.borderWidth = 2
        self.checkedImage.isHidden = true
    }

}
