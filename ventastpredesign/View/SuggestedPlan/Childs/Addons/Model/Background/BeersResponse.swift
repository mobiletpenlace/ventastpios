//
//  BeersResponse.swift
//  ventastp
//
//  Created by Ulises Ortega on 09/03/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class BeersResponse: BaseResponse {
    required init?(map: Map) {
    }

    override func mapping(map: Map) {

        result <- map["Result"]
        getProducts <- map["data.getProducts"]

    }

    var getProducts: [Beer]?

}
