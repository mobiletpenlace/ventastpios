//
//  Beer.swift
//  ventastp
//
//  Created by Ulises Ortega on 09/03/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class Beer: BaseResponse {
    required init?(map: Map) {
    }

    override func mapping(map: Map) {

        id <- map["id"]
        productID <- map["productID"]
        descripcion <- map["description"]
        size <- map["size"]
        price <- map["price"]
        image <- map["image"]

    }

    var id: Int?
    var productID: Int?
    var descripcion: String?
    var size: Int?
    var price: Int?
    var image: String?

}
