//
//  BeerCoverageResponse.swift
//  ventastp
//
//  Created by Ulises Ortega on 10/03/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class BeerCoverageResponse: BaseResponse {
    required init?(map: Map) {
    }

    override func mapping(map: Map) {

        checkCoverage <- map["checkCoverage"]

    }

    var checkCoverage: String?

}
