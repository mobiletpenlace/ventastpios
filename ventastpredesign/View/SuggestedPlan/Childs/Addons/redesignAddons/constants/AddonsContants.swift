//
//  AddonsContants.swift
//  SalesCloud
//
//  Created by Axl Estevez on 28/09/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation

var ADDONS_SERVICE_CREATION_SELECTED: [ServiciosCreacion] = []
var ADDONS_PRODUCT_SELECTED: [ProductosCreacion] = []
var ADDONS_PROMO_SELECTED: [PromocionesCreacion] = []
var PRICE_ADDONS_SERVICE: Int = 0
var PRICE_DISCOUNT: Int = 0

// MARK: funciones para addons de clase ServicioCreacion
func verifyAddonServicePreviousSelected(addonID: String) -> Bool {
    var isExist: Bool = false
    for service in ADDONS_SERVICE_CREATION_SELECTED {
        if service.id == addonID {
            isExist = true
            break
        }
    }
    return isExist
}

func deleteServiceAddons(addon: ServiciosCreacion) {
    var index: Int = 0
    for service in ADDONS_SERVICE_CREATION_SELECTED {
        if service.id == addon.id {
            ADDONS_SERVICE_CREATION_SELECTED.remove(at: index)
        } else {
            index += 1
        }
    }
}

func getServiceAddon(addonID: String) -> ServiciosCreacion {

    var serviceAddonCreacion: ServiciosCreacion?

    for service in ADDONS_SERVICE_CREATION_SELECTED {
        if service.id == addonID {
            serviceAddonCreacion = service
        }
    }
    return serviceAddonCreacion ?? ServiciosCreacion()
}

func updateAddonService(addonID: String, newValue: Int) {
    if verifyAddonServicePreviousSelected(addonID: addonID) {
        for service in ADDONS_SERVICE_CREATION_SELECTED {
            if service.id == addonID {
                service.cantidad = String(newValue)
                service.cantidadInt = newValue
                break
            }
        }
    }
}

// MARK: funciones para addons de clase ProductoCreacion
func verifyAddonProductPreviousSelected(addonID: String) -> Bool {
    var isExist: Bool = false
    for product in ADDONS_PRODUCT_SELECTED {
        if product.id == addonID {
            isExist = true
            break
        }
    }
    return isExist
}

func deleteProductAddons(addon: ProductosCreacion) {
    var index: Int = 0
    for product in ADDONS_PRODUCT_SELECTED {
        if product.id == addon.id {
            ADDONS_PRODUCT_SELECTED.remove(at: index)
        } else {
            index += 1
        }
    }
}

func getProductAddon(addonID: String) -> ProductosCreacion {

    var productAddonCreacion: ProductosCreacion?

    for product in ADDONS_PRODUCT_SELECTED {
        if product.id == addonID {
            productAddonCreacion = product
        }
    }
    return productAddonCreacion ?? ProductosCreacion()
}

// MARK: funciones para addons de clase PromocionesCreacion
func verifyAddonPromoPreviousSelected(addonID: String) -> Bool {
    var isExist: Bool = false
    for promo in ADDONS_PROMO_SELECTED {
        if promo.id == addonID {
            isExist = true
            break
        }
    }
    return isExist
}

func deletePromoAddons(addon: PromocionesCreacion) {
    var index: Int = 0
    for promo in ADDONS_PROMO_SELECTED {
        if promo.id == addon.id {
            ADDONS_PROMO_SELECTED.remove(at: index)
        } else {
            index += 1
        }
    }
}

func getPromoAddon(addonID: String) -> PromocionesCreacion {

    var promoAddonCreacion: PromocionesCreacion?

    for promo in ADDONS_PROMO_SELECTED {
        if promo.id == addonID {
            promoAddonCreacion = promo
        }
    }
    return promoAddonCreacion ?? PromocionesCreacion()
}
