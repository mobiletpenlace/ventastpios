//
//  AddonsScrollViewVCExtension.swift
//  SalesCloud
//
//  Created by aestevezn on 07/08/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

extension AddonsScrollViewVC {

    func showSpinnerView () {
        spinnerView = WaitingView(frame: CGRect(
            x: 0,
            y: 0,
            width: self.view.bounds.width,
            height: self.view.bounds.height - 100
        ))
        spinnerView.titleLabel!.text = "Consultando adicionales"
        self.view.addSubview(spinnerView)
    }

    func removeSpinnerView() {
        spinnerView.removeFromSuperview()
    }

    func searchWifiServices(services: ServiciosAddStruct) {
        let servicesArray = services.servicios
        var wifiProIsFound = false
        var wifiPro6IsFound = false
        for service in servicesArray {
            if service.nombre.lowercased() == "wifi pro" {
                makeWifiProView(wifiProService: service)
                wifiProIsFound = true
            }
            if service.nombre.lowercased() == "wifi pro 6" {
                makeWifiPro6View(wifiProService: service)
                wifiPro6IsFound = true
            }
        }

        hideButtonScroll(isFound: wifiProIsFound, buttonForHide: wifiProButton)
        hideButtonScroll(isFound: wifiPro6IsFound, buttonForHide: wifiPro6Button)
    }

    func searchClubWifi(productos: ProductosAddStruct) {
        let adicionalesArray = getServicesIP(productos: productos)
        var wifiClubIsFound = false
        for adicional in adicionalesArray {
            if adicional.nombre?.lowercased() == "club wifi" {
                makeClubWifi(clubWifi: adicional)
                wifiClubIsFound = true
                break
            }
        }
        hideButtonScroll(isFound: wifiClubIsFound, buttonForHide: clubWifi)
    }

    func getServicesIP(productos: ProductosAddStruct) -> [AdicionalStruct] {
        let productosArray = productos.productos
        var adicionalesArray: [AdicionalStruct] = []
        for producto in productosArray {
            if producto.Agrupacion?.lowercased() == "ip" {
                adicionalesArray = producto.adicional
                break
            }
        }

        return adicionalesArray
    }

    func makeClubWifi(clubWifi: AdicionalStruct) {
        let frame = CGRect(
            x: stackView.frame.minX,
            y: stackView.frame.minY,
            width: stackView.frame.width,
            height: 135
        )
        let clubWifiView = ClubWifiView(frame: frame)
        stackView.addArrangedSubview(clubWifiView)
        self.clubWifiView = clubWifiView
    }

    func makeWifiProView(wifiProService: ServicioStruct) {
        let frame = CGRect(
            x: stackView.frame.minX,
            y: stackView.frame.minY,
            width: stackView.frame.width,
            height: 135
        )
        let wifiView = GenericViewSusAdd(frame: frame)
        wifiView.setupView(
            title: "Wifi pro",
            subtitle: "Agrega wifi a cada rincón de tu hogar",
            image: "Image_rede_extender",
            service: "Wifi pro"
        )
        wifiView.setService(service: wifiProService)
        wifiView.maneger = self
        wifiView.addonsManager = self
        stackView.addArrangedSubview(wifiView)
        self.wifiProView = wifiView
    }

    func makeWifiPro6View(wifiProService: ServicioStruct) {
        let frame = CGRect(
            x: stackView.frame.minX,
            y: stackView.frame.minY,
            width: stackView.frame.width,
            height: 135
        )
        let wifiView = GenericViewSusAdd(frame: frame)
        wifiView.setupView(
            title: "Wifi pro 6",
            subtitle: "Agrega wifi a cada rincón de tu hogar",
            image: "wifi-pro-6-icon",
            service: "Wifi pro 6"
        )
        wifiView.setService(service: wifiProService)
        wifiView.maneger = self
        wifiView.addonsManager = self
        stackView.addArrangedSubview(wifiView)
        self.wifiPro6View = wifiView
    }

    func searchTvAdicional(service: ServiciosAddStruct) {
        let arrayServices = service.servicios
        var isFound = false
        for service in arrayServices {
            if service.nombre.lowercased() == "television adicional 129" {
                makeTVAdicionalView(servicioTV: service)
                isFound = true
                break
            }
        }
        hideButtonScroll(isFound: isFound, buttonForHide: tvAdicionalButton)
    }

    func makeTVAdicionalView(servicioTV: ServicioStruct) {
        let frame = CGRect(
            x: stackView.frame.minX,
            y: stackView.frame.minY,
            width: stackView.frame.width,
            height: 135
        )
        let tvAdicionalView = GenericViewSusAdd(frame: frame)
        tvAdicionalView.setupView(title: "TV Adicional", subtitle: "Agrega Totalplay TV a todas tus teles.", image: "Image_rede_TV_box", service: "TV adicional")
        tvAdicionalView.setService(service: servicioTV)
        tvAdicionalView.maneger = self
        tvAdicionalView.addonsManager = self
        stackView.addArrangedSubview(tvAdicionalView)
        self.tvAdicionalView = tvAdicionalView
    }

    func searchTVPremium(services: ServiciosAddStruct) {
        let arrayServices = services.servicios
        for service in arrayServices {
            if service.nombre.lowercased() == "video soundbox" && service.mostrarAddon {
                makeTvPremiumPlusView(servicioTV: service)
                break
            }
        }
    }

    func makeTvPremiumPlusView(servicioTV: ServicioStruct) {
        let frame = CGRect(
            x: stackView.frame.minX,
            y: stackView.frame.minY,
            width: stackView.frame.width,
            height: 135
        )
        let tvPlusView = GenericViewSusAdd(frame: frame)
        tvPlusView.maneger = self
        tvPlusView.addonsManager = self
        tvPlusView.setService(service: servicioTV)
        tvPlusView.setupView(title: "TV+ adicional", subtitle: "Agrega total a todas tus teles.", image: "Image_rede_TV_VSB", service: "TV+ adicional")
        stackView.addArrangedSubview(tvPlusView)
        self.tvAdicionalPlus = tvPlusView
    }

    func searchWebCam(services: ServiciosAddStruct) {
        let arrayServices = services.servicios
        for service in arrayServices {
            if service.nombre.lowercased() == "tv cam" && service.mostrarAddon {
                makeTvCam(servicioTvCam: service)
                break
            }
        }
    }

    func makeTvCam(servicioTvCam: ServicioStruct) {
        let frame = CGRect(
            x: stackView.frame.minX,
            y: stackView.frame.minY,
            width: stackView.frame.width,
            height: 135
        )
        let tvCamView = GenericViewSusAdd(frame: frame)
        tvCamView.maneger = self
        tvCamView.addonsManager = self
        tvCamView.setService(service: servicioTvCam)
        tvCamView.setupView(title: "Cámara web", subtitle: "TV Cam.", image: "web-cam-icon", service: "Cámara web")
        stackView.addArrangedSubview(tvCamView)
        self.tvCamView = tvCamView
    }

    func makeParrillas(productos: ProductosAddStruct) {
        let parrillas = getParrillas(productos: productos)
        let parrillasStack = ParrillasView()
        parrillasStack.delegate = self
        parrillasStack.setterAditionalArray(array: parrillas)
        stackView.addArrangedSubview(parrillasStack)
        self.parrilasView = parrillasStack
    }

    func getParrillas(productos: ProductosAddStruct) -> [AdicionalStruct] {
        let arrayProductos = productos.productos
        var parrillasArray: [AdicionalStruct] = []
        for producto in arrayProductos {
            if producto.Agrupacion?.lowercased() == "parrillas" {
                parrillasArray = producto.adicional
                break
            }
        }
        return parrillasArray
    }

    func verifyTvOnFocus(parrilla: AdicionalStruct) -> Bool {
        var isIn: Bool = false
        for name in tvPremiumFocusParrillas.tvInfocus {
            if parrilla.nombre?.lowercased() == name {
                isIn = true
                break
            }
        }
        return isIn
    }

    func makeTvPremium(productos: ProductosAddStruct, servicios: ServiciosAddStruct) {
        let chanelsAdicionales = getChanelsTvPremium(productos: productos)
        let chanelsServices = getChanelsInServicesArray(services: servicios)
        let tvPremiumChanels: [Any] = chanelsAdicionales + chanelsServices
        let tvPremiumStack = TvPremiumView(elementsArray: tvPremiumChanels, dService: self, dProducto: self)
        tvPremiumStack.translatesAutoresizingMaskIntoConstraints = true
        tvPremiumStack.widthAnchor.constraint(equalToConstant: stackView.width).isActive = true
        stackView.addArrangedSubview(tvPremiumStack)
        self.tvPremiumView = tvPremiumStack
    }

    func getChanelsInServicesArray(services: ServiciosAddStruct) -> [ServicioStruct] {
        let serviciosArray = services.servicios
        var chanels: [ServicioStruct] = []
        for service in serviciosArray {
            if service.nombre.lowercased() == "disney+" {
                chanels.append(service)
            }
            if service.nombre.lowercased() == "star+" {
                chanels.append(service)
            }
            if service.nombre.lowercased().contains(string: "combo") {
                chanels.append(service)
            }
        }
        return chanels
    }

    func getChanelsTvPremium(productos: ProductosAddStruct) -> [AdicionalStruct] {
        var tvArrayinAditionals: [AdicionalStruct] = []
        let productosArray = productos.productos
        for producto in productosArray {
            if producto.Agrupacion?.lowercased() == "canales" {
                for producto in producto.adicional {
                    if verifyTvOnFocus(parrilla: producto) && producto.tipoProducto?.lowercased() == "producto adicional"{
                        tvArrayinAditionals.append(producto)
                    }
                }
                break
            }
        }
        return tvArrayinAditionals
    }

    func makePromos(promociones: PromocionesAddStruct) {
        let promocionesStack = DescuentosEspecialesView(promos: promociones.promociones, delegate: self)
        promocionesStack.translatesAutoresizingMaskIntoConstraints = true
        promocionesStack.widthAnchor.constraint(equalToConstant: stackView.width).isActive = true
        stackView.addArrangedSubview(promocionesStack)
        self.descuentosViews = promocionesStack
    }

    func searchHogarInteligente(servicios: ServiciosAddStruct) {
        let servicesArray = servicios.servicios
        for service in servicesArray {
            if service.nombre.lowercased() == "camara inteligente" {
                makeHogarSeguroView(hogarSeguroService: service)
                break
            }
        }
    }

    func makeHogarSeguroView(hogarSeguroService: ServicioStruct) {
        let hogarSeguroView = HogarInteligenteView(hogarSeguroService: hogarSeguroService)
        hogarSeguroView.delegate = self
        hogarSeguroView.translatesAutoresizingMaskIntoConstraints = true
        hogarSeguroView.widthAnchor.constraint(equalToConstant: stackView.width).isActive = true
        stackView.addArrangedSubview(hogarSeguroView)
    }

    func addGerericSeparatorView() {
        let separator = SeparatorView()
        separator.translatesAutoresizingMaskIntoConstraints = true
        separator.widthAnchor.constraint(equalToConstant: stackView.width).isActive = true
        separator.heightAnchor.constraint(equalToConstant: 200).isActive =  true
        stackView.addArrangedSubview(separator)
    }

    func deleteAddonsService(id: String) {
        var index = 0
        for service in self.serviciosAddons.servicios {
            if service.id == id {
                self.serviciosAddons.servicios.remove(at: index)
                break
            } else {
                index += 1
            }
        }
    }

    func deleteAddonsProducts(id: String) {
        var index = 0
        for product in self.productosAddons.productos {
            if product.id == id {
                self.productosAddons.productos.remove(at: index)
                break
            } else {
                index += 1
            }
        }
    }

    func deleteAddonsPromos(id: String) {
        var index = 0
        for promo in self.promocionesAddons.promociones {
            if promo.id == id {
                self.promocionesAddons.promociones.remove(at: index)
            } else {
                index += 1
            }
        }
    }

    func total() {
        SwiftEventBus.post("setTotal", sender: priceAddonsSelected)
        SwiftEventBus.post("setDiscount", sender: priceDiscunt)
        SwiftEventBus.post("actualizarPrecio")
    }

    func verifyAddonsSelected() {
        for service in ADDONS_SERVICE_CREATION_SELECTED {
            serviciosAddons.servicios.append(service)
        }
        for product in ADDONS_PRODUCT_SELECTED {
            productosAddons.productos.append(product)
        }
        for promo in ADDONS_PROMO_SELECTED {
            promocionesAddons.promociones.append(promo)
        }
        priceAddonsSelected = PRICE_ADDONS_SERVICE
        priceDiscunt = PRICE_DISCOUNT
    }

    func saveAddons() {
        if TPAddonsSelectedShared.shared.findAddonsPrev() != nil {
            TPAddonsSelectedShared.shared.cleanAddonsCreacion()
        }
        infoAddons.serviciosAdd = self.serviciosAddons
        infoAddons.productosAdd = self.productosAddons
        infoAddons.promocionesAdd = self.promocionesAddons
        if TPAddonsSelectedShared.shared.findAddonsPrev() != nil {
            TPAddonsSelectedShared.shared.cleanAddonsCreacion()
            infoAddons.uui = "1"
            TPAddonsSelectedShared.shared.infoAddonsCreacion = infoAddons
        } else {
            infoAddons.uui = "1"
            TPAddonsSelectedShared.shared.infoAddonsCreacion = infoAddons
        }
        SwiftEventBus.post("Addons_cargados", sender: infoAddons)
    }

    func hideButtonScroll(isFound: Bool, buttonForHide: UIView) {
        if !isFound {
            buttonForHide.isHidden = true
        }
    }

    func getCoordinatesOfCustomView(view: UIView) -> CGPoint {
        var cgPoint: CGPoint = CGPoint(x: 0, y: 0)
        if let index = self.stackView.subviews.firstIndex(of: view) {
            let xCoordinate = self.stackView.arrangedSubviews[index].frame.origin.x
            let yCoordinate = self.stackView.arrangedSubviews[index].frame.origin.y
            cgPoint.x = xCoordinate
            cgPoint.y = yCoordinate
        }
        return cgPoint
    }

}
