//
//  BundlingsAddStruct.swift
//  SalesCloud
//
//  Created by aestevezn on 25/07/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation

struct BundlingsAddStruct: Decodable {
    var bundlings: [BundlingStruct] = []
}

struct BundlingStruct: Decodable {
    var promociones: [PromocionStruct] = []
    var nombre: String?
    var montoDescuento: Double!
    var image: String?
    var id: String!
    var descripcion: String?
}
