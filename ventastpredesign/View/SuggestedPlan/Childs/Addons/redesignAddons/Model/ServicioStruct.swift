//
//  ServicioStruct.swift
//  SalesCloud
//
//  Created by aestevezn on 24/07/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation

struct ServicioStruct: Decodable {
    var servicioTerceros: String?
    var precio: Double!
    var nombre: String!
    var mostrarAddon: Bool!
    var maximoAgregar: Int!
    // "llevaATA": null,
    var imagen: String?
    // "id_DP_PlanServicioEquipo": null,
    var Id: String!
    var esClubWifi: Bool!
    var cargoActivacionHS: Double?
    var cargoActDiferidoMesesHS: String?
}

struct ServiciosAddStruct: Decodable {
    var servicios: [ServicioStruct] = []
}
