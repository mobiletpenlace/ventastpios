//
//  AdicionalStruct.swift
//  SalesCloud
//
//  Created by aestevezn on 24/07/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation

struct AdicionalStruct: Decodable {
    var tipoReferencia: String?
    var tipoProducto: String?
    var productoTerceros: String?
    var precio: Double?
    var nombre: String?
    var mesInicio: Int?
    var megasCrecelo: Int?
    var maximoAgregar: Int?
    var imagen: String?
    var idServicioPadre: String?
    var idPlanParrilla: String?
    var Id: String?
    var estaEnParrila: Bool?
    var canales: String?
    var aplicaNvoDisenio: Bool?
    var grupoycategoria: [grupoCategoria]?
}

struct grupoCategoria: Decodable {
    var posicion: String
    var grupo: String
    var adicional: String
    var categoria: String
}
