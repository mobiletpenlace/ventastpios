//
//  AddonsStruct.swift
//  SalesCloud
//
//  Created by aestevezn on 24/07/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation

struct AddonsStruct: Decodable {
    var infoAddons: infoAddonsResponseStruct!
}

struct infoAddonsResponseStruct: Decodable {
    var serviciosAdd: ServiciosAddStruct!
    var promocionesAdd: PromocionesAddStruct!
    var productosAdd: ProductosAddStruct!
    var BundlingsAdd: BundlingsAddStruct!
}
