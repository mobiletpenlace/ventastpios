//
//  ConsultaAddosRequest.swift
//  SalesCloud
//
//  Created by aestevezn on 24/07/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation

struct ConsultaAddonsRequestStruct: Codable {
    var idPlan: String!
    var info: InfoAddonsStruct!
}

struct InfoAddonsStruct: Codable {
    var subcanalVenta: String!
    var distrito: String!
    var colonia: String!
    var consultaServ: Bool!
    var metodoPago: String!
    var canal: String!
    var consultaAddons: Bool!
    var estimuloFiscal: Bool!
    var consultaPromo: Bool!
    var plaza: String!
    var subcanal: String!
    var codigoPostal: String!
    var cluster: String!
    var canalFront: String!
}
