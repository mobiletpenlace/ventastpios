//
//  PromocionesStruct.swift
//  SalesCloud
//
//  Created by aestevezn on 24/07/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation

struct PromocionStruct: Decodable {
    var tipoReferencia: String?
    var requiereOpp: String?
    var nombre: String?
    var montoDescuento: Double?
    var mesInicio: Int?
    // "mesesPagoAnt": null,
    var imagen: String?
    var Id: String!
    var esAutomatica: Bool!
    var descuentoDePorVida: Bool?
    // "bundlingId": null,
    var bloqueaEfectivo: String?
    var aplicaDescEmpleado: Bool?
    var aplicaCodigo: Bool?
    var adicionalProductoNombre: String?
    var adicionalProductoId: String?
}

struct PromocionesAddStruct: Decodable {
    var promociones: [PromocionStruct]
}
