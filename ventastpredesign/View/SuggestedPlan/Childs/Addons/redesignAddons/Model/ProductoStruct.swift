//
//  ProductoStruct.swift
//  SalesCloud
//
//  Created by aestevezn on 24/07/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation

struct ProductoStruct: Decodable {
    var Index: Int?
    var Agrupacion: String?
    var adicional: [AdicionalStruct] = []
}

struct ProductosAddStruct: Decodable {
    var productos: [ProductoStruct] = []
}
