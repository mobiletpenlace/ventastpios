//
//  ComboSelectedViewController.swift
//  ventastp
//
//  Created by Axl Estevez on 23/08/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import Foundation
import UIKit

class ComboSelectedViewController: BaseVentasView {

    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var tableView: UITableView!

    var bundlingsArray: BundlingsAddStruct!

    init(bundlings: BundlingsAddStruct) {
        self.bundlingsArray = bundlings
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        self.tableView.delegate = self

        configureTableView()
    }

    func configureTableView() {
        let nibName = UINib(nibName: "\(ComboTableViewCell.self)", bundle: nil)
        self.tableView.register(nibName, forCellReuseIdentifier: "\(ComboTableViewCell.self)")
        self.tableView.rowHeight = 80
        self.tableView.reloadData()
    }

    @IBAction func closeView(_ sender: Any) {
        dismiss(animated: true)
    }

}
