//
//  ComboTableViewCell.swift
//  SalesCloud
//
//  Created by aestevezn on 25/07/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

class ComboTableViewCell: UITableViewCell {

    @IBOutlet weak var imageCheck: UIImageView!
    @IBOutlet weak var buttonSelected: UIButton!

    var bundling: BundlingStruct!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setData(bundling: BundlingStruct) {
        self.bundling = bundling
        self.setupCell()
    }

    func setupCell() {
        buttonSelected.setTitle("\(self.bundling.nombre ?? "")", for: .normal)
        imageCheck.isHidden = true
    }

}
