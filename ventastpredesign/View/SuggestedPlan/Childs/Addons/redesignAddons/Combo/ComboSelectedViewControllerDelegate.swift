//
//  ComboSelectedViewControllerDelegate.swift
//  SalesCloud
//
//  Created by aestevezn on 25/07/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

extension ComboSelectedViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bundlingsArray.bundlings.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "\(ComboTableViewCell.self)") as? ComboTableViewCell else {
            return UITableViewCell()
        }
        cell.setData(bundling: bundlingsArray.bundlings[indexPath.row])
        return cell
    }

}
