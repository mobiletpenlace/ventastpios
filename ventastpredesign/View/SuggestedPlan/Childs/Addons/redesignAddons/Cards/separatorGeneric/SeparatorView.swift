//
//  SeparatorView.swift
//  SalesCloud
//
//  Created by aestevezn on 08/08/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

class SeparatorView: UIView {

    @IBOutlet var separatorView: UIView!

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        fatalError("init(coder:) has not been implemented")
    }

    init() {
        super.init(frame: CGRect.zero)
        self.commonInit()
    }

    private func commonInit() {
        Bundle.main.loadNibNamed("SeparatorView", owner: self)
        addSubview(separatorView)
        separatorView.frame = self.bounds
        separatorView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }

}
