//
//  WifiGerenicView.swift
//  SalesCloud
//
//  Created by aestevezn on 07/08/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

protocol modalAlertProtocol {
    func showModalError(errorString: String)
}

class GenericViewSusAdd: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var titleCardLabel: UILabel!
    @IBOutlet weak var subtitleCardLabel: UILabel!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var productImage: UIImageView!

    var serviceSelf: ServicioStruct!
    var contador: Int = 0
    var maximoAgregar = 0
    var maneger: modalAlertProtocol?
    var addonsManager: servicesAddonsProtocol?
    var addon: ServiciosCreacion!
    var isSelected: Bool = false
    var price: Double = 0

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func commonInit() {
        Bundle.main.loadNibNamed("WifiProView", owner: self)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }

    func setupView(title: String, subtitle: String, image: String, service: String) {
        titleCardLabel.text = title
        subtitleCardLabel.text = subtitle
        productImage.image = UIImage(named: image)
        productNameLabel.text = service
    }

    func setService(service: ServicioStruct) {
        self.serviceSelf = service
        self.maximoAgregar = service.maximoAgregar
        self.price = service.precio
        verifyIfPreviousSelected()
    }

    private func makeAddon() {
        addon = ServiciosCreacion()
        addon.id = serviceSelf.Id
        addon.cantidad = String(contador)
        addon.cantidadInt = contador
    }

    private func verifyIfPreviousSelected() {
        if verifyAddonServicePreviousSelected(addonID: self.serviceSelf.Id) {
            self.addon = getServiceAddon(addonID: self.serviceSelf.Id)
            isSelected = true
            contador = addon.cantidadInt ?? 0
            countLabel.text = addon.cantidad ?? "0"
        } else {
            makeAddon()
        }
    }

    private func addSelfCount() {
        if contador == maximoAgregar {
            maneger?.showModalError(errorString: dialogs.Warning.maximoAlcanzado)
        } else  if (!isSelected) && (contador == 0) {
            ADDONS_SERVICE_CREATION_SELECTED.append(addon)
            isSelected = true
            contador += 1
            countLabel.text = String(contador)
            updateAmountSelfAddon(cantidad: contador)
            addonsManager?.addAddonService(addon: addon)
            addonsManager?.sendPriceService(price: Int(price))
            PRICE_ADDONS_SERVICE += Int(self.price)
        } else if (isSelected) && (contador != maximoAgregar) {
            contador += 1
            countLabel.text = String(contador)
            updateAmountSelfAddon(cantidad: contador)
            updateAddonService(addonID: addon.id!, newValue: contador)
            addonsManager?.sendPriceService(price: Int(price))
            addonsManager?.updateAmountAddon(newAmount: contador, addonId: addon.id!)
            PRICE_ADDONS_SERVICE += Int(self.price)
        }
    }

    private func updateAmountSelfAddon(cantidad: Int) {
        self.addon.cantidadInt = cantidad
        self.addon.cantidad = String(cantidad)
    }

    private func substractionSelfCount() {
        if contador == 0 {
            isSelected = false
            addonsManager?.deleteAddonService(addonId: addon.id!)
            maneger?.showModalError(errorString: dialogs.Warning.deleteComplete)
            deleteServiceAddons(addon: addon)
        } else if (contador > 0) && (isSelected) {
            contador -= 1
            countLabel.text = String(contador)
            addonsManager?.updateAmountAddon(newAmount: contador, addonId: addon.id!)
            addonsManager?.deletePriceService(price: Int(price))
            updateAddonService(addonID: addon.id!, newValue: contador)
            PRICE_ADDONS_SERVICE -= Int(self.price)
        }
    }

    @IBAction func addAction(_ sender: Any) {
        addSelfCount()
    }

    @IBAction func substractionAction(_ sender: Any) {
        substractionSelfCount()
    }

}
