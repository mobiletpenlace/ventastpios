//
//  ComboBundlingView.swift
//  SalesCloud
//
//  Created by aestevezn on 04/08/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

class ComboBundlingView: UIView {

    @IBOutlet weak var comboView: UIView!
    @IBOutlet var contentView: UIView!

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func commonInit() {
        Bundle.main.loadNibNamed("ComboBundlingView", owner: self)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }

    @IBAction func launchComboView(_ sender: Any) {

    }

}
