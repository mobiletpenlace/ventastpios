//
//  DescuentoPromoView.swift
//  SalesCloud
//
//  Created by aestevezn on 18/08/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

class DescuentoPromoView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var imageChek: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!

    var promocion: PromocionStruct!
    var icCheck: Bool = false
    var manager: promocionAddonsProtocol!
    var addon = PromocionesCreacion()

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    init(promo: PromocionStruct, manager: promocionAddonsProtocol) {
        super.init(frame: CGRect.zero)
        self.promocion = promo
        self.manager = manager
        commonInit()
        setupView()
        verifyPreviousCheckSelected()
    }

    private func commonInit() {
        Bundle.main.loadNibNamed("DescuentoPromoView", owner: self)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }

    private func setupView () {
        titleLabel.text = promocion.nombre
        if promocion.esAutomatica {
            self.icCheck = true
            contentView.layer.borderWidth = 2
            contentView.layer.borderColor = UIColor(named: "Base_rede_button_dark")?.cgColor
            imageChek.isHidden = false
        } else {
            self.icCheck = false
            imageChek.isHidden = true
        }
    }

    private func evalCheck() {
        if !self.icCheck {
            self.icCheck = true
            contentView.layer.borderWidth = 2
            contentView.layer.borderColor = UIColor(named: "Base_rede_button_dark")?.cgColor
            imageChek.isHidden = false
            manager.addPromoAddon(addon: self.addon)
            manager.sendPricePromo(price: Int(promocion.montoDescuento ?? 0.0))
            ADDONS_PROMO_SELECTED.append(addon)
            PRICE_DISCOUNT += Int(promocion.montoDescuento ?? 0.0)
        } else {
            self.icCheck = false
            contentView.layer.borderWidth = 1
            contentView.layer.borderColor = UIColor(named: "Base_rede_semiclear_gray")?.cgColor
            imageChek.isHidden = true
            manager.deletePromoAddon(addonId: promocion.Id)
            manager.deletePricePromo(price: Int(promocion.montoDescuento ?? 0.0))
            deletePromoAddons(addon: addon)
            PRICE_DISCOUNT -= Int(promocion.montoDescuento ?? 0.0)
        }
    }

    private func makeAddon() {
        addon.id = promocion.Id
        addon.esAutomatica = promocion.esAutomatica
    }

    private func verifyPreviousCheckSelected() {
        if verifyAddonPromoPreviousSelected(addonID: promocion.Id) {
            addon = getPromoAddon(addonID: promocion.Id)
            self.icCheck = true
            contentView.layer.borderWidth = 2
            contentView.layer.borderColor = UIColor(named: "Base_rede_button_dark")?.cgColor
        } else {
            makeAddon()
        }
    }

    @IBAction func sendPriceAction(_ sender: Any) {
        evalCheck()
    }
}
