//
//  DescuentosEspecialesView.swift
//  SalesCloud
//
//  Created by aestevezn on 18/08/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

class DescuentosEspecialesView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var stackView: UIStackView!

    var promos: [PromocionStruct] = []
    var delegate: promocionAddonsProtocol!

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    init (promos: [PromocionStruct], delegate: promocionAddonsProtocol) {
        super.init(frame: CGRect.zero)
        self.promos = promos
        self.delegate = delegate
        commonInit()
        addSubViews()
    }

    private func commonInit() {
        Bundle.main.loadNibNamed("DescuentosEspecialesView", owner: self)
        addSubview(contentView)
        contentView.addSubview(stackView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }

    private func addSubViews() {
        for promocion in promos {
            makePromo(promocion: promocion)
        }
    }

    private func makePromo(promocion: PromocionStruct) {
        let promoView = DescuentoPromoView(promo: promocion, manager: delegate)
        promoView.translatesAutoresizingMaskIntoConstraints = true
        promoView.widthAnchor.constraint(equalToConstant: self.stackView.bounds.width).isActive = true
        promoView.heightAnchor.constraint(equalToConstant: 80).isActive = true
        stackView.addArrangedSubview(promoView)
    }
}
