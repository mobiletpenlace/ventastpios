//
//  HogarSeguroView.swift
//  SalesCloud
//
//  Created by aestevezn on 22/08/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

class HogarInteligenteView: UIView {

    @IBOutlet var view: UIView!
    @IBOutlet weak var imageCheck: UIImageView!
    @IBOutlet weak var hogarInteligenteView: UIView!

    var service: ServicioStruct!
    var addon = ServiciosCreacion()
    var isSelected: Bool = false
    var cantiad = 0
    var price: Int = 0
    var delegate: servicesAddonsProtocol?

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    init(hogarSeguroService: ServicioStruct) {
        super.init(frame: CGRect.zero)
        service = hogarSeguroService
        price = Int(service.precio)
        commonInit()
        makeAddon()
    }

    private func commonInit() {
        Bundle.main.loadNibNamed("HogarInteligenteView", owner: self)
        addSubview(view)
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        imageCheck.isHidden = true
    }

    private func evalCheck() {
        if isSelected {
            isSelected = false
            hogarInteligenteView.layer.borderWidth = 1
            imageCheck.isHidden = true
            hogarInteligenteView.layer.borderColor = UIColor(named: "Base_rede_semiclear_gray")?.cgColor
            delegate?.deleteAddonService(addonId: addon.id!)
            delegate?.deletePriceService(price: price)
        } else {
            isSelected = true
            imageCheck.isHidden = false
            hogarInteligenteView.layer.borderWidth = 2
            hogarInteligenteView.layer.borderColor = UIColor(named: "Base_rede_button_dark")?.cgColor
            ADDONS_SERVICE_CREATION_SELECTED.append(addon)
            delegate?.addAddonService(addon: addon)
            delegate?.sendPriceService(price: price)
        }
    }

    private func setupView() {
        imageCheck.isHidden = false
        hogarInteligenteView.layer.borderWidth = 2
        hogarInteligenteView.layer.borderColor = UIColor(named: "Base_rede_button_dark")?.cgColor
    }

    private func makeAddon() {
        if verifyAddonServicePreviousSelected(addonID: service.Id) {
            addon = getServiceAddon(addonID: service.Id)
            setupView()
        } else {
            addon.id = service.Id
            addon.cantidad = "1"
            addon.cantidadInt = 1
        }
    }

    @IBAction func selectedAction(_ sender: Any) {
        evalCheck()
    }

}
