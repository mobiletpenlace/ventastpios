//
//  ClubWifiView.swift
//  SalesCloud
//
//  Created by aestevezn on 07/08/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

class ClubWifiView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var clubWifiView: UIView!
    @IBOutlet weak var imageCheck: UIImageView!

    var adicional: AdicionalStruct!
    var isCheck: Bool = false

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setAdicionalObject(adicional: AdicionalStruct) {
        self.adicional = adicional
    }

    func evalCheck() {
        if !isCheck {
            isCheck = true
            clubWifiView.layer.borderColor = UIColor(named: "Base_rede_button_dark")?.cgColor
            clubWifiView.layer.borderWidth = 2
            imageCheck.isHidden = false
        } else {
            isCheck = false
            clubWifiView.layer.borderColor = UIColor(named: "Base_rede_semiclear_gray")?.cgColor
            imageCheck.isHidden = true
        }
    }

    private func commonInit() {
        Bundle.main.loadNibNamed("clubWifiView", owner: self)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        imageCheck.isHidden = true
    }

    @IBAction func addClubAction(_ sender: Any) {
        evalCheck()
    }

}
