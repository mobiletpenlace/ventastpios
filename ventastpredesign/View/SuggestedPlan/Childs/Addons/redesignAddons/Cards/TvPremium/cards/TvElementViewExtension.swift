//
//  TvElementViewExtension.swift
//  SalesCloud
//
//  Created by aestevezn on 18/08/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

extension TvElementView {

    func evalElement () {
        if elementType is ServicioStruct {
            typeElement = .service
            guard let service = elementType as? ServicioStruct else { return }
            setupFromService(service: service)
        } else if elementType is AdicionalStruct {
            typeElement = .aditional
            guard let aditional = elementType as? AdicionalStruct else { return }
            setupFromAditional(aditional: aditional)
        }
    }

    func checkElement() {
        if elementType is ServicioStruct {
            guard let service = elementType as? ServicioStruct else { return }
            self.service = service
            self.price = Int(service.precio ?? 0.0)
            if self.addonService == nil {
                makeServiceAddon(service: self.service)
            }
            if isChek && !verifyAddonServicePreviousSelected(addonID: self.service.Id) {
                ADDONS_SERVICE_CREATION_SELECTED.append(addonService!)
                PRICE_ADDONS_SERVICE += self.price
                managerService?.addAddonService(addon: addonService!)
                managerService?.sendPriceService(price: self.price)
            } else {
                deleteServiceAddons(addon: addonService!)
                managerService?.deletePriceService(price: Int(self.price))
                managerService?.deleteAddonService(addonId: (addonService?.id)!)
                PRICE_ADDONS_SERVICE -= self.price
            }
        } else if elementType is AdicionalStruct {
            guard let adicional = elementType as? AdicionalStruct else { return }
            self.proucto = adicional
            self.price = Int(adicional.precio!)
            if self.addonProduct == nil {
                makeProductoAddon(adicional: self.proucto)
            }
            if isChek && !verifyAddonProductPreviousSelected(addonID: self.proucto.Id!) {
                ADDONS_PRODUCT_SELECTED.append(addonProduct!)
                PRICE_ADDONS_SERVICE += self.price
                managerProducts?.addProductoAddon(addon: addonProduct!)
                managerProducts?.sendPriceProduct(price: Int(self.price))
            } else {
                deleteProductAddons(addon: addonProduct!)
                PRICE_ADDONS_SERVICE -= self.price
                managerProducts?.deletePriceProduct(price: Int(self.price))
                managerProducts?.deleteProductoAddons(addonId: (addonProduct?.id)!)
            }
        }
    }

    func makeProductoAddon(adicional: AdicionalStruct) {
        self.addonProduct = ProductosCreacion()
        addonProduct?.id = adicional.Id
    }

    func makeServiceAddon(service: ServicioStruct) {
        self.addonService = ServiciosCreacion()
        addonService?.id = service.Id
        addonService?.cantidad = "1"
        addonService?.cantidadInt = 1
    }

    private func setupFromService(service: ServicioStruct) {
        if service.nombre.lowercased() == "disney+" {
            avatarImage.image = UIImage(named: "b_img_disney_dragon")
            brandAppImage.image = UIImage(named: "b_img_head_disney_plus")
            priceLabel.text = "+ $ \(service.precio ?? 0.0) /mes "
        }

        if service.nombre.lowercased() == "star+" {
            avatarImage.image = UIImage(named: "b_img_start_plus_deadpool")
            brandAppImage.image = UIImage(named: "b_img_start_plus_head")
            priceLabel.text = "+ $ \(service.precio ?? 0.0) /mes "
        }

        if service.nombre.lowercased().contains(string: "combo") {
            avatarImage.image = UIImage(named: "b_img_disney_and_start_plus")
            brandAppImage.image = UIImage(named: "b_img_combo_disney_star")
            priceLabel.text = "+ $ \(service.precio ?? 0.0) /mes "
        }
    }

    private func setupFromAditional(aditional: AdicionalStruct) {
        if aditional.nombre?.lowercased() == "fox sports premium" {
            avatarImage.image = UIImage(named: "b_fox_sports_premium_main_img")
            brandAppImage.image = UIImage(named: "fx_sports_premium_icon")
            priceLabel.text = "+ $ \(aditional.precio ?? 0.0) /mes "
        } else if aditional.nombre?.lowercased() == "nba league pass" {
            avatarImage.image = UIImage(named: "b_img_nba_head_4")
            brandAppImage.image = UIImage(named: "b_img_nba_head_3")
            priceLabel.text = "+ $ \(aditional.precio ?? 0.0) /mes "
        } else if (aditional.nombre?.lowercased() == "hbo max") || (aditional.nombre?.lowercased() == "paquete hbo") {
            avatarImage.image = UIImage(named: "b_img_hbo_scooby")
            brandAppImage.image = UIImage(named: "b_img_0013")
            priceLabel.text = "+ $ \(aditional.precio ?? 0.0) /mes "
        } else if aditional.nombre?.lowercased() == "universal+" {
            avatarImage.image = UIImage(named: "b_img_universal")
            brandAppImage.image = UIImage(named: "b_img_head_universal")
            priceLabel.text = "+ $ \(aditional.precio ?? 0.0) /mes "
        } else if aditional.nombre?.lowercased() == "paramount+" {
            avatarImage.image = UIImage(named: "iCarly")
            brandAppImage.image = UIImage(named: "paramount+")
            priceLabel.text = "+ $ \(aditional.precio ?? 0.0) /mes "
        }

    }

}
