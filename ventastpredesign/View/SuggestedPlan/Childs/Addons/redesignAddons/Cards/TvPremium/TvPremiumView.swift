//
//  TvPremiumView.swift
//  SalesCloud
//
//  Created by aestevezn on 18/08/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

class TvPremiumView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var stackView: UIStackView!

    var managerService: servicesAddonsProtocol!
    var managerProduct: productoAddonProtocol!

    var tvPremiumArray: [Any] = []

    init(elementsArray: [Any], dService: servicesAddonsProtocol, dProducto: productoAddonProtocol) {
        super.init(frame: CGRect.zero)
        tvPremiumArray = elementsArray
        self.managerService = dService
        self.managerProduct = dProducto
        commonInit()
        addSubViews()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func commonInit() {
        Bundle.main.loadNibNamed("TvPremiumView", owner: self)
        addSubview(contentView)
        contentView.addSubview(stackView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [
            .flexibleWidth, .flexibleHeight
        ]
    }

    private func addSubViews() {
        for element in tvPremiumArray {
            makeView(tvView: element)
        }
    }

    private func makeView(tvView: Any) {
        let tvPremiumView = TvElementView(element: tvView)
        tvPremiumView.managerService = self.managerService
        tvPremiumView.managerProducts = self.managerProduct
        tvPremiumView.translatesAutoresizingMaskIntoConstraints = true
        tvPremiumView.widthAnchor.constraint(equalToConstant: self.bounds.width).isActive = true
        tvPremiumView.heightAnchor.constraint(equalToConstant: 80).isActive = true
        stackView.addArrangedSubview(tvPremiumView)
    }
}
