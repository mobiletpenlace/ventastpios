//
//  TvElementView.swift
//  SalesCloud
//
//  Created by aestevezn on 18/08/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

enum TvPremiumType {
    case service
    case aditional
}

class TvElementView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var brandAppImage: UIImageView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var imageCheck: UIImageView!

    var typeElement: TvPremiumType?
    var elementType: Any!
    var isChek: Bool = false
    var managerService: servicesAddonsProtocol?
    var managerProducts: productoAddonProtocol?
    var addonService: ServiciosCreacion?
    var addonProduct: ProductosCreacion?
    var service: ServicioStruct!
    var proucto: AdicionalStruct!
    var price: Int = 0

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    init(element: Any) {
        super.init(frame: CGRect.zero)
        commonInit()
        self.elementType = element
        checkPreviousSelected()
        setupView()
    }

    private func setupView () {
        evalElement()
    }

    private func commonInit () {
        Bundle.main.loadNibNamed("TvElementView", owner: self)
        addSubview(contentView)
        imageCheck.isHidden = true
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }

    private func checkPreviousSelected() {
        if elementType is ServicioStruct {
            guard let service = elementType as? ServicioStruct else { return }
            makeServiceAddon(service: service)
            if verifyAddonServicePreviousSelected(addonID: service.Id) {
                addonService = getServiceAddon(addonID: service.Id)
                isChek = true
                imageCheck.isHidden = false
                contentView.layer.borderWidth = 2
                contentView.layer.borderColor = UIColor(named: "Base_rede_button_dark")?.cgColor
            }
        } else if elementType is AdicionalStruct {
            guard let adicional = elementType as? AdicionalStruct else { return }
            makeProductoAddon(adicional: adicional)
            if verifyAddonProductPreviousSelected(addonID: adicional.Id!) {
                addonProduct = getProductAddon(addonID: adicional.Id!)
                isChek = true
                imageCheck.isHidden = false
                contentView.layer.borderWidth = 2
                contentView.layer.borderColor = UIColor(named: "Base_rede_button_dark")?.cgColor
            }
        }
    }

    private func evaulCheck() {
        if isChek {
            isChek = false
            contentView.layer.borderWidth = 1
            contentView.layer.borderColor = UIColor(named: "Base_rede_semiclear_gray")?.cgColor
            imageCheck.isHidden = true
        } else {
            isChek = true
            imageCheck.isHidden = false
            contentView.layer.borderWidth = 2
            contentView.layer.borderColor = UIColor(named: "Base_rede_button_dark")?.cgColor
        }
    }

    @IBAction func sendPriceAction(_ sender: Any) {
        evaulCheck()
        checkElement()
    }
}
