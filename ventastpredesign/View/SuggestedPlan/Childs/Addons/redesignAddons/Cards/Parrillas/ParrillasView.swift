//
//  ParrillasView.swift
//  SalesCloud
//
//  Created by Axl Estevez on 11/08/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

class ParrillasView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var stackView: UIStackView!

    var viewArray: [AdicionalStruct] = []
    var delegate: productoAddonProtocol!

    init() {
        super.init(frame: CGRect.zero)
        commonInit()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func commonInit() {
        Bundle.main.loadNibNamed("parrillasView", owner: self)
        addSubview(contentView)
        contentView.addSubview(stackView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }

    func setterAditionalArray(array: [AdicionalStruct]) {
            self.viewArray = array.sorted { (a, b) -> Bool in
                guard let precioA = a.precio else { return false }
                guard let precioB = b.precio else { return true }
                return precioA < precioB
            }
            addViews()
        }

    private func addViews() {
        for parrilla in viewArray {
            makeAditionalView(aditional: parrilla)
        }
    }

    private func makeAditionalView(aditional: AdicionalStruct) {
        let parrillaView = ParrillaView(parrilla: aditional)
        parrillaView.manager = delegate
        parrillaView.translatesAutoresizingMaskIntoConstraints = true
        parrillaView.widthAnchor.constraint(equalToConstant: 390).isActive = true
        parrillaView.heightAnchor.constraint(equalToConstant: 80).isActive = true
        stackView.addArrangedSubview(parrillaView)
    }
}
