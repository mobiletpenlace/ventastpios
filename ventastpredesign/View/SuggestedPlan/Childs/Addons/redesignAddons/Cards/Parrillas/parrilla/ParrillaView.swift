//
//  ParrillaView.swift
//  SalesCloud
//
//  Created by aestevezn on 08/08/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

class ParrillaView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var parrillaNameLabel: UILabel!
    @IBOutlet weak var precioLabel: UILabel!
    @IBOutlet weak var imageCheck: UIImageView!

    var isSelected: Bool = false
    var aditionalSelf: AdicionalStruct!
    var manager: productoAddonProtocol?
    var addonProducto = ProductosCreacion()

    init (parrilla: AdicionalStruct) {
        super.init(frame: CGRect.zero)
        self.aditionalSelf = parrilla
        self.commonInit()
        self.setupView()
        verifyPreviousSelected()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @IBAction func addParrillaAction(_ sender: Any) {
        checkUpPrice()
    }

    private func checkUpPrice() {
        if isSelected {
            isSelected = false
            contentView.layer.borderColor = UIColor(named: "Base_rede_semiclear_gray")?.cgColor
            imageCheck.isHidden = true
            manager?.deletePriceProduct(price: Int(aditionalSelf.precio ?? 0.0))
            manager?.deleteProductoAddons(addonId: addonProducto.id ?? "")
            deleteProductAddons(addon: addonProducto)
            PRICE_ADDONS_SERVICE -= Int(aditionalSelf.precio ?? 0.0)
        } else {
            isSelected = true
            contentView.layer.borderColor = UIColor(named: "Base_rede_button_dark")?.cgColor
            contentView.layer.borderWidth = 2
            imageCheck.isHidden = false
            manager?.addProductoAddon(addon: addonProducto)
            manager?.sendPriceProduct(price: Int(aditionalSelf.precio ?? 0.0))
            ADDONS_PRODUCT_SELECTED.append(addonProducto)
            PRICE_ADDONS_SERVICE += Int(aditionalSelf.precio ?? 0.0)
        }
    }

    private func verifyPreviousSelected() {
        if verifyAddonProductPreviousSelected(addonID: aditionalSelf.Id!) {
            addonProducto = getProductAddon(addonID: aditionalSelf.Id!)
            isSelected = true
            contentView.layer.borderColor = UIColor(named: "Base_rede_button_dark")?.cgColor
            contentView.layer.borderWidth = 2
            imageCheck.isHidden = false
        } else {
            setterAddon()
        }
    }

    private func setupView() {
        parrillaNameLabel.text = aditionalSelf.canales
        precioLabel.text = "+ $ \(aditionalSelf.precio ?? 0.0) /mes"
        imageCheck.isHidden = true
    }

    private func setterAddon() {
        self.addonProducto.id = self.aditionalSelf.Id
    }

    private func commonInit() {
        Bundle.main.loadNibNamed("parrillaView", owner: self)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }

}
