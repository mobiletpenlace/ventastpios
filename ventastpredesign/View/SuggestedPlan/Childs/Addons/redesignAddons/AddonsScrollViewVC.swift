//
//  AddonsScrollViewVC.swift
//  SalesCloud
//
//  Created by aestevezn on 04/08/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class AddonsScrollViewVC: UIViewController {
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var scrollView: UIScrollView!
    // MARK: Buttons view action navigate Scroll
    @IBOutlet weak var combo: UIView!
    @IBOutlet weak var wifiProButton: UIView!
    @IBOutlet weak var wifiPro6Button: UIView!
    @IBOutlet weak var clubWifi: UIView!
    @IBOutlet weak var tvAdicionalButton: UIView!
    @IBOutlet weak var camaraWebbutton: UIView!
    @IBOutlet weak var masCanalesButton: UIView!
    @IBOutlet weak var tvPremiumButton: UIView!
    @IBOutlet weak var descuentosEspecialesButton: UIView!
    @IBOutlet weak var hogarInteligenteButton: UIView!
    // MARK: views in scroll view
    var wifiProView: UIView!
    var wifiPro6View: UIView!
    var tvAdicionalView: UIView!
    var tvAdicionalPlus: UIView!
    var clubWifiView: UIView!
    var tvCamView: UIView!
    var parrilasView: UIView!
    var tvPremiumView: UIView!
    var descuentosViews: UIView!
    var spinnerView: WaitingView!
    var request: ConsultaAddonsRequest!
    var router: ConsultaAddonsVentaRouter!
    var productosAddons = ProductosAdd()
    var serviciosAddons = ServiciosAdd()
    var promocionesAddons = PromocionesAdd()
    var infoAddons = InfoAdicionalesCreacion()
    var buttonPreviousSelected: UIButton?
    var priceAddonsSelected = 0
    var priceDiscunt = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        self.showSpinnerView()
        self.verifyAddonsSelected()
        self.scrollView.addSubview(stackView)
        SwiftEventBus.onMainThread(self, name: "sendRequest") { [weak self] result in
            if let value = result?.object as? ConsultaAddonsRequest {
                self!.router = ConsultaAddonsVentaRouter(request: value)
                self!.router.delegate = self
                self!.router.getAddonsResponse()
            }
        }
        SwiftEventBus.onMainThread(self, name: "GuardaAddons") { [weak self] _ in
            self?.saveAddons()
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        fasePostPreguntas.Index = 1
    }

    @IBAction func scrollToWifiProAction(_ sender: UIButton) {
        scrollToViewAction(viewToScroll: self.wifiProView)
        buttonPreviousSelected?.isSelected = false
        switchButton(button: buttonPreviousSelected ?? UIButton())
        sender.isSelected = true
        buttonPreviousSelected = sender
        setupButton(button: sender)
    }

    @IBAction func scrollToWifiPro6Action(_ sender: UIButton) {
        scrollToViewAction(viewToScroll: self.wifiPro6View)
        buttonPreviousSelected?.isSelected = false
        switchButton(button: buttonPreviousSelected ?? UIButton())
        sender.isSelected = true
        buttonPreviousSelected = sender
        setupButton(button: sender)
    }

    @IBAction func scrollToTvAdicionAction(_ sender: UIButton) {
        scrollToViewAction(viewToScroll: self.tvAdicionalView)
        buttonPreviousSelected?.isSelected = false
        switchButton(button: buttonPreviousSelected ?? UIButton())
        sender.isSelected = true
        buttonPreviousSelected = sender
        setupButton(button: sender)
    }

    @IBAction func scrollToParrilasAction(_ sender: UIButton) {
        scrollToViewAction(viewToScroll: self.parrilasView)
        buttonPreviousSelected?.isSelected = false
        switchButton(button: buttonPreviousSelected ?? UIButton())
        sender.isSelected = true
        buttonPreviousSelected = sender
        setupButton(button: sender)
    }

    @IBAction func scrollToCamaraWebAction(_ sender: UIButton) {
        scrollToViewAction(viewToScroll: self.tvCamView)
        buttonPreviousSelected?.isSelected = false
        switchButton(button: buttonPreviousSelected ?? UIButton())
        sender.isSelected = true
        buttonPreviousSelected = sender
        setupButton(button: buttonPreviousSelected!)
    }

    @IBAction func scrollToTvPremiumAction(_ sender: UIButton) {
        scrollToViewAction(viewToScroll: self.tvPremiumView)
        buttonPreviousSelected?.isSelected = false
        switchButton(button: buttonPreviousSelected ?? UIButton())
        sender.isSelected = true
        buttonPreviousSelected = sender
        setupButton(button: sender)
    }

    @IBAction func scrollToDescuentosAction(_ sender: UIButton) {
        scrollToViewAction(viewToScroll: self.descuentosViews)
        buttonPreviousSelected?.isSelected = false
        switchButton(button: buttonPreviousSelected ?? UIButton())
        sender.isSelected = true
        buttonPreviousSelected = sender
        setupButton(button: sender)
    }
}

extension AddonsScrollViewVC {
    fileprivate func scrollToViewAction(viewToScroll: UIView!) {
        guard let customView = viewToScroll else { return }
        let customViewFrame = customView.convert(customView.bounds, to: self.scrollView)
        let contentOffset = CGPoint(x: 0, y: customViewFrame.origin.y)
        UIView.animate(withDuration: 0.3) {
            self.scrollView.setContentOffset(contentOffset, animated: true)
        }
    }

    fileprivate func switchButton(button: UIButton) {
        if !button.isSelected {
            button.backgroundColor = UIColor(named: "Base_rede_semiclear_gray")
            button.setTitleColor(UIColor(named: "Base_rede_blue_textfields_wait"), for: .normal)
            button.layer.cornerRadius = 15
        }
    }

    fileprivate func setupButton(button: UIButton) {
        button.backgroundColor = UIColor(named: "Base_rede_button_dark")
        button.setTitleColor(.white, for: .normal)
        button.layer.cornerRadius = 15
    }
}
