//
//  AddonsScrollViewVCDelegate.swift
//  SalesCloud
//
//  Created by aestevezn on 07/08/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

extension AddonsScrollViewVC: ConsultaAddonsVentaProtocol {
    func onSucessAddons(response: AddonsStruct) {
        DispatchQueue.main.async {
            self.combo.isHidden = true
            self.searchWifiServices(services: response.infoAddons.serviciosAdd)
            self.searchClubWifi(productos: response.infoAddons.productosAdd)
            self.searchTvAdicional(service: response.infoAddons.serviciosAdd)
            self.searchTVPremium(services: response.infoAddons.serviciosAdd)
            self.searchWebCam(services: response.infoAddons.serviciosAdd)
            self.makeParrillas(productos: response.infoAddons.productosAdd)
            self.makeTvPremium(productos: response.infoAddons.productosAdd, servicios: response.infoAddons.serviciosAdd)
            self.makePromos(promociones: response.infoAddons.promocionesAdd)
            self.searchHogarInteligente(servicios: response.infoAddons.serviciosAdd)
            self.addGerericSeparatorView()
            self.removeSpinnerView()
        }
    }

    func onError(error: String) {
        print(error)
    }

}

extension AddonsScrollViewVC: modalAlertProtocol {

    func showModalError(errorString: String) {
        Constants.Alert(title: "Advertencia", body: errorString, type: type.Warning, viewC: self)
    }
}

extension AddonsScrollViewVC: servicesAddonsProtocol {

    func addAddonService(addon: ServiciosCreacion) {
        self.serviciosAddons.servicios.append(addon)
    }

    func sendPriceService(price: Int) {
        priceAddonsSelected = priceAddonsSelected + price
        total()
    }

    func deletePriceService(price: Int) {
        priceAddonsSelected = priceAddonsSelected - price
        total()
    }

    func deleteAddonService(addonId: String) {
        self.deleteAddonsService(id: addonId)
    }

    func updateAmountAddon(newAmount: Int, addonId: String) {
        let serviciosCreacionArray = self.serviciosAddons.servicios
        for service in serviciosCreacionArray {
            if service.id == addonId {
                service.cantidadInt = newAmount
                service.cantidad = String(newAmount)
                break
            }
        }
    }
}

extension AddonsScrollViewVC: productoAddonProtocol {

    func addProductoAddon(addon: ProductosCreacion) {
        self.productosAddons.productos.append(addon)
    }

    func sendPriceProduct(price: Int) {
        priceAddonsSelected = priceAddonsSelected + price
        total()
    }

    func deletePriceProduct(price: Int) {
        priceAddonsSelected = priceAddonsSelected - price
        total()
    }

    func deleteProductoAddons(addonId: String) {
        deleteAddonsProducts(id: addonId)
    }
}

extension AddonsScrollViewVC: promocionAddonsProtocol {

    func addPromoAddon(addon: PromocionesCreacion) {
        promocionesAddons.promociones.append(addon)
    }

    func sendPricePromo(price: Int) {
        priceDiscunt = priceDiscunt + price
        total()
    }

    func deletePricePromo(price: Int) {
        priceDiscunt = priceDiscunt - price
        total()
    }

    func deletePromoAddon(addonId: String) {
        self.deleteAddonsPromos(id: addonId)
    }

}
