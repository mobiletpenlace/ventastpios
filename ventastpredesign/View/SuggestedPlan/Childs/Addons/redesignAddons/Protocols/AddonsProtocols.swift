//
//  AddonsProtocols.swift
//  SalesCloud
//
//  Created by aestevezn on 22/08/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

protocol servicesAddonsProtocol {
    func addAddonService(addon: ServiciosCreacion)
    func sendPriceService(price: Int)
    func deletePriceService(price: Int)
    func deleteAddonService(addonId: String)
    func updateAmountAddon(newAmount: Int, addonId: String)
}

extension servicesAddonsProtocol {
    func updateAmountAddon(newAmount: Int, addonId: String) {
        print(newAmount)
    }
}

protocol productoAddonProtocol {
    func addProductoAddon(addon: ProductosCreacion)
    func sendPriceProduct(price: Int)
    func deletePriceProduct(price: Int)
    func deleteProductoAddons(addonId: String)
}

protocol promocionAddonsProtocol {
    func addPromoAddon(addon: PromocionesCreacion)
    func sendPricePromo(price: Int)
    func deletePricePromo(price: Int)
    func deletePromoAddon(addonId: String)
}
