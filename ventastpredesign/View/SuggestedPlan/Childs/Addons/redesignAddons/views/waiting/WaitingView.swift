//
//  WaitingView.swift
//  SalesCloud
//
//  Created by aestevezn on 25/07/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

class WaitingView: UIView {
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var titleLabel: UILabel!

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    private func commonInit() {
        Bundle.main.loadNibNamed("WaitingView", owner: self)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
}
