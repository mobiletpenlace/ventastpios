//
//  ConsultaAddonsVentaRouter.swift
//  SalesCloud
//
//  Created by aestevezn on 24/07/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation

protocol ConsultaAddonsVentaProtocol {
    func onSucessAddons(response: AddonsStruct)
    func onError(error: String)
}

class ConsultaAddonsVentaRouter {

    var delegate: ConsultaAddonsVentaProtocol?
    var serverProd = ""
    var serverQA = ""
    var request: ConsultaAddonsRequest!
    var requestData: ConsultaAddonsRequestStruct!

    init(request: ConsultaAddonsRequest) {
        self.request = request
        self.requestData = makeRequest(request: self.request)
    }

    func getAddonsResponse() {
        let urlString = AppDelegate.API_SALESFORCE + ApiDefinition.API_ADDONS_VENTA
        let url = URL(string: urlString)!
        let data = try? JSONEncoder().encode(self.requestData)

        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("Bearer \(statics.TOKEN	)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = data

        let task = URLSession.shared.dataTask(with: request) {data, response, error in
            if error != nil {
                print("🚨 Error \(String(describing: error))")
            }

            if let data = data {
                if let response = try? JSONDecoder().decode(AddonsStruct.self, from: data) {
                    print(response)
                    self.delegate?.onSucessAddons(response: response)
                } else {
                    self.delegate?.onError(error: "💭 Ha oucrrido un erro al obtener los adicionales")

                }
            }
        }
        task.resume()
    }

    func makeRequest(request: ConsultaAddonsRequest) -> ConsultaAddonsRequestStruct {
        var requestAddons = ConsultaAddonsRequestStruct()
        var infoAddons = InfoAddonsStruct()

        infoAddons.subcanalVenta = request.info.subcanalVenta
        infoAddons.distrito = request.info.distrito
        infoAddons.colonia = request.info.colonia
        infoAddons.consultaServ = request.info.consultaServ
        infoAddons.metodoPago = request.info.metodoPago
        infoAddons.canal = request.info.canal
        infoAddons.consultaAddons = request.info.consultaAddons
        infoAddons.estimuloFiscal = request.info.estimulo
        infoAddons.consultaPromo = request.info.consultaPromo
        infoAddons.plaza = request.info.plaza
        infoAddons.subcanal = request.info.subcanal
        infoAddons.codigoPostal = request.info.codigoPostal
        infoAddons.cluster = request.info.cluster
        infoAddons.canalFront = request.info.canalFront

        requestAddons.idPlan = request.idPlan
        requestAddons.info = infoAddons
        return requestAddons
    }
}
