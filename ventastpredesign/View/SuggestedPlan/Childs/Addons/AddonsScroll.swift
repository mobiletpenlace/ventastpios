//
//  AddonsScroll.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 27/04/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus
import Amplitude

class AddonsScroll: BaseVentasView {

    deinit {
        print("DEINIT ADDONSSCROLL")
    }

    @IBOutlet weak var buttonsHorizontalScroll: UIScrollView!
    @IBOutlet weak var wifiExtenderButton: UIButton!
    @IBOutlet weak var tvButton: UIButton!
    @IBOutlet weak var moreChannelsButton: UIButton!
    @IBOutlet weak var tvPremiumButton: UIButton!
    @IBOutlet weak var specialDisscountsButton: UIButton!
    @IBOutlet weak var secureHomeButton: UIButton!

    // WIFI EXTENDER
    // View
    @IBOutlet weak var wifiExtenderView: UIView!
    // Label
    @IBOutlet weak var devicesWiFiLabel: UILabel!
    @IBOutlet weak var deleteWiFiButton: UIView!

    // TV ADICIONAL
    // Label
    @IBOutlet weak var devicesTVLabel: UILabel!
    @IBOutlet weak var deleteTVButton: UIView!
    // Views
    @IBOutlet weak var tvAditionalView: UIView!

    // Channels
    // Buttons
    @IBOutlet weak var includeChanelsButton: UIButton!
    @IBOutlet weak var pack1ChanelsButton: UIButton!
    @IBOutlet weak var pack2ChanelsButton: UIButton!
    // Views
    @IBOutlet weak var channelsView: UIView!
    @IBOutlet weak var includeChannelsView: UIView!
    @IBOutlet weak var pack1ChanelsView: UIView!
    @IBOutlet weak var pack2ChanelsView: UIView!
    @IBOutlet weak var otherView: UIView!
    // Label
    @IBOutlet weak var includedChanelsLabel: UILabel!
    @IBOutlet weak var pack1ChanelsLabel: UILabel!
    @IBOutlet weak var pack2ChanelsLabel: UILabel!
    // Constraint
    @IBOutlet weak var topMasCanalesConstraint: NSLayoutConstraint!

    // TV Premium
    // Buttons
    @IBOutlet weak var hboPremiumButton: UIButton!
    @IBOutlet weak var starPremiumButton: UIButton!
    @IBOutlet weak var universalPremiumButton: UIButton!
    // Views
    @IBOutlet weak var tvPremiumView: UIView!
    @IBOutlet weak var hboView: UIView!
    @IBOutlet weak var starView: UIView!
    @IBOutlet weak var universalView: UIView!
    // Constraint
    @IBOutlet weak var topTVPremiumConstraint: NSLayoutConstraint!

    // Beer promo
    @IBOutlet weak var beerBrandImageView: UIImageView!
    @IBOutlet weak var beerPromoView: UIView!
    @IBOutlet weak var beerPromoButtonView: UIView!
    @IBOutlet weak var beerPromoImageView: UIImageView!

    // Buttons
    @IBOutlet weak var addBeerButton: UIButton!
    // Labels
    @IBOutlet weak var beerStackPrice: UIStackView!
    @IBOutlet weak var beerDiscountLabel: UILabel!
    @IBOutlet weak var beerPriceLabel: UILabel!
    @IBOutlet weak var beerDetailLabel: UILabel!
    @IBOutlet weak var changeBeerLabel: UILabel!

    // Aux variables
    var selectedBeerId: Int?
    var selectedBeer: Beer?

    // Special Discounts
    // Buttons
    @IBOutlet weak var employeeDiscountButton: UIButton!
    @IBOutlet weak var fifthyDiscountButton: UIButton!
    @IBOutlet weak var bimboDiscountButton: UIButton!
    // Views
    @IBOutlet weak var specialDiscountView: UIView!
    @IBOutlet weak var employeeDiscountView: UIView!
    @IBOutlet weak var fifthyDiscountView: UIView!
    @IBOutlet weak var bimboDiscountView: UIView!
    // Constraint
    @IBOutlet weak var topSpecialDiscounts: NSLayoutConstraint!

    // HOGAR SEGURO
    // Buttons
    @IBOutlet weak var alertPlanButton: UIButton!
    @IBOutlet weak var reactionPlanButton: UIButton!
    @IBOutlet weak var controlPlanButton: UIButton!
    // Views
    @IBOutlet weak var secureHomeView: UIView!
    @IBOutlet weak var alertPlanView: UIView!
    @IBOutlet weak var reactionPlanView: UIView!
    @IBOutlet weak var controlPlanView: UIView!
    @IBOutlet weak var detailPlansView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var promocionesheigth: NSLayoutConstraint!
    @IBOutlet weak var insideHeigth: NSLayoutConstraint!
    @IBOutlet weak var contentHeigth: NSLayoutConstraint!
    @IBOutlet weak var specialDiscountsView: UIView!

    // Other products
    @IBOutlet weak var otherProductsView: UIView!
    @IBOutlet weak var contentOtherProductsView: UIView!
    @IBOutlet weak var otherProductsHeigth: NSLayoutConstraint!
    @IBOutlet weak var insideOtherProductsHeigth: NSLayoutConstraint!

    var priceServicios = 0
    var priceProductos = 0
    var priceDiscount = 0
    let checkImage = UIImage(named: "Image_rede_blue_check.png")
    let myImageView: UIImageView = UIImageView()
    let myImageView2: UIImageView = UIImageView()
    let myImageView3: UIImageView = UIImageView()
    let myImageView4: UIImageView = UIImageView()
    var additionalsTVViewController: AdditionalsTVViewController!
    var additionalsWifiViewController: AdditionalWifiViewController!
    var mViewModel: RecommendedPlanViewModel!
    var infoAddons: InfoAdicionalesCreacion = InfoAdicionalesCreacion()
    var mInfo: InfoAdicionalesCreacion?
    var mProductosAdd = ProductosAdd()
    var mPromocionesAdd = PromocionesAdd()
    var mServiciosAdd = ServiciosAdd()
    var selectedServicios: [ServiciosCreacion] = []
    var selectedProductos: [ProductosCreacion] = []
    var selectedPromociones: [PromocionesCreacion] = []
    var arregloLocal: [Promocion] = []
    var arregloLocal2: [Adicional] = []
    let PromocionCanalesIncluidos: Adicional! = Adicional()
    let PromocionCanalesAvanzada: Adicional! = Adicional()
    let PromocionCanalesPremium: Adicional! = Adicional()
    var TvAddicional: Servicio! = Servicio()
    var ExtenderAddicional: Servicio! = Servicio()
    let CanalHBO: Adicional! = Adicional()
    let CanalSTAR: Adicional! =  Adicional()
    let CanalUNIVERSAL: Adicional! = Adicional()
    var mServicios: [Servicio] = []
    var mProductosPremium: Producto = Producto()
    var mProductosParrillas: Producto = Producto()
    var mPromociones: [Promocion] = []
    var CostoInstalacion: Float = 0.0
    var mRecomendacion: LlenadoObjeto = LlenadoObjeto()
    var newLimitSpecialDiscount = 832
    var mBeersPresenter: BeerPresenter!
    var isBeerCoverageValid = false
    var mBeers: BeersResponse?
    var modeloID: String?

    func doublePlayPlan() {
        tvButton.isHidden = true
        moreChannelsButton.isHidden = true
        tvPremiumButton.isHidden = true
        topMasCanalesConstraint.constant = 0
        topSpecialDiscounts.constant = 0
        tvAditionalView.isHidden = true
        tvPremiumView.isHidden = true
        channelsView.isHidden = true
        contentHeigth.constant = wifiExtenderView.height + specialDiscountsView.height
         newLimitSpecialDiscount = 138
    }

    @IBAction func beerViewButton(_ sender: UIButton) {
        let vc = BeerSelectViewController(nibName: "BeerSelectViewController", bundle: nil)
        vc.selectedPromoID = selectedBeerId ?? -1
        vc.beers = self.mBeers?.getProducts ?? nil
        vc.selectedBeer = self.selectedBeer
        self.present(vc, animated: true, completion: nil)
    }

    override func getPresenter() -> BasePresenter? {
        mBeersPresenter = BeerPresenter(view: self)
        return mBeersPresenter
    }

    override func viewDidLoad() {
        super.setView(view: self.view)
        super.viewDidLoad()
        mBeersPresenter.validateCoverage(mBeersDelegate: self as BeersDelegate?)
        beerExtId.isSelected = false
        buttonsHorizontalScroll.scrollIndicatorInsets = UIEdgeInsets(top: 0, left: 0, bottom: -5, right: 0)
        deleteWiFiButton.isHidden = true
        devicesWiFiSelected(selected: false)
        devicesTVSelected(selected: false)

        if faseSelectedPlan.firstPlan == true {
            if faseSelectedPlan.plan1.familiaPaquete.contains(string: "Doble") == true {
                doublePlayPlan()
            }
        } else {
            if faseSelectedPlan.plan2.familiaPaquete.contains(string: "Doble") == true {
                doublePlayPlan()
            }
        }

        SwiftEventBus.onMainThread(self, name: "Beer") { [weak self] result in

            guard let self = self else {return}
            if let value = result?.object as? Beer {
                self.selectedBeer = value
                self.selectedBeerId = Int(value.id ?? -1)
                self.beerPromoSelect(selectedBeer: value)
            } else {
                self.selectedBeer = nil
                self.beerPromoDeselect()
            }

        }

        SwiftEventBus.onMainThread(self, name: "WiFi") { [weak self] result in
            self?.setNumberOfWiFi(numero: (result?.object as? Int)!)
            self?.devicesWiFiSelected(selected: true)
        }

        SwiftEventBus.onMainThread(self, name: "TV") { [weak self] result in
            self?.setNumberOfTV(numero: (result?.object as? Int)!)
            self?.devicesTVSelected(selected: true)
        }

        SwiftEventBus.onMainThread(self, name: "recibe_addon_productosPremium") { [weak self] result in
            let value = result?.object as? Producto
            self?.mProductosPremium = value!
        }

        SwiftEventBus.onMainThread(self, name: "recibe_addon_productosParrillas") { [weak self] result in
            let value = result?.object as? Producto
            self?.mProductosParrillas = value!
        }

        SwiftEventBus.onMainThread(self, name: "recibe_addon_servicios") { [weak self] result in
            let value = result?.object as? [Servicio]
            self?.mServicios = value!
        }

        SwiftEventBus.onMainThread(self, name: "recibe_addon_Promos") { [weak self] result in
            let value = result?.object as? [Promocion]
            self?.guardarLocal(arreglo: value!)
        }

        SwiftEventBus.onMainThread(self, name: "recibe_other_products") { [weak self] result in
            let value = result?.object as? [Adicional]
            self?.guardarLocalOtherProducts(arreglo: value!)
        }

        SwiftEventBus.onMainThread(self, name: "Agrega_Promocion") { [weak self] result in
            let value = result?.object as? Promocion
            self?.agregaPromo(promocion: value!)
        }

        SwiftEventBus.onMainThread(self, name: "Retira_Promocion") { [weak self] result in
            let value = result?.object as? Promocion
            self?.eliminaPromo(promocion: value!)
        }

        SwiftEventBus.onMainThread(self, name: "Agrega_Addon") { [weak self] result in
            let value = result?.object as? Adicional
            self?.agregaAddon(adicional: value!)
        }

        SwiftEventBus.onMainThread(self, name: "Retira_Addon") { [weak self] result in
            let value = result?.object as? Adicional
            self?.eliminaAddon(value!)
        }
        SwiftEventBus.onMainThread(self, name: "GuardaAddons") { [weak self] _ in
            self?.guardaAddons()
        }

        SwiftEventBus.onMainThread(self, name: "AgregaProductoCelda") { [weak self] result in
            let value = result?.object as? String
            self?.findAddon(value!)
        }

        SwiftEventBus.onMainThread(self, name: "recibe_recomendacion_service") { [weak self] result in
            let value = result?.object as? LlenadoObjeto
            self?.mRecomendacion = value!
            self?.setServicios()
            self?.setAditionals()
            self?.setAddon()
            self?.total()
        }

        SwiftEventBus.onMainThread(self, name: "AdjustSizeScroll") { [weak self] _ in
            self?.dimensionar()
        }

    }

    func findAddon(_ addon: String) {
        var existAddon: Bool = false
        for servicio in selectedServicios {
         if servicio.id == addon {
                existAddon = true
            }
        }
        for producto in selectedProductos {
            if producto.id == addon {
                existAddon = true
            }
        }
        if !existAddon {
            if addon == TvAddicional.Id {
                setNumberOfTV(numero: 1)
            }
            if addon == ExtenderAddicional.Id {
                setNumberOfWiFi(numero: 1)
            }
            if addon == CanalSTAR.Id {
                starAction(true)
            }
            if addon == CanalHBO.Id {
                hboAction(true)
            }
            if addon == CanalUNIVERSAL.Id {
                universalAction(true)
            }
            if addon == PromocionCanalesIncluidos.Id {
                chanelsIncludeAction(true)
            }
            if addon == PromocionCanalesAvanzada.Id {
                chanelsPack1Action(true)
            }
            if addon == PromocionCanalesAvanzada.Id {
                chanelsPack2Action(true)
            }
        }
    }

    func setAddon() {
        if mRecomendacion.hasRecomendation {
            for servicio in mServicios {
                if servicio.Id == mRecomendacion.idServicioRecomendado {
                    if servicio.nombre.contains(string: "TV ADICIONAL") || servicio.nombre.contains(string: "Television Adicional") {
                        setNumberOfTV(numero: 1)
                        devicesTVSelected(selected: true)
                    } else if servicio.nombre.contains(string: "EXTENDER") {
                        setNumberOfWiFi(numero: 1)
                        devicesWiFiSelected(selected: true)
                    }
                }
            }
        }
    }

    @IBAction func deleteWiFiDevicesButton(_ sender: Any) {
        devicesWiFiSelected(selected: false)
        devicesWiFiLabel.text = "Agregar equipos"
        eliminaServicio(servicio: ExtenderAddicional)
    }

    @IBAction func deleteTVDevicesButton(_ sender: Any) {
        devicesTVSelected(selected: false)
        devicesTVLabel.text = "Agregar equipos"
        eliminaServicio(servicio: TvAddicional)
    }

    func guardarLocal(arreglo: [Promocion]) {
        arregloLocal = arreglo
        llenaViewSpecialDiscounts(arreglo: arregloLocal, identifier: "PromotionCell", anchoCelda: UIScreen.main.bounds.width, altoCelda: includeChanelsButton.frame.height + 10, container: self.specialDiscountsView, clase: claseAddon.Promocion)

    }

    func guardarLocalOtherProducts(arreglo: [Adicional]) {
        arregloLocal2 = arreglo
        llenaViewOtherProducts(arreglo: arregloLocal2, identifier: "OtherProductsCell", anchoCelda: UIScreen.main.bounds.width, altoCelda: includeChanelsButton.frame.height + 20, container: self.contentOtherProductsView, clase: claseAddon.Promocion)

    }

    override func viewDidAppear(_ animated: Bool) {
        fasePostPreguntas.Index = 1
        Amplitude.instance().logEvent("Show_AddonsScreen")
    }

    override func viewDidDisappear(_ animated: Bool) {
        // SwiftEventBus.post("", sender: nil)
        super.viewDidDisappear(animated)
        SwiftEventBus.unregister(self)
    }

//    override func viewWillDisappear(_ animated: Bool) {
//        print("ENTRO AL VIEWWILLDISSAPEAR DEL SCROLL")
//        SwiftEventBus.unregister(self)
//    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func addCheck(viewParent: UIView, viewChild: UIView) {
        myImageView.contentMode = UIView.ContentMode.scaleAspectFit
        myImageView.frame.size.width = 20
        myImageView.frame.size.height = 20
        myImageView.frame.origin.x = viewChild.frame.origin.x + viewChild.width - 14
        myImageView.frame.origin.y = viewChild.frame.origin.y - 6
        myImageView.image = checkImage
        viewParent.addSubview(myImageView)
    }

    func addCheck2(viewParent: UIView, viewChild: UIView) {
        myImageView2.contentMode = UIView.ContentMode.scaleAspectFit
        myImageView2.frame.size.width = 20
        myImageView2.frame.size.height = 20
        myImageView2.frame.origin.x = viewChild.frame.origin.x + viewChild.width - 14
        myImageView2.frame.origin.y = viewChild.frame.origin.y - 6
        myImageView2.image = checkImage
        viewParent.addSubview(myImageView2)
    }

    func addCheck3(viewParent: UIView, viewChild: UIView) {
        myImageView3.contentMode = UIView.ContentMode.scaleAspectFit
        myImageView3.frame.size.width = 20
        myImageView3.frame.size.height = 20
        myImageView3.frame.origin.x = viewChild.frame.origin.x + viewChild.width - 14
        myImageView3.frame.origin.y = viewChild.frame.origin.y - 6
        myImageView3.image = checkImage
        viewParent.addSubview(myImageView3)
    }

    func addCheck4(viewParent: UIView, viewChild: UIView) {
        myImageView4.contentMode = UIView.ContentMode.scaleAspectFit
        myImageView4.frame.size.width = 20
        myImageView4.frame.size.height = 20
        myImageView4.frame.origin.x = viewChild.frame.origin.x + viewChild.width - 14
        myImageView4.frame.origin.y = viewChild.frame.origin.y - 6
        myImageView4.image = checkImage
        viewParent.addSubview(myImageView4)
    }

    @IBAction func wifiExtenderAction(_ sender: Any) {
        deselectAll()
        if self.wifiExtenderButton.isSelected == false {
            selectSectionScroll(button: wifiExtenderButton, positionX: 0, positionY: Int(wifiExtenderView.frame.origin.y))
        }
    }

    @IBAction func tvAdicionalAction(_ sender: Any) {
        deselectAll()
        if self.tvButton.isSelected == false {
            selectSectionScroll(button: tvButton, positionX: 0, positionY: Int(tvAditionalView.frame.origin.y))
            }
    }

    @IBAction func masCanalesAction(_ sender: Any) {
        deselectAll()
        if self.moreChannelsButton.isSelected == false {
            selectSectionScroll(button: moreChannelsButton, positionX: 0, positionY: 276)
            }
    }

    @IBAction func tvPremiumAction(_ sender: Any) {
        deselectAll()
        if self.tvPremiumButton.isSelected == false {
            selectSectionScroll(button: tvPremiumButton, positionX: 0, positionY: 556)
            }
    }

    @IBAction func descEspecialesAction(_ sender: Any) {
        deselectAll()
        if self.specialDisscountsButton.isSelected == false {
            selectSectionScroll(button: specialDisscountsButton, positionX: 0, positionY: newLimitSpecialDiscount)
            }
    }

  // WiFi extender
    @IBAction func addWifiExtenderAction(_ sender: Any) {
        eliminaServicio(servicio: ExtenderAddicional)
        let additionalsWifiViewController: AdditionalWifiViewController = UIStoryboard(name: "AdditionalWifiViewController", bundle: nil).instantiateViewController(withIdentifier: "AdditionalWifiViewController") as!
            AdditionalWifiViewController
        additionalsWifiViewController.providesPresentationContextTransitionStyle = true
        additionalsWifiViewController.definesPresentationContext = true
        additionalsWifiViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        additionalsWifiViewController.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        UIApplication.topViewController()?.present(additionalsWifiViewController, animated: false, completion: nil)
    }

    // TV adicional
      @IBAction func addTVExtenderAction(_ sender: Any) {
        eliminaServicio(servicio: TvAddicional)
        let additionalsTVViewController: AdditionalsTVViewController = UIStoryboard(name: "AdditionalsTVViewController", bundle: nil).instantiateViewController(withIdentifier: "AdditionalsTVViewController") as! AdditionalsTVViewController
        additionalsTVViewController.providesPresentationContextTransitionStyle = true
        additionalsTVViewController.definesPresentationContext = true
        additionalsTVViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        additionalsTVViewController.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        UIApplication.topViewController()?.present(additionalsTVViewController, animated: false, completion: nil)
      }

  // + CANALES
    @IBAction func chanelsIncludeAction(_ sender: Any) {
        if self.includeChanelsButton.isSelected == false {
            offChannels()
            onChannelsInclude()
            agregaAddon(adicional: PromocionCanalesIncluidos)
        } else {
            offChannels()
            eliminaAddon(PromocionCanalesIncluidos)
            self.includeChanelsButton.isSelected = false

        }
    }

    @IBAction func chanelsPack1Action(_ sender: Any) {
        if self.pack1ChanelsButton.isSelected == false {
            offChannels()
            onChanelsPack1()
            agregaAddon(adicional: PromocionCanalesAvanzada)
        } else {
            eliminaAddon(PromocionCanalesAvanzada)
            offChannels()
        }
    }

    @IBAction func chanelsPack2Action(_ sender: Any) {
        if self.pack2ChanelsButton.isSelected == false {
            offChannels()
            onChanelsPack2()
            agregaAddon(adicional: PromocionCanalesPremium)
        } else {
            eliminaAddon(PromocionCanalesPremium)
            offChannels()
            self.pack2ChanelsButton.isSelected = false
                   }
    }

    // TV PREMIUM
    @IBAction func hboAction(_ sender: Any) {
        if self.hboPremiumButton.isSelected == false {
            self.hboPremiumButton.borderColor = UIColor(named: "Base_rede_button_dark")
            addCheck2(viewParent: tvPremiumView, viewChild: hboView)
            self.hboPremiumButton.borderWidth = 3
            self.hboPremiumButton.isSelected = true
            agregaAddon(adicional: CanalHBO)
        } else {
            eliminaAddon(CanalHBO)
            myImageView2.removeFromSuperview()
            offPremiums(name: "hbo")
            // offPremiums(true)
            self.hboPremiumButton.isSelected = false
        }
    }

    @IBAction func starAction(_ sender: Any) {
        if self.starPremiumButton.isSelected == false {
            self.starPremiumButton.borderColor = UIColor(named: "Base_rede_button_dark")
            self.starPremiumButton.borderWidth = 3
            addCheck3(viewParent: tvPremiumView, viewChild: starView)
            self.starPremiumButton.isSelected = true
            agregaAddon(adicional: CanalSTAR)
            } else {
            eliminaAddon(CanalSTAR)
            myImageView3.removeFromSuperview()
            offPremiums(name: "star")
            // offPremiums(false)
            self.starPremiumButton.isSelected = false
        }
    }

    @IBAction func universalAction(_ sender: Any) {
        if self.universalPremiumButton.isSelected == false {
            self.universalPremiumButton.borderColor = UIColor(named: "Base_rede_button_dark")
            self.universalPremiumButton.borderWidth = 3
            addCheck4(viewParent: tvPremiumView, viewChild: universalView)
            self.universalPremiumButton.isSelected = true
            // AGREGAR EL ADDON
            agregaAddon(adicional: CanalUNIVERSAL)
            } else {
            // ELIMINAR EL ADDON
            eliminaAddon(CanalUNIVERSAL)
            myImageView4.removeFromSuperview()
            offPremiums(name: "universal")
            // offPremiums(false)
            self.universalPremiumButton.isSelected = false
        }
    }

}
