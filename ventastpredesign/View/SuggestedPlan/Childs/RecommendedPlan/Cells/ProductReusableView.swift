//
//  ProductReusableView.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 24/05/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class ProductReusableView: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var viewGradient: GradientView!
    @IBOutlet weak var bigLabel: UILabel!
    @IBOutlet weak var cellLabel: UILabel!

    var arregloProductos: ProductosIncluidosChild!
    var streaming: String?
    var numLineasTelefonicas: String?
    var color: String?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        bigLabel.isHidden = true
        viewGradient.isHidden = true
        imageView.setNeedsLayout()
        imageView.layoutIfNeeded()
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }

    func setData(_ arreglo: NSObject, _ Color: String, _ Streaming: String ) {
        arregloProductos = arreglo as? ProductosIncluidosChild
//        cellLabel.text = String(arregloProductos.IndexAux)

        if faseConfigurarPlan.isNegocio {
            cellLabel.font = cellLabel.font.withSize(15)
            bigLabel.font = bigLabel.font.withSize(80)
        }

        switch arregloProductos.IndexAux {
        case 0:
            cellLabel.isHidden = true
            bigLabel.isHidden = false
            imageView.isHidden = true
            viewGradient.isHidden = false
            bigLabel.text = arregloProductos.canalesInc
            let label = UILabel(frame: viewGradient.bounds)
            label.text = "Canales"
            label.font = UIFont(name: "Helvetica Neue Medium", size: UIFont.labelFontSize)
            label.adjustsFontSizeToFitWidth = true
            viewGradient.addSubview(label)
            viewGradient.mask = label

            switch  Color {
            case _ where ((Color.contains("SÓNICO")) == true):
                viewGradient.firstColor = UIColor(named: "Base_rede_init_diviertete+") ?? UIColor.gray
                viewGradient.secondColor = UIColor(named: "Base_rede_end_diviertete+") ?? UIColor.gray
                bigLabel.textColor = UIColor(named: "Base_rede_init_diviertete+")
            case _ where ((Color.contains("TURBO")) == true):
                viewGradient.firstColor = UIColor(named: "Base_rede_init_diviertete") ?? UIColor.gray
                viewGradient.secondColor = UIColor(named: "Base_rede_end_diviertete") ?? UIColor.gray
                bigLabel.textColor = UIColor(named: "Base_rede_init_diviertete")
            case _ where ((Color.contains("ULTRA SÓNICO")) == true):
                viewGradient.firstColor = UIColor(named: "Base_rede_init_emocionate") ?? UIColor.gray
                viewGradient.secondColor = UIColor(named: "Base_rede_end_emocionate") ?? UIColor.gray
                bigLabel.textColor = UIColor(named: "Base_rede_init_emocionate")
            case _ where ((Color.contains("RELÁMPAGO")) == true):
                viewGradient.firstColor = UIColor(named: "Base_rede_init_sorprendete") ?? UIColor.gray
                viewGradient.secondColor = UIColor(named: "Base_rede_end_sorprendete") ?? UIColor.gray
                bigLabel.textColor = UIColor(named: "Base_rede_init_sorprendete")
            case _ where ((Color.contains("EXPERTO")) == true):
                viewGradient.firstColor = UIColor(named: "Base_rede_init_1000_micronegocios") ?? UIColor.gray
                viewGradient.secondColor = UIColor(named: "Base_rede_init_1000_micronegocios") ?? UIColor.gray
                bigLabel.textColor = UIColor(named: "Base_rede_init_1000_micronegocios")
            case _ where ((Color.contains("PROFESIONAL")) == true):
                viewGradient.firstColor = UIColor(named: "Base_rede_init_500_micronegocios") ?? UIColor.gray
                viewGradient.secondColor = UIColor(named: "Base_rede_init_500_micronegocios") ?? UIColor.gray
                bigLabel.textColor = UIColor(named: "Base_rede_init_500_micronegocios")
            case _ where ((Color.contains("EMPRENDEDOR")) == true):
                viewGradient.firstColor = UIColor(named: "Base_rede_init_200_micronegocios") ?? UIColor.gray
                viewGradient.secondColor = UIColor(named: "Base_rede_init_200_micronegocios") ?? UIColor.gray
                bigLabel.textColor = UIColor(named: "Base_rede_init_200_micronegocios")
            case _ where ((Color.contains("DINAMICO")) == true):
                viewGradient.firstColor = UIColor(named: "Base_rede_init_100_micronegocios") ?? UIColor.gray
                viewGradient.secondColor = UIColor(named: "Base_rede_init_100_micronegocios") ?? UIColor.gray
                bigLabel.textColor = UIColor(named: "Base_rede_init_100_micronegocios")
            case _ where ((Color.contains("MICRO 40")) == true):
                viewGradient.firstColor = UIColor(named: "Base_rede_init_40_micronegocios") ?? UIColor.gray
                viewGradient.secondColor = UIColor(named: "Base_rede_init_40_micronegocios") ?? UIColor.gray
                bigLabel.textColor = UIColor(named: "Base_rede_init_40_micronegocios")
            case _ where ((Color.contains("MICRO 20")) == true):
                viewGradient.firstColor = UIColor(named: "Base_rede_init_20_micronegocios") ?? UIColor.gray
                viewGradient.secondColor = UIColor(named: "Base_rede_init_20_micronegocios") ?? UIColor.gray
                bigLabel.textColor = UIColor(named: "Base_rede_init_20_micronegocios")
            case _ where ((Color.contains("VELOZ")) == true):
                viewGradient.firstColor = UIColor(named: "Base_rede_init_20_micronegocios") ?? UIColor.gray
                viewGradient.secondColor = UIColor(named: "Base_rede_init_20_micronegocios") ?? UIColor.gray
                bigLabel.textColor = UIColor(named: "Base_rede_init_20_micronegocios")
            default:
                viewGradient.firstColor = UIColor.black
                viewGradient.secondColor = UIColor.black
        }

        case 1:
            var imageNetflix = UIImage(named: "Icon_netflix")
            var imageAmazon = UIImage(named: "Image_rede_amazonPrime")
            var numPantallas = ""
            arregloProductos.nombre?.westernArabicNumeralsOnly
            if Streaming.contains(string: "NETFLIX") {
                imageView.image = imageNetflix
                if Streaming.contains(string: "ESTANDAR") {
                    numPantallas = "2"
                }
                if Streaming.contains(string: "PREMIUM") {
                    numPantallas = "4"
                }
                cellLabel.text = numPantallas + " \nPantallas"
            }
            if Streaming == "AMAZON" {
                imageView.image = imageAmazon
                cellLabel.text = "Prime \nincluido"
            }
//            imageView.image = imageNetflix
//            cellLabel.text = "4 \nPantallas"
        case 2:
            var n = "1"
            cellLabel.text = "TV x " + n + "\nequipo"

        case 3:

            let imagePhoneLines = UIImage(named: "Image_rede_phoneLines")
            imageView.image = imagePhoneLines
            cellLabel.text = "TV x 1 \nequipo"
            cellLabel.text = arregloProductos.lineasTelefonicasInc! + " Línea"

        default:
            break
        }

    }
}

extension String {
    var westernArabicNumeralsOnly: String {
        let pattern = UnicodeScalar("0")..."9"
        return String(unicodeScalars
            .compactMap { pattern ~= $0 ? Character($0) : nil })
    }
}
