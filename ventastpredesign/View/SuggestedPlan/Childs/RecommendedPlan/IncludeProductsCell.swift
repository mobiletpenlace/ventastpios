//
//  IncludeProductsCell.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 24/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class IncludeProductsCell: BaseCollectionViewCell {
    @IBOutlet weak var nombreLabel: UILabel!
    @IBOutlet weak var cellButton: UIButton!
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var selectedImage: UIImageView!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var addButton: UIButton!

    var isCheck: Bool = false
    var mAdicional: Adicional!
    let checkImage = UIImage(named: "Image_rede_blue_check.png")
    let myImageView2: UIImageView = UIImageView()
    var max: String = ""
    override func awakeFromNib() {
        super.awakeFromNib()
        dissableSubstractButton()
        enableAddButton()
        selectedImage.isHidden = true
//        SwiftEventBus.onMainThread(self, name: "RemoveEventBus") { [weak self] result in
//            self?.removeEventBus()
//        }
//        
//        SwiftEventBus.onMainThread(self, name: "AgregaPromoCelda") { [weak self] result in
//            if let adicional = result?.object as? String{
//                if(self?.mAdicional.Id == adicional){
//                    if(!self!.isCheck){
//                        self?.isCheck = !self!.isCheck
//                        self?.agregapromo()
//                    }
//                }
//            }
//        }
//       
//        SwiftEventBus.onMainThread(self, name: "RetiraPromoCelda") { [weak self] result in
//            if let promo = result?.object as? String{
//                if(self?.mAdicional.Id == promo){
//                    if(self!.isCheck){
//                        self?.isCheck = !self!.isCheck
//                        self?.retiraPromo()
//                    }
//                }
//            }
//        }
    }

    override func setData(_ arreglo: NSObject) {
        mAdicional = arreglo as? Adicional
        nombreLabel.text = mAdicional.nombre
        max = String(mAdicional.maximoAgregar)
    }

    @IBAction func minusButton(_ sender: AnyObject) {
        setViewsAndButtons(value: String(Int(numberLabel.text!)! - 1), maximo: max)
    }

    @IBAction func addButton(_ sender: AnyObject) {
        setViewsAndButtons(value: String(Int(numberLabel.text!)! + 1), maximo: max)
    }

    func setViewsAndButtons(value: String, maximo: String) {
        numberLabel.text = value
        switch value {
        case "0":
            retiraPromo()
            dissableSubstractButton()
            enableAddButton()
        case max:
            agregapromo()
            enableSubstractButton()
            dissableAddButton()
        default:
            agregapromo()
            enableSubstractButton()
            enableAddButton()
        }
        numberLabel.text = String(value)
    }

    func dissableSubstractButton() {
        minusButton.isEnabled = false
        minusButton.setImage(UIImage(named: "Icon_rede_Substract_Disable"), for: .disabled)
        minusButton.tintColor = UIColor.lightGray
    }

    func dissableAddButton() {
        addButton.isEnabled = false
        addButton.setImage(UIImage(named: "Icon_rede_Add_Disable"), for: .disabled)
        addButton.tintColor = UIColor.lightGray
    }

    func enableSubstractButton() {
        minusButton.isEnabled = true
        minusButton.tintColor = UIColor.purple
    }

    func enableAddButton() {
        addButton.isEnabled = true
        addButton.tintColor = UIColor.purple
    }

    func removeEventBus() {
        SwiftEventBus.unregister(self)
    }

    func retiraPromo() {
        self.cellButton.isSelected = false
        self.cellView.borderColor = UIColor(named: "Base_rede_semiclear_gray")
        self.cellView.borderWidth = 2
        selectedImage.isHidden = true
        SwiftEventBus.post("Retira_Addon", sender: mAdicional)
    }

    func agregapromo() {
        self.cellView.borderColor = UIColor(named: "Base_rede_button_dark")
        self.cellView.borderWidth = 3
        self.cellButton.isSelected = true
        selectedImage.isHidden = false

        SwiftEventBus.post("Agrega_Addon", sender: mAdicional)
    }

}
