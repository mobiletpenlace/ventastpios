//
//  RecommendedPlan.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 04/05/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//
import UIKit
import Foundation
import SwiftEventBus
import Amplitude

class NewRecommendedPlanViewController: BaseVentasView {

    deinit {
        print("DEINIT NEWRECOMMENDEDPLAN VIEW CONTORLLER")
    }

    @IBOutlet weak var suggestedPlanLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var gradientBackground: GradientView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var productosScrollView: UIScrollView!

    let checkImage = UIImage(named: "Image_rede_blue_check.png")
    let myImageView: UIImageView = UIImageView()
    let hiddenOne = true
    let numberFormatter = NumberFormatter()
    var bandera = false
    var contador = 0
    var objetoLocal = LlenadoObjeto()

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        if faseConfigurarPlan.isNegocio {
            suggestedPlanLabel.text = "Confirma tu plan"
        }
        if objetoLocal.Titulo == "" {
            SwiftEventBus.onMainThread(self, name: "SetPlanPrimario") { [weak self] result in
                if let value = result?.object as? LlenadoObjeto {
                    self?.setPlanPrimario(objeto: value)
                }
            }
        }
        if faseConfigurarPlan.vengodeBack == true {
            setPlanPrimario(objeto: faseSelectedPlan.selectedPlan)
            suggestedPlanLabel.text = "Confirma tu plan"
        }

    }

    override func viewWillDisappear(_ animated: Bool) {
        SwiftEventBus.unregister(self)
        // AQUI
    }

    override func viewWillAppear(_ animated: Bool) {
        productosScrollView.setNeedsLayout()
        productosScrollView.layoutIfNeeded()
    }

    func setPlanPrimario(objeto: LlenadoObjeto) {
        dump(objeto.servIncSinAgregar)
        bandera = true
        objetoLocal = objeto
        let numberFormatter = NumberFormatter()
        let label = UILabel(frame: gradientBackground.bounds)
        label.font = UIFont(name: "Helvetica Neue Bold", size: 40)
        gradientBackground.addSubview(label)
        gradientBackground.mask = label
        numberFormatter.numberStyle = NumberFormatter.Style.decimal

        if bandera == false {
            if objetoLocal.arregloProductos.count < 4 {
                if objetoLocal.streaming.contains(string: "NETFLIX") {
                    var objetoNetflix = ProductosIncluidosChild()
                    objetoNetflix.tipoServicioPadre = "Streaming"
                    switch objetoLocal.streaming {
                    case _ where (objetoLocal.streaming.contains(string: "NETFLIX ESTANDAR") == true):
                        objetoNetflix.nombre = "NETFLIX ESTANDAR"
                    case _ where (objetoLocal.streaming.contains(string: "NETFLIX PREMIUM") == true):
                        objetoNetflix.nombre = "NETFLIX PREMIUM"
                    default:
                        break
                    }
                    objetoLocal.arregloProductos.append(objetoNetflix)
                    bandera = true
                }
            }
        }

        titleLabel.text = objetoLocal.Titulo
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = .currency
        currencyFormatter.locale = Locale.current
        var precio = Int(objetoLocal.numeroTVs) ?? 0
        priceLabel.text = currencyFormatter.string(from: NSNumber(value: objetoLocal.precioProntoPago + objetoLocal.precioAdicionales))! + " /mes"
        switch  titleLabel.text {
        case _ where ((titleLabel.text!.contains("SÓNICO")) == true):
                gradientBackground.firstColor = UIColor(named: "Base_rede_init_diviertete+") ?? UIColor.gray
                gradientBackground.secondColor = UIColor(named: "Base_rede_end_diviertete+") ?? UIColor.gray
        case _ where ((titleLabel.text!.contains("TURBO")) == true):
                gradientBackground.firstColor = UIColor(named: "Base_rede_init_diviertete") ?? UIColor.gray
                gradientBackground.secondColor = UIColor(named: "Base_rede_end_diviertete") ?? UIColor.gray
        case _ where ((titleLabel.text!.contains("ULTRA SÓNICO")) == true):
                gradientBackground.firstColor = UIColor(named: "Base_rede_init_emocionate") ?? UIColor.gray
                gradientBackground.secondColor = UIColor(named: "Base_rede_end_emocionate") ?? UIColor.gray
        case _ where ((titleLabel.text!.contains("RELÁMPAGO")) == true):
                gradientBackground.firstColor = UIColor(named: "Base_rede_init_sorprendete") ?? UIColor.gray
                gradientBackground.secondColor = UIColor(named: "Base_rede_end_sorprendete") ?? UIColor.gray
        case _ where ((titleLabel.text!.contains("EXPERTO")) == true):
                gradientBackground.firstColor = UIColor(named: "Base_rede_init_1000_micronegocios") ?? UIColor.gray
                gradientBackground.secondColor = UIColor(named: "Base_rede_init_1000_micronegocios") ?? UIColor.gray
        case _ where ((titleLabel.text!.contains("PROFESIONAL")) == true):
                gradientBackground.firstColor = UIColor(named: "Base_rede_init_500_micronegocios") ?? UIColor.gray
                gradientBackground.secondColor = UIColor(named: "Base_rede_init_500_micronegocios") ?? UIColor.gray
        case _ where ((titleLabel.text!.contains("EMPRENDEDOR")) == true):
                gradientBackground.firstColor = UIColor(named: "Base_rede_init_200_micronegocios") ?? UIColor.gray
                gradientBackground.secondColor = UIColor(named: "Base_rede_init_200_micronegocios") ?? UIColor.gray
        case _ where ((titleLabel.text!.contains("DINAMICO")) == true):
                gradientBackground.firstColor = UIColor(named: "Base_rede_init_100_micronegocios") ?? UIColor.gray
                gradientBackground.secondColor = UIColor(named: "Base_rede_init_100_micronegocios") ?? UIColor.gray
        case _ where ((titleLabel.text!.contains("MICRO 40")) == true):
                gradientBackground.firstColor = UIColor(named: "Base_rede_init_40_micronegocios") ?? UIColor.gray
                gradientBackground.secondColor = UIColor(named: "Base_rede_init_40_micronegocios") ?? UIColor.gray
        case _ where ((titleLabel.text!.contains("MICRO 20")) == true):
                gradientBackground.firstColor = UIColor(named: "Base_rede_init_20_micronegocios") ?? UIColor.gray
                gradientBackground.secondColor = UIColor(named: "Base_rede_init_20_micronegocios") ?? UIColor.gray
        case _ where ((titleLabel.text!.contains("VELOZ")) == true):
                gradientBackground.firstColor = UIColor(named: "Base_rede_init_20_micronegocios") ?? UIColor.gray
                gradientBackground.secondColor = UIColor(named: "Base_rede_init_20_micronegocios") ?? UIColor.gray
        default:
                gradientBackground.firstColor = UIColor.black
                gradientBackground.secondColor = UIColor.black
        }

        if objetoLocal.hasPromoMegas == true {
            label.text = String(Int(objetoLocal.Megas)! * 2) + " Megas"
        } else {
            label.text = objetoLocal.Megas + " Megas"
        }

//        dump(objeto)

        for i in 0..<objetoLocal.arregloProductos.count {
            switch objetoLocal.arregloProductos[i].tipoServicioPadre {
            case _ where ((objetoLocal.arregloProductos[i].tipoServicioPadre?.contains("Television")) == true):
                objetoLocal.arregloProductos[i].IndexAux = 0
            case _ where ((objetoLocal.arregloProductos[i].tipoServicioPadre?.contains("Streaming")) == true):
                objetoLocal.arregloProductos[i].IndexAux = 1
            case _ where ((objetoLocal.arregloProductos[i].tipoServicioPadre?.contains("Internet")) == true):
                objetoLocal.arregloProductos[i].IndexAux = 2
            case _ where ((objetoLocal.arregloProductos[i].tipoServicioPadre?.contains("Telefonia")) == true):
                objetoLocal.arregloProductos[i].IndexAux = 3

            default:
                break
            }
        }

        objetoLocal.arregloProductos.sort { $0.IndexAux ?? 0 < $1.IndexAux ?? 0 }
        if objetoLocal.familiaPaquete.contains(string: "Doble") {
            if faseConfigurarPlan.isNegocio {
                llenaScroll(arreglo: Array(objetoLocal.arregloProductos.dropFirst()), streaming: objetoLocal.streaming, identifier: "ProductReusableView", anchoCelda: CGFloat(100 + 74), altoCelda: CGFloat(100), anchoScroll: CGFloat(110), altoScroll: CGFloat(100), scroll: productosScrollView)
            } else {
                llenaScroll(arreglo: Array(objetoLocal.arregloProductos.dropFirst()), streaming: objetoLocal.streaming, identifier: "ProductReusableView", anchoCelda: CGFloat(100 + 24), altoCelda: CGFloat(100), anchoScroll: CGFloat(110), altoScroll: CGFloat(100), scroll: productosScrollView)
            }
        } else {
            if faseConfigurarPlan.isNegocio {
                llenaScroll(arreglo: objetoLocal.arregloProductos, streaming: objetoLocal.streaming, identifier: "ProductReusableView", anchoCelda: CGFloat(140+24), altoCelda: CGFloat(140), anchoScroll: CGFloat(150), altoScroll: CGFloat(140), scroll: productosScrollView)
            } else {
                llenaScroll(arreglo: objetoLocal.arregloProductos, streaming: objetoLocal.streaming, identifier: "ProductReusableView", anchoCelda: CGFloat(100 + 24), altoCelda: CGFloat(100), anchoScroll: CGFloat(110), altoScroll: CGFloat(100), scroll: productosScrollView)
            }
        }
    }

    func llenaScroll(arreglo: [NSObject], streaming: String, identifier: String, anchoCelda: CGFloat, altoCelda: CGFloat, anchoScroll: CGFloat, altoScroll: CGFloat, scroll: UIScrollView) {
        // Elimina cualquier subvista existente en el scroll view
        scroll.subviews.forEach { $0.removeFromSuperview() }
        scroll.clipsToBounds = false

        // Calcula el número de celdas y ajusta el contentSize del scroll view
        let contador = arreglo.count
        scroll.contentSize = CGSize(width: anchoScroll * CGFloat(contador), height: altoScroll)

        for index in 0..<contador {
            // Carga la vista personalizada desde el nib
            if let view = Bundle.main.loadNibNamed(identifier, owner: self, options: nil)?.first as? ProductReusableView {
                // Configura los datos de la celda
                view.setData(arreglo[index], titleLabel.text!, streaming)
                // Ajusta el tamaño y posición de la celda
                view.frame.size.height = altoCelda
                view.frame.size.width = anchoCelda
                view.frame.origin.x = anchoScroll * CGFloat(index)
                // Añade la celda al scroll view
                scroll.addSubview(view)
            }
        }

        // Forzar el layout del scroll view
        scroll.setNeedsLayout()
        scroll.layoutIfNeeded()
    }


    override func viewDidAppear(_ animated: Bool) {
        fasePostPreguntas.Index = 0
    }

}
