//
//  PopUPDebtor.swift
//  ventastp
//
//  Created by Armando Isais Olguin Cabrera on 8/12/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import SwiftEventBus

class PopUPDebtor: BaseVentasView {

    @IBOutlet weak var ButtonView: UIView!
    @IBOutlet weak var LabelButton: UILabel!
    @IBOutlet weak var OkButton: UIButton!
    @IBOutlet weak var messageLabel: UILabel!

    func enableButton() {
        LabelButton.text = "Ok"
        ButtonView.backgroundColor = UIColor(named: "Base_rede_button_dark")
        OkButton.isUserInteractionEnabled = true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        customizeMessagge(typeMessage: 2, Name: "Jose Perez", Amount: "$ 919.20")
        LabelButton.text = "3..."
        OkButton.isUserInteractionEnabled = false
        ButtonView.backgroundColor = UIColor(named: "Base_titles_gray_medium")
        var runCount = 3
        Timer.scheduledTimer(withTimeInterval: 0.75, repeats: true) { [self] timer in
            runCount -= 1
            LabelButton.text = String(runCount) + "..."
            if runCount == 0 {
                self.enableButton()
                timer.invalidate()
            }
        }
    }

    func customizeMessagge(typeMessage: Int, Name: String, Amount: String) {

        switch typeMessage {
        case 1:
            messageLabel.text = "Este domicilio es deudor"
        case 2:

            messageLabel.text = Name + "\ntiene una deuda de " + Amount
            messageLabel.halfTextColorChange(fullText: messageLabel.text!, changeText: "\ntiene una deuda de ")
        default:
            messageLabel.text = "Este domicilio es deudor"
        }
        messageLabel.adjustsFontSizeToFitWidth = true
    }

    @IBAction func OkAction(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }

}
