//
//  PopUPDebtorCard.swift
//  ventastp
//
//  Created by Branchbit on 20/12/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

class PopUPDebtorCard: UIViewController {

    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var debtLabel: UILabel!
    @IBOutlet weak var promoLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        continueButton.setTitle("", for: .normal)
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
        // Do any additional setup after loading the view.
    }

    @IBAction func continueAction(_ sender: Any) {
        self.dismiss(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
