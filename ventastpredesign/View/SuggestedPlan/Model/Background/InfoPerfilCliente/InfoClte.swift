//
//  InfoClte.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 12/05/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class InfoClte: NSObject, Mappable {
    required init?(map: Map) {

    }

    func mapping(map: Map) {
        respuestas <- map["respuestas"]
        idPerfilClte <- map["idPerfilClte"]

    }

    override init() {
    }

    var respuestas: String?
    var idPerfilClte: String?

}
