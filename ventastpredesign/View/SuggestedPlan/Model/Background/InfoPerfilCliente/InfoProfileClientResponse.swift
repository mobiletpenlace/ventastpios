//
//  infoProfileClientResponse.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 12/05/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class InfoProfileClientResponse: BaseResponse {

    public required convenience init?(map: Map) {
        self.init()
    }

    override func mapping(map: Map) {
        resultDescription <- map["resultDescription"]
        result <- map["result"]
        infoClte <- map["infoClte"]
    }

    var infoClte: InfoClte?

}
