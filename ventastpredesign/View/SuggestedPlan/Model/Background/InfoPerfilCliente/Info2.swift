//
//  Info.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 12/05/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class Info2: NSObject, Mappable {
    required init?(map: Map) {

    }

    func mapping(map: Map) {
        cluster <- map["cluster"]
        distrito <- map["distrito"]
        Respuestas <- map["respuestas"]
        idPerfilClte <-  map["idPerfilClte"]
        cuentaBRM <- map["cuentaBRM"]
        plaza <- map["plaza"]
        tipoPlan <- map["tipoPlan"]
    }

    override init() {
    }
    var cuentaBRM: String?
    var cluster: String?
    var distrito: String?
    var idPerfilClte: String?
    var Respuestas: Respuestas?
    var plaza: String?
    var tipoPlan: String?
}
