//
//  Respuestas.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 12/05/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class Respuestas: NSObject, Mappable {
    required init?(map: Map) {

    }

    func mapping(map: Map) {
        appsFavoritas <- map["appsFavoritas"]
        proveedorInternet <- map["proveedorInternet"]
        numHabitaciones <- map["numHabitaciones"]
        personasConectadas <- map["personasConectadas"]
        proveedorTelefonia <- map["proveedorTelefonia"]
        proveedorTV <- map["proveedorTV"]
        UsosRed <- map["UsosRed"]
    }

    override init() {
    }

    var appsFavoritas: String?
    var proveedorInternet: String?
    var numHabitaciones: String?
    var personasConectadas: String?
    var proveedorTelefonia: String?
    var proveedorTV: String?
    var UsosRed: String?
}
