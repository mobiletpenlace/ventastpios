//
//  InfoAddons.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 28/05/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper
import RealmSwift

class InfoAddons: NSObject, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        serviciosAdd <- map["serviciosAdd"]
        promocionesAdd <- map["promocionesAdd"]
        productosAdd <- map["productosAdd"]
        costoInstalacionAdd <- map["costoGarantiaAdd"]
    }

    var  serviciosAdd: Servicios?
    var  promocionesAdd: Promociones?
    var  productosAdd: Productos?
    var  costoInstalacionAdd: Productos?

}
