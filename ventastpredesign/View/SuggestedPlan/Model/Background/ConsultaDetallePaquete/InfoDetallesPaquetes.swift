//
//  InfoDetallesPaquetes.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 18/05/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit

import ObjectMapper

class InfoDetallesPaquetes: NSObject, Mappable {
    required init?(map: Map) {

    }

    func mapping(map: Map) {
        infoPlanes <- map["infoPlanes"]
        infoAddons <- map["infoAddons"]
    }

    override init() {
    }

    var infoPlanes: InfoPlanes?
    var infoAddons: InfoAddonsNew?

}
