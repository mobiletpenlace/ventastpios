//
//  ServiciosPlanes.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 17/05/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit

import ObjectMapper

class ServiciosPlanes: NSObject, Mappable {
    required init?(map: Map) {

    }

    func mapping(map: Map) {
        nombre <- map["nombre"]
        Id <- map["Id"]
        cantidad <- map["cantidad"]
    }

    override init() {
    }

    var nombre: String?
    var Id: String?
    var cantidad: String?

}
