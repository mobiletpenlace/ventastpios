//
//  ConsultaDetalleRequest.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 17/05/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class ConsultaDetalleRequest: BaseRequest {

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        accion <- map["accion"]
        info <- map["info"]

    }

    var accion: String?
    var info: InfoConsultaDetallePaquetes?

}
