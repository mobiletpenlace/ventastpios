//
//  Promotions.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 17/05/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit

import ObjectMapper

class Promotions: NSObject, Mappable {
    required init?(map: Map) {

    }

    func mapping(map: Map) {
        tipoReferencia <- map["tipoReferencia"]
        nombre <- map["nombre"]
        imagen <- map["imagen"]
        Id <- map["Id"]
        esAutomatica <- map["esAutomatica"]
        adicionalProductoNombre <- map["adicionalProductoNombre"]
        adicionalProductoId <- map["adicionalProductoId"]
    }

    override init() {
    }
    var tipoReferencia: String?
    var nombre: String?
    var imagen: String?
    var Id: String?
    var esAutomatica: Bool?
    var adicionalProductoNombre: String?
    var adicionalProductoId: String?

}
