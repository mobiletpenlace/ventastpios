//
//  ServiciosRecomendados.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 17/05/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit

import ObjectMapper

class ServiciosRecomendados: NSObject, Mappable {
    required init?(map: Map) {

    }

    func mapping(map: Map) {
        servicios <- map["servicios"]
    }

    override init() {
    }

    var servicios: [ServiciosPlanes] = []

}
