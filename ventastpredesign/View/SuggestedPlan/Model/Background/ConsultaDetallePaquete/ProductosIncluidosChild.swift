//
//  ProductosIncluidosHijo.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 18/05/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper
import RealmSwift

class ProductosIncluidosChild: NSObject, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        tipoServicioPadre <- map["tipoServicioPadre"]
        tipoProducto <- map["tipoProducto"]
        streamingInc <- map["streamingInc"]
        precio <- map["precio"]
        nombre <- map["nombre"]
        megasInc <- map["megasInc"]
        lineasTelefonicasInc <- map["lineasTelefonicasInc"]
        imagen <- map["imagen"]
        Id <- map["Id"]
        canalesInc <- map["canalesInc"]
        Agrupacion <- map["Agrupacion"]
    }

    var tipoServicioPadre: String?
        var tipoProducto: String?
        var streamingInc: String?
        var precio: Int?
        var nombre: String?
        var megasInc: String?
        var lineasTelefonicasInc: String?
        var imagen: String?
        var Id: String?
        var canalesInc: String?
        var Agrupacion: String?
        var IndexAux: Int?

}
