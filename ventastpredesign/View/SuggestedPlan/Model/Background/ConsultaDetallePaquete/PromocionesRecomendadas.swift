//
//  PromocionesRecomendadas.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 17/05/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit

import ObjectMapper

class PromocionesRecomendadas: NSObject, Mappable {
    required init?(map: Map) {

    }

    func mapping(map: Map) {
        promociones <- map["promociones"]
    }

    override init() {
    }
    var promociones: [Promotions] = []
}
