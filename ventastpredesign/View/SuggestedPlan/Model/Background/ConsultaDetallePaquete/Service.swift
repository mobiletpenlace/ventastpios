//
//  Service.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 18/05/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper
import RealmSwift

class Service: NSObject, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        tipoServicio <- map["tipoServicio"]
        precio <- map["precio"]
        nombre <- map["nombre"]
        imagen <- map["mostrarAddon"]
        Id <- map["Id"]
    }

    var tipoServicio: String?
    var precio: Int?
    var nombre: String?
    var imagen: String?
    var Id: String?

}
