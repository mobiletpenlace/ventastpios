//
//  ConsultaDetalleResponse.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 17/05/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class ConsultaDetalleResponse: BaseResponse {

    public required convenience init?(map: Map) {
        self.init()
    }

    override func mapping(map: Map) {
        resultDescription <- map["resultDescription"]
        result <- map["result"]
        info <- map["info"]
    }

    var info: InfoDetallesPaquetes?

}
