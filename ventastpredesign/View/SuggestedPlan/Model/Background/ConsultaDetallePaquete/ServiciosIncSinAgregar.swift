//
//  ServiciosIncSinAgregar.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 18/05/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper
import RealmSwift

class ServiciosIncSinAgregar: NSObject, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        servicioTerceros <- map["servicioTerceros"]
        precio <- map["precio"]
        nombre <- map["nombre"]
        mostrarAddon <- map["mostrarAddon"]
        maximoAgregar <- map["maximoAgregar"]
        llevaATA <- map["llevaATA"]
        imagen <- map["imagen"]
        id_DP_PlanServicioEquipo <- map["id_DP_PlanServicioEquipo"]
        Id <- map["Id"]
        cargoActivacionHS <- map["cargoActivacionHS"]
        cargoActDiferidoMesesHS <- map["cargoActDiferidoMesesHS"]
    }

    var servicioTerceros: String?
    var precio: Int?
    var nombre: String?
    var mostrarAddon: Bool?
    var maximoAgregar: Int?
    var llevaATA: String?
    var imagen: String?
    var id_DP_PlanServicioEquipo: String?
    var Id: String?
    var cargoActivacionHS: String?
    var cargoActDiferidoMesesHS: String?

}
