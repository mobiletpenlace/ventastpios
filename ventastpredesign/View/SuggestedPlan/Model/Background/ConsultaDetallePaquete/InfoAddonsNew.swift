//
//  InfoAddonsNew.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 18/05/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper
import RealmSwift

class InfoAddonsNew: NSObject, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        serviciosAdd <- map["serviciosAdd"]
    }

    var  serviciosAdd: ServiciosNew?

}
