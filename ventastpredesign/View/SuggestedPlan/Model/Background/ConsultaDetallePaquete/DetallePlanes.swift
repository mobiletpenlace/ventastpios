//
//  DetallePlanes.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 18/05/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper
import RealmSwift

class DetallePlanes: NSObject, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        wifiExtenderIncluidos <- map["wifiExtenderIncluidos"]
        tvIncluidas <- map["tvIncluidas"]
        tipoPlan <- map["tipoPlan"]
        tipoOferta <- map["tipoOferta"]
        tipoContrato <- map["tipoContrato"]
        servIncSinAgregar <- map["servIncSinAgregar"]
        servicioStreaming <- map["servicioStreaming"]
        productosIncluidos <- map["productosIncluidos"]
        precioProntoPago <- map["precioProntoPago"]
        precioLista <- map["precioLista"]
        plazo <- map["plazo"]
        nombrePaquete <- map["nombrePaquete"]
        megas <- map["megas"]
        imagenServicioStreaming <- map["imagenServicioStream"]
        imagenPaquete <- map["imagenPaquete"]
        id <- map["id"]
        familiaPaquete <- map["familiaPaquete"]
        detallePlan <- map["detallePlan"]
        canalesIncluidos <- map["canalesIncluidos"]
        opcionPlan <- map["opcionPlan"]

    }

    var wifiExtenderIncluidos: String?
    var tvIncluidas  = ""
    var tipoPlan: String?
    var tipoOferta: String?
    var tipoContrato: String?
    var servIncSinAgregar: ServIncSinAgregar?
    var servicioStreaming: String?
    var productosIncluidos: ProductosIncluidos?
    var precioProntoPago: Int?
    var precioLista: Int?
    var plazo: String?
    var nombrePaquete: String?
    var megas: String?
    var imagenServicioStreaming: String?
    var imagenPaquete: String?
    var id: String?
    var familiaPaquete: String?
    var detallePlan: String?
    var canalesIncluidos: String?
    var opcionPlan: String?

}
