//
//  Adicionales.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 17/05/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit

import ObjectMapper

class Adicionales: NSObject, Mappable {
    required init?(map: Map) {

    }

    func mapping(map: Map) {
        idProductosAdd <- map["idProductosAdd"]
        idpromocionesAdd <- map["idpromocionesAdd"]
        idServiciosAdd <- map["idServiciosAdd"]
    }

    override init() {
    }

    var idProductosAdd: [String] = []
    var idpromocionesAdd: [String] = []
    var idServiciosAdd: [String] = []

}
