//
//  Planes.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 17/05/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit

import ObjectMapper

class Plans: NSObject, Mappable {
    required init?(map: Map) {

    }

    func mapping(map: Map) {
        idPlanes <- map["idPlanes"]
    }

    override init() {
    }

    var idPlanes: [String] = []

}
