//
//  Recomendaciones.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 17/05/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit

import ObjectMapper

class Recomendaciones: NSObject, Mappable {
    required init?(map: Map) {

    }

    func mapping(map: Map) {
        servicioStreaming <- map["servicioStreaming"]
        serviciosRecomendados <- map["serviciosRecomendados"]
        promocionesRecomendadas <- map["promocionesRecomendadas"]
        precioProntoPago <- map["precioProntoPago"]
        precioLista <- map["precioLista"]
        PlanRecomendado <- map["PlanRecomendado"]
        opcionPlan <- map["opcionPlan"]
        numeroDispositivos <- map["numeroDispositivos"]
        NombreRecomendacion <- map["NombreRecomendacion"]
        NombrePlanRecomendado <- map["NombrePlanRecomendado"]
        ImagenURL <- map["ImagenURL"]
        id <- map["id"]
        estrat_A_Ejec <- map["estrat_A_Ejec"]
        Descripcion <- map["Descripcion"]

    }

    override init() {
    }

    var servicioStreaming: String?
    var serviciosRecomendados: ServiciosRecomendados?
    var promocionesRecomendadas: PromocionesRecomendadas?
    var precioProntoPago: Int?
    var precioLista: Int?
    var PlanRecomendado: String?
    var opcionPlan: String?
    var numeroDispositivos: Int?
    var NombreRecomendacion: String?
    var NombrePlanRecomendado: String?
    var ImagenURL: String?
    var id: String?
    var estrat_A_Ejec: String?
    var Descripcion: String?

}
