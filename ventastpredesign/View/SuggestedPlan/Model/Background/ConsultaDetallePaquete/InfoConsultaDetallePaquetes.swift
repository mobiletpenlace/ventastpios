//
//  InfoConsultaDetallePaquetes.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 17/05/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit

import ObjectMapper

class InfoConsultaDetallePaquetes: NSObject, Mappable {
    required init?(map: Map) {

    }

    func mapping(map: Map) {
        adicionales <- map["adicionales"]
        planes <- map["planes"]
        estimuloFiscal <- map["estimuloFiscal"]
    }

    override init() {
    }

    var adicionales: Adicionales?
    var planes: Plans?
    var estimuloFiscal: Bool?

}
