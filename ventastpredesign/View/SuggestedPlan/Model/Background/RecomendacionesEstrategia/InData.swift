//
//  InData.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 14/05/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class InData: NSObject, Mappable {
    required init?(map: Map) {
    }

    func mapping(map: Map) {
        canalFront <- map["canalFront"]
        contextObjName <- map["contextObjName"]
        estimuloFiscal <- map["estimuloFiscal"]
        esPostVenta <- map["esPostVenta"]
        maxRecomm <- map["maxRecomm"]
        nombreCanal <- map["nombreCanal"]
        plaza <- map["plaza"]
        contextObjId <- map["contextObjId"]
    }

    override init() {
    }

    var canalFront: String?
    var contextObjName: String?
    var estimuloFiscal: Bool?
    var esPostVenta: Bool?
    var maxRecomm: String?
    var nombreCanal: String?
    var plaza: String?
    var contextObjId: String?
}
