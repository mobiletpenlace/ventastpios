//
//  RecommendationsRequest.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 14/05/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class RecommendationsRequest: BaseRequest {

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        accion <- map["accion"]
        inData <- map["inData"]
    }

    var accion: String?
    var inData: InData?

}
