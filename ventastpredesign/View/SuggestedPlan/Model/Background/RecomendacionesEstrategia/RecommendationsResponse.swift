//
//  RecommendationsResponse.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 14/05/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class RecommendationsResponse: BaseResponse {

    public required convenience init?(map: Map) {
        self.init()
    }

    override func mapping(map: Map) {
        responsetDescription <- map["responsetDescription"]
        response <- map["response"]
        infoSalidaRecomendaciones <- map["infoSalida"]
    }

    var infoSalidaRecomendaciones: infoSalidaRecomendaciones?

}
