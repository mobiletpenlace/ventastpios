//
//  infoSalidaRecomendaciones.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 14/05/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class infoSalidaRecomendaciones: NSObject, Mappable {
    required init?(map: Map) {

    }

    func mapping(map: Map) {
        serviciosStreaming <- map["serviciosStreaming"]
        recomendaciones <- map["recomendaciones"]
        idObjContext <- map["idObjContext"]
    }

    override init() {
    }

    var serviciosStreaming: [String] = []
    var recomendaciones: [Recomendaciones] = []
    var idObjContext: String?

}
