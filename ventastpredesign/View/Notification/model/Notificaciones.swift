//
//  Notificaciones.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 27/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import UIKit

import RealmSwift

class Notificaciones: Object {

    override static func primaryKey() -> String? {
        return "uui"
    }

    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  Titulo: String?
    @objc dynamic var  Mensaje: String?
    @objc dynamic var  Fecha: String?

}
