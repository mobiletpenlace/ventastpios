//
//  NotificationsCollectionReusableView.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 28/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class NotificationsCollectionReusableView: UICollectionReusableView {

    @IBOutlet weak var mFechaNoti: UILabel!
    @IBOutlet weak var mTituloNotifi: UILabel!
    @IBOutlet weak var mMensajeNotifi: UILabel!
    @IBOutlet weak var mButtonShow: UIButton!
    @IBOutlet weak var mButtonDelete: UIButton!
    @IBOutlet weak var mViewDelete: UIView!
    @IBOutlet weak var mViewNotificacion: UIView!
    @IBOutlet weak var ConstraintRightViewNotificacion: NSLayoutConstraint!

    @IBOutlet weak var ConstraintWidthViewDelete: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        Constants.Border(view: mViewNotificacion, radius: 6, width: 1)
        Constants.Border(view: mViewDelete, radius: 6, width: 1)
        Constants.BorderCustom(view: mViewDelete, radius: 6, width: 0, masked: [.layerMaxXMinYCorner, .layerMaxXMaxYCorner])

        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeLeft.direction = .left
        self.addGestureRecognizer(swipeLeft)

        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeRight.direction = .right
        self.addGestureRecognizer(swipeRight)
    }

    @objc func handleGesture(gesture: UISwipeGestureRecognizer) {

        if gesture.direction == UISwipeGestureRecognizerDirection.right {

            UIView.animate(withDuration: 0.2) {
                self.ConstraintRightViewNotificacion.constant = -15
                self.ConstraintWidthViewDelete.constant = 0
                self.layoutIfNeeded()
            }

        } else if gesture.direction == UISwipeGestureRecognizerDirection.left {
            mViewDelete.isHidden = false
            mButtonDelete.isEnabled = true
            UIView.animate(withDuration: 0.2) {
                self.ConstraintRightViewNotificacion.constant = -70
                self.ConstraintWidthViewDelete.constant = 65
                self.layoutIfNeeded()
            }
        }

    }

}
