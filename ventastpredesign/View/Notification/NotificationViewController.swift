//
//  NotificationViewController.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 28/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import RealmSwift
import Amplitude

class NotificationViewController: BaseVentasView {

    @IBOutlet weak var ScrollView: UIScrollView!
    let realm = try! Realm()
    var arrTitulo: [String] = []
    var arrMensaje: [String] = []
    var arrFecha: [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)

            loadData()
    }

    override func viewDidAppear(_ animated: Bool) {
        Amplitude.instance().logEvent("Show_NotificationsScreen")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func ActionBack(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }

    func loadData() {
        var contador = 0
        if RealManager.findFirst(object: Notificaciones.self) != nil {
            let enviada = realm.objects(Notificaciones.self)
            contador =  enviada.count

            ScrollView.contentSize = CGSize(width: 100, height: CGFloat(135) * CGFloat(contador))

            for index in 0 ..< contador {
                if let view = Bundle.main.loadNibNamed("NotificationsCollectionReusableView", owner: self, options: nil)?.first as? NotificationsCollectionReusableView {
                    view.mTituloNotifi.text = enviada[index].Titulo
                    view.mMensajeNotifi.text = enviada[index].Mensaje
                    view.mFechaNoti.text = enviada[index].Fecha
                    arrTitulo.append(enviada[index].Titulo!)
                    arrMensaje.append(enviada[index].Mensaje!)
                    arrFecha.append(enviada[index].Fecha!)
                    view.mButtonDelete.tag = index
                    view.mButtonDelete.addTarget(self, action: #selector(NotificationViewController.DeleteNotificacion), for: UIControlEvents.touchUpInside)
                    view.mButtonDelete.isEnabled = true
                    view.mButtonShow.tag = index
                    view.mButtonShow.addTarget(self, action: #selector(NotificationViewController.MostrarNotificacion), for: UIControlEvents.touchUpInside)
                    view.mButtonShow.isEnabled = true
                    view.mButtonShow.isHidden = false
                    ScrollView.addSubview(view)
                    view.frame.size.height = CGFloat(115)
                    view.frame.size.width = self.view.bounds.size.width
                    view.frame.origin.y = CGFloat(index) * CGFloat(135)
                }
            }
        }
    }

    @objc func DeleteNotificacion(sender: UIButton) {
        print(sender.tag)
        let env = realm.objects(Notificaciones.self)
        if env.indices.contains(sender.tag) {
            let res = env[sender.tag]
            try! realm.write {
                realm.delete(res)
            }
        }
        delete()
        loadData()

    }

    @objc func MostrarNotificacion(sender: UIButton) {
        Constants.Alert(title: arrTitulo[sender.tag], body: arrMensaje[sender.tag], type: type.Normal, viewC: self)

    }

    func delete() {
        let subViews = self.ScrollView.subviews
        for subview in subViews {
            subview.removeFromSuperview()
        }
        ScrollView.contentOffset.y = 0
    }

    @IBAction func DeleteAll(_ sender: Any) {
        let alert = UIAlertController(title: "¿Estas seguro de eliminar todas las notificaciones?", message: "", preferredStyle: .alert)

        let action1 = UIAlertAction(title: "Aceptar", style: .default, handler: { (_) -> Void in
            if  RealManager.findFirst(object: Notificaciones.self) != nil {
                RealManager.deleteAll(object: Notificaciones.self)
            }
            self.delete()
            self.loadData()
                  })

        let action2 = UIAlertAction(title: "Cancelar", style: .default, handler: { (_) -> Void in
             alert.dismiss(animated: true, completion: nil)

        })

        alert.addAction(action1)
        alert.addAction(action2)

        present(alert, animated: true, completion: {
            alert.view.superview?.isUserInteractionEnabled = true
        alert.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertControllerBackgroundTapped)))
        })

    }

    @objc func alertControllerBackgroundTapped() {
        self.dismiss(animated: true, completion: nil)
    }
}
