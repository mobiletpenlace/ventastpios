//
//  VideoCollectionViewCell.swift
//  ventastp
//
//  Created by Branchbit on 10/11/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import EMSpinnerButton

class VideoCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var ThumbnailImageView: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var buttonPlay: UIButton!
    @IBOutlet weak var downloadButton: EMSpinnerButton!
    @IBOutlet weak var bottomStack: UIStackView!

    var test: UIButton!

}
