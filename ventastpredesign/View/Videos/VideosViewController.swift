//  VideosViewController.swift
//  ventastpredesign
//  Created by Marisol Huerta Ortega on 20/03/18.
//  Copyright © 2018 TotalPlay. All rights reserved.

import UIKit

import AVKit
import Photos
// import Crashlytics
import RealmSwift
import Cache
import EMSpinnerButton
// import Answers

class VideosViewController: BaseVentasView {

    deinit {
        print("DEINIT VIDEOSVIEWCONTROLLER")
    }

    var mNameVideo: [String] = []
    var mDescriptionVideo: [String] = []
    private var player: AVPlayer = AVPlayer()
    var mVideoResponses: [VideosResponse] = []

    // Aux
    var download = true
    let auxTag = 10

    // Cache Disk config
    let diskConfig = DiskConfig(name: "Videos")

    lazy var storage: Cache.DiskStorage<String, Data>? = {
        return try? Cache.DiskStorage(config: diskConfig, fileManager: .default, transformer: TransformerFactory.forData())
    }()

     // let realm = try! Realm()

//    @IBOutlet weak var ScrollView: UIScrollView!
    @IBOutlet weak var ScrollView: UIScrollView!
    @IBOutlet weak var topView: UIView!

    var stackView = UIStackView()

    var mVideosPresenter: VideosPresenter!

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)

        // Initialize network monitoring (could be initialized in AppDelegate)
        NetworkMonitor.shared.startMonitoring()
        mVideosPresenter.GetVideos(mVideosDelegate: self as VideosDelegate)

    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Debug
        print("Entró al viewWillDisappear")
        // Debug
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func getPresenter() -> BasePresenter? {
        mVideosPresenter = VideosPresenter(view: self)
        return mVideosPresenter
    }

//    @IBAction func ActionBack(_ sender: Any) {
//        Constants.Back(viewC: self)
//    }
    @IBAction func backAction(_ sender: Any) {
        dismiss(animated: false, completion: nil)
        player.replaceCurrentItem(with: nil)
    }

    @objc func ActionPlay(sender: UIButton) {

        if let urlstring = mVideoResponses[sender.tag].video!.url {

            let mime = mVideoResponses[sender.tag].video?.mime
            let ext = mVideoResponses[sender.tag].video?.ext?.replacingOccurrences(of: ".", with: "")

            let playerItem: CachingPlayerItem

            do {
                let result = try storage!.entry(forKey: urlstring)
                // Video is cached.
                playerItem = CachingPlayerItem(data: result.object, mimeType: mime ?? "video/mp4", fileExtension: ext ?? "mp4")

            } catch {
                // Video is not cached.
                if NetworkMonitor.shared.connectionType == .wifi {
                    print("CONECTADO A WIFI")
                } else {
                    self.alertMessage(message: "Para visualizar conectate a una red WiFi", title: "Alerta")
                    return
                }
                // DEBUG
                print("pasó despues del alert")
                // DEBUG
                playerItem = CachingPlayerItem(url: URL(string: urlstring)!)

            }

            playerItem.delegate = self
            player.replaceCurrentItem(with: playerItem)
            self.player.automaticallyWaitsToMinimizeStalling = false

            let playerViewController = AVPlayerViewController()
            playerViewController.player = self.player

            self.present(playerViewController, animated: true, completion: {
                super.hideLoading()
                playerViewController.player!.play()
            })

        } else {
            print("Error al extraer la url")
        }

    }

}

// CACHING PLAYER ITEM DELEGATE
extension VideosViewController: CachingPlayerItemDelegate {

    func playerItem(_ playerItem: CachingPlayerItem, didFinishDownloadingData data: Data) {
        print("File is downloaded and ready for storing")

        // Store downloaded data and update downloadedstate
        do {
            let aux = try storage?.existsObject(forKey: playerItem.url.absoluteString)
            if !aux! {
                try storage?.setObject(data, forKey: playerItem.url.absoluteString, expiry: .never)
                DispatchQueue.main.sync {
                    self.refreshDownloadedState()
                }
            }
        } catch {
            print("No pudo guardarlo")
        }

    }

    func playerItem(_ playerItem: CachingPlayerItem, didDownloadBytesSoFar bytesDownloaded: Int, outOf bytesExpected: Int) {
        print("\(bytesDownloaded)bytes/\(bytesExpected)bytes")

    }

    func playerItemPlaybackStalled(_ playerItem: CachingPlayerItem) {
        print("Not enough data for playback. Probably because of the poor network. Wait a bit and try to play later.")
    }

    func playerItem(_ playerItem: CachingPlayerItem, downloadingFailedWith error: Error) {
        print("PRUEBA")
        print(error)
    }

}
