//
//  VideosPresenter.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 16/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import UIKit

import ObjectMapper
import Alamofire
import SwiftyJSON

protocol VideosDelegate: NSObjectProtocol {
    func OnSuccessVideos(mVideos: [VideosResponse])
}
class VideosPresenter: BaseVentastpredesignPresenter {
    weak var mVideosDelegate: VideosDelegate!
    var request: Alamofire.Request?

    override func onRequestWs() {}

    func GetVideos(mVideosDelegate: VideosDelegate) {
        self.mVideosDelegate = mVideosDelegate
        view.showLoading(message: "")
        if ConnectionUtils.isConnectedToNetwork() {

            // Connection success fetching data
            Alamofire.request(ApiDefinition.API_VIDEOS, method: .get).validate().responseJSON { [weak self] response in
                guard let self = self else {return}
                switch response.result {
                case .success(let value):
                    let json = JSON(value)

                    var videoResponseArray: [VideosResponse] = []

                    if json.count > 0 {
                        for i in 0...json.count-1 {
                            if let response = VideosResponse(JSONString: json[i].rawString() ?? "") {
                                // Validate isVisible field
                                if response.isVisible != false {
                                    videoResponseArray.append(response)
                                }
                            }
                        }

                        self.OnSuccessVideos(videosResponse: videoResponseArray)
                    }

                case .failure(let error):
                    print(error)
                }
            }
        } else {
            self.onErrorConnection()
        }
    }

    func GetFlyers() {
        view.showLoading(message: "")
        if ConnectionUtils.isConnectedToNetwork() {

            // Connection success fetching data
            Alamofire.request(ApiDefinition.api, method: .post).validate().responseJSON { [weak self] response in
                guard let self = self else {return}
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    var flyersResponseArray: [GetFlyersResponse] = []
                    if json.count > 0 {
                        for i in 0...json.count-1 {
                            if let response = GetFlyersResponse(JSONString: json[i].rawString() ?? "") {
                                // Validate isVisible field
                                    flyersResponseArray.append(response)
                            }
                        }

                        self.OnSuccessFlyers(flyersResponse: flyersResponseArray)
                    }

                case .failure(let error):
                    print(error)
                }
            }
        } else {
            self.onErrorConnection()
        }
    }

    func OnSuccessVideos(videosResponse: [VideosResponse]) {

        if videosResponse.count > 0 {
            mVideosDelegate.OnSuccessVideos(mVideos: videosResponse)
        } else {
            view.hideLoading()
            Constants.Alert(title: "", body: dialogs.Error.InternService, type: type.Error, viewC: mViewController)
        }
    }

    func OnSuccessFlyers(flyersResponse: [GetFlyersResponse]) {
            view.hideLoading()
    }
}
