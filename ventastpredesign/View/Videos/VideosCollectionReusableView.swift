//
//  VideosCollectionReusableView.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 20/03/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class VideosCollectionReusableView: UICollectionReusableView {

    @IBOutlet weak var TitleVideo: UILabel!
    @IBOutlet weak var ButtonPlay: UIButton!
    @IBOutlet weak var ButtonDownload: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var bottomView: UIView!

}
