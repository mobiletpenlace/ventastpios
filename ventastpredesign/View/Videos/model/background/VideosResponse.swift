//
//  VideosResponse.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 16/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class VideosResponse: BaseResponse {
    required init?(map: Map) {
    }

    override func mapping(map: Map) {

        id <- map["id"]
        titulo <- map["Titulo"]
        video <- map["video"]
        descripcion <- map["descripcion"]
        ordenAparicion <- map["ordenAparicion"]
        negocio <- map["Negocio"]
        isVisible <- map["isVisible"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
        video <- map["video.0"]
        miniatura <- map["miniatura.0"]

    }

    var id: Int?
    var titulo: String?
    var descripcion: String?
    var ordenAparicion: Int?
    var negocio: String?
    var isVisible: Bool?
    var created_at: String?
    var updated_at: String?
    var video: Video?
    var miniatura: Miniatura?

}
