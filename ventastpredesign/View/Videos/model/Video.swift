//
//  Video.swift
//  ventastp
//
//  Created by Branchbit on 08/11/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class Video: NSObject, Mappable {
    required init?(map: Map) {

    }

    func mapping(map: Map) {

        id <- map ["id"]
        name <- map["name"]
        alternativeText <- map["alternativeText"]
        caption <- map["caption"]
        width <- map["width"]
        height <- map["height"]
        formats <- map["formats"]
        hashVideo <- map["hash"]
        ext <- map["ext"]
        mime <- map ["mime"]
        size <- map["size"]
        url <- map["url"]
        previewUrl <- map["previewUrl"]
        provider <- map["provider"]
        provider_metadata <- map["provider_metadata"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
    }

    override init() {

    }

    var id: Int?
    var name: String?
    var alternativeText: String?
    var caption: String?
    var width: String?
    var height: String?
    var formats: String?
    var hashVideo: String?
    var ext: String?
    var mime: String?
    var size: Int?
    var url: String?
    var previewUrl: String?
    var provider: String?
    var provider_metadata: String?
    var created_at: String?
    var updated_at: String?

}
