//
//  VideosViewDelegate.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 27/03/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage
import EMSpinnerButton
import AVFoundation
import AVKit

extension VideosViewController: VideosDelegate {

    func OnSuccessVideos(mVideos: [VideosResponse]) {

        // TOP VIEW CONFIG
        topView.backgroundColor = .white.withAlphaComponent(0.5)
        let blurEffect = UIBlurEffect(style: .light)
        let blurView = UIVisualEffectView(effect: blurEffect)
        blurView.frame = topView.bounds

        topView.insertSubview(blurView, at: 0)

        let contador = mVideos.count
        // Sort Videos
//        mVideoResponses = mVideos
        let sortedVideos = mVideos.sorted(by: { $0.ordenAparicion ?? 0 > $1.ordenAparicion ?? 0})
        mVideoResponses = sortedVideos

        let heighSize = self.view.safeAreaLayoutGuide.layoutFrame.height/2.5

        ScrollView.contentSize = CGSize(width: .zero, height: heighSize * CGFloat(contador))

        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        // stackView.spacing = 5

        ScrollView.contentInset = UIEdgeInsets(top: 44, left: 0, bottom: 10, right: 0)
        ScrollView.contentOffset = CGPoint(x: 0, y: -44)

        stackView.size = CGSize(width: self.view.bounds.size.width, height: heighSize * CGFloat(contador))
        ScrollView.addSubview(stackView)

        for index in  0 ..< contador {
            // Constraint conflicts solved by stackview autoresize
            if let view = Bundle.main.loadNibNamed("VideoCollectionViewCell", owner: self, options: nil)?.first as? VideoCollectionViewCell {
                view.labelTitle.text = mVideoResponses[index].titulo
                view.labelDescription.text = mVideoResponses[index].descripcion ?? "El video no tiene descripción"
                let thumbnailURL = mVideoResponses[index].miniatura?.url ?? "urlinexistente"
                view.ThumbnailImageView.sd_setImage(with: URL(string: thumbnailURL), placeholderImage: UIImage(named: "ImageVideo"))
                view.buttonPlay.tag = index
                view.buttonPlay.addTarget(self, action: #selector(VideosViewController.ActionPlay), for: UIControlEvents.touchUpInside)

                // EMSpinner BUTTON
                view.downloadButton.title = ""
                view.downloadButton.spinnerColor = Colors.DarkBlue.cgColor
                view.downloadButton.backgroundColor = .white
                view.downloadButton.addTarget(self, action: #selector(buttonAction(_:)), for: .touchUpInside)
                view.downloadButton.tag = index+auxTag
                // EMSpinner BUTTON

                if checkDownloaded(index: index) {
                    view.downloadButton.setImage(UIImage(named: "Image_rede_blue_check"), for: .normal)
                    view.downloadButton.isUserInteractionEnabled = false
                } else {
                    // Nothing
                }

                view.layer.cornerRadius = 5

                stackView.addArrangedSubview(view)

            }
        }

      super.hideLoading()
    }

    @IBAction func buttonAction(_ button: EMSpinnerButton) {
        if NetworkMonitor.shared.connectionType == .wifi {
            // Debug
            print("CONECTADO A WIFI")
            // Debug
        } else {
            self.alertMessage(message: "Para descargar conectate a una red WiFi", title: "Alerta")
            button.animate(animation: .shake)
            return
        }
        button.animate(animation: .collapse)
        button.imageView?.isHidden = true

            let qualityOfServiceClass = DispatchQoS.QoSClass.background
            let backgroundQueue = DispatchQueue.global(qos: qualityOfServiceClass)
        guard let urlstringprueba = self.mVideoResponses[button.tag-self.auxTag].video!.url else {
            return
        }
            backgroundQueue.async(execute: {

                let playerItem: CachingPlayerItem

                do {
                    _ = try self.storage!.entry(forKey: urlstringprueba)
                    // Video is cached.
                    // Should not enter here, Check button interaction
                    print("Should not enter here")
                    return

                } catch {
                    // Video is not cached.
                    // Download and save

                    playerItem = CachingPlayerItem(url: URL(string: urlstringprueba)!)
                    playerItem.delegate = self
                    let player = AVPlayer(playerItem: playerItem)
                    let playerlayer = AVPlayerLayer()
                    playerlayer.player = player
                    player.isMuted = false
                    // Download playerItem (Needs to be instantiated
                    playerItem.download()
                }

            })
        }

    func checkDownloaded(index: Int) -> Bool {
        let urlstring = mVideoResponses[index].video?.url ?? "fakeurl"
        do {
            let aux = try storage?.existsObject(forKey: urlstring)
            if aux! {
                return true
            } else {
                return false
            }
        } catch {
            // Nothing
            fatalError("Disk problem")
        }
    }

    func refreshDownloadedState() {
        var contador = 0
        for item in stackView.subviews {
            let urlstring = mVideoResponses[contador].video?.url ?? "fakeurl"
            let button = item.viewWithTag(contador+auxTag) as! EMSpinnerButton
            print("URLPRUEBA: \(urlstring)")
            do {
                _ = try storage!.entry(forKey: urlstring)
                if button.isEnabled {
                    button.animate(animation: .expand)
                    button.setImage(UIImage(named: "Image_rede_blue_check"), for: .normal)
                    button.imageView?.isHidden = false
                    button.isEnabled = false
                }

            } catch {
                // Nothing
            }
            contador += 1
        }
    }

}
