//
//  ComentariosViewCell.swift
//  SalesCloud
//
//  Created by mhuertao on 01/08/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

class ComentariosViewCell: UITableViewCell {

    @IBOutlet weak var fechaComentario: UILabel!
    @IBOutlet weak var nombreUsuario: UILabel!
    @IBOutlet weak var comentarioText: UILabel!
    var heightCell: CGFloat = 163

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
