//
//  DetalleOportunidadView.swift
//  SalesCloud
//
//  Created by mhuertao on 01/08/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

class DetalleOportunidadView: BaseVentasView {

    @IBOutlet weak var comentarioTabla: UITableView!
    @IBOutlet weak var numCuentaLabel: UILabel!
    @IBOutlet weak var fechaCreacionLabel: UILabel!
    @IBOutlet weak var faseVenta: UILabel!
    @IBOutlet weak var nombreLabel: UILabel!
    var ventaViewModel: BandejaVentaViewModel!
    var idOPP: String = ""
    var opportunity = paternidad()
    var commentarios: [Comentarios] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        ventaViewModel = BandejaVentaViewModel(view: self)
        ventaViewModel.atachView(observer: self)
        setUpView()
    }

    func setUpView() {
        clearText()
        consultComentarioService()
        setDataOpportunity()
    }

    func clearText() {
        numCuentaLabel.text = ""
        fechaCreacionLabel.text = ""
        faseVenta.text = ""
        nombreLabel.text = ""
    }

    func setDataOpportunity() {
        numCuentaLabel.text = opportunity.numeroCuenta
        fechaCreacionLabel.text = ventaViewModel.getDateCreation(date: opportunity.fecha)
        faseVenta.text = opportunity.etapa
        nombreLabel.text = opportunity.nombre
    }

    func  consultComentarioService() {
        ventaViewModel.consultComentarios(idOpp: idOPP)
    }

    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: false)
    }

}

extension DetalleOportunidadView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commentarios.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ComentariosViewCell", for: indexPath) as! ComentariosViewCell
        cell.comentarioText.text = commentarios[indexPath.row].texto
        cell.fechaComentario.text = commentarios[indexPath.row].fechaCreacion
        cell.nombreUsuario.text = commentarios[indexPath.row].creadoPor
        cell.comentarioText.sizeToFit()
        cell.comentarioText.layoutIfNeeded()
        return cell
    }

}

extension DetalleOportunidadView: ventaDigitalObserver {
    func successKeyPaternity() {}

    func successComentarios(lista: [Comentarios]) {
        commentarios = lista
        comentarioTabla.reloadData()
    }

    func succesPaternity(paternidad: [paternidad]) {}

    func succesOportunidad(opp: [Oportunidad]) {}

    func succesToken() {}

    func onError(errorMessage: String) {
        Constants.Alert(title: "", body: errorMessage, type: type.Error, viewC: self)
    }

}
