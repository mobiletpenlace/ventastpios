//
//  BandejaVentaView.swift
//  SalesCloud
//
//  Created by mhuertao on 13/08/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

class BandejaVentaView: BaseVentasView {
    @IBOutlet weak var ventaDigitalTable: UITableView!
    @IBOutlet weak var filtroVentasTextfield: UITextField!
    @IBOutlet weak var emptyLabel: UILabel! {
        didSet {
            emptyLabel.text = "No hay datos para mostrar"
        }
    }
    var ventaViewModel: BandejaVentaViewModel!
    var isCurrentCount = true
    var isCurrentNumber = true
    var paternity: [paternidad] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        ventaViewModel = BandejaVentaViewModel(view: self)
        ventaViewModel.atachView(observer: self)
        setUpView()
    }

    func setUpView() {
        consultTokenService()
    }

    func consultKeyPaternity() {
        ventaViewModel.consultKeyPaternity()
    }

    func  consultTokenService() {
        ventaViewModel.consultTokenService()
    }

    func consultOportunidades() {
        ventaViewModel.consultOportunidades()
    }

    func consultPaternidades() {
        ventaViewModel.consultListPaternity()
    }

    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: false)
    }

    @IBAction func openFilter(_ sender: Any) {
        let viewAlert: UIStoryboard = UIStoryboard(name: "DialogVentasDigital", bundle: nil)
        let controller = viewAlert.instantiateViewController(withIdentifier: "DialogVentasDigital")as! DialogVentasDigital
        controller.filterDigitalDelegate = self
        controller.isNumber = isCurrentNumber
        controller.isCount = isCurrentCount
        controller.providesPresentationContextTransitionStyle = true
        controller.definesPresentationContext = true
        controller.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(controller, animated: false, completion: {})
    }

    func goDetalleOportunidad(opp: paternidad) {
        let viewAlert: UIStoryboard = UIStoryboard(name: "DetalleOportunidadView", bundle: nil)
        let controller = viewAlert.instantiateViewController(withIdentifier: "DetalleOportunidadView")as! DetalleOportunidadView
        controller.idOPP = opp.id
        controller.opportunity = opp
        controller.providesPresentationContextTransitionStyle = true
        controller.definesPresentationContext = true
        controller.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(controller, animated: false, completion: {})
    }
}

extension BandejaVentaView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if paternity.count == 0 {
            emptyLabel.isHidden = false
        } else {
            emptyLabel.isHidden = true
        }
        return paternity.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if  !paternity[indexPath.row].isValidacionNumero {
            return 130
        } else {
            return 215
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if !paternity[indexPath.row].isValidacionNumero {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PendienteViewCell", for: indexPath) as! PendienteViewCell
            cell.numTelefonoLabel.text = paternity[indexPath.row].numcel
            cell.paternidadLabel.text =  paternity[indexPath.row].estatus
            cell.buttonWasp.tag = indexPath.row
            cell.buttonWasp.addTarget(self, action: #selector(openWhatsButtonTapped(sender: )), for: .touchUpInside)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "GanadaViewCell", for: indexPath) as! GanadaViewCell
            cell.nombreLabel.text = paternity[indexPath.row].nombre
            cell.numTelefonoLabel.text = paternity[indexPath.row].numcel
            cell.numCuentaLabel.text = paternity[indexPath.row].numeroCuenta
            cell.paternidadLabel.text = paternity[indexPath.row].estatus
            cell.buttonWasp.tag = indexPath.row
            cell.buttonWasp.addTarget(self, action: #selector(openWhatsButtonTapped(sender: )), for: .touchUpInside)
            cell.buttonDetalle.tag = indexPath.row
            cell.buttonDetalle.addTarget(self, action: #selector(openDetalleButtonTapped(sender: )), for: .touchUpInside)
            return cell
        }
    }

    @objc func openWhatsButtonTapped(sender: UIButton) {
        let cel = paternity[sender.tag].numcel
        SendMessage.whatsp(withPhoneNumber: cel)
    }
    @objc func openDetalleButtonTapped(sender: UIButton) {
        goDetalleOportunidad(opp: paternity[sender.tag])
    }

}

extension BandejaVentaView: filterDigitalDelegate {
    func getFilter(isNumber: Bool, isCount: Bool) {
        let resultFilter = ventaViewModel.convertFilterToText(isCount: isCount, isNumber: isNumber)
        filtroVentasTextfield.text = resultFilter.0
        isCurrentCount = isCount
        isCurrentNumber = isNumber
        self.paternity = resultFilter.1
        ventaDigitalTable.reloadData()
    }
}

extension BandejaVentaView: ventaDigitalObserver {
    func successComentarios(lista: [Comentarios]) {}

    func succesOportunidad(opp: [Oportunidad]) {
        self.consultPaternidades()
    }

    func succesPaternity(paternidad: [paternidad]) {
        self.paternity = paternidad
        ventaDigitalTable.reloadData()
    }

    func succesToken() {
        self.consultKeyPaternity()
    }

    func successKeyPaternity() {
        self.consultOportunidades()
    }

    func onError(errorMessage: String) {
        Constants.Alert(title: "", body: errorMessage, type: type.Error, viewC: self)
    }
}
