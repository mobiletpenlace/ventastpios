//
//  DialogVentasDigital.swift
//  SalesCloud
//
//  Created by mhuertao on 01/08/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit
protocol filterDigitalDelegate {
    func getFilter(isNumber: Bool, isCount: Bool)
}
class DialogVentasDigital: UIViewController {

    @IBOutlet weak var imageCheckTelefono: UIImageView!
    @IBOutlet weak var imageCheckCuenta: UIImageView!
    var filterDigitalDelegate: filterDigitalDelegate?
    var isNumber = true
    var isCount = true

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    func setUpView() {
        validarNum()
        validarCuenta()
    }

    @IBAction func validarNumButton(_ sender: Any) {
        isNumber = !isNumber
        validarNum()
    }

    @IBAction func validarCuentaButton(_ sender: Any) {
        isCount = !isCount
        validarCuenta()

    }

    @IBAction func buttonAccept(_ sender: Any) {
        if isNumber == false && isCount == false {
            Constants.Alert(title: "AVISO", body: "Favor de elegir por lo menos un filtro", type: type.Warning, viewC: self)
        } else {
            filterDigitalDelegate?.getFilter(isNumber: isNumber, isCount: isCount)
            self.dismiss(animated: false)
        }

    }

    func validarNum() {
        if !isNumber {
            imageCheckTelefono.image = UIImage.init(named: "icon_uncheck_addons")
        } else {
            if #available(iOS 13.0, *) {
                imageCheckTelefono.image = UIImage(systemName: "checkmark.rectangle.fill")
            } else {
                // Fallback on earlier versions
            }
        }
    }

    func validarCuenta() {
        if !isCount {
            imageCheckCuenta.image = UIImage.init(named: "icon_uncheck_addons")
        } else {
            if #available(iOS 13.0, *) {
                imageCheckCuenta.image = UIImage(systemName: "checkmark.rectangle.fill")
            } else {
                // Fallback on earlier versions
            }
        }
    }

}
