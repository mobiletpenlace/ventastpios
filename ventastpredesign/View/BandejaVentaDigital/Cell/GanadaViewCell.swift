//
//  GanadaViewCell.swift
//  SalesCloud
//
//  Created by mhuertao on 01/08/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

class GanadaViewCell: UITableViewCell {
    @IBOutlet weak var paternidadLabel: UILabel!
    @IBOutlet weak var numTelefonoLabel: UILabel!
    @IBOutlet weak var numCuentaLabel: UILabel!
    @IBOutlet weak var nombreLabel: UILabel!
    @IBOutlet weak var buttonWasp: UIButton!
    @IBOutlet weak var buttonDetalle: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
