//
//  PendienteViewCell.swift
//  SalesCloud
//
//  Created by mhuertao on 01/08/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

class PendienteViewCell: UITableViewCell {
    @IBOutlet weak var paternidadLabel: UILabel!
    @IBOutlet weak var numTelefonoLabel: UILabel!
    @IBOutlet weak var buttonWasp: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
