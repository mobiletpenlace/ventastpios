//
//  paternidadResponse.swift
//  SalesCloud
//
//  Created by mhuertao on 08/08/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class paternidadResponse: BaseResponse {
    var resultPaternidad: String = ""
    var paternidades: [paternidad] = []

    required init?(map: Map) {}

    override func mapping(map: Map) {
        paternidades <- map["paternidades"]
        resultPaternidad <- map["result"]
    }
}

class paternidad: NSObject, Mappable {
    var numEmpleado: String = ""
    var descripcion: String = ""
    var estatus: String = ""
    var aux: String = ""
    var fechaActivacion: String = ""
    var canal: String = ""
    var numcel: String = ""

    var numeroOpp: String = ""
    var numeroCuenta: String = ""
    var nombre: String = ""
    var id: String = ""
    var fecha: String = ""
    var etapa: String = ""
    var celular: String = ""
    var canalVenta: String = ""
    var isValidacionNumero: Bool = false

    required init?(map: Map) {}

    func mapping(map: Map) {
        numEmpleado <- map["numEmpleado"]
        descripcion <- map["descripcion"]
        estatus <- map["estatus"]
        aux <- map["aux"]
        fechaActivacion <- map["fechaActivacion"]
        canal <- map["canal"]
        numcel <- map["numcel"]
    }

    override init() {}

}
