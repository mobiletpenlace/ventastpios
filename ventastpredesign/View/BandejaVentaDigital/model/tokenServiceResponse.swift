//
//  tokenServiceResponse.swift
//  SalesCloud
//
//  Created by mhuertao on 16/08/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class tokenServiceResponse: BaseResponse {
    var access_token: String = ""
    var organization_name: String = ""
    var token_type: String = ""
    var issued_at: String = ""
    var client_id: String = ""

    required init?(map: Map) {

    }

    override func mapping(map: Map) {
        access_token <- map["access_token"]
        organization_name <- map["organization_name"]
        token_type <- map["token_type"]
        issued_at <- map["issued_at"]
        client_id <- map["client_id"]
    }

}
