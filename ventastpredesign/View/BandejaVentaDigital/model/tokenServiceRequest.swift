//
//  tokenServiceRequest.swift
//  SalesCloud
//
//  Created by mhuertao on 16/08/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

public class tokenServiceRequest: BaseRequest {
    var password: String = ""
    var username: String = ""
    var grant_type: String = ""

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override public func mapping(map: Map) {
        grant_type <- map["grant_type"]
        password <- map["password"]
        username <- map["username"]
    }
}
