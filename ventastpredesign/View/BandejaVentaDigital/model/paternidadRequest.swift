//
//  paternidadRequest.swift
//  SalesCloud
//
//  Created by mhuertao on 08/08/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

public class paternidadRequest: BaseRequest {
    var fechaFin: String = ""
    var numEmpleado: String = ""
    var fechaInicio: String = ""

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override public func mapping(map: Map) {
        numEmpleado <- map["numEmpleado"]
        fechaFin <- map["fechaFin"]
        fechaInicio <- map["fechaInicio"]
    }
}
