//
//  BandejaVentaModel.swift
//  SalesCloud
//
//  Created by mhuertao on 15/08/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import Alamofire
import SwiftyJSON

protocol ventaDigitalModelObserver {
    func succesOportunidad(opp: [Oportunidad])
    func successComentario(lista: [Comentarios])
    func successKeyPaternity(resultKey: resultadoLLave)
    func succesPaternity(paternidad: [paternidad])
    func succesToken()
    func onError(errorMessage: String)
}

class BandejaVentaModel: BaseModel {
    var server: ServerDataManager2<paternidadResponse>?
    var server2: ServerDataManager2<CommentLeadResponse>?

    var viewModel: ventaDigitalModelObserver?

    init (observer: BaseViewModelObserver) {
        super.init(observerVM: observer)
        server = ServerDataManager2<paternidadResponse>()
        server2 = ServerDataManager2<CommentLeadResponse>()
    }

    func atachModel(viewModel: ventaDigitalModelObserver) {
        self.viewModel = viewModel
    }

    func consultTokenService() {
        if ConnectionUtils.isConnectedToNetwork() {
            print("consulta token")
            Alamofire.request(ApiDefinition.API_TOKEN_SERVICE, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: ApiDefinition.headersTOKENService).responseJSON {[weak self] response in
               guard let self = self else {return}
               switch response.result {
               case .success(let value):
                   let json = JSON(value)
                   print(json)
                   let tokenResponse = tokenServiceResponse(JSONString: json.rawString() ?? "")
                   if tokenResponse?.access_token != "" {
                       statics.TOKEN_SERVICE_API = tokenResponse?.access_token ?? ""
                       self.viewModel?.succesToken()
                   } else {
                       self.viewModel?.onError(errorMessage: "Error al consultar el token api service")
                   }
               case .failure(let error):
                   self.viewModel?.onError(errorMessage: "Error al consultar el token api service")
               }
           }
        } else {
            viewModel?.onError(errorMessage: "Verifiqué su conexión de internet")
        }
    }

    func consultLLavePaternidad() {
        if ConnectionUtils.isConnectedToNetwork() {
            Alamofire.request(ApiDefinition.API_KEY_PATERNITY, method: .get, parameters: nil, encoding: URLEncoding.queryString, headers: ApiDefinition.headersKeyApiService).responseJSON {[weak self] response in
               guard let self = self else {return}
               switch response.result {
               case .success(let value):
                   let json = JSON(value)
                   print(json)
                   let keyPaternityResponse = paternidadKeysResponse(JSONString: json.rawString() ?? "")
                   if keyPaternityResponse?.resultado != nil {
                       self.viewModel?.successKeyPaternity(resultKey: (keyPaternityResponse?.resultado)!)
                   } else {
                       self.viewModel?.onError(errorMessage: "Error al consultar la llave")
                   }
               case .failure(let error):
                   self.viewModel?.onError(errorMessage: "Error al consultar la llave de paternidad")
               }
           }
        } else {
            viewModel?.onError(errorMessage: "Verifiqué su conexión de internet")
        }
    }

    func consultOportunidades() {
        AES128.shared.setKey(key: statics.key)
        let parameters: Parameters = [
            "idEmpleado": getEmploye().Id
        ]
        if ConnectionUtils.isConnectedToNetwork() {
            Alamofire.request(AppDelegate.API_SALESFORCE + ApiDefinition.API_CONSULTA_OPORTUNIDADES, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: ApiDefinition.headersToken).responseJSON {[weak self] response in
               guard let self = self else {return}
               switch response.result {
               case .success(let value):
                   let json = JSON(value)
                   print(json)
                   let oppResponse = OportunidadResponse(JSONString: json.rawString() ?? "")
                   guard let oportunidades = oppResponse?.oportunidades else {return}
                   let oportunidadesJSON: String = AES128.shared.decode(coded: oportunidades) ?? ""
                   guard let oportunidadesObjects: [Oportunidad] = [Oportunidad].init(JSONString: oportunidadesJSON) else {return}
                   self.viewModel?.succesOportunidad(opp: oportunidadesObjects)
               case .failure(let error):
                   print(error)
                   self.viewModel?.onError(errorMessage: "Error al consultar las oportunidades")
               }
           }
        } else {
            viewModel?.onError(errorMessage: "Verifiqué su conexión de internet")
        }
    }

    func consultListPaternidad(request: paternidadRequest) {
        let urlResponse = ApiDefinition.API_PATERNITY
        server?.setValues(requestUrl: urlResponse, delegate: self, headerType: .headersApiService)
        server?.request(requestModel: request)
    }

    func consultCommentary(request: CommentLeadRequest) {
        server2?.setValues(requestUrl: AppDelegate.API_SALESFORCE + ApiDefinition.API_COMMENTS_LEAD, delegate: self, headerType: .headersSalesforce)
        server2?.request(requestModel: request)
    }

    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if requestUrl == ApiDefinition.API_PATERNITY {
            let paternidadResponse = response as! paternidadResponse
            if paternidadResponse.resultPaternidad != "101" {
                viewModel?.succesPaternity(paternidad: paternidadResponse.paternidades)
            } else {
                viewModel?.onError(errorMessage: "Error al consultar las paternidades")
            }
        } else if requestUrl == AppDelegate.API_SALESFORCE + ApiDefinition.API_COMMENTS_LEAD {
            let commentaryResponse = response as! CommentLeadResponse
            if commentaryResponse.result  == "0" {
                viewModel?.successComentario(lista: commentaryResponse.mInfoSalida!.mComentarios)
            } else {
                self.viewModel?.onError(errorMessage: "Registro no encontrado, favor de validar campo idRegistro.")
            }
        }
    }

    override func onErrorLoadResponse(requestUrl: String, messageError: String) {
        viewModel?.onError(errorMessage: messageError)
    }

    func getEmploye() -> Empleado {
        let employee = RealManager.findFirst(object: Empleado.self)
        return employee!
    }
}
