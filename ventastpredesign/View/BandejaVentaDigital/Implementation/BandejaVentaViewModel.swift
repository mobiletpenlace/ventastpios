//
//  BandejaVentaViewModel.swift
//  SalesCloud
//
//  Created by mhuertao on 15/08/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import SwiftyRSA

protocol ventaDigitalObserver {
    func successKeyPaternity()
    func successComentarios(lista: [Comentarios])
    func succesPaternity(paternidad: [paternidad])
    func succesOportunidad(opp: [Oportunidad])
    func succesToken()
    func onError(errorMessage: String)
}

class BandejaVentaViewModel: BaseViewModel, ventaDigitalModelObserver {
    var observer: ventaDigitalObserver!
    var model: BandejaVentaModel!
    var listPaternity: [paternidad] = []
    var listOPP: [Oportunidad] = []
    var arrPaternityClear: [paternidad] = []

    override init(view: BaseVentasView) {
        super.init(view: view)
        model = BandejaVentaModel(observer: self)
        model.atachModel(viewModel: self)
    }

    func atachView(observer: ventaDigitalObserver) {
        self.observer = observer
    }

    func consultTokenService() {
        view.showLoading(message: "")
        configureHeaderTokenSistem()
        model.consultTokenService()
    }

    func consultKeyPaternity() {
        view.showLoading(message: "")
        model.consultLLavePaternidad()
    }

    func consultOportunidades() {
        view.showLoading(message: "")
        model.consultOportunidades()
    }

    func consultComentarios(idOpp: String) {
        view.showLoading(message: "")
        let request = CommentLeadRequest()
        request.idRegistro = idOpp
        request.tipoObjeto = "Oportunidad"
        model.consultCommentary(request: request)
    }

    func consultListPaternity() {
        view.showLoading(message: "")
        let fechaInicioRSA = RSAKeyManager.encrypt(text: getDateStart())
        let fechaFinRSA =  RSAKeyManager.encrypt(text: getDateFinish())
        let numEmpleadoRSA = RSAKeyManager.encrypt(text: getNumEmployee())
        let request = paternidadRequest()
        request.fechaInicio = fechaInicioRSA
        request.fechaFin = fechaFinRSA
        request.numEmpleado = numEmpleadoRSA
        model.consultListPaternidad(request: request)
    }

    func onError(errorMessage: String) {
        Logger.println(errorMessage)
        view.hideLoading()
        observer.onError(errorMessage: errorMessage)
    }

    func succesPaternity(paternidad: [paternidad]) {
        listPaternity = desencryptPaternity(arrPaternidad: paternidad)
        arrPaternityClear = matchOportunityPaternity(listP: listPaternity)
        view.hideLoading()
        observer.succesPaternity(paternidad: arrPaternityClear)
    }

    func succesToken() {
        view.hideLoading()
        observer.succesToken()
    }

    func successKeyPaternity(resultKey: resultadoLLave) {
        statics.ID_ACCESO = resultKey.idAcceso
        statics.ACCESO_PRIVADO = resultKey.accesoPrivado
        statics.ACCESO_PUBLICO =  resultKey.accesoPublico
        view.hideLoading()
        observer.successKeyPaternity()
    }

    func succesOportunidad(opp: [Oportunidad]) {
        view.hideLoading()
        listOPP = opp
        observer.succesOportunidad(opp: opp)
    }

    func successComentario(lista: [Comentarios]) {
        view.hideLoading()
        observer.successComentarios(lista: lista)
    }

    func convertFilterToText(isCount: Bool, isNumber: Bool ) -> (String, [paternidad]) {
        if isCount == false && isNumber {
            let arr = getValidationNumberPaternity()
            return ("Validación con número", arr)
        } else if isCount && isNumber == false {
            let arr = getVentaCuentaPaternity()
            return ("Venta con cuenta", arr)
        } else {
            let arr = getAllPaternity()
            return ("Todas", arr)
        }
    }

    func configureHeaderTokenSistem() {
        let autorization = statics.TOKEN_USERNAME_SERVICE + ":" + statics.TOKEN_PASSWORD_SERVICE
        statics.TOKEN_SISTEM_AUTHORIZATION = autorization.base64Encoded() ?? ""
    }

    func getNumEmployee() -> String {
        return model.getEmploye().NoEmpleado
    }

    func getDateFinish() -> String {
        return DateCalendar.convertTomorrowString(formatter: Formate.yyyy_MM_dd_dash)
    }

    func getDateStart() -> String {
        return  DateCalendar.getPassMonth(formatter: Formate.yyyy_MM_dd_dash)
    }

    func desencryptPaternity(arrPaternidad: [paternidad]) -> [paternidad] {
        var arrPatDesencrip: [paternidad] = []
        for index in arrPaternidad {
            let pat = paternidad()
            pat.numEmpleado = RSAKeyManager.desencrypt(text: index.numEmpleado)
            pat.descripcion = RSAKeyManager.desencrypt(text: index.descripcion)
            pat.estatus = RSAKeyManager.desencrypt(text: index.estatus)
            pat.aux = RSAKeyManager.desencrypt(text: index.aux)
            pat.fechaActivacion = RSAKeyManager.desencrypt(text: index.fechaActivacion)
            pat.canal = RSAKeyManager.desencrypt(text: index.canal)
            pat.numcel = RSAKeyManager.desencrypt(text: index.numcel)
            arrPatDesencrip.append(pat)
        }
        return arrPatDesencrip
    }

    func validateNumEmployeeToPaternity(list: [paternidad]) -> [paternidad] {
        var arrPatern: [paternidad] = []
        for index in listPaternity {
            if index.numEmpleado == getNumEmployee() {
                let obj = index
                arrPatern.append(obj)
            }
        }
        return arrPatern
    }

    func matchOportunityPaternity(listP: [paternidad]) -> [paternidad] {
        var arrResult: [paternidad] = []
        let arrPaternity = validateNumEmployeeToPaternity(list: listP)
        for index in arrPaternity {
            for elem in listOPP {
                if index.numcel == elem.celular {
                    index.numeroOpp = elem.numeroOpp
                    index.numeroCuenta = elem.numeroCuenta
                    index.nombre = elem.nombre
                    index.id = elem.id
                    index.fecha = elem.fecha
                    index.etapa = elem.etapa
                    index.celular = elem.celular
                    index.canalVenta = elem.canalVenta
                    index.isValidacionNumero = true
                }
            }
            arrResult.append(index)
        }
        return arrResult
    }

    func getDateCreation(date: String) -> String {
        let dateComplete = date.components(separatedBy: "T")
        let dateShort: String = dateComplete[0]
        return dateShort
    }

    func getValidationNumberPaternity() -> [paternidad] {
        let arrValNum = arrPaternityClear.filter {$0.isValidacionNumero == true}
        return arrValNum
    }

    func getVentaCuentaPaternity() -> [paternidad] {
        let arrValNum = arrPaternityClear.filter {$0.isValidacionNumero == false}
        return arrValNum
    }

    func getAllPaternity() -> [paternidad] {
        return arrPaternityClear
    }

}
