//
//  KeyPaternityResponse.swift
//  SalesCloud
//
//  Created by mhuertao on 15/08/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class paternidadKeysResponse: BaseResponse {
    var resultado: resultadoLLave?
    var mensaje: String = ""
    var folio: String = ""

    required init?(map: Map) {

    }

    override func mapping(map: Map) {
        mensaje <- map["mensaje"]
        folio <- map["folio"]
        resultado <- map["resultado"]
    }

}
class resultadoLLave: NSObject, Mappable {
    var idAcceso: String = ""
    var accesoPublico: String = ""
    var accesoPrivado: String = ""

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        idAcceso <- map["idAcceso"]
        accesoPublico <- map["accesoPublico"]
        accesoPrivado <- map["accesoPrivado"]
    }

    override init() {
    }

}
