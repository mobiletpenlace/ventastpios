//
//  DetailSaleResponse.swift
//  ventastpredesign
//
//  Created by Arturo Ceron Epigmenio on 22/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class DetailSaleResponse: BaseResponse {

    required init?(map: Map) {

    }

    override func mapping(map: Map) {
        MResult <- map["Result"]
        NumResultados <- map["NumResultados"]
        MArrDetalleSolicitud <- map["ArrDetalleSolicitud"]

    }

    var MResult: Result?
    var NumResultados: String?
    var MArrDetalleSolicitud: [ArrDetalleSolicitud] = []

}
