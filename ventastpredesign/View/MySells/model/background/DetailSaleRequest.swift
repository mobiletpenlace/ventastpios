//
//  DetailSaleRequest.swift
//  ventastpredesign
//
//  Created by Arturo Ceron Epigmenio on 22/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class DetailSaleRequest: BaseRequest {

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        MInfo <- map["Info"]
        MLogin <- map["Login"]

    }

    var MInfo: InfoDetailSale?
    var MLogin: Login?

}
