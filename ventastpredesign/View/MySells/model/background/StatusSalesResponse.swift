//
//  StatusSalesResponse.swift
//  ventastpredesign
//
//  Created by Claudia Isamar Delgado Vasquez on 13/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class StatusSalesResponse: BaseResponse {
    required init?(map: Map) {

    }
    override func mapping(map: Map) {
        MResult <- map["Result"]
        MResultStatus <- map["ResultStatus"]

    }
    var MResult: Result?
    var MResultStatus: ResultStatus?

}
