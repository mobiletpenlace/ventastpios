//
//  StatusSalesRequest.swift
//  ventastpredesign
//
//  Created by Claudia Isamar Delgado Vasquez on 13/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class StatusSalesRequest: BaseRequest {

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        FechaFinal <- map["FechaFinal"]
        FechaInicio <- map["FechaInicio"]
        FolioEmpleado <- map["FolioEmpleado"]
        MLogin <- map["Login"]
    }

    var FechaFinal: String?
    var FechaInicio: String?
    var FolioEmpleado: String?
    var MLogin: Login?
}
