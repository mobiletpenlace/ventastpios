//
//  InfoDetailSale.swift
//  ventastpredesign
//
//  Created by Arturo Ceron Epigmenio on 22/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class InfoDetailSale: NSObject, Mappable {
    required init?(map: Map) {

    }
    func mapping(map: Map) {
        FechaFin <- map["FechaFin"]
        FechaInicio <- map["FechaInicio"]
        FolioEmpleado <- map["FolioEmpleado"]
        IdStatus <- map["IdStatus"]
        IdTipo <- map["IdTipo"]

    }
    override init() {

    }
    var FechaFin: String?
    var FechaInicio: String?
    var FolioEmpleado: String?
    var IdStatus: String?
    var IdTipo: String?

}
