//
//  MySellViewDelegate.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 27/03/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import Foundation
extension MySellViewController: StatusSalesDelegate {
    func OnSuccessStatusSales(statusSalesResponse: StatusSalesResponse) {

        if statusSalesResponse.MResult?.Result != "1" {

            mAprobadasLabel.text = statusSalesResponse.MResultStatus?.Aprobadas
            mPendientesLabel.text = statusSalesResponse.MResultStatus?.Pendientes
            mRechazadasLabel.text = statusSalesResponse.MResultStatus?.Rechazada
            mInstaladasAprobadasLabel.text = statusSalesResponse.MResultStatus?.InstaladasAprobadas
            mInstaladasPendientesLabel.text = statusSalesResponse.MResultStatus?.InstaladasPendientes
            mInstaladasRechazadasLabel.text = statusSalesResponse.MResultStatus?.InstaladasRechazadas

            aprobadas = Int(mAprobadasLabel.text!)
            pendientes = Int(mPendientesLabel.text!)
            rechazadas = Int(mRechazadasLabel.text!)
            insAprobadas = Int(mInstaladasAprobadasLabel.text!)
            insPendientes = Int(mInstaladasPendientesLabel.text!)
            insRechazadas = Int(mInstaladasRechazadasLabel.text!)

            totalCapturadas = aprobadas! + pendientes! + rechazadas!
            totalVendidas = insAprobadas! + insPendientes! + insRechazadas!

            mTCapturadas.text = "\(totalCapturadas!)"
            mTVendidas.text = "\(totalVendidas!)"

        }
    }
}
