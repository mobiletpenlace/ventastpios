//
//  SalesFileCollectionReusableView.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 22/03/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class SalesFileCollectionReusableView: UICollectionReusableView {

    @IBOutlet weak var NumContract: UILabel!
    @IBOutlet weak var Date: UILabel!
    @IBOutlet weak var Status: UILabel!
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var IdOportunidad: UILabel!
    @IBOutlet weak var ViewHeader: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        //  ViewHeader.layer.backgroundColor
        Date.text = ""
        Status.text = ""
        message.text = ""
        NumContract.text = ""
        IdOportunidad.text = ""
        Constants.Border(view: ViewHeader, radius: 6, width: 0)
    }
}
