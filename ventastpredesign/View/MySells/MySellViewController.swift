//
//  MySellViewController.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 16/03/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import Amplitude

class MySellViewController: BaseVentasView {

    @IBOutlet weak var ViewApproved: UIView!
    @IBOutlet weak var ViewInstalled: UIView!
    @IBOutlet weak var ViewPending: UIView!
    @IBOutlet weak var ViewPending2: UIView!
    @IBOutlet weak var ViewRejected: UIView!
    @IBOutlet weak var ViewStopped: UIView!
    @IBOutlet weak var ViewCaptured: UIView!
    @IBOutlet weak var ViewSold: UIView!
    @IBOutlet weak var ViewOrange2: UIView!
    @IBOutlet weak var ViewOrange: UIView!
    @IBOutlet weak var ViewGreen: UIView!
    @IBOutlet weak var ViewGreen2: UIView!
    @IBOutlet weak var ViewPink: UIView!
    @IBOutlet weak var ViewPink2: UIView!
    @IBOutlet weak var ViewBlue: UIView!
    @IBOutlet weak var ViewBlue2: UIView!
    var mStatusSalesPresenter: StatusSalesPresenter!
    let realm = RealManager.mReal()
    let employed: Empleado = Empleado(value: RealManager.findFirst(object: Empleado.self)!)
    @IBOutlet weak var mAprobadasLabel: UILabel!
    @IBOutlet weak var mPendientesLabel: UILabel!
    @IBOutlet weak var mRechazadasLabel: UILabel!
    @IBOutlet weak var mInstaladasAprobadasLabel: UILabel!
    @IBOutlet weak var mInstaladasPendientesLabel: UILabel!
    @IBOutlet weak var mInstaladasRechazadasLabel: UILabel!
    @IBOutlet weak var mTCapturadas: UILabel!
    @IBOutlet weak var mTVendidas: UILabel!

    var aprobadas: Int?
    var pendientes: Int?
    var rechazadas: Int?
    var insAprobadas: Int?
    var insPendientes: Int?
    var insRechazadas: Int?
    var totalCapturadas: Int?
    var totalVendidas: Int?
    var resultFechaInicio: String?
    var resultFechaFin: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        design()
        resultFechaInicio = Constants.GetDate(formater: dateFormatter.dayhzone, numDay: 0, numMonth: 0)
        resultFechaFin = Constants.GetDate(formater: dateFormatter.dayhzone, numDay: 1, numMonth: 0)
        mStatusSalesPresenter.getStatusSales(mFechaFinal: resultFechaFin!, mFechaInicio: resultFechaInicio!, mFolioEmpleado: employed.FolioEmpleado, mStatusSalesDelegate: self)
    }

    override func viewDidAppear(_ animated: Bool) {
        Amplitude.instance().logEvent("Show_MySellsScreen")
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBAction func ActionBack(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }

    @IBAction func ActionDate(_ sender: Any) {
        let alert = UIAlertController(title: "Elige una opción", message: "", preferredStyle: .alert)
        let action1 = UIAlertAction(title: "Hoy", style: .default, handler: { (_) -> Void in
            self.resultFechaInicio = Constants.GetDate(formater: dateFormatter.dayhzone, numDay: 0, numMonth: 0)
            self.resultFechaFin = Constants.GetDate(formater: dateFormatter.dayhzone, numDay: 1, numMonth: 0)

            self.mStatusSalesPresenter.getStatusSales(mFechaFinal: self.resultFechaFin!, mFechaInicio: self.resultFechaInicio!, mFolioEmpleado: self.employed.FolioEmpleado, mStatusSalesDelegate: self)

        })

        let action2 = UIAlertAction(title: "Ayer", style: .default, handler: { (_) -> Void in
            self.resultFechaInicio = Constants.GetDate(formater: dateFormatter.dayhzone, numDay: -1, numMonth: 0)
            self.resultFechaFin = Constants.GetDate(formater: dateFormatter.dayhzone, numDay: 1, numMonth: 0)
            self.mStatusSalesPresenter.getStatusSales(mFechaFinal: self.resultFechaFin!, mFechaInicio: self.resultFechaInicio!, mFolioEmpleado: self.employed.FolioEmpleado, mStatusSalesDelegate: self)

        })
        let action3 = UIAlertAction(title: "Semana", style: .default, handler: { (_) -> Void in
            self.resultFechaInicio = Constants.GetDate(formater: dateFormatter.dayhzone, numDay: -7, numMonth: 0)
            self.resultFechaFin = Constants.GetDate(formater: dateFormatter.dayhzone, numDay: 1, numMonth: 0)

            self.mStatusSalesPresenter.getStatusSales(mFechaFinal: self.resultFechaFin!, mFechaInicio: self.resultFechaInicio!, mFolioEmpleado: self.employed.FolioEmpleado, mStatusSalesDelegate: self)

        })
        let action4 = UIAlertAction(title: "Mes", style: .default, handler: { (_) -> Void in
            self.resultFechaInicio = Constants.GetDate(formater: dateFormatter.dayhzone, numDay: 0, numMonth: -1)
            self.resultFechaFin = Constants.GetDate(formater: dateFormatter.dayhzone, numDay: 1, numMonth: 0)
            self.mStatusSalesPresenter.getStatusSales(mFechaFinal: self.resultFechaFin!, mFechaInicio: self.resultFechaInicio!, mFolioEmpleado: self.employed.FolioEmpleado, mStatusSalesDelegate: self)

        })

        alert.view.tintColor = UIColor.black
        alert.addAction(action1)
        alert.addAction(action2)
        alert.addAction(action3)
        alert.addAction(action4)

        present(alert, animated: true, completion: {
            alert.view.superview?.isUserInteractionEnabled = true
            alert.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertControllerBackgroundTapped)))
        })

    }

    @IBAction func ActionApproved(_ sender: Any) {
        let viewAlert: UIStoryboard = UIStoryboard(name: "SellApprovedViewController", bundle: nil)
        let viewAlertVC = viewAlert.instantiateViewController(withIdentifier: "SellApprovedViewController")as! SellApprovedViewController
        viewAlertVC.mType = 1
        viewAlertVC.mFechaInicio = self.resultFechaInicio
        viewAlertVC.mFechaFin = self.resultFechaFin
        self.present(viewAlertVC, animated: false, completion: {})
    }

    @IBAction func ActionInstalled(_ sender: Any) {
        let viewAlert: UIStoryboard = UIStoryboard(name: "SellApprovedViewController", bundle: nil)
        let viewAlertVC = viewAlert.instantiateViewController(withIdentifier: "SellApprovedViewController")as! SellApprovedViewController
        viewAlertVC.mType = 4
        viewAlertVC.mFechaInicio = self.resultFechaInicio
        viewAlertVC.mFechaFin = self.resultFechaFin
        self.present(viewAlertVC, animated: false, completion: {})
    }

    @IBAction func ActionPending(_ sender: Any) {
        let viewAlert: UIStoryboard = UIStoryboard(name: "SellApprovedViewController", bundle: nil)
        let viewAlertVC = viewAlert.instantiateViewController(withIdentifier: "SellApprovedViewController")as! SellApprovedViewController
        viewAlertVC.mType = 2
        viewAlertVC.mFechaInicio = self.resultFechaInicio
        viewAlertVC.mFechaFin = self.resultFechaFin
        self.present(viewAlertVC, animated: false, completion: {})
    }

    @IBAction func ActionPending2(_ sender: Any) {
        let viewAlert: UIStoryboard = UIStoryboard(name: "SellApprovedViewController", bundle: nil)
        let viewAlertVC = viewAlert.instantiateViewController(withIdentifier: "SellApprovedViewController")as! SellApprovedViewController
        viewAlertVC.mType = 5
        viewAlertVC.mFechaInicio = self.resultFechaInicio
        viewAlertVC.mFechaFin = self.resultFechaFin
        self.present(viewAlertVC, animated: false, completion: {})
    }

    @IBAction func ActionRejected(_ sender: Any) {
        let viewAlert: UIStoryboard = UIStoryboard(name: "SellApprovedViewController", bundle: nil)
        let viewAlertVC = viewAlert.instantiateViewController(withIdentifier: "SellApprovedViewController")as! SellApprovedViewController
        viewAlertVC.mType = 3
        viewAlertVC.mFechaInicio = self.resultFechaInicio
        viewAlertVC.mFechaFin = self.resultFechaFin
        self.present(viewAlertVC, animated: false, completion: {})
    }

    @IBAction func ActionStopped(_ sender: Any) {
        let viewAlert: UIStoryboard = UIStoryboard(name: "SellApprovedViewController", bundle: nil)
        let viewAlertVC = viewAlert.instantiateViewController(withIdentifier: "SellApprovedViewController")as! SellApprovedViewController
        viewAlertVC.mType = 6
        viewAlertVC.mFechaInicio = self.resultFechaInicio
        viewAlertVC.mFechaFin = self.resultFechaFin
        self.present(viewAlertVC, animated: false, completion: {})
    }

    override func getPresenter() -> BasePresenter? {
        mStatusSalesPresenter = StatusSalesPresenter(view: self)
        return mStatusSalesPresenter
    }

    @objc func alertControllerBackgroundTapped() {
        self.dismiss(animated: true, completion: nil)
    }

    func design() {
        Constants.BorderCustom(view: ViewOrange, radius: 8, width: 0, masked: [.layerMinXMaxYCorner, .layerMaxXMaxYCorner])
        Constants.BorderCustom(view: ViewOrange2, radius: 8, width: 0, masked: [.layerMinXMaxYCorner, .layerMaxXMaxYCorner])
        Constants.BorderCustom(view: ViewGreen, radius: 8, width: 0, masked: [.layerMinXMaxYCorner, .layerMaxXMaxYCorner])
        Constants.BorderCustom(view: ViewGreen2, radius: 8, width: 0, masked: [.layerMinXMaxYCorner, .layerMaxXMaxYCorner])
        Constants.BorderCustom(view: ViewPink, radius: 8, width: 0, masked: [.layerMinXMaxYCorner, .layerMaxXMaxYCorner])
        Constants.BorderCustom(view: ViewPink2, radius: 8, width: 0, masked: [.layerMinXMaxYCorner, .layerMaxXMaxYCorner])
        Constants.BorderCustom(view: ViewBlue, radius: 8, width: 0, masked: [.layerMinXMaxYCorner, .layerMaxXMaxYCorner])
        Constants.BorderCustom(view: ViewBlue2, radius: 8, width: 0, masked: [.layerMinXMaxYCorner, .layerMaxXMaxYCorner])

        Constants.Border(view: ViewApproved, radius: 8, width: 1)
        Constants.Border(view: ViewInstalled, radius: 8, width: 1)
        Constants.Border(view: ViewPending, radius: 8, width: 1)
        Constants.Border(view: ViewPending2, radius: 8, width: 1)
        Constants.Border(view: ViewRejected, radius: 8, width: 1)
        Constants.Border(view: ViewStopped, radius: 8, width: 1)
        Constants.Border(view: ViewCaptured, radius: 8, width: 1)
        Constants.Border(view: ViewSold, radius: 8, width: 1)

         Constants.Gradient(view: self.ViewApproved, frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.size.width - 70, height: self.ViewApproved.frame.size.height), colors: [Colors.White, Colors.TransWhite, Colors.LowGray])
         Constants.Gradient(view: self.ViewInstalled, frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.size.width - 70, height: self.ViewInstalled.frame.size.height), colors: [Colors.White, Colors.TransWhite, Colors.LowGray])
         Constants.Gradient(view: self.ViewPending, frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.size.width - 70, height: self.ViewPending.frame.size.height), colors: [Colors.White, Colors.TransWhite, Colors.LowGray])
         Constants.Gradient(view: self.ViewPending2, frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.size.width - 70, height: self.ViewPending2.frame.size.height), colors: [Colors.White, Colors.TransWhite, Colors.LowGray])
         Constants.Gradient(view: self.ViewRejected, frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.size.width - 70, height: self.ViewRejected.frame.size.height), colors: [Colors.White, Colors.TransWhite, Colors.LowGray])
         Constants.Gradient(view: self.ViewStopped, frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.size.width - 70, height: self.ViewStopped.frame.size.height), colors: [Colors.White, Colors.TransWhite, Colors.LowGray])
         Constants.Gradient(view: self.ViewSold, frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.size.width - 70, height: self.ViewSold.frame.size.height), colors: [Colors.White, Colors.TransWhite, Colors.LowGray])
         Constants.Gradient(view: self.ViewCaptured, frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.size.width - 70, height: self.ViewCaptured.frame.size.height), colors: [Colors.White, Colors.TransWhite, Colors.LowGray])
    }
}
