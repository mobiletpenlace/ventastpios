//
//  SellApprovedViewDelegate.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 27/03/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
extension SellApprovedViewController: DetailSaleDelegate {
    func OnSuccessDetailSale(mDetailSaleResponse: DetailSaleResponse) {
        let contador = mDetailSaleResponse.MArrDetalleSolicitud.count
        ScrollView.contentSize = CGSize(width: 100, height: CGFloat(250) * CGFloat(contador))
        if contador > 0 {
            for index in 0 ..< contador {
                if let view = Bundle.main.loadNibNamed("SalesFileCollectionReusableView", owner: self, options: nil)?.first as? SalesFileCollectionReusableView {
                    if mDetailSaleResponse.MArrDetalleSolicitud[index].NoContrato == nil {
                        view.NumContract.text = ""
                    } else {
                        view.NumContract.text = mDetailSaleResponse.MArrDetalleSolicitud[index].NoContrato
                    }
                    if mDetailSaleResponse.MArrDetalleSolicitud[index].FechaLiberacion == nil {
                        view.Date.text = ""
                    } else {
                        view.Date.text = mDetailSaleResponse.MArrDetalleSolicitud[index].FechaLiberacion
                    }
                    if mDetailSaleResponse.MArrDetalleSolicitud[index].Estatus == nil {
                        view.Status.text = "Estatus de mesa de control: "
                    } else {
                        view.Status.text = "Estatus de mesa de control: " + mDetailSaleResponse.MArrDetalleSolicitud[index].Estatus!
                    }
                    if mDetailSaleResponse.MArrDetalleSolicitud[index].MotivoRechazoMC == nil {
                        view.message.text = "Motivo de Rechazo: "
                    } else {
                        view.message.text = "Motivo de Rechazo: " + mDetailSaleResponse.MArrDetalleSolicitud[index].MotivoRechazoMC!
                    }
                    if mDetailSaleResponse.MArrDetalleSolicitud[index].IdOportunidad == nil {
                        view.IdOportunidad.text = ""
                    } else {
                        view.IdOportunidad.text = mDetailSaleResponse.MArrDetalleSolicitud[index].IdOportunidad
                    }

                    view.ViewHeader.layer.backgroundColor = ColorPrincipal?.cgColor
                    ScrollView.addSubview(view)
                    view.frame.size.height = CGFloat(250)
                    view.frame.size.width = self.view.bounds.size.width
                    view.frame.origin.y = CGFloat(index) * CGFloat(250)
                }
            }
        } else {
            ViewDialogue.isHidden = false
        }

    }
}
