//
//  DetailSalePresenter.swift
//  ventastpredesign
//
//  Created by Arturo Ceron Epigmenio on 22/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

protocol DetailSaleDelegate: NSObjectProtocol {
    func OnSuccessDetailSale(mDetailSaleResponse: DetailSaleResponse)
}
class DetailSalePresenter: BaseVentastpredesignPresenter {
    var mDetailSaleDelegate: DetailSaleDelegate!

    func GetDetailSale(mFechaFin: String, mFechaInicio: String, mFolioEmpleado: String, mIdStatus: String, mIdTipo: String, mDetailSaleDelegate: DetailSaleDelegate) {
        self.mDetailSaleDelegate = mDetailSaleDelegate

        let detailRequest = DetailSaleRequest()
        detailRequest.MInfo = InfoDetailSale()
        detailRequest.MInfo?.FechaFin = mFechaFin
        detailRequest.MInfo?.FechaInicio = mFechaInicio
        detailRequest.MInfo?.FolioEmpleado = mFolioEmpleado
        detailRequest.MInfo?.IdStatus = mIdStatus
        detailRequest.MInfo?.IdTipo = mIdTipo
        detailRequest.MLogin = Login()
        detailRequest.MLogin?.Ip = "1.1.1.1"
        detailRequest.MLogin?.User = "25631"
        detailRequest.MLogin?.SecretWord = "Middle100$"

        RetrofitManager<DetailSaleResponse>.init(requestUrl: ApiDefinition.API_DETAIL_REQUEST, delegate: self).request(requestModel: detailRequest)
    }
    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if requestUrl == ApiDefinition.API_DETAIL_REQUEST {
            OnSuccessDetailSale(detailSaleResponse: response as! DetailSaleResponse)
            view.hideLoading()

        }
    }

    func OnSuccessDetailSale(detailSaleResponse: DetailSaleResponse) {
        if detailSaleResponse.MResult?.Description != "Success operation" {
             Constants.Alert(title: "", body: dialogs.Error.InternService, type: type.Error, viewC: mViewController)
        } else if detailSaleResponse.MResult?.Description == nil {
              Constants.Alert(title: "", body: dialogs.Error.InternService, type: type.Error, viewC: mViewController)
        } else {
           mDetailSaleDelegate.OnSuccessDetailSale(mDetailSaleResponse: detailSaleResponse)
        }

    }

}
