//
//  StatusSalesPresenter.swift
//  ventastpredesign
//
//  Created by Claudia Isamar Delgado Vasquez on 13/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

protocol StatusSalesDelegate: NSObjectProtocol {
    func OnSuccessStatusSales(statusSalesResponse: StatusSalesResponse)
}
class StatusSalesPresenter: BaseVentastpredesignPresenter {
    var mStatusSalesDelegate: StatusSalesDelegate!

    func getStatusSales(mFechaFinal: String, mFechaInicio: String, mFolioEmpleado: String, mStatusSalesDelegate: StatusSalesDelegate) {
    // func getStatusSales(mFolioEmpleado: String, mStatusSalesDelegate: StatusSalesDelegate){

        self.mStatusSalesDelegate = mStatusSalesDelegate

        let statusSalesRequest = StatusSalesRequest()

        statusSalesRequest.MLogin = Login()
        statusSalesRequest.MLogin?.Ip = "1.1.1.1"
        statusSalesRequest.MLogin?.User = "25631"
        statusSalesRequest.MLogin?.SecretWord = "Middle100$"

        statusSalesRequest.FechaFinal = mFechaFinal
        statusSalesRequest.FechaInicio = mFechaInicio
        statusSalesRequest.FolioEmpleado = mFolioEmpleado

        RetrofitManager<StatusSalesResponse>.init(requestUrl: ApiDefinition.API_CONSULT_STATUS_REQUEST, delegate: self).request(requestModel: statusSalesRequest)

    }
    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if requestUrl == ApiDefinition.API_CONSULT_STATUS_REQUEST {
            view.hideLoading()
            OnSuccessStatusSales(statusSalesResponse: response as! StatusSalesResponse)
        }
    }
    func OnSuccessStatusSales(statusSalesResponse: StatusSalesResponse) {

        mStatusSalesDelegate.OnSuccessStatusSales(statusSalesResponse: statusSalesResponse)
       /* if statusSalesResponse.MResult?.Result != "1" {
            mStatusSalesDelegate.OnSuccessStatusSales(statusSalesResponse: statusSalesResponse)
        }else if(statusSalesResponse.MResult?.Result == nil){
            Constants.Alert(title: "Error", body: StringDialogs.dialog_error_intern , viewC: mViewController)
        }
        else{
            Constants.Alert(title: "Error", body: StringDialogs.dialog_error_intern, viewC: mViewController)
        }*/
    }

}
