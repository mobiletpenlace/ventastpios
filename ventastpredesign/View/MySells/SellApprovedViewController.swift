//
//  SellApprovedViewController.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 22/03/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class SellApprovedViewController: BaseVentasView {

    @IBOutlet weak var ScrollView: UIScrollView!
    var mFechaInicio: String?
    var mFechaFin: String?
    var mIdStatus: String?
    var mIdTipo: String?
    @IBOutlet weak var ViewDialogue: UIView!

    var ColorPrincipal: UIColor?
    var mDetailPresenter: DetailSalePresenter!
    let employed: Empleado = Empleado(value: RealManager.findFirst(object: Empleado.self)!)
    var mType: Int?

    @IBOutlet weak var TitlePrincipal: UILabel!
    @IBOutlet weak var SubTitle: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)

        TitlePrincipal.text = ""
        SubTitle.text = ""
        loadService(num: mType!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    override func getPresenter() -> BasePresenter? {
        mDetailPresenter = DetailSalePresenter(view: self)
        return mDetailPresenter
    }

    @IBAction func ActionBack(_ sender: Any) {
        Constants.Back(viewC: self)
    }

    func loadService(num: Int) {
        ViewDialogue.isHidden = true
        switch num {
        case 1:
            mIdStatus = "1"
            mIdTipo = "1"
            ColorPrincipal = Colors.Green
            TitlePrincipal.text = "Ventas Aprobadas"
            SubTitle.text = "Ventas aprobadas por Mesa de Control"

        case 2:
            mIdStatus = "2"
            mIdTipo = "1"
            ColorPrincipal = Colors.Yellow
            TitlePrincipal.text = "Ventas Pendientes"
            SubTitle.text = "Ventas pendientes por Mesa de Control"

        case 3:
            mIdStatus = "3"
            mIdTipo = "1"
            ColorPrincipal = Colors.Pink
            TitlePrincipal.text = "Ventas Rechazadas"
            SubTitle.text = "Ventas rechazadas por Mesa de Control"

        case 4:
            mIdStatus = "1"
            mIdTipo = "2"
            ColorPrincipal = Colors.Green
            TitlePrincipal.text = "Ventas Instaladas"
            SubTitle.text = "Ventas instaladas"

        case 5:
            mIdStatus = "2"
            mIdTipo = "2"
            ColorPrincipal = Colors.Yellow
            TitlePrincipal.text = "Ventas Pendientes"
            SubTitle.text = "Ventas pedientes por Instalar"

        case 6:
            mIdStatus = "3"
            mIdTipo = "2"
            ColorPrincipal = Colors.Pink
            TitlePrincipal.text = "Ventas Detenidas"
            SubTitle.text = "Ventas detenidas por Instalar"

        default:
            break
        }
        mDetailPresenter.GetDetailSale(mFechaFin: mFechaFin!, mFechaInicio: mFechaInicio!, mFolioEmpleado: employed.FolioEmpleado, mIdStatus: mIdStatus!, mIdTipo: mIdTipo!, mDetailSaleDelegate: self)
    }

}
