//
//  CalendarComissionsViewController.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 30/09/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import FSCalendar

protocol GetDateDelegate {
    func getNewDates(_ dateInicial: Date, _ dateFinal: Date)
}

class CalendarComissionsViewController: UIViewController, FSCalendarDelegate, FSCalendarDataSource, FSCalendarDelegateAppearance {

    fileprivate let gregorian = Calendar(identifier: .gregorian)
    @IBOutlet weak var CalendarViewGral: UIView!
    @IBOutlet weak var calendarView: FSCalendar!
    @IBOutlet weak var ViewHeader: UIView!
    @IBOutlet weak var ButtonAceptarOmitir: UIButton!
    @IBOutlet weak var calendarCell: FSCalendarCell!
    weak var selectionLayer: CAShapeLayer?
    weak var roundedLayer: CAShapeLayer?
    var getDatesDelegate: GetDateDelegate?
    var currentDateSelected: String?
    let formatter = DateFormatter()
    let calendar = Calendar.current
    let date = Date()
    var daysWeekMax = 0
    var daysWeekMin = 0
    var FechaInicio: Date?
    var FechaFin: Date?
    var firstDate: Date?
    var lastDate: Date?
    var primeraFecha: Date?
    var ultimaFecha: Date?
    var predef1: Date?
    var fechaInicial: Date?
    var datesRange: [Date]?
    var componentsI = DateComponents()
    var componentsF = DateComponents()
    let Userdefaults = UserDefaults.standard

    func datesRange(from: Date, to: Date) -> [Date] {
        if from > to {return [Date]()}
        var tempDate = from
        var array = [tempDate]
        while tempDate < to {
            tempDate = Calendar.current.date(byAdding: .day, value: 1, to: tempDate)!
            array.append(tempDate)
        }
        return array
    }

    override func viewDidLoad() {
        super.viewDidLoad()

    if datesRange == nil {
        predef1 = DateCalendar.convertStringDate(date: faseComisiones.fechaInicio, formatter: Formate.yyyy_MM_dd_dash)
        let predef2 = DateCalendar.convertStringDate(date: faseComisiones.fechaFin, formatter: Formate.yyyy_MM_dd_dash)
        let predef = datesRange(from: predef1!, to: predef2!)
        for d in predef {
            calendarView.select(d)

    }
        }

        Constants.Border(view: CalendarViewGral, radius: 10, width: 0)
        Constants.Border(view: ViewHeader, radius: 10, width: 0)
        Constants.Border(view: calendarView, radius: 10, width: 0)
        Constants.BorderCustom(view: ViewHeader, radius: 10, width: 0, masked: [.layerMaxXMinYCorner, .layerMinXMinYCorner])
        calendarView.locale = Locale(identifier: "Es")
        calendarView.appearance.caseOptions = [.headerUsesUpperCase]
        ButtonAceptarOmitir.isHidden = true

    }

    func calendar(_ calendarView: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        DeselectDate(Inicio: predef1!)

        if primeraFecha == nil && ultimaFecha == nil {

            primeraFecha = date
            ButtonAceptarOmitir.isHidden = false
            ButtonAceptarOmitir.setTitle("Aceptar", for: .normal)
            SundayCalculMax(num: primeraFecha!.weekday)
            fechaInicial = self.calendar.date(byAdding: .day, value: daysWeekMax, to: primeraFecha!)
            Userdefaults.set(fechaInicial, forKey: "fechaInicial")
            datesRange = [fechaInicial!]
            ultimaFecha = date
            var ultimaFecha = self.calendar.date(byAdding: .day, value: 6, to: fechaInicial!)
            let range = datesRange(from: fechaInicial!, to: ultimaFecha!)
            ultimaFecha = range.last
            for d in range {
                calendarView.select(d)
            }
            datesRange = range
            Userdefaults.set(datesRange, forKey: "datesRange")
            Userdefaults.synchronize()
            configureVisibleCells()
            return
        }

        // Ambas fechas seleccionadas
        if primeraFecha != nil && ultimaFecha != nil {
            ButtonAceptarOmitir.setTitle("Omitir", for: .normal)
            for d in calendarView.selectedDates {
                calendarView.deselect(d)
            }
            ultimaFecha = nil
            primeraFecha = nil
            datesRange = []
            configureVisibleCells()
        }

    }

    func calendar(_ calendarView: FSCalendar, didDeselect date: Date, at monthPosition: FSCalendarMonthPosition) {
        if primeraFecha != nil && ultimaFecha != nil {
            for d in calendarView.selectedDates {
                calendarView.deselect(d)
            }
            ultimaFecha = nil
            primeraFecha = nil
            datesRange = []
        }
    }

    func DeselectDate(Inicio: Date) {
        calendarView.deselect(Inicio)
        for j in 0..<7 {
            var components9 = DateComponents()
            components9.day = j + 1
            let fecha = self.calendar.date(byAdding: components9, to: Inicio)
            calendarView.deselect(fecha!)
        }
    }

    func maximumDate(for calendar: FSCalendar) -> Date {
        // fecha actual real
        let components1 = DateComponents()
        let fechaInicio = self.calendar.date(byAdding: components1, to: self.date)
        let weekday = Calendar.current.component(.weekday, from: fechaInicio!)
        SundayCalculMax(num: weekday)
        // se obtendra el domingo anterior mas cercano a la fecha actual
        var componente = DateComponents()
        componente.day = daysWeekMax - 1
        let fechaMax = self.calendar.date(byAdding: componente, to: fechaInicio!)
        return fechaMax!
    }

    func minimumDate(for calendar: FSCalendar) -> Date {
        // fecha real de seis meses atras
        var components = DateComponents()
        components.month = -12
        let fechaFin = self.calendar.date(byAdding: components, to: self.date)
        let weekday = Calendar.current.component(.weekday, from: fechaFin!)
        SundayCalculMin(num: weekday)
        // se obtendra el domingo mas cercano a la fecha anterior
        var componente = DateComponents()
        componente.day = daysWeekMin
        let fechaMin = self.calendar.date(byAdding: componente, to: fechaFin!)
        return fechaMin!

    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        guard let location = touch?.location(in: self.view) else { return }
        if !CalendarViewGral.frame.contains(location) {
            dismiss(animated: false, completion: nil)
        }
    }

    @IBAction func actionAceptarOmitir(_ sender: Any) {

        let finDate = Calendar.current.date(byAdding: .day, value: 6, to: fechaInicial!)
        if datesRange == [] {
            ButtonAceptarOmitir.setTitle("Omitir", for: .normal)
            dismiss(animated: false, completion: nil)
        } else {
            // PASAR FECHAS DE AQUI
            getDatesDelegate?.getNewDates(fechaInicial!, finDate!)
            dismiss(animated: false, completion: nil)
            }
    }

    func SundayCalculMax(num: Int) {
        switch num {
        case 1:
            daysWeekMax = -6
        case 2:
            daysWeekMax = 0
        case 3:
            daysWeekMax = -1
        case 4:
            daysWeekMax = -2
        case 5:
            daysWeekMax = -3
        case 6:
            daysWeekMax = -4
        case 7:
            daysWeekMax = -5
        default:
            break
        }
    }

    func SundayCalculMin(num: Int) {
        switch num {
        case 1:
            daysWeekMin = 1
        case 2:
            daysWeekMin = 0
        case 3:
            daysWeekMin = 6
        case 4:
            daysWeekMin = 5
        case 5:
            daysWeekMin = 4
        case 6:
            daysWeekMin = 3
        case 7:
            daysWeekMin = 2
        default:
            break
        }

    }

    // MARK: - Private functions

    private func configureVisibleCells() {
        calendarView.visibleCells().forEach { (cell) in
            let date = calendarView.date(for: cell)
            let position = calendarView.monthPosition(for: cell)
            self.configure(cell: cell, for: date!, at: position)
        }
    }

    private func configure(cell: FSCalendarCell, for date: Date, at position: FSCalendarMonthPosition) {

        let diyCell = (cell as? DIYCalendarCell)
        // Custom today circle
        diyCell?.circleImageView?.isHidden = !self.gregorian.isDateInToday(date)
        // Configure selection layer
        if position == .current {

            var selectionType = SelectionType.none

            if calendarView.selectedDates.contains(date) {
                let previousDate = self.gregorian.date(byAdding: .day, value: -1, to: date)!
                let nextDate = self.gregorian.date(byAdding: .day, value: 1, to: date)!
                if calendarView.selectedDates.contains(date) {
                    if calendarView.selectedDates.contains(previousDate) && calendarView.selectedDates.contains(nextDate) {
                        selectionType = .middle
                    } else if calendarView.selectedDates.contains(previousDate) && calendarView.selectedDates.contains(date) {
                        selectionType = .rightBorder
                    } else if calendarView.selectedDates.contains(nextDate) {
                        selectionType = .leftBorder
                    } else {
                        selectionType = .single
                    }
                }
            } else {
                selectionType = .none
            }
            if selectionType == .none {
                diyCell?.selectionLayer?.isHidden = true
                return
            }
            diyCell?.selectionLayer?.isHidden = false
            diyCell?.selectionType = selectionType

        } else {
            diyCell?.circleImageView?.isHidden = true
            diyCell?.selectionLayer?.isHidden = true
        }
    }

}
