//
//  DateCalendar.swift
//  CustomBaseProject
//
//  Created by Marisol Huerta O. on 11/03/20.
//  Copyright © 2020 Marisol Huerta Ortega. All rights reserved.
//

import Foundation

struct Formate {
    static var D = "d"                                          // Example: 13
    static var MMM_yyyy = "MMM yyyy"                                  // Example:  febrero 2020
    static var MMM = "MMM"                                  // Example:  febrero
    static var yyyy = "yyyy"                                  // Example:  2020
    static var HH_mm = "HH:mm"                                  // Example:  13:12
    static var d_MMM_yyyy = "d MMM yyyy"                      // Example: 13 feb 2020
    static var d_MMMM_yyyy = "d MMMM yyyy"                      // Example: 13 febrero 2020
    static var dd_MMMM_yyyy = "dd MMMM yyyy"                      // Example: 03 febrero 2020
    static var d_MMM = "d MMM"                              // Example: 13 feb
    static var d_MMMM_ = "d MMMM"                              // Example: 13 febrero
    static var dd_MMMM_ = "dd MMMM"                              // Example: 03 febrero
    static var EEEE_d_MMMM_yyyy = "EEEE, d 'de' MMMM yyyy"      // Example: Lunes, 13 de febrero 2020
    static var d_MMMM = "d 'de' MMMM"                            // Example:  13 de febrero
    static var dd_MM_yyyy = "dd/MM/yyyy"                        // Example: 13/02/2020
    static var dd_MM_yy = "dd/MM/yy"                        // Example: 13/02/20
    static var dd_MM_yyyy_dash = "dd-MM-yyyy"                   // Example: 13-02-2020
    static var yyyy_MM_dd_dash = "yyyy-MM-dd"                        // Example: 2020-02-20
    static var dd_MM_yyyy_HHmm = "dd/MM/yyyy'T'HH:mm"           // Example: 13/02/2020T13:12
    static var yyyy_MM_dd_HHmmssSSS = "yyyy-MM-dd-HH:mm:ss:SSS"
    static var EEEE = "EEEE"                                    // Example: Lunes
    static var EE = "EE"
    static var EEEE_d_HHmm_a = "EEEE d, HH:mm a"                // Example: Lunes 13, 2:30 pm
    static var HH_mm_a = "HH:mm a"
    static var yyyy_MM_dd = "yyyy/MM/dd"
    static var MMMM_yyyy = "MMMM yyyy"                          // Example: Enero 2021
    static var H_mm_a = "h:mm a"                              // Example: 1:12 pm
    static var dd_MMMM_yyyy_ = "dd/MMMM/yyyy"
    static var d_d_MMM = "d/MMM"                                //Example: 13/feb
}

struct Locales {
    static var es_MX = "es_MX"                          // Español de Mexico
    static var es_Es = "es_Es"                          // Español de España
    static var en_US_POSIX = "en_US_POSIX"              // Ingles de Estados Unidos

}

class DateCalendar {

    class func convertTomorrowString(formatter: String, _ locales: String = Locales.es_MX) -> String {
        var result = ""
        let tomorrowDate = Calendar.current.date(byAdding: .day, value: 1, to: Date())
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: locales)
        dateFormatter.dateFormat = formatter
        result = dateFormatter.string(from: tomorrowDate ?? Date())
        return result
    }

    class func convertTodayString(formatter: String, _ locales: String = Locales.es_MX) -> String {
        var result = ""
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: locales)
        dateFormatter.dateFormat = formatter
        result = dateFormatter.string(from: Date())
        return result
    }

    class func convertTimeInterval(date: Date) -> Double {
        return date.timeIntervalSinceReferenceDate
    }

    class func convertTimeIntervalSince(date: Date) -> Double {
        return date.timeIntervalSince1970
    }

    class func convertDateString(date: Date, formatter: String, _ locales: String = Locales.es_MX) -> String {
        var result = ""
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: locales)
        dateFormatter.dateFormat = formatter
        result = dateFormatter.string(from: date)
        return result
    }

    class func convertStringDate(date: String, formatter: String, _ locales: String = Locales.es_MX) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: locales)
        dateFormatter.dateFormat = formatter
        let result = dateFormatter.date(from: date)
        return result
    }

    class func convertDateInt(date: Date, component: Calendar.Component) -> Int {
        return Calendar.current.component(component, from: date)
    }

    class func futureDate(date: Date, value: Int, component: Calendar.Component) -> Date? {
        return  Calendar.current.date(byAdding: component, value: value, to: date)
    }

    class func futureDateInterval30min(date: Date, _ locales: String = Locales.es_MX) -> Date? {
        var newDate: Date?
        var lessMinute: Int = 0
        let minute = convertDateInt(date: date, component: .minute)
        if minute > 30 {
            lessMinute = 60 - minute
        } else if minute < 30 {
            lessMinute = 30 - minute
        }
        newDate = futureDate(date: date, value: lessMinute, component: .minute)
        return newDate
    }

    class func pastDate(date: Date, value: Int, component: Calendar.Component) -> Date? {
        return  Calendar.current.date(byAdding: component, value: -value, to: date)
    }

    class func getToday(formatter: String, _ locales: String = Locales.es_MX) -> String {
        var result = ""
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: locales)
        dateFormatter.dateFormat = formatter
        result = dateFormatter.string(from: Date())
        return result
    }

    class func getYesterdayString(formatter: String, _ locales: String = Locales.es_MX) -> String {
        let yesterday = Calendar.current.date(byAdding: .day, value: -1, to: Date())
        var result = ""
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: locales)
        dateFormatter.dateFormat = formatter
        result = dateFormatter.string(from: yesterday ?? Date())
        return result
    }

    class func getFirstMondayWeek(formatter: String, _ locales: String = Locales.es_MX) -> String {
        let cal = Calendar.current
        var result = ""
        var comps = cal.dateComponents([.weekOfYear, .yearForWeekOfYear], from: Date())
        comps.weekday = 2 // Monday
        let mondayInWeek = cal.date(from: comps)!
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: locales)
        dateFormatter.dateFormat = formatter
        result = dateFormatter.string(from: mondayInWeek)
        return result
    }

    class func getLastWeek(formatter: String, _ locales: String = Locales.es_MX) -> String {
        let day = Calendar.current.date(byAdding: .day, value: -8, to: Date())
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: locales)
        dateFormatter.dateFormat = formatter
       return dateFormatter.string(from: day ?? Date())
    }

    class func getFirstDayMonth(formatter: String, _ locales: String = Locales.es_MX) -> String {
        var result = ""
        let components = Calendar.current.dateComponents([.year, .month], from: Date())
        let startOfMonth = Calendar.current.date(from: components)!
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: locales)
        dateFormatter.dateFormat = formatter
        result = dateFormatter.string(from: startOfMonth)
        return result
    }

    class func getPassMonth(formatter: String, _ locales: String = Locales.es_MX) -> String {
        let day = Calendar.current.date(byAdding: .month, value: -1, to: Date())
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: locales)
        dateFormatter.dateFormat = formatter
       return dateFormatter.string(from: day ?? Date())
    }

    class func getLastDayMonth(formatter: String, _ locales: String = Locales.es_MX) -> String {
        var result = ""
        let components = Calendar.current.dateComponents([.year, .month], from: Date())
        let startOfMonth = Calendar.current.date(from: components)!
        let finishDate = Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: startOfMonth)
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: locales)
        dateFormatter.dateFormat = formatter
        result = dateFormatter.string(from: finishDate ?? Date())
        return result
    }

    class func getFirstDayMonthPass(month: Int, formatter: String, _ locales: String = Locales.es_MX) -> String {
        let firstDay = getFirstDayMonth(formatter: formatter)
        let daytoDate = convertStringDate(date: firstDay, formatter: formatter)
        let lastMonth = futureDate(date: daytoDate ?? Date(), value: month, component: .month)
        return convertDateString(date: lastMonth ?? Date(), formatter: formatter)
    }

    class func getLastDayMonthPass(month: Int, formatter: String, _ locales: String = Locales.es_MX) -> String {
        let lastDay = getLastDayMonth(formatter: formatter)
        let daytoDate = convertStringDate(date: lastDay, formatter: formatter)
        let lastMonth = futureDate(date: daytoDate ?? Date(), value: month, component: .month)
        return convertDateString(date: lastMonth ?? Date(), formatter: formatter)
    }

}
