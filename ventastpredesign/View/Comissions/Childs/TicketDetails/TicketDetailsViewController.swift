//
//  TicketDetailsViewController.swift
//  ventastp
//
//  Created by BranchBit on 22/10/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class TicketDetailsViewController: BaseVentasView {

    // MARK: variables
    @IBOutlet weak var accountLabel: UILabel!
    @IBOutlet weak var folioLabel: UILabel!
    @IBOutlet weak var estatusLabel: UILabel!
    var comissionsDetailsViewModel: ComissionsDetailsViewModel!
    var accountData = ""
    var nameData = ""
    var surNameData = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
    }

    @IBAction func actionBack(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }

    @IBAction func sendTicketAction(_ sender: Any) {
    }

}
