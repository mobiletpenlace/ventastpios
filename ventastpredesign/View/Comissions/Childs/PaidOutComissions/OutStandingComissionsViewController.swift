//
//  OutStandingComissionsViewController.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 23/09/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//
import UIKit
import SwiftEventBus

class OutStandingComissionsViewController: BaseVentasView {

    @IBOutlet weak var TitleS: UILabel!
    @IBOutlet weak var ScrollView: UIScrollView!
    @IBOutlet weak var FechaInicio: UILabel!
    @IBOutlet weak var FechaFin: UILabel!
    var Cuenta: String = ""
    var fechaFL = ""
    var fechaFI = ""
    var arrCuenta: [String] = []
    var arrCuentaSinPagar: [String] = []
    var contar = 0
    var suma = 0.0
    var numS = 0
    var mDetalleFacturaPresenter: DetalleFacturaPresenter!
    var mFacturasNoPagadasPresenter: FacturasNoPagadasPresenter!
    let employed: Empleado = Empleado(value: RealManager.findFirst(object: Empleado.self)!)
    var fechaInicioString = ""
    var fechaFinString = ""
    var option = 1

    @IBOutlet weak var FPeriodo: UILabel!
    @IBOutlet weak var FSemana1: UILabel!
    @IBOutlet weak var FSemana2: UILabel!
    @IBOutlet weak var FVariable: UILabel!
    @IBOutlet weak var ViewComision: UIView!
    // viewModel
    var mOutStandingComissionsViewModel: OutStandingComissionsViewModel!
    var detalles: DetalleFactura!
    var comissionsDetailsViewController: ComissionsDetailsViewController!

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mOutStandingComissionsViewModel = OutStandingComissionsViewModel(view: self)
        mOutStandingComissionsViewModel.atachView(observer: self)
    }

    override func viewDidAppear(_ animated: Bool) {
        comissiones()
       // comissionesProdMin()
    }

    func comissiones() {
        let request = GetFacturasNoPagadasRequest()
        request.numEmpleado = faseComisiones.numEmpleado
        request.fh_inicio = faseComisiones.fechaInicio
        request.fh_fin = faseComisiones.fechaFin
       // request.fh_inicio = "2020-09-01"
       // request.fh_fin = "2020-09-30"
        request.tipo_consulta = "5"
        request.ai_cuenta = "012340212"
        mOutStandingComissionsViewModel.comissions(request: request)
    }

    func comissionesProdMin() {
        let request = GetProductividadMinimaRequest()
        request.numEmpleado = faseComisiones.numEmpleado
        request.fh_inicio = faseComisiones.fechaInicio
        request.fh_fin = faseComisiones.fechaFin
        // request.fh_inicio = "2020-09-01"
        // request.fh_fin = "2020-09-30"
        request.tipo_consulta = "5"
       // request.ai_cuenta = "012340212"
        mOutStandingComissionsViewModel.comissions2(request: request)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func regresemos () {
        self.removeFromParentViewController()
        self.view.removeFromSuperview()
    }
}

/*
//
//  Consult2ComissionsViewController.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 31/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import BaseClases

class Consult2ComissionsViewController: BaseVentasView {
   
    
    @IBOutlet weak var TitleS: UILabel!
    @IBOutlet weak var TotalLabel: UILabel!
    @IBOutlet weak var ScrollView: UIScrollView!
    @IBOutlet weak var FechaInicio: UILabel!
    @IBOutlet weak var FechaFin: UILabel!
    var fechaFL = ""
    var fechaFI = ""
    var arrCuenta:[String] = []
    var arrCuentaSinPagar:[String] = []
    var contar = 0
    var suma = 0.0
    var numS = 0
    var mDetalleFacturaPresenter: DetalleFacturaPresenter!
    var mFacturasNoPagadasPresenter: FacturasNoPagadasPresenter!
    let employed : Empleado = Empleado(value: RealManager.findFirst(object: Empleado.self)!)
    var fechaInicioString = ""
    var fechaFinString = ""
    var option = 1
    var mConsult2ComissionsViewModel: Consult2ComissionsViewModel!
    
    @IBOutlet weak var FPeriodo: UILabel!
    @IBOutlet weak var FSemana1: UILabel!
    @IBOutlet weak var FSemana2: UILabel!
    @IBOutlet weak var FVariable: UILabel!
    @IBOutlet weak var ViewComision: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mConsult2ComissionsViewModel = Consult2ComissionsViewModel(view: self)
        mConsult2ComissionsViewModel.atachView(observer: self)
        consult2Comissions()
        FechaInicio.text = fechaFI
        FechaFin.text = fechaFL
        if option != 1 {
            FechaFin.isHidden = true
            FechaInicio.isHidden = true
            FPeriodo.isHidden = true
            FSemana1.isHidden = true
            FSemana2.isHidden = true
            FVariable.isHidden = true
        }
        

        loadService(num: numS)
    }
    
    func consult2Comissions(){
        var request = SellerInvoicesOutStandingRequest()
        request.numEmpleado = "65019940"
        request.fh_inicio = "2020/01/01"
        request.fh_fin = "2020/01/01"
        request.tipo_consulta = "1"
        request.ai_cuenta = "012340212"
        mConsult2ComissionsViewModel.outStandingComissions(request: request)
        //mComissionsViewModel.OnSuccessComisionesVendedorDelegate()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func getPresenters() -> [BasePresenter]? {
        mDetalleFacturaPresenter = DetalleFacturaPresenter(view: self)
        mFacturasNoPagadasPresenter = FacturasNoPagadasPresenter(view: self)
        return [mDetalleFacturaPresenter, mFacturasNoPagadasPresenter]
    }
    
    @IBAction func Actionback(_ sender: Any) {
        self.removeFromParentViewController()
        self.view.removeFromSuperview()
    }
    
    
    @objc func ActionCuenta(sender: UIButton){
        let viewAlert: UIStoryboard = UIStoryboard(name: "CuentaViewController", bundle:nil)
        let viewAlertVC = viewAlert.instantiateViewController(withIdentifier: "CuentaViewController")as! CuentaViewController
        viewAlertVC.numOption = 2
        viewAlertVC.cuentaS = arrCuenta[sender.tag]
        viewAlertVC.inicio = (FechaInicio.text)!
        viewAlertVC.final = (FechaFin.text)!
        viewAlertVC.option = 1
        self.addChildViewController(viewAlertVC)
        self.view.addSubview(viewAlertVC.view)
        //  self.present(viewAlertVC, animated: false, completion: {})
    }
    
    @objc func ActionCuentaPendientes(sender: UIButton){
        let viewAlert: UIStoryboard = UIStoryboard(name: "CuentaViewController", bundle:nil)
        let viewAlertVC = viewAlert.instantiateViewController(withIdentifier: "CuentaViewController")as! CuentaViewController
       // print (arrCuentaSinPagar[sender.tag])
        viewAlertVC.numOption = 1
        viewAlertVC.inicio = (FechaInicio.text)!
        viewAlertVC.final = (FechaFin.text)!
        viewAlertVC.option = option
        viewAlertVC.cuentaS = arrCuentaSinPagar[sender.tag] 
        self.addChildViewController(viewAlertVC)
        self.view.addSubview(viewAlertVC.view)
        // self.present(viewAlertVC, animated: false, completion: {})
    }
    
    func loadService(num: Int){
        if  (num == 1){
            TitleS.text = "Consulta de comisiones por pagar"
            
            mFacturasNoPagadasPresenter.GetFacturasNoPagadas(mEmpleado: Security.crypt(text: employed.FolioEmpleado), mFechaFin: Security.crypt(text: fechaFinString), mFechaInicio: Security.crypt(text: fechaInicioString), mFacturasNoPagadasDelegate: self as! FacturasNoPagadasDelegate)
        }else  if  (num == 2){
             TitleS.text = "Consulta de comisiones"
            mDetalleFacturaPresenter.GetDetalleFactura(mEmpleado: Security.crypt(text: employed.FolioEmpleado), mFechaFin: Security.crypt(text: fechaFinString), mFechaInicio: Security.crypt(text: fechaInicioString), mDetalleFacturaDelegate: self as! DetalleFacturaDelegate)
        }
    }
    
    func delete(){
        let subViews = self.ScrollView.subviews
        for subview in subViews{
            subview.removeFromSuperview()
        }
    }
    
    
}
*/
