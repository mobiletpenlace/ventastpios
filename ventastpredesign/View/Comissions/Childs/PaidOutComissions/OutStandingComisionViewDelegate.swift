//
//  ComissionsVCDelegate.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 30/09/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//
import Foundation
import UIKit
import BaseClases
import SwiftEventBus

extension OutStandingComissionsViewController: OutStandingComissionsObserver, UIScrollViewDelegate {
    func ConsultaDetallesPendientes(cuenta: String) {
       // cuentaView = cuenta

    }

    func backView() {
        eliminarVista()
    }

    func eliminarVista() {
        // comissionsDetailsViewController.regresemosDeDetalles()
    }

    func succesOutStandingComissions(response: ComisionesObjeto) {
        llenaScroll(arreglo: mOutStandingComissionsViewModel.getComisionesPendientes(), identifier: "CuentaCollectionReusableView", anchoCelda: CGFloat(320), altoCelda: CGFloat(33), anchoScroll: CGFloat(320), altoScroll: CGFloat(33), scroll: ScrollView)

    }

    func succesProdMinComissions(response: ComisionesObjeto) {
        llenaScroll(arreglo: mOutStandingComissionsViewModel.getComisionesProdMin(), identifier: "CuentaCollectionReusableView", anchoCelda: CGFloat(320), altoCelda: CGFloat(33), anchoScroll: CGFloat(320), altoScroll: CGFloat(33), scroll: ScrollView)

    }

    func llenaScroll(arreglo: [NSObject], identifier: String, anchoCelda: CGFloat, altoCelda: CGFloat, anchoScroll: CGFloat, altoScroll: CGFloat, scroll: UIScrollView) {
        var contador = 0
        contador =  arreglo.count
        scroll.contentSize = CGSize(width: anchoCelda, height: CGFloat(altoCelda) * CGFloat(contador))
        for index in 0 ..< contador {
            if let view: CuentaCollectionReusableView = Bundle.main.loadNibNamed(identifier, owner: self, options: nil)?.first as? CuentaCollectionReusableView {
                view.setData(arreglo[index])
                scroll.addSubview(view)
                view.frame.size.height = CGFloat(altoCelda)
                view.frame.size.width = CGFloat(anchoCelda)
                view.frame.origin.y = CGFloat(index) * CGFloat(altoCelda)
            }
        }
    }

    func onError(errorMessage: String) {
        Logger.println("Error")
    }
}
