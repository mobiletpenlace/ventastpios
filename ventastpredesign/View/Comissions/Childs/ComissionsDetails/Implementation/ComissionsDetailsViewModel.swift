//
//  ComissionsDetailsViewModel.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 19/10/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus

protocol ComissionsDetailsObserver {
    func showFields()
    func hideFields()
    func successPaidOutComissionsDetails (response: DetalleFactura)
    func sinDatos()
    func consultaDetallesComisiones(cuenta: String)
    func consultaDetallesTicket(folio: String)
}

class ComissionsDetailsViewModel: BaseViewModel, ComissionsDetailsModelObserver {

    var observer: ComissionsDetailsObserver?
    var model: ComissionsDetailsModel!
    var detallesCuenta: DetalleFactura?
    var mComisiones: [ListCuentaComisionVO] = []

    func sinDatos() {
        observer?.sinDatos()
        view.hideLoading()
    }

    func succesPaidOutComissionsDetails(response: ComissionsPaidOutDetailsResponse) {
        detallesCuenta = DetalleFactura()
        detallesCuenta = response.DetalleFactura!
        observer?.successPaidOutComissionsDetails(response: detallesCuenta!)
        view.hideLoading()
    }

    override init(view: BaseVentasView) {
        super.init(view: view)
        model = ComissionsDetailsModel(observer: self)
        model.atachModel(viewModel: self)
    }

    func atachView(observer: ComissionsDetailsObserver) {
        self.observer = observer

            SwiftEventBus.onMainThread(self, name: "pasar_cuenta") { [weak self] result in
             let cuenta = result?.object as? String
                self?.observer?.consultaDetallesComisiones(cuenta: cuenta!)
            }

        SwiftEventBus.onMainThread(self, name: "pasar_folio") { [weak self] result in
         let cuenta = result?.object as? String
            self?.observer?.consultaDetallesTicket(folio: cuenta!)
        }
    }

    func viewWillDissapear() {
        SwiftEventBus.unregister(self)
    }

    func pasarCuenta(cuenta: String) {
        print(cuenta)

    }
    func comissionsDetails(request: ComissionsPaidOutDetailsRequest) {
       // view.showLoading()
        view.hideLoading()
        model.doGetDetailsComisiones(request: request)
    }

    func ticketDetails(request: DetailsTicketRequest) {
       // view.showLoading()
        view.hideLoading()
        model.doGetDetailsTicket(request: request)
    }

    func onError(errorMessage: String) {
        view.hideLoading()
        Logger.println(errorMessage)
    }

}
