//
//  ComissionsDetailsModel.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 19/10/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import SwiftEventBus

protocol ComissionsDetailsModelObserver {
    func succesPaidOutComissionsDetails (response: ComissionsPaidOutDetailsResponse)
//    func succesOutStangingComissionsDetails (response: ComissionsPaidOutDetailsResponse)
//    func succesProdMinComissionsDetails (response: ComissionsMinimumProductivityResponse)
    func sinDatos()
    func pasarCuenta (cuenta: String)
    func onError(errorMessage: String)// On error func
}

class ComissionsDetailsModel: BaseModel {
    var viewModel: ComissionsDetailsModelObserver?
    var server: ServerDataManager2<ComissionsPaidOutDetailsResponse>?
    var server2: ServerDataManager2<DetailsTicketResponse>?

    var urlPaidOutComissionsDetailsResponse = AppDelegate.API_TOTAL + ApiDefinition.API_DETAIL_FACT
//    var urlOutStangingComissionsResponse = AppDelegate.API_TOTAL_TEST + ApiDefinition.API_DETAIL_FACT_PRODMIN
//
    init (observer: BaseViewModelObserver) {
        super.init(observerVM: observer)
        // Se declara un server para SellerComissionsResponse
        server = ServerDataManager2<ComissionsPaidOutDetailsResponse>()
    }
    func atachModel(viewModel: ComissionsDetailsModelObserver) {
        self.viewModel = viewModel
    }

    func doGetDetailsComisiones(request: ComissionsPaidOutDetailsRequest) {
        let urlComissionsDetailsResponse = AppDelegate.API_TOTAL + ApiDefinition.API_DETAIL_FACT
        server?.setValues(requestUrl: urlComissionsDetailsResponse, delegate: self, headerType: .headersFfmap)
        server?.request(requestModel: request)
    }

    func doGetDetailsTicket(request: DetailsTicketRequest) {
        let urlComissionsDetailsResponse = AppDelegate.API_TOTAL + ApiDefinition.API_DETAIL_FACT
        server?.setValues(requestUrl: urlComissionsDetailsResponse, delegate: self, headerType: .headersFfmap)
        server?.request(requestModel: request)
    }

    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {

        let comissionsPaidOutDetailsResponse = response as! ComissionsPaidOutDetailsResponse
    if comissionsPaidOutDetailsResponse.Response?.Result == "0" {
        viewModel?.succesPaidOutComissionsDetails(response: response as! ComissionsPaidOutDetailsResponse)
    } else if comissionsPaidOutDetailsResponse.Response?.Result == "-1" {
//        viewModel?.onError(errorMessage: comissionsPaidOutDetailsResponse.Response?.Result ?? "Favor de validar de nuevo")
        viewModel?.sinDatos()
    } else {
        Logger.println("Error en la ejecucion")
    }

//            let paidOutComissionsDetailsResponse = response as! ComissionsPaidOutDetailsResponse
//          //  let prodMinComissionsDetailsResponse = response as! ComissionsMinimumProductivityResponse
//
////        if requestUrl == urlPaidOutComissionsDetailsResponse{
//            if(paidOutComissionsDetailsResponse.Response?.Result == "0"){
//                viewModel?.succesPaidOutComissionsDetails(response: response as! ComissionsPaidOutDetailsResponse)
//            }
//            else{
//                viewModel?.onError(errorMessage: paidOutComissionsDetailsResponse.Response?.Result ?? "Favor de validar de nuevo")
//            }
////        }
////
////        if requestUrl == urlOutStangingComissionsResponse{
//////            if(prodMinComissionsDetailsResponse.Response?.Result == "0"){
//////                viewModel?.succesProdMinComissionsDetails(response: response as! ComissionsMinimumProductivityResponse)
//////            }else if(prodMinComissionsDetailsResponse.Response?.Result == "-1"){
//////                viewModel?.onError(errorMessage: prodMinComissionsDetailsResponse.Response?.Result ?? "Favor de validar de nuevo")
//////            }
//////            else{
//////                Logger.println("Error en la ejecucion")
//////            }
////        }

    }

    override func onErrorLoadResponse(requestUrl: String, messageError: String) {
        viewModel?.onError(errorMessage: messageError)
    }

}
