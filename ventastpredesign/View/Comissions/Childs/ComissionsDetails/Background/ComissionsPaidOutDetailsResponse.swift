//
//  ComissionsPaidOutDetailsResponse.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 19/10/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import Foundation
import ObjectMapper

class ComissionsPaidOutDetailsResponse: BaseResponse {
    required init? (map: Map) {

    }

    override func mapping(map: Map) {
        Response <- map["Response"]
        DetalleFactura <- map ["DetalleFactura"]
    }

    var Response: Response?
    var DetalleFactura: DetalleFactura?
}
