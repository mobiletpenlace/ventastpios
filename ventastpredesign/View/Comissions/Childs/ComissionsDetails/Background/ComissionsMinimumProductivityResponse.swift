//
//  ComissionsMinimumProductivityResponse.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 2/12/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import ObjectMapper

class ComissionsMinimumProductivityResponse: BaseResponse {
    required init? (map: Map) {

    }

    override func mapping(map: Map) {
        Response <- map["Response"]
        DetalleFactura <- map ["DetalleFactura"]
    }

    var Response: Response?
    var DetalleFactura: DetalleFactura?
}
