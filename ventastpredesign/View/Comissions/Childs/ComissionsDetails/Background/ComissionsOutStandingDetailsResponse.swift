//
//  ComissionsOutStandingDetailsResponse.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 2/12/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import ObjectMapper

class ComissionsOutStandingDetailsResponse: BaseResponse {
    required init? (map: Map) {

    }

    override func mapping(map: Map) {
        Response <- map["Response"]
        Comisiones <- map ["DetalleFactura"]
    }

    var Response: Response?
    var Comisiones: Comisiones?
}
