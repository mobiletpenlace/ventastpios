//
//  ComissionsPaidOutDetailsRequest.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 19/10/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import Foundation

import ObjectMapper

public class ComissionsPaidOutDetailsRequest: BaseRequest {

    var numEmpleado: String?
    var fh_inicio: String?
    var fh_fin: String?
    var tipo_consulta: String?
    var ai_cuenta: String?

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override public func mapping(map: Map) {
        numEmpleado <- map["numEmpleado"]
        fh_inicio <- map["fh_inicio"]
        fh_fin <- map["fh_fin"]
        tipo_consulta <- map["tipo_consulta"]
        ai_cuenta <- map["ai_cuenta"]
    }

}
