//
//  CuentaViewController.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 31/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class ComissionsDetailsViewController: BaseVentasView {

    @IBOutlet weak var ViewCuenta: UIView!
    @IBOutlet weak var cuentaLabel: UILabel!
    @IBOutlet weak var clienteNameLabel: UILabel!
    @IBOutlet weak var clienteApellidoLabel: UILabel!
    @IBOutlet weak var fechaInstaladaLabel: UILabel!
    @IBOutlet weak var primerPagoLabel: UILabel!
    @IBOutlet weak var segundoPagoLabel: UILabel!
    @IBOutlet weak var fechaPrimerPagoVendedorLabel: UILabel!
    @IBOutlet weak var fechaSegundoPagoVendedorLabel: UILabel!
    @IBOutlet weak var fechaPrimerPagoClienteLabel: UILabel!
    @IBOutlet weak var fechaSegundoPagoClienteLabel: UILabel!
    @IBOutlet weak var separatorTopConstraint: NSLayoutConstraint!
    @IBOutlet var collection: [UILabel]!
    @IBOutlet weak var packageLabel: UILabel!
    @IBOutlet weak var addonsLabel: UILabel!
    var comissionsDetailsViewModel: ComissionsDetailsViewModel!
    var accountData = ""
    var nameData = ""
    var surNameData = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        comissionsDetailsViewModel = ComissionsDetailsViewModel(view: self)
        comissionsDetailsViewModel.atachView(observer: self)
        SwiftEventBus.onMainThread(self, name: "aclaracionexitosa") { [weak self] _ in
            self!.dismiss(animated: true, completion: nil)
        }
    }

    @IBAction func actionBack(_ sender: Any) {
        dismiss(animated: false, completion: nil)
        comissionsDetailsViewModel.viewWillDissapear()
    }

    @IBAction func sendTicketAction(_ sender: Any) {
        accountData = cuentaLabel.text!
        nameData = clienteNameLabel.text! + "\n" + clienteApellidoLabel.text!
        Constants.LoadStoryBoard(name: storyBoard.Ticket.SendTicketInfo, viewC: self)
        SwiftEventBus.post("EnviardatosaTicketInfoAccount", sender: accountData)
        SwiftEventBus.post("EnviardatosaTicketInfoName", sender: nameData)
    }
}
