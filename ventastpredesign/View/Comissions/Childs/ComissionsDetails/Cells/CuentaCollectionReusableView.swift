//
//  CuentaCollectionReusableView.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 31/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class CuentaCollectionReusableView: BaseCollectionViewCell {

    @IBOutlet weak var m1Factura: UILabel!
    @IBOutlet weak var mCuenta: UILabel!
    @IBOutlet weak var mButtonCuenta: UIButton!
    var listaArrCuentas: ListCuentaComisionVO!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setData(_ arreglo: NSObject) {
        listaArrCuentas = arreglo as? ListCuentaComisionVO
        m1Factura.text = Double(listaArrCuentas.pago1 ?? "00.00")?.asLocaleCurrency
//        mCuenta.font = mCuenta.font.withSize(mCuenta.frame.height * 1/3)
//        print(mCuenta.frame.height)
//        mCuenta.adjustsFontForContentSizeCategory = true
        switch listaArrCuentas.cuenta {
        case "0000000001":
            mCuenta.text = "Bono garantía"
        case "0000000002":
            mCuenta.text = "Bono planta"
        case "0000000003":
            mCuenta.text = "Ajustes"
        case "0000000004":
            mCuenta.text = "Bono adiciones netas"
        case "0000000005":
            mCuenta.text = "Bono penetración"
        case "0000000006":
            mCuenta.text = "Rally"
        case "0000000007":
            mCuenta.text = "Bono semestral"
        case "0000000008":
            mCuenta.text = "Pendiente Chargeback"
        default:
            mCuenta.text = listaArrCuentas.cuenta
        }
    }

    @IBAction func botonCuenta(_ sender: Any) {
        SwiftEventBus.post("Consulta_Detalles_Comission", sender: listaArrCuentas.cuenta)
    }
}
