//
//  ChargeBackCollectionViewCell.swift
//  ventastp
//
//  Created by Armando Isais Olguin Cabrera on 9/9/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class TicketViewCollectionViewCell: BaseCollectionViewCell {

    @IBOutlet weak var mCuenta: UILabel!
    @IBOutlet weak var mFolio: UILabel!
    @IBOutlet weak var mStatus: UILabel!

    var listaArrAclaraciones: Aclaraciones!

    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func setData(_ arreglo: NSObject) {
        listaArrAclaraciones = arreglo as? Aclaraciones
        mCuenta.text = listaArrAclaraciones.cd_cuenta
        mFolio.text = listaArrAclaraciones.nb_folio_aclaracion

        switch listaArrAclaraciones.estatus {
        case  0:
            mStatus.text = "Pendiente"
        case 1:
            mStatus.text = "Aceptado"
            mStatus.textColor = UIColor.red
        case 2:
            mStatus.text = "Rechazado"
            mStatus.textColor = UIColor.blue
        default:
            print("default")
        }

    }

    @IBAction func botonTicket(_ sender: Any) {
        SwiftEventBus.post("Consulta_Detalles_Ticket", sender: listaArrAclaraciones.cd_cuenta)
    }

}
