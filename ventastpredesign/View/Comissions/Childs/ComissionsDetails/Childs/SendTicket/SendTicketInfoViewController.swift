//
//  SendTicketInfoViewController.swift
//  ventastp
//
//  Created by BranchBit on 15/10/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus
import MaterialComponents.MDCFilledTextField
import UIKit
import Firebase

class SendTicketInfoViewController: BaseVentasView, SendTicketInfoObserver {

    func successPaidOutComissionsDetails(response: DetalleFactura) {
    }

    @IBOutlet weak var MotivoTextField: MDCFilledTextField!
    @IBOutlet weak var SendTicketButton: UIButton!
    @IBOutlet weak var SendTicketView: UIView!
    @IBOutlet weak var AccountLabel: UILabel!
    @IBOutlet weak var NameLabel: UILabel!

    // Default motives array
    var arrMotivos: [String] = ["FALTA DE PAGO POR CUENTA COMPLETA",
                                "FALTA DE PAGO POR CUENTA POR FACTURA 1",
                                "FALTA DE PAGO POR CUENTA POR FACTURA 1",
                                "CUENTA NO CONSIDERADA EN RALLY",
                                "CUENTAS NO ASIGNADAS POR OTRA POSICIÓN",
                                "FALTAN CUENTAS",
                                "COMISIÓN NO DEPOSITADA"]

    var mSendTicketInfoViewModel: SendTicketInfoViewModel!

    override func viewDidLoad() {
        MotivoTextField.setFilledBackgroundColor(UIColor.clear, for: .normal)
        MotivoTextField.setFilledBackgroundColor(UIColor.clear, for: .normal)
        MotivoTextField.setFilledBackgroundColor(UIColor.clear, for: .normal)
//        print(UIDevice.current.identifierForVendor?.uuidString)
        super.viewDidLoad()
        super.setView(view: self.view)

//        getMotivesList()

        mSendTicketInfoViewModel = SendTicketInfoViewModel(view: self)
        mSendTicketInfoViewModel.atachView(observer: self)

        SwiftEventBus.onMainThread(self, name: "Set_TipoPersonaPicker") { [weak self] result in
            if let value = result?.object as? String {
                self?.setTipoPersona(tipo: value)
            }
        }

        SwiftEventBus.onMainThread(self, name: "ShowSuccessSend") { [weak self] _ in
            self!.showToast2(message: "Aclaración enviada exitosamente", font: UIFont(name: "Helvetica Neue", size: 11)!, time: 4.0)
            self!.dismiss(animated: true, completion: nil)
            SwiftEventBus.post("aclaracionexitosa", sender: nil)
        }

        SwiftEventBus.onMainThread(self, name: "ShowAlert") { [weak self] result in
            if let value = result?.object as? String {
                AlertDialog.show(title: "Alerta", body: value, view: self!)
            }
        }

        SwiftEventBus.onMainThread(self, name: "EnviardatosaTicketInfoAccount") { [weak self] result in
            self?.AccountLabel.text = result?.object as? String
        }

        SwiftEventBus.onMainThread(self, name: "EnviardatosaTicketInfoName") { [weak self] result in
            self?.NameLabel.text = result?.object as? String
        }

        SendTicketButton.isEnabled = false
        SendTicketView.backgroundColor = UIColor(named: "Base_titles_gray_medium")
    }

    func setLabels(accountData: String) {
        NameLabel.text = accountData[0]
        AccountLabel.text = accountData[1]
    }

    override func viewWillDisappear(_ animated: Bool) {
    }

    func getMotivesList() {

//        guard let secondary = FirebaseApp.app(name: "secondary") else {assert(false, "Could not retrieve secondary app")}
//
//        //Get db reference
//        let db = Firestore.firestore(app: secondary)
//
//        //Get data from collection
//        db.collection("VSB").document("01020304").getDocument { [weak self] snapshot, error in
//            //Get strog self
//            guard let self = self else {return}
//
//            //Check error
//            if let error = error {
//                print("Error getting documents: \(error)")
//
//            }else{
//                guard let snapshot = snapshot else{return}
//                self.arrMotivos = snapshot["motivoAclaracion"] as? [String] ?? []
//            }
//        }
    }

    @IBAction func actionBack(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }

    @IBAction func optionsMotivo(_ sender: Any) {

        Constants.OptionCombo(origen: "Set_TipoPersona", array: Array(arrMotivos), viewC: self)
    }

    @IBAction func sendTicketAction(_ sender: Any) {
        Logger.println("Enviar Aclaracion presionado")
        createTicketRequest()
    }

    func setTipoPersona(tipo: String) {
        MotivoTextField.text = tipo
        if MotivoTextField.label.text?.contains(string: "Motivo") == true {
            SendTicketButton.isEnabled = true
            SendTicketView.backgroundColor = UIColor(named: "Base_rede_button_dark")
        } else {
            SendTicketButton.isEnabled = false
            SendTicketView.backgroundColor = UIColor(named: "Base_titles_gray_medium")
        }
    }

    func createTicketRequest() {
        let request = CreateTicketRequest()
        request.numEmpleado = "65052309"// faseComisiones.numEmpleado
        request.cd_cuenta = "0190003622"// AccountLabel.text
        request.tipo_aclaracion = "1"
        request.imei_creacion = "12548685/8745/1"// UIDevice.current.identifierForVendor?.uuidString

        mSendTicketInfoViewModel.ticket(request: request)
    }

}
