//
//  SendTicketInfoModel.swift
//  ventastp
//
//  Created by BranchBit on 18/10/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import SwiftEventBus

protocol SendTicketInfoModelObserver {
    func succesCreateTicket(response: CreateTicketResponse)
    func onError(errorMessage: String)
}

class SendTicketInfoModel: BaseModel {
    var viewModel: SendTicketInfoModelObserver?
    var server: ServerDataManager2<CreateTicketResponse>?
    var urlSendTicketInfoResponse = AppDelegate.API_TOTAL + ApiDefinition.API_CREATE_TICKET

    init (observer: BaseViewModelObserver) {
        super.init(observerVM: observer)
        server = ServerDataManager2<CreateTicketResponse>()
    }
    func atachModel(viewModel: SendTicketInfoModelObserver) {
        self.viewModel = viewModel
    }

    func doGetSendtTicketInfo(request: CreateTicketRequest) {
        let urlSendTicketInfoResponse = AppDelegate.API_TOTAL + ApiDefinition.API_CREATE_TICKET
        server?.setValues(requestUrl: urlSendTicketInfoResponse, delegate: self, headerType: .headersFfmap)
        server?.request(requestModel: request)
    }

    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        let createTicketResponse = response as! CreateTicketResponse

        switch createTicketResponse.Result {
        case "20001"?:
            viewModel?.succesCreateTicket(response: response as! CreateTicketResponse)
            SwiftEventBus.post("ShowSuccessSend", sender: "Aclaracion enviada exitosamente")
        case "200"?:
            SwiftEventBus.post("ShowAlert", sender: "Ya existe esta aclaracion")
            viewModel?.onError(errorMessage: "Ya existe una aclaración para esta cuenta")
        default:
            Logger.println("Error en la ejecucion")
            break
        }
    }

    override func onErrorLoadResponse(requestUrl: String, messageError: String) {
        viewModel?.onError(errorMessage: messageError)
    }

}
