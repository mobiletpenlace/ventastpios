//
//  SendTicketInfoViewModel.swift
//  ventastp
//
//  Created by BranchBit on 18/10/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus
import UIKit

protocol SendTicketInfoObserver {
    func successPaidOutComissionsDetails (response: DetalleFactura)
}

class SendTicketInfoViewModel: BaseViewModel, SendTicketInfoModelObserver {

    func ticket(request: CreateTicketRequest) {
        view.showLoading(message: "")
        model.doGetSendtTicketInfo(request: request)
    }

    func succesCreateTicket(response: CreateTicketResponse) {
        view.hideLoading()
    }

    var observer: SendTicketInfoObserver?
    var model: SendTicketInfoModel!
    var detallesCuenta: DetalleFactura?
    var mComisiones: [ListCuentaComisionVO] = []

    func succesPaidOutComissionsDetails(response: ComissionsPaidOutDetailsResponse) {
        detallesCuenta = DetalleFactura()
        detallesCuenta = response.DetalleFactura!
        observer?.successPaidOutComissionsDetails(response: detallesCuenta!)
        view.hideLoading()
    }

    override init(view: BaseVentasView) {
        super.init(view: view)
        model = SendTicketInfoModel(observer: self)
        model.atachModel(viewModel: self)
    }

    func atachView(observer: SendTicketInfoObserver) {
        self.observer = observer

//            SwiftEventBus.onMainThread(self, name: "pasar_cuenta") { [weak self] result in
//             let folio = result?.object as? String
//                self?.observer?.consultaDetallesTicket(folio: folio!) }
    }

    func viewWillDissapear() {
        SwiftEventBus.unregister(self)
    }

    func onError(errorMessage: String) {
        view.hideLoading()
    }

}
