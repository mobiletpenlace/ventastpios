//
//  CuentaViewDelegate.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 27/03/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//
import Foundation
import SwiftEventBus

extension ComissionsDetailsViewController: ComissionsDetailsObserver {

//    func successOutStandingComissionsDetails(response: DetalleFactura) {
//        showFields()
//        cuentaLabel.text = response.cuenta
//
//        var components = String(response.cliente!).components(separatedBy: " ")
//           if components.count > 0 {
//            let firstName = components.removeFirst()
//            let lastName = components.joined(separator: " ")
//            print(firstName)
//            print(lastName)
//            clienteNameLabel.text = firstName.capitalized
//            clienteApellidoLabel.text = lastName.capitalized
//           }
//
//        fechaInstaladaLabel.text = response.fechaInst
//        primerPagoLabel.text = "$ " + response.pago1!
//        segundoPagoLabel.text = "$ " + response.pago2!
//        fechaPrimerPagoVendedorLabel.text = response.fechaVend1
//        fechaSegundoPagoVendedorLabel.text = response.fechaVend2
//        fechaPrimerPagoClienteLabel.text = response.fechaClient1
//        fechaSegundoPagoClienteLabel.text = response.fechaCliente2
//    }
//
//    func successProdMinComissionsDetails(response: DetalleFactura) {
//        hideFields()
//        cuentaLabel.text = response.cuenta
//        clienteNameLabel.text = response.cliente
//        clienteApellidoLabel.text = response.cliente
//        fechaInstaladaLabel.text = response.fechaInst
//        primerPagoLabel.text = "$ " + response.pago1!
//        segundoPagoLabel.text = "$ " + response.pago2!
//        fechaPrimerPagoVendedorLabel.text = response.fechaVend1
//        fechaSegundoPagoVendedorLabel.text = response.fechaVend2
//        fechaPrimerPagoClienteLabel.text = response.fechaClient1
//        fechaSegundoPagoClienteLabel.text = response.fechaCliente2
//    }

    func sinDatos() {
            cuentaLabel.text = "Sin información"
            clienteNameLabel.text = "Sin información"
            fechaInstaladaLabel.text = "Sin información"
            primerPagoLabel.text = "Sin información"
            segundoPagoLabel.text = "Sin información"
            fechaPrimerPagoVendedorLabel.text = "Sin información"
            fechaSegundoPagoVendedorLabel.text = "Sin información"
            fechaPrimerPagoClienteLabel.text = "Sin información"
            fechaSegundoPagoClienteLabel.text = "Sin información"
            packageLabel.text = "Sin información"
            addonsLabel.text = "Sin información"
        }

    func successPaidOutComissionsDetails(response: DetalleFactura) {
        switch faseComisiones.idFlujo {
        case 1:
            showFields()
        case 2:
            showFields()
        case 3:
            hideFields()
        default:
            break
        }
        cuentaLabel.text = response.cuenta

        let components = String(response.cliente ?? "sin nombre sin apellido").components(separatedBy: " ").map {$0.capitalized}

        if components.count == 3 {
            let name = components.dropLast()
            let lastName = components.dropFirst()
            clienteNameLabel.text = name[0]
            clienteApellidoLabel.text = lastName.joined(separator: " ")
        } else {
            let name = components.dropLast(2)
            let lastName = components.dropFirst(2)

            clienteNameLabel.text = name.joined(separator: " ")
            clienteApellidoLabel.text = lastName.joined(separator: " ")
        }
        fechaInstaladaLabel.text = response.fechaInst ?? "Sin información"
        primerPagoLabel.text = Double(response.pago1 ?? "00.00")?.asLocaleCurrency
        segundoPagoLabel.text = Double(response.pago2 ?? "00.00")?.asLocaleCurrency
        fechaPrimerPagoVendedorLabel.text = response.fechaVend1 ?? "Sin información"
        fechaPrimerPagoClienteLabel.text = response.fechaClient1 ?? "Sin información"
        packageLabel.text = response.paquete ?? "Sin información"
        addonsLabel.text = "Sin Addons"
        if response.fechaVend2  == "null" {
            fechaSegundoPagoVendedorLabel.text = "Sin información"
        } else {
            fechaSegundoPagoVendedorLabel.text = response.fechaVend2
        }
        if response.fechaCliente2  == "null" {
            fechaSegundoPagoClienteLabel.text = "Sin información"
        } else {
            fechaSegundoPagoClienteLabel.text = response.fechaCliente2
        }
    }

    func consultaDetallesComisiones(cuenta: String) {
        let request = ComissionsPaidOutDetailsRequest()
        request.numEmpleado = faseComisiones.numEmpleado// "65054431"
        request.fh_inicio = "1" // "2020/01/01"
        request.fh_fin = "1" // "2020/01/01"
        request.tipo_consulta = String(faseComisiones.idFlujo)
        request.ai_cuenta = cuenta
        comissionsDetailsViewModel.comissionsDetails(request: request)
    }

    func consultaDetallesTicket(folio: String) {
        print(folio)
        let request = DetailsTicketRequest()
        request.folio_in = folio
        comissionsDetailsViewModel.ticketDetails(request: request)
    }

    func showFields() {
        for i in 0...33 {
            collection[i].isHidden = false
        }
    }

    func hideFields() {
        for i in 0...33 {
            collection[i].isHidden = true
        }
        separatorTopConstraint.constant = 130
    }

    func onError(errorMessage: String) {
        Logger.println("Error")
    }

}
