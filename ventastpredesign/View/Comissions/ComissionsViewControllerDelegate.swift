//
//  WelcomeComissionsViewControllerDelegate.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 2/3/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

extension CommissionsViewController: ComissionsObserver, UIScrollViewDelegate {

    func succesTicket(response: Aclaraciones) {
        cleanScroll()
        llenaScroll(type: 3, arreglo: mComissionsViewModel.getTickets(), identifier: "TicketViewCollectionViewCell", anchoCelda: CGFloat(padreScroll.width), altoCelda: CGFloat(50), anchoScroll: CGFloat(accountsView.frame.height), altoScroll: CGFloat(40), scroll: accountsView)
        if mComissionsViewModel.getTickets().count == 0 {
            labelWithoutData.isHidden = false
        } else {
            labelWithoutData.isHidden = true
        }
    }

    func succesPaidOutComissions(response: ComisionesObjeto) {
        cleanScroll()
        llenaScroll(type: 1, arreglo: mComissionsViewModel.getComisionesPagadas(), identifier: "CuentaCollectionReusableView", anchoCelda: CGFloat(padreScroll.width), altoCelda: CGFloat(50), anchoScroll: CGFloat(accountsView.frame.height), altoScroll: CGFloat(40), scroll: accountsView)
        if mComissionsViewModel.getComisionesPagadas().count == 0 {
            labelWithoutData.isHidden = false
            totalLabel.text! = "$ 00.00"
        } else {
            labelWithoutData.isHidden = true
            totalLabel.text! = mComissionsViewModel.comisionesObjeto1.chargebSum
        }
        var count = mComissionsViewModel.getComisionesPagadas().count
        mComissionsViewModel.getComisionesPagadas().forEach {
            switch $0.cuenta {
            case "0000000001", "0000000002", "0000000003", "0000000004", "0000000005", "0000000006", "0000000007", "0000000008":
                count -= 1
            default:
                ()
            }
        }
        (count == 1) ? (countSells.text = String(count) + " venta") : (countSells.text = String(count) + " ventas")
    }

    func succesOutStandingComissions(response: ComisionesObjeto) {
        cleanScroll()
        llenaScroll(type: 1, arreglo: mComissionsViewModel.getComisionesPendientes(), identifier: "CuentaCollectionReusableView", anchoCelda: CGFloat(padreScroll.width), altoCelda: CGFloat(50), anchoScroll: CGFloat(accountsView.frame.height), altoScroll: CGFloat(40), scroll: accountsView)
        if mComissionsViewModel.getComisionesPendientes().count == 0 {
            labelWithoutData.isHidden = false
            totalLabel.text! = "$ 00.00"
        } else {
            labelWithoutData.isHidden = true
            totalLabel.text! = mComissionsViewModel.comisionesObjeto2.chargebSum
        }
        var count = mComissionsViewModel.getComisionesPendientes().count
        mComissionsViewModel.getComisionesPendientes().forEach {
                switch $0.cuenta {
                case "0000000001", "0000000002", "0000000003", "0000000004", "0000000005", "0000000006", "0000000007", "0000000008":
                    count -= 1
                default:
                    ()
                }
        }
        if count == 1 {
            countSells.text! = String(count) + " venta"
        } else {
            countSells.text! = String(count) + " ventas"
        }
    }

    func succesProdMinComissions(response: ComisionesObjeto) {
        cleanScroll()
        llenaScroll(type: 1, arreglo: mComissionsViewModel.getComisionesProdMin(), identifier: "CuentaCollectionReusableView", anchoCelda: CGFloat(padreScroll.width), altoCelda: CGFloat(50), anchoScroll: CGFloat(accountsView.frame.height), altoScroll: CGFloat(40), scroll: accountsView)
        if mComissionsViewModel.getComisionesProdMin().count == 0 {
            labelWithoutData.isHidden = false
            totalLabel.text! = "$ 00.00"
        } else {
            labelWithoutData.isHidden = true
            totalLabel.text! = mComissionsViewModel.comisionesObjeto3.chargebSum
        }
        var count = mComissionsViewModel.getComisionesProdMin().count
        mComissionsViewModel.getComisionesProdMin().forEach {
                switch $0.cuenta {
                case "0000000001", "0000000002", "0000000003", "0000000004", "0000000005", "0000000006", "0000000007", "0000000008":
                    count -= 1
                default:
                    ()
                }
        }
        if count == 1 {
            countSells.text! = String(count) + " venta"
        } else {
            countSells.text! = String(count) + " ventas"
        }
    }

    func showViewWith(id: Int) {
        if id != 3 {
//            headerComissionsView.isHidden = false
//            headerTicketsView.isHidden = true
        } else {
//                headerComissionsView.isHidden = true
//                headerTicketsView.isHidden = false
        }
        switch id {
        case 0:
            faseComisiones.idFlujo = 4
            cleanScroll()
            comissionesPagadas()
            paidButton.isSelected = true
            labelWithoutData.isHidden = false
            scrollViewType.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        case 1:
            faseComisiones.idFlujo = 4
            cleanScroll()
            comissionesPendientes()
            labelWithoutData.isHidden = true
            unpaidButton.isSelected = true
            scrollViewType.setContentOffset(CGPoint(x: 80, y: 0), animated: true)
        case 2:
            faseComisiones.idFlujo = 6
            cleanScroll()
            comissionesProdMin()
            labelWithoutData.isHidden = true
            banderaProdMin = true
            minProductivityButton.isSelected = true
            scrollViewType.setContentOffset(CGPoint(x: 180, y: 0), animated: true)
        case 3:
            cleanScroll()
            ticket()
            labelWithoutData.isHidden = true
            clarificationsButton.isSelected = true
            scrollViewType.setContentOffset(CGPoint(x: 250, y: 0), animated: true)

        default:
            break
        }

    }

    func loadComissionDetails(cuenta: String) {
        cuentaView = cuenta
        cargarVistaDetallesComision()
        // aqui voy a pasar cuenta
        SwiftEventBus.post("pasar_cuenta", sender: cuentaView)
    }

    func loadTicketDetails(folio: String) {
        print(folio)
        cuentaView = folio
        // cargarVistaDetallesTicket()
        // aqui voy a pasar cuenta
        SwiftEventBus.post("pasar_folio", sender: cuentaView)
    }

    func cargarVistaDetallesComision() {
        Constants.LoadStoryBoard(name: storyBoard.Comissions.ConsultDetails, viewC: self)
    }

    func cargarVistaDetallesTicket() {
        Constants.LoadStoryBoard(name: storyBoard.Ticket.TicketDetail, viewC: self)
    }

    func cleanScroll() {
        countSells.text! = "0 ventas"
        totalLabel.text! = "$ 00.00"
        accountsView.subviews.forEach({$0.removeFromSuperview()})
//        chargebackView.subviews.forEach({$0.removeFromSuperview()})
    }

    func llenaScroll(type: Int, arreglo: [NSObject], identifier: String, anchoCelda: CGFloat, altoCelda: CGFloat, anchoScroll: CGFloat, altoScroll: CGFloat, scroll: UIView) {
        var contador = 0
        contador =  arreglo.count
        accountsView.frame = CGRect(x: 0, y: 0, width: anchoCelda, height: CGFloat(altoCelda) * CGFloat(mComissionsViewModel.getComisionesPagadas().count))
        for index in 0 ..< contador {
            if type == 1 {
                if let view: CuentaCollectionReusableView = Bundle.main.loadNibNamed(identifier, owner: self, options: nil)?.first as? CuentaCollectionReusableView {
                    view.setData(arreglo[index])
                    scroll.addSubview(view)
                    view.frame.size.height = CGFloat(altoCelda)
                    view.frame.size.width = CGFloat(anchoCelda)
                    view.frame.origin.y = CGFloat(index) * CGFloat(altoCelda)
                }
            }

            if type == 3 {
                if let view: TicketViewCollectionViewCell = Bundle.main.loadNibNamed(identifier, owner: self, options: nil)?.first as? TicketViewCollectionViewCell {
                    view.setData(arreglo[index])
                    scroll.addSubview(view)
                    view.frame.size.height = CGFloat(altoCelda)
                    view.frame.size.width = CGFloat(anchoCelda)
                    view.frame.origin.y = CGFloat(index) * CGFloat(altoCelda)
                }
            }
        }
    }

    func deselectSegmentButton() {
        paidButton.isSelected = false
        unpaidButton.isSelected = false
        minProductivityButton.isSelected = false
        clarificationsButton.isSelected = false

    }

    func configSegmentButton(label: UILabel, selector: UIView, wasSelected: Bool) {
        if wasSelected {
            selector.isHidden = false
            label.font = UIFont(name: "ProximaNova-Bold", size: 16.0)
        } else {
            selector.isHidden = true
            label.font = UIFont(name: "ProximaNova-Regular", size: 16.0)
        }
    }
}
