//
//  ComisionesVendedorResponse.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 27/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import UIKit
import ObjectMapper

class ComisionesVendedorResponse: BaseResponse {
    required init?(map: Map) {

    }

    override init() {

    }

    override func mapping(map: Map) {
         mComisiones <- map["comisiones"]
    }

    var mComisiones: [Comisiones] = []

}
