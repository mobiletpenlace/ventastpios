//
//  GetTicketRequest.swift
//  ventastp
//
//  Created by BranchBit on 14/10/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation

import ObjectMapper

public class GetTicketRequest: BaseRequest {

    var nu_empleado_in: String?
    var fh_inicial: String?
    var fh_final: String?

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override public func mapping(map: Map) {
        nu_empleado_in <- map["nu_empleado_in"]
        fh_inicial <- map["fh_inicial"]
        fh_final <- map["fh_final"]
    }

}
