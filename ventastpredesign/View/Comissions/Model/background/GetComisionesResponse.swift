//
//  GetComisionesResponse.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 25/09/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit
import Foundation

import ObjectMapper

class GetComisionesResponse: BaseResponse {
    required init?(map: Map) {
    }

    override func mapping(map: Map) {
        Response <- map["Response"]
        Comisiones <- map["Comisiones"]
    }

    var Response: Response?
    var Comisiones: Comisiones?
 }
