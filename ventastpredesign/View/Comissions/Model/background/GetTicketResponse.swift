//
//  GetTicketResponse.swift
//  ventastp
//
//  Created by BranchBit on 14/10/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import Foundation

import ObjectMapper

class GetTicketResponse: BaseResponse {
    required init?(map: Map) {
    }

    override func mapping(map: Map) {
        Result <- map["result"]
        Aclaraciones <- map["aclaraciones"]
    }

    var Result: String?
    var Aclaraciones: [Aclaraciones] = []

 }
