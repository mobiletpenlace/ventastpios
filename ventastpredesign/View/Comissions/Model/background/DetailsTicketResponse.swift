//
//  DetailsTicketResponse.swift
//  ventastp
//
//  Created by BranchBit on 22/10/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class DetailsTicketResponse: BaseResponse {

    required init?(map: Map) {
    }

    override init() {
    }

    override func mapping(map: Map) {
        result <- map["result"]
        estatus <- map["estatus"]
        fh_modificacion <- map["fh_modificacion"]
        tipo_aclaracion <- map["tipo_aclaracion"]
        fh_creacion <- map["fh_creacion"]
        cd_cuenta <- map["cd_cuenta"]
        resultDescripction <- map["resultDescripction"]
        user_mod <- map["user_mod"]
        nb_folio_aclaracion <- map["nb_folio_aclaracion"]
        tipo_motivo <- map["tipo_motivo"]
        comentario_mesa <- map["comentario_mesa"]
    }
    var estatus: String?
    var fh_modificacion: String?
    var tipo_aclaracion: String?
    var fh_creacion: String?
    var cd_cuenta: String?
    var resultDescripction: String?
    var user_mod: String?
    var nb_folio_aclaracion: String?
    var tipo_motivo: String?
    var comentario_mesa: String?

 }
