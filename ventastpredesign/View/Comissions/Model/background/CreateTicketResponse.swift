//
//  CreateTicketResponse.swift
//  ventastp
//
//  Created by BranchBit on 18/10/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class CreateTicketResponse: BaseResponse {
    required init?(map: Map) {
    }

    override init() {
    }

    override func mapping(map: Map) {
        Result <- map["result"]
        folio <- map["folio"]
        ResultDescription <- map["resultDescription"]
    }

    var Result: String?
    var folio: String?
    var ResultDescription: String?

}
