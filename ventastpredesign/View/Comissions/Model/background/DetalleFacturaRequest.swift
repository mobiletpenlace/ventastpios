//
//  DetalleFacturaRequest.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 27/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import UIKit
import ObjectMapper

class DetalleFacturaRequest: BaseRequest {

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        fechaFin <- map["fechaFin"]
        fechaInicio <- map["fechaInicio"]
        numeroEmpleado <- map["numeroEmpleado"]
    }

    var fechaFin: String?
    var fechaInicio: String?
    var numeroEmpleado: String?

}
