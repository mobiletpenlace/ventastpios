//
//  RankingVendedorResponse.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 02/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class RankingVendedorResponse: BaseResponse {
    required init?(map: Map) {

    }

    override init() {

    }

    override func mapping(map: Map) {
        empleado <- map["empleado"]
        ranking <- map["ranking"]
        idResult <- map["idResult"]
        result <- map["result"]
        resultDescription <- map["resultDescription"]

    }

    var empleado: String?
    var ranking: String?

}
