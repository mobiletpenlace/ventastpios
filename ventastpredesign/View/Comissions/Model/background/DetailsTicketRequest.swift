//
//  DetailsTicketRequest.swift
//  ventastp
//
//  Created by BranchBit on 22/10/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation

import ObjectMapper

public class DetailsTicketRequest: BaseRequest {

    var folio_in: String?

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override public func mapping(map: Map) {
        folio_in <- map["folio_in"]
    }

}
