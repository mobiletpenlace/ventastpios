//
//  CreateTicketRequest.swift
//  ventastp
//
//  Created by BranchBit on 18/10/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation

import ObjectMapper

public class CreateTicketRequest: BaseRequest {

    var numEmpleado: String?
    var cd_cuenta: String?
    var tipo_aclaracion: String?
    var imei_creacion: String?

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override public func mapping(map: Map) {
        numEmpleado <- map["numEmpleado"]
        cd_cuenta <- map["cd_cuenta"]
        tipo_aclaracion <- map["tipo_aclaracion"]
        imei_creacion <- map["imei_creacion"]
    }
}
