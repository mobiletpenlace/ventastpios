//
//  GetFacturasNoPagadasResponse.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 25/09/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import Foundation
import ObjectMapper

class GetFacturasNoPagadasResponse: BaseResponse {
    required init?(map: Map) {
    }

    override func mapping(map: Map) {
        Response <- map["Response"]
        FacturasNoPagadas <- map["FacturasNoPagadas"]
    }

    var Response: Response?
    var FacturasNoPagadas: Comisiones?
}
