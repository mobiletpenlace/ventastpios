//
//  FacturasNoPagadas.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 27/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class FacturasNoPagadas: BaseResponse {
    required init?(map: Map) {

    }

    override init() {

    }

    override func mapping(map: Map) {
        mFacturas <- map["facturas"]
    }

    var mFacturas: [facturas] = []

}
