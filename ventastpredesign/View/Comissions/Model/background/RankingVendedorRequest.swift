//
//  RankingVendedorRequest.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 02/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import UIKit
import ObjectMapper

class RankingVendedorRequest: BaseRequest {

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        mVendedor <- map["vendedor"]
    }

    var mVendedor: String?

}
