//
//  ComisionesObjeto.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 06/10/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import Foundation
class ComisionesObjeto: NSObject {

    var arrCuenta: [String] = []
    var arrPago1: [String] = []
    var arrPago2: [String] = []
    var arrChargeBack: [String] = []
    var arrCuentaNP: [String] = []
    var arrPago1NP: [String] = []
    var arrPago2NP: [String] = []
    var cuenta: String = ""
    var pago1: String = ""
    var pago2: String = ""
    var chargeBack: String = ""
    var instaladasNum: String = ""
    var pago1Num: String = ""
    var pago2Num: String = ""
    var chargebNum: String = ""
    var pago1Sum: String = ""
    var pago2Sum: String = ""
    var chargebSum: String = ""
    var sumaTotal: String = ""
//    var sumaTotalNP: String = ""
    var cliente: String = ""

}
