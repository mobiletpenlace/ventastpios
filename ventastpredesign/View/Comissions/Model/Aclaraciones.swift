//
//  Aclaraciones.swift
//  ventastp
//
//  Created by BranchBit on 14/10/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class Aclaraciones: NSObject, Mappable {
    var estatus: Int?
    var tipo_aclaracion: Int?
    var fh_creacion: String?
    var cd_cuenta: String?
    var nb_folio_aclaracion: String?

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        estatus <- map["estatus"]
        tipo_aclaracion <- map["tipo_aclaracion"]
        fh_creacion <- map["fh_creacion"]
        cd_cuenta <- map["cd_cuenta"]
        nb_folio_aclaracion <- map["nb_folio_aclaracion"]
    }

    override init() {
    }

}
