//
//  facturas.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 27/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import UIKit

import ObjectMapper

class facturas: NSObject, Mappable {

    var cuenta: String?
    var factura1: String?
    var factura2: String?

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        cuenta <- map["cuenta"]
        factura1 <- map["factura1"]
        factura2 <- map["factura2"]
    }

    override init() {
    }

}
