//
//  firstInvoicesDetails.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 25/09/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import Foundation

import ObjectMapper

class ListCuentaComisionVO: NSObject, Mappable {

    var cuenta: String?
    var pago1: String?

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        cuenta <- map["cuenta"]
        pago1 <- map["pago1"]
    }

    override init() {
    }

}
