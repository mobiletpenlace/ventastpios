//
//  InvoicesDetails.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 25/09/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import Foundation

import ObjectMapper

class InvoiceDetails: NSObject, Mappable {

    var cuenta: String?
    var cliente: String?
    var fechaInst: String?
    var pago1: String?
    var pago2: String?
    var fechaVend1: String?
    var fechaVend2: String?
    var fechaClient1: String?
    var fechaCliente2: String?
    var paquete: String?

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        cuenta <- map["cuenta"]
        cliente <- map["cliente"]
        fechaInst <- map["fechaInst"]
        pago1 <- map["pago1"]
        pago2 <- map["pago2"]
        fechaVend1 <- map["fechaVend1"]
        fechaVend2 <- map["fechaVend2"]
        fechaClient1 <- map["fechaClient1"]
        fechaCliente2 <- map["fechaCliente2"]
        paquete <- map["paquete"]
    }

    override init() {
    }

}
