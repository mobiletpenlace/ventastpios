//
//  InvoicesOutStanding.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 25/09/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import Foundation

import ObjectMapper

class InvoicesOutStanding: NSObject, Mappable {

    var cuenta: String?
    var pago1: String?
    var pago2: String?

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        cuenta <- map["cuenta"]
        pago1 <- map["pago1"]
        pago2 <- map["pago2"]
    }

}
