//
//  Comisiones.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 27/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class Comisiones: NSObject, Mappable {

    var idResult: String?
    var listCuentaComisionVO: [ListCuentaComisionVO] = []

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        idResult <- map["idResult"]
        listCuentaComisionVO <- map["listCuentaComisionVO"]
    }

    override init() {
    }

}
