//
//  DetalleFactura1.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 27/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import UIKit

import ObjectMapper

class DetalleFactura1: NSObject, Mappable {

    var addon: String?
    var cargoRecurrente: String?
    var comisionPagada: String?
    var comisionPagoAddons: String?
    var comisionPago: String?
    var factura: String?
    var fechaComisionPagada: String?
    var fechaCorte: String?
    var fechaCorteF2: String?
    var fechaInstalacion: String?
    var fechaPagoCliente: String?
    var fechaPagoVendedor: String?
    var nombreCliente: String?
    var paquete: String?
    var tipoProducto: String?

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        addon <- map["addon"]
        cargoRecurrente <- map["cargoRecurrente"]
        comisionPagada <- map["comisionPagada"]
        comisionPagoAddons <- map["comisionPagadaAddons"]
        comisionPago <- map["comisionPago"]
        factura <- map["factura"]
        fechaComisionPagada <- map["fechaComisionPagada"]
        fechaCorte <- map["fechaCorte"]
        fechaCorteF2 <- map["fechaCorteF2"]
        fechaInstalacion <- map["fechaInstalacion"]
        fechaPagoCliente <- map["fechaPagoCliente"]
        fechaPagoVendedor <- map["fechaPagoVendedor"]
        nombreCliente <- map["nombreCliente"]
        paquete <- map["paquete"]
        tipoProducto <- map["tipoProducto"]
    }

    override init() {
    }

}
