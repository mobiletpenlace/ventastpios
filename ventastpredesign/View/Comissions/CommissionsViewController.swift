//
//  WelcomeCommissionsViewController.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 22/09/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class CommissionsViewController: BaseVentasView, GetDateDelegate {

    @IBOutlet weak var paidButton: UIButton!
    @IBOutlet weak var unpaidButton: UIButton!
    @IBOutlet weak var minProductivityButton: UIButton!
    @IBOutlet weak var clarificationsButton: UIButton!
    @IBOutlet weak var padreScroll: UIScrollView!
    @IBOutlet weak var scrollViewType: UIScrollView!
    var controller: UIViewController?
    let Userdefaults = UserDefaults.standard
    @IBOutlet weak var datesButton: UIButton!
    @IBOutlet weak var periodDatesLabel: UILabel!

    @IBOutlet weak var countSells: UILabel!
    @IBOutlet weak var detailsDatesLabel: UILabel!
    @IBOutlet weak var headerTicketsView: UIView!

    @IBOutlet weak var labelWithoutData: UILabel!
    @IBOutlet weak var separatorViewTotal: UIView!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var accountsView: UIView!

    @IBOutlet weak var dateFolioLabel: UILabel!

    @IBOutlet weak var originStatusLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!

    @IBOutlet weak var totalView: UIView!
    //  Calendar
    var calendar = Calendar.current
    var calendarComissionsViewController: CalendarComissionsViewController!
    let fechaActual = Date()
    var fechaI = Date()
    var fechaF = Date()
    var formatter = DateFormatter()
    var componentsI = DateComponents()
    var componentsF = DateComponents()
    var cuentaView = ""
    var InicioFechaString = ""
    var FinFechaString = ""
    var weekOfMonth = 0
    var comissionsDetailsViewModel: ComissionsDetailsViewModel!
    var mComissionsViewModel: ComissionsViewModel!
    var banderaProdMin = false
    var comissionsDetailsViewController: ComissionsDetailsViewController!
    var valueSelectedButton: Int = 0
//    var mComissionsPruebaPagadas: [ListCuentaComisionVO] = []
    var mComissionsPruebaPendientes: [ListCuentaComisionVO] = []
    var mComissionsPruebaProdMin: [ListCuentaComisionVO] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        calendar.firstWeekday = 1
        mComissionsViewModel = ComissionsViewModel(view: self)
        mComissionsViewModel.atachView(observer: self)
        getFechas()
        deselectSegmentButton()
        showViewWith(id: 0)
        totalLabel.text! = InicioFechaString
//        var pruebaScroll: [ListCuentaComisionVO] = []
//        let prueba1 = ListCuentaComisionVO()
//        prueba1.cuenta = "prueba"
//        prueba1.chargeBack = "130.00"
//        pruebaScroll.append(prueba1)
//        llenaScroll(arreglo: pruebaScroll, identifier: "CuentaCollectionReusableView", anchoCelda: CGFloat(separatorViewTotal.width), altoCelda: CGFloat(50), anchoScroll: CGFloat(accountsView.frame.height-45), altoScroll: CGFloat(40), scroll: chargebackView)
    }

    func comissionesPagadas() {
        cleanScroll()
        stackType(type: 0)
        let request = GetComisionesRequest()
        request.numEmpleado = faseComisiones.numEmpleado// "17090004" ""2045420"
        request.fh_inicio = faseComisiones.fechaInicio
        request.fh_fin = faseComisiones.fechaFin
        request.tipo_consulta = "2"
//        request.ai_cuenta = "65054431"
        mComissionsViewModel.comissions(request: request)
    }
    func comissionesPendientes() {
        cleanScroll()
        stackType(type: 0)
        let request = GetFacturasNoPagadasRequest()
        request.numEmpleado = faseComisiones.numEmpleado// "17090004" ""2045420"
        request.fh_inicio = faseComisiones.fechaInicio
        request.fh_fin = faseComisiones.fechaFin
        request.tipo_consulta = "3"
//        request.ai_cuenta = "012340212"
        mComissionsViewModel.comissions2(request: request)
    }

    func comissionesProdMin() {
        cleanScroll()
        stackType(type: 0)
        let request = GetProductividadMinimaRequest()
        request.numEmpleado = faseComisiones.numEmpleado// "17090004" ""2045420"
        request.fh_inicio = faseComisiones.fechaInicio
        request.fh_fin = faseComisiones.fechaFin
        request.tipo_consulta = "5"
        mComissionsViewModel.comissions3(request: request)
    }

    func ticket() {
        cleanScroll()
        stackType(type: 1)
        let request = GetTicketRequest()
        request.nu_empleado_in = faseComisiones.numEmpleado// "17090004" ""2045420"
        request.fh_inicial = "1995-05-18"// faseComisiones.fechaInicio
        request.fh_final = "2021-10-20"// faseComisiones.fechaFin
        mComissionsViewModel.tickets(request: request)
    }

    func getFechas() {
        let weekday = Calendar.current.component(.weekday, from: fechaActual)
        switch weekday {
        case 1:
            Logger.println("Domingo")
            componentsI.day = -13
        case 2:
            Logger.println("Lunes")
            componentsI.day = -7
        case 3:
            Logger.println("Martes")
            componentsI.day = -8
        case 4:
            Logger.println("Miercoles")
            componentsI.day = -9
        case 5:
            Logger.println("Jueves")
            componentsI.day = -10
        case 6:
            Logger.println("Viernes")
            componentsI.day = -11
        case 7:
            Logger.println("Sabado")
            componentsI.day = -12
        default:
            Logger.println("Default")
        }
        componentsF.day = 6
        fechaI = calendar.date(byAdding: componentsI, to: fechaI)!
        datesButton.setTitle(DateCalendar.convertDateString(date: fechaI, formatter: Formate.dd_MMMM_yyyy) + " - " + DateCalendar.convertDateString(date: fechaF, formatter: Formate.dd_MMMM_yyyy), for: .normal)
        periodDatesLabel.text = DateCalendar.convertDateString(date: fechaI, formatter: Formate.dd_MMMM_yyyy) + " - " + DateCalendar.convertDateString(date: fechaF, formatter: Formate.dd_MMMM_yyyy)
        detailsDatesLabel.text = "Del " + DateCalendar.convertDateString(date: fechaI, formatter: Formate.dd_MMMM_yyyy) + " al " + DateCalendar.convertDateString(date: fechaF, formatter: Formate.dd_MMMM_yyyy)
        faseComisiones.fechaInicio = DateCalendar.convertDateString(date: fechaI, formatter: Formate.yyyy_MM_dd_dash)
        fechaF = calendar.date(byAdding: componentsF, to: fechaI)!
        faseComisiones.fechaFin = DateCalendar.convertDateString(date: fechaF, formatter: Formate.yyyy_MM_dd_dash)
        var weekOfMonth: Int {
            return Calendar.current.component(.weekOfMonth, from: fechaF) - 1
            }
        if Calendar.current.component(.weekOfMonth, from: fechaF) - 1 == 0 {
            datesButton.setTitle(DateCalendar.convertDateString(date: fechaI, formatter: Formate.dd_MMMM_yyyy) + " - " + DateCalendar.convertDateString(date: fechaF, formatter: Formate.dd_MMMM_yyyy), for: .normal)
            periodDatesLabel.text = DateCalendar.convertDateString(date: fechaI, formatter: Formate.dd_MMMM_yyyy) + " - " + DateCalendar.convertDateString(date: fechaF, formatter: Formate.dd_MMMM_yyyy)
            detailsDatesLabel.text =  "Del " + DateCalendar.convertDateString(date: fechaI, formatter: Formate.dd_MMMM_yyyy) + " al " + DateCalendar.convertDateString(date: fechaF, formatter: Formate.dd_MMMM_yyyy)
        } else {
            datesButton.setTitle(DateCalendar.convertDateString(date: fechaI, formatter: Formate.dd_MMMM_yyyy) + " - " + DateCalendar.convertDateString(date: fechaF, formatter: Formate.dd_MMMM_yyyy), for: .normal)
            periodDatesLabel.text = DateCalendar.convertDateString(date: fechaI, formatter: Formate.dd_MMMM_yyyy) + " - " + DateCalendar.convertDateString(date: fechaF, formatter: Formate.dd_MMMM_yyyy)
            detailsDatesLabel.text =  "Del " + DateCalendar.convertDateString(date: fechaI, formatter: Formate.dd_MMMM_yyyy) + " al " + DateCalendar.convertDateString(date: fechaF, formatter: Formate.dd_MMMM_yyyy)
        }
        // fechaFin.text! = DateCalendar.convertDateString(date: fechaF, formatter: Formate.d_MMM)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func stackType(type: Int) {
        if type == 1 {
            dateFolioLabel.text = "Folio"
            originStatusLabel.text = "Estatus"
            amountLabel.isHidden = true
            totalView.isHidden = true
        } else {
            dateFolioLabel.text = "Fecha"
            originStatusLabel.text = "Origen"
            amountLabel.isHidden = false
            totalView.isHidden = false
        }
    }

    @IBAction func CalendarButton(_ sender: Any) {
        let calendarComissionsViewController: CalendarComissionsViewController = UIStoryboard(name: "CalendarComissionsViewController", bundle: nil).instantiateViewController(withIdentifier: "CalendarComissionsViewController") as! CalendarComissionsViewController
        calendarComissionsViewController.providesPresentationContextTransitionStyle = true
        calendarComissionsViewController.getDatesDelegate = self
        calendarComissionsViewController.definesPresentationContext = true
        calendarComissionsViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        calendarComissionsViewController.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        UIApplication.topViewController()?.present(calendarComissionsViewController, animated: false, completion: nil)
    }

    @IBAction func showViewInContainer(_ sender: UIButton) {
        deselectSegmentButton()
        valueSelectedButton = Int(sender.tag)
        showViewWith(id: sender.tag)
    }

    func getNewDates(_ dateInicial: Date, _ dateFinal: Date) {
        cleanScroll()
        var banderaProdMin = false

        // RECIBO FECHAS
        faseComisiones.fechaInicio = DateCalendar.convertDateString(date: dateInicial, formatter: Formate.yyyy_MM_dd_dash)
        faseComisiones.nuevaFechaI = faseComisiones.fechaInicio
        faseComisiones.fechaFin = DateCalendar.convertDateString(date: dateFinal, formatter: Formate.yyyy_MM_dd_dash)
        faseComisiones.nuevaFechaF = faseComisiones.fechaFin
        var weekOfMonth: Int {
            return Calendar.current.component(.weekOfMonth, from: dateFinal) - 1
        }
        if Calendar.current.component(.weekOfMonth, from: dateFinal) - 1 == 0 {
            datesButton.setTitle(DateCalendar.convertDateString(date: dateInicial, formatter: Formate.dd_MMMM_yyyy) + " - " + DateCalendar.convertDateString(date: dateFinal, formatter: Formate.dd_MMMM_yyyy), for: .normal)
            periodDatesLabel.text = DateCalendar.convertDateString(date: dateInicial, formatter: Formate.dd_MMMM_yyyy) + " - " + DateCalendar.convertDateString(date: dateFinal, formatter: Formate.dd_MMMM_yyyy)
            detailsDatesLabel.text =  "Del " + DateCalendar.convertDateString(date: dateInicial, formatter: Formate.dd_MMMM_yyyy) + " al " + DateCalendar.convertDateString(date: dateFinal, formatter: Formate.dd_MMMM_yyyy)

        } else {
            datesButton.setTitle(DateCalendar.convertDateString(date: dateInicial, formatter: Formate.dd_MMMM_yyyy) + " - " + DateCalendar.convertDateString(date: dateFinal, formatter: Formate.dd_MMMM_yyyy), for: .normal)
            periodDatesLabel.text = DateCalendar.convertDateString(date: dateInicial, formatter: Formate.dd_MMMM_yyyy) + " - " + DateCalendar.convertDateString(date: dateFinal, formatter: Formate.dd_MMMM_yyyy)
            detailsDatesLabel.text =  "Del " + DateCalendar.convertDateString(date: dateInicial, formatter: Formate.dd_MMMM_yyyy) + " al " + DateCalendar.convertDateString(date: dateFinal, formatter: Formate.dd_MMMM_yyyy)
        }

        switch valueSelectedButton {
        case 0:
            comissionesPagadas()
        case 1:
            comissionesPendientes()
        case 2:
            comissionesProdMin()
        case 3:
            ticket()
        default:
            break
        }
    }

    @IBAction func actionBack(_ sender: Any) {
        dismiss(animated: false, completion: nil)
        }
    }

extension String {
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}
