//
//  OutStandingComissionsModel.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 28/09/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

protocol ComissionsModelObserver {
    func succesPaidOutComissions (response: GetComisionesResponse)
    func succesOutStandingComissions (response: GetFacturasNoPagadasResponse)
    func succesProdMinComissions (response: GetProductividadMinimaResponse)
    func succesTicket(response: GetTicketResponse)
    func onError(errorMessage: String)// On error func
}

class ComissionsModel: BaseModel {

    var viewModel: ComissionsModelObserver?
    var server: ServerDataManager2<GetComisionesResponse>?
    var server2: ServerDataManager2<GetFacturasNoPagadasResponse>?
    var server3: ServerDataManager2<GetProductividadMinimaResponse>?
    var server4: ServerDataManager2<GetTicketResponse>?
    var urlSellerComissionsResponse = AppDelegate.API_TOTAL + ApiDefinition.API_COMISSIONS_SALE
    var urlGetFacturasNoPagadasResponse = AppDelegate.API_TOTAL + ApiDefinition.API_FACT_NO_PAYMENT
    var urlGetProdMinResponse = AppDelegate.API_TOTAL + ApiDefinition.API_GET_PROD_MIN
    var urlGetTicketResponse = AppDelegate.API_TOTAL + ApiDefinition.API_GET_TICKETS
    init (observer: BaseViewModelObserver) {
        super.init(observerVM: observer)
        // Se declara un server 
        server = ServerDataManager2<GetComisionesResponse>()
        server2 = ServerDataManager2<GetFacturasNoPagadasResponse>()
        server3 = ServerDataManager2<GetProductividadMinimaResponse>()
        server4 = ServerDataManager2<GetTicketResponse>()
    }

    func atachModel(viewModel: ComissionsModelObserver) {
        self.viewModel = viewModel
    }

    func doGetPaidOutComissions(request: GetComisionesRequest) {
        let urlSellerComissionsResponse = AppDelegate.API_TOTAL + ApiDefinition.API_COMISSIONS_SALE
        server?.setValues(requestUrl: urlSellerComissionsResponse, delegate: self, headerType: .headersFfmap)
        server?.request(requestModel: request)
    }

    func doGetOutStandingComissions(request: GetFacturasNoPagadasRequest) {
        let urlGetFacturasNoPagadasResponse = AppDelegate.API_TOTAL + ApiDefinition.API_FACT_NO_PAYMENT
        server2?.setValues(requestUrl: urlGetFacturasNoPagadasResponse, delegate: self, headerType: .headersFfmap)
        server2?.request(requestModel: request)
    }

    func doGetProdMinComissions(request: GetProductividadMinimaRequest) {
        let urlGetProdMinResponse = AppDelegate.API_TOTAL + ApiDefinition.API_GET_PROD_MIN
        server3?.setValues(requestUrl: urlGetProdMinResponse, delegate: self, headerType: .headersFfmap)
        server3?.request(requestModel: request)
    }

    func doGetTickets(request: GetTicketRequest) {
        let urlGetTickets = AppDelegate.API_TOTAL + ApiDefinition.API_GET_TICKETS
        server4?.setValues(requestUrl: urlGetTickets, delegate: self, headerType: .headersFfmap)
        server4?.request(requestModel: request)
    }

    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {

        if requestUrl == urlSellerComissionsResponse {
            let paidOutComissionsResponse = response as! GetComisionesResponse
            if paidOutComissionsResponse.Response?.Result == "0" {
                viewModel?.succesPaidOutComissions(response: response as! GetComisionesResponse)
            } else if paidOutComissionsResponse.Response?.Result == "-1" {
                viewModel?.onError(errorMessage: paidOutComissionsResponse.Response?.Result ?? "Favor de validar de nuevo")

            } else {
                Logger.println("Error en la ejecucion")
            }
        }

        if requestUrl == urlGetFacturasNoPagadasResponse {
            let outStandingComissionsResponse = response as! GetFacturasNoPagadasResponse
            if outStandingComissionsResponse.Response?.Result == "0" {
                viewModel?.succesOutStandingComissions(response: response as! GetFacturasNoPagadasResponse)
            } else if outStandingComissionsResponse.Response?.Result == "-1" {
                viewModel?.onError(errorMessage: outStandingComissionsResponse.Response?.Result ?? "Favor de validar de nuevo")
            } else {
                Logger.println("Error en la ejecucion")
            }
        }

        if requestUrl == urlGetProdMinResponse {
            let prodMinComissionsResponse = response as! GetProductividadMinimaResponse
            if prodMinComissionsResponse.Response?.Result == "0" {
                viewModel?.succesProdMinComissions(response: response as! GetProductividadMinimaResponse)
            } else if prodMinComissionsResponse.Response?.Result == "-1" {
                viewModel?.onError(errorMessage: prodMinComissionsResponse.Response?.Result ?? "Favor de validar de nuevo")
            } else {
                Logger.println("Error en la ejecucion")
            }
        }

        if requestUrl == urlGetTicketResponse {
            let ticketResponse = response as! GetTicketResponse
            if ticketResponse.Result == "200" {
                viewModel?.succesTicket(response: response as! GetTicketResponse)
            } else {
                viewModel?.onError(errorMessage: ticketResponse.Result ?? "Favor de validar de nuevo")
            }
        }

  }

    override func onErrorLoadResponse(requestUrl: String, messageError: String) {
        viewModel?.onError(errorMessage: messageError)
    }

}
