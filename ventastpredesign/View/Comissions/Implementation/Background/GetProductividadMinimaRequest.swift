//
//  GetProductividadMinimaRequest.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 2/3/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import BaseClases
import ObjectMapper

public class GetProductividadMinimaRequest: BaseRequest {

    var numEmpleado: String?
    var fh_inicio: String?
    var fh_fin: String?
    var tipo_consulta: String?
    var ai_cuenta: String?

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override public func mapping(map: Map) {
        numEmpleado <- map["numEmpleado"]
        fh_inicio <- map["fh_inicio"]
        fh_fin <- map["fh_fin"]
        tipo_consulta <- map["tipo_consulta"]
        ai_cuenta <- map["ai_cuenta"]
    }

}
