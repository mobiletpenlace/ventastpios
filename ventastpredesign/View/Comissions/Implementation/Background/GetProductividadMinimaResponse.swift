//
//  GetProductividadMinimaResponse.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 2/3/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import Foundation
import BaseClases
import ObjectMapper

class GetProductividadMinimaResponse: BaseResponse {
    required init?(map: Map) {
    }

    override func mapping(map: Map) {
        Response <- map["Response"]
        Comisiones <- map["Comisiones"]
    }

    var Response: Response?
    var Comisiones: Comisiones?
 }
