//
//  ComissionsViewModel.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 28/09/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import SwiftEventBus

protocol ComissionsObserver {
    func succesPaidOutComissions  (response: ComisionesObjeto)
    func succesOutStandingComissions (response: ComisionesObjeto)
    func succesProdMinComissions (response: ComisionesObjeto)
    func succesTicket (response: Aclaraciones)
    func loadComissionDetails(cuenta: String)
    func loadTicketDetails(folio: String)
}

class ComissionsViewModel: BaseViewModel, ComissionsModelObserver {

    var observer: ComissionsObserver!
    var model: ComissionsModel!
    var comisionesObjeto1: ComisionesObjeto!
    var comisionesObjeto2: ComisionesObjeto!
    var comisionesObjeto3: ComisionesObjeto!
    var aclaracionesObjeto: Aclaraciones!

    var mComisionesPagadas: [ListCuentaComisionVO]=[]
    var mComisionesPagadasChargeBack: [ListCuentaComisionVO]=[]
    var mComisionesPendientes: [ListCuentaComisionVO]=[]
    var mComisionesProdMin: [ListCuentaComisionVO]=[]
    var mAclaraciones: [Aclaraciones]=[]

    var sumaPagadas = 0.0
    var sumaNoPagadas = 0.0
    var sumaProdMin = 0.0
    var sumaChargeBack = 0.0

    override init(view: BaseVentasView) {
        super.init(view: view)
        model = ComissionsModel(observer: self)
        model.atachModel(viewModel: self)
    }

    func atachView(observer: ComissionsObserver) {
        self.observer = observer
        SwiftEventBus.onMainThread(self, name: "Consulta_Detalles_Comission") { [weak self] result in
            if let cuenta = result?.object as? String {

                // Aqui cargo pantalla
                self?.observer?.loadComissionDetails(cuenta: cuenta)
            }

         }

        SwiftEventBus.onMainThread(self, name: "Consulta_Detalles_Ticket") { [weak self] result in
            if let cuenta = result?.object as? String {

                // Aqui cargo pantalla
                self?.observer?.loadTicketDetails(folio: cuenta)
            }
         }

         }

    func viewWillDissapear() {
        SwiftEventBus.unregister(self)
    }

    func comissions(request: GetComisionesRequest) {
        view.showLoading(message: "Consultado comisiones pagadas")
        model.doGetPaidOutComissions(request: request)
    }

    func comissions2(request: GetFacturasNoPagadasRequest) {
        view.showLoading(message: "Consultado comisiones no pagadas")
        model.doGetOutStandingComissions(request: request)
    }

    func comissions3(request: GetProductividadMinimaRequest) {
        view.showLoading(message: "Consultado productividad minima")
        model.doGetProdMinComissions(request: request)
    }

    func tickets(request: GetTicketRequest) {
        view.showLoading(message: "Consultado aclaraciones")
        model.doGetTickets(request: request)
    }

    func onError(errorMessage: String) {
        Logger.println(errorMessage)
        view.hideLoading()
    }

    func getComisionesPagadas() -> [ListCuentaComisionVO] {
        return mComisionesPagadas
    }

    func getComisionesPendientes() -> [ListCuentaComisionVO] {
        return mComisionesPendientes
    }

    func getComisionesProdMin() -> [ListCuentaComisionVO] {
        return mComisionesProdMin
    }

    func getTickets() -> [Aclaraciones] {
        return mAclaraciones
    }

    func succesPaidOutComissions(response: GetComisionesResponse) {
        view.hideLoading()
        comisionesObjeto1 = ComisionesObjeto()
        mComisionesPagadas = [ListCuentaComisionVO]()
        mComisionesPagadasChargeBack = [ListCuentaComisionVO]()

        for i in 0..<(response.Comisiones?.listCuentaComisionVO.count)! {

                mComisionesPagadas.append((response.Comisiones?.listCuentaComisionVO[i])!)
                sumaPagadas = sumaPagadas + Double((mComisionesPagadas[i].pago1)!)!

            if response.Comisiones?.listCuentaComisionVO[i].cuenta == "0000000001" {
                let elementToMove = (response.Comisiones?.listCuentaComisionVO.remove(at: i))!
                response.Comisiones?.listCuentaComisionVO.append(elementToMove)
            }
            if response.Comisiones?.listCuentaComisionVO[i].cuenta == "0000000002" {
                let elementToMove = (response.Comisiones?.listCuentaComisionVO.remove(at: i))!
                response.Comisiones?.listCuentaComisionVO.append(elementToMove)
            }
            if response.Comisiones?.listCuentaComisionVO[i].cuenta == "0000000003" {
                let elementToMove = (response.Comisiones?.listCuentaComisionVO.remove(at: i))!
                response.Comisiones?.listCuentaComisionVO.append(elementToMove)
            }
            if response.Comisiones?.listCuentaComisionVO[i].cuenta == "0000000004" {
                let elementToMove = (response.Comisiones?.listCuentaComisionVO.remove(at: i))!
                response.Comisiones?.listCuentaComisionVO.append(elementToMove)
            }
            if response.Comisiones?.listCuentaComisionVO[i].cuenta == "0000000005" {
                let elementToMove = (response.Comisiones?.listCuentaComisionVO.remove(at: i))!
                response.Comisiones?.listCuentaComisionVO.append(elementToMove)
            }
            if response.Comisiones?.listCuentaComisionVO[i].cuenta == "0000000006" {
                let elementToMove = (response.Comisiones?.listCuentaComisionVO.remove(at: i))!
                response.Comisiones?.listCuentaComisionVO.append(elementToMove)
            }
            if response.Comisiones?.listCuentaComisionVO[i].cuenta == "0000000007" {
                let elementToMove = (response.Comisiones?.listCuentaComisionVO.remove(at: i))!
                response.Comisiones?.listCuentaComisionVO.append(elementToMove)
            }
            if response.Comisiones?.listCuentaComisionVO[i].cuenta == "0000000008" {
                let elementToMove = (response.Comisiones?.listCuentaComisionVO.remove(at: i))!
                response.Comisiones?.listCuentaComisionVO.append(elementToMove)
            }
        }

        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        numberFormatter.groupingSize = 3
        numberFormatter.secondaryGroupingSize = 2
        comisionesObjeto1.sumaTotal = String(format: "%.2f", sumaPagadas)

            comisionesObjeto1.chargebSum = (sumaPagadas-sumaChargeBack).asLocaleCurrency
        sumaPagadas = 0
        sumaChargeBack = 0
        observer?.succesPaidOutComissions(response: comisionesObjeto1!)
    }

    func succesOutStandingComissions(response: GetFacturasNoPagadasResponse) {
        view.hideLoading()
        mComisionesPendientes = [ListCuentaComisionVO]()
        comisionesObjeto2 = ComisionesObjeto()

        for i in 0..<(response.FacturasNoPagadas?.listCuentaComisionVO.count)! {
            sumaNoPagadas = sumaNoPagadas + Double((response.FacturasNoPagadas?.listCuentaComisionVO[i].pago1)!)!
        }
        comisionesObjeto2?.sumaTotal =  "$ " + String(format: "%.2f", sumaNoPagadas)
        sumaNoPagadas = 0
        mComisionesPendientes = (response.FacturasNoPagadas?.listCuentaComisionVO)!
        observer?.succesOutStandingComissions(response: comisionesObjeto2!)
    }

    func succesProdMinComissions(response: GetProductividadMinimaResponse) {
        view.hideLoading()
        comisionesObjeto3 = ComisionesObjeto()
        mComisionesProdMin = [ListCuentaComisionVO]()

        for i in 0..<(response.Comisiones?.listCuentaComisionVO.count)! {

            mComisionesProdMin.append((response.Comisiones?.listCuentaComisionVO[i])!)
            sumaProdMin = sumaProdMin + Double((mComisionesProdMin[i].pago1)!)!

            if response.Comisiones?.listCuentaComisionVO[i].cuenta == "0000000001" {
                let elementToMove = (response.Comisiones?.listCuentaComisionVO.remove(at: i))!
                response.Comisiones?.listCuentaComisionVO.append(elementToMove)
            }
            if response.Comisiones?.listCuentaComisionVO[i].cuenta == "0000000002" {
                let elementToMove = (response.Comisiones?.listCuentaComisionVO.remove(at: i))!
                response.Comisiones?.listCuentaComisionVO.append(elementToMove)
            }
            if response.Comisiones?.listCuentaComisionVO[i].cuenta == "0000000003" {
                let elementToMove = (response.Comisiones?.listCuentaComisionVO.remove(at: i))!
                response.Comisiones?.listCuentaComisionVO.append(elementToMove)
            }
            if response.Comisiones?.listCuentaComisionVO[i].cuenta == "0000000004" {
                let elementToMove = (response.Comisiones?.listCuentaComisionVO.remove(at: i))!
                response.Comisiones?.listCuentaComisionVO.append(elementToMove)
            }
            if response.Comisiones?.listCuentaComisionVO[i].cuenta == "0000000005" {
                let elementToMove = (response.Comisiones?.listCuentaComisionVO.remove(at: i))!
                response.Comisiones?.listCuentaComisionVO.append(elementToMove)
            }
            if response.Comisiones?.listCuentaComisionVO[i].cuenta == "0000000006" {
                let elementToMove = (response.Comisiones?.listCuentaComisionVO.remove(at: i))!
                response.Comisiones?.listCuentaComisionVO.append(elementToMove)
            }
            if response.Comisiones?.listCuentaComisionVO[i].cuenta == "0000000007" {
                let elementToMove = (response.Comisiones?.listCuentaComisionVO.remove(at: i))!
                response.Comisiones?.listCuentaComisionVO.append(elementToMove)
            }
            if response.Comisiones?.listCuentaComisionVO[i].cuenta == "0000000008" {
                let elementToMove = (response.Comisiones?.listCuentaComisionVO.remove(at: i))!
                response.Comisiones?.listCuentaComisionVO.append(elementToMove)
            }
        }

        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        numberFormatter.groupingSize = 3
        numberFormatter.secondaryGroupingSize = 2
        comisionesObjeto3.sumaTotal = String(format: "%.2f", sumaProdMin)

            comisionesObjeto3.chargebSum = sumaProdMin.asLocaleCurrency
        sumaProdMin = 0
        observer?.succesProdMinComissions(response: comisionesObjeto3!)
    }

    func succesTicket(response: GetTicketResponse) {
        view.hideLoading()
        mAclaraciones = [Aclaraciones]()
        aclaracionesObjeto = Aclaraciones()
        mAclaraciones = (response.Aclaraciones)
        observer?.succesTicket(response: aclaracionesObjeto!)
    }

}
