//
//  OptionComboViewController.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 16/04/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class OptionComboViewController: BaseVentasView, UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerValues.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerValues[row]
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        valuePicker = pickerValues[row]
    }

    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label = (view as? UILabel) ?? UILabel()
        label.textColor = .black
        label.textAlignment = .center
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5
        label.text = pickerValues[row]
        // label.numberOfLines = 0
       // label.sizeToFit()
       // label.()
        return label
    }

    @IBOutlet weak var picker: UIPickerView!
    @IBOutlet weak var header: UIView!
    @IBOutlet weak var viewCombo: UIView!
    var valuePicker = ""
    var origin = ""
    var pickerValues: [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        design()
         if pickerValues[0] != nil {
            valuePicker = pickerValues[0]
        }
    }

    func design() {
        Constants.Border(view: viewCombo, radius: 6, width: 0)
        Constants.BorderCustom(view: header, radius: 6, width: 0, masked: [.layerMaxXMinYCorner, .layerMinXMinYCorner])
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        guard let location = touch?.location(in: self.view) else { return }
        if !viewCombo.frame.contains(location) {
           Constants.Back(viewC: self)

        }
    }

    @IBAction func selectPicker(_ sender: Any) {
        print(valuePicker)
        let valueOrigin = origin + "Picker"
        SwiftEventBus.post(valueOrigin, sender: valuePicker)
        Constants.Back(viewC: self)
    }

}
