//
//  OptionDateComboView.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 07/09/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class OptionDateComboView: BaseVentasView {
    @IBOutlet weak var viewCombo: UIView!
    @IBOutlet var picker: UIDatePicker!
    var valuePicker = ""
    var origin = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        picker.timeZone = TimeZone(abbreviation: "GMC")
        picker.date = DateCalendar.futureDateInterval30min(date: Date())!
        picker.locale = Locale(identifier: "es_MX")
        picker.minuteInterval = 30
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first as! UITouch
        if touch.view != viewCombo {
            self.dismiss(animated: false, completion: nil)
        }
    }

    @IBAction func selectPicker(_ sender: Any) {
        let valueOrigin = origin + "Picker"
        let dateString = DateCalendar.convertDateString(date: picker.date, formatter: Formate.HH_mm)
        SwiftEventBus.post(valueOrigin, sender: dateString)
        Constants.Back(viewC: self)
    }

}
