//
//  CanalEmpleadoModel.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 10/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import RealmSwift

public struct LoginEmpleadoType {
    static let canalEmpleado    : String = "CanalEmpleadoType"
    static let subCanalEmpleado : String = "SubcanalEmpleadoType"
}

protocol CanalEmpleadoModelObserver {
    func onError(errorMessage: String)
}

class CanalEmpleadoModel: BaseModel {

    var viewModel: CanalEmpleadoModelObserver?

    init(observer: BaseViewModelObserver) {
        super.init(observerVM: observer)
    }

    func atachModel(viewModel: CanalEmpleadoModelObserver) {
        self.viewModel = viewModel
    }

    func updateEmployee(canal: String, subCanal: String) {
        guard let employee = RealManager.findFirst(object: Empleado.self) else {
            return
        }
        RealManager.updateLine {
            employee.CanalSelect = canal
            employee.SubCanalSelect = subCanal
        }
    }

    func getCanales() -> [String] {
        guard let employee = RealManager.findFirst(object: Empleado.self) else {
            return []
        }
        var result: [String] = []
        for index in employee.Canales {
            result.append(index.nombre)
        }
        return result
    }

    func getSubcanal(nameCanal: String) -> [String] {
        guard let canal =  RealManager.getFromFirst(object: CanalesDB.self, field: "nombre", equalsTo: nameCanal) else {
            return []
        }
        var result: [String] = []
        result.append(contentsOf: canal.Subcanales)
       return result

    }

    func getCanalSelect() -> (String, String) {
        guard let employee = RealManager.findFirst(object: Empleado.self) else {
            return ("", "")
        }
        return (employee.CanalSelect, employee.SubCanalSelect)
    }

}
