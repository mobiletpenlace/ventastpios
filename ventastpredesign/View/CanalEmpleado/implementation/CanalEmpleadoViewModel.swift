//
//  CanalEmpleadoViewModel.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 10/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import SwiftEventBus

protocol CanalEmpleadoDelegate: AnyObject {
    func setCanal(canal: String)
    func setSubCanal(subCanal: String)
    func didDismissCanalEmpleadoView()
}

class CanalEmpleadoViewModel: BaseViewModel, CanalEmpleadoModelObserver {
   // var observer : CreateLeadObserver?
    var model: CanalEmpleadoModel!
    var viewS: CanalEmpleadoDelegate?

    override init(view: BaseVentasView) {
        super.init(view: view)
        model = CanalEmpleadoModel(observer: self)
        model.atachModel(viewModel: self)
    }

    func attachView(observer: CanalEmpleadoDelegate) {
        self.viewS = observer
        SwiftEventBus.onMainThread(self, name: "Set_CanalPicker") { [weak self] result in
            if let value = result?.object as? String {
                self?.viewS?.setCanal(canal: value)
            }
        }
        SwiftEventBus.onMainThread(self, name: "Set_SubCanalPicker") { [weak self] result in
            if let value = result?.object as? String {
                self?.viewS?.setSubCanal(subCanal: value)
            }
        }
    }

    func onError(errorMessage: String) {
    }

    func getCanalesDeVenta() -> [String] {
        return model.getCanales()
    }

    func getSubcanalDeVenta(canal: String) -> [String] {
        return model.getSubcanal(nameCanal: canal)
    }

    func updateCanalEmpleado(canal: String, SubCanal: String) {
        model.updateEmployee(canal: canal, subCanal: SubCanal)
    }

    func getCanalSelected() -> (String, String) {
        return model.getCanalSelect()
    }

}

extension CanalEmpleadoDelegate {
    func didDismissCanalEmpleadoView() {
    }
}
