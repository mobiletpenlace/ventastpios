//
//  CanalEmpleadoView.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 10/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTextControls_FilledTextFields
import Amplitude

class CanalEmpleadoView: BaseVentasView, CanalEmpleadoDelegate {

    @IBOutlet var subCanalView: UIView!
    @IBOutlet var heightViewConstraint: NSLayoutConstraint!
    @IBOutlet var canaltextfield: MDCFilledTextField!
    @IBOutlet var subCanalTextfield: MDCFilledTextField!
    @IBOutlet weak var confirmButton: UIButton!

    var canalVenta: [String] = []
    var subCanal: [String] = []
    var isThereSubCanal = false
    var mCanalEmpleadoViewModel: CanalEmpleadoViewModel!
    var currentCanal = ""
    var currentSubCanal = ""
    var inicialCanal = ""
    var inicialSubcanal = ""
    weak var delegate: CanalEmpleadoDelegate?

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        delegate?.didDismissCanalEmpleadoView()
        NotificationCenter.default.post(name: Notification.Name("CanalEmpleadoViewClosed"), object: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mCanalEmpleadoViewModel = CanalEmpleadoViewModel(view: self)
        mCanalEmpleadoViewModel.attachView(observer: self)
        configTextFields()
        clear()
        getDataDefault()
        customView()
        inicialCanal = currentCanal
        inicialSubcanal = currentSubCanal
        if currentCanal == "Punto de Venta" || currentCanal == "Convenios" {
            switch currentCanal {
            case "Punto de Venta":
                canaltextfield.text = "Punto de Venta"
                subCanal = mCanalEmpleadoViewModel.getSubcanalDeVenta(canal: "Punto de Venta")
            default:
                canaltextfield.text = "Desarrollos"
                subCanal = mCanalEmpleadoViewModel.getSubcanalDeVenta(canal: "Convenios")
            }

        }
    }

    @IBAction func goBack(_ sender: Any) {
        Constants.Back(viewC: self)
    }

    @IBAction func canalVentaAction(_ sender: Any) {
        Constants.OptionCombo(origen: "Set_Canal", array: Array(canalVenta), viewC: self)
    }

    @IBAction func subcanalVentaAction(_ sender: Any) {
        if subCanal.count == 0 {
            self.showToast2(message: "No se encuentran subcanales", font: UIFont(name: "Helvetica Neue", size: 11)!, time: 4.0)
        } else {
            Constants.OptionCombo(origen: "Set_SubCanal", array: Array(subCanal), viewC: self)
        }

    }

    @IBAction func acceptAction(_ sender: Any) {

        mCanalEmpleadoViewModel.updateCanalEmpleado(canal: currentCanal, SubCanal: currentSubCanal)
        Constants.Alert(title: "", body: dialogs.Success.canalEmpleado, type: type.Success, viewC: self, dimissParent: true)
            Amplitude.instance().logEvent("Channel_Changed", withEventProperties: ["Previus Channel": inicialCanal, "Previus Subchannel": inicialSubcanal, "Actual Channel": currentCanal, "Actual Subchannel": currentSubCanal])
    }

    func configTextFields() {
        canaltextfield.normalTheme(type: 2)
        subCanalTextfield.normalTheme(type: 2)
    }

    func getDataDefault() {
        canalVenta = mCanalEmpleadoViewModel.getCanalesDeVenta()
        let canalSaved = mCanalEmpleadoViewModel.getCanalSelected().0
        let subcanalSaved = mCanalEmpleadoViewModel.getCanalSelected().1
        if canalSaved != "" {
            canaltextfield.text = canalSaved
            currentCanal = canalSaved
            if subcanalSaved != "" {
                subCanalTextfield.text = subcanalSaved
                currentSubCanal = subcanalSaved
                isThereSubCanal = true
                customView()
            }
        }
        updateButonStatus()
    }

    func customView() {
        if isThereSubCanal {
            subCanalView.isHidden = false
            heightViewConstraint.constant = 380
        } else {
            subCanalView.isHidden = true
            heightViewConstraint.constant = 300
        }
    }

    func setCanal(canal: String) {
        canaltextfield.text = canal
        subCanalTextfield.text = ""
        currentCanal = canal
        currentSubCanal = ""
        subCanal = mCanalEmpleadoViewModel.getSubcanalDeVenta(canal: canal)
        if subCanal.count > 0 {
            isThereSubCanal = true
            customView()
        } else {
            isThereSubCanal = false
            customView()
        }
        updateButonStatus()
    }

    func setSubCanal(subCanal: String) {
        subCanalTextfield.text = subCanal
        currentSubCanal = subCanal
        updateButonStatus()
    }

    func clear() {
        subCanalTextfield.text = ""
        canaltextfield.text = ""
    }

    func updateButonStatus() {
        let canalText = canaltextfield.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        let subCanalText = subCanalTextfield.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""

        if (canalText == "Cambaceo" || canalText == "Desarrollos"  || canalText == "Recuperación" || (canalText == "Punto de Venta" && !subCanalText.isEmpty) || (canalText == "Convenios" && !subCanalText.isEmpty)) {
            confirmButton.isEnabled = true
            confirmButton.titleLabel?.textColor = .systemBlue
        } else {
            confirmButton.isEnabled = false
            confirmButton.titleLabel?.textColor = .systemGray
        }
    }

    @objc func textFieldsDidChange() {
        updateButonStatus()
    }

}
