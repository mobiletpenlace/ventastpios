//
//  Data.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera on 01/08/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class DataNewClient: BaseRequest {
    var Nombre: String = ""
    var Telefono: String = ""

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        self.Nombre <- map["Nombre"]
        self.Telefono <- map["Telefono"]
    }

}
