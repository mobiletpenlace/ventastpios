//
//  NewClientModel.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera on 31/07/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

protocol NewClientModelObserver {
    func successToken()
    func successEvents()
    func onError(errorMessage: String)
}

class NewClientModel: BaseModel {
    var viewModel: NewClientModelObserver?
    var serverCloudApiToken: ServerDataManager2<CloudApiTokenResponse>?
    var serverCloudEvents: ServerDataManager2<CloudApiEventsResponse>?
    var urlCloudApiToken = AppDelegate.API_CLOUD_TOKEN
    var urlCloudEvents = AppDelegate.API_CLOUD_EVENTS

    init (observer: BaseViewModelObserver) {
        super.init(observerVM: observer)
        serverCloudApiToken = ServerDataManager2<CloudApiTokenResponse>()
        serverCloudEvents = ServerDataManager2<CloudApiEventsResponse>()
    }

    func attachModel(viewModel: NewClientModelObserver) {
        self.viewModel = viewModel
    }

    func getToken() {
        let request = CloudApiTokenRequest()
        request.client_secret = "SUe9O0LuUvarwqi7mj3p7EDe"
        request.account_id = "7295536"
        request.client_id = "hhle7x22kj4upzblgnr1zkdu"
        request.grant_type = "client_credentials"
        serverCloudApiToken?.setValues(requestUrl: urlCloudApiToken, delegate: self, headerType: .headersCloudToken)
        serverCloudApiToken?.request(requestModel: request)
    }

    func sendEvent() {
        let request = CloudApiEventsRequest()
        let dataNewClient: DataNewClient = DataNewClient()
        request.Data = DataNewClient()
        request.ContactKey = faseNuevoCliente.number
        request.Data?.Nombre = faseNuevoCliente.name
        request.Data?.Telefono = faseNuevoCliente.number
        request.EstablishContactKey = true
        request.EventDefinitionKey = "APIEvent-9fe4f386-5c90-b72f-c19e-924be935ef6c "
        serverCloudEvents?.setValues(requestUrl: urlCloudEvents, delegate: self, headerType: .headersCloudEvents)
        serverCloudEvents?.request(requestModel: request)
    }

    override func onErrorLoadResponse(requestUrl: String, messageError: String) {
        viewModel?.onError(errorMessage: messageError)
    }

    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if requestUrl == urlCloudApiToken {
            let cloudApiTokenResponse = response as! CloudApiTokenResponse
            if !cloudApiTokenResponse.access_token.isEmpty {
                statics.CLOUD_TOKEN = cloudApiTokenResponse.access_token
                viewModel?.successToken()
            } else {
                viewModel?.onError(errorMessage: "Error al obtener el token de Cloud")
            }
        }
        if requestUrl == urlCloudEvents {
            let cloudApiEventsResponse = response as! CloudApiEventsResponse
            if !cloudApiEventsResponse.eventInstanceId.isEmpty {
                viewModel?.successEvents()
            } else {
                viewModel?.onError(errorMessage: "Error al obtener el token de Cloud")
            }
        }
    }

}
