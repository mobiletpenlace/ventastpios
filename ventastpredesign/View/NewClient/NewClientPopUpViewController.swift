//
//  NewClientPopUpViewController.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera on 11/05/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import MaterialComponents.MaterialTextControls_FilledTextAreas
import MaterialComponents.MaterialTextControls_FilledTextFields

protocol NewClientPopUpViewControllerProtocol {
}

class NewClientPopUpViewController: BaseVentasView, NewClientObserver {
    func successEvents() {
        var runCount = 3
        Timer.scheduledTimer(withTimeInterval: 0.75, repeats: true) { [self] timer in
            runCount -= 1
            sendMessageButton.setTitle("Envio Exitoso", for: .normal)
            if runCount == 0 {
                self.dismiss(animated: true)
                timer.invalidate()
            }
        }
    }

    func successGetToken() {
        print("")
    }

    func onError(errorMessage: String) {
        var runCount = 3
        Timer.scheduledTimer(withTimeInterval: 0.75, repeats: true) { [self] timer in
            runCount -= 1
            sendMessageButton.backgroundColor = UIColor.red
            sendMessageButton.setTitle("Fallo el envio", for: .normal)
            if runCount == 0 {
                self.dismiss(animated: true)
                timer.invalidate()
            }
        }
    }

    var manager: NewClientPopUpViewControllerProtocol!
    @IBOutlet weak var cortinaView: UIView!
    @IBOutlet weak var numberTextField: NumberTextField! {
        didSet {
            numberTextField.normalTheme(type: 2)
        }
    }
    @IBOutlet weak var nameTextField: MDCFilledTextField! {
        didSet {
            nameTextField.normalTheme(type: 2)
        }
    }
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var closeViewButton: UIButton!
    @IBOutlet weak var sendMessageButton: UIButton!

    var validName = false
    var validPhone = false
    var mNewClientViewModel: NewClientViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mNewClientViewModel = NewClientViewModel(view: self)
        mNewClientViewModel.attachView(observer: self)
    }

    @IBAction func closeViewAction(_ sender: Any) {
        self.dismiss(animated: true)
    }

    @IBAction func sendMessageAction(_ sender: Any) {
        mNewClientViewModel.getToken()
        let number = String(numberTextField.text!.dropFirst(4))
        faseNuevoCliente.number = number
        faseNuevoCliente.name = nameTextField.text ?? ""
        sendMessageButton.isUserInteractionEnabled = false
    }

    @IBAction func numberDidEndEditing(_ sender: NumberTextField) {
        guard let number = sender.getNumber() else { return }
        sender.validateNumber()
        if sender.isValid {
            validPhone = true
            if validName && validPhone {
                sendMessageButton.isEnabled = true
            }
        } else {
            validPhone = false
            sendMessageButton.isEnabled = false
        }
    }

    @IBAction func nameDidEndEditing(_ sender: MDCFilledTextField) {
        if !(sender.text?.count ?? 0 > 0) {
            validName = false
            sendMessageButton.isEnabled = false
            sender.leadingAssistiveLabel.isHidden = false
            sender.setTextColor(UIColor.red, for: .normal)
            sender.setNormalLabelColor(UIColor.red, for: .normal)
            sender.setFloatingLabelColor(UIColor.red, for: .normal)
            sender.setLeadingAssistiveLabelColor(UIColor.red, for: .normal)
            sender.leadingAssistiveLabel.text = "Ingresa un nombre"
        } else {
            validName = true
            if validName && validPhone {
                sendMessageButton.isEnabled = true
            }
            sender.leadingAssistiveLabel.isHidden = true
            sender.leadingAssistiveLabel.text = ""
            sender.setFilledBackgroundColor(UIColor.clear, for: .normal)
            sender.setFilledBackgroundColor(UIColor.clear, for: .editing)
            sender.setTextColor(UIColor(named: "Base_rede_ligth_gray_titles")!, for: .normal)
            sender.setNormalLabelColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .normal)
            sender.setFloatingLabelColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .normal)
            sender.setUnderlineColor(UIColor(named: "Base_rede_ligth_gray_titles")!, for: .normal)
            sender.setUnderlineColor(UIColor(named: "Base_rede_ligth_gray_titles")!, for: .editing)
        }
    }

    func configTextFields() {
        nameTextField.normalTheme(type: 2)
    }

    func enableButton() {
        sendMessageButton.backgroundColor = UIColor(named: "Base_rede_button_dark")
        sendMessageButton.isEnabled = true
    }

    func disableButton() {
        sendMessageButton.backgroundColor = .gray
        sendMessageButton.isEnabled = false
    }
}
