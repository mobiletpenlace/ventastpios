//
//  NewClientViewModel.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera on 31/07/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import SwiftEventBus

protocol NewClientObserver {
    func successEvents()
    func successGetToken()
    func onError(errorMessage: String)
}

class NewClientViewModel: BaseViewModel, NewClientModelObserver {

    func successEvents() {
        view.hideLoading()
        observer?.successEvents()
    }

    func successToken() {
        view.hideLoading()
        observer?.successEvents()
        model.sendEvent()
    }

    func onError(errorMessage: String) {
        view.hideLoading()
        observer?.onError(errorMessage: errorMessage)
    }

    var observer: NewClientObserver!
    var model: NewClientModel!

    override init(view: BaseVentasView) {
        super.init(view: view)
        model = NewClientModel(observer: self)
        model.attachModel(viewModel: self)
    }

    func attachView(observer: NewClientObserver) {
        self.observer = observer
    }

    func viewWillDisappear() {
        SwiftEventBus.unregister(self)
    }

    func getToken() {
        view.showLoading(message: "")
        model.getToken()
    }
}
