//
//  CloudApiEventsResponse.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera on 01/08/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class CloudApiEventsResponse: BaseResponse {
    var eventInstanceId: String = ""

    required init?(map: Map) {
}
    override func mapping(map: Map) {
        self.eventInstanceId <- map["eventInstanceId"]
    }
}
