//
//  CloudApiTokenRequest.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera on 31/07/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import ObjectMapper

public class CloudApiTokenRequest: BaseRequest {
    var client_secret: String = ""
    var account_id: String = ""
    var client_id: String = ""
    var grant_type: String = ""

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override public func mapping(map: Map) {
        client_secret <- map["client_secret"]
        account_id <- map["account_id"]
        client_id <- map["client_id"]
        grant_type <- map["grant_type"]
    }
}
