//
//  CloudApiEventsRequest.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera on 01/08/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import ObjectMapper

public class CloudApiEventsRequest: BaseRequest {
    var ContactKey: String = ""
    var Data: DataNewClient?
    var EstablishContactKey: Bool = false
    var EventDefinitionKey: String = ""

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override public func mapping(map: Map) {
        ContactKey <- map["ContactKey"]
        Data <- map["Data"]
        EstablishContactKey <- map["EstablishContactKey"]
        EventDefinitionKey <- map["EventDefinitionKey"]
    }
}
