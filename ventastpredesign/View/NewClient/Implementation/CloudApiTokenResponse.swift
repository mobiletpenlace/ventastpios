//
//  Response.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera on 31/07/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class CloudApiTokenResponse: BaseResponse {
    var access_token: String = ""
    var token_type: String = ""
    var expires_in: String = ""
    var scope: String = ""
    var soap_instance_url: String = ""
    var rest_instance_url: String = ""

    required init?(map: Map) {
}
    override func mapping(map: Map) {
        self.access_token <- map["access_token"]
        self.token_type <- map["token_type"]
        self.expires_in <- map["expires_in"]
        self.scope <- map["scope"]
        self.soap_instance_url <- map["soap_instance_url"]
        self.rest_instance_url <- map["rest_instance_url"]
    }
}
