//
//  CustomTextField.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera  on 27/12/24.
//  Copyright © 2024 TotalPlay. All rights reserved.
//

import SwiftUI

struct CustomTextField: View {
    var placeholder: String
    @Binding var text: String
    var imageName: String?
    var isError: Bool = false

    var body: some View {
        VStack(alignment: .leading, spacing: 4) {
            HStack {
                if let imageName = imageName {
                    Image(systemName: imageName)
                        .foregroundColor(isError ? .red : Color("AppLeadsColor"))
                        .font(.body)
                }

                TextField(placeholder, text: $text)
                    .padding(8)
                    .background(Color.gray.opacity(0.1))
                    .cornerRadius(8)
                    .overlay(
                        RoundedRectangle(cornerRadius: 8)
                            .stroke(isError ? Color.red : Color.clear, lineWidth: 1)
                    )
                    .font(.body)
            }

            if isError {
                Text("Campo inválido")
                    .font(.caption)
                    .foregroundColor(.red)
            }
        }
    }
}
