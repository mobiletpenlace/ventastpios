//
//  LeadStatus.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera  on 04/11/24.
//  Copyright © 2024 TotalPlay. All rights reserved.
//

import Foundation

struct LeadStatus {
    let title: String
    let count: Int
}
