//
//  HomeLeadsView.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera  on 23/12/24.
//  Copyright © 2024 TotalPlay. All rights reserved.
//

import SwiftUI

struct HomeLeadsView: View {
    var body: some View {
        NavigationView {
            VStack(spacing: 0) {
                HStack {
                    Button(action: {
                        print("Hamburguesa presionada")
                    }) {
                        Image(systemName: "line.horizontal.3")
                            .font(.title)
                            .foregroundColor(.black)
                    }
                    Spacer()
                    Image("AppLeads_logo")
                        .resizable()
                        .scaledToFit()
                        .frame(height: 30)
                    Spacer()

                    Button(action: {
                        print("Perfil presionado")
                    }) {
                        Image(systemName: "person.circle.fill")
                            .font(.title)
                            .foregroundColor(.black)
                    }
                }
                .frame(height: 40)
                .padding(.horizontal, 16)
                .background(Color.white)
                .shadow(color: .gray.opacity(0.1), radius: 4, x: 0, y: 2)


                Text("¡Hola, Diana Garcia!")
                    .font(.title)
                    .fontWeight(.medium)
                    .foregroundColor(.gray)
                    .padding(.top, 20)
                    .padding(.bottom, 20)

                HStack(spacing: 80) {
                    NavigationLink(destination: NuevoLeadView()) {
                        VStack {
                            Image("AppLeadsToDo")
                                .font(.system(size: 60))
                                .foregroundColor(.green)
                            Text("Nuevo Lead")
                                .font(.headline)
                                .foregroundColor(.black)
                        }
                    }

                    Button(action: {
                        print("Estatus Lead presionado")
                    }) {
                        VStack {
                            Image(systemName: "person.crop.circle.badge.plus")
                                .font(.system(size: 60))
                                .foregroundColor(.green)
                            Text("Estatus Lead")
                                .font(.headline)
                                .foregroundColor(.black)
                        }
                    }
                }

                Spacer()
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            .background(Color(UIColor.systemGroupedBackground))
        }
    }
}

struct HomeLeadsView_Previews: PreviewProvider {
    static var previews: some View {
        HomeLeadsView()
    }
}
