//
//  NuevoLeadView.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera on 23/12/24.
//  Copyright © 2024 TotalPlay. All rights reserved.
//

import SwiftUI

struct NuevoLeadView: View {
    @State private var nombre: String = ""
    @State private var apellidoPaterno: String = ""
    @State private var apellidoMaterno: String = ""
    @State private var telefono: String = ""
    @State private var correo: String = ""
    @State private var notas: String = ""

    @State private var isNombreError: Bool = false
    @State private var isApellidoPaternoError: Bool = false
    @State private var isApellidoMaternoError: Bool = false
    @State private var isTelefonoError: Bool = false
    @State private var isCorreoError: Bool = false

    @FocusState private var focusedField: Field? // Estado de enfoque

    enum Field: Hashable {
        case nombre, apellidoPaterno, apellidoMaterno, telefono, correo, notas
    }

    var body: some View {
        ScrollView {
            VStack(spacing: 16) {
                VStack(alignment: .leading, spacing: 16) {
                    HStack {
                        Image(systemName: "person.fill.checkmark")
                            .font(.title2)
                            .foregroundColor(Color("AppLeadsColor"))
                        Text("Interesado en contratar:")
                            .font(.body)
                    }

                    Group {
                        CustomTextField(placeholder: "Nombre (s)", text: $nombre)
                            .focused($focusedField, equals: .nombre)
                            .submitLabel(.next)
                        CustomTextField(placeholder: "Apellido Paterno", text: $apellidoPaterno)
                            .focused($focusedField, equals: .apellidoPaterno)
                            .submitLabel(.next)
                        CustomTextField(placeholder: "Apellido Materno", text: $apellidoMaterno)
                            .focused($focusedField, equals: .apellidoMaterno)
                            .submitLabel(.next)
                        CustomTextField(placeholder: "Teléfono", text: $telefono, imageName: "phone.fill")
                            .keyboardType(.numberPad)
                            .focused($focusedField, equals: .telefono)
                            .submitLabel(.next)
                        CustomTextField(placeholder: "Correo electrónico", text: $correo, imageName: "envelope.fill")
                            .focused($focusedField, equals: .correo)
                            .submitLabel(.next)
                        CustomTextField(placeholder: "Notas", text: $notas)
                            .focused($focusedField, equals: .notas)
                            .submitLabel(.done)
                    }
                }
                .padding()
            }

            Spacer()

            // Botón flotante
            HStack {
                Spacer()
                Button(action: validateForm) {
                    ZStack {
                        Circle()
                            .fill(Color("AppLeadsColor"))
                            .frame(width: 56, height: 56)
                        Image(systemName: "paperplane.fill")
                            .foregroundColor(.white)
                            .font(.title2)
                    }
                }
                .padding()
            }
        }
        .background(Color.white.ignoresSafeArea())
        .navigationBarTitle("+NUEVO LEAD", displayMode: .inline)
        .toolbar {
            ToolbarItemGroup(placement: .keyboard) {
                Button("Anterior") {
                    focusPrevious()
                }
                Button("Siguiente") {
                    focusNext()
                }
                Spacer()
                Button("Cerrar") {
                    hideKeyboard()
                }
            }
        }
        .onSubmit {
            focusNext()
        }
    }

    private func focusNext() {
        switch focusedField {
        case .nombre: focusedField = .apellidoPaterno
        case .apellidoPaterno: focusedField = .apellidoMaterno
        case .apellidoMaterno: focusedField = .telefono
        case .telefono: focusedField = .correo
        case .correo: focusedField = .notas
        default: focusedField = nil
        }
    }

    private func focusPrevious() {
        switch focusedField {
        case .apellidoPaterno: focusedField = .nombre
        case .apellidoMaterno: focusedField = .apellidoPaterno
        case .telefono: focusedField = .apellidoMaterno
        case .correo: focusedField = .telefono
        case .notas: focusedField = .correo
        default: focusedField = nil
        }
    }

    private func hideKeyboard() {
        focusedField = nil
    }

    func validateForm() {
        isNombreError = nombre.trimmingCharacters(in: .whitespaces).isEmpty
        isApellidoPaternoError = apellidoPaterno.trimmingCharacters(in: .whitespaces).isEmpty
        isApellidoMaternoError = apellidoMaterno.trimmingCharacters(in: .whitespaces).isEmpty
        isTelefonoError = telefono.trimmingCharacters(in: .whitespaces).count != 10 || !telefono.isNumeric
        isCorreoError = !correo.matches(regex: "^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}$")
    }
}
