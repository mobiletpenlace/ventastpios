//
//  LeadDetailViewController.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera  on 04/11/24.
//  Copyright © 2024 TotalPlay. All rights reserved.
//

import UIKit

class LeadDetailViewController: UIViewController {

    private let status: LeadStatus

    init(status: LeadStatus) {
        self.status = status
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
        title = status.title

        let titleLabel = UILabel()
        titleLabel.text = "Detalles para \(status.title)"
        titleLabel.font = UIFont.systemFont(ofSize: 24, weight: .bold)
        titleLabel.textAlignment = .center
        titleLabel.translatesAutoresizingMaskIntoConstraints = false

        view.addSubview(titleLabel)

        NSLayoutConstraint.activate([
            titleLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            titleLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
    }
}
