//
//  LeadRequest.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera  on 31/10/24.
//  Copyright © 2024 TotalPlay. All rights reserved.
//

import Foundation

struct Lead {
    var nombre: String
    var apellidoPaterno: String
    var apellidoMaterno: String
    var telefono: String
    var email: String
    var comentario: String
    var canalReferido: String
    var estatus: String
    var origen: String
    var subCanal: String
    var ventaOrigen: String
}
