//
//  CustomAlertViewController.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera  on 04/11/24.
//  Copyright © 2024 TotalPlay. All rights reserved.
//

import UIKit

class CustomAlertViewController: UIViewController {

    private let titleLabel = UILabel()
    private let messageLabel = UILabel()
    private let imageView = UIImageView()
    private let acceptButton = UIButton(type: .system)

    private let result: String


    init(title: String, subtitle: String, result: String) {
        self.result = result
        super.init(nibName: nil, bundle: nil)
        setupUI(title: title, subtitle: subtitle)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupUI(title: String, subtitle: String) {
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)

        let containerView = UIView()
        containerView.backgroundColor = .white
        containerView.layer.cornerRadius = 12
        containerView.clipsToBounds = true
        containerView.translatesAutoresizingMaskIntoConstraints = false

        view.addSubview(containerView)

        titleLabel.text = title
        titleLabel.font = UIFont.boldSystemFont(ofSize: 18)
        titleLabel.textAlignment = .center
        titleLabel.translatesAutoresizingMaskIntoConstraints = false

        messageLabel.text = subtitle
        messageLabel.font = UIFont.systemFont(ofSize: 14)
        messageLabel.textAlignment = .center
        messageLabel.translatesAutoresizingMaskIntoConstraints = false

        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        setupImage()

        acceptButton.setTitle("Aceptar", for: .normal)
        acceptButton.backgroundColor = UIColor(named: "AppLeads_Color")
        acceptButton.setTitleColor(.white, for: .normal)
        acceptButton.layer.cornerRadius = 10
        acceptButton.translatesAutoresizingMaskIntoConstraints = false
        acceptButton.addTarget(self, action: #selector(dismissAlert), for: .touchUpInside)

        containerView.addSubview(titleLabel)
        containerView.addSubview(messageLabel)
        containerView.addSubview(imageView)
        containerView.addSubview(acceptButton)

        setupConstraints(for: containerView)
    }

    private func setupConstraints(for containerView: UIView) {
        NSLayoutConstraint.activate([
            containerView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            containerView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            containerView.widthAnchor.constraint(equalToConstant: 300),
            containerView.heightAnchor.constraint(greaterThanOrEqualToConstant: 200),

            titleLabel.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 20),
            titleLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
            titleLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),

            imageView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 10),
            imageView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            imageView.widthAnchor.constraint(equalToConstant: 100),
            imageView.heightAnchor.constraint(equalToConstant: 100),

            messageLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 10),
            messageLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
            messageLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),

            acceptButton.topAnchor.constraint(equalTo: messageLabel.bottomAnchor, constant: 20),
            acceptButton.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -20),
            acceptButton.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
            acceptButton.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),
            acceptButton.heightAnchor.constraint(equalToConstant: 44)
        ])
    }

    private func setupImage() {
        if result == "No se inserto correctamente" {
            imageView.image = UIImage(named: "icon_Event_erased")
        } else {
            imageView.image = UIImage(named: "icon_event_planned")
        }
    }

    @objc private func dismissAlert() {
        dismiss(animated: true, completion: nil)
    }
}
