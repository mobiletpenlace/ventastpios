//
//  HomeLeadsViewController.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera  on 04/11/24.
//  Copyright © 2024 TotalPlay. All rights reserved.
//

import UIKit
import CoreLocation
import Amplitude

class HomeLeadsViewController: BaseVentasView {
    @IBOutlet weak var mNameLabel: UILabel!

    @IBOutlet weak var newLeadButton: UIButton! {
        didSet {
            newLeadButton.addTarget(self, action: #selector(handleNewLead), for: .touchUpInside)
        }
    }

    @IBOutlet weak var statusLeadButton: UIButton! {
        didSet {
            statusLeadButton.addTarget(self, action: #selector(handleStatusLead), for: .touchUpInside)
        }
    }

    @objc func handleNewLead() {
        print("Botón de nuevo lead presionado")

           let nuevoLeadVC = NuevoLeadViewController()
           if let navController = self.navigationController {
               navController.pushViewController(nuevoLeadVC, animated: true)
           } else {
               print("El navigationController es nil")
           }
    }

    @objc func handleStatusLead() {
           let leadStatusVC = LeadStatusViewController()
           if let navController = self.navigationController {
               navController.pushViewController(leadStatusVC, animated: true)
           } else {
               print("El navigationController es nil")
           }
    }


    var MenuVC: MenuViewController!
    var mEventViewModel: EventViewModel!
    var employed: EmployeeStreet?
    var currentProfile: String = ""
    let locationManager = CLLocationManager()
    var nameProfile = ["Coordinador", "Supervisor", "Promotor"]

    override func viewDidLoad() {
        super.viewDidLoad()
        mEventViewModel = EventViewModel(view: self)
        setUpView()
    }


    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    override func viewDidAppear(_ animated: Bool) {
        Amplitude.instance().logEvent("Show_HomeScreen")
    }

    @IBAction func MenuAction(_ sender: Any) {

        let viewAlert: UIStoryboard = UIStoryboard(name: "LoginVC", bundle: nil)
        let viewAlertVC = viewAlert.instantiateViewController(withIdentifier: "LoginVC")as! LoginVC
        self.present(viewAlertVC, animated: false, completion: {})
        /*if AppDelegate.menu_bool{
            ShowMenu()
        }else{
            CloseMenu()
        }*/
    }

    func setUpView() {
        getEmploye()
        validateFirebase()
        configLocation()
        // setMenu()

    }

    func validateFirebase() {
        mEventViewModel.authUser(customToken: mEventViewModel.getEmploye()?.tokenFirebase ?? "")
        if  mEventViewModel.getEmploye()?.connectFirebase == "0" {
            Constants.Alert(title: "", body: "No se pudo conectar a la base de datos de Firebase", type: type.Warning, viewC: self)
        }
    }

    func getEmploye() {
        guard let employee = mEventViewModel.getEmploye() else {return}
        employed = employee
        currentProfile = employed?.profileUser ?? "0"
        mNameLabel.text =  " \(nameProfile[(Int(currentProfile) ?? 0) - 1]) | \( employed?.numEmployee ?? "") "

    }

    @objc func HostesNewLeadAction() {
        Constants.LoadStoryBoard(name: storyBoard.Street.createLead, viewC: self)
    }

    @objc func HostesConsultEventAction() {
        Constants.LoadStoryBoard(name: storyBoard.Street.homePromotor, viewC: self)
    }

    @objc func HostesEstatusLeadAction() {
        Constants.LoadStoryBoard(name: storyBoard.Street.statusleadPromotor, viewC: self)
    }

    @objc func SupervEstatusLeadAction() {
        Constants.LoadStoryBoard(name: storyBoard.Street.statusLeadSuper, viewC: self)
    }

    @objc func SupervConsultEventAction() {
        Constants.LoadStoryBoard(name: storyBoard.Street.homeSupervisor, viewC: self)
    }

    @objc func SupervAddEventAction() {
        Constants.LoadStoryBoard(name: storyBoard.Street.createEventSuper, viewC: self)
    }

    @objc func SupervMissingAction() {
        Constants.LoadStoryBoardBoot(storyboard: storyBoard.Street.reasonMissing, identifier: storyBoard.Street.reasonMissing, viewC: self)
    }

    @objc func CoordiConsultEventAction() {
        Constants.LoadStoryBoard(name: storyBoard.Street.consultEventCoord, viewC: self)
    }

    @objc func CoordiAddEventAction() {
        Constants.LoadStoryBoard(name: storyBoard.Street.newEventCoord, viewC: self)
    }

    @objc func CoordiPlanningAction() {
        Constants.LoadStoryBoard(name: storyBoard.Street.planningCoord, viewC: self)
    }

    func setMenu() {
        MenuVC = UIStoryboard(name: "MenuViewController", bundle: nil).instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        MenuVC.currentProfile = "street"
        let SwipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToGesture))
        SwipeRight.direction = UISwipeGestureRecognizerDirection.right
        let SwipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToGesture))
        SwipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(SwipeRight)
        self.view.addGestureRecognizer(SwipeLeft)
        self.MenuVC.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height )
        MenuVC.mNombreLabel.text = mNameLabel.text

    }

    @objc func respondToGesture(gesture: UISwipeGestureRecognizer) {
        switch gesture.direction {
        case UISwipeGestureRecognizerDirection.right:
            ShowMenu()
        case UISwipeGestureRecognizerDirection.left:
            CloseMenu()
        default:
            break
        }
    }

    func ShowMenu() {
        UIView.animate(withDuration: 0.5) {() -> Void in
            self.MenuVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height )
            self.MenuVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.0)
            self.addChildViewController(self.MenuVC)
            self.view.addSubview(self.MenuVC.view)
            AppDelegate.menu_bool = false
        }
        UIView.animate(withDuration: 1.00) {() -> Void in
            self.MenuVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        }
    }

    func CloseMenu() {
        self.MenuVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.0)
        UIView.animate(withDuration: 0.5, animations: {
            self.MenuVC.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        }) {(_) in
            self.MenuVC.view.removeFromSuperview()
        }
        AppDelegate.menu_bool = true
    }

}

extension HomeLeadsViewController: CLLocationManagerDelegate {
    func configLocation() {
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        stopLocation()
        mEventViewModel.updateLocation(latitude: locValue.latitude, longitude: locValue.longitude)
        print("locations = \(locValue.latitude) \(locValue.longitude)")

    }

    func stopLocation() {
        if CLLocationManager.locationServicesEnabled() {
            locationManager.stopUpdatingLocation()
        }
    }
}
