//
//  LeadStatusViewController.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera  on 04/11/24.
//  Copyright © 2024 TotalPlay. All rights reserved.
//

import UIKit

class LeadStatusViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    private let tableView = UITableView()
    private let dateFilterButton = UIButton(type: .system)

    private let leadStatuses: [LeadStatus] = [
        LeadStatus(title: "Por contactar", count: 1),
        LeadStatus(title: "Por instalar", count: 0),
        LeadStatus(title: "No exitoso", count: 0),
        LeadStatus(title: "Instalados", count: 0)
    ]

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
        setupTableView()
        setupDateFilterButton()

        title = "Estatus de Leads"
        navigationItem.title = "Estatus de Leads"
    }

    private func setupTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "LeadStatusCell")
        tableView.separatorStyle = .none
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = UIColor(white: 0.95, alpha: 1)
        view.addSubview(tableView)

        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -100)
        ])
    }

    private func setupDateFilterButton() {
        dateFilterButton.setTitle("📅", for: .normal)
        dateFilterButton.setTitleColor(.white, for: .normal)
        dateFilterButton.backgroundColor = .systemGreen
        dateFilterButton.layer.cornerRadius = 40
        dateFilterButton.translatesAutoresizingMaskIntoConstraints = false
        dateFilterButton.addTarget(self, action: #selector(showDateRangePicker), for: .touchUpInside)

        view.addSubview(dateFilterButton)

        NSLayoutConstraint.activate([
            dateFilterButton.widthAnchor.constraint(equalToConstant: 80),
            dateFilterButton.heightAnchor.constraint(equalToConstant: 80),
            dateFilterButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            dateFilterButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -16)
        ])
    }

    @objc private func showDateRangePicker() {
        let alertController = UIAlertController(title: "Seleccionar Rango de Fechas", message: nil, preferredStyle: .actionSheet)

        let startDatePicker = UIDatePicker()
        startDatePicker.datePickerMode = .date
        if #available(iOS 13.4, *) {
            startDatePicker.preferredDatePickerStyle = .wheels
        } else {
        }
        startDatePicker.translatesAutoresizingMaskIntoConstraints = false

        let endDatePicker = UIDatePicker()
        endDatePicker.datePickerMode = .date
        if #available(iOS 13.4, *) {
            endDatePicker.preferredDatePickerStyle = .wheels
        } else {

        }
        endDatePicker.translatesAutoresizingMaskIntoConstraints = false

        let pickerStackView = UIStackView(arrangedSubviews: [startDatePicker, endDatePicker])
        pickerStackView.axis = .vertical
        pickerStackView.spacing = 10
        pickerStackView.translatesAutoresizingMaskIntoConstraints = false

        alertController.view.addSubview(pickerStackView)

        NSLayoutConstraint.activate([
            pickerStackView.leadingAnchor.constraint(equalTo: alertController.view.leadingAnchor, constant: 8),
            pickerStackView.trailingAnchor.constraint(equalTo: alertController.view.trailingAnchor, constant: -8),
            pickerStackView.topAnchor.constraint(equalTo: alertController.view.topAnchor, constant: 40),
            pickerStackView.heightAnchor.constraint(equalToConstant: 200)
        ])

        let confirmAction = UIAlertAction(title: "Aceptar", style: .default) { _ in
            let startDate = startDatePicker.date
            let endDate = endDatePicker.date
            self.applyDateRangeFilter(startDate: startDate, endDate: endDate)
        }

        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)

        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)

        alertController.view.translatesAutoresizingMaskIntoConstraints = false
        alertController.view.heightAnchor.constraint(equalToConstant: 300).isActive = true

        present(alertController, animated: true, completion: nil)
    }

    private func applyDateRangeFilter(startDate: Date, endDate: Date) {
        print("Rango de fechas seleccionado: \(startDate) - \(endDate)")
    }

    // MARK: - UITableViewDataSource

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return leadStatuses.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeadStatusCell", for: indexPath)

        let status = leadStatuses[indexPath.row]
        cell.textLabel?.text = status.title
        cell.textLabel?.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        cell.textLabel?.textColor = .darkGray

        cell.backgroundColor = .white
        cell.layer.cornerRadius = 10
        cell.layer.borderWidth = 1
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.clipsToBounds = true
        cell.selectionStyle = .none

        let countLabel = UILabel()
        countLabel.text = "\(status.count)"
        countLabel.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        countLabel.textColor = .darkGray
        countLabel.translatesAutoresizingMaskIntoConstraints = false
        cell.contentView.addSubview(countLabel)

        NSLayoutConstraint.activate([
            countLabel.centerYAnchor.constraint(equalTo: cell.contentView.centerYAnchor),
            countLabel.trailingAnchor.constraint(equalTo: cell.contentView.trailingAnchor, constant: -16)
        ])

        return cell
    }

    // MARK: - UITableViewDelegate

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 80 
    }
}
