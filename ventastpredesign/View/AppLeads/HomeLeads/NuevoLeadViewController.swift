//
//  NuevoLeadViewController.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera on 04/11/24.
//  Copyright © 2024 TotalPlay. All rights reserved.
//

import UIKit

class NuevoLeadViewController: UIViewController {

    private let viewModel = NuevoLeadViewModel()

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "+NUEVO LEAD"
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let infoLabel: UILabel = {
        let label = UILabel()
        label.text = "Por favor, completa los datos"
        label.font = UIFont.systemFont(ofSize: 14)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let infoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "AppLeads_target")
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()

    private let nombreTextField: UITextField = {
        let textField = UITextField()
        textField.borderStyle = .roundedRect
        textField.placeholder = "Nombre (s)"
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()

    private let apellidoPaternoTextField: UITextField = {
        let textField = UITextField()
        textField.borderStyle = .roundedRect
        textField.placeholder = "Apellido Paterno"
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()

    private let apellidoMaternoTextField: UITextField = {
        let textField = UITextField()
        textField.borderStyle = .roundedRect
        textField.placeholder = "Apellido Materno"
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()

    private let telefonoTextField: UITextField = {
        let textField = UITextField()
        textField.borderStyle = .roundedRect
        textField.placeholder = "Teléfono"
        textField.translatesAutoresizingMaskIntoConstraints = false

        let telefonoImageView = UIImageView()
        telefonoImageView.image = UIImage(named: "AppLeads_phone")
        telefonoImageView.contentMode = .scaleAspectFit
        telefonoImageView.translatesAutoresizingMaskIntoConstraints = false
        telefonoImageView.isUserInteractionEnabled = false

        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 0))
        paddingView.addSubview(telefonoImageView)

        telefonoImageView.centerYAnchor.constraint(equalTo: paddingView.centerYAnchor).isActive = true
        telefonoImageView.leadingAnchor.constraint(equalTo: paddingView.leadingAnchor, constant: 5).isActive = true
        telefonoImageView.widthAnchor.constraint(equalToConstant: 20).isActive = true
        telefonoImageView.heightAnchor.constraint(equalToConstant: 20).isActive = true

        textField.leftView = paddingView
        textField.leftViewMode = .always

        return textField
    }()

    private let emailTextField: UITextField = {
        let textField = UITextField()
        textField.borderStyle = .roundedRect
        textField.placeholder = "Email"
        textField.translatesAutoresizingMaskIntoConstraints = false

        let emailImageView = UIImageView()
        emailImageView.image = UIImage(named: "AppLeads_email")
        emailImageView.contentMode = .scaleAspectFit
        emailImageView.translatesAutoresizingMaskIntoConstraints = false
        emailImageView.isUserInteractionEnabled = false

        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 0))
        paddingView.addSubview(emailImageView)

        emailImageView.centerYAnchor.constraint(equalTo: paddingView.centerYAnchor).isActive = true
        emailImageView.leadingAnchor.constraint(equalTo: paddingView.leadingAnchor, constant: 5).isActive = true
        emailImageView.widthAnchor.constraint(equalToConstant: 20).isActive = true
        emailImageView.heightAnchor.constraint(equalToConstant: 20).isActive = true

        textField.leftView = paddingView
        textField.leftViewMode = .always

        return textField
    }()



    private let comentarioTextField: UITextField = {
        let textField = UITextField()
        textField.borderStyle = .roundedRect
        textField.placeholder = "Comentario"
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()

    private let enviarButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(systemName: "paperplane"), for: .normal)
        button.tintColor = .white
        button.backgroundColor = UIColor(named: "AppLeads_Color")
        button.layer.cornerRadius = 30
        button.layer.masksToBounds = true
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarTitleImage()
        setupUI()
        enviarButton.addTarget(self, action: #selector(enviarButtonTapped), for: .touchUpInside)
    }

    @objc private func enviarButtonTapped() {
        viewModel.nombre = nombreTextField.text
        viewModel.apellidoPaterno = apellidoPaternoTextField.text
        viewModel.apellidoMaterno = apellidoMaternoTextField.text
        viewModel.telefono = telefonoTextField.text
        viewModel.email = emailTextField.text
        viewModel.enviarSolicitud()
        viewModel.onCompletion = { [weak self] result in
            guard let self = self else { return }
            DispatchQueue.main.async {
                self.showAlert(for: result ?? "No se insertó correctamente")
            }
        }
    }

    private func showAlert(for result: String?) {
        var title: String
        var message: String
        var image: String
        if let result = result {
            if result == "No se insertó correctamente" {
                title = "Error"
                message = "No se insertó correctamente."
                image = "icon_Event_erased"
            } else {
                title = "Lead Enviado a Validación"
                message = "La información se envió correctamente para ser autenticada."
                image = "icon_event_planned"
            }
        } else {
            title = "Error"
            message = "Ocurrió un error desconocido."
            image = "icon_Event_erased"
        }

        let alert = CustomAlertViewController(title: title, subtitle: message, result: image)
        alert.modalPresentationStyle = .overCurrentContext
        alert.modalTransitionStyle = .crossDissolve
        present(alert, animated: true, completion: nil)
    }

    private func setupUI() {
        view.backgroundColor = .white
        view.addSubview(titleLabel)
        view.addSubview(infoImageView)
        view.addSubview(infoLabel)
        view.addSubview(nombreTextField)
        view.addSubview(apellidoPaternoTextField)
        view.addSubview(apellidoMaternoTextField)
        view.addSubview(telefonoTextField)
        view.addSubview(emailTextField)
        view.addSubview(comentarioTextField)
        view.addSubview(enviarButton)
        setupConstraints()
    }

    private func setupConstraints() {
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20),
            titleLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),

            infoImageView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 10),
            infoImageView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            infoImageView.widthAnchor.constraint(equalToConstant: 20),
            infoImageView.heightAnchor.constraint(equalToConstant: 20),

            infoLabel.centerYAnchor.constraint(equalTo: infoImageView.centerYAnchor),
            infoLabel.leadingAnchor.constraint(equalTo: infoImageView.trailingAnchor, constant: 8),
            infoLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20)
        ])

        let textFields = [nombreTextField, apellidoPaternoTextField, apellidoMaternoTextField, telefonoTextField, emailTextField, comentarioTextField]
        for (index, textField) in textFields.enumerated() {
            textField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
            textField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
            if index == 0 {
                textField.topAnchor.constraint(equalTo: infoLabel.bottomAnchor, constant: 20).isActive = true
            } else {
                textField.topAnchor.constraint(equalTo: textFields[index - 1].bottomAnchor, constant: 15).isActive = true
            }
        }

        NSLayoutConstraint.activate([
            enviarButton.widthAnchor.constraint(equalToConstant: 60),
            enviarButton.heightAnchor.constraint(equalToConstant: 60),
            enviarButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            enviarButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -20)
        ])
    }

    private func setupNavigationBarTitleImage() {
        let logoImageView = UIImageView(image: UIImage(named: "AppLogo"))
        logoImageView.contentMode = .scaleAspectFit
        navigationItem.titleView = logoImageView
    }
}
