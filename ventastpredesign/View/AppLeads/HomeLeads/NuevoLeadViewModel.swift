//
//  NuevoLeadViewModel.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera  on 04/11/24.
//  Copyright © 2024 TotalPlay. All rights reserved.
//

import Foundation

class NuevoLeadViewModel {

    var nombre: String?
    var apellidoPaterno: String?
    var apellidoMaterno: String?
    var telefono: String?
    var email: String?

    var onCompletion: ((String?) -> Void)?

    func enviarSolicitud() {
        guard let url = URL(string: "https://totalplay.my.salesforce.com/services/apexrest/WS_CreacionLEADRest") else {
            onCompletion?("URL inválida.")
            return
        }

        var request = URLRequest(url: url)
        request.httpMethod = "POST"

        let headers = [
            "Accept": "application/json; charset=UTF-8",
            "Content-Type": "application/json; charset=UTF-8",
            "Authorization": "Bearer " + statics.TOKEN
        ]

        for (key, value) in headers {
            request.setValue(value, forHTTPHeaderField: key)
        }

        let informacion: [String: Any] = [
            "canalReferido": "Call Center",
            "email": email ?? "",
            "estatus": "Candidato",
            "nombre": "\(nombre ?? "") \(apellidoPaterno ?? "") \(apellidoMaterno ?? "")",
            "nombreTecnico": "200015602",
            "numEmpleadoTecnico": "200015602",
            "origen": "Call Center",
            "subCanal": "Ejecutivos EKT",
            "telefono": telefono ?? "",
            "ventaOrigen": "App Leads"
        ]

        let requestBody: [String: Any] = ["informacion": informacion]

        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: requestBody, options: [])
        } catch {
            onCompletion?("Error al convertir a JSON: \(error.localizedDescription)")
            return
        }

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if let error = error {
                self.onCompletion?("Error en la solicitud: \(error.localizedDescription)")
                return
            }

            guard let data = data else {
                self.onCompletion?("No se recibió datos.")
                return
            }

            do {
                if let jsonResponse = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                    if let resultDescription = jsonResponse["resultDescription"] as? String {
                        self.onCompletion?(resultDescription)
                    } else {
                        self.onCompletion?("Respuesta no válida.")
                    }
                } else {
                    self.onCompletion?("La respuesta no es un JSON válido.")
                }
            } catch {
                self.onCompletion?("Error al procesar la respuesta JSON: \(error.localizedDescription)")
            }
        }
        task.resume()
    }
}
