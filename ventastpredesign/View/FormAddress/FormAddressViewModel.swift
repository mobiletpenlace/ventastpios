//
//  FormAddressViewModel.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera  on 29/05/24.
//  Copyright © 2024 TotalPlay. All rights reserved.
//

import UIKit
import Foundation
import SwiftEventBus

protocol FormAddressObserver {
    func onError(errorMessage: String)
    func onSuccessTPCoverage()
}

class FormAddressViewModel: BaseViewModel, FormAddressModelObserver {
    func successCoverageTPService(response: CoberturaResponse) {
        view.hideLoading()
            if response.MCalculaFactibilidad?.factibilidad == "1" {
                observer?.onSuccessTPCoverage()
            } else {
                back()
                SwiftEventBus.post("ErrorCobertura", sender: nil)
            }
    }

    func successCoordinatesGoogle(lon: Double, lat: Double) {
        view.hideLoading()
        model.consultCoverage(lon: lon, lat: lat)
    }

    func onError(errorMessage: String) {
        view.hideLoading()
    }

    func convertCoordinates(address: String) {
        view.showLoading(message: "")
        model.convertCoordinates(address: address)
    }

    var observer: FormAddressObserver?
    var model: FormAddressModel!

    override init(view: BaseVentasView) {
        super.init(view: view)
        model = FormAddressModel(observer: self)
        model.atachModel(viewModel: self)
    }

    func atachView(observer: FormAddressObserver) {
        self.observer = observer
    }

    func back() {
        if RealManager.findFirst(object: Sitio.self) != nil && RealManager.findFirst(object: NProspecto.self) != nil {
            RealManager.deleteAll(object: Sitio.self)
            RealManager.deleteAll(object: NProspecto.self)
        }
        RealManager.deleteAll(object: Sitio.self)
        SwiftEventBus.post("Cambia_paso_container", sender: 1)
    }

}
