//
//  GoogleAddress.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera  on 29/05/24.
//  Copyright © 2024 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import AlamofireObjectMapper
import RealmSwift

class GoogleAddress: BaseRequest {
    var addressLines: [String] = []

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        self.addressLines <- map["addressLines"]
    }

}
