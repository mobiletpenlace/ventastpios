//
//  FormAddressView.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera  on 29/05/24.
//  Copyright © 2024 TotalPlay. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTextControls_FilledTextAreas
import MaterialComponents.MaterialTextControls_FilledTextFields

class FormAddressViewController: BaseVentasView, FormAddressObserver {
    func onError(errorMessage: String) {
        print("onError en el View Controller")
    }

    func onSuccessTPCoverage() {
        stackSection1.isHidden = true
        stackSection2.isHidden = false
        stackSection3.isHidden = false
    }
    var mFormAddressViewModel: FormAddressViewModel!

    @IBOutlet weak var stackSection1: UIView!
    @IBOutlet weak var stackSection2: UIView!
    @IBOutlet weak var stackSection3: UIView!
    @IBOutlet weak var codeTextField: MDCFilledTextField!
    @IBOutlet weak var numberStreetView: UIStackView!
    @IBOutlet var textFieldsArray: [MDCFilledTextField]!
    @IBOutlet weak var withoutNumberButton: UIButton!
    @IBOutlet weak var checkButtonImage: UIImageView!

    let iconUncheck = UIImage(named: "icon_uncheck_addons")
    let iconCheck = UIImage(named: "icon_check_green")

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }


    func extractAndFormatTextFromTextFields() -> String {
        var texts: [String] = []

        for textField in textFieldsArray {
            if let text = textField.text, !text.isEmpty {
                texts.append(text)
            }
        }
        return texts.joined(separator: ", ")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mFormAddressViewModel = FormAddressViewModel(view: self)
        mFormAddressViewModel.atachView(observer: self)
//        codeTextField.delegate = self
//        withoutNumberButton.addTarget(self, action: #selector(toggleSelectableButtonSelection), for: .touchUpInside)
    }

    private var isSelectableButtonSelected: Bool = false {
        didSet {
            updateSelectableButtonAppearance()
        }
    }

    @objc private func toggleSelectableButtonSelection() {
        isSelectableButtonSelected.toggle()
    }

    private func updateSelectableButtonAppearance() {
        // Personaliza la apariencia del botón según su estado seleccionado o no seleccionado
        if isSelectableButtonSelected {
            checkButtonImage.image = iconCheck
            numberStreetView.isUserInteractionEnabled = false
            numberStreetView.alpha = 0.25
        } else {
            checkButtonImage.image = iconUncheck
            numberStreetView.isUserInteractionEnabled = true
            numberStreetView.alpha = 1
        }
    }


    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text?.isEmpty ?? true {
            textField.text = "      +   " // Provisional, se ajustará mientras se escribe
        }
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "      +   " {
            textField.text = "" // Limpiar si no se ingresó nada
        }
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // Obtener el texto actual, incluyendo la nueva entrada
        let currentText = textField.text ?? ""
        guard let stringRange = Range(range, in: currentText) else {
            return false
        }
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)

        // Remover cualquier carácter no alfanumérico
        let alphanumeric = updatedText.replacingOccurrences(of: "[^a-zA-Z0-9]", with: "", options: .regularExpression)

        // Aplicar la máscara
        let maskedText = applyMask(to: alphanumeric)

        // Actualizar el texto del UITextField
        textField.text = maskedText

        // Mover el cursor al final
        if let newPosition = textField.position(from: textField.beginningOfDocument, offset: maskedText.count) {
            textField.selectedTextRange = textField.textRange(from: newPosition, to: newPosition)
        }

        // Prevenir el cambio directo del texto
        return false
    }

    private func applyMask(to alphanumeric: String) -> String {
        let maxLength = 7
        let paddedAlphanumeric = alphanumeric.padding(toLength: maxLength, withPad: " ", startingAt: 0)

        let part1 = paddedAlphanumeric.prefix(6)
        let part2 = paddedAlphanumeric.suffix(3)

        let maskedText = "\(part1)-\(part2)".trimmingCharacters(in: .whitespaces)

        return maskedText
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func validateCoverage(_ sender: Any) {
        let formattedText = extractAndFormatTextFromTextFields()
        convertAddress(address: formattedText)
    }

    override func viewDidAppear(_ animated: Bool) {
    }

    func convertAddress(address: String) {
        mFormAddressViewModel.convertCoordinates(address: address)
    }
}
