//
//  GoogleRequest.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera  on 29/05/24.
//  Copyright © 2024 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class GoogleRequest: BaseRequest {
    var address: GoogleAddress?

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        self.address <- map["address"]
    }

}
