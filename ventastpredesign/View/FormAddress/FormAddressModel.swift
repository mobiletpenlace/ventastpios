//
//  FormAddressModel.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera  on 29/05/24.
//  Copyright © 2024 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import RealmSwift
import Alamofire
import Amplitude
import AlamofireObjectMapper
import SwiftyJSON
import SwiftCoroutine

protocol FormAddressModelObserver {
    func onError(errorMessage: String)
    func successCoordinatesGoogle(lon: Double, lat: Double)
    func successCoverageTPService(response: CoberturaResponse)
}

class FormAddressModel: BaseModel {

    var viewModel: FormAddressModelObserver?
    var server: ServerDataManager2<GoogleResponse>?
    var server2: ServerDataManager2<CoberturaResponse>?
    var requestGoogle: GoogleRequest!
    var requestCobertura: CoberturaRequest!
    var urlGoogleCoordinates = AppDelegate.API_GOOGLE_COORDINATES
    var urlConsultCoverage = AppDelegate.API_TOTAL + ApiDefinition.API_COVERAGE

    init(observer: BaseViewModelObserver) {
        super.init(observerVM: observer)
        server = ServerDataManager2<GoogleResponse>()
        server2 = ServerDataManager2<CoberturaResponse>()
    }

    func atachModel(viewModel: FormAddressModelObserver) {
        self.viewModel = viewModel
    }

    func convertCoordinates(address: String) {
        let request = GoogleRequest()
        request.address = GoogleAddress()
        request.address?.addressLines = [address]
        self.requestGoogle = request
        server?.setValues(requestUrl: urlGoogleCoordinates, delegate: self, headerType: .headersMiddle)
        server?.request(requestModel: request)
    }

    func consultCoverage(lon: Double, lat: Double) {
        let coberturaRequest = CoberturaRequest()
        coberturaRequest.MLogin = Login()
        coberturaRequest.MLogin?.Ip = "1.1.1.1"
        coberturaRequest.MLogin?.User = "25631"
        coberturaRequest.MLogin?.SecretWord = "Middle100$"
        coberturaRequest.MCoordenadas = Coordenadas()
        coberturaRequest.MCoordenadas?.TipoCliente = "Totalplay"
        coberturaRequest.MCoordenadas?.latitud = String(lat)
        coberturaRequest.MCoordenadas?.longitud = String(lon)
        self.requestCobertura = coberturaRequest
        server2?.setValues(requestUrl: urlConsultCoverage, delegate: self, headerType: .headersMiddle)
        server2?.request(requestModel: coberturaRequest)
    }

    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if requestUrl == urlGoogleCoordinates {
            let googleResponse = response as! GoogleResponse
            if googleResponse.resultate?.geocode?.location?.latitude?.isZero == false {
                let lon = Double(googleResponse.resultate?.geocode?.location?.longitude ?? 0.0)
                let lat = Double(googleResponse.resultate?.geocode?.location?.latitude ?? 0.0)
                viewModel?.successCoordinatesGoogle(lon: lon, lat: lat)
            } else {
                viewModel?.onError(errorMessage: "No se pudo convertir a coordenadas")
            }
        } else if requestUrl == urlConsultCoverage {
            let coberturaResponse = response as! CoberturaResponse
            if coberturaResponse.MCalculaFactibilidad?.factibilidad != nil {
                viewModel?.successCoverageTPService(response: coberturaResponse)
            } else {
                viewModel?.onError(errorMessage: "Coverage Service Error")
            }
        }
    }
}
