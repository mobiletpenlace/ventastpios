//
//  GoogleResponse.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera  on 29/05/24.
//  Copyright © 2024 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class GoogleResponse: BaseResponse {

    required init?(map: Map) {

    }

    override func mapping(map: Map) {
        resultate <- map["result"]
    }

    var resultate: Resultate?

}

class Resultate: BaseResponse {

    required init?(map: Map) {

    }

    override func mapping(map: Map) {
        geocode <- map["geocode"]
    }

    var geocode: Geocode?

}

class Geocode: BaseResponse {

    required init?(map: Map) {

    }

    override func mapping(map: Map) {
        location <- map["location"]
    }

    var location: Locations?

}

class Locations: BaseResponse {

    required init?(map: Map) {

    }

    override func mapping(map: Map) {
        latitude <- map["latitude"]
        longitude <- map["longitude"]
    }

    var latitude: Double?
    var longitude: Double?

}
