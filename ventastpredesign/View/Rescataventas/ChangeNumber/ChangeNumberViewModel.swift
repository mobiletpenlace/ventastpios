//
//  ChangeNumberViewModel.swift
//  ventastp
//
//  Created by Ulises Ortega on 01/11/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//
import UIKit
import ObjectMapper
import Alamofire
import SwiftyJSON
import SwiftEventBus
import SwiftCoroutine

class ChangeNumberViewModel {

    func getNumbers(idObject: String?) {
        guard let idObject = idObject else {return}
        let parameters: Parameters = [
            "accion": "ConsultaCreacion",
            "idOportunidad": idObject
        ]
        if ConnectionUtils.isConnectedToNetwork() {
            // Connection is active
            Alamofire.request(AppDelegate.API_SALESFORCE + ApiDefinition.API_CONSULTA_VENTA, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: ApiDefinition.headersToken).responseJSON {[weak self] response in
                guard let self = self else {return}
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print("JSON Numbers: \(json)")
                    let documentsResponse = ConsultaOpResponse(JSONString: json.rawString() ?? "")
                    guard let infoEncript = documentsResponse?.infoEncript else { return }
                    let jsonDec = AES128.shared.decode(coded: infoEncript)!
                    let infoDec = InfoEncript(JSONString: jsonDec)
                    print("INFORECIVED: \(jsonDec)")
                    print("INFOMAPPED: \(Mapper().toJSONString(infoDec!, prettyPrint: true)!)")
                    SwiftEventBus.post("SuccessGetNumbers", sender: infoDec)
                case .failure(let error):
                    print(error)
                }
            }
        } else {
            // Conection error
        }
    }

    func updateAdditionalNumber(info: InfoEncript?, idObject: String?) {
        guard let info = info, let idObject = idObject else {return}
        guard let infoJSONString = Mapper().toJSONString(info, prettyPrint: true) else { return }
        guard let infoEncript = AES128.shared.code(plainText: infoJSONString) else { return }
        print("InfoJSONSTIRNG:")
        print("\(infoJSONString)")
        print("infoEncript: \(infoEncript)")
        let parameters: Parameters = [
            "accion": "ModificacionCreacion",
            "idOportunidad": idObject,
            "infoOpp": infoEncript
        ]
        print(parameters)
        if ConnectionUtils.isConnectedToNetwork() {
            // Connection is active
            Alamofire.request(AppDelegate.API_SALESFORCE + ApiDefinition.API_CONSULTA_VENTA, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: ApiDefinition.headersToken).responseJSON {[weak self] response in
                guard let self = self else {return}
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print("JSON NumberUPDATED: \(json)")
                    SwiftEventBus.post("SuccessUpdateAdditionalNumber")
                case .failure(let error):
                    print(error)
                }
            }
        } else {
            // Conection error
        }
    }
}
