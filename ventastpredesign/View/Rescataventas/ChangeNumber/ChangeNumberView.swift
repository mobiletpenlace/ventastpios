//
//  ChangeNumberView.swift
//  ventastp
//
//  Created by Ulises Ortega on 05/09/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

@available(iOS 13.0, *)
class ChangeNumberView: UIViewController {

    private var idObject: String?
    private var info: InfoEncript?

    private var viewAlreadyShowed = false

    private var prevAdditionalNumber: String?

    let viewModel = ChangeNumberViewModel()

    @IBOutlet weak var numberTextField: NumberTextField!
    @IBOutlet weak var additionalNumberTextField: NumberTextField!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var confirmButton: UIButton!

    deinit {
        print("DEINIT CHANGENUMBER")
        SwiftEventBus.unregister(self)
        ProgressHUD.dismiss()
    }

    init(idObject: String?) {
        self.idObject = idObject
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
        configureTextFields()
        getNumbers()

        SwiftEventBus.onMainThread(self, name: "SuccessGetNumbers", handler: { [weak self] result in
            guard let info = result?.object as? InfoEncript, let self = self else { return }
            self.info = info
            print("NUMERO:\(info.prospecto.telefono) ADICIONAL:\(info.prospecto.otroTelefono)")
            self.prevAdditionalNumber = info.prospecto.otroTelefono
            if self.numberTextField.setNumber(prefix: "+52", number: info.prospecto.telefono) {
                print("Numero principal agregado de forma correcta")
            } else {
                print("Error al agregar número principal")
            }
            if self.additionalNumberTextField.setNumber(prefix: "+52", number: info.prospecto.otroTelefono) {
                print("Numero secundario agregado de forma correcta")
            } else {
                print("Error al agregar número secundario")
            }
            ProgressHUD.dismiss()
        })

        SwiftEventBus.onMainThread(self, name: "SuccessUpdateAdditionalNumber", handler: { [weak self] _ in
            ProgressHUD.showSuccess()
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5, execute: {
                SwiftEventBus.post("SuccessNumberChanged")
                self?.dismiss(animated: true)
            })
        })
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !viewAlreadyShowed {
            ProgressHUD.show()
            viewAlreadyShowed = true
        }
    }

    func getNumbers() {
        viewModel.getNumbers(idObject: idObject)
    }

    @IBAction func backButtonAction(_ sender: Any) {
        self.dismiss(animated: false)
    }

    @IBAction func confirmButtonAction(_ sender: Any) {
        ProgressHUD.show()
        confirmButton.isEnabled = false
        guard let newAdditionalNumber = additionalNumberTextField.getNumber() else {return}
        info?.prospecto.otroTelefono = newAdditionalNumber
        viewModel.updateAdditionalNumber(info: info, idObject: idObject)
    }

    @IBAction func numberEditingDidEnd(_ sender: NumberTextField) {
        print("Entro al didend")
        guard let number = sender.getNumber() else { return }
        guard let mainNumber = numberTextField.getNumber() else { return }
        sender.validateNumber()
        print("Number: \(number) Valid: \(sender.isValid)")
        if sender.isValid {
            if number != prevAdditionalNumber && number != mainNumber {
                confirmButton.isEnabled = true
                messageLabel.isHidden = true
            } else {
                confirmButton.isEnabled = false
                // SHOW SAME NUMBER MESSAGE
                messageLabel.isHidden = false
            }
        } else {
            messageLabel.isHidden = true
            confirmButton.isEnabled = false
        }

    }

    func configUI() {
        messageLabel.text = "Ingresa un número diferente"
        messageLabel.isHidden = true
        confirmButton.isEnabled = false
    }

    func configureTextFields() {
        numberTextField.label.text = "Whatsapp/Celular"
        additionalNumberTextField.label.text = "Número telefónico adicional"
        numberTextField.isEnabled = false
    }
}
