//
//  NumberTextField.swift
//  ventastp
//
//  Created by Ulises Ortega on 05/09/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import UIKit
import MaterialComponents

class NumberTextField: MDCFilledTextField {

    deinit {
        print("Deinit TEXTFIELD \(self.text ?? "nohaytexto")")
    }

    private var numberPrefix = "+52 "
    private var numberMaxLength = 10
    private var numberMinLength = 10
    private let imageViewFlag = UIImageView()
    private let viewModel = CountryCodeListModel()
    private(set) var isValid = false
    private let errorMessage = "Ingresa un número válido"

    override func awakeFromNib() {
        configure()
        configureFlagButton()
        // TODO: CHANGE TO .always WHEN EXTENSION FLAGS HAVE TO BE ENABLED
        leadingViewMode = .never
    }

    // Block paste
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(UIResponderStandardEditActions.paste(_:)) {
            return false
        }
        return super.canPerformAction(action, withSender: sender)
    }

    @objc func flagTapped(_ sender: UITapGestureRecognizer) {
        print("flagTapped", sender)
        let vc = CountryCodeListView()
        vc.sender = sender
        self.viewContainingController()?.present(vc, animated: true)
    }

    private func configureFlagButton() {
        imageViewFlag.image = UIImage(named: "mxFlag")
        let tap = UITapGestureRecognizer(target: self, action: #selector(flagTapped(_:)))
        imageViewFlag.addGestureRecognizer(tap)
        imageViewFlag.isUserInteractionEnabled = true

        self.leadingView = imageViewFlag
        self.leadingView?.width = 27
        self.leadingView?.height = 17
        self.leadingViewMode = .always

    }

    /// Sets number on textfield given a prefix and number, returns false when prefix number does not exist. Sets flag icon with the given prefix.
    func setNumber(prefix: String, number: String?) -> Bool {
        for country in viewModel.countries {
            var modelPrefix = country.ext.replacingOccurrences(of: " ", with: "")
            modelPrefix = modelPrefix.replacingOccurrences(of: "+", with: "")
            var prefixAux = prefix.replacingOccurrences(of: " ", with: "")
            prefixAux = prefixAux.replacingOccurrences(of: "+", with: "")
            if modelPrefix == prefixAux {
                imageViewFlag.image = country.image
                numberMinLength = country.numberMinLength
                numberMaxLength = country.numberMaxLength
                numberPrefix = "+" + modelPrefix + " "
                text = numberPrefix + (number ?? "")
                return true
            }
        }
        return false
    }

    private func configure() {
        self.delegate = self
        self.textContentType = .telephoneNumber
        self.keyboardType = .numberPad
        self.clearButtonMode = .whileEditing
        self.text = numberPrefix
        self.normalTheme(type: 1)
    }

    /// Gets number ignoring prefix
    func getNumber() -> String? {
        let text = self.text?.split(character: " ")[1]
        return text
    }

    func getPrefix() -> String {
        return numberPrefix
    }

    private func setValid() {
        self.isValid = true
        self.leadingAssistiveLabel.isHidden = true
        self.leadingAssistiveLabel.text = ""
        self.setFilledBackgroundColor(UIColor.clear, for: .normal)
        self.setFilledBackgroundColor(UIColor.clear, for: .editing)
        self.setTextColor(UIColor(named: "Base_rede_ligth_gray_titles")!, for: .normal)
        self.setNormalLabelColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .normal)
        self.setFloatingLabelColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .normal)
        self.setUnderlineColor(UIColor(named: "Base_rede_ligth_gray_titles")!, for: .normal)
        self.setUnderlineColor(UIColor(named: "Base_rede_ligth_gray_titles")!, for: .editing)
    }

    private func setInvalid() {
        self.isValid = false
        self.leadingAssistiveLabel.isHidden = false
        self.setTextColor(UIColor.red, for: .normal)
        self.setNormalLabelColor(UIColor.red, for: .normal)
        self.setFloatingLabelColor(UIColor.red, for: .normal)
        self.setLeadingAssistiveLabelColor(UIColor.red, for: .normal)
        self.leadingAssistiveLabel.text = errorMessage
    }

    func validateNumber() {
        let phone = self.getNumber()
        if phone?.length ?? 0 < numberMinLength {
            self.setInvalid()
        } else {
            self.setValid()
        }
    }

}

extension NumberTextField: UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("ENTRO AL DELEGATE")
        let protectedRange = NSRange(location: 0, length: numberPrefix.length)
        let intersection = NSIntersectionRange(protectedRange, range)
        if intersection.length > 0 {
            return false
        }
        if range.location == numberMaxLength + numberPrefix.length-1 {
            return true
        }
        if range.location + range.length > numberMaxLength + numberPrefix.length-1 {
            return false
        }
        if range.location < numberPrefix.length-1 {
            return false
        }
        return true
    }

//    func textFieldDidEndEditing(_ textField: UITextField) {
//        print("ENTRO AL ENDEDDITING")
//        validateNumber()
//    }

    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        textField.text = self.numberPrefix
        return false
    }

}
