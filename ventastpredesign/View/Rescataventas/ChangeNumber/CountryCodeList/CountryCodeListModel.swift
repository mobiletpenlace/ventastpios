//
//  CountryCodeListModel.swift
//  ventastp
//
//  Created by Ulises Ortega on 07/09/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import UIKit

class CountryCodeListModel {

    struct Country {
        let name: String
        let image: UIImage
        let ext: String
        let numberMinLength: Int
        let numberMaxLength: Int
    }

    let countries: [Country] = {
        let country0 = Country(name: "México", image: UIImage(named: "mxFlag")!, ext: "+52 ", numberMinLength: 10, numberMaxLength: 10)
        let country1 = Country(name: "EE.UU", image: UIImage(named: "usFlag")!, ext: "+1 ", numberMinLength: 10, numberMaxLength: 10)
        let country2 = Country(name: "Colombia", image: UIImage(named: "coFlag")!, ext: "+57 ", numberMinLength: 10, numberMaxLength: 10)
        let country3 = Country(name: "China", image: UIImage(named: "cnFlag")!, ext: "+86 ", numberMinLength: 10, numberMaxLength: 10)
        let country4 = Country(name: "Brasil", image: UIImage(named: "brFlag")!, ext: "+55 ", numberMinLength: 10, numberMaxLength: 10)
        let country5 = Country(name: "Guatemala", image: UIImage(named: "gtFlag")!, ext: "+502 ", numberMinLength: 10, numberMaxLength: 10)
        let country6 = Country(name: "Belice", image: UIImage(named: "bzFlag")!, ext: "+501 ", numberMinLength: 10, numberMaxLength: 10)

        let countriesArray = [country0, country1, country2, country3, country4, country5, country6]

        return countriesArray
    }()

}
