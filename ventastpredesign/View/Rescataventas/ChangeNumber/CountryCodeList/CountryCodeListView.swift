//
//  CountryCodeListView.swift
//  ventastp
//
//  Created by Ulises Ortega on 06/09/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import UIKit

class CountryCodeListView: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    let viewModel = CountryCodeListModel()
    var sender: UITapGestureRecognizer?
    @IBOutlet var backgroundView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // initializeCountriesList()
        self.view.backgroundColor = .none
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.delegate = self
        tableView.dataSource = self
    }

}

@available(iOS 14.0, *)
extension CountryCodeListView: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.countries.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        let model = viewModel.countries[indexPath.row]
        var listConfiguration = UIListContentConfiguration.valueCell()
        listConfiguration.text = model.name
        listConfiguration.image = model.image
        listConfiguration.secondaryText = model.ext

        cell.contentConfiguration = listConfiguration
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = viewModel.countries[indexPath.row]
        if (sender?.view?.superview as! NumberTextField).setNumber(prefix: model.ext, number: nil) {
            print("Succes loading country")
        } else {
            // Handle error
        }
        self.dismiss(animated: true)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }

}
