//
//  ProspectDetailView.swift
//  ventastp
//
//  Created by Ulises Ortega on 17/08/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

@available(iOS 13.0, *)
class ProspectDetailView: UIViewController {

    deinit {
        print("DETAIL VC DEINIT")
        SwiftEventBus.unregister(self)
        ProgressHUD.dismiss()
    }

    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!

    @IBOutlet weak var contentView: UIView!

    @IBOutlet weak var centeredView: UIView!

    // ProspectDetailSection
    @IBOutlet weak var accountNumberLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!

    // ProspectRejectSection
    @IBOutlet weak var prospectRejectSectionView: UIView!
    @IBOutlet weak var secondSeparatorView: UIView!
    @IBOutlet weak var rejectReasonLabel: UILabel!
    @IBOutlet weak var rejectReasonDetailLabel: UILabel!
    @IBOutlet weak var rejectCommentLabel: UILabel!

    // ClientInfo section
    @IBOutlet weak var clientInfoSectionView: UIView!
    @IBOutlet weak var thirdSeparatorView: UIView!
    @IBOutlet weak var changeDocumentsButton: UIButton!
    @IBOutlet weak var changePhoneButton: UIButton!

    // Comments Section
    @IBOutlet weak var commentsStack: UIStackView!
    @IBOutlet weak var stackAuxView: UIView!
    @IBOutlet weak var commentTextAndButtonView: UIView!
    @IBOutlet weak var commentTextView: UITextView!
    @IBOutlet weak var sendCommentButton: UIButton!
    let textViewPlaceHolder = "Agregar un comentario"

    var prospect: Oportunidad?

    let viewModel = ProspectDetailViewModel()
    let buttonSend = UIButton()

    var previousComments: [Comentarios] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        commentTextView.delegate = self
        configUI()
        fillProspectDetail()
        getCommentsDetails()
        ProgressHUD.show()

        SwiftEventBus.onMainThread(self, name: "ProspectCommentsLoaded", handler: { [weak self] result in
            guard let commentResponse = result?.object as? MotivosOpp else {return}
            if commentResponse.motivoRechazoMC == nil && commentResponse.detalleMotivoRechazo == nil && commentResponse.comentariosMC == nil {
                self?.prospectRejectSectionView.isHidden = true
                self?.secondSeparatorView.isHidden = true
            }
            self?.rejectReasonLabel.text = commentResponse.motivoRechazoMC
            self?.rejectReasonDetailLabel.text = commentResponse.detalleMotivoRechazo
            self?.rejectCommentLabel.text = commentResponse.comentariosMC
            let motivoRechazo = commentResponse.motivoRechazoMC?.lowercased()
            if motivoRechazo == "pom ilocalizable" || motivoRechazo == "cliente ilocalizable" {
                self?.changeDocumentsButton.isHidden = true
            }
            ProgressHUD.dismiss()
        })

        SwiftEventBus.onMainThread(self, name: "FillCommentsSection", handler: { [weak self] result in
            guard let comments = result?.object as? [Comentarios] else {return}
            print("Entro al fill comments")
            self?.fillCommentSection(comments: comments)
        })

        SwiftEventBus.onMainThread(self, name: "UpdateCommentConstraint", handler: { [weak self] result in
            guard let commentView = result?.object as? ChatBubbleView else {return}
            self?.updateCommentConstraint(commentView: commentView)
        })

        SwiftEventBus.onMainThread(self, name: "UpdateComments", handler: {[weak self] _ in
            self?.getCommentsDetails()
        })

        SwiftEventBus.onMainThread(self, name: "SuccessDocumentsChanged", handler: { [weak self] _ in
            ProgressHUD.showSuccess("Documentos actualizados")
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5, execute: {
                if let motive = self?.rejectReasonLabel.text {
                    if motive.lowercased().contains(string: "documento") {
                        self?.saveSell(motivoRescate: "Actualización de documentos")
                    }
                }
            })
        })

        SwiftEventBus.onMainThread(self, name: "SuccessNumberChanged", handler: { [weak self] _ in
            ProgressHUD.showSuccess("Número actualizado")
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5, execute: {
                if let motive = self?.rejectReasonLabel.text {
                    if motive.lowercased().contains(string: "ilocalizable") {
                        self?.saveSell(motivoRescate: "Actualización de número telefónico")
                    }
                }
            })
        })
    }

    @available(iOS 14, *)
    @IBAction func changeDocumentsButtonAction(_ sender: Any) {
        let vc = ChangeDocumentsView(idObject: prospect?.id ?? "", BRMAcc: prospect?.numeroCuenta ?? "")
        vc.modalPresentationStyle = .overFullScreen
        present(vc, animated: false, completion: nil)
    }

    @IBAction func changePhoneButtonAction(_ sender: Any) {
        let vc = ChangeNumberView(idObject: prospect?.id ?? "")
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: false, completion: nil)
    }

    @IBAction func closeButtonAction(_ sender: Any) {
        dismiss(animated: false)
    }

    @IBAction func sendCommentButtonAction(_ sender: UIButton) {
        print("ButtonClicked")
        if !commentTextView.text.isEmpty {
            sendNewComment(comment: commentTextView.text)
        }
        commentTextView.text = textViewPlaceHolder
        commentTextView.textColor = .lightGray
        sender.isEnabled = false
        commentTextView.resignFirstResponder()
    }

    func sendNewComment(comment: String) {
        viewModel.sendNewComment(idObject: prospect?.id ?? "", comment: comment)
    }

    func saveSell(motivoRescate: String) {
        ProgressHUD.show()
        viewModel.saveSell(idOpp: prospect?.id ?? "", motive: motivoRescate) { [weak self] in
            SwiftEventBus.post("UpdateProspects")
            ProgressHUD.dismiss()
            self?.dismiss(animated: true)
        }
    }

    func getCommentsDetails() {
        viewModel.getCommentsDetails(idRegistro: prospect?.id ?? "")
    }

    func fillProspectDetail() {
        guard let prospect = prospect else {
            return
        }
        accountNumberLabel.text = prospect.numeroCuenta
        nameLabel.text = prospect.nombre
        dateLabel.text = String(prospect.fecha.split(separator: "T")[0])
        statusLabel.text = prospect.etapa
    }

    func configUI() {
        switch prospect?.etapa {
        case "Perdida":
            print("Show everything")
        case "Crédito":
            changeDocumentsButton.isHidden = true
        case "Ganada":
            commentTextAndButtonView.isHidden = true
            clientInfoSectionView.isHidden = true
            thirdSeparatorView.isHidden = true
        default:
            return
        }
        stackAuxView.isHidden = true
        commentTextView.textColor = .lightGray
        sendCommentButton.setTitle("", for: .normal)
        sendCommentButton.isEnabled = false
        closeButton.setTitle("", for: .normal)
        self.view.backgroundColor = .black.withAlphaComponent(0.8)
    }

}

// MARK: Comments section and textViewhandling
@available(iOS 13.0, *)
extension ProspectDetailView: UITextViewDelegate {

    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == .lightGray && textView.text == textViewPlaceHolder {
            textView.text = nil
            textView.textColor = .black
        }
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        if let nonBlankText = textView.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) {
            if nonBlankText.count < 1 {
                textView.textColor = .lightGray
                textView.text = textViewPlaceHolder
            }
        }
        textView.setContentOffset(.zero, animated: true)
    }

    func textViewDidChange(_ textView: UITextView) {
        if let nonBlankText = textView.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) {
            if nonBlankText.count < 1 {
                sendCommentButton.isEnabled = false
            } else {
                sendCommentButton.isEnabled = true
            }
        }
    }

    func fillCommentSection(comments: [Comentarios]) {
        print("ENTRO A LA FUNCIN PROSPE")
        let commentsReversed = comments.reversed()
        for comment in commentsReversed {
            if !previousComments.contains(where: {$0.Id == comment.Id}) {
                let commentView = ChatBubbleView()
                commentView.configure(with: Comment(title: comment.creadoPor,
                                                    comment: comment.texto,
                                                    date: comment.fechaCreacion,
                                                    id: comment.Id))
                commentView.heightAnchor.constraint(equalToConstant: 90).isActive = true
                commentsStack.addArrangedSubview(commentView)
            } else {
                print("COMMENT ALREADY EXISTS")
            }
        }
        previousComments = comments
    }

    func updateCommentConstraint(commentView: ChatBubbleView) {
        print("Entro al update constraints")
        if let constraint = commentView.constraints.last {
            constraint.isActive = false
            commentView.heightAnchor.constraint(equalToConstant: CGFloat(commentView.heightVar + 90)).isActive = true
        } else {
            commentView.heightAnchor.constraint(equalToConstant: CGFloat(commentView.heightVar + 90)).isActive = true
        }
    }
}
