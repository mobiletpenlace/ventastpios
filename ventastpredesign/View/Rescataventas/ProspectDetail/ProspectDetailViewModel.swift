//
//  ProspectDetailViewModel.swift
//  ventastp
//
//  Created by Ulises Ortega on 18/08/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import Alamofire
import SwiftyJSON
import SwiftEventBus

class ProspectDetailViewModel {

    func getCommentsDetails(idRegistro: String) {
        if idRegistro == "" {return}
        let parameters: Parameters = [
            "idRegistro": idRegistro,
            "tipoObjeto": "Oportunidad"
        ]
        if ConnectionUtils.isConnectedToNetwork() {
            // Connection is active
            Alamofire.request(AppDelegate.API_SALESFORCE + ApiDefinition.API_COMMENTS_LEAD, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: ApiDefinition.headersToken).responseJSON {[weak self] response in
                guard let self = self else {return}
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print("JSON comment: \(json)")
                    let commentResponse = CommentLeadResponse(JSONString: json.rawString() ?? "")
                    SwiftEventBus.post("ProspectCommentsLoaded", sender: commentResponse?.mInfoSalida?.mMotivosDetalle?.motivosOpp)
                    SwiftEventBus.post("FillCommentsSection", sender: commentResponse?.mInfoSalida?.mComentarios)
                case .failure(let error):
                    print(error)
                }
            }
        } else {
            // Conection error
        }
    }

    func saveSell(idOpp: String, motive: String, completion: @escaping () -> Void) {
        let infoRequest = InfoOpp()
        infoRequest.motivoDeRescate = motive
        let params = Mapper().toJSON(infoRequest)
        let paramsEncr = AES128.shared.code(plainText: Mapper().toJSONString(infoRequest, prettyPrint: true )!)
        print("////////////////////////////////////")
        print(params)
        print(paramsEncr)
        guard let paramsEncr = paramsEncr else {return}
        let parameters: Parameters = [
            "accion": "RescatarVenta",
            "idOportunidad": idOpp,
            "infoOpp": paramsEncr
        ]
        Alamofire.request(AppDelegate.API_SALESFORCE + ApiDefinition.API_CONSULTA_VENTA, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: ApiDefinition.headersToken).responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                print("Success RESCATAR VENTA : \(json)")
                completion()
            case .failure(let error):
                print(error)
                return
            }
        }
    }

    func sendNewComment(idObject: String, comment: String) {
        let parameters: Parameters = [
            "idObject": idObject,
            "mensajeChatter": comment
        ]
        if ConnectionUtils.isConnectedToNetwork() {
            // Connection is active
            Alamofire.request(AppDelegate.API_SALESFORCE + ApiDefinition.API_CREATE_COMMENTS_LEAD, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: ApiDefinition.headersToken).responseJSON {[weak self] response in
                guard let self = self else {return}
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print("JSON comment: \(json)")
                    SwiftEventBus.post("UpdateComments")
                case .failure(let error):
                    print(error)
                }
            }
        } else {
            // Conection error
        }
    }

}
