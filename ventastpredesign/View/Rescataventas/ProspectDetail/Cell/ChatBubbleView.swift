//
//  ChatBubbleView.swift
//  ventastp
//
//  Created by Ulises Ortega on 22/08/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

struct Comment {
    let title: String
    let comment: String
    let date: String
    let id: String
}

class ChatBubbleView: UIView {

    var heightVar = 0
    var id = ""

    let titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.textAlignment = .left
        label.font = .systemFont(ofSize: 13, weight: .bold)

        return label
    }()

    let dateLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.textAlignment = .left
        label.font = .systemFont(ofSize: 10, weight: .bold)

        return label
    }()

    let commentLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .left
        label.font = .systemFont(ofSize: 13, weight: .regular)
        label.sizeToFit()
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(titleLabel)
        addSubview(commentLabel)
        addSubview(dateLabel)
        // clipsToBounds = false
        layer.cornerRadius = 10

        // SHADOW
        clipsToBounds = false
        layer.shadowRadius = 2
        layer.shadowOffset = CGSize(width: 0, height: 0)
        layer.shadowOpacity = 0.5
        layer.shadowColor = UIColor(red: 0, green: 0, blue: 0 ).cgColor

        backgroundColor = UIColor(named: "Base_rede_semiclear_gray")

        // view.sizeToFit()
    }

    required init(coder: NSCoder) {
        fatalError()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        titleLabel.frame = CGRect(x: 10,
                                  y: 10,
                                  width: 100,
                                  height: 20)
        titleLabel.sizeToFit()
        commentLabel.frame = CGRect(x: 10,
                                    y: titleLabel.height+10+10,
                                    width: frame.size.width - 20,
                                    height: 20)
        print("commentLabel Size = \(commentLabel.frame.size.height)")
        commentLabel.sizeToFit()
        print("commentLabel Size = \(commentLabel.frame.size.height)")
        heightVar = Int(commentLabel.frame.size.height - 20)
        dateLabel.frame = CGRect(x: frame.size.width - 120,
                                 y: frame.size.height - 20,
                                 width: 100,
                                 height: 20)
        dateLabel.sizeToFit()
        SwiftEventBus.post("UpdateCommentConstraint", sender: self)
    }

    func configure(with comment: Comment) {
        titleLabel.text = comment.title
        dateLabel.text = comment.date
        commentLabel.text = comment.comment
        id = comment.id
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
