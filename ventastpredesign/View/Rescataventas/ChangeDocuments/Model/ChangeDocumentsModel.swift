//
//  ChangeDocumentsModel.swift
//  ventastp
//
//  Created by Ulises Ortega on 29/09/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import UIKit

class ChangeDocumentsModel {

    let frontIDName = "Identificacion1.jpeg"
    let backIDName = "Identificacion2.jpeg"
    let proofOfAddressName = "ComprobanteDomicilio1.jpeg"
    let idType = "Identificación del representante legal"
    let proofOfAddressType = "Comprobante de domicilio"
    let contentType = "image/jpeg"

}
