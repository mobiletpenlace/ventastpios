//
//  Docs.swift
//  ventastp
//
//  Created by Ulises Ortega on 13/09/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class Docs: BaseResponse {

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        doscumentosWRP <- map["documentosWRP"]
    }

    var doscumentosWRP: [DocumentoWRP]?

}
