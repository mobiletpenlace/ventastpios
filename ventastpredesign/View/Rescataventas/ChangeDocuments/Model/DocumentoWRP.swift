//
//  DocumentoWRP.swift
//  ventastp
//
//  Created by Ulises Ortega on 13/09/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class DocumentoWRP: BaseResponse {

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        tipoDocumento <- map["tipoDocumento"]
        tipoArchivo <- map["tipoArchivo"]
        nombreArchivo <- map["nombreArchivo"]
        idDocumento <- map["idDocumento"]
        body <- map ["body"]
    }

    var tipoDocumento: String?
    var tipoArchivo: String?
    var nombreArchivo: String?
    var idDocumento: String?
    var body: String?

}
