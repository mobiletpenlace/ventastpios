//
//  InfoOpp.swift
//  ventastp
//
//  Created by Ulises Ortega on 29/09/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class InfoOpp: NSObject, Mappable {
    required init?(map: Map) {
        super.init()
    }

    func mapping(map: Map) {
        docs <- map["docs"]
        motivoDeRescate <- map["motivoDeRescate"]
    }

    override init() {
        super.init()
    }

    var docs: Docs?
    var motivoDeRescate: String?

}
