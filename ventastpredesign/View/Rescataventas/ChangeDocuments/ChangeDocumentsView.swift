//
//  ChangeDocumentsView.swift
//  ventastp
//
//  Created by Ulises Ortega on 05/09/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import UIKit
import Photos
import PhotosUI
import SwiftEventBus

@available(iOS 14, *)
class ChangeDocumentsView: UIViewController {

    private var idObject: String?
    private var BRMAcc: String?

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var confirmButton: UIButton!

    @IBOutlet weak var frontIDImageView: UIImageView!
    @IBOutlet weak var changeFrontButton: UIButton!

    @IBOutlet weak var backIDImageView: UIImageView!
    @IBOutlet weak var changeBackButton: UIButton!

    @IBOutlet weak var proofOfAddressImageView: UIImageView!
    @IBOutlet weak var changeProofButton: UIButton!

    @IBOutlet weak var proofOfAddressStackView: UIStackView!
    @IBOutlet weak var proofOfAddressLabel: UILabel!

    let viewModel = ChangeDocumentsViewModel()

    deinit {
        print("Deiniti CHANGE DOCUMENTS VIEW")
        SwiftEventBus.unregister(self)
        ProgressHUD.dismiss()
    }

    private var previousFrontImage: UIImage?
    private var previousBackImage: UIImage?
    private var previousProofOfAddress: UIImage?

    private var buttonPressed = 0
    private var changeFront = false
    private var changeBack = false
    private var changeProof = false
    private var viewAlreadyShowed = false

    private var documentsToChange: [String: UIImage] = [:]
    private var previousIDs: [String: [String]] = ["Identificacion1.jpeg": [], "Identificacion2.jpeg": [], "ComprobanteDomicilio1.jpeg": []]

    override func viewDidLoad() {
        super.viewDidLoad()
        backButton.setTitle("", for: .normal)
        confirmButton.isEnabled = false
        getDocuments()

        SwiftEventBus.onMainThread(self, name: "NoDocumentsFound", handler: { _ in
            ProgressHUD.showError("No hay documentos")
        })

        SwiftEventBus.onMainThread(self, name: "SetFrontID", handler: { [weak self] result in
            guard let tuple = result?.object as? (image: UIImage, documentID: String) else {return}
            print("Entro al SETFRONTID")
            self?.previousFrontImage = tuple.image
            self?.previousIDs["Identificacion1.jpeg"]?.append(tuple.documentID)
            self?.frontIDImageView.image = tuple.image
            ProgressHUD.dismiss()
        })

        SwiftEventBus.onMainThread(self, name: "SetBackID", handler: { [weak self] result in
            guard let tuple = result?.object as? (image: UIImage, documentID: String) else {return}
            print("Entro al SETBACKID")
            self?.previousBackImage = tuple.image
            self?.previousIDs["Identificacion2.jpeg"]?.append(tuple.documentID)
            self?.backIDImageView.image = tuple.image
            ProgressHUD.dismiss()
        })

        SwiftEventBus.onMainThread(self, name: "SetProofOfAddress", handler: { [weak self] result in
            guard let tuple = result?.object as? (image: UIImage, documentID: String) else {return}
            print("Entro al SETPROOFOFADDRESS")
            self?.previousProofOfAddress = tuple.image
            self?.previousIDs["ComprobanteDomicilio1.jpeg"]?.append(tuple.documentID)
            self?.proofOfAddressImageView.image = tuple.image
            self?.proofOfAddressLabel.isHidden = false
            self?.proofOfAddressStackView.isHidden = false
            ProgressHUD.dismiss()
        })

//        SwiftEventBus.onMainThread(self, name: "NewDocumentsLoaded", handler: { [weak self] _ in
//            self?.exitViewSuccess()
//        })
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !viewAlreadyShowed {
            ProgressHUD.show()
            viewAlreadyShowed = true
        }
    }

    init(idObject: String, BRMAcc: String) {
        self.idObject = idObject
        self.BRMAcc = BRMAcc
        super.init(nibName: nil, bundle: nil)
    }

    @IBAction func actionChangeFrontDocument(_ sender: Any) {
        if changeFront {
            frontIDImageView.image = previousFrontImage
            documentsToChange.removeValue(forKey: "Identificacion1.jpeg")
            changeFront = false
            confirmButton.isEnabled = changeExists() ? true : false
            setButtonChange(button: changeFrontButton)
        } else {
            buttonPressed = 1
            presentChooseImageAlert()
        }
    }

    func changeExists() -> Bool {
        if !changeFront && !changeBack && !changeProof {
            return false
        }
        return true
    }

    @IBAction func actionChangeBackDocument(_ sender: Any) {
        if changeBack {
            backIDImageView.image = previousBackImage
            documentsToChange.removeValue(forKey: "Identificacion2.jpeg")
            changeBack = false
            confirmButton.isEnabled = changeExists() ? true : false
            setButtonChange(button: changeBackButton)
        } else {
            buttonPressed = 2
            presentChooseImageAlert()
        }
    }
    @IBAction func actionChangeProofOfAddress(_ sender: Any) {
        if changeProof {
            proofOfAddressImageView.image = previousProofOfAddress
            documentsToChange.removeValue(forKey: "ComprobanteDomicilio1.jpeg")
            changeProof = false
            confirmButton.isEnabled = changeExists() ? true : false
            setButtonChange(button: changeProofButton)
        } else {
            buttonPressed = 3
            presentChooseImageAlert()
        }
    }

    func getDocuments() {
        viewModel.getDocuments(idObject: idObject)
    }

    func exitViewSuccess() {
        ProgressHUD.showSuccess()
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5, execute: {
            SwiftEventBus.post("SuccessDocumentsChanged")
            self.dismiss(animated: true)
        })
    }

    func setButtonChange(button: UIButton?) {
        button?.tintColor = .systemBlue
        button?.setTitle("Cambiar", for: .normal)
    }

    func setButtonCancel(button: UIButton?) {
        button?.tintColor = .systemRed
        button?.setTitle("Cancelar", for: .normal)
    }

    @IBAction func actionConfirm(_ sender: Any) {
        // TODO: Implement functionality to delete and add documents
        print(previousIDs)
        print(documentsToChange)
        (sender as! UIButton).isEnabled = false
        changeFrontButton.isEnabled = false
        changeBackButton.isEnabled = false
        changeProofButton.isEnabled = false
        ProgressHUD.show()
        viewModel.updateDocuments(BRMAcc: BRMAcc ?? "", idOportunity: idObject ?? "", documents: documentsToChange, previousIDs: previousIDs) {
            exitViewSuccess()
        }
    }

    func presentChooseImageAlert() {
        let actionSheet = UIAlertController(title: "", message: "Toma una foto o selecciona una desde la galería", preferredStyle: .actionSheet)
            // add camera action to imagePickerController
        actionSheet.addAction(UIAlertAction(title: "Camara", style: .default, handler: { [weak self] _ in
            self?.presentCamera()
        }))

        actionSheet.addAction(UIAlertAction(title: "Galería", style: .default, handler: { [weak self] _ in
            self?.presentImagePicker()
        }))

        actionSheet.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))

        self.present(actionSheet, animated: true, completion: nil)

    }

    @available(iOS 14, *)
    func presentImagePicker() {
        var config = PHPickerConfiguration(photoLibrary: .shared())
        config.selectionLimit = 1
        config.filter = .images
        let vc = PHPickerViewController(configuration: config)
        vc.delegate = self
        present(vc, animated: true)
    }

    func presentCamera() {
        let vc = UIImagePickerController()
        vc.sourceType = .camera
        vc.delegate = self
        present(vc, animated: true)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @IBAction func backButtonAction(_ sender: Any) {
        dismiss(animated: false)
    }

}

@available(iOS 14, *)
extension ChangeDocumentsView: PHPickerViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
        results.forEach { result in
            result.itemProvider.loadObject(ofClass: UIImage.self, completionHandler: { [weak self] reading, error in
                guard let image = reading as? UIImage, error == nil, let self = self else {return}
                switch self.buttonPressed {
                case 1:
                    DispatchQueue.main.sync {
                        self.frontIDImageView.image = image
                        self.setButtonCancel(button: self.changeFrontButton)
                    }
                    self.documentsToChange["Identificacion1.jpeg"] = image
                    self.changeFront = true
                case 2:
                    DispatchQueue.main.sync {
                        self.backIDImageView.image = image
                        self.setButtonCancel(button: self.changeBackButton)
                    }
                    self.documentsToChange["Identificacion2.jpeg"] = image
                    self.changeBack = true
                case 3:
                    DispatchQueue.main.sync {
                        self.proofOfAddressImageView.image = image
                        self.setButtonCancel(button: self.changeProofButton)
                    }
                    self.documentsToChange["ComprobanteDomicilio1.jpeg"] = image
                    self.changeProof = true
                default:
                    print("No case found")
                }
                if self.changeFront || self.changeBack || self.changeProof {
                    DispatchQueue.main.async {
                        self.confirmButton.isEnabled = true
                    }
                }
            })
        }
        picker.dismiss(animated: true)
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String: Any]) {
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            print("No image found")
            return
        }
        switch self.buttonPressed {
        case 1:
            self.frontIDImageView.image = image
            self.documentsToChange["Identificacion1.jpeg"] = image
            self.changeFront = true
            self.setButtonCancel(button: self.changeFrontButton)
            picker.dismiss(animated: true)
        case 2:
            self.backIDImageView.image = image
            self.documentsToChange["Identificacion2.jpeg"] = image
            self.changeBack = true
            self.setButtonCancel(button: self.changeBackButton)
            picker.dismiss(animated: true)
        case 3:
            self.proofOfAddressImageView.image = image
            self.documentsToChange["ComprobanteDomicilio1.jpeg"] = image
            self.changeProof = true
            self.setButtonCancel(button: self.changeProofButton)
            picker.dismiss(animated: true)
        default:
            print("No case found")
        }
        if self.changeFront || self.changeBack || self.changeProof {
            self.confirmButton.isEnabled = true
        }
    }

}
