//
//  ChangeDocumentsViewModel.swift
//  ventastp
//
//  Created by Ulises Ortega on 05/09/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import Alamofire
import SwiftyJSON
import SwiftEventBus
import SwiftCoroutine

class ChangeDocumentsViewModel {
    // Implement logic

    let model = ChangeDocumentsModel()
    // REMOVE
    var deletesPending = 0
    // REMOVE

    func imageWithSize(image: UIImage, size: CGSize) -> UIImage {
        var scaledImageRect = CGRect.zero
        let aspectWidth: CGFloat = size.width / image.size.width
        let aspectHeight: CGFloat = size.height / image.size.height
        let aspectRatio: CGFloat = min(aspectWidth, aspectHeight)
        scaledImageRect.size.width = image.size.width * aspectRatio
        scaledImageRect.size.height = image.size.height * aspectRatio
        scaledImageRect.origin.x = (size.width - scaledImageRect.size.width) / 2.0
        scaledImageRect.origin.y = (size.height - scaledImageRect.size.height) / 2.0
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        image.draw(in: scaledImageRect)
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return scaledImage!
    }

    func convertImageTobase64(image: UIImage) -> String? {
        var imageData: Data?
        var imageDataantes: Data?
        imageDataantes  = UIImageJPEGRepresentation(image, 1)
        let widthInPixels = image.size.width * image.scale
        let heightInPixels = image.size.height * image.scale
        print("tamaño de la imagen original en pixeles \(widthInPixels) x \(heightInPixels)")
        let imageSize1: Int = imageDataantes!.count
        let heightTemp = (300 * heightInPixels) / widthInPixels
        print(heightTemp)
        print("size of image antes de in KB: %f ", Double(imageSize1) / 1024.0)
        imageData  = UIImageJPEGRepresentation(imageWithSize(image: image, size: CGSize(width: 300, height: heightTemp)), 1)
        // size image
        let imageSize: Int = imageData!.count
        print("size of image despues de in KB: %f ", Double(imageSize) / 1024.0)
        //
        return imageData?.base64EncodedString()
    }

    func updateDocuments(BRMAcc: String, idOportunity: String, documents: [String: UIImage], previousIDs: [String: [String]], completion: () -> Void) {
        for document in documents {
            switch document.key {
            case model.frontIDName:
                uploadDocument(previousIDs: previousIDs, BRMAcc: BRMAcc, idOportunity: idOportunity, image: document.value, type: model.idType, name: model.frontIDName) {
                    self.deletePreviousDocuments(idOpportunity: idOportunity, previousIDS: previousIDs, documentName: self.model.frontIDName)
                }
            case model.backIDName:
                uploadDocument(previousIDs: previousIDs, BRMAcc: BRMAcc, idOportunity: idOportunity, image: document.value, type: model.idType, name: model.backIDName) {
                    self.deletePreviousDocuments(idOpportunity: idOportunity, previousIDS: previousIDs, documentName: self.model.backIDName)
                }
            case model.proofOfAddressName:
                uploadDocument(previousIDs: previousIDs, BRMAcc: BRMAcc, idOportunity: idOportunity, image: document.value, type: model.proofOfAddressType, name: model.proofOfAddressName) {
                    self.deletePreviousDocuments(idOpportunity: idOportunity, previousIDS: previousIDs, documentName: self.model.proofOfAddressName)
                }
            default:
                print("Error: error en el nombre del documento")

            }
        }
        completion()
    }

    func uploadDocument(previousIDs: [String: [String]], BRMAcc: String, idOportunity: String, image: UIImage, type: String, name: String, completion: @escaping () -> Void) {
        let request = AddDocumentRequest()
        let document = Documento()

        document.Body = convertImageTobase64(image: image)
        document.ContentType = model.contentType
        document.Tipo = type
        document.Nombre = name
        document.syncStatus = "1"
        document.OportunidadId = idOportunity
        document.CuentaBRM = BRMAcc

        request.MLogin = Login()
        request.MLogin?.Ip = "1.1.1.1"
        request.MLogin?.User = "25631"
        request.MLogin?.SecretWord = "Middle100$"
        request.MDocumentosAdjuntos = DocumentosAdjuntos()
        request.MDocumentosAdjuntos?.CuentaBRM = BRMAcc
        request.MDocumentosAdjuntos?.Documento.append(document)
        request.MDocumentosAdjuntos?.OportunidadId = idOportunity

        print("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&")
        let params = Mapper().toJSON(request)
        let paramsPretty = Mapper().toJSONString(request, prettyPrint: true)
        print(paramsPretty)
        print("prueba1 + \(AppDelegate.API_TOTAL)")
//        deletePreviousDocuments(idOpportunity: idOportunity, previousIDS: previousIDs, documentName: name)
        Alamofire.request(AppDelegate.API_TOTAL + ApiDefinition.API_QUERY_UPLOAD_DOCUMENT, method: .post, parameters: params, encoding: JSONEncoding.default, headers: ApiDefinition.headersMiddle).responseJSON { response in
            print("prueba2")
            switch response.result {
            case .success(let value):
                print("prueba3")
                let json = JSON(value)
                print("JSON UPLOAD RESULT: \(json)")
                completion()
            case .failure(let error):
                print("prueba4")
                print(error)
                return
            }
        }

    }

    func deletePreviousDocuments(idOpportunity: String, previousIDS: [String: [String]], documentName: String) {
        if let array = previousIDS[documentName] {
            let docs = Docs()
            docs.doscumentosWRP = []
            for documentID in array {
                let documentWRP = DocumentoWRP()
                documentWRP.idDocumento = documentID
                docs.doscumentosWRP?.append(documentWRP)
            }
            let infoRequest = InfoOpp()
            infoRequest.docs = docs
            let params = Mapper().toJSON(infoRequest)
            let paramsEncr = AES128.shared.code(plainText: Mapper().toJSONString(infoRequest, prettyPrint: true )!)
            print("////////////////////////////////////")
            print(params)
            print(paramsEncr)
            guard let paramsEncr = paramsEncr else {return}
            let parameters: Parameters = [
                "accion": "EliminarDocumentos",
                "idOportunidad": idOpportunity,
                "infoOpp": paramsEncr
            ]
            Alamofire.request(AppDelegate.API_SALESFORCE + ApiDefinition.API_CONSULTA_VENTA, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: ApiDefinition.headersToken).responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    // REMOVE
                    self.deletesPending -= 1
                    print("deletesPending: \(self.deletesPending)")
                    print("JSON DELETE RESULT: \(json)")
                    // REMOVE
                case .failure(let error):
                    print(error)
                }
            }
        } else {
            print("No id found for the key \(documentName)")
        }
    }

    func getDocuments(idObject: String?) {
        guard let idObject = idObject else {return}
        let parameters: Parameters = [
            "accion": "ConsultaDocumentos",
            "idOportunidad": idObject
        ]
        if ConnectionUtils.isConnectedToNetwork() {
            // Connection is active
            Alamofire.request(AppDelegate.API_SALESFORCE + ApiDefinition.API_CONSULTA_VENTA, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: ApiDefinition.headersToken).responseJSON {[weak self] response in
                guard let self = self else {return}
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print("JSON comment: \(json)")
                    let documentsResponse = ConsultaOpResponse(JSONString: json.rawString() ?? "")
                    guard let documents = documentsResponse?.info?.docs?.doscumentosWRP else {return}
                    if documents.count == 0 {
                        SwiftEventBus.post("NoDocumentsFound")
                    }
                    for document in documents {
                        if let documentData = document.body {
                            if let data = Data(base64Encoded: documentData), let idDocumento = document.idDocumento {
                                let image = UIImage(data: data)
                                switch document.nombreArchivo {
                                case "Identificacion1.jpeg":
                                    SwiftEventBus.post("SetFrontID", sender: (image: image, documentID: idDocumento))
                                case "Identificacion2.jpeg":
                                    SwiftEventBus.post("SetBackID", sender: (image: image, documentID: idDocumento))
                                case "ComprobanteDomicilio1.jpeg":
                                    SwiftEventBus.post("SetProofOfAddress", sender: (image: image, documentID: idDocumento))
                                default:
                                    print("Nombre de documento incorrecto")
                                }
                            }
                        }
                    }
                case .failure(let error):
                    print(error)
                }
            }
        } else {
            // Conection error
        }
    }
}
