//
//  StatusTrayView.swift
//  ventastp
//
//  Created by Ulises Ortega on 15/08/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

@available(iOS 13.0, *)
class StatusTrayView: UIViewController {

    let viewModel = StatusTrayViewModel()
    var prospectsTotal: [Oportunidad] = []
    var prospectsFiltered: [Oportunidad] = []
    let searchController = UISearchController()
    var numeroPRUEBA = 0
    var statusPRUEBA = true

    deinit {
        print("DEINIT STATUSTABLE VC")
        SwiftEventBus.unregister(self)
        ProgressHUD.dismiss()
    }

    @IBOutlet weak var noContentLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        configTableView()
        configNavigationView()
        loadProspects()
        ProgressHUD.show()

        SwiftEventBus.onMainThread(self, name: "ProspectsLoaded", handler: {[weak self] result in
            guard let prospects = result?.object as? [Oportunidad] else {return}
            self?.prospectsTotal = prospects
            self?.prospectsFiltered = prospects
            self?.tableView.reloadData()
            ProgressHUD.dismiss()
        })

        SwiftEventBus.onMainThread(self, name: "Prospects_Name_Filtered", handler: { [weak self] result in
            guard let prospects = result?.object as? [Oportunidad] else {return}
            if prospects.count < 1 {
                self?.noContentLabel.isHidden = false
            } else {
                self?.noContentLabel.isHidden = true
            }
            self?.prospectsFiltered = prospects
            self?.tableView.reloadData()
        })

        SwiftEventBus.onMainThread(self, name: "UpdateProspects", handler: { [weak self] _ in
            ProgressHUD.showSuccess("Documentos actualizados")
            self?.loadProspects()
            self?.searchController.searchBar.text = nil
            self?.searchController.searchBar.resignFirstResponder()
        })
    }

    func configNavigationView() {
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.title = viewModel.getScreenTitle()
        self.navigationItem.searchController = searchController
        self.navigationItem.searchController?.searchBar.delegate = self
        self.navigationItem.searchController?.searchBar.setValue("Cancelar", forKey: "cancelButtonText")
        self.navigationItem.searchController?.searchBar.scopeButtonTitles = viewModel.getStatusArray()
        self.navigationItem.searchController?.searchBar.showsScopeBar = true
        self.navigationItem.hidesSearchBarWhenScrolling = false
        self.navigationItem.searchController?.searchBar.placeholder = viewModel.getSearchBarPlaceholder()
        self.navigationController?.navigationBar.topItem?.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "Back_Arrow_Black"), style: .plain, target: self, action: #selector(backAction))
        self.navigationController?.navigationBar.topItem?.leftBarButtonItem?.tintColor = .black
    }

    func configTableView() {
        tableView.register(UINib(nibName: "StatusTableViewCell", bundle: nil), forCellReuseIdentifier: "statusCell")
        tableView.delegate = self
        tableView.dataSource = self
    }

    @objc func backAction() {
        print("BackPressed")
        self.navigationController?.dismiss(animated: false)
        self.navigationController?.viewControllers.removeAll()
    }

    func loadProspects() {
        viewModel.loadProspects()
    }

    func filterProspects(string: String, prospects: [Oportunidad], selectedStatus: String) {
        viewModel.filterProspects(string: string, prospects: prospects, selectedStatus: selectedStatus)
    }

}

@available(iOS 13.0, *)
extension StatusTrayView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 107
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return prospectsFiltered.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "statusCell", for: indexPath) as! StatusTableViewCell
        cell.prospect = prospectsFiltered[indexPath.section]
        cell.configureCell()
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedProspect = prospectsFiltered[indexPath.section]
        let array = viewModel.getStatusArray()
        // Check if selected status is Factibilidad or !exist in status Array and shows error message if true
        if !(array.contains(where: { $0.lowercased() == selectedProspect.etapa.lowercased() })) || selectedProspect.etapa == "Factibilidad" {
            ProgressHUD.showError("No puede modificar esta venta")
            return
        }
        let vc = ProspectDetailView()
        vc.prospect = selectedProspect
//      vc.fillProspectDetail()
        vc.modalPresentationStyle = .overFullScreen
        present(vc, animated: false)
    }

}

@available(iOS 13.0, *)
extension StatusTrayView: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let text = searchBar.text else {return}
        filterProspects(string: text, prospects: prospectsTotal, selectedStatus: viewModel.getStatusArray()[searchBar.selectedScopeButtonIndex])
    }

    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        guard let text = searchBar.text else {return}
        filterProspects(string: text, prospects: prospectsTotal, selectedStatus: viewModel.getStatusArray()[selectedScope])
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        filterProspects(string: "", prospects: prospectsTotal, selectedStatus: viewModel.getStatusArray()[searchBar.selectedScopeButtonIndex])
        tableView.reloadData()
    }
}
