//
//  StatusTrayViewModel.swift
//  ventastp
//
//  Created by Ulises Ortega on 15/08/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import Alamofire
import SwiftyJSON
import SwiftEventBus

class StatusTrayViewModel {
    let model = StatusTrayModel()

    func getScreenTitle() -> String {
        return model.screenTitle
    }

    func getSearchBarPlaceholder() -> String {
        return model.searchBarPlaceHolder
    }

    func getEmptyMessage() -> String {
        return model.emptyMessage
    }

    func getStatusArray () -> [String] {
        return model.statusArray
    }

    func filterProspects(string: String, prospects: [Oportunidad], selectedStatus: String) {
        let text = string.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        var newProspects: [Oportunidad] = []
        if selectedStatus != "Todas" {
            if text.isNumeric {
                newProspects = prospects.filter { $0.numeroCuenta.contains(string: text) && $0.etapa.lowercased() == selectedStatus.lowercased() }
            } else if text.isEmpty {
                newProspects = prospects.filter { $0.etapa.lowercased() == selectedStatus.lowercased() }
            } else {
                newProspects = prospects.filter { $0.nombre.lowercased().contains(string: text.lowercased()) && $0.etapa.lowercased() == selectedStatus.lowercased() }
            }
        } else {
            if text.isNumeric {
                newProspects = prospects.filter { $0.numeroCuenta.contains(string: text) }
            } else if text.isEmpty {
                newProspects = prospects
            } else {
                newProspects = prospects.filter { $0.nombre.lowercased().contains(string: text.lowercased()) }
            }
        }
        SwiftEventBus.post("Prospects_Name_Filtered", sender: newProspects)
    }

    func loadProspects() {
        AES128.shared.setKey(key: statics.key)
        if RealManager.findFirst(object: Empleado.self) != nil {
            // Empleado exisits in Realm
            let empleado = Empleado(value: RealManager.findFirst(object: Empleado.self)!)
            let id = empleado.Id
            print("ID del empleado: \(id)")
            let parameters: Parameters = [
                "idEmpleado": id
            ]
            if ConnectionUtils.isConnectedToNetwork() {
                // Connection is active
                Alamofire.request(AppDelegate.API_SALESFORCE + ApiDefinition.API_CONSULTA_OPORTUNIDADES, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: ApiDefinition.headersToken).responseJSON {[weak self] response in
                    guard let self = self else {return}
                    switch response.result {
                    case .success(let value):
                        let json = JSON(value)
                        let oportunidadResponse = OportunidadResponse(JSONString: json.rawString() ?? "")
                        guard let oportunidades = oportunidadResponse?.oportunidades else {return}
                        let oportunidadesJSON: String = AES128.shared.decode(coded: oportunidades) ?? ""
                        print("prospects")
                        print(oportunidadesJSON)
                        guard let oportunidadesObjects: [Oportunidad] = [Oportunidad].init(JSONString: oportunidadesJSON) else {return}
                        SwiftEventBus.post("ProspectsLoaded", sender: oportunidadesObjects)
                    case .failure(let error):
                        print(error)
                    }
                }
            } else {
                // Conection error
            }
        } else {
            // Empleado not found in Realm
            print("ERROR, no se encontró el empleado")
        }
    }
}
