//
//  StatusTableViewCell.swift
//  ventastp
//
//  Created by Ulises Ortega on 16/08/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import UIKit

class StatusTableViewCell: UITableViewCell {
    @IBOutlet weak var clientNameLabel: UILabel!
    @IBOutlet weak var accNumberLabel: UILabel!
//    @IBOutlet weak var dateLabel: UILabel!
//    @IBOutlet weak var hourLabel: UILabel!
//    @IBOutlet weak var motiveLabel: UILabel!
    @IBOutlet weak var statusButton: UIButton!

    @IBOutlet weak var motiveView: UIView!
    @IBOutlet weak var dateView: UIView!

    var prospect: Oportunidad?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    func configureCell() {
        clientNameLabel.text = prospect?.nombre
        accNumberLabel.text = prospect?.numeroCuenta
        statusButton.setTitle(prospect?.etapa, for: .normal)
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
//        let date = prospect?.fecha.date(dateFormatter)
//        dateLabel.text = date?.getFormatted("dd/MM/yyyy") ?? "--/--/--"
//        hourLabel.text = (date?.getFormatted("HH:mm") ?? "--:--") + "hrs"

        if #available(iOS 15.0, *) {
            var config = UIButton.Configuration.filled()
            config.titleTextAttributesTransformer = UIConfigurationTextAttributesTransformer { incoming in
                var outgoing = incoming
                outgoing.font = UIFont.systemFont(ofSize: 12, weight: .bold)
                return outgoing
            }
            statusButton.configuration = config
        }

        switch prospect?.etapa {
        case "Perdida":
            statusButton.tintColor = .systemRed
//            motiveView.isHidden = false
//            dateView.isHidden = true
//            self.isUserInteractionEnabled = true
        case "Crédito":
//            statusButton.setTitle("Enviada", for: .normal)
            statusButton.tintColor = .systemOrange
//            motiveView.isHidden = true
//            dateView.isHidden = false
//            self.isUserInteractionEnabled = true
        case "Ganada":
            statusButton.tintColor = .systemGreen
//            motiveView.isHidden = true
//            dateView.isHidden = false
//            self.isUserInteractionEnabled = true
        case "Factibilidad":
            statusButton.tintColor = .systemPurple
//            motiveView.isHidden = true
//            dateView.isHidden = false
//            self.isUserInteractionEnabled = false
        default:
            statusButton.tintColor = .systemGray
//            motiveView.isHidden = true
//            dateView.isHidden = false
//            self.isUserInteractionEnabled = false
            return
        }
    }

}
