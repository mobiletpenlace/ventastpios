//
//  StatusTrayModel.swift
//  ventastp
//
//  Created by Ulises Ortega on 15/08/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import UIKit

class StatusTrayModel {
    let screenTitle = "Bandeja de estatus"
    let searchBarPlaceHolder = "Buscar por nombre/cuenta"
    let emptyMessage = "No hay coincidencias"
    let statusArray = ["Todas", "Ganada", "Perdida", "Crédito", "Factibilidad"]
}
