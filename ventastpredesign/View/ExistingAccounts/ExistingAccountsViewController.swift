//
//  ExistingAccountsViewController.swift
//  ventastp
//
//  Created by Armando Isais Olguin Cabrera on 09/02/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import SwiftEventBus

class ExistingAccountsViewController: BaseVentasView, UISearchBarDelegate, ExistingAccountObserver {
    func successSearchAccounts(response: InfoAccountNameResponse) {
        print("successSearchAccounts")
    }

    func onError(errorMessage: String) {
        print("onError")

    }

    @IBOutlet weak var typeReportContainer: UIView!
    @IBOutlet weak var searchViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchButtonImage: UIImageView!
    @IBOutlet weak var buttonSearch: UISearchBar!

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var mMainContainerView: UIView!

//    var viewModel: ExistingAccountsViewModel?
    var isSearchViewHide = false
    var mExistingAccountViewModel: ExistingAccountViewModel!

    var mComissionsViewModel: ComissionsViewModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setView(view: self.view)
        searchBar.delegate = self
        ViewEmbedder.embed(withIdentifier: "ListAccountsViewController", parent: self, container: mMainContainerView)
        mExistingAccountViewModel = ExistingAccountViewModel(view: self)
        mExistingAccountViewModel.atachView(observer: self)

    }

    @IBAction func backAction(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if searchBar.text!.count > 3 {
            searchAccounts(searchText: searchBar.text!)
            searchBar.resignFirstResponder()
        } else {

            searchBar.resignFirstResponder()
        }
    }

    func searchAccounts(searchText: String) {
        let request = InfoAccountNameRequest()
        let numbersRange = searchText.rangeOfCharacter(from: .decimalDigits)
        let hasNumbers = (numbersRange != nil)
        if hasNumbers == true {
            request.noCuenta = searchText
            request.pais = "MX"
        } else {
            request.nombre = searchText
            request.pais = "MX"
        }
        mExistingAccountViewModel.consultExistingAccounts(request: request)
    }

}
