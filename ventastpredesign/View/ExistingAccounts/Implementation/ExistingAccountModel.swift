//
//  ExistingAccountModel.swift
//  ventastp
//
//  Created by Armando Isais Olguin Cabrera on 21/02/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

protocol ExistingAccountModelObserver {
    func successSearchAccounts (response: InfoAccountNameResponse)
    func onError(errorMessage: String)// On error func
}

class ExistingAccountModel: BaseModel {

    var viewModel: ExistingAccountModelObserver?
    var server: ServerDataManager2<InfoAccountNameResponse>?
    var urlSearchAccounts = AppDelegate.API_SALESFORCE + ApiDefinition.API_SEARCH

    init (observer: BaseViewModelObserver) {
        super.init(observerVM: observer)
        // Se declara un server
        server = ServerDataManager2<InfoAccountNameResponse>()

    }

    func atachModel(viewModel: ExistingAccountModelObserver) {
        self.viewModel = viewModel
    }

    func searchAccounts(request: InfoAccountNameRequest) {
        urlSearchAccounts = AppDelegate.API_SALESFORCE + ApiDefinition.API_SEARCH
        server?.setValues(requestUrl: urlSearchAccounts, delegate: self, headerType: .headersSalesforce)
        server?.request(requestModel: request)
    }

    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if requestUrl == urlSearchAccounts {
            let infoAccountNameResponse = response as! InfoAccountNameResponse
            if infoAccountNameResponse.result == "0" {
                viewModel?.successSearchAccounts(response: response as! InfoAccountNameResponse)
            } else {
                viewModel?.onError(errorMessage: infoAccountNameResponse.result ?? "Favor de validar de nuevo")
            }
        }
  }

    override func onErrorLoadResponse(requestUrl: String, messageError: String) {
        viewModel?.onError(errorMessage: messageError)
    }

}
