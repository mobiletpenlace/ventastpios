//
//  ExistingAccountViewModel.swift
//  ventastp
//
//  Created by Armando Isais Olguin Cabrera on 21/02/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import SwiftEventBus

protocol ExistingAccountObserver {
    func successSearchAccounts  (response: InfoAccountNameResponse)
    func onError(errorMessage: String)
}

class ExistingAccountViewModel: BaseViewModel, ExistingAccountObserver, ExistingAccountModelObserver {

    func onError(errorMessage: String) {
        view.hideLoading()
        SwiftEventBus.post("withoutExistingAccounts", sender: nil)
    }

    func successSearchAccounts(response: InfoAccountNameResponse) {
        view.hideLoading()
        SwiftEventBus.post("sendExistingAccounts", sender: response.CuentasRelacionadas?.listBusquedaNombre)
    }

    func consultExistingAccounts(request: InfoAccountNameRequest) {
        view.showLoading(message: "Consultado comisiones pagadas")
        model.searchAccounts(request: request)
    }

    var observer: ExistingAccountObserver?
    var model: ExistingAccountModel!

    override init(view: BaseVentasView) {
        super.init(view: view)
        model = ExistingAccountModel(observer: self)
        model.atachModel(viewModel: self)
    }

    func atachView(observer: ExistingAccountObserver) {
        self.observer = observer
    }

}
