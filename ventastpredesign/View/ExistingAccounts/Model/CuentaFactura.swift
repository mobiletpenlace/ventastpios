//
//  CuentaFactura.swift
//  ventastp
//
//  Created by Armando Isais Olguin Cabrera on 11/02/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class CuentaFactura: NSObject, Mappable {

    var informacionGeneral: [ListBusquedaNombre]?
    var direccionInstalacion: [ListBusquedaNombre]?

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        informacionGeneral <- map["informacionGeneral"]
        direccionInstalacion <- map ["direccionInstalacion"]

    }

    override init() {
    }

}
