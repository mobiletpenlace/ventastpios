//
//  InformacionGeneral.swift
//  ventastp
//
//  Created by Armando Isais Olguin Cabrera on 11/02/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class InformacionGeneral: NSObject, Mappable {

    var idPlanPostventa: String = ""

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        idPlanPostventa <- map["idPlanPostventa"]

    }

    override init() {
    }

}
