//
//  CuentasRelacionadas.swift
//  ventastp
//
//  Created by Armando Isais Olguin Cabrera on 11/02/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class CuentasRelacionadas: NSObject, Mappable {

    var listBusquedaNombre: [ListBusquedaNombre]?

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        listBusquedaNombre <- map["listBusquedaNombre"]

    }

    override init() {
    }

}
