//
//  ListBusquedaNombre.swift
//  ventastp
//
//  Created by Armando Isais Olguin Cabrera on 11/02/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class ListBusquedaNombre: NSObject, Mappable {

    var nombre: String = ""
    var idCuentaBrm: String = ""

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        nombre <- map["nombre"]
        idCuentaBrm <- map["idCuentaBrm"]
    }

    override init() {
    }

}
