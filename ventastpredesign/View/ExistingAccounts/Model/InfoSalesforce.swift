//
//  InfoSalesforce.swift
//  ventastp
//
//  Created by Armando Isais Olguin Cabrera on 11/02/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class InfoSalesforce: NSObject, Mappable {

    var cuentaFactura: [CuentaFactura]?

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        cuentaFactura <- map["cuentaFactura"]
    }

    override init() {
    }

}
