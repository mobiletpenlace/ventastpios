//
//  DatosCliente.swift
//  ventastp
//
//  Created by Armando Isais Olguin Cabrera on 11/02/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class DatosCliente: NSObject, Mappable {

    var listBusquedaNombre: [ListBusquedaNombre]?
    var accountNo: String = ""
    var name: String = ""
    var lastName: String = ""
    var dn: String = ""
    var ciudad: String = ""
    var idPackage: String = ""
    var packageDescription: String = ""
    var statusCuenta: String = ""

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        listBusquedaNombre <- map["listBusquedaNombre"]
        accountNo <- map ["accountNo"]
        name <- map ["name"]
        lastName <- map ["lastName"]
        dn <- map ["dn"]
        ciudad <- map ["ciudad"]
        idPackage <- map ["idPackage"]
        packageDescription <- map ["packageDescription"]
        statusCuenta <- map ["statusCuenta"]
    }

    override init() {
    }

}
