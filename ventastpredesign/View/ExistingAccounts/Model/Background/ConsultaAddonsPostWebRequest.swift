//
//  ConsultaAddonsPostWebRequest.swift
//  ventastp
//
//  Created by Armando Isais Olguin Cabrera on 11/02/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class ConsultaAddonsPostWebRequest: BaseRequest {

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        estimuloFiscal <- map["estimuloFiscal"]
        idPlan <- map["idPlan"]
        InfoConsultaAddonsPostWeb <- map["info"]
        numCuentaBRM <- map["numCuentaBRM"]
    }

    var estimuloFiscal: Bool?
    var idPlan: String?
    var InfoConsultaAddonsPostWeb: [InfoConsultaAddonsPostWeb]?
    var numCuentaBRM: String?

}
