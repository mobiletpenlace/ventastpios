//
//  InfoDashboardWebRequest.swift
//  ventastp
//
//  Created by Armando Isais Olguin Cabrera on 11/02/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class InfoDashboardWebRequest: BaseRequest {

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        numCuenta <- map["numCuenta"]
    }

    var numCuenta: String?

}
