//
//  RSTGetInfoCuentaResponse.swift
//  ventastp
//
//  Created by Armando Isais Olguin Cabrera on 11/02/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class RSTGetInfoCuentaResponse: BaseResponse {

     required init?(map: Map) {
       }

    override func mapping(map: Map) {
        Response <- map["Response"]
        datosCliente <- map["datosCliente"]
        resultDescription <- map["ResultDescription"]
    }
    var Response: Response?
    var datosCliente: DatosCliente?
    var IdCuentaFactura: String = ""
}
