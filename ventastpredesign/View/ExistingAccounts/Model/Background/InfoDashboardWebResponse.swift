//
//  InfoDashboardWebResponse.swift
//  ventastp
//
//  Created by Armando Isais Olguin Cabrera on 11/02/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class InfoDashboardWebResponse: BaseResponse {

    required init?(map: Map) {
       }

    override func mapping(map: Map) {
        infoSalesforce <- map["informacionSalesforce"]
        result <- map["result"]
        resultDescription <- map["ResultDescription"]
    }
    var CuentasRelacionadas: CuentasRelacionadas?
    var infoSalesforce: InfoSalesforce?
}
