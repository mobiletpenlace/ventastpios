//
//  InfoAccountNameRequest.swift
//  ventastp
//
//  Created by Armando Isais Olguin Cabrera on 11/02/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class InfoAccountNameRequest: BaseRequest {

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        noCuenta <- map["noCuenta"]
        nombre <- map["nombre"]
        pais <- map["pais"]
    }

    var noCuenta: String?
    var nombre: String?
    var pais: String?

}
