//
//  InfoAccountNameResponse.swift
//  ventastp
//
//  Created by Armando Isais Olguin Cabrera on 11/02/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class InfoAccountNameResponse: BaseResponse {

     required init?(map: Map) {
       }

    override func mapping(map: Map) {
        CuentasRelacionadas <- map["CuentasRelacionadas"]
        result <- map["result"]
        resultDescription <- map["ResultDescription"]
    }
    var CuentasRelacionadas: CuentasRelacionadas?
}
