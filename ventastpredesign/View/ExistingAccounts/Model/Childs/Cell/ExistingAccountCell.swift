//
//  ExistingAccountCell.swift
//  ventastp
//
//  Created by Armando Isais Olguin Cabrera on 17/02/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import UIKit

class ExistingAccountCell: UITableViewCell {

    @IBOutlet weak var nameAccountLabel: UILabel!
    @IBOutlet weak var numberAccountLabel: UILabel!
    @IBOutlet weak var accountDetailsButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        nameAccountLabel.adjustsFontSizeToFitWidth = true
        numberAccountLabel.adjustsFontSizeToFitWidth = true
        nameAccountLabel.minimumScaleFactor = 0.4
        numberAccountLabel.minimumScaleFactor = 0.2
    }
}
