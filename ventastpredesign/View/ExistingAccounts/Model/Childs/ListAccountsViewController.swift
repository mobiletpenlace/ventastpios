//
//  ListAccountsViewController.swift
//  ventastp
//
//  Created by Armando Isais Olguin Cabrera on 16/02/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class ListAccountsViewController: BaseVentasView {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var withoutDataLabel: UILabel!

    var reports: [ListBusquedaNombre] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        SwiftEventBus.onMainThread(self, name: "sendExistingAccounts") { [weak self] result in
                   guard let self = self else {return}
                   if let value = result?.object as? [ListBusquedaNombre] {
                       self.reports = value
                       self.reloadTable()
                   }
            self.unregister()
               }
        SwiftEventBus.onMainThread(self, name: "withoutExistingAccounts") { [weak self] _ in
                   guard let self = self else {return}
            self.withoutDataLabel.text = "No se encontraron cuentas existentes"
            self.unregister()
               }

    }

    override func viewDidAppear(_ animated: Bool) {
        withoutDataLabel.isHidden = false
        faseReportes.emptyReports = true
    }

    func unregister() {
        SwiftEventBus.unregister(self)
    }

}

extension ListAccountsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reports.count     }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExistingAccountCell") as! ExistingAccountCell
        cell.nameAccountLabel.text = reports[indexPath.row].nombre
        cell.numberAccountLabel.text = reports[indexPath.row].idCuentaBrm
        return cell
    }

    func reloadTable() {
        if reports.isEmpty {
            withoutDataLabel.isHidden = false
        } else {
            withoutDataLabel.isHidden = true
        }
        tableView.reloadData()
    }
}
