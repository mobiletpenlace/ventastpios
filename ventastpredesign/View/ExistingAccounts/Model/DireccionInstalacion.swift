//
//  DireccionInstalacion.swift
//  ventastp
//
//  Created by Armando Isais Olguin Cabrera on 11/02/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class DireccionInstalacion: NSObject, Mappable {

    var plaza: String = ""
    var distritoInstalacion: String = ""
    var coloniaInstalacion: String = ""
    var codigoPostalInstalacion: String = ""
    var clusterInstalacion: String = ""

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        plaza <- map["plaza"]
        distritoInstalacion <- map["distritoInstalacion"]
        coloniaInstalacion <- map["coloniaInstalacion"]
        codigoPostalInstalacion <- map["codigoPostalInstalacion"]
        clusterInstalacion <- map["clusterInstalacion"]
    }

    override init() {
    }

}
