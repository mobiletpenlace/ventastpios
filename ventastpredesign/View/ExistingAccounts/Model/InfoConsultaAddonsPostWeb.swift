//
//  InfoConsultaAddonsPostWeb.swift
//  ventastp
//
//  Created by Armando Isais Olguin Cabrera on 11/02/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class InfoConsultaAddonsPostWeb: BaseRequest {

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        canalFront <- map["canalFront"]
        cluster <- map["cluster"]
        colonia <- map["colonia"]
        distrito <- map["distrito"]
        metodoPago <- map["metodoPago"]
        plaza <- map["plaza"]
        codigoPostal <- map["codigoPostal"]
        consultaAddons <- map["consultaAddons"]
        consultaPromo <- map["consultaPromo"]
        consultaServ <- map["consultaServ"]
        subcanalVenta <- map["subcanalVenta"]
    }

    var canalFront: String?
    var cluster: String?
    var colonia: String?
    var distrito: String?
    var metodoPago: String?
    var plaza: String?
    var codigoPostal: String?
    var consultaAddons: String?
    var consultaPromo: String?
    var consultaServ: String?
    var subcanalVenta: String?

}
