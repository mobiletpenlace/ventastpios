//
//  MenuViewController.swift
//  ventastpredesign
//
//  Created by julian dorantes fuertes on 13/03/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus
import Amplitude
// import Identity

class MenuViewController: BaseVentasView {

    @IBOutlet weak var mSelect1Image: UIImageView!
    @IBOutlet weak var mSelect2Image: UIImageView!
    @IBOutlet weak var mSelect3Image: UIImageView!
    @IBOutlet weak var mSelect4Image: UIImageView!
    @IBOutlet weak var mSelect5Image: UIImageView!
    @IBOutlet weak var mReportimage: UIImageView!
    @IBOutlet weak var mCreateLeadImage: UIImageView!

    @IBOutlet weak var digitalTrayView: UIView!
    @IBOutlet weak var MCell3View: UIView!
    @IBOutlet weak var MCell4View: UIView!
    @IBOutlet weak var MCreateLeadView: UIView!
    @IBOutlet weak var mNombreLabel: UILabel!
    @IBOutlet weak var mPositionLabel: UILabel!
    @IBOutlet weak var mapSwitch: UISwitch!
    @IBOutlet weak var RHContainer: UIView!
    @IBOutlet weak var CreteLeadContainer: UIView!
    @IBOutlet weak var LeadIndicatorContainer: UIView!
    @IBOutlet weak var statusTrayContainer: UIView!
    @IBOutlet weak var mReportes: UIView!
    @IBOutlet weak var ChanelEmploye: UIView!
    @IBOutlet weak var ComisionsContainer: UIView!
    @IBOutlet weak var MySaleContainer: UIView!
    @IBOutlet weak var topLogoImageView: UIImageView!
    @IBOutlet weak var MapSwitchContainer: UIView!
    @IBOutlet weak var LogoutContainer: UIView!
    @IBOutlet weak var NewSales: UIView!
    @IBOutlet weak var SellersListContainer: UIView!
    @IBOutlet weak var testSectionButton: UIButton!

    @IBOutlet weak var salesMonitoringContainer: UIView!
    @IBOutlet weak var salesmenMonitoringContainer: UIView!
    @IBOutlet weak var coachMonitoringContainer: UIView!

    var isNewPerfil: Bool = true
    var isEmbajador: Bool!
    var currentProfile: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        NotificationCenter.default.addObserver(self, selector: #selector(handleCanalEmpleadoViewClosed), name: Notification.Name("CanalEmpleadoViewClosed"), object: nil)
        mNombreLabel.text = ""
        if isEmbajador {
            embajador()
        } else {
            normal()
        }
       configReportsView()
        if UserDefaults.standard.bool(forKey: "isAutoentrepreneur") {
            topLogoImageView.image = UIImage(named: "autoentrepreneurs_mini")
//            MapSwitchContainer.isHidden = true
            RHContainer.isHidden = true
            ChanelEmploye.isHidden = true
            MCell4View.isHidden = true
        }

    }

    @objc func handleCanalEmpleadoViewClosed() {
        // Llama a la función getEmployee cuando se cierre CanalEmpleadoView
        if let homeVC = self.navigationController?.viewControllers.first(where: { $0 is HomeScreenRedesign }) as? HomeScreenRedesign {
            homeVC.getEmploye()
        }
    }

    deinit {
        // Elimina el observador al liberar la vista
        NotificationCenter.default.removeObserver(self)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if RealManager.findFirst(object: Empleado.self) != nil {
            let empleado: Empleado = Empleado(value: RealManager.findFirst(object: Empleado.self)!)
            addEmployedOrListEmployed(position: empleado.Posicion)
            showMonitoringTray(position: empleado.Posicion)
        }
    }

    func addEmployedOrListEmployed(position: String) {
            switch position.uppercased() {
            case "JEFE DE PLAZA":
                self.SellersListContainer.isHidden = false
                self.NewSales.isHidden = false
                self.digitalTrayView.isHidden = true
            case "COACH":
                self.mReportes.isHidden = false
                self.SellersListContainer.isHidden = true
                self.NewSales.isHidden = true
                self.digitalTrayView.isHidden = true
            case "LIDER":
                self.SellersListContainer.isHidden = true
                self.NewSales.isHidden = true
                self.digitalTrayView.isHidden = true
            default:
                self.mReportes.isHidden = true
                self.SellersListContainer.isHidden = true
                self.NewSales.isHidden = true
            }
        }

    // TODO: Check if this func can be moved
    func showMonitoringTray(position: String) {
        switch position.uppercased() {
        case "EXPERTO":
            salesMonitoringContainer.isHidden = false
        case "COACH":
            salesmenMonitoringContainer.isHidden = false
        case "LIDER":
            coachMonitoringContainer.isHidden = false
        default:
            break
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

    }
    func embajador() {
        RHContainer.isHidden = true
        CreteLeadContainer.isHidden = true
        LeadIndicatorContainer.isHidden = false
        ChanelEmploye.isHidden = true
        ComisionsContainer.isHidden = true
        MySaleContainer.isHidden = false
        mReportes.isHidden = true
    }

    func normal() {
        mReportes.isHidden = false
        RHContainer.isHidden = false
        CreteLeadContainer.isHidden = true
        LeadIndicatorContainer.isHidden = true
        ChanelEmploye.isHidden = false
        ComisionsContainer.isHidden = true
        MySaleContainer.isHidden = true
    }

    @available(iOS 13.0, *)
    @IBAction func onClickMySalesMonitoring(_ sender: Any) {
        // TODO: Agregar la nueva pantalla aqui
        DeselectView()
        let navigation = UINavigationController()
        navigation.modalPresentationStyle = .fullScreen
        AES128.shared.setKey(key: statics.key)
        if RealManager.findFirst(object: Empleado.self) != nil {
            // Empleado exisits in Realm
            let empleado = Empleado(value: RealManager.findFirst(object: Empleado.self)!)
            let statusTrayVc = MonitoringTrayView(employeeID: empleado.Id, employeeName: empleado.Nombre)
//            statusTrayVc.employee = empleado
            navigation.pushViewController(statusTrayVc, animated: false)
            present(navigation, animated: false)
        } else {
            // DO Nothing
            print("Error, no se encontró el empleado en realm")
        }

    }

    @IBAction func onClickSalesmenMonitoring(_ sender: Any) {
        DeselectView()
        let navigation = UINavigationController()
        navigation.modalPresentationStyle = .fullScreen
        AES128.shared.setKey(key: statics.key)
        if RealManager.findFirst(object: Empleado.self) != nil {
            // Empleado exisits in Realm
            let empleado = Empleado(value: RealManager.findFirst(object: Empleado.self)!)
            let salesmenMonitoringView = SalesmenMonitoringView(employeeNum: empleado.NoEmpleado)
//            statusTrayVc.employee = empleado
            navigation.pushViewController(salesmenMonitoringView, animated: false)
            present(navigation, animated: false)
        } else {
            // DO Nothing
            print("Error, no se encontró el empleado en realm")
        }
    }

    @IBAction func onClickCoachesMonitoring(_ sender: Any) {
        DeselectView()
        let navigation = UINavigationController()
        navigation.modalPresentationStyle = .fullScreen
        AES128.shared.setKey(key: statics.key)
        if RealManager.findFirst(object: Empleado.self) != nil {
            // Empleado exisits in Realm
            let empleado = Empleado(value: RealManager.findFirst(object: Empleado.self)!)
            let salesmenMonitoringView = CoachesListView(employeeNum: empleado.NoEmpleado)
//            statusTrayVc.employee = empleado
            navigation.pushViewController(salesmenMonitoringView, animated: false)
            present(navigation, animated: false)
        } else {
            // DO Nothing
            print("Error, no se encontró el empleado en realm")
        }
    }

    @IBAction func NotificationClic(_ sender: Any) {
        DeselectView()
        mSelect3Image.isHidden = false
        MCell3View.layer.backgroundColor = Colors.LowGray
        Constants.LoadStoryBoard(name: storyBoard.Notification, viewC: self)
    }

    @IBAction func MVideos(_ sender: Any) {
        DeselectView()
        mSelect4Image.isHidden = false
        MCell4View.layer.backgroundColor = Colors.LowGray
        Constants.LoadStoryBoard(name: storyBoard.Video, viewC: self)
    }

    @IBAction func digitalTrayAction(_ sender: Any) {
        DeselectView()
        digitalTrayView.layer.backgroundColor = Colors.LowGray
        Constants.LoadStoryBoard(name: storyBoard.DigitalTray, viewC: self)
    }

    @available(iOS 13.0, *)
    @IBAction func statusTrayAction(_ sender: Any) {
        DeselectView()
        let navigation = UINavigationController()
        navigation.modalPresentationStyle = .fullScreen
        let statusTrayVc = StatusTrayView()
        navigation.pushViewController(statusTrayVc, animated: false)
        present(navigation, animated: false)
    }

    @available(iOS 13.0, *)
        @IBAction func osTrayAction(_ sender: Any) {
            DeselectView()
            let navigation = UINavigationController()
            navigation.modalPresentationStyle = .fullScreen
            let statusTrayVc = ServiceOrderTableView()
            navigation.pushViewController(statusTrayVc, animated: false)
            present(navigation, animated: false)
        }

    @IBAction func HumanResources(_ sender: Any) {
        DeselectView()
        mSelect5Image.isHidden = false
        LeadIndicatorContainer.layer.backgroundColor = Colors.LowGray
        Constants.LoadStoryBoard(name: storyBoard.HumanResources, viewC: self)
    }

    @IBAction func createLeadButtonPressed(_ sender: Any) {
        DeselectView()
        mCreateLeadImage.isHidden = false
        MCreateLeadView.layer.backgroundColor = Colors.LowGray
        Constants.LoadStoryBoard(name: storyBoard.Crearlead, viewC: self)
    }

    @IBAction func IndicadoresButtonPressed(_ sender: Any) {
        DeselectView()
        mSelect1Image.isHidden = false
        RHContainer.layer.backgroundColor = Colors.LowGray
        Constants.LoadStoryBoard(name: storyBoard.Indicadores, viewC: self)
    }

    @IBAction func ComisionesAction(_ sender: Any) {
        Constants.LoadStoryBoard(name: storyBoard.Comissions.Welcome, viewC: self)
    }

    @IBAction func goReports(_ sender: Any) {
        DeselectView()
        mReportimage.isHidden = false
        mReportes.layer.backgroundColor = Colors.LowGray
        Constants.LoadStoryBoard(name: storyBoard.ReportsNew, viewC: self)
    }

    @IBAction func MisVentasAction(_ sender: Any) {
            Constants.LoadStoryBoard(name: storyBoard.MySell, viewC: self)
    }

    @IBAction func CanalEmpleadoButtonPressed(_ sender: Any) {
        DeselectView()
        mSelect2Image.isHidden = false
        ChanelEmploye.layer.backgroundColor = Colors.LowGray
        SwiftEventBus.post("OcultarMenu", sender: nil)
        // Constants.LoadStoryBoard(name: storyBoard.CanalEmpleado, viewC: self)
        let viewAlert: UIStoryboard = UIStoryboard(name: "CanalEmpleadoView", bundle: nil)
        let viewAlertVC = viewAlert.instantiateViewController(withIdentifier: "CanalEmpleadoView")as! CanalEmpleadoView
        viewAlertVC.providesPresentationContextTransitionStyle = true
        viewAlertVC.definesPresentationContext = true
        viewAlertVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        viewAlertVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(viewAlertVC, animated: false, completion: {})
    }

    @IBAction func MExitButton(_ sender: Any) {
        dismiss(animated: false, completion: nil)
        SwiftEventBus.post("LogOutInfoUpdate")
    }

    @IBAction func onChangeMapSwitch(_ sender: Any) {
        if mapSwitch.isOn {
            AppDelegate.Gmaps = true
        } else {
            AppDelegate.Gmaps = false
        }
    }

    @IBAction func dissmisAction(_ sender: Any) {
        SwiftEventBus.post("OcultarMenu", sender: nil)
    }

    func DeselectView() {
        mReportes.layer.backgroundColor = Colors.White
        ChanelEmploye.layer.backgroundColor = Colors.White
        RHContainer.layer.backgroundColor = Colors.White
        MCell3View.layer.backgroundColor = Colors.White
        MCell4View.layer.backgroundColor = Colors.White
        LeadIndicatorContainer.layer.backgroundColor = Colors.White
        MCreateLeadView.layer.backgroundColor = Colors.White
        digitalTrayView.layer.backgroundColor = Colors.White
        NewSales.layer.backgroundColor = Colors.White
        SellersListContainer.layer.backgroundColor = Colors.White
    }

    func configReportsView() {
        if isNewPerfil == false {
            // MCell6View.isHidden = true
        }
    }

    func getResponseInformation(identityToken: String, userId: String) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "NewSeller", bundle: Bundle(for: NewSellerView.self))
        let vC = storyBoard.instantiateViewController(withIdentifier: "NewSellerView") as! NewSellerView
        vC.identificationUser = userId
        vC.tokenIdentity = identityToken
        vC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vC.providesPresentationContextTransitionStyle = true
        vC.definesPresentationContext = true
        vC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        present(vC, animated: false)
    }

    @IBAction func openNewSeller(_ sender: Any) {
        //   DeselectView()
        //   self.NewSales.layer.backgroundColor = Colors.LowGray
//        getResponseInformation(identityToken: "", userId: "")
//        let storyBoard: UIStoryboard = UIStoryboard(name: "Identy", bundle: Bundle(for: IdentyController.self))
//        let vC  = storyBoard.instantiateViewController(withIdentifier: "IdentyController") as! IdentyController
        //  vC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        //  vC.providesPresentationContextTransitionStyle = true
        //  vC.definesPresentationContext = true
        //  vC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        //  vC.urlBase = .PROD
        //   vC.selectedFlow = .INESELFIE
        //  vC.delegate = self
        //  self.present(vC, animated: false)
    }

    @IBAction func openListNewSeller(_ sender: Any) {
        DeselectView()
        self.SellersListContainer.layer.backgroundColor = Colors.LowGray
        let viewAlert: UIStoryboard = UIStoryboard(name: "ListSellerView", bundle: nil)
        let viewVC = viewAlert.instantiateViewController(withIdentifier: "ListSellerController") as! ListSellerController
        viewVC.providesPresentationContextTransitionStyle = true
        viewVC.definesPresentationContext = true
        viewVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        viewVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(viewVC, animated: false)
    }
}
