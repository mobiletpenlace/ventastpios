//
//  MenuTableViewCell.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 09/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {
    @IBOutlet weak var mTitleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
