//
//  GenerateQR.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 23/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit

class GenerateQR: BaseVentasView {
    var currentBrightness: CGFloat!

    @IBOutlet weak var qrImageView: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        currentBrightness = UIScreen.main.brightness
        setBrightness(to: 1.0)
        let myString = "https://totalplay.dev/embajadores/paquetes?empleado=a0I3C00000189vlUAA&noempleado=" + faseComisiones.numEmpleado
        let data = myString.data(using: String.Encoding.ascii)

        guard let qrFilter = CIFilter(name: "CIQRCodeGenerator") else { return }
        qrFilter.setValue(data, forKey: "inputMessage")

        guard let qrImage = qrFilter.outputImage else { return }

        // Scale the image
        let transform = CGAffineTransform(scaleX: 10, y: 10)
        let scaledQrImage = qrImage.transformed(by: transform)

        // Invert the colors
        guard let colorInvertFilter = CIFilter(name: "CIColorInvert") else { return }
        colorInvertFilter.setValue(scaledQrImage, forKey: "inputImage")
        guard let outputInvertedImage = colorInvertFilter.outputImage else { return }

        // Replace the black with transparency
        guard let maskToAlphaFilter = CIFilter(name: "CIMaskToAlpha") else { return }
        maskToAlphaFilter.setValue(outputInvertedImage, forKey: "inputImage")
        guard let outputCIImage = maskToAlphaFilter.outputImage else { return }
        let context = CIContext()
        guard let cgImage = context.createCGImage(scaledQrImage, from: outputCIImage.extent) else { return }
        let processedImage = UIImage(cgImage: cgImage)

        // Set Image
        qrImageView.tintColor = UIColor.black
        qrImageView.image = processedImage
    }

    override func viewDidDisappear(_ animated: Bool) {
           setBrightness(to: currentBrightness)
    }

    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }

    public func setBrightness(to value: CGFloat, duration: TimeInterval = 0.5, ticksPerSecond: Double = 120) {
        let startingBrightness = UIScreen.main.brightness
        let delta = value - startingBrightness
        let totalTicks = Int(ticksPerSecond * duration)
        let changePerTick = delta / CGFloat(totalTicks)
        let delayBetweenTicks = 1 / ticksPerSecond
        let time = DispatchTime.now()

        for i in 1...totalTicks {
            DispatchQueue.main.asyncAfter(deadline: time + delayBetweenTicks * Double(i)) {
                UIScreen.main.brightness = max(min(startingBrightness + (changePerTick * CGFloat(i)), 1), 0)
            }
        }
    }

}
