//
//  AddressViewController.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin cabrera on 07/09/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTextControls_FilledTextAreas
import MaterialComponents.MaterialTextControls_FilledTextFields
import SwiftEventBus

class AddressViewController: BaseVentasView {

    private let streetKey = "streetTextFieldKey"
    private let numberKey = "numberTextFieldKey"
    private let colonyKey = "colonyTextFieldKey"
    private let zipCodeKey = "zipCodeTextFieldKey"
    private let plusCodeKey = "plusCodeTextFieldKey"
    private let cityKey = "cityTextFieldKey"
    private let stateKey = "stateTextFieldKey"
    private let selectedSegmentIndexKey = "selectedSegmentIndexKey"

    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var streetTextField: MDCFilledTextField! {
        didSet {
            streetTextField.normalTheme(type: 2)
        }
    }

    @IBOutlet weak var numberTextField: MDCFilledTextField! {
        didSet {
            numberTextField.normalTheme(type: 2)
        }
    }

    @IBOutlet weak var colonyTextField: MDCFilledTextField! {
        didSet {
            colonyTextField.normalTheme(type: 1)
        }
    }

    @IBOutlet weak var zipCodeTextField: MDCFilledTextField! {
        didSet {
            zipCodeTextField.normalTheme(type: 1)
        }
    }
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!{
        didSet {
            segmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.darkGray], for: .normal)

            segmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)
        }
    }

    @IBOutlet var AddressType: [MDCFilledTextField]!

    @IBOutlet var PlusCodeType: [MDCFilledTextField]!

    @IBOutlet weak var stackView: UIStackView!

    override func viewDidLoad() {
            super.viewDidLoad()
            super.setView(view: self.view)
            streetTextField.delegate = self
            streetTextField.becomeFirstResponder()
            segmentedControl.addTarget(self, action: #selector(segmentChanged), for: .valueChanged)
            loadTextFieldData()
            updateTextFieldVisibilityAndCursor()
        }


    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    func updateTextFieldVisibilityAndCursor() {
        self.view.frame = CGRectOffset(self.view.frame, 0, 0)
        PlusCodeType.forEach { $0.isHidden = true }
        AddressType.forEach { $0.isHidden = true }

        switch segmentedControl.selectedSegmentIndex {
        case 0:
            AddressType.forEach { $0.isHidden = false }
            AddressType.first?.becomeFirstResponder()

        default:
            PlusCodeType.forEach { $0.isHidden = false }
            PlusCodeType.first?.becomeFirstResponder()
        }
    }

    @objc func segmentChanged() {
        updateTextFieldVisibilityAndCursor()
        saveTextFieldData()
    }

    private func saveTextFieldData() {
        UserDefaults.standard.set(streetTextField.text, forKey: streetKey)
        UserDefaults.standard.set(numberTextField.text, forKey: numberKey)
        UserDefaults.standard.set(colonyTextField.text, forKey: colonyKey)
        UserDefaults.standard.set(zipCodeTextField.text, forKey: zipCodeKey)
        UserDefaults.standard.set(PlusCodeType[0].text, forKey: plusCodeKey)
        UserDefaults.standard.set(PlusCodeType[1].text, forKey: cityKey)
        UserDefaults.standard.set(PlusCodeType[2].text, forKey: stateKey)
        UserDefaults.standard.set(segmentedControl.selectedSegmentIndex, forKey: selectedSegmentIndexKey)
    }

    // Cargar valores de los UITextField desde UserDefaults
    private func loadTextFieldData() {
        streetTextField.text = UserDefaults.standard.string(forKey: streetKey)
        numberTextField.text = UserDefaults.standard.string(forKey: numberKey)
        colonyTextField.text = UserDefaults.standard.string(forKey: colonyKey)
        zipCodeTextField.text = UserDefaults.standard.string(forKey: zipCodeKey)
        PlusCodeType[0].text = UserDefaults.standard.string(forKey: plusCodeKey)
        PlusCodeType[1].text = UserDefaults.standard.string(forKey: cityKey)
        PlusCodeType[2].text = UserDefaults.standard.string(forKey: stateKey)

        // Cargar el valor del segmento seleccionado
        if let savedSegmentIndex = UserDefaults.standard.value(forKey: selectedSegmentIndexKey) as? Int {
            segmentedControl.selectedSegmentIndex = savedSegmentIndex
        }
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        if touch?.view != self.cardView {
            self.dismiss(animated: true, completion: nil)
        }
    }

    @IBAction func closeAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }

    @IBAction func continueAction(_ sender: Any) {
        let direccion = Direccion()
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            direccion.Calle = streetTextField.text ?? ""
            direccion.NumeroExterior = numberTextField.text ?? ""
            direccion.Colonia = colonyTextField.text ?? ""
            direccion.CodigoPostal = zipCodeTextField.text ?? ""
        default:
            direccion.plusCode = PlusCodeType[0].text ?? ""
            direccion.Ciudad = PlusCodeType[1].text ?? ""
            direccion.Estado = PlusCodeType[2].text ?? ""
        }
        SwiftEventBus.post("ReceiveDirection", sender: direccion )
        saveTextFieldData()
        self.dismiss(animated: false, completion: nil)
    }

    @IBAction func changedValue(_ sender: Any) {
        switch segmentedControl.selectedSegmentIndex {
         case 0:
             let isStreetFilled = !(streetTextField.text?.isEmpty ?? true)
             let isNumberFilled = !(numberTextField.text?.isEmpty ?? true)
             let isColonyOrZipCodeFilled = !(colonyTextField.text?.isEmpty ?? true) || !(zipCodeTextField.text?.isEmpty ?? true)
             continueButton.isEnabled = isStreetFilled && isNumberFilled && isColonyOrZipCodeFilled

         default:
             let isPlusCodeFilled = !(PlusCodeType[0].text?.isEmpty ?? true)
             let isCityFilled = !(PlusCodeType[1].text?.isEmpty ?? true)
             let isStateFilled = !(PlusCodeType[2].text?.isEmpty ?? true)
             continueButton.isEnabled = isPlusCodeFilled && isCityFilled && isStateFilled
         }    }
}
