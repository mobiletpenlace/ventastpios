//
//  LoginModel.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 17/02/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import RealmSwift
import Alamofire
import Amplitude
import AlamofireObjectMapper

import SwiftyJSON
import SwiftCoroutine

protocol LoginModelObserver {
    func succesLoginStreet()
    func succesLogin()
    func succesToken()
    func successLoginOkta(empleado: String)
    func succesEmploye()
    func sessionIsActive()
    func onError(errorMessage: String)
    func successInfoLoginUpdate()
    func successLeadsLogin()
}

class LoginModel: BaseModel {

    var viewModel: LoginModelObserver?
    var server: ServerDataManager2<LoginResponse>?
    var server2: ServerDataManager2<EmployeResponse>?
    var server3: ServerDataManager2<LoginAppResponse>?
    var server4: ServerDataManager2<LoginEmpleadoResponse>?
    var requestBase: LoginRequest!
    var requestStreetBase: LoginAppRequest!
    var urlOldLogin = AppDelegate.API_TOTAL + ApiDefinition.API_LOGIN
    var urlFFMLogin = AppDelegate.API_TOTAL + ApiDefinition.API_LOGIN_FFM
    var urlStreetLogin = AppDelegate.API_TOTAL + ApiDefinition.API_LOGIN_STREETTP
    var urlEmploye =  AppDelegate.API_SALESFORCE + ApiDefinition.API_EMPLEADO_PAIS
    var urlLoginInfo = AppDelegate.API_SALESFORCE + ApiDefinition.API_INICIO_EMPLEADO
    var urlOkta = ApiDefinition.API_OKTA

    var auxSW = ""
    var auxRequest = LoginRequest()
    var auxEmployeId = ""
    var successlogin = false
    let userTypeLogin = UserDefaults.standard

    init(observer: BaseViewModelObserver) {
        super.init(observerVM: observer)

        // Se declara un server por tipo de response
        server = ServerDataManager2<LoginResponse>()
        server2 = ServerDataManager2<EmployeResponse>()
        server3 = ServerDataManager2<LoginAppResponse>()
        server4 = ServerDataManager2<LoginEmpleadoResponse>()
    }

    func atachModel(viewModel: LoginModelObserver) {
        self.viewModel = viewModel
    }

    // Login con atributos desencriptados Middle
    func doLogin(request: LoginRequest) {

        urlFFMLogin = AppDelegate.API_TOTAL + ApiDefinition.API_LOGIN_FFM
        TPCipher.shared.setKey(b64Key: "VG90YWxwbGF5TW9iaWxlU2VjdXJpdHlMYXllcjIwMTk=")
//        TPCipher.shared.setKey(b64Key: "LZn293KxL/uFtz71MwBWxQ==")
//        TPCipher.shared.decode(coded: request.userId!)
//        print(TPCipher.shared.decode(coded: request.userId!))
        request.secretWord = TPCipher.shared.code(plainText: request.secretWord!)
        request.userId =  TPCipher.shared.code(plainText: request.userId!)
        request.VersionAPP = "3.0.0.5"
        request.fcmToken = " "
        request.ip = "10.10.10.10"
        request.systemId = "1"
        self.requestBase = request
        server?.setValues(requestUrl: urlFFMLogin, delegate: self, headerType: .headersMiddle)
        server?.request(requestModel: request)
    }

    func doOktaLogin(token: String) {
        let request = AlfaRequest()
        ApiDefinition.headersOkta["Authorization"]! += token
        print("AUTHORIZATION ACTUALIZADO: \(ApiDefinition.headersOkta)")
        server?.setValues(requestUrl: urlOkta, delegate: self, headerType: .headersOkta)
        server?.request(requestModel: request)
    }

    // Login con atributos encriptados para FFM
    func doOldLogin(request: LoginRequest) {
        urlOldLogin = AppDelegate.API_TOTAL + ApiDefinition.API_LOGIN
        TPCipher.shared.setKey(b64Key: "VG90YWxwbGF5TW9iaWxlU2VjdXJpdHlMYXllcjIwMTk=")
//        request.secretWord = TPCipher.shared.decode(coded: request.secretWord!)
//        request.userId =  TPCipher.shared.decode(coded: request.userId!)
        self.requestBase = request
        server?.setValues(requestUrl: urlOldLogin, delegate: self, headerType: .headersMiddle)
        server?.request(requestModel: request)
    }

    // Login con atributos encriptados para StreetTP
    func doStreetLogin(request: LoginRequest) {
        urlStreetLogin = AppDelegate.API_TOTAL + ApiDefinition.API_LOGIN_STREETTP
        let loginAppRequest = LoginAppRequest()
        TPCipher.shared.setKey(b64Key: Strings.SHA256_KEY)
        loginAppRequest.idDevice = TPCipher.shared.code(plainText: UIDevice.id)
        loginAppRequest.idUser = request.userId// TPCipher.shared.code(plainText: request.userId!)
        loginAppRequest.passwordUser = request.secretWord// TPCipher.shared.code(plainText: request.secretWord!)
        loginAppRequest.Mlogin = LoginApp()
        loginAppRequest.Mlogin?.Ip = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_IP) ?? ""
        loginAppRequest.Mlogin?.input = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_IP) ?? ""
        loginAppRequest.Mlogin?.UserId =  TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_USER) ?? ""
        loginAppRequest.Mlogin?.Password = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_SECRETWORD) ?? ""
        self.requestBase = request
        server3?.setValues(requestUrl: urlStreetLogin, delegate: self, headerType: .headersMiddle)
        server3?.request(requestModel: loginAppRequest)
    }

    // Consulta empleado API Salesforce
    func consultaEmpleado(request: EmployeRequest) {
        urlEmploye = AppDelegate.API_SALESFORCE + ApiDefinition.API_EMPLEADO_PAIS
        server2?.setValues(requestUrl: urlEmploye, delegate: self, headerType: .headersSalesforce)
        server2?.request(requestModel: request)
    }

    func updateInfoLogin(request: LoginEmpleadoRequest) {
        urlLoginInfo = AppDelegate.API_SALESFORCE + ApiDefinition.API_INICIO_EMPLEADO
        server4?.setValues(requestUrl: urlLoginInfo, delegate: self, headerType: .headersSalesforce)
        server4?.request(requestModel: request)
    }

    // Obtencion del token Bearer de acceso a Salesforce
    func token() {
//        auxSW = request.secretWord ?? ""
        successlogin = true
        server?.getToken {(res, _) in
            if res {
                self.viewModel?.succesToken()
            } else {
                self.viewModel?.onError(errorMessage: "Error al consumir el token")
            }
        }
    }

    func tokenFirst(request: LoginRequest) {
        auxSW = request.secretWord ?? ""
        auxRequest = request
        print("RequestSECRET: \(request.secretWord) auxRequestID: \(request.userId)")
        print("auxRequestSECRET: \(auxRequest.secretWord) auxRequestID: \(auxRequest.userId)")
        server?.getToken {(res, _) in
            if res {
                self.viewModel?.succesToken()
            } else {
                self.viewModel?.onError(errorMessage: "Error al consumir el token")
            }
        }
    }

    // funcion de multiples proceso de la informacion del empleado
    func saveEmployeDB(employeResponse: EmployeResponse) {
        let canales: List<CanalesDB> = List<CanalesDB>()

        AES128.shared.setKey(key: statics.key)
        let json: String =  AES128.shared.decode(coded: employeResponse.empleado!)!
        var employe: Employe = Employe(JSONString: json)!
        let empleado = Empleado()
        var isFirst = true
        
        // MARK: Proceso de guardado de usuario loggeado previamente
        userTypeLogin.set(employe.canal, forKey: LoginEmpleadoType.canalEmpleado)
        userTypeLogin.set(employe.subcanal, forKey: LoginEmpleadoType.subCanalEmpleado)
        // proceso de estructuracion de los canales
        for c in employe.canales {
            var canal: CanalesDB = CanalesDB()
            canal.nombre = c.nombre
            for s in c.subCanales {
                canal.Subcanales.append(s.nombre)
            }
            canales.append(canal)
        }

        if let puntoDeVentaCanal = canales.first(where: { $0.nombre == "Punto de Venta" }) {
            empleado.CanalSelect = "Punto de Venta"
            empleado.SubCanalSelect = " "
        } else {
            if let lastCanal = canales.last {
                empleado.CanalSelect = lastCanal.nombre
                if lastCanal.Subcanales.count > 0 {
                    empleado.SubCanalSelect = lastCanal.Subcanales[0]
                }
            }
        }

        print("CANAL SELECTED: \(empleado.CanalSelect)")
        print("SUBCANAL SELECTED: \(empleado.SubCanalSelect)")

        // llenado de objeto realm Empleado
        print("%%%%%%% empleado ID: \(employe.folioEmpleado) contraseña: \(Security.decrypt(text: employe.contrasena ))")
        let secretWord = Security.decrypt(text: employe.contrasena)
        auxEmployeId = employe.idEmpleado
        print("EMPLOYEIDDD = \(auxEmployeId)")
        if employe.contrasena == "" && successlogin == false {
            print("SAVEEMPLOYESECRETWORD: \(auxRequest.secretWord)")
            self.doOldLogin(request: auxRequest)
        } else {
            if auxSW != secretWord  && successlogin == false {
//                doOldLogin(request: auxRequest)
//                auxEmployeId = empleado.Id
                viewModel?.onError(errorMessage: "contrasena")
            } else {
                empleado.Id = employe.idEmpleado
                UserDefinitions.idEmployee = employe.idEmpleado
                empleado.Nombre = employe.nombreEmpleado
                empleado.Canal = employe.canal
                empleado.subCanal = employe.subcanal
                empleado.Canales = canales
                empleado.Distrital = employe.distritalId
                empleado.DistritalName = employe.distritalName
                empleado.Estatus = employe.estatus
                empleado.FolioEmpleado = employe.folioEmpleado
                empleado.IdExterno = employe.idExterno
                empleado.Posicion = employe.posicion
                empleado.EmpleadoDistrital = employe.empleadoDistritalId
                empleado.EmpleadoDistritalName = employe.empleadoDistritalName
                empleado.FolioEmpleadoLogueado = employe.folioEmpleado
                empleado.NoEmpleado = employe.folioEmpleado
                empleado.numberLogin = employe.folioEmpleado
                empleado.idDevice = employe.idDevice
                empleado.isSessionActive = employe.isSessionActive
                empleado.SubCanalSelect = employe.subcanal
                // guardado o actualizado
                if RealManager.findFirst(object: Empleado.self) != nil {
                   RealManager.deleteAll(object: Empleado.self)
                    _ = RealManager.insert(object: empleado)
                } else {
                    _ = RealManager.insert(object: empleado)
                }

                if employe.pais != nil {
                    if employe.pais == "CO" {
                        server?.getTokenCol {(res, _) in
                            if res {
                                self.viewModel?.succesToken()
                            } else {
                                self.viewModel?.onError(errorMessage: "Error al consumir el token")
                                self.viewModel?.onError(errorMessage: "Error al consumir el token")
                            }
                        }
                    }
                }
                if empleado.NoEmpleado == "65853437" || empleado.NoEmpleado == "41" || empleado.NoEmpleado == "17005246" || empleado.NoEmpleado == "65713538" {
                    self.viewModel?.succesEmploye()
                } else {
                    if empleado.isSessionActive == true {
                        if empleado.idDevice == UIDevice.id {
                            self.viewModel?.succesEmploye()
                        } else {
                            self.viewModel?.sessionIsActive()
                        }
                    } else {
                        self.viewModel?.succesEmploye()
                    }
                }
                
                // Set userId
                Amplitude.instance().setUserId(empleado.NoEmpleado)
                let identify = AMPIdentify()
                    .set("employeeCh", value: NSString(string: empleado.Canal))
                    .set("employeeName", value: NSString(string: empleado.Nombre))
                    .set("employeeNumber", value: NSString(string: empleado.NoEmpleado))
                    .set("environmentSw", value: "release" as NSObject)
                Amplitude.instance().identify(identify!)
            }
        }
    }

    // Se procesa segun el url de consulta la respuesta en cada caso
    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if requestUrl == urlFFMLogin {
            let loginResponse = response as! LoginResponse
            if loginResponse.Result == "0" {
                saveTypeUser(type: "FFM")
                // Update secretword SF
                updateSecretWord(id: auxEmployeId, secretWord: auxSW)
                self.token()
            } else if loginResponse.Result == "1" {
                doStreetLogin(request: auxRequest)
            }
        } else if requestUrl == urlStreetLogin {
            let loginResponse = response as! LoginAppResponse
            if loginResponse.MResult?.Result == "0" {
                guard let profile = TPCipher.shared.decode(coded: loginResponse.profileUser!) else { return}
                if profile != "1" && profile != "2" && profile != "3" {
                    viewModel?.onError(errorMessage: "No existe el perfil para este usuario")
                } else {
                    saveTypeUser(type: "STREET")
                    saveEmployeeStreetTP(response: loginResponse)
                   // FirebaseStorage.authUser(customToken: getEmployeStreet()?.tokenFirebase ?? "")
                    viewModel?.succesLoginStreet()
                   // self.token()
                }

            } else if loginResponse.MResult?.Result == "1" {
                // doOldLogin(request: self.requestBase)
                viewModel?.onError(errorMessage: "contrasena")
            }
        } else if requestUrl == urlOldLogin {
            let loginResponse = response as! LoginResponse

            // TODO: QA SW
//            if loginResponse.Result == "0" {
//            if AppDelegate.API_TOTAL.contains(string: "mssqa") || AppDelegate.API_TOTAL.contains(string: "dssc"){
//                saveTypeUser(type: "VENTAS")
//                viewModel?.succesLogin()
//                self.token()
//                //            } else if loginResponse.Result == "1" {
//            }else {
                if loginResponse.Result == "0" {
                    saveTypeUser(type: "VENTAS")
                    // Update secretword SF
                    updateSecretWord(id: auxEmployeId, secretWord: auxSW)
                    viewModel?.succesLogin()
                    self.token()
                } else if loginResponse.Result == "1" {
                    doLogin(request: auxRequest)
//                    viewModel?.onError(errorMessage: loginResponse.ResultDescription ?? "Favor de validar de nuevo")
                }
//                viewModel?.onError(errorMessage: loginResponse.ResultDescription ?? "Favor de validar de nuevo")
//            }
        } else if requestUrl == urlEmploye {
            let employeResponse = response as! EmployeResponse

            if employeResponse.result == "0" {
                let employeJson = AES128.shared.decode(coded: employeResponse.empleado!)!
                let employed = Employe(JSONString: employeJson)!
                
                if employed.canal.lowercased() == ApiDefinition.LEADS_CANAL && employed.subcanal.lowercased() == ApiDefinition.LEADS_SUBCANAL {
                    viewModel?.successLeadsLogin()
                } else {
                    self.saveEmployeDB(employeResponse: employeResponse)
                }
            } else {
                doOldLogin(request: auxRequest)
//                viewModel?.onError(errorMessage: employeResponse.resultDescription ?? "Favor de validar de nuevo")
            }
        } else if requestUrl == urlLoginInfo {
            let loginEmpleadoResponse = response as! LoginEmpleadoResponse
            if loginEmpleadoResponse.result == "0" {
                viewModel?.successInfoLoginUpdate()
            } else {
                viewModel?.onError(errorMessage: loginEmpleadoResponse.resultDescription ?? "Error en el servicio de urlLoginInfo")
            }
        } else if requestUrl == urlOkta {
//            server?.getToken(){(res,token) in
//                if(res){
//                    //Success TOken
//                    let oktaResponse = response as! OktaUserResponse
//                    print("Num EMP: \(oktaResponse.preferredUsername ?? "EMPTY NAME")")
//                    self.viewModel?.successLoginOkta(empleado: oktaResponse.preferredUsername ?? "")
//                }
//                else{
//                    self.viewModel?.onError(errorMessage: "Error al consumir el token")
//                }
//            }
            let oktaResponse = response as! OktaUserResponse
            print("Num EMP: \(oktaResponse.preferredUsername ?? "EMPTY NAME")")
            self.viewModel?.successLoginOkta(empleado: oktaResponse.preferredUsername ?? "")
        }
    }

    func updateSecretWord(id: String, secretWord: String) {

        let cryptedSecretWord = Security.crypt(text: secretWord)

        let infoParam: Parameters = [
            "Id": id,
            "Contrasena": cryptedSecretWord
        ]

        let parameters: Parameters = [
            "accion": "Actualizar",
            "info": infoParam
        ]
        if ConnectionUtils.isConnectedToNetwork() {
            // Connection is active
            Alamofire.request(AppDelegate.API_SALESFORCE + ApiDefinition.API_INFO_EMPLEADO, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: ApiDefinition.headersToken).responseJSON {[weak self] response in
                guard let self = self else {return}
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print("JSONCAMBIOCONTRASEÑA: \(json)")
                case .failure(let error):
                    print(error)
                }
            }
        } else {
            // Conection error
        }
    }

    override func onErrorLoadResponse(requestUrl: String, messageError: String) {
        viewModel?.onError(errorMessage: messageError)
    }

    func saveEmployeeStreetTP(response: LoginAppResponse) {
        guard let id = TPCipher.shared.decode(coded: response.idUser!) else { return}
        guard let profile = TPCipher.shared.decode(coded: response.profileUser!) else { return}
        guard let tokenF = TPCipher.shared.decode(coded: response.tokenFB!) else { return}
        guard let numEmployee = TPCipher.shared.decode(coded: self.requestBase.userId ?? "") else { return}
        RealManager.deleteAll(object: EmployeeStreet.self)
        RealManager.deleteAll(object: Empleado.self)
        let empleado = Empleado()
        empleado.NoEmpleado = numEmployee
        empleado.Id = id
        empleado.Nombre = ""
        let user = EmployeeStreet()
        user.userId = id
        user.profileUser = profile
        user.tokenFirebase = tokenF
        user.numEmployee = numEmployee
        RealManager.insert(object: user)
        RealManager.insert(object: empleado)
        Amplitude.instance().setUserId(empleado.NoEmpleado)
        let identify = AMPIdentify()
            .set("employeeCh", value: empleado.Canal as NSObject)
            .set("employeeName", value: empleado.Nombre as NSObject)
            .set("employeeNumber", value: empleado.NoEmpleado as NSObject)
            .set("environmentSw", value: "release" as NSObject)
        Amplitude.instance().identify(identify!)
    }

    func getEmployeStreet() -> EmployeeStreet? {
        let employee = RealManager.findFirst(object: EmployeeStreet.self)
        return employee
    }

    func saveTypeUser(type: String) {
        let defaults = UserDefaults.standard
        defaults.set(type, forKey: "typeUser")

    }

}
