//
//  LoginViewModel.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 17/02/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//
import UIKit
import Foundation
import SwiftEventBus

protocol LoginObserver {
    func succesLoginStreetTP()
    func succesLogin()
    func succesToken()
    func succesEmploye()
    func sessionIsActive()
    func successLoginOkta(empleado: String)
    func successInfoLoginUpdate()
    func successLeadsLogin()
    func onError(errorMessage: String)
}

class LoginViewModel: BaseViewModel, LoginModelObserver {

    var observer: LoginObserver?
    var model: LoginModel!

    override init(view: BaseVentasView) {
        super.init(view: view)
        model = LoginModel(observer: self)
        model.atachModel(viewModel: self)
    }

    func atachView(observer: LoginObserver) {
        self.observer = observer

        SwiftEventBus.onMainThread(self, name: "LogOutInfoUpdate") { [weak self] _ in
                self?.updateInfoLogin(logout: false)
        }
    }

    func login(request: LoginRequest) {
        view.showLoading(message: "")
        model.tokenFirst(request: request)
//        model.doLogin(request: request)
    }

    func loginOkta(token: String) {
        model.doOktaLogin(token: token)
    }

    func updateInfoLogin(logout: Bool) {

        let request = LoginEmpleadoRequest()
        let infoEmpleado: InfoEmpleado = InfoEmpleado()
        request.InfoEmpleado = InfoEmpleado()
        request.InfoEmpleado?.idDevice = UIDevice.id
        request.InfoEmpleado?.isSessionActive = logout
        request.InfoEmpleado?.idEmpleado = UserDefinitions.idEmployee
        request.InfoEmpleado?.tipo = "actualiza"
        model.updateInfoLogin(request: request)
    }

    func consultaEmpleado(empleado: String) {
        let request: EmployeRequest = EmployeRequest()
        AES128.shared.setKey(key: statics.key)
        request.folio = AES128.shared.code(plainText: empleado)
        model.consultaEmpleado(request: request)
    }

    func succesLogin() {
        view.hideLoading()
        observer?.succesLogin()
    }

    func successLoginOkta(empleado: String) {
        view.hideLoading()
        observer?.successLoginOkta(empleado: empleado)
    }

    func succesLoginStreet() {
        view.hideLoading()
        observer?.succesLoginStreetTP()
    }

    func succesToken() {
        observer?.succesToken()
    }

    func sessionIsActive() {
        view.hideLoading()
        observer?.sessionIsActive()
    }
    func successInfoLoginUpdate() {
        view.hideLoading()
        observer?.successInfoLoginUpdate()
    }

    func succesEmploye() {
        view.hideLoading()
        observer?.succesEmploye()
    }

    func onError(errorMessage: String) {
        view.hideLoading()
        observer?.onError(errorMessage: errorMessage)
    }
    
    func successLeadsLogin() {
        view.hideLoading()
        observer?.successLeadsLogin()
    }
}
