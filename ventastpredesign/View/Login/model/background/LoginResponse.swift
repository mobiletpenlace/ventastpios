//
//  LoginResponse.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 04/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class LoginResponse: BaseResponse {

    required init?(map: Map) {

    }

    override func mapping(map: Map) {
        IdResult <- map["IdResult"]
        Result <- map["Result"]
        ResultDescription <- map["ResultDescription"]
    }

    var IdResult: String?
    var Result: String?
    var ResultDescription: String?

}
