//
//  AppVersionResponse.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 06/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class AppVersionResponse: BaseResponse {
    required init?(map: Map) {
    }

    override func mapping(map: Map) {
        version <- map["version"]
    }

    var version: String?
}
