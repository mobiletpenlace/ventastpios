//
//  LoginRequest.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 04/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

public class LoginRequest: BaseRequest {
    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override public func mapping(map: Map) {
        VersionAPP <- map["VersionAPP"]
        fcmToken <- map["fcmToken"]
        ip <- map["ip"]
        secretWord <- map["password"]
        systemId <- map["systemId"]
        userId <- map["userId"]
    }

    var VersionAPP: String?
    var fcmToken: String?
    var ip: String?
    var secretWord: String?
    var systemId: String?
    var userId: String?
}
