//
//  LoginVC.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 16/12/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import UIKit
import LocalAuthentication
import MaterialComponents.MaterialTextControls_FilledTextAreas
import MaterialComponents.MaterialTextControls_FilledTextFields
// import OktaOidc
// import OktaJWT

class LoginVC: BaseVentasView {

    @IBOutlet var viewOktaLogin: UIView!
    @IBOutlet var viewContainer: UIView!
    @IBOutlet weak var viewDataLogin: UIView!
    @IBOutlet weak var MUserTextField: MDCFilledTextField!
    @IBOutlet weak var MSecretWordTextField: MDCFilledTextField!
    @IBOutlet weak var MVersionLabel: UILabel!
    @IBOutlet weak var MViewButton: UIButton!
    @IBOutlet weak var MSignButton: UIButton!
    @IBOutlet weak var MViewOptionQA: UIView!
    @IBOutlet weak var mImageSelectQA: UIImageView!
    @IBOutlet weak var MButtonQA: UIButton!
    @IBOutlet weak var topLogoImageView: UIImageView!

    var secretWordInvisible: Bool!
    var checkQA = false
    var debug = false
    var embajador = false
    var isQa = false
    var numEmployed = ""
    var mLoginViewModel: LoginViewModel!
    var canalActual: String = String.emptyString
    var subCanalActual: String = String.emptyString
//    var stateManager: OktaOidcStateManager?
//    var oktaOidc: OktaOidc?
//    var stateManager: OktaOidcStateManager?
//    var oktaOidc: OktaOidc?

    override func viewWillAppear(_ animated: Bool) {
        if UserDefaults.standard.bool(forKey: "isAutoentrepreneur") {
            topLogoImageView.image = UIImage(named: "autoentrepreneurs_logo")
        }
        super.viewWillAppear(animated)
        viewOktaLogin.isHidden = true
        viewDataLogin.isHidden = false
        viewContainer.addSubview(viewOktaLogin)
        viewOktaLogin.translatesAutoresizingMaskIntoConstraints = false
            let horizontalConstraint = NSLayoutConstraint(item: viewOktaLogin, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0)
            let verticalConstraint = NSLayoutConstraint(item: viewOktaLogin, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: 0)
        NSLayoutConstraint.activate([horizontalConstraint, verticalConstraint])
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        MUserTextField.text = "" // TEMP
        MSecretWordTextField.text = "" // TEMP
        design()
        secretWordInvisible = true
        mLoginViewModel = LoginViewModel(view: self)
        mLoginViewModel.atachView(observer: self)
        configTextFields()
        // mAppVersionPresenter.GetAppVersion(mAppVersionDelegate: self)
        validateCurrentUserType()
        
        if UserDefaults.standard.object(forKey: "mUser") != nil {
            IDTouch()
        }
        if isQa {
            toPruebas()
            if debug {
                if embajador {
                    MSecretWordTextField.text = "super1234567"
                    MUserTextField.text = "3412345"
//                    MUserTextField.text = "41"
//                    MSecretWordTextField.text = "TPventas#01"
//                    MUserTextField.text = "150086173"
//                    MSecretWordTextField.text = "qwe1asd2zxc3"
                } else {
                    MUserTextField.text = "65853437"
                    MSecretWordTextField.text = "Ce65853437$"
                }
            }
        } else {
            toProd()
            if debug {
                if embajador {
                 MUserTextField.text = "65007084"
                 MSecretWordTextField.text = "65007084"
                } else {
                    MUserTextField.text = "65853437"
                    MSecretWordTextField.text = "Ce65853437$"
                }
            }
        }
    }

    func prueba() {

//        AppDelegate.API_SALESFORCE = "https://totalplay--Pruebas.my.salesforce.com/services/"
        AppDelegate.API_SALESFORCE = "https://totalplay--pruebas.sandbox.my.salesforce.com/services/"

        AppDelegate.API_TOTAL = "https://mssqa.totalplay.com.mx"
        // AppDelegate.API_TOTAL = "https://dssc-dev.dralmey.com.uy"
        AppDelegate.GRANT = "grant_type=password&client_id=3MVG9oZtFCVWuSwOvwwAVjUooDcuXh_jhkAkE5gY5U9gj92CtWtoPdYLlfQCdIrdWoWjg7puk0yOiu5B.srv.&client_secret=47A8074CEBF6718C127C4726B0CB2D001605D79E82C16A5E839CBEAAF8A9A863&username=appresidencial@totalplay.com.mx.pruebas&password=Totalplay123456TmXRrPxqwft5MLVGw4X7cNVW"
    }

    func errorTheme(textField: MDCFilledTextField) {
        textField.setTextColor(UIColor.red, for: .normal)
        textField.setNormalLabelColor(UIColor.red, for: .normal)
        textField.setFloatingLabelColor(UIColor.red, for: .normal)
        textField.setLeadingAssistiveLabelColor(UIColor.red, for: .normal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func MLoginButton(_ sender: Any) {
        login()
    }

    override func viewDidAppear(_ animated: Bool) {
        if ApiDefinition.PRODUCTION {
            MViewOptionQA.isHidden = true
        } else {
            MViewOptionQA.isHidden = false
            mImageSelectQA.image = UIImage(named: "no.png")
            // toPruebas()
            AppDelegate.mServiceStatus = false
        }
        AppDelegate.mServiceStatus = false
        if let x = UserDefaults.standard.object(forKey: "mUser") as? String {
            MUserTextField.text = x
        }
        initTouchID()
        // prueba()
    }

    func initTouchID() {
        if UserDefaults.standard.object(forKey: "mUser") != nil {
            AuthenticateUserTouchID.touchFaceID(vc: self) { _ in
                if let password = UserDefaults.standard.object(forKey: "password") as? String {
                    self.MSecretWordTextField.text = Security.decrypt(text: password)
                    self.login()
                }
            }
        }
    }

    func savingUserPassword() {
        let defaults = UserDefaults.standard
        defaults.set(MUserTextField.text, forKey: "mUser")
        defaults.set(Security.crypt(text: MSecretWordTextField.text!), forKey: "password")
    }

    func generateQRCode() {
        // import UIKit
        // Get define string to encode
        let myString = "https://pennlabs.org"
        // Get data from the string
        let data = myString.data(using: String.Encoding.ascii)
        // Get a QR CIFilter
        guard let qrFilter = CIFilter(name: "CIQRCodeGenerator") else { return }
        // Input the data
        qrFilter.setValue(data, forKey: "inputMessage")
        // Get the output image
        guard let qrImage = qrFilter.outputImage else { return }
        // Scale the image
        let transform = CGAffineTransform(scaleX: 10, y: 10)
        let scaledQrImage = qrImage.transformed(by: transform)
        // Invert the colors
        guard let colorInvertFilter = CIFilter(name: "CIColorInvert") else { return }
        colorInvertFilter.setValue(scaledQrImage, forKey: "inputImage")
        guard let outputInvertedImage = colorInvertFilter.outputImage else { return }
        // Replace the black with transparency
        guard let maskToAlphaFilter = CIFilter(name: "CIMaskToAlpha") else { return }
        maskToAlphaFilter.setValue(outputInvertedImage, forKey: "inputImage")
        guard let outputCIImage = maskToAlphaFilter.outputImage else { return }
        // Do some processing to get the UIImage
        let context = CIContext()
        guard let cgImage = context.createCGImage(outputCIImage, from: outputCIImage.extent) else { return }
        let processedImage = UIImage(cgImage: cgImage)
    }

    @IBAction func iconAction(sender: AnyObject) {

        generateQRCode()

        if secretWordInvisible {
            MViewButton.setImage(UIImage(named: "Icon_eye"), for: .normal)
            MSecretWordTextField.isSecureTextEntry = false
        } else {
            MViewButton.setImage(UIImage(named: "icon_eye_bloqued"), for: .normal)
            MSecretWordTextField.isSecureTextEntry = true
        }
        secretWordInvisible = !secretWordInvisible
    }

    func IDTouch() {
        let context: LAContext = LAContext()
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil) {
            var localizedReason = "Por favor verifíquese"
            if #available(iOS 11.0, *) {
                switch context.biometryType {
                case .faceID: localizedReason = "Desbloquear usando Face ID"
                case .touchID: localizedReason = "Desbloquear usando Touch ID"
                case .opticID: localizedReason = ""
                case .none: print("Sin soporte biométrico")
                }
            } else {}
            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: localizedReason, reply: { (wasSuccesful, _) -> Void in
                if wasSuccesful {
                    DispatchQueue.main.sync {
                        if let y = UserDefaults.standard.object(forKey: "mSecretWord") as? String {
                            self.MSecretWordTextField.text = Security.decrypt(text: y)
                            self.login()
                        }
                    }
                }
            })
        }
    }

    @IBAction func MButtonActionQA(_ sender: Any) {
        if AppDelegate.mServiceStatus {
            mImageSelectQA.image = UIImage(named: "no.png")
            toProd()
        } else {
            mImageSelectQA.image = UIImage(named: "ok.png")
            toPruebas()
//            mImageSelectQA.image = UIImage(named: "ok.png")
//            AppDelegate.API_SALESFORCE = "https://totalplay--qa.my.salesforce.com/services/"
//            AppDelegate.API_TOTAL = "https://msstest.totalplay.com.mx"
//            AppDelegate.GRANT = "grant_type=password&client_id=3MVG9KI2HHAq33RxdFES5KXE7Sz8lu.FC0.Lk53x64FHLI9V1B11UAhBcz8bX584F0ugq1aNlMNMKvWS2Tung&client_secret=8884931604809527571&username=acerone@totalplay.com.mx.qa&password=totalplay123456"
        }
        AppDelegate.mServiceStatus = !AppDelegate.mServiceStatus
        print(AppDelegate.API_TOTAL)
    }

    @IBAction func userAction(_ sender: Any) {
        MSecretWordTextField.becomeFirstResponder()
    }

    @IBAction func keyboardAction(_ sender: Any) {
        login()
    }

    @IBAction func NoOktaLoginAction(_ sender: Any) {
        self.viewOktaLogin.isHidden = true
        self.viewDataLogin.isHidden = false
    }

    func toPruebas() {
//        AppDelegate.API_SALESFORCE = "https://totalplay--Pruebas.my.salesforce.com/services/"
        AppDelegate.API_SALESFORCE = "https://totalplay--pruebas.sandbox.my.salesforce.com/services/"
        AppDelegate.API_TOTAL = "https://mssqa.totalplay.com.mx"
//        AppDelegate.API_TOTAL = "https://dssc-dev.dralmey.com.uy"
        AppDelegate.GRANT = "grant_type=password&client_id=3MVG9oZtFCVWuSwOvwwAVjUooDcuXh_jhkAkE5gY5U9gj92CtWtoPdYLlfQCdIrdWoWjg7puk0yOiu5B.srv.&client_secret=47A8074CEBF6718C127C4726B0CB2D001605D79E82C16A5E839CBEAAF8A9A863&username=appresidencial@totalplay.com.mx.pruebas&password=Totalplay123456TmXRrPxqwft5MLVGw4X7cNVW"
    }

    func toProd() {
        AppDelegate.API_SALESFORCE = "https://totalplay.my.salesforce.com/services/"
        AppDelegate.API_TOTAL = "https://mss.totalplay.com.mx"
        AppDelegate.GRANT =  "grant_type=password&client_id=3MVG9KI2HHAq33RxdFES5KXE7S33bVP0UfROooHlOdvmRoaGjvZuU3SkV_HL2JwVF.DEzXLnq39uKPYUbhIWM&client_secret=975959C56FCF9F6A916A39D172E7E519933FECA6B1A78A3EC8B4078D26C80887&username=appresidencial@totalplay.com.mx&password=totalplay1234564up20pvZSyYibYOeEPFDNaRYJ"
    }

    func configTextFields() {
        MUserTextField.setFilledBackgroundColor(UIColor.clear, for: .normal)
        MUserTextField.setFilledBackgroundColor(UIColor.clear, for: .disabled)
        MUserTextField.setFilledBackgroundColor(UIColor.clear, for: .editing)

        MUserTextField.setTextColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .normal)
        MUserTextField.setTextColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .editing)

        MUserTextField.setNormalLabelColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .normal)
        MUserTextField.setNormalLabelColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .editing)

        MUserTextField.setFloatingLabelColor(UIColor(named: "Base_rede_ligth_gray_titles")!, for: .normal)
        MUserTextField.setFloatingLabelColor(UIColor(named: "Base_rede_ligth_gray_titles")!, for: .editing)

        MSecretWordTextField.setTextColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .normal)
        MSecretWordTextField.setTextColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .editing)

        MSecretWordTextField.setNormalLabelColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .normal)
        MSecretWordTextField.setNormalLabelColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .editing)

        MSecretWordTextField.setFloatingLabelColor(UIColor(named: "Base_rede_ligth_gray_titles")!, for: .normal)
        MSecretWordTextField.setFloatingLabelColor(UIColor(named: "Base_rede_ligth_gray_titles")!, for: .editing)

        MSecretWordTextField.setFilledBackgroundColor(UIColor.clear, for: .normal)
        MSecretWordTextField.setFilledBackgroundColor(UIColor.clear, for: .disabled)
        MSecretWordTextField.setFilledBackgroundColor(UIColor.clear, for: .editing)
    }

    func login() {
        if MUserTextField.text == "" /*|| MSecretWordTextField.text == ""*/{
//            Constants.Alert(title: "", body: dialogs.Error.FillFields, type: type.Error, viewC: self)
        } else {
            var request = LoginRequest()
            request.userId = MUserTextField.text
            request.secretWord = MSecretWordTextField.text
            request.VersionAPP = (Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String)
            mLoginViewModel.login(request: request)
        }
    }

    func design() {
        MUserTextField.delegate =  self
        MSecretWordTextField.delegate =  self
        MUserTextField.tag = 0
        MSecretWordTextField.tag = 1
        MUserTextField.returnKeyType = .next
        MSecretWordTextField.returnKeyType = .done
        let shortBundleVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        let bundleVersion = Bundle.main.infoDictionary?["CFBundleVersion"] as? String
        MVersionLabel.text = "Versión: \(shortBundleVersion ?? "3.").\(bundleVersion ?? "0.0" )"
        Constants.Border(view: MSignButton, radius: 6, width: 0)
        Constants.Shadow(color: Colors.Gray, opacity: 0.5, offSet: CGSize(width: 5, height: 0), radius: 6, view: MSignButton)
    }

    func validateCurrentUserType() {
        guard let canalEmpleadoActual = UserDefaults.standard.object(forKey: LoginEmpleadoType.canalEmpleado) as? String else { return }
        guard let subCanalEmpleadoActual = UserDefaults.standard.object(forKey: LoginEmpleadoType.subCanalEmpleado) as? String else { return }
        
        if (canalEmpleadoActual == "Call Center") && (subCanalEmpleadoActual == "Ejecutivos EKT") {
            self.topLogoImageView.image = UIImage(named: "AppLeads_logo")
        }
    }
}
