//
//  EmployeeStreet.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 09/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class EmployeeStreet: Object {
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  userId: String = ""
    @objc dynamic var  numEmployee: String = ""
    @objc dynamic var  nameEmployee: String = ""
    @objc dynamic var  profileUser: String = ""
    @objc dynamic var  tokenFirebase: String = ""
    @objc dynamic var  connectFirebase: String = "0"
    @objc dynamic var  currentIdEvent: String = ""
    @objc dynamic var  currentTitleEvent: String = ""
    @objc dynamic var  currentIdActivity: String = ""
    @objc dynamic var  currentNameCampaign: String = ""
    @objc dynamic var  currentIdCategory: String = ""
    @objc dynamic var  currentTitleCategory: String = ""
    @objc dynamic var  currentLatitude: Double = 0.00
    @objc dynamic var  currentLongitude: Double = 0.00
    @objc dynamic var  currentLatitudeEvent: Double = 0.00
    @objc dynamic var  currentLongitudeEvent: Double = 0.00
    @objc dynamic var  distanceUserToEvent: Double = 0.00
    @objc dynamic var  isNearEvent: Bool = false
    @objc dynamic var  firstIdEvent: String = ""
    @objc dynamic var  lasttIdEvent: String = ""
    @objc dynamic var  checkIn: String = "0"
    @objc dynamic var  checkOut: String = "0"
    @objc dynamic var  typeCheck: String = "0"
    @objc dynamic var  isCheckInOutCurrentEvent: String = "0"
    @objc dynamic var  isReasonMissing: String = "0"

    override static func primaryKey() -> String? {
        return "uui"
    }

}
