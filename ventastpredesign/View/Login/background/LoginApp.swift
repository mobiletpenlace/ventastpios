//
//  LoginApp.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 09/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift

class LoginApp: NSObject, Mappable {

    @objc dynamic var  input: String = ""
    @objc dynamic var  Ip: String = ""
    @objc dynamic var  Password: String = ""
    @objc dynamic var  UserId: String = ""

    required init?(map: Map) {
    }

    override init() {
    }

    func mapping(map: Map) {
        input <- map["input"]
        Ip <- map["Ip"]
        Password <- map["Password"]
        UserId <- map["UserId"]
    }

}
