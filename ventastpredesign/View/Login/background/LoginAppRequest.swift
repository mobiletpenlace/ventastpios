//
//  LoginAppRequest.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta Ortega on 10/8/19.
//  Copyright © 2019 Juan Reynaldo Escobar Miron. All rights reserved.
//

import UIKit
import ObjectMapper

class LoginAppRequest: BaseRequest {

    var idDevice: String?
    var passwordUser: String?
    var idUser: String?
    var Mlogin: LoginApp?

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        idDevice <- map[""]
        passwordUser <- map["passwordUser"]
        idUser <- map["idUser"]
        Mlogin <- map["Login"]
    }

}
