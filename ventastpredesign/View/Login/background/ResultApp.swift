//
//  Result.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta Ortega on 10/7/19.
//  Copyright © 2019 Juan Reynaldo Escobar Miron. All rights reserved.
//

import UIKit
import RealmSwift
import ObjectMapper

class ResultApp: Object, Mappable {
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  IdResult: String = ""
    @objc dynamic var  Result: String = ""
    @objc dynamic var  ResultDescription: String?

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        IdResult <- map["IdResult"]
        Result <- map["Result"]
        ResultDescription <- map["ResultDescription"]
    }

    override static func primaryKey() -> String? {
        return "uui"
    }

}
