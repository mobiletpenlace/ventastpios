//
//  Employe.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 13/04/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class Employe: NSObject, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        idEmpleado <- map["idEmpleado"]
        nombreEmpleado <- map["nombreEmpleado"]
        canal <- map["canal"]
        distritalId <- map["distritalId"]
        estatus <- map["estatus"]
        folioEmpleado <- map["folioEmpleado"]
        idExterno <- map["idExterno"]
        posicion <- map["posicion"]
        distritalName <- map["distritalName"]
        empleadoDistritalName <- map["empleadoDistritalName"]
        empleadoDistritalId <- map["empleadoDistritalId"]
        distritalName <- map["distritalName"]
        pais <- map["pais"]
        canalVenta <- map["canalVenta"]
        canales <- map ["canales"]
        idDevice <- map ["idDevice"]
        isSessionActive <- map ["isSesionActive"]
        contrasena <- map["contrasena"]
        subcanal <- map["subcanal"]
    }

    var idEmpleado: String = ""
    var nombreEmpleado: String = ""
    var canal: String = ""
    var distritalId: String = ""
    var estatus: String = ""
    var folioEmpleado: String = ""
    var idExterno: String = ""
    var posicion: String = ""
    var distritalName: String = ""
    var empleadoDistritalName: String = ""
    var empleadoDistritalId: String = ""
    var pais: String = ""
    var canalVenta: String = ""
    var canales: [Canales] = []
    var idDevice: String = ""
    var isSessionActive: Bool = false
    var contrasena: String = ""
    var subcanal: String = String.emptyString
}
