//
//  employeRequest.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 07/04/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

public class EmployeRequest: AlfaRequest {
    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override public func mapping(map: Map) {
        folio <- map["folioEmpleado"]
    }

    var folio: String?
}
