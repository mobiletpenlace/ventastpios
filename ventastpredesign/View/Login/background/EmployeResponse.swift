//
//  EmployeResponse.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 13/04/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class EmployeResponse: BaseResponse {

    required init?(map: Map) {

    }

    override func mapping(map: Map) {
        resultDescription <- map["resultDescription"]
        result <- map["result"]
        empleado <- map["empleado"]

    }
    override init() {

    }

    var empleado: String?

}
