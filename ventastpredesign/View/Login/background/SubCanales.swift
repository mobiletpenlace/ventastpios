//
//  SubCanales.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 13/04/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper
import RealmSwift

class SubCanales: NSObject, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        nombre <- map["nombre"]

    }

    var  nombre: String = ""

}
