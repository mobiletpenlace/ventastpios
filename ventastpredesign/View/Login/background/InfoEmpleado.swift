//
//  InfoEmpleado.swift
//  ventastp
//
//  Created by Armando Isais Olguin Cabrera on 21/10/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//
//

import UIKit
import ObjectMapper

class InfoEmpleado: NSObject, Mappable {
    required init?(map: Map) {

    }

    func mapping(map: Map) {
        idDevice <- map["idDevice"]
        idEmpleado <- map["idEmpleado"]
        isSessionActive <- map["isSesionActive"]
        tipo <- map["tipo"]

    }

    override init() {
    }

    var idDevice: String = ""
    var idEmpleado: String = ""
    var isSessionActive: Bool = false
    var tipo: String = ""

}
