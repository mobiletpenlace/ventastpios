//
//  LoginAppResponse.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta Ortega on 10/8/19.
//  Copyright © 2019 Juan Reynaldo Escobar Miron. All rights reserved.
//

import UIKit
import ObjectMapper

class LoginAppResponse: BaseResponse {

    var MResult: ResultApp?
    var tokenFB: String?
    var idUser: String?
    var profileUser: String?

    required init?(map: Map) {

    }

    override func mapping(map: Map) {
        MResult <- map["Result"]
        tokenFB <- map["tokenFB"]
        idUser <- map["idUser"]
        profileUser <- map["profileUser"]
    }

}
