//
//  OktaUserResponse.swift
//  ventastp
//
//  Created by Ulises Ortega on 19/07/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class OktaUserResponse: BaseResponse {

    required init?(map: Map) {

    }

    override func mapping(map: Map) {
        sub <- map["sub"]
        name <- map["name"]
        locale <- map["locale"]
        preferredUsername <- map["preferred_username"]
        givenName <- map["given_name"]
        familyName <- map["family_name"]
        zoneinfo <- map["zoneinfo"]
        updateAt <- map["update_at"]
    }
    override init() {

    }

    var sub: String?
    var name: String?
    var locale: String?
    var preferredUsername: String?
    var givenName: String?
    var familyName: String?
    var zoneinfo: String?
    var updateAt: String?

}
