//
//  LoginEmpleadoRequest.swift
//  ventastp
//
//  Created by Armando Isais Olguin Cabrera on 21/10/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class LoginEmpleadoRequest: BaseRequest {

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        InfoEmpleado <- map["infoEmpleado"]
    }

    var InfoEmpleado: InfoEmpleado?

}
