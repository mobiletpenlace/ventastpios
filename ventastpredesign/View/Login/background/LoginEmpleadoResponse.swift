//
//  LoginEmpleadoResponse.swift
//  ventastp
//
//  Created by Armando Isais Olguin Cabrera on 21/10/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class LoginEmpleadoResponse: BaseResponse {

    required init?(map: Map) {
    }

    override func mapping(map: Map) {
        resultDescription <- map["resultDescription"]
        result <- map["result"]
    }

    override init() {
    }
}
