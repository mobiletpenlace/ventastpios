//
//  loginVCDelegate.swift
//  SalesCloud
//
//  Created by Juan Reynaldo Escobar Miron on 16/12/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import UIKit
import Amplitude
import SwiftUI

extension LoginVC: LoginObserver {

    func successInfoLoginUpdate() {
        Logger.println("Se actualizo correctamente")
        
    }

    func succesLoginStreetTP() {
        Amplitude.instance().logEvent("Login_Success")
        let identify = AMPIdentify()
            .set("employeeType", value: "Promotor" as NSObject)
        Amplitude.instance().identify(identify!)
        savingUserPassword()
        Constants.LoadStoryBoard(storyboard: "HomeStreetView", identifier: "HomeStreetView", viewC: self)
        print("successSTREET")
    }

    func onError(errorMessage: String) {

        if errorMessage.contains(string: "usuario") {
            errorTheme(textField: MUserTextField)
            MUserTextField.leadingAssistiveLabel.text = "Verifique su Usuario"
            errorTheme(textField: MSecretWordTextField)
            MSecretWordTextField.leadingAssistiveLabel.text = "Verifique su Contraseña"
        } else {
            Logger.println("Verifique su Contraseña")
            errorTheme(textField: MSecretWordTextField)
            MSecretWordTextField.leadingAssistiveLabel.text = "Verifique su Contraseña"
        }

    }

    func succesLogin() {
        savingUserPassword()
        MViewButton.frame.origin = CGPoint(x: 265, y: 96)
    }

    func successLoginOkta(empleado: String) {
        mLoginViewModel.consultaEmpleado(empleado: empleado)
    }

    func succesToken() {
        mLoginViewModel.consultaEmpleado(empleado: MUserTextField.text!)
    }
    
    
    func successLoginInfoUpdate() {

    }

    func sessionIsActive() {
        Constants.Alert(title: "Ocurrio un problema", body: "Actualmente tiene una sesion activa", type: type.Error, viewC: self)
    }
    
    func successLeadsLogin() {
        let homeLeadsView = HomeLeadsView()
        let hostingController = UIHostingController(rootView: homeLeadsView)

        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.window?.rootViewController = hostingController
            appDelegate.window?.makeKeyAndVisible()
        }
    }


    func succesEmploye() {
        mLoginViewModel.updateInfoLogin(logout: true)
        Amplitude.instance().logEvent("Login_Success")
        let identify = AMPIdentify()
            .set("employeeType", value: "Vendedor" as NSObject)
        Amplitude.instance().identify(identify!)
//TODO: AQUI IMPLEMENTAR CONSUMO DE BANDERAS
        Constants.LoadStoryBoard(storyboard: "HomeScreenRedesign", identifier: "HomeScreenRedesign", viewC: self)

    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        MUserTextField.setTextColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .normal)
        MUserTextField.setTextColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .editing)

        MUserTextField.setNormalLabelColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .normal)
        MUserTextField.setNormalLabelColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .editing)

        MUserTextField.setFloatingLabelColor(UIColor(named: "Base_rede_ligth_gray_titles")!, for: .normal)
        MUserTextField.setFloatingLabelColor(UIColor(named: "Base_rede_ligth_gray_titles")!, for: .editing)

        MSecretWordTextField.setTextColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .normal)
        MSecretWordTextField.setTextColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .editing)

        MSecretWordTextField.setNormalLabelColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .normal)
        MSecretWordTextField.setNormalLabelColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .editing)

        MSecretWordTextField.setFloatingLabelColor(UIColor(named: "Base_rede_ligth_gray_titles")!, for: .normal)
        MSecretWordTextField.setFloatingLabelColor(UIColor(named: "Base_rede_ligth_gray_titles")!, for: .editing)

    }
}
