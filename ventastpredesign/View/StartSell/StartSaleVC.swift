//
//  StartSaleDelegate.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 25/05/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

extension StartSaleViewController: StartSaleObserver {
    func setDataLoc(loc: loc2) {
        LocationLocal = loc
    }

    func onDocumentos() {
        Documentos = true
    }

    func cargaOP() {
        faseFlujo = fase.ValidaCov
        isFirst = false
        LoadContainer(number: 1)
    }

    func cambiaPadre(paso: Int) {
        switch paso {
        case fase.Deal:
            LoadContainer(number: 1)
            faseFlujo = fase.ValidaCov
        case fase.InfoPersonal:
            LoadContainer(number: 2)
        case fase.Documentos:
            LoadContainer(number: 3)
        case fase.Pago:
            LoadContainer(number: 4)
        default:
            Logger.println("no jala el back")
        }
    }

    func actualizaFase(fase: Int) {
        faseFlujo = fase
    }

    func PrintMessage(message: String) {
        AlertDialog.show(title: "Alerta", body: message, view: self)
    }

    func changeItem(_ mController: UIViewController) {
        for subview in mViewContainer.subviews {
            subview.removeFromSuperview()
        }
        controller = mController
        addChildViewController(controller!)
        mViewContainer.addSubview(controller!.view)
        controller!.view.frame = mViewContainer.bounds
        controller!.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        controller?.didMove(toParentViewController: self)
    }

    func mapa() {
        if AppDelegate.Gmaps {
            var viewAlert: UIStoryboard!
            viewAlert = UIStoryboard(name: "MapValidateCoverageViewController", bundle: nil)
            let mController = viewAlert!.instantiateViewController(withIdentifier: "MapValidateCoverageViewController")
            changeItem(mController)
        } else {
            var viewAlert: UIStoryboard!
            viewAlert = UIStoryboard(name: "MapValidateCoverageViewController2", bundle: nil)
            let mController = viewAlert!.instantiateViewController(withIdentifier: " MapValidateCoverageViewController2")
            changeItem(mController)
        }
    }

    func form() {
        var viewAlert: UIStoryboard!
        viewAlert = UIStoryboard(name: "FormAddressView", bundle: nil)
        let mController = viewAlert!.instantiateViewController(withIdentifier: "FormAddressView")
        changeItem(mController)
    }

    func hogar() {
            var viewAlert: UIStoryboard!
            viewAlert = UIStoryboard(name: "CuestionsContainer", bundle: nil)
            let mController = viewAlert!.instantiateViewController(withIdentifier: "CuestionsContainer")
            changeItem(mController)
        }

    func bestFit() {
            var viewAlert: UIStoryboard!
            viewAlert = UIStoryboard(name: "BestFitView", bundle: nil)
            let mController = viewAlert!.instantiateViewController(withIdentifier: "BestFitView")
            changeItem(mController)
        }

    func suggestedPlan() {
            var viewAlert: UIStoryboard!
            viewAlert = UIStoryboard(name: "StepContainer", bundle: nil)
            let mController = viewAlert!.instantiateViewController(withIdentifier: "StepContainer")
            changeItem(mController)
        }

    func validateCovergeAndData(loc: loc2) {
        let viewAlert: UIStoryboard = UIStoryboard(name: "AlertWithCoverageViewController", bundle: nil)
        let mController = viewAlert.instantiateViewController(withIdentifier: "AlertWithCoverageViewController")as! AlertWithCoverageViewController
        mController.txtLatitud = loc.lat
        mController.txtLongitud = loc.long
        if (mController.txtLatitud != nil ) && (mController.txtLongitud != nil ) {
            mController.providesPresentationContextTransitionStyle = true
            mController.definesPresentationContext = true
            mController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            mController.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            changeItem(mController)
        }
    }

    func changeContainerPage(index: Int) {
        switch index {
        case 1:
            faseFlujo = fase.ValidaCov
        case 2:
            faseFlujo = fase.Deal
        case 3:
            faseFlujo = fase.Home
            fase.TypeHome = true
        case 4:
            faseFlujo = fase.BestFit
            fase.TypeHome = false
        case 5:
            faseFlujo = fase.Pago
        default:
            break
        }
        LoadContainer(number: index)
    }
}
