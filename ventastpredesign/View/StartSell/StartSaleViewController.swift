//
//  StartSaleViewController.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 03/04/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import UIKit

import SwiftEventBus

class StartSaleViewController: BaseVentasView {

    @IBOutlet weak var mViewContainer: UIView!

    var mStartSaleViewModel: StartSaleViewModel!
    var idRecovery: String = ""
    var viewAlert: UIStoryboard?
    var controller: UIViewController?
    var isFirst: Bool?
    var vista = 0
    var faseFlujo = 0
    var LocationLocal: loc2?
    var Documentos = false
    var banderaLocal = false

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mStartSaleViewModel = StartSaleViewModel(view: self)
        mStartSaleViewModel.attachView(self)
        if banderaLocal == false {
            mapa()
        } else {
            form()
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        if idRecovery != "" {
            mStartSaleViewModel.consultaOP(idOportunidad: idRecovery)
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        mStartSaleViewModel.viewWillDissapear()
    }

    func LoadContainer(number: Int) {
        if vista != number {
            switch number {
            case 1:
                mapa()
            case 2:
                validateCovergeAndData(loc: LocationLocal!)
            case 3:
                hogar()
            case 4:
                bestFit()
            case 5:
                suggestedPlan()
            default:
                break
            }
        }
    }

}
