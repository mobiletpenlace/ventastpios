//
//  AddDocumentRequest.swift
//  ventastpredesign
//
//  Created by Arturo Ceron Epigmenio on 05/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import UIKit
import ObjectMapper

class AddDocumentRequest: BaseRequest {

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        MLogin <- map["Login"]
        MDocumentosAdjuntos <- map["DocumentosAdjuntos"]
    }

    var MLogin: Login?
    var MDocumentosAdjuntos: DocumentosAdjuntos?

}
