//
//  AddDocumentResponse.swift
//  ventastpredesign
//
//  Created by Arturo Ceron Epigmenio on 05/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import UIKit
import ObjectMapper

class AddDocumentResponse: BaseResponse {

    required init?(map: Map) {
    }

    override func mapping(map: Map) {
        MResult <- map["Result"]
    }

    override init() {
    }

    var MResult: Result?

}
