//
//  LoadData.swift
//  ventastpredesign
//
//  Created by Arturo Ceron Epigmenio on 24/05/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import UIKit

import ObjectMapper
import RealmSwift

class ImgContrato: Object {
    override static func primaryKey() -> String? {
        return "uui"
    }
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  contrato: String = ""
    @objc dynamic var  contratoThumbnail: String = ""
    @objc dynamic var  nombre = ""
    @objc dynamic var  tipo = ""
}
class ImgIDFront: Object {
    override static func primaryKey() -> String? {
        return "uui"
    }
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  idFront: String = ""
    @objc dynamic var  idFrontThumbnail: String = ""
    @objc dynamic var  nombre = ""
    @objc dynamic var  tipo = ""
}
class ImgIDBack: Object {
    override static func primaryKey() -> String? {
        return "uui"
    }
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  idBack: String = ""
    @objc dynamic var  idBackThumbnail: String = ""
    @objc dynamic var  nombre = ""
    @objc dynamic var  tipo = ""
}
class ImgComprobante: Object {
    override static func primaryKey() -> String? {
        return "uui"
    }
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  comprobante: String = ""
    @objc dynamic var  comprobanteThumbnail: String = ""
    @objc dynamic var  nombre = ""
    @objc dynamic var  tipo = ""

}
class ImgFirma: Object {
    override static func primaryKey() -> String? {
        return "uui"
    }
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  firma: String = ""
    @objc dynamic var  firmaThumbnail: String = ""
    @objc dynamic var  nombre = ""
    @objc dynamic var  tipo = ""
}
class ImgRFC: Object {
    override static func primaryKey() -> String? {
        return "uui"
    }
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  rfc: String = ""
    @objc dynamic var  rfcThumbnail: String = ""
    @objc dynamic var  nombre = ""
    @objc dynamic var  tipo = ""
}
class ImgActa1: Object {
    override static func primaryKey() -> String? {
        return "uui"
    }
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  acta1: String = ""
    @objc dynamic var  acta1Thumbnail: String = ""
    @objc dynamic var  nombre = ""
    @objc dynamic var  tipo = ""
}
class ImgActa2: Object {
    override static func primaryKey() -> String? {
        return "uui"
    }
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  acta2: String = ""
    @objc dynamic var  acta2Thumbnail: String = ""
    @objc dynamic var  nombre = ""
    @objc dynamic var  tipo = ""
}
class ImgActa3: Object {
    override static func primaryKey() -> String? {
        return "uui"
    }
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  acta3: String = ""
    @objc dynamic var  acta3Thumbnail: String = ""
    @objc dynamic var  nombre = ""
    @objc dynamic var  tipo = ""
}
class ImgIDFrontCard: Object {
    override static func primaryKey() -> String? {
        return "uui"
    }
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  idFront: String = ""
    @objc dynamic var  idFrontThumbnail: String = ""
    @objc dynamic var  nombre = ""
    @objc dynamic var  tipo = ""
}
class ImgIDBackCard: Object {
    override static func primaryKey() -> String? {
        return "uui"
    }
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  idBack: String = ""
    @objc dynamic var  idBackThumbnail: String = ""
    @objc dynamic var  nombre = ""
    @objc dynamic var  tipo = ""
}
class ImgFirma2: Object {
    override static func primaryKey() -> String? {
        return "uui"
    }
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  firma2: String = ""
    @objc dynamic var  firma2Thumbnail: String = ""
    @objc dynamic var  nombre = ""
    @objc dynamic var  tipo = ""
}
