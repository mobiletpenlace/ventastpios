//
//  DocumentosAdjuntos.swift
//  ventastpredesign
//
//  Created by Arturo Ceron Epigmenio on 05/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import UIKit

import ObjectMapper

class DocumentosAdjuntos: NSObject, Mappable {
    required init?(map: Map) {

    }

    func mapping(map: Map) {
        CuentaBRM <- map["CuentaBRM"]
        Documento <- map["Documento"]
        OportunidadId <- map["OportunidadId"]

    }

    override init() {
    }

    var CuentaBRM: String?
    var Documento: [Documento] = []
    var OportunidadId: String?

}
