//
//  Documento.swift
//  ventastpredesign
//
//  Created by Arturo Ceron Epigmenio on 05/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class Documento: NSObject, Mappable {
    required init?(map: Map) {

    }

    func mapping(map: Map) {
        Body <- map["Body"]
        Tipo <- map["Tipo"]
        CuentaBRM <- map["CuentaBRM"]
        ContentType <- map["ContentType"]
        documentId <- map["documentId"]
        OportunidadId <- map["OportunidadId"]
        Nombre <- map["Nombre"]
        syncStatus <- map["syncStatus"]
        thumbnail <- map["thumbnail"]
    }

    override init() {
    }

    var Body: String?
    var CuentaBRM: String?
    var ContentType: String?
    var documentId: String?
    var OportunidadId: String?
    var Nombre: String?
    var syncStatus: String?
    var thumbnail: String?
    var Tipo: String?
}
