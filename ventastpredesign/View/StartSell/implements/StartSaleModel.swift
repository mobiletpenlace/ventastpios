//
//  StartSaleModel.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 02/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import RealmSwift

protocol StartSaleModelObserver {
    func onSuccesCreaVenta(response: CreaOPResponse)
    func onSuccesCreaModelado(response: CreaOPResponse)
    func onSuccesAgregaAddons(response: CreaOPResponse)
    func onSuccesRegistraFirma(response: CreaOPResponse)
    func onErrorVenta(message: String)
    func onSuccesConsultaOP(response: ConsultaOpResponse)
    func onSuccesModeladoOP(response: ConsultaOpResponse)
    func noModelado()
    func onSuccesDocumentos()
    func onErrorDoc(message: String)
    func onSuccesEnvioMarketing()
    func onSuccesIdPerfil()
}

class StartSaleModel: BaseModel {

    var mProspecto: NProspecto?
    var mSitio: Sitio?
    var mInfo: InfoAdicionalesCreacion?
    var mObjetoModelado: ObjetoModelado?
    var mPuntos: PuntosControl!
    var viewModel: StartSaleModelObserver?
    var server: ServerDataManager2<CreaOPResponse>?
    var server2: ServerDataManager2<ConsultaOpResponse>?
    var server3: ServerDataManager2<AddDocumentResponse>?
    var server4: ServerDataManager2<EnvioMarketingResponse>?
    var server5: ServerDataManager2<InfoProfileClientResponse>?
    var url = AppDelegate.API_SALESFORCE + ApiDefinition.API_CREA_VENTA
    var url2 = AppDelegate.API_SALESFORCE + ApiDefinition.API_CONSULTA_VENTA
    var url3 = AppDelegate.API_TOTAL + ApiDefinition.API_QUERY_UPLOAD_DOCUMENT
    var url4 = AppDelegate.API_SALESFORCE + ApiDefinition.API_ENVIA_MARKETING
    var url5 = AppDelegate.API_SALESFORCE + ApiDefinition.API_PLAN_SUGERIDO
    var tipo = 0
    var documentos: [AddDocumentRequest] = []
    var idOportunity = ""
    var brmAcount = ""
    var numDoc = 0

    init (observer: BaseViewModelObserver) {
        server = ServerDataManager2<CreaOPResponse>()
        server2 = ServerDataManager2<ConsultaOpResponse>()
        server3 = ServerDataManager2<AddDocumentResponse>()
        server4 = ServerDataManager2<EnvioMarketingResponse>()
        server5 = ServerDataManager2<InfoProfileClientResponse>()
        super.init(observerVM: observer)
    }

    func atachModel(viewModel: StartSaleModelObserver) {
        self.viewModel = viewModel

    }

    func creaOP(request: CreaOPRequest, Tipo: Int) {
        tipo = Tipo
        url = AppDelegate.API_SALESFORCE + ApiDefinition.API_CREA_VENTA
        server?.setValues(requestUrl: url, delegate: self, headerType: .headersSalesforce)
        server?.request(requestModel: request)
    }

    func creaVenta(request: CreaOPModeladoRequest, Tipo: Int) {
        tipo = Tipo
        url = AppDelegate.API_SALESFORCE + ApiDefinition.API_CREA_VENTA
        server?.setValues(requestUrl: url, delegate: self, headerType: .headersSalesforce)
        server?.request(requestModel: request)
    }

    func consultaOP(request: ConsultaOportunidadRequest, Tipo: Int) {
        tipo = Tipo
        url2 = AppDelegate.API_SALESFORCE + ApiDefinition.API_CONSULTA_VENTA
        server2?.setValues(requestUrl: url2, delegate: self, headerType: .headersSalesforce)
        server2?.request(requestModel: request)
    }

    func EnviaMarketing(request: EnvioMarketingRequest) {
        url4 = AppDelegate.API_SALESFORCE + ApiDefinition.API_ENVIA_MARKETING
        server4?.setValues(requestUrl: url4, delegate: self, headerType: .headersSalesforce)
        server4?.request(requestModel: request)
    }

    func EnviaIdCliente(request: InfoProfileClientRequest) {
        url5 = AppDelegate.API_SALESFORCE + ApiDefinition.API_PLAN_SUGERIDO
        server5?.setValues(requestUrl: url5, delegate: self, headerType: .headersSalesforce)
        server5?.request(requestModel: request)
    }

    func CargaDocumentos(docs: DocumentosObject, cuentaBRM: String, idOportunidad: String ) {
        idOportunity = idOportunidad
        brmAcount = cuentaBRM
        if numDoc == 0 {
            numDoc = 0
            let IneFront: AddDocumentRequest = AddDocumentRequest()
            let IneBack: AddDocumentRequest = AddDocumentRequest()
            let ineFrontDocument: Documento = Documento()
            let ineBackDocument: Documento = Documento()
            let ProofDoc: AddDocumentRequest = AddDocumentRequest()
            let ProofDocument: Documento = Documento()
            // identificacion frontal
            ineFrontDocument.Body = docs.ineFront
            ineFrontDocument.ContentType = "image/jpeg"
            ineFrontDocument.Tipo = "Identificación del representante legal"
            ineFrontDocument.Nombre = "Identificacion1.jpeg"
            ineFrontDocument.syncStatus = "1"
            ineFrontDocument.OportunidadId = idOportunity
            ineFrontDocument.CuentaBRM = brmAcount
            IneFront.MLogin = Login()
            IneFront.MLogin?.Ip = "1.1.1.1"
            IneFront.MLogin?.User = "25631"
            IneFront.MLogin?.SecretWord = "Middle100$"
            IneFront.MDocumentosAdjuntos = DocumentosAdjuntos()
            IneFront.MDocumentosAdjuntos?.CuentaBRM = brmAcount
            IneFront.MDocumentosAdjuntos?.Documento.append(ineFrontDocument)
            IneFront.MDocumentosAdjuntos?.OportunidadId = idOportunity
            // identificacion reverso
            ineBackDocument.Body = docs.ineBack
            ineBackDocument.ContentType = "image/jpeg"
            ineBackDocument.Tipo = "Identificación del representante legal"
            ineBackDocument.Nombre = "Identificacion2.jpeg"
            ineBackDocument.syncStatus = "1"
            ineBackDocument.OportunidadId = idOportunity
            ineBackDocument.CuentaBRM = brmAcount
            IneBack.MLogin = Login()
            IneBack.MLogin?.Ip = "1.1.1.1"
            IneBack.MLogin?.User = "25631"
            IneBack.MLogin?.SecretWord = "Middle100$"
            IneBack.MDocumentosAdjuntos = DocumentosAdjuntos()
            IneBack.MDocumentosAdjuntos?.CuentaBRM = brmAcount
            IneBack.MDocumentosAdjuntos?.Documento.append(ineBackDocument)
            IneBack.MDocumentosAdjuntos?.OportunidadId = idOportunity

            if docs.proofDirection != "" {
                ProofDocument.Body = docs.ineBack
                ProofDocument.ContentType = "image/jpeg"
                ProofDocument.Tipo = "Comprobante de domicilio"
                ProofDocument.Nombre = "ComprobanteDomicilio1.jpeg"
                ProofDocument.syncStatus = "1"
                ProofDocument.OportunidadId = idOportunity
                ProofDocument.CuentaBRM = brmAcount
                ProofDoc.MLogin = Login()
                ProofDoc.MLogin?.Ip = "1.1.1.1"
                ProofDoc.MLogin?.User = "25631"
                ProofDoc.MLogin?.SecretWord = "Middle100$"
                ProofDoc.MDocumentosAdjuntos = DocumentosAdjuntos()
                ProofDoc.MDocumentosAdjuntos?.CuentaBRM = brmAcount
                ProofDoc.MDocumentosAdjuntos?.Documento.append(ProofDocument)
                ProofDoc.MDocumentosAdjuntos?.OportunidadId = idOportunity
            } else {
                ProofDoc.MDocumentosAdjuntos = DocumentosAdjuntos()
                ProofDoc.MDocumentosAdjuntos?.CuentaBRM = ""
            }
            documentos.append(IneFront)
            documentos.append(IneBack)
            documentos.append(ProofDoc)
            enviaDoc(doc: documentos[0])
        } else {
            enviaDoc(doc: documentos[numDoc])
        }
    }

    func enviaDoc(doc: AddDocumentRequest) {
        url3 = AppDelegate.API_TOTAL + ApiDefinition.API_QUERY_UPLOAD_DOCUMENT
        server3?.setValues(requestUrl: url3, delegate: self, headerType: .headersMiddle)
        server3?.request(requestModel: doc)
    }

    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if requestUrl == url {
            let mCreaOpResponse = response as! CreaOPResponse
            if mCreaOpResponse.result == "0" {
                if tipo == 0 {
                    idOportunity = mCreaOpResponse.info.idOportunidad!
                    brmAcount = mCreaOpResponse.info.numeroCuenta!
                    viewModel?.onSuccesCreaVenta(response: mCreaOpResponse)
                } else if tipo == 1 {
                    viewModel?.onSuccesCreaModelado(response: mCreaOpResponse)
                } else if tipo == 2 {
                    viewModel?.onSuccesAgregaAddons(response: mCreaOpResponse)
                } else {
                    viewModel?.onSuccesRegistraFirma(response: mCreaOpResponse)
                }
            } else {
                viewModel?.onErrorVenta(message: mCreaOpResponse.resultDescription)
                faseDataOK.isOK = false
            }
        } else if requestUrl == url2 {
            let mConsultaOpResponse = response as! ConsultaOpResponse
            if mConsultaOpResponse.result == "0" {
                if tipo == 0 {
                    viewModel?.onSuccesConsultaOP(response: mConsultaOpResponse)
                } else if tipo == 1 {
                    viewModel?.onSuccesModeladoOP(response: mConsultaOpResponse)
                } else {

                }
            } else {
                if tipo == 0 {
                    viewModel?.onErrorVenta(message: mConsultaOpResponse.resultDescription)
                } else {
                    viewModel?.noModelado()
                }
            }
        } else if requestUrl == url3 {
            let mAddDocumentResponse = response as! AddDocumentResponse
            if mAddDocumentResponse.MResult?.Description == "Petición procesada con éxito" {
                if numDoc == 0 {
                    numDoc = 1
                    enviaDoc(doc: documentos[numDoc])
                } else if numDoc == 1 {
                    numDoc = 2
                    if documentos[numDoc].MDocumentosAdjuntos?.CuentaBRM != "" {
                        enviaDoc(doc: documentos[numDoc])
                    } else {
                        viewModel?.onSuccesDocumentos()
                    }
                } else if numDoc == 2 {
                    viewModel?.onSuccesDocumentos()
                }
            } else {
                viewModel?.onErrorDoc(message: String("No se ha podido enviar el documento \(documentos[numDoc].MDocumentosAdjuntos!.Documento[0].Nombre!)"))
            }
        } else if requestUrl == url4 {
            let mEnviaMarketing = response as! EnvioMarketingResponse
            if mEnviaMarketing.result ==  "0" {
                viewModel?.onSuccesEnvioMarketing()
            } else {
                viewModel?.onErrorDoc(message: "Ha ocurrido un error al enviar el mesaje de bienvenida")
            }
        } else if requestUrl == url5 {
            let mInfoPerfil = response as! InfoProfileClientResponse
            if mInfoPerfil.result == "0" {
                viewModel?.onSuccesIdPerfil()
            } else {
                viewModel?.onErrorDoc(message: "Ha ocurrido un error")
            }
        }
    }

    func getSitio() -> Sitio {
        var mSitio = Sitio()
        if RealManager.findFirst(object: Sitio.self) != nil {
            mSitio = Sitio(value: RealManager.findFirst(object: Sitio.self)!)
        }
        return mSitio
    }

    func getProspecto() -> NProspecto {
        var mProspecto = NProspecto()
        if RealManager.findFirst(object: NProspecto.self) != nil {
            mProspecto = NProspecto(value: RealManager.findFirst(object: NProspecto.self)!)
        }
        return mProspecto
    }

    func setSitio(_ sitio: Sitio) {
        mSitio = Sitio()
        mSitio = sitio
        if RealManager.findFirst(object: Sitio.self) != nil {
            RealManager.deleteAll(object: Sitio.self)
            _ = RealManager.insert(object: mSitio!)
        } else {
            _ = RealManager.insert(object: mSitio!)
        }
    }

    func getPuntos() -> PuntosControl {
        mPuntos = PuntosControl()
        if RealManager.findFirst(object: PuntosControl.self) != nil {
            mPuntos = PuntosControl(value: RealManager.findFirst(object: PuntosControl.self))
        }
        return mPuntos
    }

    func setPuntos(_ puntos: PuntosControl) {
        if RealManager.findFirst(object: PuntosControl.self) != nil {
            _ = RealManager.update(object: puntos)
        } else {
            _ = RealManager.insert(object: puntos)
        }
    }

    func setProspecto(_ prospecto: NProspecto) {
        mProspecto = NProspecto()
        mProspecto = prospecto
        mProspecto?.uui = "1"
        if RealManager.findFirst(object: NProspecto.self) != nil {
            RealManager.deleteAll(object: NProspecto.self)
            _ = RealManager.insert(object: mProspecto!)
        } else {
            _ = RealManager.insert(object: mProspecto!)
        }
    }

    func setObjetoModelado(_ modelado: ObjetoModelado) {
        mObjetoModelado = ObjetoModelado()
        mObjetoModelado = modelado
        if RealManager.findFirst(object: ObjetoModelado.self) != nil {
            RealManager.deleteAll(object: ObjetoModelado.self)
            _ = RealManager.insert(object: mObjetoModelado!)
        } else {
            _ = RealManager.insert(object: mObjetoModelado!)
        }
    }

    func getInfoAddons() -> InfoAdicionalesCreacion {
        mInfo = InfoAdicionalesCreacion()

        if TPAddonsSelectedShared.shared.findAddonsPrev() != nil {
            mInfo = TPAddonsSelectedShared.shared.infoAddonsCreacion
        }

        /*if RealManager.findFirst(object: InfoAdicionalesCreacion.self) != nil {
            mInfo = RealManager.getFromFirst(object: InfoAdicionalesCreacion.self, field: "uui", equalsTo: "1")
        }*/
        return mInfo!
    }

    func setInfoAddons(_ infoAddons: InfoAdicionalesCreacion) {
        mInfo = InfoAdicionalesCreacion()
        mInfo = infoAddons

        if TPAddonsSelectedShared.shared.findAddonsPrev() != nil {
            TPAddonsSelectedShared.shared.cleanAddonsCreacion()
            TPAddonsSelectedShared.shared.infoAddonsCreacion = mInfo
        } else {
            mInfo?.uui = "1"
            TPAddonsSelectedShared.shared.infoAddonsCreacion = mInfo
        }

        /*if RealManager.findFirst(object: InfoAdicionalesCreacion.self) != nil {
            RealManager.deleteAll(object: InfoAdicionalesCreacion.self)
            _ = RealManager.insert(object: mInfo!)
        } else {
            mInfo?.uui = "1"
            _ = RealManager.insert(object: mInfo!)
        }*/
    }

    func limpieza() {

        if TPAddonsSelectedShared.shared.findAddonsPrev() != nil {
            TPAddonsSelectedShared.shared.cleanAddonsCreacion()
        }

        /*if RealManager.findFirst(object: InfoAdicionalesCreacion.self) != nil {
            RealManager.deleteAll(object: InfoAdicionalesCreacion.self)
        }*/
        if RealManager.findFirst(object: ObjetoModelado.self) != nil {
            RealManager.deleteAll(object: ObjetoModelado.self)
        }
        if RealManager.findFirst(object: NProspecto.self) != nil {
            RealManager.deleteAll(object: NProspecto.self)
        }
        if RealManager.findFirst(object: Sitio.self) != nil {
            RealManager.deleteAll(object: Sitio.self)
        }
        if RealManager.findFirst(object: PuntosControl.self) != nil {
            RealManager.deleteAll(object: PuntosControl.self)
        }
        ADDONS_PROMO_SELECTED = []
        ADDONS_PRODUCT_SELECTED = []
        ADDONS_SERVICE_CREATION_SELECTED = []
        PRICE_DISCOUNT = 0
        PRICE_ADDONS_SERVICE = 0
    }

}
