//
//  StartSalePresenter.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 08/04/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus
import ObjectMapper
import RealmSwift
import Logging
import Alamofire
import SwiftyJSON

protocol StartSaleObserver {
    func changeContainerPage(index: Int)
    func mapa()
    func form()
    func PrintMessage(message: String)
    func actualizaFase(fase: Int)
    func cambiaPadre(paso: Int)
    func cargaOP()
    func onDocumentos()
    func setDataLoc(loc: loc2)
    func validateCovergeAndData(loc: loc2)
}

class StartSaleViewModel: BaseViewModel, StartSaleModelObserver {

    var model: StartSaleModel!
    var mObserver: StartSaleObserver?
    var mProspecto: NProspecto?
    var mPlan: Plan!
    var cuentaBRM: String = ""
    var mIdOportunidad: String = ""
    var numeroOportunidad: String = ""
    var mIdPlan: String = ""
    var idCliente: String =  ""
    var mAddons: InfoAdicionalesCreacion!
    var existeModelado: Bool = false
    var existenAddons: Bool = false
    var existeProspecto: Bool = false
    var addonsCargados: Bool = false
    var documentos: Bool = false
    var mSitio: Sitio?
    var step2: Bool = false
    var ineBack: String = ""
     // CargaDocumentos
    var mDocs: DocumentosObject!
    var mProductosAdd = ProductosAdd()
    var mPromocionesAdd = PromocionesAdd()
    var mServiciosAdd = ServiciosAdd()
    var addonsCarga: InfoAddonsCreacionRequest!
    var modeladoCargado: InfoModeladoResponse?
    var mPuntosDeControl: PuntosControl!
    var mRealm = RealManager.mReal()
    var mFirma =  FirmaObject()
    var mDataIne: ConsultINEDataResponse!

    override init(view: BaseVentasView) {
        super.init(view: view)
        model = StartSaleModel(observer: self)
        model.atachModel(viewModel: self)
        recuperaPuntos()
    }

    func attachView(_ Observer: StartSaleObserver) {
        self.mObserver = Observer
        SwiftEventBus.onMainThread(self, name: "Actualiza_fase") { [weak self] result in
            if let number = result?.object as? Int {
                self?.mObserver?.actualizaFase(fase: number)
            }
        }

        SwiftEventBus.onMainThread(self, name: "backPadre") { [weak self] result in
            if let number = result?.object as? Int {
                self?.mObserver?.cambiaPadre(paso: number)
            }
        }

        SwiftEventBus.onMainThread(self, name: "Cambia_paso_container") { [weak self] result in
            if let number = result?.object as? Int {
                self?.mObserver?.changeContainerPage(index: number)
            }
        }
        SwiftEventBus.onMainThread(self, name: "Documentos") { [weak self] _ in
            self?.mObserver?.onDocumentos()
        }

        SwiftEventBus.onMainThread(self, name: "Mapa") { [weak self] _ in
            self?.mObserver?.mapa()
        }

        SwiftEventBus.onMainThread(self, name: "DireccionFormulario") { [weak self] _ in
            self?.mObserver?.form()
        }

        SwiftEventBus.onMainThread(self, name: "SetLocationForSearch") { [weak self] result in
            if let loc = result?.object as? loc2 {
                self?.mObserver?.setDataLoc(loc: loc)
            }
        }

        SwiftEventBus.onMainThread(self, name: "CargaInfoStep1Sitio") { [weak self] result in
            if let sitio = result?.object as? Sitio {
                self?.setSitio(sitio)
                self?.step2 = true
            } else {
                self?.mObserver?.PrintMessage(message: "Ocurrio un  error en la comunicacion interna")
            }
        }

        SwiftEventBus.onMainThread(self, name: "CargaInfoStep1Prospecto") { [weak self] result in
            if let prospecto = result?.object as? NProspecto {
                self?.setProspecto(prospecto)
            } else {
                self?.mObserver?.PrintMessage(message: "Ocurrio un  error en la comunicacion interna")
            }
        }

        SwiftEventBus.onMainThread(self, name: "receiveIDProfileCliente") { [weak self] result in
            if let idProfileClte = result?.object as? String {

            }
        }

        SwiftEventBus.onMainThread(self, name: "receiveIDPlan") { [weak self] result in
            if let idPlan = result?.object as? String {
                self?.mIdPlan = idPlan
                print(idPlan)
            }
        }

        SwiftEventBus.onMainThread(self, name: "CargaInfoStep3") { [weak self] result in
            self?.view.showLoading(message: "Creando venta")
            if let prospecto = result?.object as? NProspecto {
                self?.completaProspecto(prospecto)
            } else {
                self?.mObserver?.PrintMessage(message: "Ocurrio un  error en la comunicacion interna")
            }
        }

        SwiftEventBus.onMainThread(self, name: "Valida_Oportunidad") { [weak self] _ in
            self?.ValidaOP()
        }

        SwiftEventBus.onMainThread(self, name: "Modelado_cargado") { [weak self] result in
            if let plan = result?.object as? Plan {
                self?.mPlan = Plan()
                self?.mPlan = plan
                self?.mIdPlan = plan.id
            } else {
                self?.mObserver?.PrintMessage(message: "Ocurrio un  error en la comunicacion interna")
            }
        }

        SwiftEventBus.onMainThread(self, name: "Addons_cargados") { [weak self] result in
            if let info = result?.object as? InfoAdicionalesCreacion {
                self!.mAddons = info
                self?.recibeModelado()
            } else {
//                self?.observer?.PrintMessage(message: "Ocurrio un  error en la comunicacion interna")
            }

        }

        SwiftEventBus.onMainThread(self, name: "envia_IDCliente") { [weak self] result in
            if let id = result?.object as? String {
                self?.idCliente = id
            } else {
                self?.mObserver?.PrintMessage(message: "Ocurrio un  error en la comunicacion interna")
            }
        }

        SwiftEventBus.onMainThread(self, name: "CargaDocumentos") { [weak self] result in
            if let docs = result?.object as? DocumentosObject {
                self?.cargaDocumentos(docs: docs)
            } else {
                self?.mObserver?.PrintMessage(message: "Ocurrio un  error en la comunicacion interna")
            }
        }

        SwiftEventBus.onMainThread(self, name: "carga_firma_request") { [weak self] result in
                if let firma = result?.object as? FirmaObject {
                    self?.mFirma = firma
                } else {
                    self?.mObserver?.PrintMessage(message: "Ocurrio un error en la comunicacion interna")
                }
        }

        SwiftEventBus.onMainThread(self, name: "DatosINE") { [weak self] result in
            if let data = result?.object as? ConsultINEDataResponse {
                self?.mDataIne = data
            } else {
                self?.mObserver?.PrintMessage(message: "Ocurrio un error en la comunicacion interna")
            }
        }

        SwiftEventBus.onMainThread(self, name: "Limpieza_general") { [weak self] _ in
            self?.Limpieza()
        }

    }

    func viewWillDissapear() {
        SwiftEventBus.post("RemoveEventBus", sender: nil)
        SwiftEventBus.post("RemoveEventBusP3", sender: nil)
        SwiftEventBus.unregister(self)
    }

    // MARK: - proceso de datos:

    func recuperaPuntos() {
        if RealManager.findFirst(object: PuntosControl.self) != nil {
            mPuntosDeControl = model.getPuntos()
        } else {
            mPuntosDeControl = PuntosControl()
        }
        existeProspecto = mPuntosDeControl.existeProspecto
        existeModelado = mPuntosDeControl.existeModelado
        existenAddons = mPuntosDeControl.existenAddons
        addonsCargados = mPuntosDeControl.addonsCargados
        mIdPlan = mPuntosDeControl.idPlan
        mIdOportunidad = mPuntosDeControl.idOP
        cuentaBRM = mPuntosDeControl.cuentaBRM
    }

    func ValidaOP() {
        if mIdOportunidad != "" {
            try! mRealm.write {
                mPuntosDeControl.idOP = mIdOportunidad
                mPuntosDeControl.cuentaBRM = cuentaBRM
            }
            if mIdPlan != "" {
                try! mRealm.write {
                    mPuntosDeControl.idPlan = mIdPlan
                }
                if existeModelado {
                    try! mRealm.write {
                        mPuntosDeControl.existeModelado = true
                    }
                    if existenAddons {
                        try! mRealm.write {
                            mPuntosDeControl.existenAddons = true
                        }
                        if addonsCargados {
                            try! mRealm.write {
                                mPuntosDeControl.addonsCargados = true
                            }
                            model.CargaDocumentos(docs: mDocs, cuentaBRM: self.cuentaBRM, idOportunidad: self.mIdOportunidad)
                        } else {
                            agregaAddonsOP()
                        }
                    } else if documentos {
                        model.CargaDocumentos(docs: mDocs, cuentaBRM: self.cuentaBRM, idOportunidad: self.mIdOportunidad)
                    } else {
                        view.hideLoading()
                    }
                } else {
                    agregaModeladoOP()
                }
            } else {
                view.hideLoading()
            }
        } else {
            if existeProspecto {
                try! mRealm.write {
                    mPuntosDeControl.existeProspecto = true
                }
                creaOP()
            } else {
                let modelado = ObjetoModelado()
                modelado.planSeleccionadoId = mIdPlan
                model.setObjetoModelado(modelado)
                if existenAddons {
                    model.setInfoAddons((mAddons)!)
                }
                SwiftEventBus.post("Cambia_paso_container", sender: 3)
            }
        }
        model.setPuntos(mPuntosDeControl)
    }

    func completaProspecto(_ prospecto: NProspecto) {
        mProspecto = NProspecto()
        let aux = getProspecto()
        let empleado = Empleado(value: RealManager.findFirst(object: Empleado.self))
        mProspecto!.codigoPostal = aux.codigoPostal
        mProspecto!.colonia = aux.colonia
        mProspecto!.delegacionMunicipio = aux.delegacionMunicipio
        mProspecto!.ciudad = aux.ciudad
        mProspecto!.estado = aux.estado
        mProspecto!.calle = aux.calle
        mProspecto!.numExterior = aux.numExterior
        mProspecto!.numInterior = aux.numInterior
        mProspecto!.plusCode = aux.plusCode
        mProspecto!.refCalle = aux.refCalle
        mProspecto!.refUrbana = aux.refUrbana
        mProspecto!.canalEmpleado = aux.canalEmpleado
        mProspecto!.subCanalEmpleado = aux.subCanalEmpleado
        mProspecto!.idCodigoPostal = aux.idCodigoPostal
        mProspecto!.estimuloFiscal = aux.estimuloFiscal
        mProspecto!.firstName = prospecto.firstName
        mProspecto!.lastName = prospecto.lastName
        mProspecto!.midleName = prospecto.midleName
        mProspecto!.fechaNacimiento = prospecto.fechaNacimiento
        mProspecto!.rfc = prospecto.rfc
        mProspecto!.company = prospecto.company
        mProspecto!.telefono = prospecto.telefono
        mProspecto!.otroTelefono = prospecto.otroTelefono
        mProspecto!.celular = prospecto.celular
        mProspecto!.correo = prospecto.correo
        mProspecto!.otroCoreo = prospecto.otroCoreo
        mProspecto!.tipoMoneda = "Peso"
        mProspecto!.tipoPersona = prospecto.tipoPersona
        mProspecto!.idEmpleado = empleado.Id
        if prospecto.esNegocio {
            mProspecto!.sector = prospecto.sector
            mProspecto!.subsector = prospecto.subsector
            mProspecto!.nombreNegocio = prospecto.nombreNegocio
            mProspecto!.esNegocio = prospecto.esNegocio
            mProspecto!.aforo = prospecto.aforo
        }
        if UserDefinitions.isEmbajador {
            mProspecto!.canalEmpleado = "Autoempresarios"
            mProspecto!.subCanalEmpleado = "Vta Directa"
        } else {
            mProspecto?.canalEmpleado = aux.canalEmpleado
            mProspecto?.subCanalEmpleado = aux.subCanalEmpleado
        }
        self.setProspecto(mProspecto!)
        existeProspecto = true
    }

    func cargaDocumentos(docs: DocumentosObject) {
        documentos  = true
        mDocs = DocumentosObject()
        mDocs = docs
    }

    func comparaAddons() {
        var servicioDiferente: [ServiciosCreacionRequest] = []
        for servicioNuevo in addonsCarga.serviciosAdd.servicios {
            var contador = 0
            for servicio in modeladoCargado!.serviciosAdd.servicios {
                if servicio.id != servicioNuevo.id {
                    contador += 1
                }
            }
            if contador == modeladoCargado!.serviciosAdd.servicios.count {
                servicioDiferente.append(servicioNuevo)
            }
        }

        var productoDiferente: [ProductosCreacionRequest] = []
        for productoNuevo in addonsCarga.productosAdd.productos {
            var contador = 0
            for producto in modeladoCargado!.productosConsulta.productos {
                if productoNuevo.id != producto.id {
                    contador += 1
                }
            }
            if contador == modeladoCargado?.productosConsulta.productos.count {
                productoDiferente.append(productoNuevo)
            }
        }

        var promocionesDiferente: [PromocionesCreacionRequest] = []
        for promocionNueva in addonsCarga.promocionesAdd.promociones {
            var contador = 0
            for promocion in modeladoCargado!.promocionesAdd.promociones {
                if promocion.id != promocionNueva.id {
                    contador += 1
                }
            }
            if contador == modeladoCargado!.promocionesAdd.promociones.count {
                promocionesDiferente.append(promocionNueva)
            }
        }

        if promocionesDiferente.count != 0 || productoDiferente.count != 0 || servicioDiferente.count != 0 {
            eliminaAddons(info: modeladoCargado!)
        } else {
            view.hideLoading()
            SwiftEventBus.post("Cambia_paso_container", sender: 4)
        }
    }

    func recibeModelado() {
//        mAddons = InfoAdicionalesCreacion()
//        mAddons = model.getInfoAddons()
        addonsCarga = InfoAddonsCreacionRequest()
        for producto in mAddons.productosAdd!.productos {
            let addon: ProductosCreacionRequest = ProductosCreacionRequest()
            addon.id = producto.id
            addonsCarga.productosAdd.productos.append(addon)
        }
        for promocion in mAddons.promocionesAdd!.promociones {
            let promo: PromocionesCreacionRequest = PromocionesCreacionRequest()
            promo.id = promocion.id
            addonsCarga.promocionesAdd.promociones.append(promo)
        }
        for servicio in mAddons.serviciosAdd!.servicios {
            let serv: ServiciosCreacionRequest = ServiciosCreacionRequest()
            serv.id = servicio.id
            if serv.id == beerExtId.idModelo {
                serv.idExternoGM = beerExtId.idExternoGM
            } else {
                serv.idExternoGM = nil
            }
            Logger.blockSeparator()
            Logger.println(servicio.cantidad ?? "")
            serv.cantidad = servicio.cantidad
            addonsCarga.serviciosAdd.servicios.append(serv)
        }
        if addonsCarga.serviciosAdd.servicios.count != 0 || addonsCarga.productosAdd.productos.count != 0 || addonsCarga.promocionesAdd.promociones.count != 0 {
            existenAddons = true
        } else {
            existenAddons = false
        }
    }

    func noModelado() {
        view.hideLoading()
        mObserver?.cargaOP()
    }

    // MARK: - Envio de Info

    func creaOP() {
        let Venta: ObjetoCreacion = ObjetoCreacion()
        Venta.prospecto = NProspecto(value: RealManager.findFirst(object: NProspecto.self)!)
        Venta.sitio = Sitio(value: RealManager.findFirst(object: Sitio.self)!)
        AES128.shared.setKey(key: statics.key)
        let request: CreaOPRequest = CreaOPRequest()
        Logger.println(Mapper().toJSONString(Venta, prettyPrint: true)!)// sin encriptar
        request.ventaTP = AES128.shared.code(plainText: Mapper().toJSONString(Venta, prettyPrint: true )!)
        request.accion = "Creacion"
        model.creaOP(request: request, Tipo: 0)
    }

    func agregaModeladoOP() {
        let venta: ObjetoModelado = ObjetoModelado()
        venta.planSeleccionadoId = mIdPlan
        AES128.shared.setKey(key: statics.key)
        let request: CreaOPModeladoRequest = CreaOPModeladoRequest()
        Logger.println(Mapper().toJSONString(venta, prettyPrint: true)!)// sin encriptar
        request.ventaTP = AES128.shared.code(plainText: Mapper().toJSONString(venta, prettyPrint: true )!)
        request.idOportunidad = mIdOportunidad
        request.accion = "Modelado"// id del plan puro plan
        model.creaVenta(request: request, Tipo: 1)
    }

    func agregaAddonsOP() {
        AES128.shared.setKey(key: statics.key)
        let venta: ObjetoAdicionales = ObjetoAdicionales()
        venta.informacionAdicionales = addonsCarga
        let request: CreaOPModeladoRequest = CreaOPModeladoRequest()
        request.ventaTP = AES128.shared.code(plainText: Mapper().toJSONString(venta, prettyPrint: true )!)
        request.idOportunidad = mIdOportunidad
        request.accion = "AgregarAdicional"// aqui se envian muchas veces
        Logger.println(Mapper().toJSONString(venta, prettyPrint: true)!)// sin encriptar
        model.creaVenta(request: request, Tipo: 2)
    }

    func realizaFirma() {
        AES128.shared.setKey(key: statics.key)
        let request: CreaOPModeladoRequest = CreaOPModeladoRequest()
        Logger.println(Mapper().toJSONString(mFirma, prettyPrint: true)!)// sin encriptar
        request.ventaTP = AES128.shared.code(plainText: Mapper().toJSONString(mFirma, prettyPrint: true )!)
        request.idOportunidad = mIdOportunidad
        request.accion = "Firma"
        model.creaVenta(request: request, Tipo: 3)
    }

    func consultaOP(idOportunidad: String) {
        let request: ConsultaOportunidadRequest = ConsultaOportunidadRequest()
        request.accion = "ConsultaCreacion"
        request.idOportunidad = idOportunidad
        mIdOportunidad = idOportunidad
        model.consultaOP(request: request, Tipo: 0)
    }

    func ConsultaAddonsOP() {
        let request: ConsultaOportunidadRequest = ConsultaOportunidadRequest()
        request.accion = "ConsultaModelado"
        request.idOportunidad = mIdOportunidad
        model.consultaOP(request: request, Tipo: 1)
    }

    func eliminaAddons(info: InfoModeladoResponse) {
        Logger.println("si jala el comparador")
    }

    func sendModeloMessage(cuenta: String) {
        Alamofire.request(ApiDefinition.API_MODELO_TOKEN, method: .post, parameters: ApiDefinition.bodyModeloToken, encoding: JSONEncoding.default, headers: ApiDefinition.headersModeloToken).responseJSON { [weak self] response in
            guard let strongSelf = self else {return}
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                guard let at = json["access_token"].string else {return}
                ApiDefinition.headersModeloMessage["Authorization"]! += at
                guard let firstName = strongSelf.mProspecto?.firstName,
                      let midleName = strongSelf.mProspecto?.midleName,
                      let lastName = strongSelf.mProspecto?.lastName else {return}
                let name = firstName + " " + midleName + " " + lastName
                let parameters: Parameters = [
                    "ContactKey": strongSelf.cuentaBRM,
                    "Data": [
                        "Nombre": name,
                        "Telefono": strongSelf.mProspecto?.telefono,
                        "Subscriberkey": strongSelf.cuentaBRM
                    ],
                    "EstablishContactKey": true,
                    "EventDefinitionKey": "APIEvent-dfec0582-abd2-3740-7be9-4c6cf0e6a346"
                ]
                Alamofire.request(ApiDefinition.API_MODELO_MESSAGE, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: ApiDefinition.headersModeloMessage).responseJSON { response2 in
                    switch response2.result {
                    case .success(let value):
                        print("Success: Message sent \(value)")

                    case .failure(let error):
                        print("Error: \(error)")
                    }
                }
            case .failure(let error):
                print("FAILURE: \(error)")
            }
        }
    }

    // MARK: - Respuestas:
    func onSuccesCreaVenta(response: CreaOPResponse) {
        mIdOportunidad = response.info.idOportunidad!
        cuentaBRM = response.info.numeroCuenta!
        numeroOportunidad = response.info.numeroOportunidad!
        ValidaOP()
    }

    func onSuccesCreaModelado(response: CreaOPResponse) {
        existeModelado = true
        ValidaOP()
    }

    func onSuccesRegistraFirma(response: CreaOPResponse) {
//        let viewAlert: UIStoryboard = UIStoryboard(name: "CongratulationsViewController", bundle:nil)
//        let viewAlertVC = viewAlert.instantiateViewController(withIdentifier: "CongratulationsViewController")as! CongratulationsViewController
//        viewAlertVC.idOportunidad = mIdOportunidad
//        viewAlertVC.providesPresentationContextTransitionStyle = true
//        viewAlertVC.definesPresentationContext = true
//        viewAlertVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//        viewAlertVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
//        view.present(viewAlertVC, animated: false, completion: {})
//        var mEnvioMarketing: EnvioMarketingRequest = EnvioMarketingRequest()
//        mEnvioMarketing.idRegistro = numeroOportunidad
//        model.EnviaMarketing(request: mEnvioMarketing)

            // MENSAJE MODELO
            if beerExtId.isSelected {
                sendModeloMessage(cuenta: cuentaBRM)
            }
            // MENSAJE MODELO
            let viewAlert: UIStoryboard = UIStoryboard(name: "EndSaleViewController", bundle: nil)
            let viewAlertVC = viewAlert.instantiateViewController(withIdentifier: "EndSaleViewController")as! EndSaleViewController
            viewAlertVC.noCuenta = cuentaBRM
            viewAlertVC.providesPresentationContextTransitionStyle = true
            viewAlertVC.definesPresentationContext = true
            viewAlertVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            viewAlertVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            view.present(viewAlertVC, animated: false, completion: {})
            var mEnvioMarketing: EnvioMarketingRequest = EnvioMarketingRequest()
            mEnvioMarketing.idRegistro = numeroOportunidad
            model.EnviaMarketing(request: mEnvioMarketing)

    }

    func onErrorVenta(message: String) {
        view.hideLoading()
        mObserver?.PrintMessage(message: message)
    }

    func onSuccesAgregaAddons(response: CreaOPResponse) {
        addonsCargados = true
        ValidaOP()
    }

    func onSuccesConsultaOP(response: ConsultaOpResponse) {
        AES128.shared.setKey(key: statics.key)
        let json: String =  AES128.shared.decode(coded: response.infoEncript)!
        let mInfoEncript: InfoEncript = InfoEncript(JSONString: json)!
        mSitio = Sitio()
        mSitio = mInfoEncript.sitio
        mProspecto = mInfoEncript.prospecto
        // mIdPlan = mInfoEncript.
        existeProspecto = true
        setSitio(mSitio!)
        setProspecto(mProspecto!)
        ConsultaAddonsOP()
        try! mRealm.write {
            mPuntosDeControl.idOP = mIdOportunidad
            mPuntosDeControl.existeProspecto =  true
        }
        model.setPuntos(mPuntosDeControl)
    }

    func onSuccesModeladoOP(response: ConsultaOpResponse) {
        mIdPlan = response.infoModelado.idPlanSelecciondado!
        existeModelado = true
        mAddons = InfoAdicionalesCreacion()
        mAddons.serviciosAdd = ServiciosAdd()
        mAddons.productosAdd = ProductosAdd()
        mAddons.promocionesAdd = PromocionesAdd()
        modeladoCargado = response.infoModelado
        for producto in response.infoModelado.productosConsulta.productos {
            let addon = ProductosCreacion()
            addon.id = producto.id
            mAddons.productosAdd!.productos.append(addon)
        }
        for promocion in response.infoModelado.promocionesAdd.promociones {
            let promo = PromocionesCreacion()
            promo.id = promocion.id
            mAddons.promocionesAdd!.promociones.append(promo)
        }
        for servicio in response.infoModelado.serviciosAdd.servicios {
            let serv = ServiciosCreacion()
            serv.cantidad = String("\(servicio.cantidad ?? 1)")
            serv.cantidadInt = servicio.cantidad!
            serv.id = servicio.id
            mAddons.serviciosAdd!.servicios.append(serv)
        }
        if mAddons.serviciosAdd!.servicios.count != 0 || mAddons.promocionesAdd!.promociones.count != 0 || mAddons.productosAdd!.productos.count != 0 {
            existenAddons =  true
            addonsCargados = true
            try! mRealm.write {
                mPuntosDeControl.addonsCargados = true
            }
        }
        try! mRealm.write {
            mPuntosDeControl.existeModelado = existeModelado
            mPuntosDeControl.existenAddons = existenAddons
            mPuntosDeControl.addonsCargados = addonsCargados
        }
        model.setPuntos(mPuntosDeControl)
        let mObjetoModelado: ObjetoModelado = ObjetoModelado()
        mObjetoModelado.planSeleccionadoId = mIdPlan
        model.setObjetoModelado(mObjetoModelado)
        model.setInfoAddons(mAddons)
        view.hideLoading()
        mObserver?.cargaOP()
    }

    func onSuccesDocumentos() {
        LlenaFirma()

    }

    func onSuccesEnvioMarketing() {
        var recomendacion: InfoProfileClientRequest =  InfoProfileClientRequest()
        recomendacion.accion = "Modificacion"
        recomendacion.info?.idPerfilClte = idCliente
        recomendacion.info?.cuentaBRM = cuentaBRM
        model.EnviaIdCliente(request: recomendacion)
    }

    func onSuccesIdPerfil() {
        model.limpieza()
    }

    func onErrorDoc(message: String) {
        view.hideLoading()
        mObserver?.PrintMessage(message: message)
    }

    func Limpieza() {
        viewWillDissapear()
        model.limpieza()
    }

    func LlenaFirma() {
        // direccion de facturacion
        mFirma.direccionFacturacion.calle = mProspecto?.calle ?? ""
        mFirma.direccionFacturacion.estado = mProspecto?.estado ?? ""
        mFirma.direccionFacturacion.mismaDirInst = true
        mFirma.direccionFacturacion.ciudad = mProspecto?.ciudad ?? ""
        mFirma.direccionFacturacion.delegacionMunicipio = mProspecto?.delegacionMunicipio ??  ""
        mFirma.direccionFacturacion.numExterior = mProspecto?.numExterior ??  ""
        mFirma.direccionFacturacion.numInterior = mProspecto?.numInterior ??  ""
        mFirma.direccionFacturacion.colonia = mProspecto?.colonia ?? ""
        mFirma.direccionFacturacion.codigoPostalFacturacion = mProspecto?.codigoPostal ?? ""

        // metodo de pago
        mFirma.metodoPago.metodoPago = "Efectivo"

        // datos adicionales
        mFirma.datosAdicionales.tipoIdentificacion = mDataIne.tipo?.uppercased() ?? ""
        mFirma.datosAdicionales.identificacionOficial = mDataIne.folio ?? ""

        realizaFirma()
    }

    // MARK: - a base:
    func setProspecto(_ prospecto: NProspecto) {
        model.setProspecto(prospecto)
    }

    func getProspecto() -> NProspecto {
        return model.getProspecto()
    }

    func setSitio(_ sitio: Sitio) {
        model.setSitio(sitio)
    }

    func getSitio() -> Sitio {
        return model.getSitio()
    }

    func getStep2() -> Bool {
        return step2
    }

    func getPlan() -> String {
        return mIdPlan
    }

}
