//
//  reportsReponse.swift
//  SalesCloud
//
//  Created by mhuertao on 7/02/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class reportsReponse: BaseResponse {
    var datos: dataReports?
    var mResult: String = ""
    var mResultDescription: String?
    required init?(map: Map) {

    }

    override func mapping(map: Map) {
        datos <- map["datos"]
        mResult <- map["result"]
        mResultDescription <- map["resultDescription"]
    }

}

class dataReports: NSObject, Mappable {
    var colaboradores: [Colaboradores] = []

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        colaboradores <- map["colaboradores"]
    }

    override init() {
    }

}

class Colaboradores: NSObject, Mappable {
    var ventas: Ventas?
    var ordenesDeServicio: OrdenesServicio?
    var noEmpleado: String = ""
    var name: String = ""
    var mesaControl: MesaControl?

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        ventas <- map["ventas"]
        ordenesDeServicio <- map["ordenesDeServicio"]
        noEmpleado <- map["noEmpleado"]
        name <- map["name"]
        mesaControl <- map["mesaControl"]
    }

    override init() {
    }

}

class Ventas: NSObject, Mappable {
    var sumaVentas: Int = 0
    var sumaRechazadas: Int = 0
    var sumaPendientes: Int = 0
    var sumaAprobadas: Int = 0
    var rechazadas: [DetalleVenta] = []
    var pendientes: [DetalleVenta] = []
    var aprobadas: [DetalleVenta] = []

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        sumaVentas <- map["sumaVentas"]
        sumaRechazadas <- map["sumaRechazadas"]
        sumaPendientes <- map["sumaPendientes"]
        sumaAprobadas <- map["sumaAprobadas"]
        rechazadas <- map["rechazadas"]
        pendientes <- map["pendientes"]
        aprobadas <- map["aprobadas"]
    }

    override init() {
    }

}

class DetalleVenta: NSObject, Mappable {
    var numOp: String?
    var motivo: String?
    var idventa: String?
    var flagMotivo: Bool?
    var flagComentario: Bool?
    var fecha: String?
    var empleado: String?
    var comentario: String = ""

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        numOp <- map["numOp"]
        motivo <- map["motivo"]
        idventa <- map["idventa"]
        flagMotivo <- map["flagMotivo"]
        flagComentario <- map["flagComentario"]
        fecha <- map["fecha"]
        empleado <- map["empleado"]
        comentario <- map["comentario"]
    }

    override init() {
    }

}

class OrdenesServicio: NSObject, Mappable {
    var sumaRescate: Int = 0
    var sumaInstaladas: Int = 0
    var sumaConfirmado: Int = 0
    var sumaCompletado: Int = 0
    var sumaCancelado: Int = 0
    var sumaAgendado: Int = 0
    var rescate: [DetalleVenta] = []
    var confirmado: [DetalleVenta] = []
    var completado: [DetalleVenta] = []
    var cancelado: [DetalleVenta] = []
    var agendado: [DetalleVenta] = []

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        sumaRescate <- map["sumaRescate"]
        sumaInstaladas <- map["sumaInstaladas"]
        sumaConfirmado <- map["sumaConfirmado"]
        sumaCompletado <- map["sumaCompletado"]
        sumaCancelado <- map["sumaCancelado"]
        sumaAgendado <- map["sumaAgendado"]
        rescate <- map["rescate"]
        confirmado <- map["confirmado"]
        completado <- map["completado"]
        cancelado <- map["cancelado"]
        agendado <- map["agendado"]
    }

    override init() {
    }

}

class MesaControl: NSObject, Mappable {
    var sumaVentas: Int = 0
    var sumaRechazadas: Int = 0
    var sumaPendientes: Int = 0
    var sumaAprobadas: Int = 0
    var rechazadasMC: [DetalleVentaControl] = []
    var pendientesMC: [DetalleVentaControl] = []
    var aprobadasMC: [DetalleVentaControl] = []

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        sumaVentas <- map["sumaVentas"]
        sumaRechazadas <- map["sumaRechazadas"]
        sumaPendientes <- map["sumaPendientes"]
        sumaAprobadas <- map["sumaAprobadas"]
        rechazadasMC <- map["rechazadasMC"]
        pendientesMC <- map["pendientesMC"]
        aprobadasMC <- map["aprobadasMC"]
    }

    override init() {
    }

}

class DetalleVentaControl: NSObject, Mappable {
    var motivo: String?
    var idventa: String?
    var flagMotivo: Int = 0
    var fecha: String?
    var estatusMC: String?
    var comentario: String?
    var isHiddeSubTitle: Bool = true

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        motivo <- map["motivo"]
        idventa <- map["idventa"]
        flagMotivo <- map["flagMotivo"]
        estatusMC  <- map["estatusMC"]
        fecha <- map["fecha"]
        comentario <- map["comentario"]
    }

    override init() {
    }

}
