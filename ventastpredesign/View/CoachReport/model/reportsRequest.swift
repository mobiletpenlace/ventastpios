//
//  reportsRequest.swift
//  SalesCloud
//
//  Created by mhuertao on 7/02/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

public class reportsRequest: BaseRequest {
    var numEmpleado: String = ""
    var strFechaFin: String = ""
    var strFechaInicio: String = ""

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override public func mapping(map: Map) {
        numEmpleado <- map["numEmpleado"]
        strFechaFin <- map["strFechaFin"]
        strFechaInicio <- map["strFechaInicio"]

    }
}
