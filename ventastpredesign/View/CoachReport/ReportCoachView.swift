//
//  ReportCoachView.swift
//  SalesCloud
//
//  Created by mhuertao on 18/05/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class ReportCoachView: BaseVentasView {
    @IBOutlet weak var barVentas: UIView!
    @IBOutlet weak var barMesaControl: UIView!
    @IBOutlet weak var barProcesoInstalacion: UIView!
    @IBOutlet weak var barInstaladas: UIView!
    @IBOutlet weak var searchTextfield: UITextField!
    @IBOutlet weak var rightConstantView: NSLayoutConstraint!
    @IBOutlet weak var firstContainerView: UIView!
    @IBOutlet weak var secondContainerView: UIView!
    @IBOutlet weak var textDatesLabel: UITextField!
    @IBOutlet weak var viewSearch: UIView!
    var reportsViewModel: reportsCoachViewModel!
    var searchOpen = false
    var currentStartDate: String = ""
    var currentFinishDate: String = ""
    var currentTabBar: Int = 0
    var colaboradores: [Colaboradores] = []
    var isVisibleContainerPri = true
    var currentColaborador: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        reportsViewModel = reportsCoachViewModel(view: self)
        reportsViewModel.atachView(observer: self)
        setUpView()
    }

    @IBAction func goBack(_ sender: Any) {
        if isVisibleContainerPri {
            self.dismiss(animated: false)
        } else {
           hiddenContainerPrincipal(isHidden: false)
            colaboradores = reportsViewModel.colaboradores
            self.currentColaborador  = ""
            goSubMenu(id: currentTabBar)
            self.filtrar(filtro: "")
        }

    }

    @IBAction func selectMenuButton(_ sender: UIButton) {
        currentTabBar = sender.tag // borrar
        hiddenContainerPrincipal(isHidden: false)
        selectBarMenu(id: sender.tag)
        goSubMenu(id: sender.tag)
        filtrar(filtro: "")
    }

    @IBAction func goFiltersDialog(_ sender: Any) {
        let viewAlert: UIStoryboard = UIStoryboard(name: "DialogDatesView", bundle: nil)
        let controller = viewAlert.instantiateViewController(withIdentifier: "DialogDatesView")as! DialogDatesView
        controller.dialogDateDelegate = self
        controller.providesPresentationContextTransitionStyle = true
        controller.definesPresentationContext = true
        controller.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(controller, animated: false, completion: {})
    }

    @IBAction func goCalendar(_ sender: Any) {
        let viewAlert: UIStoryboard = UIStoryboard(name: "DialogCalendarView", bundle: nil)
        let controller = viewAlert.instantiateViewController(withIdentifier: "DialogCalendarView")as! DialogCalendarView
        controller.setDateCalendarDelegate = self
        self.present(controller, animated: false, completion: {})
    }

    @IBAction func goSearch(_ sender: Any) {
        if !searchOpen {
            openSearch(open: true)
        }
    }

    @IBAction func goClosedSearch(_ sender: Any) {
        openSearch(open: false)
        searchTextfield.text = ""
        filtrar(filtro: "")
    }

    func setUpView() {
        hiddenContainerPrincipal(isHidden: false)
        selectBarMenu(id: currentTabBar)
        searchTextfield.text = ""
        textDatesLabel.text = ""
        initDateDefault()
        consultReports()
        searchTextfield.addTarget(self, action: #selector(textFieldEditingDidChange(_:)), for: UIControlEvents.editingChanged)
    }

    @objc func textFieldEditingDidChange(_ sender: UITextField) {
        filtrar(filtro: sender.text ?? "")
    }

    func filtrar(filtro: String) {
        switch currentTabBar {
        case 0:
            SwiftEventBus.post("SearchListVentas", sender: filtro )
        case 1:
            SwiftEventBus.post("SearchListMesa", sender: filtro )
        case 2:
            SwiftEventBus.post("SearchListProceso", sender: filtro )
        case 3:
            SwiftEventBus.post("SearchListInstalada", sender: filtro )
        default:
            break
        }

    }

    func offBarMenu() {
        barVentas.backgroundColor = UIColor.init(named: "base_gray_lines")
        barMesaControl.backgroundColor = UIColor.init(named: "base_gray_lines")
        barProcesoInstalacion.backgroundColor = UIColor.init(named: "base_gray_lines")
        barInstaladas.backgroundColor = UIColor.init(named: "base_gray_lines")
    }

    func onBarMenu(view: UIView) {
        offBarMenu()
        view.backgroundColor = UIColor.init(named: "Base_purple_names")
    }

    func selectBarMenu(id: Int) {
        switch id {
        case 0:
            currentTabBar = 0
            onBarMenu(view: barVentas)
        case 1:
            currentTabBar = 1
            onBarMenu(view: barMesaControl)
        case 2:
            currentTabBar = 2
            onBarMenu(view: barProcesoInstalacion)
        case 3:
            currentTabBar = 3
            onBarMenu(view: barInstaladas)
        default:
            break
        }
    }

    func goSubMenu(id: Int) {
        switch id {
        case 0:
            ViewEmbedder.embed(withIdentifier: "VentasReportView", parent: self, container: self.firstContainerView) { (vc) in
                let ventas = vc as! VentasReportView
                ventas.getDetailDelegate = self
                ventas.colaboradores = self.colaboradores
            }
        case 1:
            ViewEmbedder.embed(withIdentifier: "MesaControlView", parent: self, container: self.firstContainerView) { (vc) in
                let mesa = vc as! MesaControlView
                mesa.getDetailDelegate = self
                mesa.colaboradores = self.colaboradores
            }
        case 2:
            ViewEmbedder.embed(withIdentifier: "ProcesoInstalacionView", parent: self, container: self.firstContainerView) { (vc) in
                let mesa = vc as! ProcesoInstalacionView
                mesa.getDetailDelegate = self
                mesa.colaboradores = self.colaboradores
            }
        case 3:
            ViewEmbedder.embed(withIdentifier: "InstaladasReportsView", parent: self, container: self.firstContainerView) { (vc) in
                let ventas = vc as! InstaladasReportsView
                ventas.getDetailDelegate = self
                ventas.colaboradores = self.colaboradores
            }
        default:
            break
        }
    }

    func goDetailMenu(id: Int) {
        switch id {
        case 0:
            guard let colaborador = getColaborador(numEmploye: currentColaborador) else {return}
            ViewEmbedder.embed(withIdentifier: "ListaOportunidadesView", parent: self, container: self.secondContainerView) { (vc) in
                let oport = vc as! ListaOportunidadesView
                oport.colaborador = colaborador
                oport.nombre.text = colaborador.name
                let  sum: Int = colaborador.ventas?.sumaVentas ?? 0
                oport.totalVentasLabel.text = String(sum)
               // oport.totalVentasLabel.set(text: "Total de ventas:")
            }
        case 1:
            guard let colaborador = getColaborador(numEmploye: currentColaborador) else {return}
            ViewEmbedder.embed(withIdentifier: "DetalleMesaControlView", parent: self, container: self.secondContainerView) { (vc) in
                let oport = vc as! DetalleMesaControlView
                let detalleSubtitle = DetalleVentaControl()
                var detalleArrSub: [DetalleVentaControl] = []
                detalleArrSub.append(detalleSubtitle)
                let arrApro = (colaborador.mesaControl?.aprobadasMC ?? [])
                let arrPen = (colaborador.mesaControl?.pendientesMC ?? [])
                let arrRech = (colaborador.mesaControl?.rechazadasMC ?? [])
                oport.detalleMesa = detalleArrSub + arrApro + detalleArrSub + arrPen + detalleArrSub + arrRech
                oport.colaborador = colaborador
            }
        case 2:
            guard let colaborador = getColaborador(numEmploye: currentColaborador) else {return}
            ViewEmbedder.embed(withIdentifier: "DetalleProcesoInsView", parent: self, container: self.secondContainerView) { (vc) in
                let oport = vc as! DetalleProcesoInsView
                let detalleSubtitle = DetalleVenta()
                var detalleArrSub: [DetalleVenta] = []
                detalleArrSub.append(detalleSubtitle)
                let arrAgen = (colaborador.ordenesDeServicio?.agendado ?? [])
                let arrConf = (colaborador.ordenesDeServicio?.confirmado ?? [])
                let arrComp = (colaborador.ordenesDeServicio?.completado ?? [])
                let arrRes = (colaborador.ordenesDeServicio?.rescate ?? [])
                let arrCanc = (colaborador.ordenesDeServicio?.cancelado ?? [])
                oport.detalleMesa = detalleArrSub + arrAgen + detalleArrSub + arrConf + detalleArrSub + arrComp + detalleArrSub + arrRes + detalleArrSub + arrCanc
                oport.colaborador = colaborador
            }
        case 3:
            guard let colaborador = getColaborador(numEmploye: currentColaborador) else {return}
            ViewEmbedder.embed(withIdentifier: "DetalleInstaladasView", parent: self, container: self.secondContainerView) { (vc) in
                let oport = vc as! DetalleInstaladasView
                oport.colaborador = colaborador
                oport.nombre.text = colaborador.name
                let  sum: Int = colaborador.ordenesDeServicio?.sumaInstaladas ?? 0
                oport.totalVentasLabel.text = String(sum)
               // oport.totalVentasLabel.set(text: "Total de instaladas:")
            }
        default:
            break
        }
    }

    func openSearch(open: Bool) {
        self.searchOpen = open
        UIView.animate(withDuration: 1, animations: { () -> Void in
            self.rightConstantView.constant = open == true ?  UIScreen.main.bounds.width : 60
        })
    }

    func initDateDefault() {
        let id = "thisWeek"
        let (startDay, finishDay) = reportsViewModel.getPredefinedDate(id: id)
        textDatesLabel.text = reportsViewModel.getTextDate(id: id, startDate: startDay, finishDate: finishDay)
        currentStartDate = startDay
        currentFinishDate = finishDay
    }

    func consultReports() {
        reportsViewModel.consultReports(dateStart: currentStartDate, dateFinish: currentFinishDate, numEmploye: reportsViewModel.getNumEmployee())
    }

    func hiddenContainerPrincipal(isHidden: Bool) {
        if isHidden {
            firstContainerView.isHidden = true
            secondContainerView.isHidden = false
            isVisibleContainerPri = false
            viewSearch.isHidden = true
        } else {
            firstContainerView.isHidden = false
            secondContainerView.isHidden = true
            isVisibleContainerPri = true
            viewSearch.isHidden = false
        }
    }

    func getColaborador(numEmploye: String) -> Colaboradores? {
        if let user = colaboradores.first(where: {$0.noEmpleado == numEmploye}) {
            return user
        }
        return nil
    }

}

extension ReportCoachView: DialogDate {
    func selectDateDialog(startDate: String, finishDate: String, id: String) {
        print( "fecha inicio \(startDate) , fecha final \(finishDate) , id \(id)" )
        textDatesLabel.text = reportsViewModel.getTextDate(id: id, startDate: startDate, finishDate: finishDate)
        currentStartDate = startDate
        currentFinishDate = finishDate
        consultReports()
    }
}

extension ReportCoachView: setDateCalendarDelegate {
    func getDate(dateStart: String, dateFinish: String) {
        print( "fecha inicio \(dateStart) , fecha final \(dateFinish)" )
        textDatesLabel.text = reportsViewModel.getTextDate(id: "other", startDate: dateStart, finishDate: dateFinish)
        currentStartDate = dateStart
        currentFinishDate = dateFinish
        consultReports()
    }
}

extension ReportCoachView: reportsCoachObserver {
    func onError(errorMessage: String) {
        Constants.Alert(title: "", body: errorMessage, type: type.Error, viewC: self)
    }

    func searchTable(filtro: String, idTab: Int) {}

    func succesReports() {
        print("listo")
        colaboradores = reportsViewModel.colaboradores
        if isVisibleContainerPri {
            hiddenContainerPrincipal(isHidden: false)
            goSubMenu(id: currentTabBar)
        } else {
            goSubMenu(id: currentTabBar)
            hiddenContainerPrincipal(isHidden: true)
            goDetailMenu(id: currentTabBar)
        }
        self.filtrar(filtro: "")

    }

}

extension ReportCoachView: getDetalleVentaDelegate, getDetalleMesaDelegate, getDetalleInstaladaDelegate, getDetalleProcesoDelegate {

    func detailUser(numEmployee: String) {
        hiddenContainerPrincipal(isHidden: true)
        self.currentColaborador = numEmployee
        goDetailMenu(id: currentTabBar)
    }

    func detailUserMesa(numEmployee: String) {
        hiddenContainerPrincipal(isHidden: true)
        self.currentColaborador = numEmployee
        goDetailMenu(id: currentTabBar)
    }

    func detailUserInst(numEmployee: String) {
        hiddenContainerPrincipal(isHidden: true)
        self.currentColaborador = numEmployee
        goDetailMenu(id: currentTabBar)
    }

    func detailUserProc(numEmployee: String) {
        hiddenContainerPrincipal(isHidden: true)
        self.currentColaborador = numEmployee
        goDetailMenu(id: currentTabBar)
    }
}
