//
//  ListaOportunidadesView.swift
//  SalesCloud
//
//  Created by mhuertao on 9/02/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

class ListaOportunidadesView: UIViewController {

    @IBOutlet weak var nombre: UILabel!
    @IBOutlet weak var OportunidadTable: UITableView!
    @IBOutlet weak var totalVentasLabel: UILabel!
    var colaborador: Colaboradores?

    override func viewDidLoad() {
        super.viewDidLoad()

    }

}
extension ListaOportunidadesView: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return colaborador?.ventas?.sumaVentas ?? 0
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListaOportunidadesTableViewCell", for: indexPath) as! ListaOportunidadesTableViewCell
        cell.numeroOportunidad.text = colaborador?.ventas?.aprobadas[indexPath.row].idventa
        return cell
    }

}
