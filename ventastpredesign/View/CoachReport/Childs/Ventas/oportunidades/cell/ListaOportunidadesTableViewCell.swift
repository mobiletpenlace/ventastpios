//
//  ListaOportunidadesTableViewCell.swift
//  SalesCloud
//
//  Created by mhuertao on 11/02/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

class ListaOportunidadesTableViewCell: UITableViewCell {
    @IBOutlet weak var numeroOportunidad: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
