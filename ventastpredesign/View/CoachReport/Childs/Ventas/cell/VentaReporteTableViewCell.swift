//
//  VentaReporteTableViewCell.swift
//  SalesCloud
//
//  Created by mhuertao on 9/02/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

class VentaReporteTableViewCell: UITableViewCell {
    @IBOutlet weak var nombreEmpleado: UILabel!
    @IBOutlet weak var numVentas: UILabel!
    @IBOutlet weak var detailButton: UIButton!
    @IBOutlet weak var widthNumVent: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
