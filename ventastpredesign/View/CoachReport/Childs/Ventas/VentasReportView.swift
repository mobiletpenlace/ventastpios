//
//  VentasReportView.swift
//  SalesCloud
//
//  Created by mhuertao on 9/02/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit
protocol getDetalleVentaDelegate {
    func detailUser(numEmployee: String)
}

class VentasReportView: BaseVentasView {

    @IBOutlet weak var sumTotalLabel: UILabel!
    @IBOutlet weak var VentasTable: UITableView!
    var reportsViewModel: reportsCoachViewModel!
    var getDetailDelegate: getDetalleVentaDelegate?
    var colaboradores: [Colaboradores] = []
    var listFiltro: [Colaboradores] = []
    var filtro: String = ""
    var sumTotal = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        reportsViewModel = reportsCoachViewModel(view: self)
        reportsViewModel.atachView(observer: self)
        filtrar(filtro: "")
        sumTotalLabel.text = String(sumTotal)
    }

    override func viewDidAppear(_ animated: Bool) {
        VentasTable.reloadData()
    }

    func filtrar(filtro: String) {
        let filtroUpper = filtro.uppercased()
        listFiltro = colaboradores.filter { dato in
            return  dato.name.uppercased().contains(filtroUpper) || dato.noEmpleado.uppercased().contains(filtroUpper) ||
            filtroUpper.count == 0
        }
        self.VentasTable?.reloadData()
        getSumTotal()
    }

    func getSumTotal() {
        let suma = listFiltro.map { $0.ventas?.sumaVentas }
        let sumArray = suma.reduce(0) { (sum, num) -> Int in
            sum + (num ?? 0)
        }
        sumTotalLabel.text = String(sumArray)

    }

}

extension VentasReportView: reportsCoachObserver {
    func onError(errorMessage: String) {}

    func succesReports() {}

    func searchTable(filtro: String, idTab: Int) {
        if idTab == 0 {
            self.filtro = filtro
            filtrar(filtro: filtro)
        }
    }
}

extension VentasReportView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listFiltro.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 92
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VentaReporteTableViewCell", for: indexPath) as! VentaReporteTableViewCell
        cell.nombreEmpleado.text = listFiltro[indexPath.row].name
        let numVentas: String = String(listFiltro[indexPath.row].ventas?.sumaVentas ?? 0)
        cell.numVentas.text = numVentas
        cell.widthNumVent.constant = CGFloat(10 + (numVentas.count * 10))
        cell.detailButton.tag = indexPath.row
        cell.detailButton.addTarget(self, action: #selector(handleButtonTapped(sender: )), for: .touchUpInside)
        return cell
    }

    @objc func handleButtonTapped(sender: UIButton) {
        let numVentas =  String(listFiltro[sender.tag].ventas?.sumaVentas ?? 0)
        if numVentas == "0" {
            AlertDialog.show(title: "Aviso", body: "No hay información de detalle", view: self)
        } else {
            self.getDetailDelegate?.detailUser(numEmployee: listFiltro[sender.tag].noEmpleado )
        }

    }

}
