//
//  comentarioView.swift
//  SalesCloud
//
//  Created by mhuertao on 13/06/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

class comentarioView: UIViewController {
    @IBOutlet weak var numOport: UILabel!
    @IBOutlet weak var textComentario: UITextView!
    var venta: DetalleVenta?

    override func viewDidLoad() {
        super.viewDidLoad()
        numOport.text = venta?.idventa ?? ""
        textComentario.text = venta?.comentario ?? ""
    }

    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: false)
    }

}
