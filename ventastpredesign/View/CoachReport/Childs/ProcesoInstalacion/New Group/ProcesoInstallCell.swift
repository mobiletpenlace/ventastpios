//
//  ProcesoInstallCell.swift
//  SalesCloud
//
//  Created by mhuertao on 13/06/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

class ProcesoInstallCell: UITableViewCell {

    @IBOutlet weak var nombreEmpleado: UILabel!
    @IBOutlet weak var numAgendado: UILabel!
    @IBOutlet weak var numConfirmado: UILabel!
    @IBOutlet weak var numCompletado: UILabel!
    @IBOutlet weak var numRescate: UILabel!
    @IBOutlet weak var numCancelado: UILabel!
    @IBOutlet weak var detalleVenta: UIButton!
    @IBOutlet weak var numAgendConstraint: NSLayoutConstraint!
    @IBOutlet weak var numConfConstraint: NSLayoutConstraint!
    @IBOutlet weak var numCompConstraint: NSLayoutConstraint!
    @IBOutlet weak var numRescConstraint: NSLayoutConstraint!
    @IBOutlet weak var numCancConstraint: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
