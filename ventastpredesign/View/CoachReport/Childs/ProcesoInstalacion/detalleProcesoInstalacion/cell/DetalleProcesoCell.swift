//
//  DetalleProcesoCell.swift
//  SalesCloud
//
//  Created by mhuertao on 13/06/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

class DetalleProcesoCell: UITableViewCell {
    @IBOutlet weak var numOportunidad: UILabel!
    @IBOutlet weak var fechaCreacion: UILabel!
    @IBOutlet weak var numComentario: UILabel!
    @IBOutlet weak var comentarioButton: UIButton!

    @IBOutlet weak var comentarioImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
