//
//  DetalleProcesoInsView.swift
//  SalesCloud
//
//  Created by mhuertao on 13/06/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

class DetalleProcesoInsView: UIViewController {
    @IBOutlet weak var ListaDetalleTable: UITableView!
    @IBOutlet weak var numAgendConstraint: NSLayoutConstraint!
    @IBOutlet weak var numConfConstraint: NSLayoutConstraint!
    @IBOutlet weak var numCompConstraint: NSLayoutConstraint!
    @IBOutlet weak var numRescConstraint: NSLayoutConstraint!
    @IBOutlet weak var numCancConstraint: NSLayoutConstraint!
    @IBOutlet weak var numAgendado: UILabel!
    @IBOutlet weak var numConfirmado: UILabel!
    @IBOutlet weak var numCompletado: UILabel!
    @IBOutlet weak var numRescate: UILabel!
    @IBOutlet weak var numCancelado: UILabel!
    var detalleMesa: [DetalleVenta] = []
    var colaborador: Colaboradores?
    var sumTotalAgen = 0
    var sumTotalConf = 0
    var sumTotalComp = 0
    var sumTotalRes = 0
    var sumTotalCanc = 0

    override func viewDidLoad() {
        super.viewDidLoad()

    }

     override func viewDidAppear(_ animated: Bool) {
         setUpView()
    }

    func setUpView() {
        sumTotalAgen = colaborador?.ordenesDeServicio?.sumaAgendado ?? 0
        sumTotalConf = colaborador?.ordenesDeServicio?.sumaConfirmado ?? 0
        sumTotalComp = colaborador?.ordenesDeServicio?.sumaCompletado ?? 0
        sumTotalRes = colaborador?.ordenesDeServicio?.sumaRescate ?? 0
        sumTotalCanc = colaborador?.ordenesDeServicio?.sumaCancelado ?? 0

        numAgendado.text = String(sumTotalAgen)
        numConfirmado.text = String(sumTotalConf)
        numCompletado.text = String(sumTotalComp)
        numRescate.text = String(sumTotalRes)
        numCancelado.text = String(sumTotalCanc)

        numAgendConstraint.constant = CGFloat(10 + (String(sumTotalAgen).count * 10))
        numConfConstraint.constant = CGFloat(10 + (String(sumTotalConf).count * 10))
        numCompConstraint.constant = CGFloat(10 + (String(sumTotalComp).count * 10))
        numRescConstraint.constant = CGFloat(10 + (String(sumTotalRes).count * 10))
        numCancConstraint.constant = CGFloat(10 + (String(sumTotalCanc).count * 10))
    }

}

extension DetalleProcesoInsView: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return detalleMesa.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        let agendadoCount = colaborador?.ordenesDeServicio?.agendado.count ?? 0
        let confirmadoCount = colaborador?.ordenesDeServicio?.confirmado.count ?? 0
        let completadoCount = colaborador?.ordenesDeServicio?.completado.count ?? 0
        let rescateCount = colaborador?.ordenesDeServicio?.rescate.count ?? 0

        let indiceAgendado = 0
        let indiceConfirmado = agendadoCount + 1
        let indiceCompletado = agendadoCount + confirmadoCount + 2
        let indiceRescate = agendadoCount + confirmadoCount + completadoCount + 3
        let indiceFinal = agendadoCount + confirmadoCount + completadoCount + rescateCount + 4

        if indexPath.row == indiceAgendado ||
            indexPath.row == indiceConfirmado ||
            indexPath.row == indiceCompletado ||
            indexPath.row == indiceRescate ||
            indexPath.row == indiceFinal {
            return 40
        } else {
            return 150
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let agendadoCount = colaborador?.ordenesDeServicio?.agendado.count ?? 0
        let confirmadoCount = colaborador?.ordenesDeServicio?.confirmado.count ?? 0
        let completadoCount = colaborador?.ordenesDeServicio?.completado.count ?? 0
        let rescateCount = colaborador?.ordenesDeServicio?.rescate.count ?? 0
        let canceladoCount = colaborador?.ordenesDeServicio?.cancelado.count ?? 0


        let indiceAgendado = 0
        let indiceConfirmado = agendadoCount + 1
        let indiceCompletado = indiceConfirmado + confirmadoCount + 1
        let indiceRescate = indiceCompletado + completadoCount + 1
        let indiceCancelado = indiceRescate + rescateCount + 1

        switch indexPath.row {
        case indiceAgendado:
            return createSubTitleCell(for: tableView, indexPath: indexPath, title: "Agendado", count: agendadoCount)
        case indiceConfirmado:
            return createSubTitleCell(for: tableView, indexPath: indexPath, title: "Confirmado", count: confirmadoCount)
        case indiceCompletado:
            return createSubTitleCell(for: tableView, indexPath: indexPath, title: "Completado", count: completadoCount)
        case indiceRescate:
            return createSubTitleCell(for: tableView, indexPath: indexPath, title: "Rescatado", count: rescateCount)
        case indiceCancelado:
            return createSubTitleCell(for: tableView, indexPath: indexPath, title: "Cancelado", count: canceladoCount)
        default:
            return createDetalleProcesoCell(for: tableView, indexPath: indexPath)
        }
    }

    private func createSubTitleCell(for tableView: UITableView, indexPath: IndexPath, title: String, count: Int) -> SubTitleCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SubTitleCell", for: indexPath) as! SubTitleCell
        cell.subTitleLabel.text = title
        cell.number.text = String(count)
        return cell
    }

    private func createDetalleProcesoCell(for tableView: UITableView, indexPath: IndexPath) -> DetalleProcesoCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetalleProcesoCell", for: indexPath) as! DetalleProcesoCell
        let detalle = detalleMesa[indexPath.row]

        cell.numOportunidad.text = detalle.idventa
        if let fecha = detalle.fecha {
            cell.fechaCreacion.text = fecha.removeBackslash()
        }
        cell.numComentario.text = detalle.comentario.isEmpty ? "0" : "1"
        cell.comentarioButton.isEnabled = !detalle.comentario.isEmpty
        cell.comentarioImage.isHidden = detalle.comentario.isEmpty
        cell.comentarioButton.tag = indexPath.row
        cell.comentarioButton.addTarget(self, action: #selector(handleButtonTapped(sender:)), for: .touchUpInside)

        return cell
    }

    @objc func handleButtonTapped(sender: UIButton) {

        let controller: comentarioView = UIStoryboard(name: "comentarioView", bundle: nil).instantiateViewController(withIdentifier: "comentarioView") as! comentarioView
        controller.venta = detalleMesa[sender.tag]
        controller.providesPresentationContextTransitionStyle = true
        controller.definesPresentationContext = true
        controller.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(controller, animated: false, completion: nil)
    }
}
