//
//  ProcesoInstalacionView.swift
//  SalesCloud
//
//  Created by mhuertao on 14/02/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit
protocol getDetalleProcesoDelegate {
    func detailUserProc(numEmployee: String)
}

class ProcesoInstalacionView: BaseVentasView {
    @IBOutlet weak var VentasTable: UITableView!
    @IBOutlet weak var numAgendConstraint: NSLayoutConstraint!
    @IBOutlet weak var numConfConstraint: NSLayoutConstraint!
    @IBOutlet weak var numCompConstraint: NSLayoutConstraint!
    @IBOutlet weak var numRescConstraint: NSLayoutConstraint!
    @IBOutlet weak var numCancConstraint: NSLayoutConstraint!
    @IBOutlet weak var numAgendado: UILabel!
    @IBOutlet weak var numConfirmado: UILabel!
    @IBOutlet weak var numCompletado: UILabel!
    @IBOutlet weak var numRescate: UILabel!
    @IBOutlet weak var numCancelado: UILabel!
    var reportsViewModel: reportsCoachViewModel!
    var getDetailDelegate: getDetalleProcesoDelegate?
    var colaboradores: [Colaboradores] = []
    var listFiltro: [Colaboradores] = []
    var filtro: String = ""
    var sumTotalAgen = 0
    var sumTotalConf = 0
    var sumTotalComp = 0
    var sumTotalRes = 0
    var sumTotalCanc = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        reportsViewModel = reportsCoachViewModel(view: self)
        reportsViewModel.atachView(observer: self)
        setUpview()
    }

    func setUpview() {
        filtrar(filtro: "")
    }

    override func viewDidAppear(_ animated: Bool) {
        VentasTable.reloadData()
    }

    func filtrar(filtro: String) {
        let filtroUpper = filtro.uppercased()
        listFiltro = colaboradores.filter { dato in
            return  dato.name.uppercased().contains(filtroUpper) || dato.noEmpleado.uppercased().contains(filtroUpper) ||
            filtroUpper.count == 0
        }
        self.VentasTable?.reloadData()
        getSumTotal()
    }

    func getSumTotal() {
        let sumaA = listFiltro.map { $0.ordenesDeServicio?.sumaAgendado }
        let sumAgendada = sumaA.reduce(0) { (sum, num) -> Int in
            sum + (num ?? 0)
        }

        let sumaC = listFiltro.map { $0.ordenesDeServicio?.sumaConfirmado }
        let sumConfirm = sumaC.reduce(0) { (sum, num) -> Int in
            sum + (num ?? 0)
        }

        let sumaCo = listFiltro.map { $0.ordenesDeServicio?.sumaCompletado }
        let sumComplete = sumaCo.reduce(0) { (sum, num) -> Int in
            sum + (num ?? 0)
        }

        let sumaR = listFiltro.map { $0.ordenesDeServicio?.sumaRescate }
        let sumRescate = sumaR.reduce(0) { (sum, num) -> Int in
            sum + (num ?? 0)
        }

        let sumaCa = listFiltro.map { $0.ordenesDeServicio?.sumaCancelado }
        let sumCancel = sumaCa.reduce(0) { (sum, num) -> Int in
            sum + (num ?? 0)
        }

        numAgendado.text = String(sumAgendada)
        numConfirmado.text = String(sumConfirm)
        numCompletado.text = String(sumComplete)
        numRescate.text = String(sumRescate)
        numCancelado.text = String(sumCancel)

        numAgendConstraint.constant = CGFloat(10 + (String(sumAgendada).count * 10))
        numConfConstraint.constant = CGFloat(10 + (String(sumConfirm).count * 10))
        numCompConstraint.constant = CGFloat(10 + (String(sumComplete).count * 10))
        numRescConstraint.constant = CGFloat(10 + (String(sumRescate).count * 10))
        numCancConstraint.constant = CGFloat(10 + (String(sumCancel).count * 10))
    }

}

extension ProcesoInstalacionView: reportsCoachObserver {
    func onError(errorMessage: String) {}

    func succesReports() {}

    func searchTable(filtro: String, idTab: Int) {
        if idTab == 2 {
            self.filtro = filtro
            filtrar(filtro: filtro)
        }
    }
}

extension ProcesoInstalacionView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listFiltro.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProcesoInstallCell", for: indexPath) as! ProcesoInstallCell
        cell.nombreEmpleado.text = listFiltro[indexPath.row].name
        let numAge: String = String(listFiltro[indexPath.row].ordenesDeServicio?.sumaAgendado ?? 0)
        cell.numAgendado.text = numAge
        let numConf: String = String(listFiltro[indexPath.row].ordenesDeServicio?.sumaConfirmado ?? 0)
        cell.numConfirmado.text = numConf
        let numCom: String = String(listFiltro[indexPath.row].ordenesDeServicio?.sumaCompletado ?? 0)
        cell.numCompletado.text = numCom
        let numRes: String = String(listFiltro[indexPath.row].ordenesDeServicio?.sumaRescate ?? 0)
        cell.numRescate.text = numRes
        let numCanc: String = String(listFiltro[indexPath.row].ordenesDeServicio?.sumaCancelado ?? 0)
        cell.numCancelado.text = numCanc
        cell.numAgendConstraint.constant = CGFloat(10 + (numAge.count * 10))
        cell.numConfConstraint.constant = CGFloat(10 + (numConf.count * 10))
        cell.numCompConstraint.constant = CGFloat(10 + (numCom.count * 10))
        cell.numRescConstraint.constant = CGFloat(10 + (numRes.count * 10))
        cell.numCancConstraint.constant = CGFloat(10 + (numCanc.count * 10))
        cell.detalleVenta.tag = indexPath.row
        cell.detalleVenta.addTarget(self, action: #selector(handleButtonTapped(sender: )), for: .touchUpInside)
        return cell
    }

    @objc func handleButtonTapped(sender: UIButton) {
        let numA =  listFiltro[sender.tag].ordenesDeServicio?.sumaAgendado ?? 0
        let numC =  listFiltro[sender.tag].ordenesDeServicio?.sumaCompletado ?? 0
        let numCo =  listFiltro[sender.tag].ordenesDeServicio?.sumaConfirmado ?? 0
        let numRe =  listFiltro[sender.tag].ordenesDeServicio?.sumaRescate ?? 0
        let numCan =  listFiltro[sender.tag].ordenesDeServicio?.sumaCancelado ?? 0

        let sumTotal = numA + numC + numCo + numRe + numCan

        if sumTotal == 0 {
            AlertDialog.show(title: "Aviso", body: "No hay información de detalle", view: self)
        } else {
            self.getDetailDelegate?.detailUserProc(numEmployee: listFiltro[sender.tag].noEmpleado )
        }
    }

}
