//
//  SubTitleCell.swift
//  SalesCloud
//
//  Created by mhuertao on 12/06/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

class SubTitleCell: UITableViewCell {

    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var number: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
