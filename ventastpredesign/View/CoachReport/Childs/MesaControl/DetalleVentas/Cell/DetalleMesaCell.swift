//
//  DetalleMesaCell.swift
//  SalesCloud
//
//  Created by mhuertao on 11/02/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

class DetalleMesaCell: UITableViewCell {

    @IBOutlet weak var numOportunidad: UILabel!
    @IBOutlet weak var fechaCreacion: UILabel!
    @IBOutlet weak var estatus: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
