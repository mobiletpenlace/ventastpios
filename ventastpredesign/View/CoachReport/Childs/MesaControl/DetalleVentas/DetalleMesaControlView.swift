//
//  DetalleMesaControlView.swift
//  SalesCloud
//
//  Created by mhuertao on 11/02/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

class DetalleMesaControlView: UIViewController {
    @IBOutlet weak var ListaDetalleTable: UITableView!
    @IBOutlet weak var sumTotalAprobLabel: UILabel!
    @IBOutlet weak var sumTotalPendLabel: UILabel!
    @IBOutlet weak var sumTotalRechaLabel: UILabel!
    @IBOutlet weak var totalAprobConstraint: NSLayoutConstraint!
    @IBOutlet weak var totalPendConstraint: NSLayoutConstraint!
    @IBOutlet weak var totalRechConstraint: NSLayoutConstraint!
    var detalleMesa: [DetalleVentaControl] = []
    var colaborador: Colaboradores?
    var sumTotalAprob = 0
    var sumTotalPend = 0
    var sumTotalRechaz = 0

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        preview()
    }

    func preview() {
        sumTotalAprob = colaborador?.mesaControl?.sumaAprobadas ?? 0
        sumTotalPend = colaborador?.mesaControl?.sumaPendientes ?? 0
        sumTotalRechaz = colaborador?.mesaControl?.sumaRechazadas ?? 0

        sumTotalAprobLabel.text = String(sumTotalAprob)
        sumTotalPendLabel.text = String(sumTotalPend)
        sumTotalRechaLabel.text = String(sumTotalRechaz)

        totalAprobConstraint.constant = CGFloat(15 + (String(sumTotalAprob).count * 10))
        totalPendConstraint.constant = CGFloat(15 + (String(sumTotalPend).count * 10))
        totalRechConstraint.constant = CGFloat(15 + (String(sumTotalRechaz).count * 10))
    }

}

extension DetalleMesaControlView: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return detalleMesa.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 || indexPath.row == (colaborador?.mesaControl?.aprobadasMC.count ?? 0) + 1  || indexPath.row == (colaborador?.mesaControl?.aprobadasMC.count ?? 0) + (colaborador?.mesaControl?.pendientesMC.count ?? 0) + 2 {
            return 40
        } else {
            return 150
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
           let cell = tableView.dequeueReusableCell(withIdentifier: "SubTitleCell", for: indexPath) as! SubTitleCell
            cell.subTitleLabel.text = "Aprobadas"
            cell.number.text = String((colaborador?.mesaControl?.aprobadasMC.count ?? 0))
            return cell
        } else if indexPath.row == (colaborador?.mesaControl?.aprobadasMC.count ?? 0) + 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SubTitleCell", for: indexPath) as! SubTitleCell
            cell.subTitleLabel.text = "Pendientes"
            cell.number.text = String((colaborador?.mesaControl?.pendientesMC.count ?? 0))
            return cell
        } else if indexPath.row == (colaborador?.mesaControl?.aprobadasMC.count ?? 0) + (colaborador?.mesaControl?.pendientesMC.count ?? 0) + 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SubTitleCell", for: indexPath) as! SubTitleCell
            cell.subTitleLabel.text = "Rechazadas"
            cell.number.text = String((colaborador?.mesaControl?.rechazadasMC.count ?? 0))
            return cell
        } else {

        let cell = tableView.dequeueReusableCell(withIdentifier: "DetalleMesaCell", for: indexPath) as! DetalleMesaCell
        cell.numOportunidad.text = detalleMesa[indexPath.row].idventa
        let fecha  = detalleMesa[indexPath.row].fecha
        cell.fechaCreacion.text = fecha?.removeBackslash()
        cell.estatus.text = detalleMesa[indexPath.row].estatusMC
            return cell
        }
    }

}
