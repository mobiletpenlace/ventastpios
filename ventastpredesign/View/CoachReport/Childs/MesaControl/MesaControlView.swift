//
//  MesaControlView.swift
//  SalesCloud
//
//  Created by mhuertao on 11/02/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

protocol getDetalleMesaDelegate {
    func detailUserMesa(numEmployee: String)
}
class MesaControlView: BaseVentasView {

    @IBOutlet weak var VentasTable: UITableView!
    @IBOutlet weak var sumTotalAprobLabel: UILabel!
    @IBOutlet weak var sumTotalPendLabel: UILabel!
    @IBOutlet weak var sumTotalRechaLabel: UILabel!
    var reportsViewModel: reportsCoachViewModel!
    @IBOutlet weak var totalAprobConstraint: NSLayoutConstraint!
    @IBOutlet weak var totalPendConstraint: NSLayoutConstraint!
    @IBOutlet weak var totalRechConstraint: NSLayoutConstraint!
    var getDetailDelegate: getDetalleMesaDelegate?
    var colaboradores: [Colaboradores] = []
    var listFiltro: [Colaboradores] = []
    var filtro: String = ""
    var sumTotalAprob = 0
    var sumTotalPend = 0
    var sumTotalRechaz = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        reportsViewModel = reportsCoachViewModel(view: self)
        reportsViewModel.atachView(observer: self)
        setUpview()
    }

    func setUpview() {
        filtrar(filtro: "")
        sumTotalAprobLabel.text = String(sumTotalAprob)
        sumTotalPendLabel.text = String(sumTotalPend)
        sumTotalRechaLabel.text = String(sumTotalRechaz)
    }

    override func viewDidAppear(_ animated: Bool) {
        VentasTable.reloadData()
    }

    func filtrar(filtro: String) {
        let filtroUpper = filtro.uppercased()
        listFiltro = colaboradores.filter { dato in
            return  dato.name.uppercased().contains(filtroUpper) || dato.noEmpleado.uppercased().contains(filtroUpper) ||
            filtroUpper.count == 0
        }
        self.VentasTable?.reloadData()
        getSumTotal()
    }

    func getSumTotal() {
        let sumaA = listFiltro.map { $0.mesaControl?.sumaAprobadas }
        let sumAprob = sumaA.reduce(0) { (sum, num) -> Int in
            sum + (num ?? 0)
        }

        let sumaP = listFiltro.map { $0.mesaControl?.sumaPendientes }
        let sumPendi = sumaP.reduce(0) { (sum, num) -> Int in
            sum + (num ?? 0)
        }

        let sumaR = listFiltro.map { $0.mesaControl?.sumaRechazadas }
        let sumRecha = sumaR.reduce(0) { (sum, num) -> Int in
            sum + (num ?? 0)
        }

        sumTotalAprobLabel.text = String(sumAprob)
        sumTotalPendLabel.text = String(sumPendi)
        sumTotalRechaLabel.text = String(sumRecha)
        totalAprobConstraint.constant = CGFloat(15 + (String(sumAprob).count * 10))
        totalPendConstraint.constant = CGFloat(15 + (String(sumPendi).count * 10))
        totalRechConstraint.constant = CGFloat(15 + (String(sumRecha).count * 10))
    }

}
extension MesaControlView: reportsCoachObserver {
    func onError(errorMessage: String) {}

    func succesReports() {}

    func searchTable(filtro: String, idTab: Int) {
        if idTab == 1 {
            self.filtro = filtro
            filtrar(filtro: filtro)
        }
    }
}

extension MesaControlView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listFiltro.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MesaControlCell", for: indexPath) as! MesaControlCell
        cell.nombreEmpleado.text = listFiltro[indexPath.row].name
        let numAprob: String = String(listFiltro[indexPath.row].mesaControl?.sumaAprobadas ?? 0)
        cell.numAprobadas.text = numAprob
        let numPend: String = String(listFiltro[indexPath.row].mesaControl?.sumaPendientes ?? 0)
        cell.numPendientes.text = numPend
        let numRech: String = String(listFiltro[indexPath.row].mesaControl?.sumaRechazadas ?? 0)
        cell.numRechazadas.text = numRech
        cell.numAprobConstraint.constant = CGFloat(15 + (numAprob.count * 10))
        cell.numPendConstraint.constant = CGFloat(15 + (numPend.count * 10))
        cell.numRechConstraint.constant = CGFloat(15 + (numRech.count * 10))
        cell.detalleVenta.tag = indexPath.row
        cell.detalleVenta.addTarget(self, action: #selector(handleButtonTapped(sender: )), for: .touchUpInside)
        return cell
    }

    @objc func handleButtonTapped(sender: UIButton) {
        let numApro =  listFiltro[sender.tag].mesaControl?.sumaAprobadas ?? 0
        let numPend =  listFiltro[sender.tag].mesaControl?.sumaPendientes ?? 0
        let numRech =  listFiltro[sender.tag].mesaControl?.sumaRechazadas ?? 0
        let sumTotal = numApro + numPend + numRech

        if sumTotal == 0 {
            AlertDialog.show(title: "Aviso", body: "No hay información de detalle", view: self)
        } else {
            self.getDetailDelegate?.detailUserMesa(numEmployee: listFiltro[sender.tag].noEmpleado )
        }
    }

}
