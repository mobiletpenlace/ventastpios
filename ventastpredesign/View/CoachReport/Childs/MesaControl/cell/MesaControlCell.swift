//
//  MesaControlCell.swift
//  SalesCloud
//
//  Created by mhuertao on 14/02/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

class MesaControlCell: UITableViewCell {

    @IBOutlet weak var nombreEmpleado: UILabel!
    @IBOutlet weak var numAprobadas: UILabel!
    @IBOutlet weak var numPendientes: UILabel!
    @IBOutlet weak var numRechazadas: UILabel!
    @IBOutlet weak var detalleVenta: UIButton!
    @IBOutlet weak var numAprobConstraint: NSLayoutConstraint!
    @IBOutlet weak var numRechConstraint: NSLayoutConstraint!
    @IBOutlet weak var numPendConstraint: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
