//
//  InstaladasReportsView.swift
//  SalesCloud
//
//  Created by mhuertao on 06/03/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit
protocol getDetalleInstaladaDelegate {
    func detailUserInst(numEmployee: String)
}

class InstaladasReportsView: BaseVentasView {

    @IBOutlet weak var sumTotalLabel: UILabel!
    @IBOutlet weak var VentasTable: UITableView!
    var reportsViewModel: reportsCoachViewModel!
    var getDetailDelegate: getDetalleInstaladaDelegate?
    var colaboradores: [Colaboradores] = []
    var listFiltro: [Colaboradores] = []
    var filtro: String = ""
    var sumTotal = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        reportsViewModel = reportsCoachViewModel(view: self)
        reportsViewModel.atachView(observer: self)
        filtrar(filtro: "")
        sumTotalLabel.text = String(sumTotal)
    }

    override func viewDidAppear(_ animated: Bool) {
        VentasTable.reloadData()
    }

    func filtrar(filtro: String) {
        let filtroUpper = filtro.uppercased()
        listFiltro = colaboradores.filter { dato in
            return  dato.name.uppercased().contains(filtroUpper) || dato.noEmpleado.uppercased().contains(filtroUpper) ||
            filtroUpper.count == 0
        }
        self.VentasTable?.reloadData()
        getSumTotal()
    }

    func getSumTotal() {
        let suma = listFiltro.map { $0.ordenesDeServicio?.sumaInstaladas}
        let sumArray = suma.reduce(0) { (sum, num) -> Int in
            sum + (num ?? 0)
        }
        sumTotalLabel.text = String(sumArray)

    }

}

extension InstaladasReportsView: reportsCoachObserver {
    func onError(errorMessage: String) {}

    func succesReports() {}

    func searchTable(filtro: String, idTab: Int) {
        if idTab == 3 {
            self.filtro = filtro
            filtrar(filtro: filtro)
        }
    }
}

extension InstaladasReportsView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listFiltro.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 92
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VentaReporteTableViewCell", for: indexPath) as! VentaReporteTableViewCell
        cell.nombreEmpleado.text = listFiltro[indexPath.row].name
        let numInstal: String = String(listFiltro[indexPath.row].ordenesDeServicio?.sumaInstaladas ?? 0)
        cell.numVentas.text = numInstal
        cell.widthNumVent.constant = CGFloat(10 + (numInstal.count * 10))
        cell.detailButton.tag = indexPath.row
        cell.detailButton.addTarget(self, action: #selector(handleButtonTapped(sender: )), for: .touchUpInside)
        return cell
    }

    @objc func handleButtonTapped(sender: UIButton) {
        let numInstal =  String(listFiltro[sender.tag].ordenesDeServicio?.sumaInstaladas ?? 0)
        if numInstal == "0" {
            AlertDialog.show(title: "Aviso", body: "No hay información de detalle", view: self)
        } else {
            self.getDetailDelegate?.detailUserInst(numEmployee: listFiltro[sender.tag].noEmpleado )
        }

    }

}
