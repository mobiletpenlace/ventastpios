//
//  reportsCoachModel.swift
//  SalesCloud
//
//  Created by mhuertao on 8/02/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

protocol reportsCoachModelObserver {
    func successReports(response: reportsReponse)
    func onError(errorMessage: String)
}

class reportsCoachModel: BaseModel {
    var viewModel: reportsCoachModelObserver?
    var server: ServerDataManager2<reportsReponse>?
    var urlReports = AppDelegate.API_SALESFORCE + ApiDefinition.API_REPORTS

    init (observer: BaseViewModelObserver) {
        super.init(observerVM: observer)

        server = ServerDataManager2<reportsReponse>()

    }

    func atachModel(viewModel: reportsCoachModelObserver) {
        self.viewModel = viewModel
    }

    func consultReports(request: reportsRequest) {
        let urlReportsResponse = AppDelegate.API_SALESFORCE + ApiDefinition.API_REPORTS
        server?.setValues(requestUrl: urlReportsResponse, delegate: self, headerType: .headersSalesforce)
        server?.request(requestModel: request)
    }

    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if requestUrl == urlReports {
            let reportsResponse = response as! reportsReponse
            if reportsResponse.mResult == "0" {
                viewModel?.successReports(response: response as! reportsReponse)
            } else {
                viewModel?.onError(errorMessage: reportsResponse.mResultDescription ?? "No se encontraron empleados colaboradores y sus ventas")
            }
        }
    }

    override func onErrorLoadResponse(requestUrl: String, messageError: String) {
        viewModel?.onError(errorMessage: messageError)
    }

    func getEmploye() -> Empleado {
        let employee = RealManager.findFirst(object: Empleado.self)
        return employee!
    }

}
