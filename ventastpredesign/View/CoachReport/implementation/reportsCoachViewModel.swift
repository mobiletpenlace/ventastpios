//
//  reportsCoachViewModel.swift
//  SalesCloud
//
//  Created by mhuertao on 8/02/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import SwiftEventBus

protocol reportsCoachObserver {
    func succesReports()
    func searchTable(filtro: String, idTab: Int)
    func onError(errorMessage: String)
}

class reportsCoachViewModel: BaseViewModel, reportsCoachModelObserver {
    var observer: reportsCoachObserver!
    var model: reportsCoachModel!
    var colaboradores: [Colaboradores] = []

    override init(view: BaseVentasView) {
        super.init(view: view)
        model = reportsCoachModel(observer: self)
        model.atachModel(viewModel: self)
    }

    func atachView(observer: reportsCoachObserver) {
        self.observer = observer
        SwiftEventBus.onMainThread(self, name: "SearchListVentas") { [weak self] result in
            if let filtro = result?.object as? String {
                self?.observer.searchTable(filtro: filtro, idTab: 0)
            }
        }

        SwiftEventBus.onMainThread(self, name: "SearchListMesa") { [weak self] result in
            if let filtro = result?.object as? String {
                self?.observer.searchTable(filtro: filtro, idTab: 1)
            }
        }

        SwiftEventBus.onMainThread(self, name: "SearchListProceso") { [weak self] result in
            if let filtro = result?.object as? String {
                self?.observer.searchTable(filtro: filtro, idTab: 2)
            }
        }

        SwiftEventBus.onMainThread(self, name: "SearchListInstalada") { [weak self] result in
            if let filtro = result?.object as? String {
                self?.observer.searchTable(filtro: filtro, idTab: 3)
            }
        }

    }

    func consultReports(dateStart: String, dateFinish: String, numEmploye: String) {
        let request = reportsRequest()
        request.numEmpleado = numEmploye
        request.strFechaInicio = dateStart
        request.strFechaFin = dateFinish
        view.showLoading(message: "Consultando reportes")
        model.consultReports(request: request)
    }

    func successReports(response: reportsReponse) {
        view.hideLoading()
        guard let result = response.datos?.colaboradores else {return}
        self.colaboradores = result
        observer.succesReports()
    }

    func onError(errorMessage: String) {
        Logger.println(errorMessage)
        view.hideLoading()
        observer.onError(errorMessage: errorMessage)
    }

    func getPredefinedDate(id: String) -> (String, String) {
        var startDate = ""
        var finishDate = ""
        switch id {
        case "today":
             startDate =  DateCalendar.getToday(formatter: Formate.yyyy_MM_dd_dash)
             finishDate = startDate
        case "yesterday":
             startDate = DateCalendar.getYesterdayString(formatter: Formate.yyyy_MM_dd_dash)
             finishDate = DateCalendar.getToday(formatter: Formate.yyyy_MM_dd_dash)
        case "lastWeek":
            startDate = DateCalendar.getLastWeek(formatter: Formate.yyyy_MM_dd_dash)
            finishDate = DateCalendar.getToday(formatter: Formate.yyyy_MM_dd_dash)
        case "thisWeek":
            startDate = DateCalendar.getFirstMondayWeek(formatter: Formate.yyyy_MM_dd_dash)
            finishDate = DateCalendar.getToday(formatter: Formate.yyyy_MM_dd_dash)
        case "thisMonth":
            startDate = DateCalendar.getFirstDayMonth(formatter: Formate.yyyy_MM_dd_dash)
            finishDate = DateCalendar.getToday(formatter: Formate.yyyy_MM_dd_dash)
        case "lastMonth":
            startDate = DateCalendar.getFirstDayMonthPass(month: -1, formatter: Formate.yyyy_MM_dd_dash)
            finishDate = DateCalendar.getLastDayMonthPass(month: -1, formatter: Formate.yyyy_MM_dd_dash)
        default:
            break
        }
        return (startDate, finishDate)
    }

    func convertDatetoText(date: String, formatte: String = Formate.dd_MMMM_yyyy) -> String {
        let dateConvert = DateCalendar.convertStringDate(date: date, formatter: Formate.yyyy_MM_dd_dash) ?? Date()
        return DateCalendar.convertDateString(date: dateConvert, formatter: formatte)
    }

    func getTextDate(id: String, startDate: String, finishDate: String) -> String {
        var result = ""
        switch id {
        case "today":
            result = convertDatetoText(date: finishDate).uppercased()
        case "yesterday":
            result = convertDatetoText(date: startDate).uppercased()
        case "lastWeek":
            result = convertDatetoText(date: startDate, formatte: Formate.dd_MMMM_).uppercased() + " al " + convertDatetoText(date: finishDate).uppercased()
        case "thisWeek":
            result = convertDatetoText(date: startDate, formatte: Formate.dd_MMMM_).uppercased() + " al " + convertDatetoText(date: finishDate).uppercased()
        case "thisMonth":
            result = convertDatetoText(date: startDate, formatte: Formate.dd_MMMM_).uppercased() + " al " + convertDatetoText(date: finishDate).uppercased()
        case "lastMonth":
            result = convertDatetoText(date: startDate, formatte: Formate.dd_MMMM_).uppercased() + " al " + convertDatetoText(date: finishDate).uppercased()
        case "other":
            result = convertDatetoText(date: startDate, formatte: Formate.dd_MMMM_).uppercased() + " al " + convertDatetoText(date: finishDate).uppercased()

        default:
            break
        }
        return result
    }

    func getNumEmployee() -> String {
       return model.getEmploye().NoEmpleado
    }

}
