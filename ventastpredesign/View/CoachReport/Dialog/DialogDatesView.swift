//
//  DialogDatesView.swift
//  SalesCloud
//
//  Created by mhuertao on 07/03/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit
protocol DialogDate {
    func selectDateDialog(startDate: String, finishDate: String, id: String)
}

class DialogDatesView: BaseVentasView {

    @IBOutlet weak var dialogView: UIView!
    var reportsViewModel: reportsCoachViewModel!
    var dialogDateDelegate: DialogDate?

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        reportsViewModel = reportsCoachViewModel(view: self)

    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        if touch?.view != self.dialogView { self.dismiss(animated: false, completion: nil)
        }
    }

    @IBAction func todayAction(_ sender: Any) {
        let id = "today"
        let (startDay, finishDay) = reportsViewModel.getPredefinedDate(id: id)
        dialogDateDelegate?.selectDateDialog(startDate: startDay, finishDate: finishDay, id: id)
        self.dismiss(animated: false)
    }

    @IBAction func yesterdayAction(_ sender: Any) {
        let id = "yesterday"
        let (startDay, finishDay) = reportsViewModel.getPredefinedDate(id: id)
        dialogDateDelegate?.selectDateDialog(startDate: startDay, finishDate: finishDay, id: id)
        self.dismiss(animated: false)
    }

    @IBAction func lastWeekAction(_ sender: Any) {
        let id = "lastWeek"
        let (startDay, finishDay) = reportsViewModel.getPredefinedDate(id: id)
        dialogDateDelegate?.selectDateDialog(startDate: startDay, finishDate: finishDay, id: id)
        self.dismiss(animated: false)
    }

    @IBAction func thisWeekAction(_ sender: Any) {
        let id = "thisWeek"
        let (startDay, finishDay) = reportsViewModel.getPredefinedDate(id: id)
        dialogDateDelegate?.selectDateDialog(startDate: startDay, finishDate: finishDay, id: id)
        self.dismiss(animated: false)
    }

    @IBAction func thisMonthAction(_ sender: Any) {
        let id = "thisMonth"
        let (startDay, finishDay) = reportsViewModel.getPredefinedDate(id: id )
        dialogDateDelegate?.selectDateDialog(startDate: startDay, finishDate: finishDay, id: id)
        self.dismiss(animated: false)
    }

    @IBAction func lastMonthAction(_ sender: Any) {
        let id = "lastMonth"
        let (startDay, finishDay) = reportsViewModel.getPredefinedDate(id: id)
        dialogDateDelegate?.selectDateDialog(startDate: startDay, finishDate: finishDay, id: id)
        self.dismiss(animated: false)
    }

}
