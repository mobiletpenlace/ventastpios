//
//  DialogCalendarView.swift
//  SalesCloud
//
//  Created by mhuertao on 09/04/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit
import FSCalendar

protocol setDateCalendarDelegate {
    func getDate(dateStart: String, dateFinish: String)
}
class DialogCalendarView: UIViewController {

    @IBOutlet weak var calendarFirst: FSCalendar!
    @IBOutlet weak var calendarSecond: FSCalendar!
    var setDateCalendarDelegate: setDateCalendarDelegate?
    var currentDateStart: String = ""
    var currentDateFinish: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: false)
    }

    @IBAction func saveDate(_ sender: Any) {
        if calendarFirst.selectedDate != nil  && calendarSecond.selectedDate != nil {
            let start = DateCalendar.convertDateString(date: calendarFirst.selectedDate ?? Date(), formatter: Formate.yyyy_MM_dd_dash)
            let finish = DateCalendar.convertDateString(date: calendarSecond.selectedDate ?? Date(), formatter: Formate.yyyy_MM_dd_dash)
            setDateCalendarDelegate?.getDate(dateStart: start, dateFinish: finish)
            self.dismiss(animated: false)
        } else {
            Constants.Alert(title: "AVISO", body: "Favor de elegir una fecha de inicio y una fecha de fin", type: type.Warning, viewC: self)
        }
    }

    func maximumDate(for calendar: FSCalendar) -> Date {
        return Date()
    }

}

extension DialogCalendarView: FSCalendarDataSource, FSCalendarDelegate {

}
