//
//  homeVC.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 17/12/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import UIKit
import ImageSlideshow
import Lottie
import PDFKit
import Amplitude

class HomeVC: BaseVentasView {

    @IBOutlet weak var mNameLabel: UILabel!
    @IBOutlet weak var mCityLabel: UILabel!
    @IBOutlet weak var ViewNuevaVenta: GradientView!
    @IBOutlet weak var callCenterView: UIView!
    @IBOutlet weak var polidicticoView: GradientView!
    @IBOutlet weak var ventaNuevaImage: UIImageView!
    @IBOutlet weak var ventajasImage: UIImageView!
    @IBOutlet weak var ventajasLabel: UILabel!
    @IBOutlet weak var nuevaVentaLabel: UILabel!
    @IBOutlet weak var misVentasLabel: UILabel!
    @IBOutlet weak var nuevoLeadLabel: UILabel!
    @IBOutlet var slideshow: ImageSlideshow!
    @IBOutlet weak var polidicticoButton: UIButton!
    @IBOutlet weak var topSlideShowConstraint: NSLayoutConstraint!
    @IBOutlet weak var pageControl: UIPageControl!
//    @IBOutlet weak var viewAnimation: AnimationView!
    @IBOutlet weak var existingAccountsImageView: UIImageView!
    @IBOutlet weak var qrImageView: UIImageView!
    @IBOutlet weak var existingAccountsLabel: UILabel!

    var MenuVC: MenuViewController!
    var rankingS: String = ""
    var employed: Empleado?
    var viewModel: HomeViewModel?
    var IdOp: String = ""
    var ImageBanners: [InputSource] = []
    var ImageBannersTitles: [String] = []
    var isDownloadedBanners: Bool = false

    override func viewDidLoad() {
        super.setView(view: self.view)
        viewModel = HomeViewModel(view: self)
        viewModel?.atachView(observer: self)
        getEmploye()
        setMenu()
    }

    override func viewDidAppear(_ animated: Bool) {
        faseDataOK.isSended = false
        Amplitude.instance().logEvent("Show_HomeScreen")
        if isDownloadedBanners == false {
            viewModel?.getBanners()
        }

        // Variable globar para activar o desactivar flujo comissiones.
        faseComisiones.enableComissions = true
        clearDataNewSell()
    }

    func getEmploye() {
        if RealManager.findFirst(object: Empleado.self) != nil {
            employed = Empleado(value: RealManager.findFirst(object: Empleado.self)!)
            faseComisiones.numEmpleado = employed!.NoEmpleado
            Amplitude.instance().setUserId(employed!.NoEmpleado)
        }
    }

    @IBAction func zeuzAction(_ sender: Any) {
        if let url = URL(string: "https://apps.apple.com/mx/app/tareas-zeus/id1444687630?l=en") {
            UIApplication.shared.open(url)
        }
    }

    @IBAction func startSell(_ sender: Any) {
        ventaNueva()
        Amplitude.instance().logEvent("Show_NewSell_From_StartSale")
    }

    @IBAction func MenuAction(_ sender: Any) {
        if AppDelegate.menu_bool {
            ShowMenu()
        } else {
            CloseMenu()
        }
    }

    @IBAction func MisVentasAction(_ sender: Any) {
        if UserDefinitions.isEmbajador {
            Constants.LoadStoryBoard(name: storyBoard.Crearlead, viewC: self)
        } else {
            Constants.LoadStoryBoard(name: storyBoard.MySell, viewC: self)
        }
    }

    @IBAction func ComisionesAction(_ sender: Any) {
        if faseComisiones.enableComissions == true {
            print("embajador")
            print(UserDefinitions.isEmbajador)
            print("ministro")
            print(UserDefinitions.isMinistro)
            print("promotor")
            print(UserDefinitions.isPromotor)
            print("isempresarial")
            print(UserDefinitions.isEmpresarial)
            print("embajador")
            print(UserDefinitions.isResidencial)

            if UserDefinitions.isEmbajador {
                if UserDefinitions.isMinistro {
                    Constants.LoadStoryBoard(name: storyBoard.ComissionsEmbajador.WelcomeMinistro, viewC: self)
                } else {
                    Constants.LoadStoryBoard(name: storyBoard.ComissionsEmbajador.WelcomeEmbajador, viewC: self)
                }
            } else {
                Constants.Alert(title: "", body: "Modulo en construcción. \nDisculpe las molestias.", type: type.Info, viewC: self)
                // Constants.LoadStoryBoard(name: storyBoard.Comissions.Welcome, viewC: self)
            }
        } else {
            Constants.Alert(title: "", body: "Modulo en construcción. \nDisculpe las molestias.", type: type.Info, viewC: self)
        }
        print("embajador")
        print(UserDefinitions.isEmbajador)
        print("ministro")
        print(UserDefinitions.isMinistro)
        print("promotor")
        print(UserDefinitions.isPromotor)
        print("isempresarial")
        print(UserDefinitions.isEmpresarial)
        print("isresidencial")
        print(UserDefinitions.isResidencial)

    }

    @IBAction func CodigoQRAction(_ sender: Any) {
        if UserDefinitions.isEmbajador {
            let generateQR: GenerateQR = UIStoryboard(name: "GenerateQR", bundle: nil).instantiateViewController(withIdentifier: "GenerateQR") as! GenerateQR
            generateQR.providesPresentationContextTransitionStyle = true
            generateQR.definesPresentationContext = true
            generateQR.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            generateQR.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            UIApplication.topViewController()?.present(generateQR, animated: false, completion: nil)
            print("if")
        } else {
//            Constants.LoadStoryBoard(name: storyBoard.ExistingAccount.ExistingAccount, viewC: self)
        }
    }

    @IBAction func CallAction(_ sender: Any) {
             callNumber(phoneNumber: 8000508682)
    }

    @IBAction func PromocionesAction(_ sender: Any) {
        if UserDefinitions.isEmbajador {
            ventaNueva()
        } else {
            Constants.LoadStoryBoard(name: storyBoard.Diferenciadores.Welcome, viewC: self)
        }
    }

    @IBAction func CoberturaAction(_ sender: Any) {

        ventaNueva()

//        let generateQR : SuggestedPlanBigScroll = UIStoryboard(name: "SuggestedPlanBigScroll", bundle:nil).instantiateViewController(withIdentifier: "SuggestedPlanBigScroll") as! SuggestedPlanBigScroll
//        generateQR.providesPresentationContextTransitionStyle = true
//        generateQR.definesPresentationContext = true
//        generateQR.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//        generateQR.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
//        UIApplication.topViewController()?.present(generateQR, animated: false, completion: nil)

        Amplitude.instance().logEvent("Show_NewSell_From_StartSale")
    }

//    @IBAction func LanzamientosAction(_ sender: Any) {
//        Constants.LoadStoryBoard(name: storyBoard.Diferenciadores.Welcome, viewC: self)
//    }

    @IBAction func polidicticoAction(_ sender: Any) {
        if let url = URL(string: "https://cms-files-app.s3.amazonaws.com/TP_Embajador_32_2fddfd4abd.pdf") {
                    UIApplication.shared.open(url, options: [:])
                }
    }

    @objc func alertControllerBackgroundTapped() {
        self.dismiss(animated: true, completion: nil)
    }

    private func callNumber(phoneNumber: Int) {
        if let phoneCallURL = URL(string: "tel://\(+phoneNumber)") {
            let application: UIApplication = UIApplication.shared
            if application.canOpenURL(phoneCallURL) {
                application.open(phoneCallURL, options: [:], completionHandler: nil)
            }
        }
    }

}
