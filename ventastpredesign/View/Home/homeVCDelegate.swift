//
//  homeVCDelegate.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 08/05/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import Foundation
import ImageSlideshow
import Amplitude

extension HomeVC: ImageSlideshowDelegate, HomeObserver {
    func onSuccesBanners(_ banners: [BannersHome]) {
        print("success")
    }

    func IniciaVenta(id: String) {
        IdOp = id
        CloseMenu()
        ventaNueva()
    }

    func OcultaMenu() {
        CloseMenu()
    }

    func imageSlideshow(_ imageSlideshow: ImageSlideshow, didChangeCurrentPageTo page: Int) {
        // por el moemnto no se ocupa
    }

    func setSlide(image: [InputSource]) {
        pageControl.numberOfPages = ImageBanners.count
        slideshow.currentPageChanged = { [self] page in
            self.pageControl.currentPage = page}
        slideshow.slideshowInterval = 3.0
        slideshow.contentScaleMode = UIViewContentMode.scaleAspectFill
        slideshow.pageIndicator = nil
        slideshow.activityIndicator = DefaultActivityIndicator()
        slideshow.delegate = self
        slideshow.setImageInputs(image)
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(HomeVC.didTap))
        slideshow.addGestureRecognizer(recognizer)
    }

    @objc func didTap() {
        if slideshow.currentPage == 0 {
            Constants.LoadStoryBoard(name: storyBoard.HumanResources, viewC: self)
        } else {
            let fullScreenController = slideshow.presentFullScreenController(from: self)
            fullScreenController.slideshow.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
        }
        Amplitude.instance().logEvent("Banner_Tapped", withEventProperties: ["Banner_Title": ImageBannersTitles[slideshow.currentPage], "Banner_Position": slideshow.currentPage + 1])
    }

    func setMenu() {
        MenuVC = (UIStoryboard(name: "MenuViewController", bundle: nil).instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController)
//        if (employed?.Canales.count)! <= 1{
            for canal in employed!.Canales {
                if canal.nombre == "Embajadores" || canal.nombre == "Autoempresarios" {
                    UserDefinitions.isEmbajador = true
                    UserDefinitions.isPromotor = false
                    UserDefinitions.isEmpresarial = false
                    UserDefinitions.isResidencial = false
                }
            }
        if UserDefinitions.isEmbajador {
            if employed?.Posicion != nil {
                if (employed?.Posicion.uppercased().contains(string: "MINISTRO"))! {
                    UserDefinitions.isMinistro = true
                } else {
                    UserDefinitions.isMinistro = false
                }
            }
        } else {
            UserDefinitions.isEmbajador = false
            UserDefinitions.isPromotor = false
            UserDefinitions.isEmpresarial = false
            UserDefinitions.isResidencial = true
            UserDefinitions.isMinistro = false
        }
        embajador()
        MenuVC.isEmbajador = UserDefinitions.isEmbajador
        let SwipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToGesture))
        SwipeRight.direction = UISwipeGestureRecognizerDirection.right
        let SwipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToGesture))
        SwipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(SwipeRight)
        self.view.addGestureRecognizer(SwipeLeft)
        self.MenuVC.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height )
        let fullName = employed?.Nombre
        let arrFullName = fullName?.components(separatedBy: " ")
        let name = arrFullName![2].capitalizingFirstLetter()
        let middleName = arrFullName![0].capitalizingFirstLetter()
//        let lastName = arrFullName![1].capitalizingFirstLetter()
        if employed?.Nombre != nil {
            mNameLabel.text = "\(name) \(middleName)"
        } else {
            mNameLabel.text = "No name"
        }
        MenuVC.mNombreLabel.text = employed?.Nombre ?? "No name"
        MenuVC.mPositionLabel.text = mCityLabel.text
    }

    func ShowMenu() {
        UIView.animate(withDuration: 0.5) {() -> Void in
            self.MenuVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height )
            self.MenuVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.0)
            self.addChildViewController(self.MenuVC)
            self.view.addSubview(self.MenuVC.view)
            AppDelegate.menu_bool = false
        }
        UIView.animate(withDuration: 1.00) {() -> Void in
            self.MenuVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        }
    }

    func CloseMenu() {
        self.MenuVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.0)
        UIView.animate(withDuration: 0.5, animations: {
            self.MenuVC.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        }) {(_) in
            self.MenuVC.view.removeFromSuperview()
        }
        AppDelegate.menu_bool = true
    }

    func ventaNueva() {
        Constants.LoadStoryBoard(name: storyBoard.Start, viewC: self)
    }

    @objc func respondToGesture(gesture: UISwipeGestureRecognizer) {
        switch gesture.direction {
        case UISwipeGestureRecognizerDirection.right:
            ShowMenu()
        case UISwipeGestureRecognizerDirection.left:
            CloseMenu()
        default:
            break
        }
    }

    func embajador() {
        if UserDefinitions.isEmbajador {
            ViewNuevaVenta.isHidden = true
            callCenterView.isHidden = false
            ventaNuevaImage.isHidden = false
            nuevaVentaLabel.isHidden = false
            ventajasImage.isHidden = true
            ventajasLabel.isHidden = true
            existingAccountsImageView.isHidden = true
            existingAccountsLabel.text = "QR"
            qrImageView.isHidden = false
            misVentasLabel.isHidden = true
            nuevoLeadLabel.isHidden = false
            polidicticoView.isHidden = false
        } else {
            ViewNuevaVenta.isHidden = false
            callCenterView.isHidden = true
            ventaNuevaImage.isHidden = true
            nuevaVentaLabel.isHidden = true
            ventajasImage.isHidden = false
            ventajasLabel.isHidden = false
            // True
            existingAccountsImageView.isHidden = true
            qrImageView.isHidden = true
            existingAccountsLabel.isHidden = true

            misVentasLabel.isHidden = false
            nuevoLeadLabel.isHidden = true
            polidicticoView.isHidden = true
            topSlideShowConstraint.constant = 0
        }
    }

    func clearDataNewSell() {
        fasePreguntas.editar = false
        fasePreguntas.idCliente = ""
        fasePreguntas.numberOfPeople = 2
        fasePreguntas.numberOfRooms = 2
        fasePreguntas.tvProvider = ""
        fasePreguntas.internetProvider = ""
        fasePreguntas.phoneProvider = ""
        fasePreguntas.favoriteApps = ""
        fasePreguntas.usesInternet = ""
        faseSelectedPlan.plan1 = LlenadoObjeto()
        faseSelectedPlan.plan2 = LlenadoObjeto()
    }
}
