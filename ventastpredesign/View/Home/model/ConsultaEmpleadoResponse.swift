//
//  empleadoConsulta.swift
//  ventastpredesign
//
//  Created by apple on 1/16/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import Foundation

struct ConsultaEmpleadoResponse: Codable {
    let resultDescription: String?
    let result: String?
    let empleado: EmpleadoConsulta?

    enum CodingKeys: String, CodingKey {
        case resultDescription
        case result
        case empleado
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        resultDescription = try values.decodeIfPresent(String.self, forKey: .resultDescription)
        result = try values.decodeIfPresent(String.self, forKey: .result)
        empleado = try values.decodeIfPresent(EmpleadoConsulta.self, forKey: .empleado)
    }
}
