import Foundation
struct TokenResponse: Codable {
    let access_token: String?
    let instance_url: String?
    let id: String?
    let token_type: String?
    let issued_at: String?
    let signature: String?

    enum CodingKeys: String, CodingKey {

        case access_token
        case instance_url
        case id
        case token_type
        case issued_at
        case signature
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        access_token = try values.decodeIfPresent(String.self, forKey: .access_token)
        instance_url = try values.decodeIfPresent(String.self, forKey: .instance_url)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        token_type = try values.decodeIfPresent(String.self, forKey: .token_type)
        issued_at = try values.decodeIfPresent(String.self, forKey: .issued_at)
        signature = try values.decodeIfPresent(String.self, forKey: .signature)
    }

}
