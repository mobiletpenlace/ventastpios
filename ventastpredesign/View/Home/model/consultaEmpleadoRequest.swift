//
//  consultaEmpleadoRequest.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 14/01/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import Foundation

class consultaEmpleadoRequest: Codable {
    var numEmpleado: String?

    enum CodingKeys: String, CodingKey {
        case numEmpleado
    }

    required init(numEmpleado: String) {
        self.numEmpleado = numEmpleado
    }

}
