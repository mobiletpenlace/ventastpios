//
//  BannersHomeResponse.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 29/10/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class BannersHomeResponse: BaseResponse {

    required init?(map: Map) {

    }

    override func mapping(map: Map) {
        bannersHomes <- map["banners_homes"]
    }
    override init() {

    }

    var bannersHomes: [BannersHome] = []

}
