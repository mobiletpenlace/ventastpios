//
//  CanalesDB.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 14/04/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper
import RealmSwift

class CanalesDB: Object, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        nombre <- map["nombre"]
        Subcanales <- map["Subcanales"]
    }

    override static func primaryKey() -> String? {
        return "uui"
    }
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  nombre: String = ""
    var Subcanales: List<String> = List<String>()

}
