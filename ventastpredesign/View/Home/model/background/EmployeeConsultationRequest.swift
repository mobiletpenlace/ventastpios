//
//  EmployeeConsultationRequest.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 12/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class EmployeeConsultationRequest: BaseRequest {

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        MInfo <- map["Info"]
        MLogin <- map["Login"]

    }

    var MInfo: Info?
    var MLogin: Login?

}
