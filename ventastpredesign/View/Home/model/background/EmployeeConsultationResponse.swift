//
//  EmployeeConsultationResponse.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 12/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class EmployeeConsultationResponse: BaseResponse {

    required init?(map: Map) {

    }

    override func mapping(map: Map) {
        MResult <- map["Result"]
        MEmpleado <- map["Empleado"]

    }
    override init() {

    }

    var MResult: Result?
    var MEmpleado: [Empleado] = []

}
