//
//  ConsultaEmpleadoResponse.swift
//  ventastpredesign
//
//  Created by apple on 1/14/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import Foundation

struct EmpleadoConsulta: Codable {
    let PuestoEmpleado: String?
    let Plaza: String?
    let NombreEmpleado: String?
    let Id: String?
    let FolioEmpleado: String?
    let Distrito: String?

    enum CodingKeys: String, CodingKey {
        case PuestoEmpleado = "puestoEmpleado"
        case Plaza = "plaza"
        case NombreEmpleado = "nombreEmpleado"
        case Id = "Id"
        case FolioEmpleado = "folioEmpleado"
        case Distrito = "distrito"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        PuestoEmpleado = try values.decodeIfPresent(String.self, forKey: .PuestoEmpleado)
        Plaza = try values.decodeIfPresent(String.self, forKey: .Plaza)
        NombreEmpleado = try values.decodeIfPresent(String.self, forKey: .NombreEmpleado)
        Id = try values.decodeIfPresent(String.self, forKey: .Id)
        FolioEmpleado = try values.decodeIfPresent(String.self, forKey: .FolioEmpleado)
        Distrito = try values.decodeIfPresent(String.self, forKey: .Distrito)
    }
}
