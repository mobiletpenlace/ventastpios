//
//  Informacion.swift
//  ventastpredesign
//
//  Created by apple on 1/16/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import Foundation

class Informacion: Codable {
    var fechaHoraSalida: String?
    var idRegistro: String?
    var tipoRegistro: String?

    enum CodingKeys: String, CodingKey {
        case fechaHoraSalida
        case idRegistro
        case tipoRegistro
    }

    required init(fechaHoraSalida: String, idRegistro: String, tipoRegistro: String) {
        self.fechaHoraSalida = fechaHoraSalida
        self.idRegistro = idRegistro
        self.tipoRegistro = tipoRegistro
    }

}
