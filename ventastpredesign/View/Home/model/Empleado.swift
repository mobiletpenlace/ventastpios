//
//  Employee.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 12/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import UIKit

import ObjectMapper
import RealmSwift

class Empleado: Object, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        Id <- map["Id"]
        Nombre <- map["Nombre"]
        Canal <- map["Canal"]
        Distrital <- map["Distrital"]
        Estatus <- map["Estatus"]
        FolioEmpleado <- map["FolioEmpleado"]
        IdExterno <- map["IdExterno"]
        Posicion <- map["Posicion"]
        DistritalName <- map["DistritalName"]
        EmpleadoDistrital <- map["EmpleadoDistrital"]
        EmpleadoDistritalName <- map["EmpleadoDistritalName"]
        NoEmpleado <- map["NoEmpleado"]
        Canales <- map ["Canales"]
        CanalSelect <- map ["CanalSelect"]
        SubCanalSelect <- map ["SubCanalSelect"]
        idDevice <- map ["idDevice"]
        isSessionActive <- map ["isSessionActive"]
        contrasena <- map["contrasena"]
    }

    override static func primaryKey() -> String? {
        return "uui"
    }
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  Id: String = ""
    @objc dynamic var  Nombre: String = ""
    @objc dynamic var  Canal: String = ""
    @objc dynamic var  Distrital: String = ""
    @objc dynamic var  Estatus: String = ""
    @objc dynamic var  FolioEmpleado: String = ""
    @objc dynamic var  IdExterno: String = ""
    @objc dynamic var  Posicion: String = ""
    @objc dynamic var  DistritalName: String = ""
    @objc dynamic var  EmpleadoDistrital: String = ""
    @objc dynamic var  EmpleadoDistritalName: String = ""
    @objc dynamic var  NoEmpleado: String = ""
    @objc dynamic var  numberLogin: String = ""
    @objc dynamic var  FolioEmpleadoLogueado: String = ""
    var  Canales: List<CanalesDB> = List<CanalesDB>()
    @objc dynamic var  CanalSelect: String = ""
    @objc dynamic var  SubCanalSelect: String = ""
    @objc dynamic var  idDevice: String = ""
    @objc dynamic var  isSessionActive: Bool = false
    @objc dynamic var  contrasena: String = ""
    @objc dynamic var subCanal: String = String.emptyString

}
