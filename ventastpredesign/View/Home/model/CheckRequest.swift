//
//  CheckRequest.swift
//  ventastpredesign
//
//  Created by apple on 1/16/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import Foundation

class CheckRequest: Codable {
    var informacion: Informacion?

    enum CodingKeys: String, CodingKey {
        case informacion
    }

    required init(fechaHoraSalida: String, idRegistro: String, tipoRegistro: String) {
        self.informacion = Informacion(fechaHoraSalida: fechaHoraSalida, idRegistro: idRegistro, tipoRegistro: tipoRegistro)
    }

}
