//
//  BannersHome.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 29/10/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class BannersHome: NSObject, Mappable {
    required init?(map: Map) {

    }

    func mapping(map: Map) {
        visible <- map["visible"]
        orden <- map["orden"]
        imagen <- map["imagen"]
    }
    override init() {

    }

    var visible: Bool?
    var orden: Int?
    var imagen: [ImagenStrappi] = []

}
