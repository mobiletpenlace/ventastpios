//
//  CheckResponse.swift
//  ventastpredesign
//
//  Created by apple on 1/14/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import Foundation

struct CheckResponse: Codable {

    let resultDescription: String?
    let result: String?
    let idRegistroAsistencia: String?

    enum CodingKeys: String, CodingKey {
        case resultDescription
        case result
        case idRegistroAsistencia
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        resultDescription = try values.decodeIfPresent(String.self, forKey: .resultDescription)
        result = try values.decodeIfPresent(String.self, forKey: .result)
        idRegistroAsistencia = try values.decodeIfPresent(String.self, forKey: .idRegistroAsistencia)
    }

}
