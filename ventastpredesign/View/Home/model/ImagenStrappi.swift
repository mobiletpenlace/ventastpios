//
//  ImagenStrappi.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 29/10/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class ImagenStrappi: NSObject, Mappable {
    required init?(map: Map) {

    }

    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        url <- map["url"]
    }
    override init() {

    }

    var id: Int?
    var name: String?
    var url: String?

}
