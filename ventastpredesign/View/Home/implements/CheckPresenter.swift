//
//  CheckPresenter.swift
//  ventastpredesign
//
//  Created by apple on 1/14/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import Foundation
import UIKit

protocol CheckDelegate: NSObjectProtocol {
    func devuelveDatosCheck(idRegistro: String, idEmpleado: String)
    func changeStatusCheck()
}

class CheckPresenter: BaseVentastpredesignPresenter {
    var viewC: CheckDelegate!
    var service: CheckService!

    init(service: CheckService, view: BaseVentasView) {
        super.init(view: view)
        self.service = service
    }

    func attachView(viewC: CheckDelegate) {
        self.viewC = viewC
    }

    func viewDidShow() {
    }

    func getCheck(idRegistro: String, tipoRegistro: String) {
        service?.getCheck(IdRegistro: idRegistro, TipoRegistro: tipoRegistro, { (res, response) in
            if res {
                Logger.println("funciono el check")
                self.view.hideLoading()
                self.viewC?.devuelveDatosCheck(idRegistro: (response?.result)!, idEmpleado: (response?.idRegistroAsistencia)!)
            } else {
                let alerta = UIAlertController(title: "Error", message: "No se pudo realizar el check", preferredStyle: .alert)

                alerta.addAction(UIAlertAction(title: "CONTINUAR", style: .default, handler: {[weak alerta](_) in
                    Logger.println("Error en el check")
                    self.view.hideLoading()
                    self.viewC?.changeStatusCheck()
                }))
            }
        })
    }
}
