//
//  HomeViewModel.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 17/04/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus

protocol HomeObserver {
    func OcultaMenu()
    func IniciaVenta(id: String)
    func onSuccesBanners(_ banners: [BannersHome])
}

class HomeViewModel: BaseViewModel, HomeModelObserver {

    var observer: HomeObserver?
    var model: HomeModel!
    var mBanners: [BannersHome] = []

    override init(view: BaseVentasView) {
        super.init(view: view)
        model = HomeModel(observer: self)
        model.atachModel(viewModel: self)
    }

    func atachView(observer: HomeObserver) {
        self.observer = observer

        SwiftEventBus.onMainThread(self, name: "OcultarMenu") { [weak self] _ in
            self?.observer?.OcultaMenu()
        }

        SwiftEventBus.onMainThread(self, name: "CargaOp") { [weak self] result in
            if let id = result?.object as? String {
                self?.observer?.IniciaVenta(id: id)
            }
        }
    }

    func getBanners() {
        model.getBanners()
    }

    func onSuccesBanners(banners: BannersHomeResponse) {
        var i = 1
        while i < 200 {
            for ban in banners.bannersHomes {
                if i == ban.orden {
                    if ban.visible! {
                        mBanners.append(ban)
                    }
                }
            }

            i += 1
        }
        view.hideLoading()
        observer?.onSuccesBanners(mBanners)
    }
}
