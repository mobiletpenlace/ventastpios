//
//  HomeModel.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 17/04/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import RealmSwift
import SwiftEventBus

protocol HomeModelObserver {
    func onSuccesBanners(banners: BannersHomeResponse)
}

class HomeModel: BaseModel {

    var viewModel: HomeModelObserver?
    var server: ServerDataManager2<BannersHomeResponse>?
    var urlBanners = "https://cmsapp.totalplay.dev/plazas/1"

    init(observer: BaseViewModelObserver) {
        super.init(observerVM: observer)
        server = ServerDataManager2<BannersHomeResponse>()
    }

    func atachModel(viewModel: HomeModelObserver ) {
        self.viewModel = viewModel
    }

    func getBanners() {
        urlBanners = "https://cmsapp.totalplay.dev/plazas/1"
        server?.setValues(requestUrl: urlBanners, delegate: self, headerType: .headersStrapi)
        server?.requestStrappi()
    }

    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if requestUrl == urlBanners {
            let BannerResponse = response as! BannersHomeResponse
            viewModel?.onSuccesBanners(banners: BannerResponse)
        }
    }
}
