//
//  CheckService.swift
//  ventastpredesign
//
//  Created by apple on 1/14/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import RealmSwift

class CheckService {
    let serverManager: ServerDataManager2<BaseResponse>?
    let URL = "https://na34.salesforce.com/services/apexrest/WS_IngresoSalidaVendedores"

    init() {
        serverManager = ServerDataManager2()
    }

    func getCheck(IdRegistro: String, TipoRegistro: String, _ callBack: @escaping ((Bool, CheckResponse?)) -> Void) {
       /* let fecha = Date()
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd-MM-yyyy HH:mm"*/
        let today = Constants.GetDate(formater: dateFormatter.dayhour, numDay: 0, numMonth: 0)
//        var check : String = ""
        var res: CheckResponse!
        let model = CheckRequest(fechaHoraSalida: today, idRegistro: IdRegistro, tipoRegistro: TipoRegistro)

        serverManager?.post(model: model, url: URL) {[weak self] data in
            guard let data = data else {
                callBack((false, nil))
                return
            }
            do {
                let decoder = JSONDecoder()
                res = try decoder.decode(CheckResponse.self, from: data)
                callBack((true, res))
            } catch let err {
                print(err)
                callBack((false, nil))
            }
        }
    }
}
