//
//  TokenService.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 14/01/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import RealmSwift

class TokenService {
    let serverManager: ServerDataManager2<BaseResponse>
    let URL = "https://na34.salesforce.com/services/oauth2/token"

    init() {
        serverManager = ServerDataManager2()
    }

    func getToken(_ callBack: @escaping ((Bool, String)) -> Void) {
        var token: String = ""
        var res: TokenResponse!
        serverManager.postToken(model: "grant_type=password&client_id=3MVG9KI2HHAq33RxdFES5KXE7Sz8lu.FC0.Lk53x64FHLI9V1B11UAhBcz8bX584F0ugq1aNlMNMKvWS2Tung&client_secret=8884931604809527571&username=rcruzc@enlacetp.mx&password=65007084Crackzrifa3GZqNuTLDdn6qvSGP6wcJM5b", url: URL ) { data in
            guard let data = data else {
                callBack((false, ""))
                return
            }
            do {
                let decoder = JSONDecoder()
                res = try decoder.decode(TokenResponse.self, from: data)
                token = res.access_token ?? ""
                callBack((true, token))
            } catch let err {
                print(err)
                callBack((false, ""))
            }
        }
    }
}
