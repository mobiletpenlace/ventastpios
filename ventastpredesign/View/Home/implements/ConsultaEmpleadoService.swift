//
//  ConsultaEmpleadoService.swift
//  ventastpredesign
//
//  Created by apple on 1/14/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import RealmSwift

class ConsultaEmpleadoService {
    let serverManager: ServerDataManager2<BaseResponse>?
    let URL = "https://na34.salesforce.com/services/apexrest/WS_ConsultaEmpleadosAsistenciaRest"

    init() {
        serverManager = ServerDataManager2()
    }

    func getConsultaEmpleado(NoEmpleado: String, _ callBack: @escaping ((Bool, ConsultaEmpleadoResponse?)) -> Void) {
        var consulta: String = ""
        var res: ConsultaEmpleadoResponse!
        let model  = consultaEmpleadoRequest(numEmpleado: NoEmpleado)
        serverManager?.post(model: model, url: URL) {[weak self] data in
            guard let data = data else {
                callBack((false, nil))
                return
            }

            do {
                let decoder = JSONDecoder()
                res = try decoder.decode(ConsultaEmpleadoResponse.self, from: data)
                callBack((true, res))
            } catch let err {
                print(err)
                callBack((false, nil))
            }
        }
    }
}
