//
//  EmployeeConsultationPresenter.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 12/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

protocol EmployeeConsultationDelegate: NSObjectProtocol {
    func OnSuccessEmployeeConsultation(mEmployeeConsultationResponse: EmployeeConsultationResponse)
}
class EmployeeConsultationPresenter: BaseVentastpredesignPresenter {
    var mEmployeeConsultationDelegate: EmployeeConsultationDelegate!
    var numEmployee = ""

    func GetEmployeeConsultation(mFolioEmpleado: String, mEmployeeConsultationDelegate: EmployeeConsultationDelegate) {
        self.mEmployeeConsultationDelegate = mEmployeeConsultationDelegate

        let employeeRequest = EmployeeConsultationRequest()
        employeeRequest.MInfo = Info()
        employeeRequest.MInfo?.FolioEmpleado = mFolioEmpleado
        employeeRequest.MLogin = Login()
        employeeRequest.MLogin?.Ip = "1.1.1.1"
        employeeRequest.MLogin?.User = "25631"
        employeeRequest.MLogin?.SecretWord = "Middle100$"

        if  RealManager.findFirst(object: Login.self) != nil {
            RealManager.deleteAll(object: Login.self)
        }
        let login = Login()
        login.Ip = Security.crypt(text: "1.1.1.1")
        login.User = Security.crypt(text: "25631")
        login.SecretWord = Security.crypt(text: "Middle100$")
         _ = RealManager.insert(object: login)

        RetrofitManager<EmployeeConsultationResponse>.init(requestUrl: ApiDefinition.API_QUERY_EMPLOYEES_BY_TICKET, delegate: self).request(requestModel: employeeRequest)
    }
    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        view.hideLoading()
        if requestUrl == ApiDefinition.API_QUERY_EMPLOYEES_BY_TICKET {
            OnSuccessEmployeeConsultation(employeeConsultationResponse: response as! EmployeeConsultationResponse)
        }
    }

    func OnSuccessEmployeeConsultation(employeeConsultationResponse: EmployeeConsultationResponse) {
        if employeeConsultationResponse.MResult?.Result == nil {
            view.hideLoading()
            Constants.Alert(title: "", body: dialogs.Error.InternService, type: type.Error, viewC: mViewController)
        } else if employeeConsultationResponse.MResult?.Description != "Success operation" {
            view.hideLoading()
            Constants.Alert(title: "", body: (employeeConsultationResponse.MResult?.Description)!, type: type.Error, viewC: mViewController)
        } else {
            mEmployeeConsultationDelegate.OnSuccessEmployeeConsultation(mEmployeeConsultationResponse: employeeConsultationResponse)
            view.hideLoading()
        }

    }

}
