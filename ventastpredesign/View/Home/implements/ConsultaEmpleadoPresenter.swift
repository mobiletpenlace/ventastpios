//
//  ConsultaEmpleadoPresenter.swift
//  ventastpredesign
//
//  Created by apple on 1/14/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import UIKit
import Foundation

protocol ConsultaEmpleadoDelegate: NSObjectProtocol {
    func devuelveDatos(distrito: String, id: String)
    func changeStatusEmpleado()
}

class ConsultaEmpleadoPresenter: BaseVentastpredesignPresenter {
    var delegado: ConsultaEmpleadoDelegate!
    var service: ConsultaEmpleadoService!

    init(service: ConsultaEmpleadoService, view: BaseVentasView) {
        super.init(view: view)
        self.service = service

    }

    func attachView(delegado: ConsultaEmpleadoDelegate) {
        self.delegado = delegado
    }

    func viewDidShow() {
    }

    func getConsultaEmpleado(noEmpleado: String, viewC: UIViewController) {
        service?.getConsultaEmpleado(NoEmpleado: noEmpleado) { (res, empleado) in
            if res {
                Logger.println("jalo la consulta del empleado")
                self.delegado?.devuelveDatos(distrito: (empleado?.empleado?.Distrito)!, id: (empleado?.empleado?.Id)!)
            } else {
                let alerta = UIAlertController(title: "Error", message: "No se pudo consultar el empleado", preferredStyle: .alert)

                alerta.addAction(UIAlertAction(title: "CONTINUAR", style: .default, handler: {[weak alerta](_) in
                    Logger.println("Error al consultar el empleado")
                    self.view.hideLoading()
                    self.delegado?.changeStatusEmpleado()
                }))
                viewC.present(alerta, animated: true, completion: nil)
            }
        }
    }
}
