//
//  TokenPresenter.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 14/01/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import Foundation
import UIKit

protocol TokenDelegate: NSObjectProtocol {
    func getInfoEmploye()
    func changeStatusToken()
}

class TokenPresenter: BaseVentastpredesignPresenter {

    weak fileprivate var viewC: TokenDelegate?
    fileprivate var service: TokenService?
//    fileprivate var data = [tablaVentasModel]()
//    fileprivate var dataAuxiliar = [tablaVentasModel]()

    init(service: TokenService, view: BaseVentasView) {
        super.init(view: view)
        self.service = service
    }

    func attachView(viewC: TokenDelegate) {
        self.viewC = viewC
    }

    func viewDidShow() {
    }

    func getToken() {
        view.showLoading(message: "")
        service?.getToken { (res, token) in
            if res {
                Logger.println("el token generado es: \(token)")
                statics.TOKEN = token
                self.viewC?.getInfoEmploye()
            } else {
                let alerta = UIAlertController(title: "Error", message: "No se ha podido realizar la operacion intente mas tarde", preferredStyle: .alert)

                alerta.addAction(UIAlertAction(title: "CONTINUAR", style: .default, handler: {[weak alerta](_) in
                    Logger.println("No se pudo generar el token")
                    self.view.hideLoading()
                    self.viewC?.changeStatusToken()
                }))
            }
        }
    }

}
