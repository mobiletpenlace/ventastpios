//
//  SalesmenMonitoringView.swift
//  SalesCloud
//
//  Created by Ulises Ortega on 21/10/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class SalesmenMonitoringView: UIViewController {

    private let viewModel = SalesmenMonitoringViewModel()
    private var salesmenTotal: [CoachSaleReport] = []
    private var salesmenFiltered: [CoachSaleReport] = []
    private let searchController = UISearchController()
    private var employeeNum: String
    private var calendarButton: UIButton!

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noContentLabel: UILabel!

    deinit {
        print("DEINIT SalesmenMonitoring VC")
        SwiftEventBus.unregister(self)
//        ProgressHUD.dismiss()
    }

    init(employeeNum: String) {
        self.employeeNum = employeeNum
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configTableView()
        configNavigationView()
        loadSales()
//        loadProspects()
        view.isUserInteractionEnabled = false
        ProgressHUD.show()

        SwiftEventBus.onMainThread(self, name: "SalesmenLoaded", handler: { [weak self] result in
            guard let salesmen = result?.object as? [CoachSaleReport] else {return}
            if salesmen.isEmpty {
                self?.noContentLabel.isHidden = false
            }
            self?.salesmenTotal = salesmen
            self?.salesmenFiltered = salesmen
            self?.tableView.reloadData()
            self?.view.isUserInteractionEnabled = true
            ProgressHUD.dismiss()
        })

        SwiftEventBus.onMainThread(self, name: "Salesmen_Filtered", handler: { [weak self] result in
            guard let salesmenFiltered = result?.object as? [CoachSaleReport] else {return}

            if salesmenFiltered.count < 1 {
                self?.noContentLabel.isHidden = false
            } else {
                self?.noContentLabel.isHidden = true
            }
            self?.salesmenFiltered = salesmenFiltered
            self?.tableView.reloadData()
        })

    }

    func configTableView() {
        // TODO: Fix this with the new celltype
        tableView.register(UINib(nibName: "SalesmenTableViewCell", bundle: nil), forCellReuseIdentifier: "salesmanCell")

        tableView.delegate = self
        tableView.dataSource = self
    }

    func configNavigationView() {
        self.navigationController?.navigationBar.prefersLargeTitles = true
        // TODO: Check if this title is correct
        self.title = "Monitoreo de Vendedores"
        self.navigationItem.setValue(1, forKey: "__largeTitleTwoLineMode")
        self.navigationItem.searchController = searchController
        self.navigationItem.searchController?.searchBar.delegate = self
        self.navigationItem.searchController?.searchBar.setValue("Cancelar", forKey: "cancelButtonText")
//        self.navigationItem.searchController?.searchBar.showsScopeBar = false
        self.navigationItem.hidesSearchBarWhenScrolling = false
        self.navigationItem.searchController?.searchBar.placeholder = "Buscar por nombre o cuenta"
        self.navigationController?.navigationBar.topItem?.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "Back_Arrow_Black"), style: .plain, target: self, action: #selector(backAction))
        self.navigationController?.navigationBar.topItem?.leftBarButtonItem?.tintColor = .black
    }

    @objc func backAction() {
        print("BackPressed")
        self.navigationController?.dismiss(animated: false)
        self.navigationController?.viewControllers.removeAll()
    }

    func loadSales() {
        viewModel.loadSales(employeeNum: employeeNum)
    }

    func filterSalesmen(string: String) {
        viewModel.filterSalesmen(string: string, salesmen: salesmenTotal)
    }

}

@available(iOS 13.0, *)
// TODO: Fix this
extension SalesmenMonitoringView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "salesmanCell", for: indexPath) as! SalesmenTableViewCell
        cell.saleReport = salesmenFiltered[indexPath.section]
        cell.configCell()
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        // TODO: this is only test fix with returning array count
        return salesmenFiltered.count
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // TODO: Open new detail view in this section
        let selectedSaleReport = salesmenFiltered[indexPath.section]
//        ProgressHUD.show()
        viewModel.getEmpid(employeeNum: selectedSaleReport.numEmpleado) {[weak self] (result) in
            // TODO: Navigation controller shouldn't be presented like this on top of other NC, improve implementation
            print(result)
            let navigation = UINavigationController()
            navigation.modalPresentationStyle = .fullScreen
            let vc = MonitoringTrayView(employeeID: result, employeeName: selectedSaleReport.nombreEmpleado)
            navigation.pushViewController(vc, animated: false)
            self?.present(navigation, animated: false)
        }

    }
}

@available(iOS 13.0, *)
extension SalesmenMonitoringView: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // Implement search function
        filterSalesmen(string: searchText)
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        // Implement search function
        filterSalesmen(string: "")
    }
}
