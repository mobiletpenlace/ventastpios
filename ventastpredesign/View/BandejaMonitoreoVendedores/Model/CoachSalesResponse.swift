//
//  CoachSalesResponse.swift
//  SalesCloud
//
//  Created by Ulises Ortega on 23/10/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class CoachSalesResponse: BaseResponse {

    var colaboradores: [CoachSaleReport] = []
    var error: String = ""

    required init?(map: Map) {
    }

    override func mapping(map: Map) {
        colaboradores <- map["datos.colaboradores"]
        error <- map["error"]
    }
}
