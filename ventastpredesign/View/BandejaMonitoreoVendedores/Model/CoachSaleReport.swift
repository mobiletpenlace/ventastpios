//
//  CoachSaleReport.swift
//  SalesCloud
//
//  Created by Ulises Ortega on 23/10/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class CoachSaleReport: NSObject, Mappable {

    var numEmpleado: String = ""
    var nombreEmpleado: String = ""
    var sumaVentas: Int?
    var sumaAprobadas: Int?
    var sumaPendientes: Int?
    var sumaRechazadas: Int?
//    var aprobadas: [SaleReportDetails] = []
//    var pendientes: [SaleReportDetails] = []
//    var rechazadas: [SaleReportDetails] = []

    required init?(map: Map) {
    }
    override init() {

    }
    func mapping(map: Map) {
        numEmpleado <- map["noEmpleado"]
        nombreEmpleado <- map["name"]
        sumaVentas <- map["ventas.sumaVentas"]
//        sumaAprobadas <- map["sumaAprobadas"]
//        sumaPendientes <- map["sumaPendientes"]
//        sumaRechazadas <- map["sumarechazadas"]
//        aprobadas <- map["aprobadas"]
//        pendientes <- map["pendientes"]
//        rechazadas <- map["rechazadas"]
    }
}
