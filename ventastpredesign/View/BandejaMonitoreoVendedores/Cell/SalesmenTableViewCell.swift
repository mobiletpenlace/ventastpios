//
//  SalesmenTableViewCell.swift
//  SalesCloud
//
//  Created by Ulises Ortega on 21/10/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

class SalesmenTableViewCell: UITableViewCell {

    @IBOutlet weak var employeeNumLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var salesCounterLabel: UILabel!

    var saleReport: CoachSaleReport?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configCell() {
        nameLabel.text = saleReport?.nombreEmpleado
        employeeNumLabel.text = saleReport?.numEmpleado
        salesCounterLabel.text = String(saleReport?.sumaVentas ?? 0)
    }

}
