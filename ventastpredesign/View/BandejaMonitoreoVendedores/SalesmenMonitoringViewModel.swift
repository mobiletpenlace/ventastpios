//
//  SalesmenMonitoringViewModel.swift
//  SalesCloud
//
//  Created by Ulises Ortega on 21/10/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import Alamofire
import SwiftyJSON
import SwiftEventBus

class SalesmenMonitoringViewModel {

    func loadSales(employeeNum: String) {
        let format: DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            return formatter
        }()
//        let id = employee.Id
        // TODO: Change this for the dates required ( now only considering 2 month old data )
        guard let startDate = Calendar.current.date(byAdding: .month, value: -2, to: Date()) else { return }
        let endDate = Date()
//              let endDate = Calendar.current.date(byAdding: .month, value: 1, to: Date()) else { return }
        let startDateString = format.string(from: startDate)
        let endDateString = format.string(from: endDate)
        print("START DATE = \(startDateString) ENDATE = \(endDateString)")
        let parameters: Parameters = [
            "numEmpleado": employeeNum,
            "strFechaFin": endDateString,
            "strFechaInicio": startDateString
        ]
        if ConnectionUtils.isConnectedToNetwork() {
            // Connection is active
            Alamofire.request(AppDelegate.API_SALESFORCE + ApiDefinition.API_REPORTS, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: ApiDefinition.headersToken).responseJSON {[weak self] response in
                guard let self = self else {return}
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print("JSONRESPONSECOLAB: \(json)")
                    //                        let oportunidadResponse = OportunidadResponse(JSONString: json.rawString() ?? "")
                    let salesReportResponse = CoachSalesResponse(JSONString: json.rawString() ?? "")
                    //                        guard let oportunidades = oportunidadResponse?.oportunidades else {return}
                    //                        let oportunidadesJSON: String = AES128.shared.decode(coded: oportunidades) ?? ""
                    let colaboradores = salesReportResponse?.colaboradores ?? []

                    for colaborador in colaboradores {
                        print("IDempleado: \(colaborador.numEmpleado) ")
                    }
                    SwiftEventBus.post("SalesmenLoaded", sender: colaboradores)
                case .failure(let error):
                    print(error)
                }
            }
        } else {
            // Conection error
        }
    }

    func getEmpid(employeeNum: String, handler: @escaping (String) -> Void) {
        guard let folio = AES128.shared.code(plainText: employeeNum) else { return }
        print("Folio : \(folio) ")
        let parameters: Parameters = [
            "folioEmpleado": folio
        ]
        if ConnectionUtils.isConnectedToNetwork() {
            // Connection is active
            Alamofire.request(AppDelegate.API_SALESFORCE + ApiDefinition.API_EMPLEADO_PAIS, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: ApiDefinition.headersToken).responseJSON {[weak self] response in
                guard let self = self else {return}
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print("JSONRESPONSEEMPLOYEE: \(json)")
                    guard let employeeResponse = EmployeResponse(JSONString: json.rawString() ?? "") else { return }
                    guard let jsonEmp: String =  AES128.shared.decode(coded: employeeResponse.empleado ?? "") else { return }
                    guard let employe: Employe = Employe(JSONString: jsonEmp) else { return }
                    handler(employe.idEmpleado)
                case .failure(let error):
                    print(error)
                }
            }
        } else {
            // Conection error
        }
    }

    func filterSalesmen(string: String, salesmen: [CoachSaleReport]) {
        let text = string.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        var newSalesmen: [CoachSaleReport] = []
        if text.isNumeric {
            newSalesmen = salesmen.filter { $0.numEmpleado.lowercased().contains(string: text.lowercased()) }
        } else if text.isEmpty {
            newSalesmen = salesmen
        } else {
            newSalesmen = salesmen.filter { $0.nombreEmpleado.lowercased().contains(string: text.lowercased()) }
        }
        SwiftEventBus.post("Salesmen_Filtered", sender: newSalesmen)
    }
}
