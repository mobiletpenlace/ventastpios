//
//  BienestarViewController.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 25/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class BienestarViewController: BaseVentasView {

    @IBOutlet weak var ViewTraining: UIView!
    @IBOutlet weak var ViewChampions: UIView!
    @IBOutlet weak var ViewOther: UIView!
    let realm = RealManager.mReal()
    let employed: Empleado = Empleado(value: RealManager.findFirst(object: Empleado.self)!)
    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        ViewOther.isHidden = true
        design()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }

    func design() {
        Constants.Border(view: ViewTraining, radius: 6, width: 1)
        Constants.Border(view: ViewChampions, radius: 6, width: 1)
        Constants.Border(view: ViewOther, radius: 6, width: 1)
        Constants.Gradient(view: self.ViewTraining, frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.size.width - 40, height: self.ViewTraining.frame.size.height), colors: [Colors.White, Colors.TransWhite, Colors.LowGray])
        Constants.Gradient(view: self.ViewChampions, frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.size.width - 40, height: self.ViewChampions.frame.size.height), colors: [Colors.White, Colors.TransWhite, Colors.LowGray])
        Constants.Gradient(view: self.ViewOther, frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.size.width - 40, height: self.ViewOther.frame.size.height), colors: [Colors.White, Colors.TransWhite, Colors.LowGray])
    }

    @IBAction func ActionChampions(_ sender: Any) {
        if let url = URL(string: "https://es.surveymonkey.com/r/MDGNZ5V") {
            UIApplication.shared.open(url, options: [:])
        }
    }

    @IBAction func ActionTraining(_ sender: Any) {
        if let url = URL(string: "https://recursoshumanos.gruposalinas.com.mx/lmsPlus/app/capacitacion/plantpy?paramNumEmpleado=\(employed.FolioEmpleado)&paramToken=7uod60Fx00bf0125c9ap") {
            UIApplication.shared.open(url, options: [:])
        }
    }

    @IBAction func Actionback(_ sender: Any) {
        Constants.Back(viewC: self)
    }

}
