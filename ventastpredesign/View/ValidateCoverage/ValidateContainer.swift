//
//  ValidateCoverageContainer.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 9/9/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import UIKit

class ValidateCoverageContainer: BaseVentasView {

    @IBOutlet weak var
    UIView!
    var viewAlert: UIStoryboard?
    var controller: UIViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        goValidateCoverage()
    }

    @IBAction func goBack(_ sender: Any) {
        Constants.Back(viewC: self)
    }

    func SetView(num: Int, nombre: String) {
        viewAlert = UIStoryboard(name: nombre, bundle: nil)
        controller = viewAlert!.instantiateViewController(withIdentifier: nombre)
        addChildViewController(controller!)
        self.StepOneContainer.addSubview((controller?.view)!)
        controller?.didMove(toParentViewController: self)

    }

    func goValidateCoverage() {
       let viewAlert = UIStoryboard(name: "ValidateCoverageContainer", bundle: nil)
        let controller = viewAlert.instantiateViewController(withIdentifier: "ValidateCoverageContainer")
        StepOneContainer.addSubview(controller.view)

//        let viewAlert = UIStoryboard(name: "ValidateCoverageContainer", bundle:nil)
//        let controller = viewAlert.instantiateViewController(withIdentifier: "ValidateCoverageContainer")
//      //  addChildViewController(controller)
//        self.ValidateContainer.addSubview((controller.view)!)
//        controller.didMove(toParentViewController: self)

    }

}
