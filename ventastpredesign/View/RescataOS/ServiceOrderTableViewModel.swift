//
//  ServiceOrderTableViewModel.swift
//  SalesCloud
//
//  Created by Ulises Ortega on 07/02/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import Alamofire
import SwiftyJSON
import SwiftEventBus

class ServiceOrderTableViewModel {

    func loadOSProspects() {
        AES128.shared.setKey(key: statics.key)
        if RealManager.findFirst(object: Empleado.self) != nil {
            // Empleado exisits in Realm
            let empleado = Empleado(value: RealManager.findFirst(object: Empleado.self)!)
            let id = empleado.Id
            let parameters: Parameters = [
                "accion": "ConsultarOpo",
                "idEmpleado": id
            ]
            if ConnectionUtils.isConnectedToNetwork() {
                // Connection is active
                Alamofire.request(AppDelegate.API_SALESFORCE + ApiDefinition.API_CONSULTA_OS, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: ApiDefinition.headersToken).responseJSON {[weak self] response in
                    guard let self = self else {return}
                    switch response.result {
                    case .success(let value):
                        let json = JSON(value)
                        print(json)
                        guard let oportunidades = OportunidadOSListResponse(JSONString: json.rawString() ?? "") else {return}
                        SwiftEventBus.post("osProspectsLoaded", sender: oportunidades.listainfoOS)
                    case .failure(let error):
                        print(error)
                    }
                }
            } else {
                // Conection error
            }
        } else {
            // Empleado not found in Realm
            print("ERROR, no se encontró el empleado")
        }
    }

    func filterProspects(string: String, prospects: [OportunidadOSResponse], selectedStatus: String) {
        let text = string.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        var newProspects: [OportunidadOSResponse] = []
        if selectedStatus != "Todas" {
            if text.isNumeric {
                newProspects = prospects.filter { $0.OS[0].cuenta?.contains(string: text) ?? false && $0.OS[0].estatus?.lowercased() == selectedStatus.lowercased() }
            } else if text.isEmpty {
                newProspects = prospects.filter { $0.OS[0].estatus?.lowercased() == selectedStatus.lowercased() }
            } else {
                newProspects = prospects.filter { $0.OS[0].nombre?.lowercased().contains(string: text.lowercased()) ?? false && $0.OS[0].estatus?.lowercased() == selectedStatus.lowercased() }
            }
        } else {
            if text.isNumeric {
                newProspects = prospects.filter { $0.OS[0].cuenta?.contains(string: text) ?? false}
            } else if text.isEmpty {
                newProspects = prospects
            } else {
                newProspects = prospects.filter { $0.OS[0].nombre?.lowercased().contains(string: text.lowercased()) ?? false}
            }
        }
        SwiftEventBus.post("OS_Filtered", sender: newProspects)
    }

}
