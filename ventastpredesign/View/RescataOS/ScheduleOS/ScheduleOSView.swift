//
//  ScheduleOSView.swift
//  SalesCloud
//
//  Created by Ulises Ortega on 16/02/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//
import UIKit
import SwiftEventBus

@available(iOS 13.0, *)
class ScheduleOSView: UIViewController {

    var oportunidadOS: OportunidadOSResponse?
    let viewModel = ScheduleOSViewModel()
    var currentDate = Date()
    var sortedAvailability: [DayAvailability] = []

    var calendarPrimaryAction = false

    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var morningButton: UIButton!
    @IBOutlet weak var eveningButton: UIButton!
    @IBOutlet weak var confirmationLabel: UILabel!
    @IBOutlet weak var finishButton: UIButton!

    deinit {
        SwiftEventBus.unregister(self)
        ProgressHUD.dismiss()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        ProgressHUD.show()
        checkAvailability()

        SwiftEventBus.onMainThread(self, name: "SalesAvailabilityChecked", handler: { [weak self] result in
            guard let availability = result?.object as? [DayAvailability] else {return}
            let df = DateFormatter()
            df.dateFormat = "yyyy-MM-dd"
            df.locale = Locale(identifier: "es_MX")
            if availability.isEmpty {
                // Include actions when array is empty
                self?.datePicker.isEnabled = false
            } else {
                print("ENTRO AL ELSE DE SALESAVAILABILITY")
//                let sortedDays = availability.sorted{ df.date(from: $0.fecha ?? "") ?? Date() < df.date(from: $1.fecha ?? "") ?? Date() }
                self?.sortedAvailability = availability.sorted { df.date(from: $0.fecha ?? "") ?? Date() < df.date(from: $1.fecha ?? "") ?? Date() }
                self?.datePicker.minimumDate = df.date(from: self?.sortedAvailability.first!.fecha ?? "") ?? Date()
                self?.datePicker.maximumDate = df.date(from: self?.sortedAvailability.last!.fecha ?? "") ?? Date()
                self?.datePicker.setDate(df.date(from: self?.sortedAvailability.first!.fecha ?? "") ?? Date(), animated: true)
                self?.currentDate = df.date(from: self?.sortedAvailability.first!.fecha ?? "") ?? Date()

                // Button availability
                guard let sortedAvailability = self?.sortedAvailability else {return}
                for day in sortedAvailability {
                    if df.date(from: day.fecha ?? "") ?? Date() == self?.currentDate {
                        print("DAY: \(day.fecha ?? "--/--/--") Matutino \(day.matutino?.intValue ?? 500) Vespertino\(day.vespertino?.intValue ?? 500)")
                        self?.morningButton.isEnabled = day.matutino?.intValue ?? 0 > 0
                        self?.eveningButton.isEnabled = day.vespertino?.intValue ?? 0 > 0
                    }
                }

                self?.datePicker.isEnabled = true
                ProgressHUD.dismiss()
            }
        })
        SwiftEventBus.onMainThread(self, name: "RescheduleSuccess", handler: { [weak self] _ in
            ProgressHUD.showSuccess("Reagendado correctamente")
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5, execute: {
                SwiftEventBus.post("SuccessRescheduled")
                self?.dismiss(animated: true)
            })
        })

        // Do any additional setup after loading the view.
    }

    func checkAvailability() {
        viewModel.checkAvailability(oportunidad: oportunidadOS)
    }

    @IBAction func backButtonAction(_ sender: Any) {
        dismiss(animated: false)
    }

    @IBAction func buttonActionMorning(_ sender: UIButton) {
        morningButton.isSelected = true
        eveningButton.isSelected = false
        confirmationLabel.isHidden = false
        guard let schedule = sender.titleLabel?.text else {return}
        updateConfirmationLabel(schedule: schedule)
        finishButton.isEnabled = true
    }

    @IBAction func buttonActionEvening(_ sender: UIButton) {
        eveningButton.isSelected = true
        morningButton.isSelected = false
        confirmationLabel.isHidden = false
        guard let schedule = sender.titleLabel?.text else {return}
        updateConfirmationLabel(schedule: schedule)
        finishButton.isEnabled = true
    }

    func updateConfirmationLabel(schedule: String) {
        let df = DateFormatter()
        df.dateFormat = "LLLL"
        df.locale = Locale(identifier: "es_MX")
        let day = currentDate.get(.day)
        let month = df.string(from: currentDate)
        confirmationLabel.text = "Confirma tu visita para el día \(String(format: "%02d", day)) de \(month) en un horario \(schedule)"
    }

    @IBAction func tapGestureRecognizer(_ sender: Any) {
        print("tapGestureEntered")
        // TODO: Find a way to make this cleaner
        // This is a quick fix for native datePicker inline calendar bugged behavior in old devices (not sure about whats failing)
        if self.datePicker.subviews.count > 0 {
            print("Entro al primer if \(self.datePicker.subviews)")
            if self.datePicker.subviews[0].subviews.count > 0 {
                print("Entro al segundo if \(self.datePicker.subviews[0].subviews)")
                if self.datePicker.subviews[0].subviews[0].subviews.count > 1 {
                    print("Entro al tercer if \(self.datePicker.subviews[0].subviews[0].subviews)")
                    if self.datePicker.subviews[0].subviews[0].subviews[1].subviews.count > 1 {
                        print("Entro al cuarto if \(self.datePicker.subviews[0].subviews[0].subviews[1].subviews)")
                        if let collectionView = self.datePicker.subviews[0].subviews[0].subviews[1].subviews[1] as? UICollectionView {
                            collectionView.contentOffset.x += 1
                        } else {
                            // Device calendar built different
                        }
                    }
                }
            }
        }
    }

    @IBAction func finishButtonAction(_ sender: Any) {
        ProgressHUD.show()
        finishButton.isEnabled = false
        morningButton.isEnabled = false
        eveningButton.isEnabled = false
        datePicker.isEnabled = false

        print("Date: \(datePicker.date)")
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        df.locale = Locale(identifier: "es_MX")

        let dateString = df.string(from: currentDate)

        if morningButton.isSelected {
            viewModel.scheduleOS(oportunidad: oportunidadOS, date: dateString, shift: "1")
        } else if eveningButton.isSelected {
            viewModel.scheduleOS(oportunidad: oportunidadOS, date: dateString, shift: "2")
        } else {
            return
        }

    }
    @IBAction func datePickerTouchDown(_ sender: UIDatePicker) {
        print("TouchedDown")
    }

    @IBAction func datePickerClickedOutside(_ sender: UIDatePicker) {
        print("Do nothing OUTSIDE")
    }
    @IBAction func datepickerPrimaryActionTriggered(_ sender: UIDatePicker) {
        print("Primary action TRIGGERED")
        datePicker.isUserInteractionEnabled = true
        currentDate = datePicker.date

        // Set schdule buttons as unselected
        confirmationLabel.isHidden = true
        morningButton.isEnabled = false
        eveningButton.isEnabled = false
        morningButton.isSelected = false
        eveningButton.isSelected = false
        finishButton.isEnabled = false

        // Enable or disable schedule buttons functionality
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        df.locale = Locale(identifier: "es_MX")

        for day in sortedAvailability {
            if df.date(from: day.fecha ?? "") ?? Date() == currentDate {
                print("DAY: \(day.fecha ?? "--/--/--") Matutino \(day.matutino?.intValue ?? 500) Vespertino\(day.vespertino?.intValue ?? 500)")
                morningButton.isEnabled = day.matutino?.intValue ?? 0 > 0
                eveningButton.isEnabled = day.vespertino?.intValue ?? 0 > 0
            }
        }

    }
    @IBAction func datePickerClicked(_ sender: UIDatePicker) {
        print("Do nothing INSIDE")
    }
    @IBAction func datePickerValueChanged(_ sender: UIDatePicker) {
        print("DATE:\(sender.date) DAY: \((sender.date.weekday))")
    }

}
