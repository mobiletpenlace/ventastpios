//
//  ScheduleOSViewModel.swift
//  SalesCloud
//
//  Created by Ulises Ortega on 20/02/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import Alamofire
import SwiftyJSON
import SwiftEventBus

class ScheduleOSViewModel {

    func checkAvailability(oportunidad: OportunidadOSResponse?) {
        guard let cuenta = oportunidad?.OS[0].cuenta else {return}
        let parameters: Parameters = [
            "numeroCuenta": cuenta,
            "subtipoOrden": "49"
        ]
//        print("NUM CUENTA %%% : \(cuenta) LINK: \(AppDelegate.API_TOTAL + ApiDefinition.API_SALES_AVAILABILITY)")
        if ConnectionUtils.isConnectedToNetwork() {
            // Connection is active
            Alamofire.request(AppDelegate.API_TOTAL + ApiDefinition.API_SALES_AVAILABILITY, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: ApiDefinition.headersFfmapp).responseJSON {[weak self] response in
                guard let self = self else {return}
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print(json)
                    guard let availabilityResponse = SalesAvailabilityResponse(JSONString: json.rawString() ?? "") else {return}
                    guard let availability = availabilityResponse.disponibilidad?.dia else {return}
//                    print("Availability %%%: \(availability.toJSONString(prettyPrint: true)) ")
                    SwiftEventBus.post("SalesAvailabilityChecked", sender: availability)
                case .failure(let error):
                    print(error)
                }
            }
        } else {
            // Conection error
        }
    }

    func scheduleOS(oportunidad: OportunidadOSResponse?, date: String, shift: String) {
        guard let idOTFFM = oportunidad?.OS[0].idOT else {return}
        let parameters: Parameters = [
            "comentarios": "Se Reagenda OT",
            "fechaAgendamiento": date,
            "hora": "",
            "idOTFFM": idOTFFM,
            "motivo": "169",
            "turno": shift
        ]
        if ConnectionUtils.isConnectedToNetwork() {
            // Connection is active
            Alamofire.request(AppDelegate.API_TOTAL + ApiDefinition.API_SALES_RESCHEDULE, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: ApiDefinition.headersFfmapp).responseJSON {[weak self] response in
                guard let self = self else {return}
                print("Entro al request RESCHEDULE ")
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print("Entro al success RESCHEDULE ")
                    print(json)
                    guard let response = RescheduleOTResponse(JSONString: json.rawString() ?? "") else {return}
                    if response.resultDescription.contains(string: "correctamente") {
                        print("Entro al Correctamente RESCHEDULE ")
                        SwiftEventBus.post("RescheduleSuccess")
                    }
                case .failure(let error):
                    print(error)
                }
            }
        } else {
            // Conection error
        }
    }

}
