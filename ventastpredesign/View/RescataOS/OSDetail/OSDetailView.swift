//
//  OSDetailView.swift
//  SalesCloud
//
//  Created by Ulises Ortega on 16/02/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

@available(iOS 13.0, *)
class OSDetailView: UIViewController {

    var OSProspect: OportunidadOSResponse?

    @IBOutlet weak var closeButton: UIButton!

    // OS Detail SECTION
    @IBOutlet weak var osDetailSection: UIView!
    @IBOutlet weak var workOrderLabel: UILabel!
    @IBOutlet weak var accNumLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var closingDateLabel: UILabel!
    @IBOutlet weak var confirmationDateLabel: UILabel!
    @IBOutlet weak var confirmedLabel: UILabel!
    @IBOutlet weak var squareLabel: UILabel!
    @IBOutlet weak var districtLabel: UILabel!
    @IBOutlet weak var clusterLabel: UILabel!

    // Comments SECTION
    @IBOutlet weak var commentsSection: UIView!
    // Comments are printed out in a label for now
    @IBOutlet weak var commentsLabel: UILabel!

    // Order management SECTION
    @IBOutlet weak var orderManagementSection: UIView!
    @IBOutlet weak var scheduleOSButton: UIButton!

    @IBAction func closeButtonAction(_ sender: Any) {
        dismiss(animated: false)
    }

    @available(iOS 13.0, *)
    @IBAction func scheduleButtonAction(_ sender: Any) {
        let vc = ScheduleOSView()
        vc.oportunidadOS = OSProspect
        vc.modalPresentationStyle = .overFullScreen
        present(vc, animated: false)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configProspectDetail()

        SwiftEventBus.onMainThread(self, name: "SuccessRescheduled", handler: { [weak self] _ in
            ProgressHUD.showSuccess("Reagendado correctamente")
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5, execute: {
                SwiftEventBus.post("UpdateOS")
                self?.dismiss(animated: false)
            })
        })
        // Do any additional setup after loading the view.
    }

    func configProspectDetail() {
        guard let OSProspect = OSProspect else {
            return
        }
        // UI config
        closeButton.setTitle("", for: .normal)
        view.backgroundColor = .black.withAlphaComponent(0.8)
        // Fill OS Detail Section
        accNumLabel.text = OSProspect.OS[0].cuenta
        nameLabel.text = OSProspect.OS[0].nombre
        workOrderLabel.text = OSProspect.OS[0].idOT
        if let closingDate = OSProspect.fechacierre {
            closingDateLabel.text = String(closingDate.split(separator: "T")[0])
        }
        if let confirmationDate = OSProspect.fechacierre {
            confirmationDateLabel.text = String(confirmationDate.split(separator: "T")[0])
        }
//        confirmationDateLabel.text = OSProspect.OS[0].fechaconfirmada
        confirmedLabel.text = OSProspect.OS[0].confirmada ?? false ? "Sí" : "No"
        squareLabel.text = OSProspect.OS[0].plaza
        districtLabel.text = OSProspect.OS[0].distrito
        clusterLabel.text = OSProspect.OS[0].cluster
        // Fill Comments Section
        commentsLabel.text = OSProspect.OS[0].comentarios
        // Enable or disable schedule button
        scheduleOSButton.isEnabled = OSProspect.OS[0].estatus == "Rescate"

    }
}
