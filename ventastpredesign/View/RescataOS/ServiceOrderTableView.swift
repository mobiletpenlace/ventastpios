//
//  ServiceOrderTableView.swift
//  SalesCloud
//
//  Created by Ulises Ortega on 07/02/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

@available(iOS 13.0, *)
class ServiceOrderTableView: UIViewController {

    let viewModel = ServiceOrderTableViewModel()
    var prospectsTotal: [OportunidadOSResponse] = []
    var prospectsFiltered: [OportunidadOSResponse] = []
    var scopes: [String] = ["Todas"]
    let searchController = UISearchController()

    @IBOutlet weak var noContentLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!

    deinit {
        print("DEINIT ServiceOrderTable VC")
        SwiftEventBus.unregister(self)
        ProgressHUD.dismiss()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.loadOSProspects()
        configNavigationView()
        configTableView()
        ProgressHUD.show()

        SwiftEventBus.onMainThread(self, name: "osProspectsLoaded", handler: {[weak self] result in
            guard let prospects = result?.object as? [OportunidadOSResponse] else {return}
            if prospects.count == 0 {
                self?.noContentLabel.isHidden = false
            }
            self?.prospectsTotal = prospects
            self?.prospectsFiltered = prospects

            // Get scopes array
            for prospect in prospects {
                if !(self?.scopes.contains(where: { $0 == prospect.OS[0].estatus}) ?? true) {
                    if let scope = prospect.OS[0].estatus {
                        self?.scopes.append(scope)
                    }
                }
            }
            self?.tableView.reloadData()
            self?.navigationItem.searchController?.searchBar.scopeButtonTitles = self?.scopes
            self?.navigationItem.searchController?.searchBar.showsScopeBar = true

            ProgressHUD.dismiss()
        })

        SwiftEventBus.onMainThread(self, name: "OS_Filtered", handler: { [weak self] result in
            guard let prospects = result?.object as? [OportunidadOSResponse] else {return}
            if prospects.count < 1 {
                self?.noContentLabel.isHidden = false
            } else {
                self?.noContentLabel.isHidden = true
            }
            self?.prospectsFiltered = prospects
            self?.tableView.reloadData()
        })

        SwiftEventBus.onMainThread(self, name: "UpdateOS", handler: { [weak self] _ in
            ProgressHUD.showSuccess("Reagendado correctamente")
            self?.viewModel.loadOSProspects()
            // TODO: TEST THIS
            if self?.navigationItem.searchController?.searchBar.scopeButtonTitles?.count ?? 0 > 0 {
                self?.navigationItem.searchController?.searchBar.selectedScopeButtonIndex = 0
            }
        })
    }

    func configNavigationView() {
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.title = "Rescata órdenes"
        self.navigationItem.searchController = searchController
        self.navigationItem.searchController?.searchBar.delegate = self
        self.navigationItem.searchController?.searchBar.setValue("Cancelar", forKey: "cancelButtonText")
        self.navigationItem.searchController?.searchBar.showsScopeBar = false
        self.navigationItem.hidesSearchBarWhenScrolling = false
        self.navigationItem.searchController?.searchBar.placeholder = "Buscar por nombre o cuenta"
        self.navigationController?.navigationBar.topItem?.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "Back_Arrow_Black"), style: .plain, target: self, action: #selector(backAction))
        self.navigationController?.navigationBar.topItem?.leftBarButtonItem?.tintColor = .black
    }

    @objc func backAction() {
        print("BackPressed")
        self.navigationController?.dismiss(animated: false)
        self.navigationController?.viewControllers.removeAll()
    }

    func configTableView() {
        tableView.register(UINib(nibName: "ServiceOrderTableViewCell", bundle: nil), forCellReuseIdentifier: "osCell")
        tableView.delegate = self
        tableView.dataSource = self
    }

    func filterProspects(string: String, prospects: [OportunidadOSResponse], selectedStatus: String) {
        viewModel.filterProspects(string: string, prospects: prospects, selectedStatus: selectedStatus)
    }

}

@available(iOS 13.0, *)
extension ServiceOrderTableView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "osCell", for: indexPath) as! ServiceOrderTableViewCell
        cell.prospect = prospectsFiltered[indexPath.section]
        cell.configureCell()
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return prospectsFiltered.count
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedProspect = prospectsFiltered[indexPath.section]
        let vc = OSDetailView()
        vc.OSProspect = selectedProspect
        vc.modalPresentationStyle = .overFullScreen
        present(vc, animated: false)
    }
}

@available(iOS 13.0, *)
extension ServiceOrderTableView: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let text = searchBar.text else {return}
        filterProspects(string: text, prospects: prospectsTotal, selectedStatus: scopes[searchBar.selectedScopeButtonIndex])
    }

    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        guard let text = searchBar.text else {return}
        filterProspects(string: text, prospects: prospectsTotal, selectedStatus: scopes[searchBar.selectedScopeButtonIndex])
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        filterProspects(string: "", prospects: prospectsTotal, selectedStatus: scopes[searchBar.selectedScopeButtonIndex])
        tableView.reloadData()
    }
}
