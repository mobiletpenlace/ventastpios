//
//  DayAvailability.swift
//  SalesCloud
//
//  Created by Ulises Ortega on 20/02/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class DayAvailability: BaseResponse {

    public required convenience init?(map: Map) {
        self.init()
    }

    override func mapping(map: Map) {
        vespertino <- map["vespertino"]
        matutino <- map["matutino"]
        fecha <- map["fecha"]

    }

    var vespertino: String?
    var matutino: String?
    var fecha: String?
}
