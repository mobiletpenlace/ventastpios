//
//  RescheduleOTResponse.swift
//  SalesCloud
//
//  Created by Ulises Ortega on 07/03/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class RescheduleOTResponse: BaseResponse {

    public required convenience init?(map: Map) {
        self.init()
    }

    override func mapping(map: Map) {
        result <- map["resultId"]
        resultDescription <- map["resultDescripcion"]
    }
}
