//
//  SalesAvailability.swift
//  SalesCloud
//
//  Created by Ulises Ortega on 20/02/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class SalesAvailability: BaseResponse {

    public required convenience init?(map: Map) {
        self.init()
    }

    override func mapping(map: Map) {
        dia <- map["dia"]
    }

    var dia: [DayAvailability] = []
}
