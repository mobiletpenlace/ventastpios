//
//  SalesAvailabilityResponse.swift
//  SalesCloud
//
//  Created by Ulises Ortega on 20/02/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class SalesAvailabilityResponse: BaseResponse {

    public required convenience init?(map: Map) {
        self.init()
    }

    override func mapping(map: Map) {
        cluster <- map["cluster"]
        tipoOrden <- map["tipoOrden"]
        resultId <- map["resultId"]
        disponibilidad <- map["disponibilidad"]
    }

    var cluster: String?
    var tipoOrden: String?
    var resultId: String?
    var disponibilidad: SalesAvailability?
}
