//
//  OportunidadOSResponse.swift
//  SalesCloud
//
//  Created by Ulises Ortega on 08/02/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class OportunidadOSResponse: BaseResponse {

    public required convenience init?(map: Map) {
        self.init()
    }

    override func mapping(map: Map) {
        motivoRechazoMC <- map["motivoRechazoMC"]
        fechacierre <- map["fechacierre"]
        plan <- map["plan"]
        idOportunidad <- map["idoportunidad"]
        nombreoportunidad <- map["nombreoportunidad"]
        nombremepleado <- map["fechacierre"]
        OS <- map["OS"]
        etapa <- map["etapa"]
    }

    var motivoRechazoMC: String?
    var fechacierre: String?
    var plan: String?
    var idOportunidad: String?
    var nombreoportunidad: String?
    var nombremepleado: String?
    var OS: [OportunidadOS] = []
    var etapa: String?

}
