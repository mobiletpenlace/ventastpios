//
//  OportunidadOSResponseList.swift
//  SalesCloud
//
//  Created by Ulises Ortega on 08/02/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class OportunidadOSListResponse: BaseResponse {

    public required convenience init?(map: Map) {
        self.init()
    }

    override func mapping(map: Map) {
        result <- map["result"]
        resultDescription <- map["resultDescription"]
        listainfoOS <- map["listainfoOS"]
    }

    var listainfoOS: [OportunidadOSResponse] = []

}
