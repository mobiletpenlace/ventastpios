//
//  OportunidadOS.swift
//  SalesCloud
//
//  Created by Ulises Ortega on 08/02/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class OportunidadOS: BaseResponse {

    public required convenience init?(map: Map) {
        self.init()
    }

    override func mapping(map: Map) {
        idOT <- map["idOT"]
        idOS <- map["idOS"]
        comentarios <- map["comentarios"]
        calle <- map["calle"]
        numOS <- map["numOS"]
        telefono <- map["telefono"]
        plaza <- map["plaza"]
        turno <- map["turno"]
        cuenta <- map["cuenta"]
        distrito <- map["distrito"]
        tipoOrden <- map["tipoOrden"]
        cluster <- map["cluster"]
        confirmada <- map["confirmada"]
        estatus <- map["estatus"]
        fechaconfirmada <- map["fechaconfirmada"]
        colonia <- map["colonia"]
        nombre <- map["nombre"]
        fechadeagendamiento <- map["fechadeagendamiento"]
    }

    var idOT: String?
    var idOS: String?
    var comentarios: String?
    var calle: String?
    var numOS: String?
    var telefono: String?
    var plaza: String?
    var turno: String?
    var cuenta: String?
    var distrito: String?
    var tipoOrden: String?
    var cluster: String?
    var confirmada: Bool?
    var estatus: String?
    var fechaconfirmada: String?
    var colonia: String?
    var nombre: String?
    var fechadeagendamiento: String?

}
