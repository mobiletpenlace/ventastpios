//
//  ServiceOrderTableViewCell.swift
//  SalesCloud
//
//  Created by Ulises Ortega on 08/02/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

class ServiceOrderTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var osNumberLabel: UILabel!
    @IBOutlet weak var accNumberLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!

    var prospect: OportunidadOSResponse?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureCell() {
        nameLabel.text = prospect?.OS[0].nombre
        accNumberLabel.text = prospect?.OS[0].cuenta
        osNumberLabel.text = prospect?.OS[0].numOS
        dateLabel.text = prospect?.OS[0].fechadeagendamiento
        }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
