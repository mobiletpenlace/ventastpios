//
//  CoachesListView.swift
//  SalesCloud
//
//  Created by Ulises Ortega on 24/10/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class CoachesListView: UIViewController {

    private let viewModel = CoachesListViewModel()
    private var coachesTotal: [CoachInfo] = []
    private var coachesFiltered: [CoachInfo] = []
    private let searchController = UISearchController()
    private var employeeNum: String

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noContentLabel: UILabel!

    deinit {
        print("DEINIT CoachesMonitoring VC")
        SwiftEventBus.unregister(self)
        ProgressHUD.dismiss()
    }

    init(employeeNum: String) {
        self.employeeNum = employeeNum
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configTableView()
        configNavigationView()
        loadCoaches()
//        loadProspects()
        view.isUserInteractionEnabled = false
        ProgressHUD.show()

        SwiftEventBus.onMainThread(self, name: "CoachesLoaded", handler: { [weak self] result in
            guard let coaches = result?.object as? [CoachInfo] else {
                self?.noContentLabel.isHidden = false
                ProgressHUD.dismiss()
                return
            }
            if coaches.isEmpty {
                self?.noContentLabel.isHidden = false
            }
            self?.coachesTotal = coaches
            self?.coachesFiltered = coaches
            self?.tableView.reloadData()
            self?.view.isUserInteractionEnabled = true
//            self?.salesmenTotal = salesmen
//            self?.salesmenFiltered = salesmen
//            self?.tableView.reloadData()
//            self?.view.isUserInteractionEnabled = true
            ProgressHUD.dismiss()
        })
//        
        SwiftEventBus.onMainThread(self, name: "CoachesFiltered", handler: { [weak self] result in
            guard let coachesFiltered = result?.object as? [CoachInfo] else {return}

            if coachesFiltered.count < 1 {
                self?.noContentLabel.isHidden = false
            } else {
                self?.noContentLabel.isHidden = true
            }
            self?.coachesFiltered = coachesFiltered
            self?.tableView.reloadData()
        })

    }

    func configTableView() {
        // TODO: Fix this with the new celltype
        tableView.register(UINib(nibName: "CoachesTableViewCell", bundle: nil), forCellReuseIdentifier: "coachCell")

        tableView.delegate = self
        tableView.dataSource = self
    }

    func configNavigationView() {
        self.navigationController?.navigationBar.prefersLargeTitles = true
        // TODO: Check if this title is correct
        self.title = "Monitoreo de Coaches"
        self.navigationItem.setValue(1, forKey: "__largeTitleTwoLineMode")
        self.navigationItem.searchController = searchController
        self.navigationItem.searchController?.searchBar.delegate = self
        self.navigationItem.searchController?.searchBar.setValue("Cancelar", forKey: "cancelButtonText")
//        self.navigationItem.searchController?.searchBar.showsScopeBar = false
        self.navigationItem.hidesSearchBarWhenScrolling = false
        self.navigationItem.searchController?.searchBar.placeholder = "Buscar por nombre o cuenta"
        self.navigationController?.navigationBar.topItem?.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "Back_Arrow_Black"), style: .plain, target: self, action: #selector(backAction))
        self.navigationController?.navigationBar.topItem?.leftBarButtonItem?.tintColor = .black
    }

    @objc func backAction() {
        print("BackPressed")
        self.navigationController?.dismiss(animated: false)
        self.navigationController?.viewControllers.removeAll()
    }

    func loadCoaches() {
        viewModel.loadCoaches(employeeNum: employeeNum)
    }

    func filterCoaches(string: String) {
        viewModel.filterCoaches(string: string, coaches: coachesTotal)
    }

}

@available(iOS 13.0, *)
// TODO: Fix this
extension CoachesListView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "coachCell", for: indexPath) as! CoachesTableViewCell
        cell.coach = coachesFiltered[indexPath.section]
        cell.configCell()
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        // TODO: this is only test fix with returning array count
        return coachesFiltered.count
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // TODO: Open new detail view in this section
        let selectedCoach = coachesFiltered[indexPath.section]
        ProgressHUD.show()
        let navigation = UINavigationController()
        navigation.modalPresentationStyle = .fullScreen
        let vc = SalesmenMonitoringView(employeeNum: selectedCoach.numeroEmpleado)
        navigation.pushViewController(vc, animated: false)
        self.present(navigation, animated: false)
    }
}

@available(iOS 13.0, *)
extension CoachesListView: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // Implement search function
        filterCoaches(string: searchText)
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        // Implement search function
        filterCoaches(string: "")
    }
}
