//
//  CoachInfo.swift
//  SalesCloud
//
//  Created by Ulises Ortega on 24/10/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class CoachInfo: BaseResponse {

    var nombre: String = ""
    var estatus: String = ""
    var numeroEmpleado: String = ""
    var Id: String = ""

    required init?(map: Map) {
    }

    override func mapping(map: Map) {
        nombre <- map["nombre"]
        estatus <- map["estatus"]
        numeroEmpleado <- map["numeroEmpleado"]
        Id <- map["Id"]
    }
}
