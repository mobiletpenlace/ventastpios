//
//  LeaderResponse.swift
//  SalesCloud
//
//  Created by Ulises Ortega on 24/10/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class LeaderResponse: BaseResponse {

    var info: [CoachInfo] = []
    var idRegistro: String = ""

    required init?(map: Map) {
    }

    override func mapping(map: Map) {
        info <- map["info"]
        idRegistro <- map["idRegistro"]
    }
}
