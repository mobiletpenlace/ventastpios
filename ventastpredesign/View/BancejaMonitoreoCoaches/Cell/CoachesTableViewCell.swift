//
//  CoachesTableViewCell.swift
//  SalesCloud
//
//  Created by Ulises Ortega on 24/10/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

class CoachesTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var empNumLabel: UILabel!
    @IBOutlet weak var statusButton: UIButton!

    var coach: CoachInfo?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configCell() {

        nameLabel.text = coach?.nombre
        empNumLabel.text = coach?.numeroEmpleado
        statusButton.setTitle(coach?.estatus, for: .normal)
        statusButton.isUserInteractionEnabled = false

        if #available(iOS 15.0, *) {
            var config = UIButton.Configuration.filled()
            config.titleTextAttributesTransformer = UIConfigurationTextAttributesTransformer { incoming in
                var outgoing = incoming
                outgoing.font = UIFont.systemFont(ofSize: 12, weight: .bold)
                return outgoing
            }
            statusButton.configuration = config
        }

        // Swith to implement probable future states
        switch coach?.estatus {
        case "Activo":
            statusButton.tintColor = .systemGreen
        default:
            statusButton.tintColor = .systemGray
        }
    }

}
