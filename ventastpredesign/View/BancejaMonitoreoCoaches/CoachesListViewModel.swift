//
//  CoachesListViewModel.swift
//  SalesCloud
//
//  Created by Ulises Ortega on 24/10/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import Alamofire
import SwiftyJSON
import SwiftEventBus

class CoachesListViewModel {
    func loadCoaches(employeeNum: String) {

        let parameters: Parameters = [
            "accion": "Consulta Lider",
            "infoEmpleado": [
                "numeroEmpleado": employeeNum
            ]
        ]
        if ConnectionUtils.isConnectedToNetwork() {
            // Connection is active
            Alamofire.request(AppDelegate.API_SALESFORCE + ApiDefinition.API_CONSULTA_CREACION_EMPLEADO, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: ApiDefinition.headersToken).responseJSON {[weak self] response in
                guard let self = self else {return}
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print("JSONCoaches: \(json)")
                    guard let leaderResponse = LeaderResponse(JSONString: json.rawString() ?? "") else {return}
                    SwiftEventBus.post("CoachesLoaded", sender: leaderResponse.info)
                case .failure(let error):
                    print(error)
                }
            }
        } else {
            // Conection error
        }
    }

    func filterCoaches(string: String, coaches: [CoachInfo]) {
        let text = string.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        var newCoaches: [CoachInfo] = []
        if text.isNumeric {
            newCoaches = coaches.filter { $0.numeroEmpleado.lowercased().contains(string: text.lowercased()) }
        } else if text.isEmpty {
            newCoaches = coaches
        } else {
            newCoaches = coaches.filter { $0.nombre.lowercased().contains(string: text.lowercased()) }
        }
        SwiftEventBus.post("CoachesFiltered", sender: newCoaches)
    }
}
