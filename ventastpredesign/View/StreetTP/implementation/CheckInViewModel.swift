//
//  CheckInViewModel.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 10/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation

protocol CheckInObserver {
    func successReasonMissing()
    func successConsultCheck()
    func successUpdateCheck()
    func onError(errorMessage: String)
}

class CheckInViewModel: BaseViewModel, CheckInHostessObserver {
    var observer: CheckInObserver?
    var model: CheckInModel!

    override init(view: BaseVentasView) {
        super.init(view: view)
        model = CheckInModel(observer: self)
        model.atachModel(viewModel: self)
    }

    func atachView(observer: CheckInObserver) {
        self.observer = observer
    }

    func consultCheckIn() {
        let request = CheckOutRequest()
        TPCipher.shared.setKey(b64Key: Strings.SHA256_KEY)
        request.IdEmployee =  TPCipher.shared.code(plainText: getEmploye()?.userId ?? "")
        request.Mlogin = LoginApp()
        request.Mlogin?.Ip = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_IP) ?? ""
        request.Mlogin?.input = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_IP) ?? ""
        request.Mlogin?.UserId = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_USER) ?? ""
        request.Mlogin?.Password = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_SECRETWORD) ?? ""
        model.consultCheckIn(request: request)
    }

    func updateCheckin() {
        let request = UpdateCheckOutRequest()
        TPCipher.shared.setKey(b64Key: Strings.SHA256_KEY)
        request.IdEvent = TPCipher.shared.code(plainText: getEmploye()?.currentIdEvent ?? "")
        request.TypeCheck =  TPCipher.shared.code(plainText: getEmploye()?.typeCheck ?? "")
        request.IdEmployee = TPCipher.shared.code(plainText: getEmploye()?.userId ?? "")
        request.Latitude = TPCipher.shared.code(plainText: String(getEmploye()?.currentLatitude ?? 0))
        request.Longitude = TPCipher.shared.code(plainText: String(getEmploye()?.currentLongitude ?? 0))
        request.Mlogin = LoginApp()
        request.Mlogin?.Ip = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_IP) ?? ""
        request.Mlogin?.input = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_IP) ?? ""
        request.Mlogin?.UserId = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_USER) ?? ""
        request.Mlogin?.Password = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_SECRETWORD) ?? ""
        model.UpdateCheckIn(request: request)
    }

    func sendReasonMissing(idMotivo: String, idModifica: String, fecha: String) {
        let request = ReasonMissingRequest()
        TPCipher.shared.setKey(b64Key: Strings.SHA256_KEY)
        request.IdMotivo =  TPCipher.shared.code(plainText: idMotivo)
        request.Date =  TPCipher.shared.code(plainText: fecha)
        request.IdUsuario = TPCipher.shared.code(plainText: idModifica)
        request.IdModifica = TPCipher.shared.code(plainText: getEmploye()?.userId ?? "")
        request.Mlogin = LoginApp()
        request.Mlogin?.Ip = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_IP) ?? ""
        request.Mlogin?.input = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_IP) ?? ""
        request.Mlogin?.UserId = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_USER) ?? ""
        request.Mlogin?.Password = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_SECRETWORD) ?? ""
        model.reasonMissing(request: request)
    }

    func successReasonMissing() {
        view.hideLoading()
        observer?.successReasonMissing()
    }

    func successConsultCheckInOut(response: CheckOutResponse?) {
        guard let result = response else {return}
        getCheckInOutEmploye(response: result)
    }

    func successUpdateCheckInOut() {
        view.hideLoading()
        observer?.successUpdateCheck()
    }

    func getEmploye() -> EmployeeStreet? {
        return model.getEmploye()
    }

    func getDetailEvent() -> EventoDetail? {
        return model.getEventDetail()
    }

    func onError(errorMessage: String) {
        view.hideLoading()
        observer?.onError(errorMessage: errorMessage)
    }

    func getCheckInOutEmploye(response: CheckOutResponse) {
        var checkIn = "0"
        var checkout = "0"
        var typeCheck = "47"
        let isCheckInCurrentEvent = "1"
        var isMissing = "0"

        isMissing = validateReasonMissing(checkIn: response.CheckIn)
        guard let event = validatePositionEvent() else {return}
            if event == "primeroEvento" {
                typeCheck = "47"
                checkIn = validateNullCheck(value: response.HoraCheckIn)
            } else if event == "ultimoEvento" {
                typeCheck = "48"
                checkIn = validateNullCheck(value: response.HoraCheckIn)
                checkout = validateNullCheck(value: response.HoraCheckOut)
            } else if event == "unicoEvento" {
                if validateNullCheck(value: response.HoraCheckIn) == "1" {
                    checkIn = validateNullCheck(value: response.HoraCheckIn)
                    checkout = validateNullCheck(value: response.HoraCheckOut)
                    typeCheck = "48"
                } else {
                    checkIn = validateNullCheck(value: response.HoraCheckIn)
                    typeCheck = "47"
                }

            } else {
                print("no entro")
            }

        model.updateCheckinOut(checkin: checkIn, checkout: checkout, typecheck: typeCheck, isCheckEvent: isCheckInCurrentEvent, isReasonMissing: isMissing)
        view.hideLoading()
        observer?.successConsultCheck()
    }

     func validatePositionEvent() -> String? {
        let currentEvent = getEmploye()?.currentIdEvent
        let firstEvent = getEmploye()?.firstIdEvent
        let lastEvent = getEmploye()?.lasttIdEvent

        if currentEvent == firstEvent && currentEvent != lastEvent {
            return "primeroEvento"
        } else if currentEvent != firstEvent && currentEvent == lastEvent {
            return "ultimoEvento"
        } else if currentEvent == firstEvent && currentEvent == lastEvent {
            return "unicoEvento"
        }
        return nil
    }

    private func validateNullCheck(value: String) -> String {
        let result = value != "" && value != "1QxURwtfPiHweDgdQLZanA==" ? "1": "0"
        return result
    }

    private func validateReasonMissing(checkIn: String) -> String {
        var result: String = "0"
        if checkIn != "" {
            TPCipher.shared.setKey(b64Key: Strings.SHA256_KEY)
            guard let value =  TPCipher.shared.decode(coded: checkIn) else {return result }
            result = value == "55" ? "1" : "0"
        }
        return result
    }

    func cleanCheck() {
        model.updateCheckinOut(checkin: "0", checkout: "0", typecheck: "0", isCheckEvent: "0", isReasonMissing: "0")
    }

}
