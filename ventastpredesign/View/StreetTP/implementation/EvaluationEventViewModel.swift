//
//  EvaluationEventViewModel.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 10/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation

protocol EvaluationObserver {
    func successEvaluationEmploye()
    func successEvaluationEvent()
    func successEvaluation()
    func onError(errorMessage: String)
}

class EvaluationEventViewModel: BaseViewModel, EvaluationEventObserver {
    var observer: EvaluationObserver?
    var model: EvaluationEventModel!

    override init(view: BaseVentasView) {
        super.init(view: view)
        model = EvaluationEventModel(observer: self)
        model.atachModel(viewModel: self)
    }

    func atachView(observer: EvaluationObserver) {
        self.observer = observer
    }

    func sendEventEvaluation( value: String, commentary: String) {
        let request = EventEvaluationRequest()
        TPCipher.shared.setKey(b64Key: Strings.SHA256_KEY)
        request.IdEvent =  TPCipher.shared.code(plainText: getEmploye()?.currentIdEvent ?? "")
        request.Value = TPCipher.shared.code(plainText: value)
        request.Commentary = TPCipher.shared.code(plainText: commentary)
        request.Item = TPCipher.shared.code(plainText: "6")
        request.IdModifica = TPCipher.shared.code(plainText: getEmploye()?.userId ?? "")
        request.Mlogin = LoginApp()
        request.Mlogin?.Ip = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_IP) ?? ""
        request.Mlogin?.input = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_IP) ?? ""
        request.Mlogin?.UserId = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_USER) ?? ""
        request.Mlogin?.Password = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_SECRETWORD) ?? ""
        model.eventEvaluation(request: request)
    }

    func sendEmployeeEvaluation( item: [String], value: [String], comentary: [String], idModifica: String) {
        let request = EmployeeEvaluationRequest()
        TPCipher.shared.setKey(b64Key: Strings.SHA256_KEY)
        request.IdEvent =  TPCipher.shared.code(plainText: getEmploye()?.currentIdEvent ?? "")
        request.IdEmployee = TPCipher.shared.code(plainText: getEmploye()?.userId ?? "")
        request.MValuationEmployee = convertValuationEmployee(item: item, value: value, comentary: comentary)
        request.IdModifica = TPCipher.shared.code(plainText: idModifica)
        request.Mlogin = LoginApp()
        request.Mlogin?.Ip = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_IP) ?? ""
        request.Mlogin?.input = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_IP) ?? ""
        request.Mlogin?.UserId = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_USER) ?? ""
        request.Mlogin?.Password = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_SECRETWORD) ?? ""
        model.employeeEvaluation(request: request)
    }

    func getQueryEvaluation(idEvent: String) {
        let request = QueryEvaluationRequest()
        TPCipher.shared.setKey(b64Key: Strings.SHA256_KEY)
        request.IdEvent =  TPCipher.shared.code(plainText: idEvent)
        request.Mlogin = LoginApp()
        request.Mlogin?.Ip = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_IP) ?? ""
        request.Mlogin?.input = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_IP) ?? ""
        request.Mlogin?.UserId = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_USER) ?? ""
        request.Mlogin?.Password = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_SECRETWORD) ?? ""
        model.consultEvaluation(request: request)
    }

    func successEvaluationEvent() {
        view.hideLoading()
        observer?.successEvaluationEvent()
    }

    func successEvaluationEmploye() {
        view.hideLoading()
        observer?.successEvaluationEmploye()
    }

    func successConsultEvaluation() {
        view.hideLoading()
        observer?.successEvaluation()
    }

    func getEmploye() -> EmployeeStreet? {
        return model.getEmploye()
    }

    func getValuationEmployee() -> [ValuationEmployee] {
        return model.getValuationEmployee()
    }

    func getValuationEvent() -> [ValuationEvent] {
        return model.getValuationEvent()
    }

    func convertValuationEmployee(item: [String], value: [String], comentary: [String]) -> [ValuationEmployee] {
        TPCipher.shared.setKey(b64Key: Strings.SHA256_KEY)
        var valuationEmployee: [ValuationEmployee] = []
        for index in 0..<item.count {
            let valuation = ValuationEmployee()
            valuation.Item = TPCipher.shared.code(plainText: item[index])!
            valuation.Commentary = TPCipher.shared.code( plainText: comentary[index])!
            valuation.Value = TPCipher.shared.code( plainText: value[index])!
            valuationEmployee.append(valuation)
        }
        return valuationEmployee
    }

    func onError(errorMessage: String) {
        view.hideLoading()
        observer?.onError(errorMessage: errorMessage)
    }

}
