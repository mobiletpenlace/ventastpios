//
//  EventModel.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 10/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import RealmSwift

protocol EventModelObserver {
    func successSearchEvent()
    func successDetailEvent()
    func successSearchHostes()
    func successDeleteEvent()
    func successAssignedHostess()
    func successListCategory()
    func successDeleteHostess()
    func successEditEvent()
    func onError(errorMessage: String)
}

class EventModel: BaseModel {
    var viewModel: EventModelObserver?
    var server: ServerDataManager2<EventListResponse>?
    var server2: ServerDataManager2<EventDetailsResponse>?
    var server3: ServerDataManager2<HostessSupervisorResponse>?
    var server4: ServerDataManager2<AssignedHostessResponse>?
    var server5: ServerDataManager2<CategoryResponse>?
    var server6: ServerDataManager2<DeleteEventResponse>?
    var server7: ServerDataManager2<DeleteHostessResponse>?
    var server8: ServerDataManager2<EditEventResponse>?
    var urlSearchEvent = AppDelegate.API_TOTAL + ApiDefinition.API_EVENT_LIST
    var urlDetailEvent = AppDelegate.API_TOTAL + ApiDefinition.API_EVENT_DETAIL
    var urlSearcHostes = AppDelegate.API_TOTAL + ApiDefinition.API_HOSTESS_SUPERVISE
    var urlAsignedHostes = AppDelegate.API_TOTAL + ApiDefinition.API_ASSIGNED_HOSTESS
    var urlListCategory = AppDelegate.API_TOTAL + ApiDefinition.API_TREE_CATEGORY
    var urlDeleteEvent = AppDelegate.API_TOTAL + ApiDefinition.API_DELETE_EVENT
    var urlDeleteHostess =  AppDelegate.API_TOTAL + ApiDefinition.API_DELETE_HOSTESS
    var urlEditEvent =  AppDelegate.API_TOTAL + ApiDefinition.API_EDIT_EVENT

    init(observer: BaseViewModelObserver) {
        super.init(observerVM: observer)
        server = ServerDataManager2<EventListResponse>()
        server2 = ServerDataManager2<EventDetailsResponse>()
        server3 = ServerDataManager2<HostessSupervisorResponse>()
        server4 = ServerDataManager2<AssignedHostessResponse>()
        server5 = ServerDataManager2<CategoryResponse>()
        server6 = ServerDataManager2<DeleteEventResponse>()
        server7 = ServerDataManager2<DeleteHostessResponse>()
        server8 = ServerDataManager2<EditEventResponse>()
    }

    func atachModel(viewModel: EventModelObserver) {
        self.viewModel = viewModel
    }

    func consultEvents(request: EventListRequest) {
        urlSearchEvent = AppDelegate.API_TOTAL + ApiDefinition.API_EVENT_LIST
        server?.setValues(requestUrl: urlSearchEvent, delegate: self, headerType: .headersMiddle)
        server?.request(requestModel: request)
    }

    func detailEvents(request: EventDetailsRequest) {
        urlDetailEvent = AppDelegate.API_TOTAL + ApiDefinition.API_EVENT_DETAIL
        server2?.setValues(requestUrl: urlDetailEvent, delegate: self, headerType: .headersMiddle)
        server2?.request(requestModel: request)
    }

    func searchHostess(request: HostessSupervisorRequest) {
        urlSearcHostes = AppDelegate.API_TOTAL + ApiDefinition.API_HOSTESS_SUPERVISE
        server3?.setValues(requestUrl: urlSearcHostes, delegate: self, headerType: .headersMiddle)
        server3?.request(requestModel: request)
    }

    func assignedHostess(request: AssignedHostessRequest) {
        urlAsignedHostes = AppDelegate.API_TOTAL + ApiDefinition.API_ASSIGNED_HOSTESS
        server4?.setValues(requestUrl: urlAsignedHostes, delegate: self, headerType: .headersMiddle)
        server4?.request(requestModel: request)
    }

    func consultCategory(request: CategoryRequest) {
        urlListCategory = AppDelegate.API_TOTAL + ApiDefinition.API_TREE_CATEGORY
        server5?.setValues(requestUrl: urlListCategory, delegate: self, headerType: .headersMiddle)
        server5?.request(requestModel: request)
    }

    func sendDeleteEvent(request: DeleteEventRequest) {
        urlDeleteEvent =  AppDelegate.API_TOTAL + ApiDefinition.API_DELETE_EVENT
        server6?.setValues(requestUrl: urlDeleteEvent, delegate: self, headerType: .headersMiddle)
        server6?.request(requestModel: request)
    }

    func sendDeleteHostess(request: DeleteHostessRequest) {
        urlDeleteHostess =  AppDelegate.API_TOTAL + ApiDefinition.API_DELETE_HOSTESS
        server7?.setValues(requestUrl: urlDeleteHostess, delegate: self, headerType: .headersMiddle)
        server7?.request(requestModel: request)
    }

    func sendEditEvent(request: EditEventRequest) {
        urlEditEvent =  AppDelegate.API_TOTAL + ApiDefinition.API_EDIT_EVENT
        server8?.setValues(requestUrl: urlEditEvent, delegate: self, headerType: .headersMiddle)
        server8?.request(requestModel: request)
    }

    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {

        if requestUrl == urlSearchEvent {
            let eventResponse = response as! EventListResponse
            if eventResponse.MResult?.Result == "0" && eventResponse.MEvents.count != 0 {
                saveEventList(eventResponse: eventResponse)
                self.viewModel?.successSearchEvent()
            } else {
                viewModel?.onError(errorMessage: eventResponse.MResult?.ResultDescription ?? "Error al consultar los eventos")
            }

        } else  if requestUrl == urlDetailEvent {
            let eventResponse = response as! EventDetailsResponse
            if eventResponse.MResult?.Result == "0" {
                saveDetailEvent(response: eventResponse)
                saveHostessEvent(response: eventResponse)
                self.viewModel?.successDetailEvent()
            } else {
                viewModel?.onError(errorMessage: eventResponse.MResult?.ResultDescription ?? "Error al consultar el detalle del evento")
            }
        } else  if requestUrl == urlSearcHostes {
            let hostesResponse = response as! HostessSupervisorResponse
            if hostesResponse.MResult?.Result == "0" {
                if hostesResponse.Mteam.count > 0 {
                    saveListHostess(response: hostesResponse)
                    self.viewModel?.successSearchHostes()
                } else {
                    viewModel?.onError(errorMessage: "Mo hay datos que mostrar")
                }
            } else {
                viewModel?.onError(errorMessage: hostesResponse.MResult?.ResultDescription ?? "Error al consultar los promotores del supervisor")
            }
        } else  if requestUrl == urlAsignedHostes {
            let hostesResponse = response as! AssignedHostessResponse
            if hostesResponse.MResult?.Result == "0" {
                self.viewModel?.successAssignedHostess()
            } else {
                viewModel?.onError(errorMessage: hostesResponse.MResult?.ResultDescription ?? "Error al asignar los hostess al evento")
            }
        } else  if requestUrl == urlListCategory {
            let categoryResponse = response as! CategoryResponse
            if categoryResponse.MResult?.Result == "0" {
                saveCategoryEvent(response: categoryResponse)
                self.viewModel?.successListCategory()
            } else {
                viewModel?.onError(errorMessage: categoryResponse.MResult?.ResultDescription ?? "Error al obtener las categorias del evento")
            }
        } else  if requestUrl == urlDeleteEvent {
            let deleteResponse = response as! DeleteEventResponse
            if deleteResponse.MResult?.IdResult == "0" {
                self.viewModel?.successDeleteEvent()
            } else {
                viewModel?.onError(errorMessage: deleteResponse.MResult?.ResultDescription ?? "Error al eliminar el evento")
            }
        } else  if requestUrl == urlDeleteHostess {
            let deleteResponse = response as! DeleteHostessResponse
            if deleteResponse.MResult?.IdResult == "0" {
                self.viewModel?.successDeleteHostess()
            } else {
                viewModel?.onError(errorMessage: deleteResponse.MResult?.ResultDescription ?? "Error al eliminar el promotor")
            }
        } else  if requestUrl == urlEditEvent {
            let editResponse = response as! EditEventResponse
            if editResponse.MResult?.IdResult == "0" {
                self.viewModel?.successEditEvent()
            } else {
                viewModel?.onError(errorMessage: editResponse.MResult?.ResultDescription ?? "Error al editar el evento")
            }
        }

    }

    override func onErrorLoadResponse(requestUrl: String, messageError: String) {
        viewModel?.onError(errorMessage: messageError)
    }

    func saveListHostess(response: HostessSupervisorResponse) {
        RealManager.deleteAll(object: team.self)
        RealManager.insertCollection(objects: response.Mteam)
    }

    func saveHostessEvent(response: EventDetailsResponse) {
        RealManager.deleteAll(object: hostessDetails.self)
        if response.Mhostess.count > 0 {
            RealManager.insertCollection(objects: response.Mhostess)
        }
    }

    func saveDetailEvent(response: EventDetailsResponse) {
        RealManager.deleteAll(object: EventoDetail.self)
        let evento = EventoDetail()
        evento.IdActivity = response.IdActivity
        evento.IdCampaign = response.IdCampaign
        evento.NameCampaign = response.NameCampaign
        evento.dateEnd = response.dateEnd
        evento.dateStar = response.dateStar
        evento.descriptionEvent = response.descriptionEvent
        evento.latitude = response.latitude
        evento.endHour = response.endHour
        evento.length = response.length
        evento.startHour = response.startHour
        evento.typeActivity = response.typeActivity
        _ = RealManager.insert(object: evento)
    }

    func saveEventList(eventResponse: EventListResponse ) {
        RealManager.deleteAll(object: Events.self)
        RealManager.insertCollection(objects: eventResponse.MEvents)
    }

    func saveEventListQuality(events: [Events] ) {
        RealManager.deleteAll(object: Events.self)
        RealManager.insertCollection(objects: events)
    }

    func saveCategoryEvent(response: CategoryResponse) {
        if response.Mtree.count > 0 {
            RealManager.deleteAll(object: tree.self)
            RealManager.insertCollection(objects: response.Mtree)
        }

    }

    func deleteCategory() {
        RealManager.deleteAll(object: tree.self)
    }

    func saveEventCalendar() {
        RealManager.deleteAll(object: EventCalendar.self)
        let eventCalendar = EventCalendar()
        _ = RealManager.insert(object: eventCalendar)
    }

    func getEventDetail() -> EventoDetail? {
        return RealManager.findFirst(object: EventoDetail.self)
    }

    func getEvents() -> [Events] {
        return RealManager.getAll(object: Events.self)
    }

    func getHostessEvent() -> [hostessDetails] {
        return RealManager.getAll(object: hostessDetails.self)
    }

    func getHostess() -> [team] {
        return RealManager.getAll(object: team.self)
    }

    func getEmploye() -> EmployeeStreet? {
        let employee = RealManager.findFirst(object: EmployeeStreet.self)
        return employee
    }

    func getCategory() -> [tree] {
        return RealManager.getAll(object: tree.self)
    }

    func getEventCalendar() -> EventCalendar? {
        return RealManager.findFirst(object: EventCalendar.self)
    }

    func getCurrentDetailEvent() -> DetailEvent? {
        return RealManager.findFirst(object: DetailEvent.self)
    }

    func getCurrentSupervisor() -> Supervisor? {
        return RealManager.findFirst(object: Supervisor.self)
    }

    func updateIsSelectHostess(id: String, isSelect: String) {
        guard let hostess =  RealManager.getFromFirst(object: team.self, field: "id", equalsTo: id) else { return }
        RealManager.updateLine {
            hostess.isSelect = isSelect
        }
    }

    func updateIsSelectCategory(id: String, isSelect: String) {
        guard let category =  RealManager.getFromFirst(object: tree.self, field: "id", equalsTo: id) else { return }
        RealManager.updateLine {
            category.isSelect = isSelect
        }
    }

    func updateIdCategory(idCategory: String, title: String) {
        guard let employee = getEmploye() else { return }
        RealManager.updateLine {
            employee.currentIdCategory = idCategory
            employee.currentTitleCategory = title
        }
    }

    func updateLocation(latitude: Double, longitude: Double) {
        guard let employee = getEmploye() else { return }
        RealManager.updateLine {
            employee.currentLongitude = longitude
            employee.currentLatitude = latitude
        }
    }

    func updateIdActivity(idActivity: String, title: String) {
        guard let event = getEventDetail() else { return }
        RealManager.updateLine {
            event.IdActivity = idActivity
            event.NameCampaign = title
        }
    }

    func updateLocationEvent(latitude: Double, longitude: Double) {
        guard let event = getEventDetail() else { return }
        RealManager.updateLine {
            event.latitudeDouble = latitude
            event.longitudDouble = longitude
        }
    }

    func updateEventCalendar(week: String, days: Int, positionPicker: Int) {
        guard let eventCalendar = getEventCalendar() else { return }
        RealManager.updateLine {
            eventCalendar.days = days
            eventCalendar.week = week
            eventCalendar.positionPicker = positionPicker
        }
    }

    func updateDistanceUserToEvent(distance: Double, isNear: Bool) {
        guard let employee = getEmploye() else { return }
        RealManager.updateLine {
            employee.distanceUserToEvent = distance
            employee.isNearEvent = isNear
        }
    }

    func updateDataEvent( id: String, dayOfYear: Int) {
        guard let event = RealManager.getFromFirst(object: Events.self, field: "idEvent", equalsTo: id )else { return }
        RealManager.updateLine {
            event.dayOfYear = dayOfYear
        }
    }

    func updateCurrentSupervisor(_ supervisor: Supervisor) {
        if  RealManager.findFirst(object: Supervisor.self) != nil {
            RealManager.deleteAll(object: Supervisor.self)
        }
       _ =  RealManager.insert(object: supervisor)
    }

    func updateFirstLastEvent(first: String, last: String) {
        guard let employee = getEmploye() else { return }
        RealManager.updateLine {
            employee.firstIdEvent = first
            employee.lasttIdEvent = last
        }
    }

    func updateColor( position: Int) {
        guard let eventCalendar = getEventCalendar()  else { return }
        RealManager.updateLine {
            eventCalendar.positionColor = position
        }
    }

    func updateIdEvent(idEvent: String, titleEvent: String) {
        guard let employee = getEmploye() else { return }
        RealManager.updateLine {
            employee.currentIdEvent = idEvent
            employee.currentTitleEvent = titleEvent
        }
    }

    func updateNameSupervisor(nameSupervisor: String) {
        guard let employee = getEmploye() else { return }
        RealManager.updateLine {
            employee.nameEmployee = nameSupervisor

        }
    }

    func updateCurrentDetailEvent(_ newEvent: DetailEvent) {
        RealManager.deleteAll(object: DetailEvent.self)
         _ =  RealManager.insert(object: newEvent)
       }

    func updateShowDateColor(positionColor: Int, showDate: Bool, id: String, numberDay: String, nameDay: String) {
        guard let event = RealManager.getFromFirst(object: Events.self, field: "idEvent", equalsTo: id ) else { return }
        RealManager.updateLine {
            event.indexColor = positionColor
            event.showDate = showDate
            event.numberDay = numberDay
            event.nameDay = nameDay
        }
    }

    func updateConnectFirebase(connect: String) {
        guard let employee = RealManager.findFirst(object: EmployeeStreet.self) else {
            return
        }
        RealManager.updateLine {
            employee.connectFirebase = connect
        }
    }

    func updateDayOfYearEventHourFinal( id: String, dayOfYear: Int) {
        guard let event = RealManager.getFromFirst(object: Events.self, field: "idEvent", equalsTo: id )else {
            return
        }
        RealManager.updateLine {
            event.dayOfYearHourFinal = dayOfYear
        }
    }

    func sortDateEvent() -> [Events] {
        return RealManager.getSorted(object: Events.self, field: "dayOfYear", ascending: true)
    }
}
