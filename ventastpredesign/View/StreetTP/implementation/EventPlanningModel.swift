//
//  EventPlanningModel.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 08/09/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import Firebase
import FirebaseCore
import FirebaseFirestore
import FirebaseFirestoreSwift
// import CodableFirebase

protocol EventPlanningModelObserver {
    func successSearchEventPlanning()
    func successCategoryEvaluation()
    func successAddEvent()
    func successDeleteEvent()
    func successDataUser()
    func onError(errorMessage: String)
}

class EventPlanningModel: BaseModel {
    var viewModel: EventPlanningModelObserver?
    var eventsPlanning: ListaEventos?
    var isNewUser = false
    var optionsEvaluation: [OptionsEvaluation]?
    var userData: userFFM?

    init(observer: BaseViewModelObserver) {
        super.init(observerVM: observer)
    }

    func atachModel(viewModel: EventPlanningModelObserver) {
        self.viewModel = viewModel
    }

    func userDataPlanning(idSupervisor: String) {
        let db = Firestore.firestore()
        db.collection("usuarios").document("ffm\(idSupervisor)").getDocument { (snapshot, _) in
            do {
                let  item = try snapshot?.data(as: userFFM.self)
                self.userData = item
                self.viewModel?.successDataUser()
            } catch _ {
                self.viewModel?.onError(errorMessage: "No se encontraron datos del supervisor para crear el evento")
            }
        }
    }

    func listEventPlanning(numberSupervisor: String) {
        let db = Firestore.firestore()
        db.collection("events").document(numberSupervisor).getDocument { [self] (snapshot, _) in
            do {
                let  item = try snapshot?.data(as: ListaEventos.self)
                if item == nil {
                    self.isNewUser = true
                } else {
                    self.isNewUser = false
                }
                if item?.eventos?.count ?? 0 > 0 {
                    self.eventsPlanning = item
                    print(item?.eventos?.count as Any)
                    self.viewModel?.successSearchEventPlanning()
                } else {
                    self.viewModel?.onError(errorMessage: "No se encontraron eventos en planificación")
                }
            } catch _ {
                self.viewModel?.onError(errorMessage: "No se encontraron eventos en planificación")
            }
        }
    }

    func listEvaluationCategory() {
        let db = Firestore.firestore()
        db.collection("evaluationCat").getDocuments { (snapshot, error) in
            do {
                let options = snapshot?.documents.compactMap { (document) -> OptionsEvaluation? in
                    var x: OptionsEvaluation?
                             do {
                               x = try document.data(as: OptionsEvaluation.self)
                             } catch {
                             print(error)
                             }
                    return  x // try? document.data(as: OptionsEvaluation.self)
                }
                print(options)

                if options?.count ?? 0 > 0 {
                    self.optionsEvaluation = options
                    self.viewModel?.successCategoryEvaluation()
                } else {
                    self.viewModel?.onError(errorMessage: "No se encontraron opciones en el catalogo de comentarios")
                }

            } catch _ {
                self.viewModel?.onError(errorMessage: "No se encontraron opciones en el catalogo de comentarios")
            }
        }
    }

    func deleteEventPlaning(numberSupervisor: String, listaEventos: ListaEventos, position: Int ) {
        var copyListEvent = ListaEventos()
        var docData = [[String: Any]]()
        copyListEvent = listaEventos
        copyListEvent.eventos!.remove(at: position)
        print(copyListEvent.eventos?.count)

        for item in copyListEvent.eventos! {
            docData.append([
                "nombreactividad": [
                    "editNombreActividad": item.nombreactividad?.editNombreActividad as Any,
                    "object": [
                        "descripcion": item.nombreactividad?.object?.descripcion as Any,
                        "descripcionCatalogo": item.nombreactividad?.object?.descripcionCatalogo as Any,
                        "id": item.nombreactividad?.object?.id as Any,
                        "idCatalogo": item.nombreactividad?.object?.idCatalogo as Any
                    ]
                ],
                "objectUbicacion": [
                    "editObjectUbicacion": item.objectUbicacion?.editObjectUbicacion as Any,
                    "latitudSelected": item.objectUbicacion?.latitudSelected as Any,
                    "longitudSelected": item.objectUbicacion?.longitudSelected as Any,
                    "textSelectedPlace": item.objectUbicacion?.textSelectedPlace as Any
                ],
                "objetoFechas": [
                    "fechaFin": item.objetoFechas?.fechaFin as Any,
                    "fechaInicio": item.objetoFechas?.fechaInicio as Any

                ],
                "objetoHoraFin": [
                    "horabase": item.objetoHoraFin?.horabase as Any,
                    "horatext": item.objetoHoraFin?.horatext as Any
                ],
                "objetoHoraInicio": [
                    "horabase": item.objetoHoraInicio?.horabase as Any,
                    "horatext": item.objetoHoraInicio?.horatext as Any
                ],
                "seletedItem": item.selectedItem as Any,
                "supervisor": [
                    "descripcion": item.supervisor?.descripcion as Any,
                    "id": item.supervisor?.id as Any,
                    "idSupervisor": item.supervisor?.idSupervisor as Any,
                    "numeroempleado": item.supervisor?.numeroempleado as Any
                ],
                "tipoactividad": [
                    "editTipoActividad": item.tipoactividad?.editTipoActividad as Any,
                    "object": [
                        "descripcion": item.tipoactividad?.object?.descripcion as Any,
                        "descripcionCatalogo": item.tipoactividad?.object?.descripcionCatalogo as Any,
                        "id": item.tipoactividad?.object?.id as Any,
                        "idCatalogo": item.tipoactividad?.object?.idCatalogo as Any
                    ]
                ]

            ])
        }
        print(docData)//
        do {
            let db = Firestore.firestore()
            try db.collection("events").document(numberSupervisor).updateData(["eventos": docData])
            self.viewModel?.successDeleteEvent()
        } catch _ {

            self.viewModel?.onError(errorMessage: "Error al eliminar el evento de planeación")
        }
    }

    func addEventPlaning(numberSupervisor: String, listaEventos: ListaEventos?, evento: Eventos) {
        var copyListEvent = ListaEventos()
        if listaEventos != nil {
            copyListEvent = listaEventos!
            copyListEvent.eventos!.insert(evento, at: copyListEvent.eventos!.endIndex)
        } else {
            copyListEvent.eventos = []
            copyListEvent.eventos!.append(evento)
        }

        var docData = [[String: Any]]()
        for item in copyListEvent.eventos! {
            docData.append([
                "nombreactividad": [
                    "editNombreActividad": item.nombreactividad?.editNombreActividad as Any,
                    "object": [
                        "descripcion": item.nombreactividad?.object?.descripcion as Any,
                        "descripcionCatalogo": item.nombreactividad?.object?.descripcionCatalogo as Any,
                        "id": item.nombreactividad?.object?.id as Any,
                        "idCatalogo": item.nombreactividad?.object?.idCatalogo as Any
                    ]
                ],
                "objectUbicacion": [
                    "editObjectUbicacion": item.objectUbicacion?.editObjectUbicacion as Any,
                    "latitudSelected": item.objectUbicacion?.latitudSelected as Any,
                    "longitudSelected": item.objectUbicacion?.longitudSelected as Any,
                    "textSelectedPlace": item.objectUbicacion?.textSelectedPlace as Any
                ],
                "objetoFechas": [
                    "fechaFin": item.objetoFechas?.fechaFin as Any,
                    "fechaInicio": item.objetoFechas?.fechaInicio as Any

                ],
                "objetoHoraFin": [
                    "horabase": item.objetoHoraFin?.horabase as Any,
                    "horatext": item.objetoHoraFin?.horatext as Any
                ],
                "objetoHoraInicio": [
                    "horabase": item.objetoHoraInicio?.horabase as Any,
                    "horatext": item.objetoHoraInicio?.horatext as Any
                ],
                "seletedItem": item.selectedItem as Any,
                "supervisor": [
                    "descripcion": item.supervisor?.descripcion as Any,
                    "id": item.supervisor?.id as Any,
                    "idSupervisor": item.supervisor?.idSupervisor as Any,
                    "numeroempleado": item.supervisor?.numeroempleado as Any
                ],
                "tipoactividad": [
                    "editTipoActividad": item.tipoactividad?.editTipoActividad as Any,
                    "object": [
                        "descripcion": item.tipoactividad?.object?.descripcion as Any,
                        "descripcionCatalogo": item.tipoactividad?.object?.descripcionCatalogo as Any,
                        "id": item.tipoactividad?.object?.id as Any,
                        "idCatalogo": item.tipoactividad?.object?.idCatalogo as Any
                    ]
                ]

            ])
        }
        do {
            let db = Firestore.firestore()
            if self.isNewUser {
                try db.collection("events").document(numberSupervisor).setData(["eventos": docData])
            } else {
                try db.collection("events").document(numberSupervisor).updateData(["eventos": docData])
            }
            self.viewModel?.successAddEvent()
        } catch let error {
            self.viewModel?.onError(errorMessage: "Error al agregar el evento a planeación")
        }

    }

    func getEventCalendar() -> EventCalendar? {
        return RealManager.findFirst(object: EventCalendar.self)
    }

    func updateColor( position: Int) {
        guard let eventCalendar =  RealManager.findFirst(object: EventCalendar.self)  else {
            return
        }
        RealManager.updateLine {
            eventCalendar.positionColor = position
        }
    }

}
