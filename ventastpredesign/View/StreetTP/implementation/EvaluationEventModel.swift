//
//  EvaluationEventModel.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 10/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import RealmSwift

protocol EvaluationEventObserver {
    func successEvaluationEvent()
    func successEvaluationEmploye()
    func successConsultEvaluation()
    func onError(errorMessage: String)
}

class EvaluationEventModel: BaseModel {
    var viewModel: EvaluationEventObserver?
    var server: ServerDataManager2<EventEvaluationResponse>?
    var server2: ServerDataManager2<EmployeeEvaluationResponse>?
    var server3: ServerDataManager2<QueryEvaluationResponse>?
    var urlEventEvaluation = AppDelegate.API_TOTAL + ApiDefinition.API_EVENT_EVALUATION
    var urlEmployeeEvaluation = AppDelegate.API_TOTAL + ApiDefinition.API_EMPLOYEE_EVALUATION
    var urlConsultEvaluation = AppDelegate.API_TOTAL + ApiDefinition.API_QUERY_EVALUATION

    init(observer: BaseViewModelObserver) {
        super.init(observerVM: observer)
        server = ServerDataManager2<EventEvaluationResponse>()
        server2 = ServerDataManager2<EmployeeEvaluationResponse>()
        server3 = ServerDataManager2<QueryEvaluationResponse>()
    }

    func atachModel(viewModel: EvaluationEventObserver) {
        self.viewModel = viewModel
    }

    func eventEvaluation(request: EventEvaluationRequest) {
        urlEventEvaluation = AppDelegate.API_TOTAL + ApiDefinition.API_EVENT_EVALUATION
        server?.setValues(requestUrl: urlEventEvaluation, delegate: self, headerType: .headersMiddle)
        server?.request(requestModel: request)
    }

    func employeeEvaluation(request: EmployeeEvaluationRequest) {
        urlEmployeeEvaluation = AppDelegate.API_TOTAL + ApiDefinition.API_EMPLOYEE_EVALUATION
        server2?.setValues(requestUrl: urlEmployeeEvaluation, delegate: self, headerType: .headersMiddle)
        server2?.request(requestModel: request)
    }

    func consultEvaluation(request: QueryEvaluationRequest) {
        urlConsultEvaluation = AppDelegate.API_TOTAL + ApiDefinition.API_QUERY_EVALUATION
        server3?.setValues(requestUrl: urlConsultEvaluation, delegate: self, headerType: .headersMiddle)
        server3?.request(requestModel: request)
    }

    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if requestUrl == urlEventEvaluation {
            let response = response as! EventEvaluationResponse
            if response.MResult?.Result == "0" {
                self.viewModel?.successEvaluationEvent()
            } else {
                viewModel?.onError(errorMessage: response.MResult?.ResultDescription ?? "Error al evaluar el evento")
            }

        } else  if requestUrl == urlEmployeeEvaluation {
            let response = response as! EmployeeEvaluationResponse
            if response.MResult?.Result == "0" {
                self.viewModel?.successEvaluationEmploye()
            } else {
                viewModel?.onError(errorMessage: response.MResult?.ResultDescription ?? "Error al evaluar al promotor")
            }

        } else  if requestUrl == urlConsultEvaluation {
            let response = response as! QueryEvaluationResponse
            if response.MResult?.Result == "0" {
                saveValuationEvent(response: response)
                saveValuationEmployee(response: response)
                self.viewModel?.successConsultEvaluation()
            } else {
                viewModel?.onError(errorMessage: response.MResult?.ResultDescription ?? "Error al obtener los datos de la evaluación del evento")
            }
        }
    }

    override func onErrorLoadResponse(requestUrl: String, messageError: String) {
        viewModel?.onError(errorMessage: messageError)
    }

    func saveValuationEvent(response: QueryEvaluationResponse) {
        if response.MValuationEvent.count > 0 {
            RealManager.deleteAll(object: ValuationEvent.self)
            RealManager.insertCollection(objects: response.MValuationEvent)
        }
    }

    func saveValuationEmployee(response: QueryEvaluationResponse) {
        if response.MValuationEmployee.count > 0 {
            RealManager.deleteAll(object: ValuationEmployee.self)
            RealManager.insertCollection(objects: response.MValuationEmployee)
        }
    }

    func getEmploye() -> EmployeeStreet? {
        let employee = RealManager.findFirst(object: EmployeeStreet.self)
        return employee
    }

    func getValuationEmployee() -> [ValuationEmployee] {
        return RealManager.getAll(object: ValuationEmployee.self)
    }

    func getValuationEvent() -> [ValuationEvent] {
        return RealManager.getAll(object: ValuationEvent.self)
    }

}
