//
//  CreateEventViewModel.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 10/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation

protocol CreateEventSuccessObserver {
    func successCreateEvent()
    func onError(errorMessage: String)
}

class CreateEventViewModel: BaseViewModel, CreateEventObserver {

    var observer: CreateEventSuccessObserver?
    var model: CreateEventModel!

    override init(view: BaseVentasView) {
        super.init(view: view)
        model = CreateEventModel(observer: self)
        model.atachModel(viewModel: self)
    }

    func atachView(observer: CreateEventSuccessObserver) {
        self.observer = observer
    }

    func createEvent(idSupervisor: String, idCampaign: String, idActivity: String, date: String, hourStart: String, hourFinish: String, description: String, longitude: String, latitude: String) {
        let createEventRequest = CreateEventRequest()
        TPCipher.shared.setKey(b64Key: Strings.SHA256_KEY)
        createEventRequest.MAddEvent = AddEvent()
        createEventRequest.MAddEvent?.p_eeo_date_ini = TPCipher.shared.code(plainText: (date + " " + hourStart)) ?? ""
        createEventRequest.MAddEvent?.p_eeo_date_fin = TPCipher.shared.code(plainText: (date + " " + hourFinish)) ?? ""
        createEventRequest.MAddEvent?.p_eeo_descr = TPCipher.shared.code(plainText: description) ?? ""
        createEventRequest.MAddEvent?.p_eeo_coo_lat = TPCipher.shared.code(plainText: latitude) ?? ""
        createEventRequest.MAddEvent?.p_eeo_coo_long = TPCipher.shared.code(plainText: longitude) ?? ""
        createEventRequest.MAddEvent?.p_ecg_id_camp = TPCipher.shared.code(plainText: idCampaign) ?? ""
        createEventRequest.MAddEvent?.p_ecg_id_act = TPCipher.shared.code(plainText: idActivity) ?? ""
        createEventRequest.MAddEvent?.p_eua_created_by = TPCipher.shared.code(plainText: getEmployee().userId) ?? ""
        createEventRequest.MAddEvent?.p_EUA_ID = TPCipher.shared.code(plainText: idSupervisor) ?? ""
        createEventRequest.Mlogin = LoginApp()
        createEventRequest.Mlogin?.Ip = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_IP) ?? ""
        createEventRequest.Mlogin?.UserId = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_USER) ?? ""
        createEventRequest.Mlogin?.Password = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_SECRETWORD) ?? ""
        model.createEvent(request: createEventRequest)

    }

    func successCreateEvent() {
        view.hideLoading()
        observer?.successCreateEvent()
    }

    func onError(errorMessage: String) {
        view.hideLoading()
        observer?.onError(errorMessage: errorMessage)
    }

    func getEmployee() -> EmployeeStreet {
        let employee = model.getEmployee()
        return employee

    }

    func getPlanningEvent() -> planningEvent {
        let event = model.getPlanningEvent()
        return event
    }

    func createPlanningEvent(nameCampaing: String, idCampaing: String, nameActivity: String, idActivity: String, dateEvent: String, hourFinish: String, hourStart: String) {

        model.setEventPlaning(nameCampaing: nameCampaing, idCampaing: idCampaing, nameActivity: nameActivity, idActivity: idActivity, dateEvent: dateEvent, hourFinish: hourFinish, hourStart: hourStart)
    }

    func updateLocationEvent(latitude: Double, longitude: Double, location: String) {
        model.updateLocationEvent(latitude: latitude, longitude: longitude, location: location)
    }

    func getDateEvent(date: String, hourStart: String, hourFinish: String) -> String {

        var firstDate = ""
        var secondDate = ""

        let dateFirstComplete = date + "T"  + hourStart
        let newFirstDate = DateCalendar.convertStringDate(date: dateFirstComplete, formatter: Formate.dd_MM_yyyy_HHmm)
        firstDate = DateCalendar.convertDateString(date: newFirstDate!, formatter: Formate.EEEE_d_HHmm_a)

        let dateSecondComplete = date + "T"  + hourFinish
        let newSecondDate = DateCalendar.convertStringDate(date: dateSecondComplete, formatter: Formate.dd_MM_yyyy_HHmm)
        secondDate = DateCalendar.convertDateString(date: newSecondDate!, formatter: Formate.HH_mm_a)

        return (firstDate + " - " + secondDate).capitalizingFirstLetter()
    }

}
