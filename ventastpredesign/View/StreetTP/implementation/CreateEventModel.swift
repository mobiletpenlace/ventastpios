//
//  CreateEventModel.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 10/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import RealmSwift

protocol CreateEventObserver {
    func successCreateEvent()
    func onError(errorMessage: String)
}

class CreateEventModel: BaseModel {
    var viewModel: CreateEventObserver?
    var server: ServerDataManager2<CreateEventResponse>?
    var urlCreateEvent = AppDelegate.API_TOTAL + ApiDefinition.API_CREATE_EVENT

    init(observer: BaseViewModelObserver) {
        super.init(observerVM: observer)
        server = ServerDataManager2<CreateEventResponse>()

    }

    func atachModel(viewModel: CreateEventObserver) {
        self.viewModel = viewModel
    }

    func createEvent(request: CreateEventRequest) {
        urlCreateEvent = AppDelegate.API_TOTAL + ApiDefinition.API_CREATE_EVENT
        server?.setValues(requestUrl: urlCreateEvent, delegate: self, headerType: .headersMiddle)
        server?.request(requestModel: request)
    }

    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if requestUrl == urlCreateEvent {
            let response = response as! CreateEventResponse
            if response.MResult?.Result == "0" {
                self.viewModel?.successCreateEvent()
            } else {
                viewModel?.onError(errorMessage: response.MResult?.ResultDescription ?? "Error al crear el evento")
            }

        }
    }

    override func onErrorLoadResponse(requestUrl: String, messageError: String) {
        viewModel?.onError(errorMessage: messageError)
    }

    func getEmployee() -> EmployeeStreet {
        let employee = RealManager.findFirst(object: EmployeeStreet.self)
        return employee!
    }

    func getPlanningEvent() -> planningEvent {
        let event = RealManager.findFirst(object: planningEvent.self)
        return event!
    }

    func setEventPlaning(nameCampaing: String, idCampaing: String, nameActivity: String, idActivity: String, dateEvent: String, hourFinish: String, hourStart: String) {
        if RealManager.findFirst(object: planningEvent.self) != nil {
            RealManager.deleteAll(object: planningEvent.self)
        }
        let event = planningEvent()
        event.nameCampaign = nameCampaing
        event.idCampaign = idCampaing
        event.nameActivity = nameActivity
        event.idActivity = idActivity
        event.dateEvent = dateEvent
        event.hourFinish = hourFinish
        event.hourStart = hourStart
        _ = RealManager.insert(object: event)
    }

    func updateLocationEvent(latitude: Double, longitude: Double, location: String) {
        guard let event = RealManager.findFirst(object: planningEvent.self) else {
            return
        }
        RealManager.updateLine {
            event.latitud = String(latitude)
            event.longitud = String(longitude)
            event.nameLocation = location == "" ? "Ubiación seleccionada": location
        }
    }
}
