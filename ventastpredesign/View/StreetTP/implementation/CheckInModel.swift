//
//  CheckInModel.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 10/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import RealmSwift

protocol CheckInHostessObserver {
    func successReasonMissing()
    func successConsultCheckInOut(response: CheckOutResponse?)
    func successUpdateCheckInOut()
    func onError(errorMessage: String)
}

class CheckInModel: BaseModel {
    var viewModel: CheckInHostessObserver?
    var server: ServerDataManager2<ReasonMissingResponse>?
    var server2: ServerDataManager2<CheckOutResponse>?
    var server3: ServerDataManager2<UpdateCheckOutResponse>?
    var urlReasonMissing = AppDelegate.API_TOTAL + ApiDefinition.API_REASON_MISSING
    var urlConsultCheckIn = AppDelegate.API_TOTAL + ApiDefinition.API_CHECK_OUT
    var urlUpdateCheckIn = AppDelegate.API_TOTAL + ApiDefinition.API_UPDATE_CHECK_OUT

    init(observer: BaseViewModelObserver) {
        super.init(observerVM: observer)
        server = ServerDataManager2<ReasonMissingResponse>()
        server2 = ServerDataManager2<CheckOutResponse>()
        server3 = ServerDataManager2<UpdateCheckOutResponse>()
    }

    func atachModel(viewModel: CheckInHostessObserver) {
        self.viewModel = viewModel
    }

    func reasonMissing(request: ReasonMissingRequest) {
        urlReasonMissing = AppDelegate.API_TOTAL + ApiDefinition.API_REASON_MISSING
        server?.setValues(requestUrl: urlReasonMissing, delegate: self, headerType: .headersMiddle)
        server?.request(requestModel: request)
    }

    func consultCheckIn(request: CheckOutRequest) {
        urlConsultCheckIn = AppDelegate.API_TOTAL + ApiDefinition.API_CHECK_OUT
        server2?.setValues(requestUrl: urlConsultCheckIn, delegate: self, headerType: .headersMiddle)
        server2?.request(requestModel: request)
    }

    func UpdateCheckIn(request: UpdateCheckOutRequest) {
        urlUpdateCheckIn = AppDelegate.API_TOTAL + ApiDefinition.API_UPDATE_CHECK_OUT
        server3?.setValues(requestUrl: urlUpdateCheckIn, delegate: self, headerType: .headersMiddle)
        server3?.request(requestModel: request)
    }

    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if requestUrl == urlReasonMissing {
            let response = response as! ReasonMissingResponse
            if response.MResult?.Result == "0" {
                self.viewModel?.successReasonMissing()
            } else {
                viewModel?.onError(errorMessage: response.MResult?.ResultDescription ?? "Error al agregar la falta")
            }

        } else  if requestUrl == urlConsultCheckIn {
            let response = response as! CheckOutResponse
            if response.MResult?.Result == "0" {
                self.viewModel?.successConsultCheckInOut(response: response)
            } else {
                viewModel?.onError(errorMessage: response.MResult?.ResultDescription ?? "Error al consultar el check in y check out")
            }

        } else  if requestUrl == urlUpdateCheckIn {
            let response = response as! UpdateCheckOutResponse
            if response.MResult?.Result == "0" {
                self.viewModel?.successUpdateCheckInOut()
            } else {
                viewModel?.onError(errorMessage: response.MResult?.ResultDescription ?? "Error al hacer check in/out")
            }
        }
    }

    override func onErrorLoadResponse(requestUrl: String, messageError: String) {
        viewModel?.onError(errorMessage: messageError)
    }

    func getEmploye() -> EmployeeStreet? {
        let employee = RealManager.findFirst(object: EmployeeStreet.self)
        return employee
    }

    func getEventDetail() -> EventoDetail? {
        return RealManager.findFirst(object: EventoDetail.self)
    }

    func updateCheckinOut(checkin: String, checkout: String, typecheck: String, isCheckEvent: String, isReasonMissing: String) {
        guard let employee =  getEmploye()  else {
            return
        }
        RealManager.updateLine {
            employee.checkIn = checkin
            employee.checkOut = checkout
            employee.typeCheck = typecheck
            employee.isCheckInOutCurrentEvent = isCheckEvent
            employee.isReasonMissing = isReasonMissing
        }
    }

}
