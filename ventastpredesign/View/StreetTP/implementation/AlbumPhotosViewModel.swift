//
//  AlbumPhotosViewModel.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 10/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import Firebase
import FirebaseStorage
import FirebaseDatabase

protocol AlbumPhotoObserver {
    func successDownload()
    func successUpLoad()
    func onError(errorMessage: String)
}

class AlbumPhotosViewModel: BaseViewModel, AlbumModelObserver {

    var observer: AlbumPhotoObserver?
    var model: AlbumPhotosModel!

    override init(view: BaseVentasView) {
        super.init(view: view)
        model = AlbumPhotosModel(observer: self)
        model.atachModel(viewModel: self)
    }

    func atachView(observer: AlbumPhotoObserver) {
        self.observer = observer
    }

    func ConsultPhotos(idCategoty: String = "") {
        let request = DownloadUrlImageRequest()
        let profile = getEmploye()?.profileUser
        let id  = profile == "3" ? getEmploye()?.userId : "0"
        let categoria  = idCategoty == "" ? getEmploye()?.currentIdCategory : idCategoty
        TPCipher.shared.setKey(b64Key: Strings.SHA256_KEY)
        request.idUser = TPCipher.shared.code(plainText: id ?? "")
        request.idEvent = TPCipher.shared.code(plainText: getEmploye()?.currentIdEvent ?? "")
        request.idCategory = TPCipher.shared.code(plainText: categoria ?? "")
        request.MLogin = LoginApp()
        request.MLogin?.Ip = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_IP) ?? ""
        request.MLogin?.input = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_IP) ?? ""
        request.MLogin?.UserId =  TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_USER) ?? ""
        request.MLogin?.Password = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_SECRETWORD) ?? ""
        model.downloadPhoto(request: request)
    }

    func UploadPhotos(url: String) {
        let request = UploadUrlImageRequest()
        TPCipher.shared.setKey(b64Key: Strings.SHA256_KEY)
        request.url = TPCipher.shared.code(plainText: url)
        request.idUser = TPCipher.shared.code(plainText: getEmploye()?.userId ?? "")
        request.idEvent = TPCipher.shared.code(plainText: getEmploye()?.currentIdEvent ?? "")
        request.idCategory = TPCipher.shared.code(plainText: getEmploye()?.currentIdCategory ?? "")
        request.MLogin = LoginApp()
        request.MLogin?.Ip = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_IP) ?? ""
        request.MLogin?.input = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_IP) ?? ""
        request.MLogin?.UserId =  TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_USER) ?? ""
        request.MLogin?.Password = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_SECRETWORD) ?? ""
        model.uploadPhoto(request: request)
    }

    func successDownloadPhoto() {
        view.hideLoading()
        observer?.successDownload()
    }

    func successUpLoadPhoto() {
        view.hideLoading()
        observer?.successUpLoad()
    }

    func getPhotos() -> [photos] {
        return model.getPhotoDownload()
    }

    func getEmploye() -> EmployeeStreet? {
        return model.getEmploye()
    }

    func getDetailEvent() -> EventoDetail? {
        return model.getEventDetail()
    }

    func getUrlPhoto() -> String {
        var url = ""
        let photo = model.getPhotosUpload()
        if photo.count > 0 && photo[0].url != "" {
            url = photo[0].url
        }
        return url
    }

    func updateConnectFirebase(connect: String) {
        model.updateConnectFirebase(connect: connect)
    }

    func saveUrlPhoto(url: String) {
        model.saveUrlPhoto(url: url)
    }

    func setNamePhoto() -> String {
        var result = ""
        let employee = getEmploye()
        let date = DateCalendar.convertTodayString(formatter: Formate.yyyy_MM_dd_HHmmssSSS)
        result = "\(employee?.numEmployee ?? "") - \(date)"
        return result
    }

    func uploadProfileImage(imageData: Data) {
        view.showLoading(message: "")
        let employee = getEmploye()
        let activityDesencrypt = TPCipher.shared.decode(coded: getDetailEvent()?.NameCampaign ?? "")
        let campign = activityDesencrypt
        let activity = employee?.currentTitleEvent
        let idEvent = employee?.currentIdEvent
        let category = employee?.currentTitleCategory
        let archive = AppDelegate.API_TOTAL == "https://mss.totalplay.com.mx/" ? "Street_release" : "Street_debug"

        let storageReference = Storage.storage().reference()
        let profileImageRef = storageReference.child(archive).child(campign ?? "").child(activity ?? "").child("event_\(String(idEvent ?? "0"))").child(category ?? "").child("\(setNamePhoto()).jpeg")
        let uploadMetaData = StorageMetadata()
        uploadMetaData.contentType = "image/jpeg"

        profileImageRef.putData(imageData, metadata: uploadMetaData) { (uploadedImageMeta, error) in

            if error != nil {
                print("Error took place \(String(describing: error?.localizedDescription))")
                return
            } else {
                profileImageRef.downloadURL(completion: { (url, _) in
                    if let url = url {
                        self.saveUrlPhoto(url: String(describing: url))
                        print(url)
                        if self.getUrlPhoto() != "" {
                            self.UploadPhotos(url: self.getUrlPhoto())
                        } else {
                            self.observer?.onError(errorMessage: "No fue posible guardar la foto, vuelva a intentar")
                        }
                        self.view.hideLoading()
                    } else {

                    }
                    print("Meta data of uploaded image \(String(describing: uploadedImageMeta))")
                })
            }
        }

    }

    func onError(errorMessage: String) {
        view.hideLoading()
        observer?.onError(errorMessage: errorMessage)
    }

}
