//
//  AlbumPhotosModel.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 10/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import RealmSwift

protocol AlbumModelObserver {
    func successDownloadPhoto()
    func successUpLoadPhoto()
    func onError(errorMessage: String)
}

class AlbumPhotosModel: BaseModel {
    var viewModel: AlbumModelObserver?
    var server: ServerDataManager2<DownloadUrlImageResponse>?
    var server2: ServerDataManager2<UploadUrlImageResponse>?
    var urldownloadPhoto = AppDelegate.API_TOTAL + ApiDefinition.API_DOWNLOAD_PHOTO
    var urlUpLoadPhoto = AppDelegate.API_TOTAL + ApiDefinition.API_UPLOAD_PHOTO

    init(observer: BaseViewModelObserver) {
        super.init(observerVM: observer)
        server = ServerDataManager2<DownloadUrlImageResponse>()
        server2 = ServerDataManager2<UploadUrlImageResponse>()
    }

    func atachModel(viewModel: AlbumModelObserver) {
        self.viewModel = viewModel
    }

    func downloadPhoto(request: DownloadUrlImageRequest) {
        urldownloadPhoto = AppDelegate.API_TOTAL + ApiDefinition.API_DOWNLOAD_PHOTO
        server?.setValues(requestUrl: urldownloadPhoto, delegate: self, headerType: .headersMiddle)
        server?.request(requestModel: request)
    }

    func uploadPhoto(request: UploadUrlImageRequest) {
        urlUpLoadPhoto = AppDelegate.API_TOTAL + ApiDefinition.API_UPLOAD_PHOTO
        server2?.setValues(requestUrl: urlUpLoadPhoto, delegate: self, headerType: .headersMiddle)
        server2?.request(requestModel: request)
    }

    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if requestUrl == urldownloadPhoto {
            let response = response as! DownloadUrlImageResponse
            if response.MResult?.Result == "0" {
                saveDownloadPhoto(response: response)
                self.viewModel?.successDownloadPhoto()
            } else {
                viewModel?.onError(errorMessage: response.MResult?.ResultDescription ?? "Error al descargar las fotos")
            }

        } else  if requestUrl == urlUpLoadPhoto {
            let response = response as! UploadUrlImageResponse
            if response.MResult?.Result == "0" {
                self.viewModel?.successUpLoadPhoto()
            } else {
                viewModel?.onError(errorMessage: response.MResult?.ResultDescription ?? "Error al subir la foto")
            }
        }
    }

    override func onErrorLoadResponse(requestUrl: String, messageError: String) {
        viewModel?.onError(errorMessage: messageError)
    }

    func saveDownloadPhoto(response: DownloadUrlImageResponse) {
        RealManager.deleteAll(object: photos.self)
        RealManager.insertCollection(objects: response.Mphotos)
    }

    func saveUrlPhoto(url: String) {
        RealManager.deleteAll(object: photoUpload.self)
        let photo = photoUpload()
        photo.url = url
        _ = RealManager.insert(object: photo)

    }

    func getPhotoDownload() -> [photos] {
        return RealManager.getAll(object: photos.self)
    }

    func getEmploye() -> EmployeeStreet? {
        let employee = RealManager.findFirst(object: EmployeeStreet.self)
        return employee
    }

    func getPhotosUpload() -> [photoUpload] {
        return RealManager.getAll(object: photoUpload.self)
    }

    func getEventDetail() -> EventoDetail? {
        return RealManager.findFirst(object: EventoDetail.self)
    }

     func updateConnectFirebase(connect: String) {
        guard let employee = RealManager.findFirst(object: EmployeeStreet.self) else {
            return
        }
        RealManager.updateLine {
            employee.connectFirebase = connect
        }
    }

}
