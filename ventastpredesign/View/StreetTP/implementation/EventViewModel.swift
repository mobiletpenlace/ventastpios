//
//  EventViewModel.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 10/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus
import MapKit
import Firebase
import FirebaseStorage
import FirebaseDatabase

protocol EventObserver {
    func retryEvent(id: String)
    func pickerOptions(value: String, id: String)
    func successEvents()
    func successDetailEvent()
    func successListHostes()
    func successAssignedHostes()
    func successCategory()
    func successDeleteEvent()
    func successDeleteHostess()
    func successEditEvent()
    func onError(errorMessage: String)
}

class EventViewModel: BaseViewModel, EventModelObserver {
    var observer: EventObserver?
    var model: EventModel!
    let numWeek = ["1 semana", "2 semanas", "3 semanas", "4 semanas"]
    let numDays = [7, 14, 21, 28]
    let colorBar = [Colors.PRIMARY, Colors.SECONDARY, Colors.LIGHT_GREEN, Colors.DARK_GREEN]
    var isEventQuality = false

    override init(view: BaseVentasView) {
        super.init(view: view)
        model = EventModel(observer: self)
        model.atachModel(viewModel: self)
    }

    func atachView(observer: EventObserver) {
        self.observer = observer

        SwiftEventBus.onMainThread(self, name: "WeekPicker") { [weak self] result in
            if let value = result?.object as? String {
                self?.observer?.pickerOptions(value: value, id: "week")
            }
        }

        SwiftEventBus.onMainThread(self, name: "hostessPicker") { [weak self] result in
            if let value = result?.object as? String {
                self?.observer?.pickerOptions(value: value, id: "hostess")
            }
        }

        SwiftEventBus.onMainThread(self, name: "reasonPicker") { [weak self] result in
            if let value = result?.object as? String {
                self?.observer?.pickerOptions(value: value, id: "reason")
            }
        }

        SwiftEventBus.onMainThread(self, name: "activityPicker") { [weak self] result in
            if let value = result?.object as? String {
                self?.observer?.pickerOptions(value: value, id: "activity")
            }
        }

        SwiftEventBus.onMainThread(self, name: "campaignPicker") { [weak self] result in
            if let value = result?.object as? String {
                self?.observer?.pickerOptions(value: value, id: "campaign")
            }
        }

        SwiftEventBus.onMainThread(self, name: "hourStartPicker") { [weak self] result in
            if let value = result?.object as? String {
                self?.observer?.pickerOptions(value: value, id: "hourStart")
            }
        }

        SwiftEventBus.onMainThread(self, name: "hourFinishPicker") { [weak self] result in
            if let value = result?.object as? String {
                self?.observer?.pickerOptions(value: value, id: "hourFinish")
            }
        }

        SwiftEventBus.onMainThread(self, name: "oneStartPicker") { [weak self] result in
            if let value = result?.object as? String {
                self?.observer?.pickerOptions(value: value, id: "oneStart")
            }
        }

        SwiftEventBus.onMainThread(self, name: "RetryEventHostess") { [weak self] _ in
            self?.observer?.retryEvent(id: "eventHostess")
        }

        SwiftEventBus.onMainThread(self, name: "RetryEventDetail") { [weak self] _ in
            self?.observer?.retryEvent(id: "eventDetail")
        }

    }

    func consultListEvents(startDate: String, endDate: String, idUser: String) {
        let request = EventListRequest()
        TPCipher.shared.setKey(b64Key: Strings.SHA256_KEY)
        request.endDate = TPCipher.shared.code(plainText: endDate)
        request.startDate = TPCipher.shared.code(plainText: startDate)
        request.idUser = TPCipher.shared.code(plainText: idUser)
        request.Mlogin = LoginApp()
        request.Mlogin?.Ip = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_IP) ?? ""
        request.Mlogin?.input = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_IP) ?? ""
        request.Mlogin?.UserId =  TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_USER) ?? ""
        request.Mlogin?.Password = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_SECRETWORD) ?? ""
        model.consultEvents(request: request)
    }

    func consultDetailEvent(idEvent: String) {
        let request = EventDetailsRequest()
        TPCipher.shared.setKey(b64Key: Strings.SHA256_KEY)
        request.idEvent = TPCipher.shared.code(plainText: idEvent)
        request.MLogin = LoginApp()
        request.MLogin?.Ip = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_IP) ?? ""
        request.MLogin?.input = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_IP) ?? ""
        request.MLogin?.UserId =  TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_USER) ?? ""
        request.MLogin?.Password = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_SECRETWORD) ?? ""
        model.detailEvents(request: request)
    }

    func ConsultListHostess(idUser: String = "") {
        let request = HostessSupervisorRequest()
        let id  = idUser == "" ? getEmploye()?.userId : idUser
        TPCipher.shared.setKey(b64Key: Strings.SHA256_KEY)
        request.idSupervisor = TPCipher.shared.code(plainText: id ?? "")
        request.MLogin = LoginApp()
        request.MLogin?.Ip = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_IP) ?? ""
        request.MLogin?.input = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_IP) ?? ""
        request.MLogin?.UserId =  TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_USER) ?? ""
        request.MLogin?.Password = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_SECRETWORD) ?? ""
        model.searchHostess(request: request)
    }

    func AddAssignedHostess() {
        let request = AssignedHostessRequest()
        TPCipher.shared.setKey(b64Key: Strings.SHA256_KEY)
        request.idEvent = TPCipher.shared.code(plainText: getEmploye()?.currentIdEvent ?? "")
        request.MHostess = [Hostess]()
        request.MHostess = getArrHostessSelect()
        request.Mlogin = LoginApp()
        request.Mlogin?.Ip = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_IP) ?? ""
        request.Mlogin?.input = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_IP) ?? ""
        request.Mlogin?.UserId =  TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_USER) ?? ""
        request.Mlogin?.Password = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_SECRETWORD) ?? ""
        model.assignedHostess(request: request)
    }

    func ConsultCategoryTree(idGeneric: String) {
        let request = CategoryRequest()
        TPCipher.shared.setKey(b64Key: Strings.SHA256_KEY)
        request.idGeneric = TPCipher.shared.code(plainText: idGeneric )
        request.MLogin = LoginApp()
        request.MLogin?.Ip = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_IP) ?? ""
        request.MLogin?.input = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_IP) ?? ""
        request.MLogin?.UserId =  TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_USER) ?? ""
        request.MLogin?.Password = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_SECRETWORD) ?? ""
        model.consultCategory(request: request)
    }

    func deleteEvent(idEvent: String) {
       let request = DeleteEventRequest()
        TPCipher.shared.setKey(b64Key: Strings.SHA256_KEY)
        request.MDeleteEvent = eliminarEvento()
        request.MDeleteEvent?.p_eeo_id = TPCipher.shared.code(plainText: idEvent) ?? ""
        request.MLogin = LoginApp()
        request.MLogin?.input = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_IP) ?? ""
        request.MLogin?.UserId =  TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_USER) ?? ""
        request.MLogin?.Password = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_SECRETWORD) ?? ""
        model.sendDeleteEvent(request: request)
    }

    func deleteHostess(idHostess: String, idEvent: String) {
       let request = DeleteHostessRequest()
        TPCipher.shared.setKey(b64Key: Strings.SHA256_KEY)
        request.MDeleteHostess = eliminarHostess()
        request.MDeleteHostess?.eeo_id = TPCipher.shared.code(plainText: idEvent) ?? ""// idHostess) ?? ""
        request.MDeleteHostess?.eua_id = TPCipher.shared.code(plainText: idHostess) ?? ""// idEvent) ?? ""
        request.MLogin = LoginApp()
        request.MLogin?.input = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_IP) ?? ""
        request.MLogin?.UserId =  TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_USER) ?? ""
        request.MLogin?.Password = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_SECRETWORD) ?? ""
        model.sendDeleteHostess(request: request)
    }

    func editEvent(idEvent: String, idCampaign: String, idActivity: String, date: String, hourStart: String, hourFinish: String, description: String, longitude: String, latitude: String) {
        let request = EditEventRequest()
        TPCipher.shared.setKey(b64Key: Strings.SHA256_KEY)
        request.MEditEvent = EditEvent()
        request.MEditEvent?.p_eeo_date_ini = TPCipher.shared.code(plainText: (date + " " + hourStart)) ?? ""
        request.MEditEvent?.p_eeo_date_fin = TPCipher.shared.code(plainText: (date + " " + hourFinish)) ?? ""
        request.MEditEvent?.p_eeo_descr = TPCipher.shared.code(plainText: description) ?? ""
        request.MEditEvent?.p_eeo_coo_lat = TPCipher.shared.code(plainText: latitude) ?? ""
        request.MEditEvent?.p_eeo_coo_long = TPCipher.shared.code(plainText: longitude) ?? ""
        request.MEditEvent?.p_ecg_id_camp = TPCipher.shared.code(plainText: idCampaign) ?? ""
        request.MEditEvent?.p_ecg_id_act = TPCipher.shared.code(plainText: idActivity) ?? ""
        request.MEditEvent?.p_eua_update_by = TPCipher.shared.code(plainText: getEmploye()?.userId ?? "") ?? ""
        request.MEditEvent?.p_eeo_id = TPCipher.shared.code(plainText: idEvent) ?? ""
        request.MLogin = LoginApp()
        request.MLogin?.input = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_IP) ?? ""
        request.MLogin?.UserId =  TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_USER) ?? ""
        request.MLogin?.Password = TPCipher.shared.code(plainText: Strings.LOGIN_REQUEST_SECRETWORD) ?? ""
        model.sendEditEvent(request: request)
    }

    func successSearchEvent() {
        if isEventQuality {
            view.hideLoading()
            completeDataEventSort()
            updateDayOfYearHourFinalEvent()
            updateNameDate()
            // getEventSortDateQuality()
        } else {
            view.hideLoading()
            completeDataEventSort()
            updateNameDate()
            validateFirstLastEvent()
        }

    }

    func successDetailEvent() {
        view.hideLoading()
        observer?.successDetailEvent()
    }

    func successDeleteEvent() {
        view.hideLoading()
        observer?.successDeleteEvent()
    }

    func successDeleteHostess() {
        view.hideLoading()
        observer?.successDeleteHostess()
    }

    func successSearchHostes() {
        view.hideLoading()
        observer?.successListHostes()
    }

    func successAssignedHostess() {
        view.hideLoading()
        observer?.successAssignedHostes()
    }

    func successListCategory() {
        view.hideLoading()
        observer?.successCategory()
    }

    func successEditEvent() {
        view.hideLoading()
        observer?.successEditEvent()
    }

    func getColor(position: Int) -> UIColor {
        return colorBar[position] ?? UIColor.white
    }

    func getArrHostessSelect() -> [Hostess] {
        var arrHostess: [Hostess] = []
        let hostess = getHostessActiveEvent()
        for index in hostess {
            if index.isSelect == "1" {
                let hos = Hostess()
                hos.idHostess =  index.id
                arrHostess.append(hos)
            }
        }
        return arrHostess
    }

    func getHostessEvent() -> [hostessDetails] {
        return model.getHostessEvent()
    }

    func getHostessActiveEvent() -> [team] {
        let hostess = getHostessTotal()
        var hostessActive: [team] = []
        for index in hostess {
            if TPCipher.shared.decode(coded: index.activo) != nil {
                let activo = TPCipher.shared.decode(coded: index.activo)
                if activo == "1" {
                    hostessActive.append(index)
                }
            }
        }
        return hostessActive
    }

    func getHostessDesencripte() -> [team] {
        var hostess: [team] = []
        for index in getHostessActiveEvent() {
            let host = team()
            host.name = TPCipher.shared.decode(coded: index.name)!
            host.numeroEmpleado = TPCipher.shared.decode(coded: index.numeroEmpleado)!
            host.id = TPCipher.shared.decode(coded: index.id)!
            host.activo = TPCipher.shared.decode(coded: index.activo)!
            host.puesto = TPCipher.shared.decode(coded: index.puesto)!
            hostess.append(host)
        }
        return hostess
    }

    func getHostessTotal() -> [team] {
        return model.getHostess()
    }

    func getDetailEvent() -> EventoDetail? {
        return model.getEventDetail()
    }

    func getEmploye() -> EmployeeStreet? {
        return model.getEmploye()
    }

    func getEvents() -> [Events] {
        return model.sortDateEvent()
    }

    func getCategory() -> [tree] {
        var treeDecode: [tree] = []
        let treeEncode = model.getCategory()
        for index in treeEncode {
            let category = tree()
            category.id = TPCipher.shared.decode(coded: index.id) ?? ""
            category.descriptionTree = TPCipher.shared.decode(coded: index.descriptionTree) ?? ""
            treeDecode.append(category)
        }
        return treeDecode
    }

    func getEventCalendar() -> EventCalendar? {
        return model.getEventCalendar()
    }

    func getCurrentDetailEvent() -> DetailEvent? {
        return model.getCurrentDetailEvent()
    }

    func initEventCalendar() {
        model.saveEventCalendar()
    }

    func updateConnectFirebase(connect: String) {
        model.updateConnectFirebase(connect: connect)
    }

    func updateIsSelectHostess(id: String, isSelect: String) {
        model.updateIsSelectHostess(id: id, isSelect: isSelect)
    }

    func updateIsSelectCategory(id: String, isSelect: String) {
        model.updateIsSelectCategory(id: id, isSelect: isSelect)
    }

    func updateWeekDay(numberDay: Int, week: String, positionPicker: Int) {
        model.updateEventCalendar(week: week, days: numberDay, positionPicker: positionPicker)
    }

    func updateLocation(latitude: Double, longitude: Double) {
        model.updateLocation(latitude: latitude, longitude: longitude)
    }

    func updateLocationEvent(latitude: String, longitude: String) {
        let lat = Double(latitude) ?? 0.0
        let long = Double(longitude) ?? 0.0
        model.updateLocationEvent(latitude: lat, longitude: long)
    }

    func updateIdActivity(id: String, title: String) {
        model.updateIdActivity(idActivity: id, title: title)
    }

    func updateIdEvent(idEvent: String, title: String) {
        model.updateIdEvent(idEvent: idEvent, titleEvent: title)
    }

    func updateIdCategory(id: String, title: String) {
        model.updateIdCategory(idCategory: id, title: title)
    }

    func updateNameSupervisor(name: String) {
        model.updateNameSupervisor(nameSupervisor: name)
    }

    func updateCurrentDetailEvent(_ event: DetailEvent) {
        model.updateCurrentDetailEvent(event)
    }

    func getCurrentSupervisor() -> Supervisor? {
        return model.getCurrentSupervisor()
    }

    func saveCurrentSupervisor(_ supervisor: Supervisor) {
        model.updateCurrentSupervisor(supervisor)
    }

    func onError(errorMessage: String) {
        view.hideLoading()
        observer?.onError(errorMessage: errorMessage)
    }

    func dateToday() -> String {
        return DateCalendar.convertTodayString(formatter: Formate.dd_MM_yyyy)
    }

    func authUser(customToken: String) {
       Auth.auth().signIn(withCustomToken: customToken ) { (user, _) in
           if user != nil {
               self.updateConnectFirebase(connect: "1")
               print("Conectado a Firebase")
           } else {
               self.updateConnectFirebase(connect: "0")
               print("Error al conectar a Firebase")
           }

       }
   }

    func dateEventDetail(startDate: String, hourStart: String, hourFinish: String) -> String {
        guard let startDate = TPCipher.shared.decode(coded: startDate) else { return ""}
        let hourS = String(TPCipher.shared.decode(coded: hourStart)!)
        let hourE = String(TPCipher.shared.decode(coded: hourFinish)!)
        guard let startDateFormate = DateCalendar.convertStringDate(date: startDate, formatter: Formate.dd_MM_yyyy) else {return ""}
        let numberDay = String(format: "%02d", DateCalendar.convertDateInt(date: startDateFormate, component: .day))
        let nameDay = DateCalendar.convertDateString(date: startDateFormate, formatter: Formate.EEEE)
        let date = "\(hourEventFormat(hour: hourS)) - \(hourEventFormat(hour: hourE))"                    // hourAMPM(hourStart: hourStart, hourFinish: hourFinish)
        return "\(nameDay) \(numberDay), \(date)"
    }

    //    func hourAMPM(hourStart: String, hourFinish: String) -> String {
    //        TPCipher.shared.setKey(b64Key: Strings.SHA256_KEY)
    //        guard let start = TPCipher.shared.decode(coded: hourStart) else{ return ""}
    //        guard let finish = TPCipher.shared.decode(coded: hourFinish) else{ return ""}
    //        return "\(hourEventFormat(hour: start)) - \(hourEventFormat(hour: finish))"
    //    }

    func futureDate(startDate: String, days: Int) -> String {
        guard let dateStart = DateCalendar.convertStringDate(date: startDate, formatter: Formate.dd_MM_yyyy) else {return "" }
        guard let dateFuture =  DateCalendar.futureDate(date: dateStart, value: days, component: .day) else {return ""}
        return DateCalendar.convertDateString(date: dateFuture, formatter: Formate.dd_MM_yyyy)
    }

    func getPositionWeekSelect(value: String) -> Int {
        for (index, element) in numWeek.enumerated() {
            if value == element {
                return index
            }
        }
        return 0
    }

    func completeDataEventSort() {
        let listEvents = model.getEvents()
        for element in listEvents {
            let dayYear = updateDayYear(dateEvent: element.startDate, hourEvent: element.startHour)
            model.updateDataEvent(id: element.idEvent, dayOfYear: dayYear)
        }
    }

    func getEventSortDateQuality() -> [Events] {
        let events = model.sortDateEvent()
        var result: [Events] = []
        let timeIntervalCurrent = Int(Date().timeIntervalSince1970)// 234234233
        print(timeIntervalCurrent)
        print("Intevale current ")
        if events != [] {
            for index in events {
                if index.dayOfYearHourFinal < timeIntervalCurrent {
                    result.append(index)
                }
            }
        }
        // model.saveEventListQuality(events: result)
        return result

    }

    private func updateDayYear(dateEvent: String, hourEvent: String) -> Int {
        TPCipher.shared.setKey(b64Key: Strings.SHA256_KEY)
        guard let dateDecrypt = TPCipher.shared.decode(coded: dateEvent) else { return 0}
        guard let hourDecrypt = TPCipher.shared.decode(coded: hourEvent) else { return 0}
        let dateComplete = dateDecrypt + "T"  + hourDecrypt
        guard let date = DateCalendar.convertStringDate(date: dateComplete, formatter: Formate.dd_MM_yyyy_HHmm) else {return 0}
        let result = Int(date.timeIntervalSince1970)
        return result
    }

    private func updateDayOfYearHourFinalEvent() {
        TPCipher.shared.setKey(b64Key: Strings.SHA256_KEY)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy'T'HH:mm"
        dateFormatter.timeZone = .current
        dateFormatter.locale = Locale(identifier: "es_MX")
        let events = model.getEvents()
        for (index, _) in events.enumerated() {
            guard let dateDecrypt = TPCipher.shared.decode(coded: events[index].startDate) else { return}
            guard let hourDecrypt = TPCipher.shared.decode(coded: events[index].endHour) else { return}
            let dateComplete = dateDecrypt + "T"  + hourDecrypt
            guard let date = dateFormatter.date(from: dateComplete) else { return}
            model.updateDayOfYearEventHourFinal(id: events[index].idEvent, dayOfYear: Int(date.timeIntervalSince1970))
        }
    }

    private func updateNameDate() {
        let today = DateCalendar.convertTodayString(formatter: Formate.dd_MM_yyyy)
        var dateEvent = ""
        var nameDay = ""
        var numberDay = ""
        let listEvents = model.sortDateEvent()
        for (index, element) in listEvents.enumerated() {
            var showViewDate = true
            guard let startDate = TPCipher.shared.decode(coded: element.startDate) else { return}
            guard let startDateFormate = DateCalendar.convertStringDate(date: startDate, formatter: Formate.dd_MM_yyyy) else {return}
            if startDate == today {
                numberDay = "HOY"
                nameDay = ""
            } else {
                numberDay = String(format: "%02d", DateCalendar.convertDateInt(date: startDateFormate, component: .day))
                nameDay = DateCalendar.convertDateString(date: startDateFormate, formatter: Formate.EE)
            }
            if dateEvent != startDate {
                showViewDate = false
                if index != 0 {
                    changeColor()
                }
            }
            dateEvent = startDate
            model.updateShowDateColor(positionColor: model.getEventCalendar()?.positionColor ?? 0, showDate: showViewDate, id: listEvents[index].idEvent, numberDay: numberDay, nameDay: nameDay)
        }

        if isEventQuality {
            observer?.successEvents()
        }
    }

    func validateFirstLastEvent() {
        if getEmploye()?.profileUser == "3" {
            var idEventFinal  = ""
            var idEventStart  = ""
            var result: [Events] = []
            let tomorrow = DateCalendar.convertTomorrowString(formatter: Formate.dd_MM_yyyy)
            let tomorrowDate = DateCalendar.convertStringDate(date: tomorrow, formatter: Formate.dd_MM_yyyy) ?? Date()
            let tomorrowInterval = Int(DateCalendar.convertTimeIntervalSince(date: tomorrowDate))

            let today = DateCalendar.convertTodayString(formatter: Formate.dd_MM_yyyy)
            let todayDate = DateCalendar.convertStringDate(date: today, formatter: Formate.dd_MM_yyyy) ?? Date()
            let todayInterval = Int(DateCalendar.convertTimeIntervalSince(date: todayDate))

            for  element in getEvents() {
                if element.dayOfYear > todayInterval {
                    if element.dayOfYear < tomorrowInterval {
                        if idEventStart == "" {
                            idEventStart = element.idEvent
                        }
                        idEventFinal = element.idEvent
                    }
                }
            }
            getFirstLastEvent(idEventFinal: idEventFinal, idEventStart: idEventStart)
        }
        observer?.successEvents()
    }

    func clearCategory() {
        model.deleteCategory()
    }

    func getFirstLastEvent(idEventFinal: String, idEventStart: String) {
        guard let firstEvent = TPCipher.shared.decode(coded: idEventStart) else { return  }
        guard let lastEvent =  TPCipher.shared.decode(coded: idEventFinal) else {return}
        model.updateFirstLastEvent(first: firstEvent, last: lastEvent)
    }

    private func changeColor() {
        guard let eventCalendar = getEventCalendar() else {
            return
        }
        let currentPosition = eventCalendar.positionColor
        var newPosition = 0
        if currentPosition != 3 {
            newPosition = currentPosition + 1
        }
        model.updateColor(position: newPosition)
    }

    func hourEventFormat(hour: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        dateFormatter.locale = Locale(identifier: "es_MX_POSIX")
        let date = dateFormatter.date(from: hour)
        dateFormatter.dateFormat = "h:mm a"
        dateFormatter.amSymbol = "AM"
        dateFormatter.pmSymbol = "PM"
        let Date12 = dateFormatter.string(from: date!).uppercased()
        return Date12
    }

    func calculateDistanceTwoPoint(from coord1: CLLocationCoordinate2D, to coord2: CLLocationCoordinate2D) -> Double {
        let distanceLong: Double = 71.5 * (coord1.longitude - coord2.longitude)
        let squareDistanceLong = pow(distanceLong, 2)
        let distanceLat: Double = 111.3 * (coord1.latitude - coord2.latitude)
        let squareDistanceLat = pow(distanceLat, 2)
        let distance = (squareDistanceLong + squareDistanceLat).squareRoot()
        let distanceInMeters = distance * 1000.0
        return distanceInMeters
    }

    func validateDistanceBetweenEventUser() {
        guard let employee = getEmploye() else {return}
        guard let event = getDetailEvent() else {return}
        let coordinateEvent = CLLocationCoordinate2D(latitude: event.latitudeDouble, longitude: event.longitudDouble)
        let coordinateUser = CLLocationCoordinate2D(latitude: employee.currentLatitude, longitude: employee.currentLongitude)
        let distance = employee.profileUser == "3" ? calculateDistanceTwoPoint(from: coordinateUser, to: coordinateEvent) : 200.00
        let isNearEvent = distance > 200 ? false : true
        model.updateDistanceUserToEvent(distance: distance, isNear: isNearEvent)
    }

    func getPosition(array: [String], value: String) -> Int {
        var pos = 0
        for (index, element) in array.enumerated() {
            if value == element {
                pos = index
            }
        }
        return pos
    }

    func eventSchedule(startDate: String, hourStart: String, hourEnd: String) -> String {
        TPCipher.shared.setKey(b64Key: Strings.SHA256_KEY)
        let date = TPCipher.shared.decode(coded: startDate)
        let hourS = String(TPCipher.shared.decode(coded: hourStart)!)
        let hourE = String(TPCipher.shared.decode(coded: hourEnd)!)
        let nameDay = Dates.nameDay(date: date!, long: true)
        let numberDay = Dates.numberDay(date: date!)
        return "\(nameDay) \(numberDay), \(hourEventFormat(hour: hourS)) - \(hourEventFormat(hour: hourE) )"
    }
}
