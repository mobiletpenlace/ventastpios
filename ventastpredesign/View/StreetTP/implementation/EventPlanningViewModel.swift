//
//  EventPlanningViewModel.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 08/09/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit

protocol EventPlanningObserver {
    func successSearchEventPlanning()
    func successCategoryEvaluation()
    func successAddEvent()
    func successDeleteEvent()
    func successDataUser()
    func onError(errorMessage: String)
}

class EventPlanningViewModel: BaseViewModel, EventPlanningObserver, EventPlanningModelObserver {
    var observer: EventPlanningObserver?
    var model: EventPlanningModel!
    let colorBar = [Colors.PRIMARY, Colors.SECONDARY, Colors.LIGHT_GREEN, Colors.DARK_GREEN]

    override init(view: BaseVentasView) {
        super.init(view: view)
        model = EventPlanningModel(observer: self)
        model.atachModel(viewModel: self)
    }

    func atachView(observer: EventPlanningObserver) {
        self.observer = observer
    }

    func ConsultEventsPlanning(numSupervisor: String) {
        view.showLoading(message: "")
        model.listEventPlanning(numberSupervisor: numSupervisor)
    }

    func DeleteEventsPlanning(numSupervisor: String, eventos: ListaEventos, position: Int) {
        model.deleteEventPlaning(numberSupervisor: numSupervisor, listaEventos: eventos, position: position )
    }

    func ConsultListEvaluacion() {
        model.listEvaluationCategory()
    }

    func ConsultUserData(idSupervisor: String) {
        model.userDataPlanning(idSupervisor: idSupervisor)
    }

    func AddEventsPlanning(listaEvento: ListaEventos?, numSupervisor: String, idSupervisor: String, nameSupervisor: String, idCampaign: String, nameCampaign: String, idActivity: String, nameActivity: String, date: String, hourStart: String, hourFinish: String, description: String, longitude: String, latitude: String) {
        var event = Eventos()
        event.selectedItem = true
        event.nombreactividad = Nombreactividad()
        event.nombreactividad?.editNombreActividad = false
        event.nombreactividad?.object = MObject()
        event.nombreactividad?.object?.id = Int(idCampaign)
        event.nombreactividad?.object?.descripcion = nameCampaign
        event.nombreactividad?.object?.idCatalogo = 0
        event.nombreactividad?.object?.descripcionCatalogo = nil
        event.objectUbicacion = ObjectUbicacion()
        event.objectUbicacion?.editObjectUbicacion = false
        event.objectUbicacion?.latitudSelected = Double(latitude )
        event.objectUbicacion?.longitudSelected = Double(longitude )
        event.objectUbicacion?.textSelectedPlace = description
        event.objetoFechas = ObjetoFechas()
        event.objetoFechas?.fechaFin = date
        event.objetoFechas?.fechaInicio = date
        event.objetoHoraFin = ObjetoHoraFin()
        event.objetoHoraFin?.horabase = hourFinish
        event.objetoHoraFin?.horatext = convertHourAmPm(hour: hourFinish, date: date)
        event.objetoHoraInicio = ObjetoHoraInicio()
        event.objetoHoraInicio?.horabase = hourStart
        event.objetoHoraInicio?.horatext = convertHourAmPm(hour: hourStart, date: date)
        event.supervisor = SupervisorEvent()
        event.supervisor?.descripcion = nameSupervisor
        event.supervisor?.id = idSupervisor
        event.supervisor?.idSupervisor = ""
        event.supervisor?.numeroempleado = numSupervisor
        event.tipoactividad = Tipoactividad()
        event.tipoactividad?.editTipoActividad = false
        event.tipoactividad?.object = MObject()
        event.tipoactividad?.object?.id = Int(idActivity)
        event.tipoactividad?.object?.descripcion = nameActivity
        event.tipoactividad?.object?.idCatalogo = 0
        event.tipoactividad?.object?.descripcionCatalogo = nil
        model.addEventPlaning(numberSupervisor: numSupervisor, listaEventos: listaEvento, evento: event )
    }

    func successSearchEventPlanning() {
        view.hideLoading()
        self.observer?.successSearchEventPlanning()
    }

    func successCategoryEvaluation() {
        view.hideLoading()
        self.observer?.successCategoryEvaluation()
    }

    func successAddEvent() {
        view.hideLoading()
        self.observer?.successAddEvent()
    }

    func successDeleteEvent() {
        view.hideLoading()
        self.observer?.successDeleteEvent()
    }

    func successDataUser() {
        view.hideLoading()
        self.observer?.successDataUser()
    }

    func onError(errorMessage: String) {
        view.hideLoading()
        observer?.onError(errorMessage: errorMessage)
    }

    func getEvents() -> ListaEventos? {
        return model.eventsPlanning
    }

    func getEvaluacionOptions() -> [OptionsEvaluation]? {
        return model.optionsEvaluation
    }

    func getUserData() -> userFFM? {
        return model.userData
    }

    func convertHourAmPm(hour: String, date: String) -> String {
        var resultHour = ""
        let completeDate = date + "T" + hour
        let hourDate = DateCalendar.convertStringDate(date: completeDate, formatter: Formate.dd_MM_yyyy_HHmm)
        let hourConvert  = DateCalendar.convertDateString(date: hourDate!, formatter: Formate.H_mm_a)
        resultHour = clearPoint(word: hourConvert)
        return resultHour
    }

    func clearPoint(word: String) -> String {
        var result = ""
        result = word.replacingOccurrences(of: ".", with: "")
        return result
    }

    func convertDateToNumberDay(date: String) -> String {
        var result = ""
        let dateConvert = DateCalendar.convertStringDate(date: date, formatter: Formate.dd_MM_yyyy)
        result = DateCalendar.convertDateString(date: dateConvert!, formatter: Formate.D)
        return result
    }

    func convertDateToNameDay(date: String) -> String {
        var result = ""
        let dateConvert = DateCalendar.convertStringDate(date: date, formatter: Formate.dd_MM_yyyy)
        result = DateCalendar.convertDateString(date: dateConvert!, formatter: Formate.EE)
        return result.capitalizingFirstLetter()
    }

    func getEventsSortDate() -> ListaEventos? {
        let events = model.eventsPlanning
        var eventsSuccess = ListaEventos()
        eventsSuccess = events!
        for (index, element) in eventsSuccess.eventos!.enumerated() {
            let dateEventString = element.objetoFechas?.fechaInicio ?? ""
            let hourEventString = element.objetoHoraInicio?.horabase ?? ""
            let joinDateHour = DateCalendar.convertStringDate(date: dateEventString+"T"+hourEventString, formatter: Formate.dd_MM_yyyy_HHmm)!
            let timeDate = DateCalendar.convertTimeInterval(date: joinDateHour)
            eventsSuccess.eventos![index].dayOfYearHourFinal = Int(timeDate)
        }
        eventsSuccess.eventos = eventsSuccess.eventos!.sorted { $0.dayOfYearHourFinal ?? 0 < $1.dayOfYearHourFinal ?? 0  }

        return eventsSuccess
    }

    func getColor(position: Int) -> UIColor {
        return colorBar[position]
    }

    func completeMainDesign(listEvents: ListaEventos) -> ListaEventos {
        var eventsSuccess = ListaEventos()
        eventsSuccess = listEvents
        var currentDateEvent = ""
        var nameDay = ""
        var numberDay = ""
        for (index, element) in eventsSuccess.eventos!.enumerated() {
            let dateEventString = element.objetoFechas?.fechaInicio ?? ""
            var showViewDate = true

            if dateIsCurrent(date: dateEventString) {
                numberDay = "HOY"
                nameDay = ""
            } else {
                numberDay = convertDateToNumberDay(date: dateEventString)
                nameDay = convertDateToNameDay(date: dateEventString)
            }

            if currentDateEvent != dateEventString {
                showViewDate = false
                if index != 0 {
                    changeColor()
                }
            }
            currentDateEvent = dateEventString

            eventsSuccess.eventos![index].indexColor = getIndexColor()
            eventsSuccess.eventos![index].showDate = showViewDate
            eventsSuccess.eventos![index].numberDay = numberDay
            eventsSuccess.eventos![index].nameDay = nameDay

        }

        return eventsSuccess
    }

    func dateIsCurrent(date: String) -> Bool {
        var result = false
        if date == DateCalendar.convertTodayString(formatter: Formate.dd_MM_yyyy) {
            result = true
        }
        return result
    }

    func getIndexColor() -> Int {
        let eventCalendar = model.getEventCalendar()
        return eventCalendar!.positionColor
    }

    func changeColor() {
        guard let eventCalendar = getEventCalendar() else {
            return
        }
        let currentPosition = eventCalendar.positionColor
        var newPosition = 0
        if currentPosition != 3 {
            newPosition = currentPosition + 1
        }
        model.updateColor(position: newPosition)
    }

    func getEventCalendar() -> EventCalendar? {
        return model.getEventCalendar()
    }

}
