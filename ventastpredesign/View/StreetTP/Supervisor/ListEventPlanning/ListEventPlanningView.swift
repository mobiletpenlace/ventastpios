//
//  ListEventPlanningView.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 12/11/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

class ListEventPlanningView: BaseVentasView {
    @IBOutlet weak var calendarTable: UITableView!
    var events: [Events] = []
    var listaEventos: ListaEventos?
    var mEventPlanningViewModel: EventPlanningViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mEventPlanningViewModel = EventPlanningViewModel(view: self)
    }

    override func viewDidAppear(_ animated: Bool) {
        calendarTable.reloadData()
    }

    func goDetailEvent(nameCampaign: String, nameActivity: String, dateEvent: String, hourStart: String, hourEnd: String, currentLatitud: Double, currentLongitud: Double, currentLocation: String) {
        let detail: NewEventDetailView = UIStoryboard(name: "NewEventDetailView", bundle: nil).instantiateViewController(withIdentifier: "NewEventDetailView") as! NewEventDetailView
        detail.isCreatingAnEvent = false
        detail.nameCampaign = nameCampaign
        detail.nameActivity = nameActivity
        detail.dateEvent = dateEvent
        detail.hourStart = hourStart
        detail.hourEnd = hourEnd
        detail.currentLatitud = String(currentLatitud)
        detail.currentLongitud = String(currentLongitud)
        detail.currentLocation = currentLocation
        self.present(detail, animated: false, completion: nil)
    }
}

extension ListEventPlanningView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
}

extension ListEventPlanningView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaEventos?.eventos?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           let cell = tableView.dequeueReusableCell(withIdentifier: "EventTableViewCell", for: indexPath) as! EventTableViewCell

        cell.barColorView.backgroundColor = mEventPlanningViewModel.getColor(position: listaEventos?.eventos![indexPath.row].indexColor ?? 0)
        cell.dateView.isHidden = (listaEventos?.eventos![indexPath.row].showDate) ?? false
        cell.numberDayLabel.text = listaEventos?.eventos![indexPath.row].numberDay ?? ""
        cell.dayWeekLabel.text =  listaEventos?.eventos![indexPath.row].nameDay ?? ""
        cell.selectButton.tag = indexPath.row
        cell.folioLabel.text = listaEventos?.eventos![indexPath.row].objectUbicacion!.textSelectedPlace ?? "Ubicación seleccionada"
        cell.selectButton.addTarget(self, action: #selector(handleButtonTapped(sender: )), for: .touchUpInside)
        cell.nameEventLabel.text =   listaEventos?.eventos![indexPath.row].tipoactividad!.object!.descripcion ?? ""
        cell.hourStartEndLabel.text = "\(listaEventos?.eventos![indexPath.row].objetoHoraInicio!.horatext!.uppercased() ?? "00:00 am" ) - \(listaEventos?.eventos![indexPath.row].objetoHoraFin!.horatext!.uppercased() ?? "00:00 am" )"
             return cell
    }

    @objc func handleButtonTapped(sender: UIButton) {
        goDetailEvent(nameCampaign: listaEventos?.eventos![sender.tag].nombreactividad?.object?.descripcion ?? "", nameActivity: listaEventos?.eventos![sender.tag].tipoactividad?.object?.descripcion ?? "", dateEvent: listaEventos?.eventos![sender.tag].objetoFechas?.fechaInicio ?? "", hourStart: listaEventos?.eventos![sender.tag].objetoHoraInicio?.horabase ?? "", hourEnd: listaEventos?.eventos![sender.tag].objetoHoraFin?.horabase ?? "", currentLatitud: listaEventos?.eventos![sender.tag].objectUbicacion?.latitudSelected ?? 0.0, currentLongitud: listaEventos?.eventos![sender.tag].objectUbicacion?.longitudSelected ?? 0.0, currentLocation: listaEventos?.eventos![sender.tag].objectUbicacion?.textSelectedPlace ?? "")

    }

}
