//
//  EventCalendarSupervisorView.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 06/09/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import CoreLocation

class EventCalendarSupervisorView: BaseVentasView {
    @IBOutlet weak var eventsContainerView: UIView!
    @IBOutlet weak var weekTextfield: UITextField!
    @IBOutlet weak var typeEventSegmented: UISegmentedControl!
    @IBOutlet weak var topConstraintWeekCheckList: NSLayoutConstraint!
    @IBOutlet weak var topContraintContainerView: NSLayoutConstraint!
    @IBOutlet weak var WeekCheckList: UIView!

    var mEventViewModel: EventViewModel!
    var mPlanningViewModel: EventPlanningViewModel!
    let locationManager = CLLocationManager()
    var positionPicker: Int = 0
    var eventCalendar: EventCalendar!
    var pickerSelectWeek: String = "1 semana"
    var isToQuality = false

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mEventViewModel = EventViewModel(view: self)
        mPlanningViewModel = EventPlanningViewModel(view: self)
        mPlanningViewModel.atachView(observer: self)
        mEventViewModel.atachView(observer: self)
        setUpView()
    }

    func setUpView() {
        // configLocation()
        // validateFirebase()
        mEventViewModel.initEventCalendar()
        getEventCalendar()
        consultInfoSupervisor()
        typeEventSegmented.isHidden = false
        topConstraintWeekCheckList.constant = 60
    }

    func consultInfoSupervisor() {
        mPlanningViewModel.ConsultUserData(idSupervisor: mEventViewModel.getEmploye()?.userId ?? "")
    }

    func getEventCalendar() {
        guard let calendar = mEventViewModel.getEventCalendar() else {return }
        eventCalendar = calendar
        positionPicker = calendar.positionPicker
        weekTextfield.text = calendar.week
        pickerSelectWeek = calendar.week
        consultEvent()
    }

    /*func validateFirebase() {
        mEventViewModel.authUser(customToken: mEventViewModel.getEmploye()?.tokenFirebase ?? "")
        if  mEventViewModel.getEmploye()?.connectFirebase == "0"{
            Constants.Alert(title: "", body: "No se pudo conectar a la base de datos de Firebase", type: type.Warning, viewC: self)
        }
    }*/

    func goAssignedEvents() {
        ViewEmbedder.embed(withIdentifier: "AssignedEventsView", parent: self, container: self.eventsContainerView) { _ in }
    }

    func goNoAssignedEvents() {
        ViewEmbedder.embed(withIdentifier: "NoAssignedEventsView", parent: self, container: self.eventsContainerView) { _ in }
    }

    func consultEvent() {
        mEventViewModel.isEventQuality = false
        let startDate = mEventViewModel.dateToday()
        let endDate = mEventViewModel.futureDate(startDate: startDate, days: self.mEventViewModel.numDays[self.positionPicker])
        let idUser = mEventViewModel.getEmploye()?.userId ?? ""
        mEventViewModel.consultListEvents(startDate: startDate, endDate: endDate, idUser: idUser)
    }

    @IBAction func backAction(_ sender: Any) {
        Constants.Back(viewC: self)
    }

    @IBAction func ChangePeriodAction(_ sender: Any) {
        Constants.OptionCombo(origen: "Week", array: mEventViewModel.numWeek, viewC: self)
        self.positionPicker = 0
    }

    @IBAction func typeEventActionSegmented(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            isToQuality = false
            getEventCalendar()
            topContraintContainerView.constant = 8
            WeekCheckList.isHidden = false

        case 1:
            isToQuality = true
            getEventCalendarQuality()
            topContraintContainerView.constant = -50
            WeekCheckList.isHidden = true
        case 2:
            isToQuality = false
            getPlanningEvent()
            topContraintContainerView.constant = -50
            WeekCheckList.isHidden = true
        default:
            break
        }
    }

    func getPlanningEvent() {
        guard let calendar = mEventViewModel.getEventCalendar() else {return }
        eventCalendar = calendar
        positionPicker = eventCalendar.positionPicker
        weekTextfield.text = eventCalendar.week
        pickerSelectWeek = eventCalendar.week
        getEventListPlanningService()
        print(eventCalendar)
    }

    func getEventCalendarQuality() {
        guard let calendar = mEventViewModel.getEventCalendar() else {return }
        eventCalendar = calendar
        positionPicker = eventCalendar.positionPicker
        weekTextfield.text = eventCalendar.week
        pickerSelectWeek = eventCalendar.week
        self.getEventListQualityService()
        print(eventCalendar)
    }

    func getEventListQualityService() {
        mEventViewModel.isEventQuality = true
        let startDate = Dates.futureDate(startDate: Dates.currentDate, dayF: -1 )
        let endDate = Dates.currentDate
        let idUser = mEventViewModel.getEmploye()?.userId ?? ""
        mEventViewModel.consultListEvents(startDate: startDate, endDate: endDate, idUser: idUser)
    }

    func getEventListPlanningService() {
        let numEmployee = mEventViewModel.getEmploye()?.numEmployee ?? ""

        mPlanningViewModel.ConsultEventsPlanning(numSupervisor: numEmployee )
    }

    func goRatingEvent() {
        //        AnalyticsUtils.quality_event()
        ViewEmbedder.embed(withIdentifier: "ListEventQualityView", parent: self, container: self.eventsContainerView) { _ in }
    }

    func goPlanningEvents(listaEventos: ListaEventos?) {
                let planning: ListEventPlanningView = UIStoryboard(name: "ListEventPlanningView", bundle: nil).instantiateViewController(withIdentifier: "ListEventPlanningView") as! ListEventPlanningView
                planning.listaEventos = listaEventos
                 ViewEmbedder.embed(
                     parent: self,
                     container: self.eventsContainerView,
                     child: planning,
                     previous: self.childViewControllers.first
                 )

    }

}

extension EventCalendarSupervisorView: EventObserver {
    func successDeleteHostess() {}
    func successEditEvent() {}

    func retryEvent(id: String) {
        if id == "eventHostess" {
            getEventCalendar()
        }
    }
    func pickerOptions(value: String, id: String) {
        weekTextfield.text = value
        self.positionPicker = mEventViewModel.getPositionWeekSelect(value: value)
        if self.positionPicker != self.eventCalendar.positionPicker {
            mEventViewModel.updateWeekDay(numberDay: mEventViewModel.numDays[self.positionPicker], week: value, positionPicker: self.positionPicker)
            self.getEventCalendar()
        }
    }

    func successEvents() {
        if isToQuality {
            if mEventViewModel.getEventSortDateQuality().count > 0 {
                 goRatingEvent()
            } else {
                goNoAssignedEvents()
            }
        } else {
            goAssignedEvents()
        }
    }

    func successDetailEvent() { }

    func successListHostes() { }

    func successAssignedHostes() { }

    func successCategory() { }

    func onError(errorMessage: String) {
        goNoAssignedEvents()
    }

}

extension EventCalendarSupervisorView: CLLocationManagerDelegate {
    func configLocation() {
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        stopLocation()
        mEventViewModel.updateLocation(latitude: locValue.latitude, longitude: locValue.longitude)
        print("locations = \(locValue.latitude) \(locValue.longitude)")

    }

    func stopLocation() {
        if CLLocationManager.locationServicesEnabled() {
            locationManager.stopUpdatingLocation()
        }
    }
}

extension EventCalendarSupervisorView: EventPlanningObserver {
    func successSearchEventPlanning() {
        let events = mPlanningViewModel.getEventsSortDate()
        let eventsFullDesign = mPlanningViewModel.completeMainDesign(listEvents: events!)
        goPlanningEvents(listaEventos: eventsFullDesign)
    }

    func successCategoryEvaluation() {

    }

    func successAddEvent() {

    }

    func successDeleteEvent() {

    }

    func successDataUser() {
        let user = mPlanningViewModel.getUserData()
        let nameFull = "\(String(user?.nombre ?? "")) \(String(user?.apellidoPaterno ?? "")) \(String(user?.apellidoMaterno ?? ""))"
         mEventViewModel.updateNameSupervisor(name: nameFull)
    }

}
