//
//  ListEventQualityView.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 12/11/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

class ListEventQualityView: BaseVentasView {
    @IBOutlet weak var EventTable: UITableView!
    var mEvaluationEventViewModel: EvaluationEventViewModel!
    var mEventViewModel: EventViewModel!
    var events: [Events] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        super.viewDidLoad()
        super.setView(view: self.view)
        mEvaluationEventViewModel = EvaluationEventViewModel(view: self)
        mEventViewModel = EventViewModel(view: self)
    }

    override func viewDidAppear(_ animated: Bool) {
       // viewModel.updateEventsQuality()
      //  events = viewModel.getEventSortDateQuality()
        events = mEventViewModel.getEventSortDateQuality()
        EventTable.reloadData()
    }

    func goRatingEventView() {
        let quality: QualityDetailEventView = UIStoryboard(name: "QualityDetailEventView", bundle: nil).instantiateViewController(withIdentifier: "QualityDetailEventView") as! QualityDetailEventView
        self.present(quality, animated: false, completion: nil)
    }

}

extension ListEventQualityView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventCell", for: indexPath) as! EventTableViewCell
        cell.barColorView.backgroundColor = mEventViewModel.getColor(position: events[indexPath.row].indexColor)
        cell.folioLabel.text = "Folio #" + (TPCipher.shared.decode(coded: events[indexPath.row].idEvent) ?? "-")
        cell.dateView.isHidden = events[indexPath.row].showDate
        cell.numberDayLabel.text = events[indexPath.row].numberDay
        cell.dayWeekLabel.text =  events[indexPath.row].nameDay
        cell.selectButton.tag = indexPath.row
        cell.idEvent = TPCipher.shared.decode(coded: events[indexPath.row].idEvent)
        cell.selectButton.addTarget(self, action: #selector(handleButtonTapped(sender: )), for: .touchUpInside)
        cell.nameEventLabel.text =  TPCipher.shared.decode(coded: events[indexPath.row].Title)
      cell.hourStartEndLabel.text =  mEventViewModel.hourEventFormat(hour: TPCipher.shared.decode(coded: events[indexPath.row].startHour)!) + " - " +   mEventViewModel.hourEventFormat(hour: TPCipher.shared.decode(coded: events[indexPath.row].endHour)!)
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }

    @objc func handleButtonTapped(sender: UIButton) {
        let selectedIndex = IndexPath(row: sender.tag, section: 0)
        EventTable.selectRow(at: selectedIndex, animated: true, scrollPosition: .none)
        mEventViewModel.updateIdEvent(idEvent: TPCipher.shared.decode(coded: events[sender.tag].idEvent)!, title: TPCipher.shared.decode(coded: events[sender.tag].Title)!)
         goRatingEventView()
    }
}
