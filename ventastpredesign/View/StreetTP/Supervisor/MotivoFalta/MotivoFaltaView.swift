//
//  MotivoFaltaView.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 06/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

class MotivoFaltaView: BaseVentasView {
    @IBOutlet weak var reasonView: UIView!
    @IBOutlet weak var hostessMissingTexfield: UITextField!
    @IBOutlet weak var dateMissingTextfield: UITextField!
    @IBOutlet weak var reasonMissingTexfield: UITextField!
    var reasons: [tree] = []
    var hostess: [team] = []
    var idHostessCurrent = ""
    var idReasonCurrent = ""
    var mEventViewModel: EventViewModel!
    var mCheckInViewModel: CheckInViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mEventViewModel = EventViewModel(view: self)
        mEventViewModel.atachView(observer: self)
        mCheckInViewModel = CheckInViewModel(view: self)
        mCheckInViewModel.atachView(observer: self)
        setUpView()
    }

    func setUpView() {
        mEventViewModel.ConsultListHostess()
        mEventViewModel.ConsultCategoryTree(idGeneric: "49")
    }

    @IBAction func cancelAction(_ sender: Any) {
        Constants.Back(viewC: self)
    }

    @IBAction func showCalendar(_ sender: Any) {
        let calendarView: CalendarView = UIStoryboard(name: "CalendarView", bundle: nil).instantiateViewController(withIdentifier: "CalendarView") as! CalendarView
        calendarView.getDateDelegate = self
        calendarView.maximum = DateCalendar.futureDate(date: Date(), value: 7, component: .day)
        calendarView.providesPresentationContextTransitionStyle = true
        calendarView.definesPresentationContext = true
        calendarView.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        calendarView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(calendarView, animated: false, completion: nil)
    }

    @IBAction func showHostessAction(_ sender: Any) {
        Constants.OptionCombo(origen: "hostess", array: hostess.map({ $0.name }), viewC: self)
    }

    @IBAction func showReasonAction(_ sender: Any) {
        Constants.OptionCombo(origen: "reason", array: reasons.map({ $0.descriptionTree}), viewC: self)
    }

    @IBAction func sendReasonMissingAction(_ sender: Any) {
        if validateTextfieldData() {
            sendReasonMissing()
        } else {
            Constants.Alert(title: "", body: "Favor de completar todos los campos", type: type.Warning, viewC: self)
        }
    }

    func sendReasonMissing() {
        mCheckInViewModel.sendReasonMissing(idMotivo: idReasonCurrent, idModifica: idHostessCurrent, fecha: dateMissingTextfield.text ?? "")
    }

    func validateTextfieldData() -> Bool {
        if (hostessMissingTexfield.text != "" && hostessMissingTexfield.text != nil ) && (reasonMissingTexfield.text != "" && reasonMissingTexfield.text != nil) &&
            (dateMissingTextfield.text != "" && dateMissingTextfield.text != nil) {
            return true
        }
        return false
    }
}

extension MotivoFaltaView: EventObserver {
    func successEditEvent() {}

    func successDeleteEvent() { }

    func successDeleteHostess() {}
    func retryEvent(id: String) {}

    func pickerOptions(value: String, id: String) {
        if id == "hostess" {
            let position = mEventViewModel.getPosition(array: hostess.map({ $0.name }), value: value)
            idHostessCurrent = hostess[position].id
            hostessMissingTexfield.text = value
        } else if id == "reason" {
            let position = mEventViewModel.getPosition(array: reasons.map({ $0.descriptionTree }), value: value)
            idReasonCurrent = reasons[position].id
            reasonMissingTexfield.text = value
        }
    }

    func successEvents() {}

    func successDetailEvent() { }

    func successListHostes() {
        hostess = mEventViewModel.getHostessDesencripte()
    }

    func successAssignedHostes() {}

    func successCategory() {
        reasons = mEventViewModel.getCategory()
    }

    func onError(errorMessage: String) {
        if errorMessage == "-1 ORA-01403: no data found" {
            Constants.Alert(title: "", body: "Ya existe un registro de asistencia del promotor", type: type.Error, viewC: self)
        } else {
            Constants.Alert(title: "", body: errorMessage, type: type.Error, viewC: self)
        }
    }
}

extension MotivoFaltaView: CheckInObserver {
    func successReasonMissing() {
        Constants.Back(viewC: self)
    }

    func successConsultCheck() {}

    func successUpdateCheck() {}
}

extension MotivoFaltaView: getDateDelegate {
    func getDate(date: String, id: String) {
        dateMissingTextfield.text = date
    }
}
