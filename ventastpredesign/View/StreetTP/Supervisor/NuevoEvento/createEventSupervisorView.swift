//
//  createEventSupervisorView.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 06/09/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

class createEventSupervisorView: BaseVentasView {

    @IBOutlet weak var activityTexfield: UITextField!
    @IBOutlet weak var campaignTexfield: UITextField!
    @IBOutlet weak var dateEvent: UITextField!
    @IBOutlet weak var hourStartTexfield: UITextField!
    @IBOutlet weak var hourFinishTexfield: UITextField!

    var mEventViewModel: EventViewModel!
    var mCreateEventViewModel: CreateEventViewModel!

    var arrayCampaign: [tree] = []
    var arrayActivity: [tree] = []
    var currentIdActivity = ""
    var currentIdCampaign = ""
    var isActivity = false

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mEventViewModel = EventViewModel(view: self)
        mEventViewModel.atachView(observer: self)
        mCreateEventViewModel = CreateEventViewModel(view: self)
        mCreateEventViewModel.atachView(observer: self)
        // mEventViewModel.clearCategory()
    }

    @IBAction func BackAction(_ sender: Any) {
        Constants.Back(viewC: self)
    }

    @IBAction func ActivityAction(_ sender: Any) {
        if  arrayActivity.count == 0 {
            isActivity = true
            consultActivity()
        } else {
            Constants.OptionCombo(origen: "activity", array: arrayActivity.map({ $0.descriptionTree }), viewC: self)
        }
    }

    @IBAction func campaignAction(_ sender: Any) {
        if  arrayCampaign.count == 0 {
            isActivity = false
            consultCampaign()
        } else {
            Constants.OptionCombo(origen: "campaign", array: arrayCampaign.map({ $0.descriptionTree }), viewC: self)
        }
    }

    @IBAction func showCalendar(_ sender: Any) {
        let calendarView: CalendarView = UIStoryboard(name: "CalendarView", bundle: nil).instantiateViewController(withIdentifier: "CalendarView") as! CalendarView
        calendarView.getDateDelegate = self
        calendarView.providesPresentationContextTransitionStyle = true
        calendarView.definesPresentationContext = true
        calendarView.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        calendarView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(calendarView, animated: false, completion: nil)
    }

    @IBAction func showMapCreateEvent(_ sender: Any) {
        if validateFullData() {
            saveDataNewEvent()
            goMapView()
        } else {
            Constants.Alert(title: "", body: "Por favor completa todos los campos", type: type.Warning, viewC: self)
        }
    }

    @IBAction func hourStartAction(_ sender: Any) {
        Constants.OptionComboDate(origen: "hourStart", viewC: self)
    }

    @IBAction func hourFinishAction(_ sender: Any) {
        Constants.OptionComboDate(origen: "hourFinish", viewC: self)
    }

    func consultActivity() {
        mEventViewModel.ConsultCategoryTree(idGeneric: "2")
    }

    func consultCampaign() {
        mEventViewModel.ConsultCategoryTree(idGeneric: "1")
    }

    func validateFullData() -> Bool {
        var isValidate = false
        if campaignTexfield.text != "" && campaignTexfield.text  != nil &&
            activityTexfield.text != "" && activityTexfield.text  != nil &&
            dateEvent.text != "" && dateEvent.text != nil &&
            hourStartTexfield.text != "" && hourStartTexfield.text != nil &&
            hourFinishTexfield.text != "" && hourFinishTexfield.text != nil {
            isValidate = true
        }
        return isValidate
    }

    func goMapView() {
        let map: LocationEventView = UIStoryboard(name: "LocationEventView", bundle: nil).instantiateViewController(withIdentifier: "LocationEventView") as! LocationEventView
        self.present(map, animated: false, completion: nil)
    }

    func saveDataNewEvent() {

        mCreateEventViewModel.createPlanningEvent(nameCampaing: campaignTexfield.text!, idCampaing: currentIdCampaign, nameActivity: activityTexfield.text!, idActivity: currentIdActivity, dateEvent: dateEvent.text!, hourFinish: hourFinishTexfield.text!, hourStart: hourStartTexfield.text!)
    }

}

extension createEventSupervisorView: getDateDelegate {
    func getDate(date: String, id: String) {
        dateEvent.text = date
    }
}

extension createEventSupervisorView: EventObserver {
    func successEditEvent() {}

    func successDeleteEvent() {}

    func successDeleteHostess() {}

    func retryEvent(id: String) { }

    func pickerOptions(value: String, id: String) {
        if id == "activity" {
            let position = mEventViewModel.getPosition(array: arrayActivity.map({ $0.descriptionTree }), value: value)
            activityTexfield.text = value
            currentIdActivity = arrayActivity[position].id
        } else if id == "campaign" {
            let position = mEventViewModel.getPosition(array: arrayCampaign.map({ $0.descriptionTree }), value: value)
            campaignTexfield.text = value
            currentIdCampaign = arrayCampaign[position].id
        } else if id == "hourStart" {
            hourStartTexfield.text = value
        } else if id == "hourFinish" {
            hourFinishTexfield.text = value
        }
    }

    func successEvents() { }

    func successDetailEvent() { }

    func successListHostes() {}

    func successAssignedHostes() {}

    func successCategory() {
        if isActivity {
            arrayActivity  = mEventViewModel.getCategory()
            Constants.OptionCombo(origen: "activity", array: arrayActivity.map({ $0.descriptionTree }), viewC: self)
        } else {
            arrayCampaign  = mEventViewModel.getCategory()
            Constants.OptionCombo(origen: "campaign", array: arrayCampaign.map({ $0.descriptionTree }), viewC: self)
        }
    }

    func onError(errorMessage: String) {
        Constants.Alert(title: "", body: errorMessage, type: type.Error, viewC: self)
    }

}

extension createEventSupervisorView: CreateEventSuccessObserver {
    func successCreateEvent() {}

}
