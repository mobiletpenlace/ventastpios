//
//  NewEventDetailView.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 06/09/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import MapKit

class NewEventDetailView: BaseVentasView {

    @IBOutlet weak var titleOfView: UILabel!
    @IBOutlet weak var subtitleOfView: UILabel!
    @IBOutlet weak var dateEventLabel: UILabel!
    @IBOutlet weak var locationEventLabel: UILabel!
    @IBOutlet weak var nameCampaignLabel: UILabel!
    @IBOutlet weak var nameActivityLabel: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var sendButton: UIButton!
    var pointAnnotation: MKPointAnnotation!
    var pinAnnotationView: MKPinAnnotationView!
    var mCreateEventViewModel: CreateEventViewModel!
    var mEventPlanningViewModel: EventPlanningViewModel!
    var event: planningEvent?
    var listaEventos: ListaEventos?
    var isCreatingAnEvent = true
    var isConsultEventsPlanning = false
    var idSupervisor: String = ""
    var nameSupervisor: String = ""
    var numEmployeeSupervisor: String = ""

    var nameCampaign = ""
    var nameActivity = ""
    var dateEvent = ""
    var hourStart = ""
    var hourEnd = ""
    var currentLatitud = ""
    var currentLongitud = ""
    var currentLocation = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mCreateEventViewModel = CreateEventViewModel(view: self)
        mCreateEventViewModel.atachView(observer: self)
        mEventPlanningViewModel = EventPlanningViewModel(view: self)
        mEventPlanningViewModel.atachView(observer: self)
        setUpView()
        // Do any additional setup after loading the view.
    }

    @IBAction func BackAction(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }

    @IBAction func sendEvent(_ sender: Any) {
        isConsultEventsPlanning = true
        mEventPlanningViewModel.ConsultEventsPlanning(numSupervisor: numEmployeeSupervisor)
    }

    func setUpView() {
            if isCreatingAnEvent {
                // titleOfView.text = "Alta de Evento"
                subtitleOfView.text = "Confirma los datos del evento"
                sendButton.isHidden = false
                sendButton.isEnabled = true
                fullData()

            } else {
              //  titleOfView.text = "Evento"
                subtitleOfView.text = "Datos del evento"
                sendButton.isHidden = true
                sendButton.isEnabled = false
                showDetailEvent()
            }
    }

    func showDetailEvent() {
        nameCampaignLabel.text = nameCampaign
        nameActivityLabel.text = nameActivity
        locationEventLabel.text = currentLocation
        dateEventLabel.text = mCreateEventViewModel.getDateEvent(date: dateEvent, hourStart: hourStart, hourFinish: hourEnd)

        let latitud = Double(currentLatitud)
        let longitud = Double(currentLongitud)
        let coordinateTemp: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: latitud!, longitude: longitud!)

        fullMapView(coordinates: coordinateTemp)
    }

    func fullData() {
        event = mCreateEventViewModel.getPlanningEvent()

        print(event!)

        if let detailEvent = event {

            nameCampaignLabel.text = detailEvent.nameCampaign
            nameActivityLabel.text = detailEvent.nameActivity
            locationEventLabel.text = detailEvent.nameLocation
            dateEventLabel.text = mCreateEventViewModel.getDateEvent(date: detailEvent.dateEvent, hourStart: detailEvent.hourStart, hourFinish: detailEvent.hourFinish)

            nameSupervisor = mCreateEventViewModel.getEmployee().nameEmployee
            idSupervisor = mCreateEventViewModel.getEmployee().userId
            numEmployeeSupervisor = mCreateEventViewModel.getEmployee().numEmployee

            let latitud = Double(detailEvent.latitud)
            let longitud = Double(detailEvent.longitud)
            let coordinateTemp: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: latitud!, longitude: longitud!)

            fullMapView(coordinates: coordinateTemp)
        }

    }

    func fullMapView(coordinates: CLLocationCoordinate2D) {
        mapView.isZoomEnabled = true
        mapView.isPitchEnabled = false
        mapView.isScrollEnabled = false
        mapView.isRotateEnabled = false
        removeAnnotations()
        addAnotation(coordinate: coordinates)
        addRegion(coordinate: coordinates)
    }

    func addAnotation(coordinate: CLLocationCoordinate2D, title: String = "") {
        self.pointAnnotation = MKPointAnnotation()
        self.pointAnnotation.coordinate = coordinate
        self.pointAnnotation.title = title
        self.pinAnnotationView = MKPinAnnotationView(annotation: self.pointAnnotation, reuseIdentifier: nil)
        self.mapView.centerCoordinate = self.pointAnnotation.coordinate
        self.mapView.addAnnotation(self.pinAnnotationView.annotation!)
    }

    func addRegion(coordinate: CLLocationCoordinate2D) {
        let region = MKCoordinateRegionMakeWithDistance(coordinate, 500, 500)
        self.mapView.setRegion(region, animated: true)
    }

    func removeAnnotations() {
        if mapView.annotations.count > 0 {
            mapView.removeAnnotations(mapView.annotations)
        }

    }

    func consultSaveEvent() {
        isConsultEventsPlanning = false
        mEventPlanningViewModel.AddEventsPlanning(listaEvento: listaEventos, numSupervisor: numEmployeeSupervisor, idSupervisor: idSupervisor, nameSupervisor: nameSupervisor, idCampaign: event?.idCampaign ?? "", nameCampaign: event?.nameCampaign ?? "", idActivity: event?.idActivity ?? "", nameActivity: event?.nameActivity ?? "", date: event?.dateEvent ?? "", hourStart: event?.hourStart ?? "", hourFinish: event?.hourFinish ?? "", description: locationEventLabel.text ?? "", longitude: event?.longitud ?? "", latitude: event?.latitud ?? "")
    }

}

extension NewEventDetailView: CreateEventSuccessObserver {
    func successCreateEvent() {

    }

    func onError(errorMessage: String) {
        if errorMessage == "No se encontraron eventos en planificación" {
            successSearchEventPlanning()
        } else {
            Constants.Alert(title: "", body: errorMessage, type: type.Error, viewC: self)
        }
    }

}

extension NewEventDetailView: EventPlanningObserver {
    func successSearchEventPlanning() {
        listaEventos = mEventPlanningViewModel.getEvents()
        consultSaveEvent()
    }

    func successCategoryEvaluation() {
    }

    func successAddEvent() {
        Constants.Alert(title: "", body: "El evento se guardo exitosamente", type: type.Success, viewC: self, dimissParent: true, dismissDouble: true)
        print("se guardo el evento ")
    }

    func successDeleteEvent() {

    }

    func successDataUser() {

    }

}
