//
//  QualityQuestionView.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 18/11/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import Cosmos

protocol getUpdateDelegate {
    func getUpdate(update: Bool)
}
class QualityQuestionView: BaseVentasView {
    @IBOutlet weak var titleAlert: UILabel!
    @IBOutlet weak var question: UILabel!
    @IBOutlet weak var startEvaluation: CosmosView!
    @IBOutlet weak var viewEvaluation: UIView!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var commentaryTextfield: UITextField!
    var mEvaluationViewModel: EvaluationEventViewModel!
    var mPlanningViewModel: EventPlanningViewModel!
    var mEventViewModel: EventViewModel!
    var getUpdateDelegate: getUpdateDelegate?
    var pointCurrent = 0
    var item  = ["2", "3", "4"]
    var comentary  = ["", "", ""]
    var value  = ["0", "0", "0"]
    var idModifica = ""
    var nameEmployee = ""
    var optionsEvaluation: [OptionsEvaluation] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mEvaluationViewModel = EvaluationEventViewModel(view: self)
        mEvaluationViewModel.atachView(observer: self)
        mPlanningViewModel = EventPlanningViewModel(view: self)
        mPlanningViewModel.atachView(observer: self)
        mEventViewModel = EventViewModel(view: self)
        mEventViewModel.atachView(observer: self)
        consultOptionsEvaluation()
        titleAlert.text = "Evaluación de \(nameEmployee)"
        questionTitle()
        settingsStart() // Revisar
    }

    @IBAction func CommentaryAction(_ sender: Any) {
        if startEvaluation.rating != 0 && startEvaluation.rating != nil {
            showOptionsEvaluation()
        } else {
            Constants.Alert(title: "", body: "Favor de seleccionar las estrellas para poder mostrar las opciones de los comentarios", type: type.Warning, viewC: self)
        }
    }

    @IBAction func SendEvaluationEmployeAction(_ sender: Any) {
        if commentaryTextfield.text != "" && startEvaluation.rating != 0 && startEvaluation.rating != nil {
            comentary[pointCurrent] = commentaryTextfield.text!
            value[pointCurrent] = String(startEvaluation.rating)
            if pointCurrent == 2 {
                mEvaluationViewModel.sendEmployeeEvaluation(item: item, value: value, comentary: comentary, idModifica: idModifica)
            } else {
                pointCurrent = pointCurrent + 1
                questionTitle()
            }
        } else {
            Constants.Alert(title: "", body: "Favor de completar todos los campos", type: type.Warning, viewC: self)
        }
    }

    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }

    func settingsStart() {
        startEvaluation.didFinishTouchingCosmos = { _ in
            self.commentaryTextfield.text = ""
        }
    }

    func consultOptionsEvaluation() {
        mPlanningViewModel.ConsultListEvaluacion()
    }

    func questionTitle() {
        switch pointCurrent {
        case 0:
            question.text = "¿La presentación del promotor es la adecuada?"
            continueButton.setTitle("Continuar", for: .normal)
            startEvaluation.rating = 0
            commentaryTextfield.text = ""
        case 1:
            question.text = "¿Porta adecuadamente el uniforme?"
            continueButton.setTitle("Continuar", for: .normal)
            startEvaluation.rating = 0
            commentaryTextfield.text = ""
        case 2:
            question.text = "¿Cómo fue la calidad del servicio?"
            continueButton.setTitle("Enviar", for: .normal)
            startEvaluation.rating = 0
            commentaryTextfield.text = ""
        default:
            break
        }
    }

    func showOptionsEvaluation() {

        switch startEvaluation.rating {
        case 1:
            Constants.OptionCombo(origen: "oneStart", array: optionsEvaluation[pointCurrent].oneStar ?? [], viewC: self)
        case 2:
            Constants.OptionCombo(origen: "oneStart", array: optionsEvaluation[pointCurrent].twoStar ?? [], viewC: self)
        case 3:
            Constants.OptionCombo(origen: "oneStart", array: optionsEvaluation[pointCurrent].threeStar ?? [], viewC: self)
        case 4:
            Constants.OptionCombo(origen: "oneStart", array: optionsEvaluation[pointCurrent].fourStar ?? [], viewC: self)
        case 5:
            Constants.OptionCombo(origen: "oneStart", array: optionsEvaluation[pointCurrent].fiveStar ?? [], viewC: self)
        default:
            break
        }

    }

    func getQueryEvaluationEvent() {
        guard let employee = mEvaluationViewModel.getEmploye() else {return}
        mEvaluationViewModel.getQueryEvaluation(idEvent: employee.currentIdEvent)
    }

}

extension QualityQuestionView: EvaluationObserver {

    func successEvaluationEmploye() {
        getQueryEvaluationEvent()
    }

    func successEvaluationEvent() {}

    func successEvaluation() {
        getUpdateDelegate?.getUpdate(update: true)
        dismiss(animated: false, completion: nil)
    }

}
extension QualityQuestionView: EventPlanningObserver {
    func successSearchEventPlanning() {}

    func successCategoryEvaluation() {
        optionsEvaluation = mPlanningViewModel.getEvaluacionOptions() ?? []
    }

    func successAddEvent() {}

    func successDeleteEvent() {}

    func successDataUser() {}

    func onError(errorMessage: String) {
        Constants.Alert(title: "", body: errorMessage, type: type.Error, viewC: self)
    }

}

extension QualityQuestionView: EventObserver {
    func successEditEvent() {}

    func successDeleteHostess() {}

    func pickerOptions(value: String, id: String) {
      //  commentaryTextfield.text = value
        if id == "oneStart" {
            commentaryTextfield.text = value
        }/*else if id == "twoStart"{
            
        }else if id == "threeStar"{
            
        }else if id == "fourStar"{
            
        }else if id == "fiveStar"{
            
        }*/
    }

    func retryEvent(id: String) {}
    func successEvents() {}
    func successDetailEvent() {}
    func successListHostes() {}
    func successAssignedHostes() {}
    func successCategory() {}
}
