//
//  QualityDetailEventView.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 06/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import Cosmos

class QualityDetailEventView: BaseVentasView, getUpdateDelegate {

    @IBOutlet weak var startEvaluationEvent: CosmosView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var titleEvent: UILabel!
    @IBOutlet weak var dateEvent: UILabel!
    @IBOutlet weak var locationEvent: UILabel!
    @IBOutlet weak var sendEventButton: UIButton!
    @IBOutlet weak var tableHostessEvaluation: UITableView!
    var hostess: [hostessDetails] = []
    var valuationEvent: [ValuationEvent] = []
    var valuationEmployee: [ValuationEmployee] = []
    var mEventViewModel: EventViewModel!
    var mEvaluationViewModel: EvaluationEventViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mEventViewModel = EventViewModel(view: self)
        mEvaluationViewModel = EvaluationEventViewModel(view: self)
        mEvaluationViewModel.atachView(observer: self)
        mEventViewModel.atachView(observer: self)
        getQueryEvaluationEvent(idEvent: mEvaluationViewModel.getEmploye()?.currentIdEvent ?? "")
    }

    @IBAction func sendEventEvaluationAction(_ sender: Any) {
        if textView.text != "" {
            if startEvaluationEvent.rating == nil || startEvaluationEvent.rating == 0.0 {
                Constants.Alert(title: "", body: "Favor de completar el nivel de aceptación del evento", type: type.Warning, viewC: self)
            } else {
                mEvaluationViewModel.sendEventEvaluation(value: String(startEvaluationEvent.rating), commentary: textView.text)
            }
        } else {
            Constants.Alert(title: "", body: "Favor de completar el campo de comentario", type: type.Warning, viewC: self)

        }
    }

    @IBAction func BackAction(_ sender: Any) {
        self.dismiss(animated: false)
    }

    func getQueryEvaluationEvent(idEvent: String) {
        mEvaluationViewModel.getQueryEvaluation(idEvent: idEvent)
    }

    func getDetailsEvent() {
        TPCipher.shared.setKey(b64Key: Strings.SHA256_KEY)
        guard let employee = mEventViewModel.getEmploye() else {return}
        titleEvent.text =  employee.currentTitleEvent
        mEventViewModel.consultDetailEvent(idEvent: employee.currentIdEvent)
    }

    func setDecriptionEvent() {
        TPCipher.shared.setKey(b64Key: Strings.SHA256_KEY)
        guard let response = mEventViewModel.getDetailEvent() else {return}
        dateEvent.text = mEventViewModel.eventSchedule(startDate: response.dateStar, hourStart: response.startHour, hourEnd: response.endHour)
        locationEvent.text = "Ubicación"
        if  response.descriptionEvent != "" {
            if TPCipher.shared.decode(coded: response.descriptionEvent) != "" {
                locationEvent.text = TPCipher.shared.decode(coded: response.descriptionEvent)
            }
        }
    }

    func eventEvaluation() {
        if valuationEvent.count > 0 {
            TPCipher.shared.setKey(b64Key: Strings.SHA256_KEY)
            if valuationEvent[0].Commentary != "" && valuationEvent[0].Value != "" {
                var ratingEvent = "0.0"
                textView.text = TPCipher.shared.decode(coded: valuationEvent[0].Commentary)
                ratingEvent = TPCipher.shared.decode(coded: valuationEvent[0].Value) ?? "0.0"
                // var rating = 1
                guard let newvalue = Double(ratingEvent) else { return }
                startEvaluationEvent.rating = newvalue
                textView.isEditable = false
                startEvaluationEvent.isUserInteractionEnabled = false
                sendEventButton.isEnabled = false
                sendEventButton.backgroundColor = UIColor.lightGray
            } else {
                textView.isEditable = true
                startEvaluationEvent.isUserInteractionEnabled = true
                sendEventButton.isEnabled = true
                sendEventButton.backgroundColor = Colors.PRIMARY
            }
        } else {
            textView.isEditable = true
            startEvaluationEvent.isUserInteractionEnabled = true
            sendEventButton.isEnabled = true
            sendEventButton.backgroundColor = Colors.PRIMARY
        }
    }

    func consultValueEmployee(idHostess: String) -> (String, UIImage?, Bool) {
        var result = "0.0"
        var startImage = UIImage(named: "icon_star_gray.png")
        var isEditable = true
        var sumValue = 0.0
        var sumElement = 0.0
        for index in valuationEmployee {
            if index.IdEmployee == idHostess {
                var ratingEvent = "0.0"
                ratingEvent = TPCipher.shared.decode(coded: index.Value) ?? "0.0"
                if let newvalue = Double(ratingEvent) {
                    sumValue = sumValue + newvalue
                    sumElement = sumElement + 1
                }

            }
        }
        if sumValue > 0 {
            let prom = sumValue / sumElement
            result = String(format: "%.1f", prom)
            startImage = UIImage(named: "icon_star_green.png")
            isEditable = false
        } else {
            result = "0.0"
            startImage = UIImage(named: "icon_star_gray.png")
            isEditable = true
        }
        return ( result, startImage, isEditable)
    }

    func getUpdate(update: Bool) {
        if update {
            assignedHostess()
            valuationEvent = mEvaluationViewModel.getValuationEvent()
            valuationEmployee = mEvaluationViewModel.getValuationEmployee()

        }
    }

    func assignedHostess() {
        hostess = mEventViewModel.getHostessEvent()
        tableHostessEvaluation.reloadData()
    }
}

extension QualityDetailEventView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        hostess.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HostesRatingCell", for: indexPath) as! HostesRatingCell
        cell.selectButton.tag = indexPath.row
        cell.selectButton.addTarget(self, action: #selector(handleButtonTapped(sender: )), for: .touchUpInside)
        cell.name.text = TPCipher.shared.decode(coded: hostess[indexPath.row].name)
        cell.rating.text = consultValueEmployee(idHostess: hostess[indexPath.row].id).0
        cell.starImage.image = consultValueEmployee(idHostess: hostess[indexPath.row].id).1
        cell.selectButton.isEnabled = consultValueEmployee(idHostess: hostess[indexPath.row].id).2
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }

    @objc func handleButtonTapped(sender: UIButton) {
        let rating: QualityQuestionView = UIStoryboard(name: "QualityQuestionView", bundle: nil).instantiateViewController(withIdentifier: "QualityQuestionView") as! QualityQuestionView
        rating.getUpdateDelegate = self
        rating.idModifica = TPCipher.shared.decode(coded: hostess[sender.tag].id)!
        rating.nameEmployee = TPCipher.shared.decode(coded: hostess[sender.tag].name)!
        rating.providesPresentationContextTransitionStyle = true
        rating.definesPresentationContext = true
        rating.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        rating.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(rating, animated: false, completion: nil)
    }
}

extension QualityDetailEventView: EventObserver {
    func successEditEvent() {}

    func successDeleteEvent() { }

    func successDeleteHostess() {}

    func retryEvent(id: String) {}

    func pickerOptions(value: String, id: String) {}

    func successEvents() {}

    func successDetailEvent() {
        eventEvaluation()
        setDecriptionEvent()
        if mEventViewModel.getHostessEvent().count > 0 {
            assignedHostess()
        }
    }

    func successListHostes() { }

    func successAssignedHostes() { }

    func successCategory() {}

    func onError(errorMessage: String) {
        if errorMessage != "Resultados No Encontrados." {
            Constants.Alert(title: "", body: errorMessage, type: type.Error, viewC: self)
        }

    }
}

extension QualityDetailEventView: EvaluationObserver {
    func successEvaluationEmploye() { }

    func successEvaluationEvent() {
        getQueryEvaluationEvent(idEvent: mEvaluationViewModel.getEmploye()?.currentIdEvent ?? "")
    }

    func successEvaluation() {
        valuationEvent = mEvaluationViewModel.getValuationEvent()
        valuationEmployee = mEvaluationViewModel.getValuationEmployee()
        getDetailsEvent()

    }
}
