//
//  DialogDontHaveHostessView.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 05/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

class DialogDontHaveHostessView: UIViewController {
    @IBOutlet weak var hostessView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        hostessView.isUserInteractionEnabled = true
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first as! UITouch
        if touch.view != hostessView {
            // performSegueToReturnBack()
            self.dismiss(animated: false, completion: nil)
        }
    }

}
