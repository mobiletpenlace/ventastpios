//
//  DialogNoHostessAssignedView.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 05/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class DialogNoHostessAssignedView: BaseVentasView {
    var mEventViewModel: EventViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mEventViewModel = EventViewModel(view: self)
        mEventViewModel.atachView(observer: self)
    }

    @IBAction func assignedAction(_ sender: Any) {
        mEventViewModel.ConsultListHostess()
    }

    func goDialogListAssinged() {
        let viewAlertVC: DialogSelectHostess = UIStoryboard(name: "DialogSelectHostess", bundle: nil).instantiateViewController(withIdentifier: "DialogSelectHostess") as! DialogSelectHostess
        viewAlertVC.providesPresentationContextTransitionStyle = true
        viewAlertVC.definesPresentationContext = true
        viewAlertVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        viewAlertVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(viewAlertVC, animated: false, completion: nil)
    }

}

extension DialogNoHostessAssignedView: EventObserver {
    func successEditEvent() {}

    func successDeleteEvent() { }

    func successDeleteHostess() {}
    func retryEvent(id: String) {}

    func pickerOptions(value: String, id: String) {}

    func successEvents() { }

    func successDetailEvent() {}

    func successListHostes() {
        goDialogListAssinged()
    }

    func successAssignedHostes() {}

    func successCategory() {}

    func onError(errorMessage: String) {
        Constants.Alert(title: "", body: errorMessage, type: type.Error, viewC: self)
    }

}
