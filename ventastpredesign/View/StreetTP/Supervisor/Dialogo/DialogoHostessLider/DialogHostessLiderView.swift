//
//  DialogHostessLiderView.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 05/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

class DialogHostessLiderView: BaseVentasView {
    @IBOutlet weak var hostessTable: UITableView!
    var mEventViewModel: EventViewModel!
    var hostess: [hostessDetails] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mEventViewModel = EventViewModel(view: self)
        hostessTable.isScrollEnabled = false
    }

    override func viewDidAppear(_ animated: Bool) {
        hostess = mEventViewModel.getHostessEvent()
        hostessTable.reloadData()
    }
}

extension DialogHostessLiderView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hostess.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HostessAssignedTableViewCell", for: indexPath) as! HostessAssignedTableViewCell
       TPCipher.shared.setKey(b64Key: Strings.SHA256_KEY)
      cell.name.text = TPCipher.shared.decode(coded: hostess[indexPath.row].name)
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
          return 25
      }

}
