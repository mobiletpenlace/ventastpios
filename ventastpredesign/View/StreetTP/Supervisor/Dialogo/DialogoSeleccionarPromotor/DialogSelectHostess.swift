//
//  DialogSelectHostess.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 06/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class DialogSelectHostess: BaseVentasView {
    @IBOutlet weak var hostessViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var hostessView: UIView!
    @IBOutlet weak var hostessTable: UITableView!
    var mEventViewModel: EventViewModel!
    var hostess: [team] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mEventViewModel = EventViewModel(view: self)
        mEventViewModel.atachView(observer: self)
    }

    override func viewDidAppear(_ animated: Bool) {
        hostess = mEventViewModel.getHostessActiveEvent()
        designView(num: hostess.count)
        hostessTable.reloadData()
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first as! UITouch
        if touch.view != hostessView {
            self.dismiss(animated: false, completion: nil)
        }
    }

    @IBAction func AssignedAction(_ sender: Any) {
       let hostess =  mEventViewModel.getHostessActiveEvent()
        let isSelect = hostess.map { $0.isSelect }
        if isSelect.contains("1") {
            setHostessEvent()
        } else {
            Constants.Alert(title: "", body: "Debes seleccionar por lo menos una opción", type: type.Warning, viewC: self)
        }
    }

    func designView(num: Int) {
        hostessViewTopConstraint.constant = num > 5 ? 260 : (CGFloat(125 + (40 * num)))
    }

    func setHostessEvent() {
        mEventViewModel.AddAssignedHostess()
    }

    func radioButton(isSelect: String, button: UIButton) {
        let checkedImage = UIImage(named: "icon_radiobutton_on")! as UIImage
        let uncheckedImage = UIImage(named: "icon_radiobutton_off")! as UIImage
        if isSelect == "0" {
            button.setImage(uncheckedImage, for: UIControl.State.normal)
        } else {
            button.setImage(checkedImage, for: UIControl.State.normal)
        }
    }
}

extension DialogSelectHostess: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hostess.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectOptionViewCell", for: indexPath) as! SelectOptionViewCell
            radioButton(isSelect: hostess[indexPath.row].isSelect, button: cell.selectButton)
            cell.name.text =  TPCipher.shared.decode(coded: hostess[indexPath.row].name)
            cell.selectButton.tag = indexPath.row
            cell.selectButton.addTarget(self, action: #selector(handleButtonTapped(sender: )), for: .touchUpInside)

        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }

    @objc func handleButtonTapped(sender: UIButton) {
        if hostess[sender.tag].isSelect == "0" {
            mEventViewModel.updateIsSelectHostess(id: hostess[sender.tag].id, isSelect: "1")
        } else {
            mEventViewModel.updateIsSelectHostess(id: hostess[sender.tag].id, isSelect: "0")
        }
         hostessTable.reloadData()
    }
}

extension DialogSelectHostess: EventObserver {
    func successEditEvent() {}

    func successDeleteEvent() {}

    func successDeleteHostess() {}

    func retryEvent(id: String) {}

    func pickerOptions(value: String, id: String) {}

    func successEvents() {}

    func successDetailEvent() { }

    func successListHostes() {}

    func successAssignedHostes() {
        SwiftEventBus.post("RetryEventDetail")
        dismiss(animated: false, completion: nil)
    }

    func successCategory() { }

    func onError(errorMessage: String) {
        Constants.Alert(title: "", body: errorMessage, type: type.Error, viewC: self)
    }

}
