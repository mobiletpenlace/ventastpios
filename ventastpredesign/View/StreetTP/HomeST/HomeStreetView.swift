//
//  HomeStreetView.swift
//  ventastp
//
//  Created by mhuertao on 11/01/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import UIKit
import CoreLocation
import Amplitude

class HomeStreetView: BaseVentasView {
    @IBOutlet weak var mNameLabel: UILabel!
    @IBOutlet weak var missingView: UIView!
    @IBOutlet weak var mProfileLabel: UILabel!

    @IBOutlet weak var mAddEventImage: UIImageView!
    @IBOutlet weak var mAddEventLabel: UILabel!
    @IBOutlet weak var mAddEventButton: UIButton!

    @IBOutlet weak var mEventsImage: UIImageView!
    @IBOutlet weak var mEventsLabel: UILabel!
    @IBOutlet weak var mEventsButton: UIButton!

    @IBOutlet weak var mPlanningEventImage: UIImageView!
    @IBOutlet weak var mPlanningEventLabel: UILabel!
    @IBOutlet weak var mPlanningEventButton: UIButton!

    @IBOutlet weak var mMissingReasonImage: UIImageView!
    @IBOutlet weak var mMissingReasonLabel: UILabel!
    @IBOutlet weak var mMissingReasonButton: UIButton!

    var MenuVC: MenuViewController!
    var mEventViewModel: EventViewModel!
    var employed: EmployeeStreet?
    var currentProfile: String = ""
    let locationManager = CLLocationManager()
    var nameProfile = ["Coordinador", "Supervisor", "Promotor"]

    override func viewDidLoad() {
        super.viewDidLoad()
        mEventViewModel = EventViewModel(view: self)
        setUpView()
    }

    override func viewDidAppear(_ animated: Bool) {
        Amplitude.instance().logEvent("Show_HomeScreen")
    }

    @IBAction func MenuAction(_ sender: Any) {

        let viewAlert: UIStoryboard = UIStoryboard(name: "LoginVC", bundle: nil)
        let viewAlertVC = viewAlert.instantiateViewController(withIdentifier: "LoginVC")as! LoginVC
        self.present(viewAlertVC, animated: false, completion: {})
        /*if AppDelegate.menu_bool{
            ShowMenu()
        }else{
            CloseMenu()
        }*/
    }

    func setUpView() {
        getEmploye()
        validateFirebase()
        configLocation()
        // setMenu()
        validateTypeProfile()
    }

    func validateFirebase() {
        mEventViewModel.authUser(customToken: mEventViewModel.getEmploye()?.tokenFirebase ?? "")
        if  mEventViewModel.getEmploye()?.connectFirebase == "0" {
            Constants.Alert(title: "", body: "No se pudo conectar a la base de datos de Firebase", type: type.Warning, viewC: self)
        }
    }

    func getEmploye() {
        guard let employee = mEventViewModel.getEmploye() else {return}
        employed = employee
        currentProfile = employed?.profileUser ?? "0"
        mProfileLabel.text =  " \(nameProfile[(Int(currentProfile) ?? 0) - 1]) | \( employed?.numEmployee ?? "") "

    }

    func validateTypeProfile() {
        switch currentProfile {
        case "3":
            missingView.isHidden = true
            mAddEventImage.image = UIImage(named: "new_lead.png")
            mAddEventLabel.text = "Nuevo lead"
            mAddEventButton.addTarget(self, action: #selector(HostesNewLeadAction), for: .touchUpInside)
            mEventsImage.image = UIImage(named: "eventos.png")
            mEventsLabel.text = "Consulta de eventos"
            mEventsButton.addTarget(self, action: #selector(HostesConsultEventAction), for: .touchUpInside)
            mPlanningEventImage.image = UIImage(named: "estatus_lead.png")
            mPlanningEventLabel.text = "Estatus Lead"
            mPlanningEventButton.addTarget(self, action: #selector(HostesEstatusLeadAction), for: .touchUpInside)
        case "2":
            missingView.isHidden = false
            mMissingReasonButton.addTarget(self, action: #selector(SupervMissingAction), for: .touchUpInside)
            mAddEventImage.image = UIImage(named: "estatus_lead.png")
            mAddEventLabel.text = "Estatus lead"
            mAddEventButton.addTarget(self, action: #selector(SupervEstatusLeadAction), for: .touchUpInside)
            mEventsImage.image = UIImage(named: "eventos.png")
            mEventsLabel.text = "Consulta de eventos"
            mEventsButton.addTarget(self, action: #selector(SupervConsultEventAction), for: .touchUpInside)
            mPlanningEventImage.image = UIImage(named: "agregar_evento.png")
            mPlanningEventLabel.text = "Crear evento"
            mPlanningEventButton.addTarget(self, action: #selector(SupervAddEventAction), for: .touchUpInside)

        case "1":
            missingView.isHidden = true
            mAddEventImage.image = UIImage(named: "agregar_evento.png")
            mAddEventLabel.text = "Alta de evento"
            mAddEventButton.addTarget(self, action: #selector(CoordiAddEventAction), for: .touchUpInside)
            mEventsImage.image = UIImage(named: "eventos.png")
            mEventsLabel.text = "Consulta de eventos"
            mEventsButton.addTarget(self, action: #selector(CoordiConsultEventAction), for: .touchUpInside)
            mPlanningEventImage.image = UIImage(named: "planeacion.png")
            mPlanningEventLabel.text = "Planeación"
            mPlanningEventButton.addTarget(self, action: #selector(CoordiPlanningAction), for: .touchUpInside)

        default:
            break
        }
    }

    @objc func HostesNewLeadAction() {
        Constants.LoadStoryBoard(name: storyBoard.Street.createLead, viewC: self)
    }

    @objc func HostesConsultEventAction() {
        Constants.LoadStoryBoard(name: storyBoard.Street.homePromotor, viewC: self)
    }

    @objc func HostesEstatusLeadAction() {
        Constants.LoadStoryBoard(name: storyBoard.Street.statusleadPromotor, viewC: self)
    }

    @objc func SupervEstatusLeadAction() {
        Constants.LoadStoryBoard(name: storyBoard.Street.statusLeadSuper, viewC: self)
    }

    @objc func SupervConsultEventAction() {
        Constants.LoadStoryBoard(name: storyBoard.Street.homeSupervisor, viewC: self)
    }

    @objc func SupervAddEventAction() {
        Constants.LoadStoryBoard(name: storyBoard.Street.createEventSuper, viewC: self)
    }

    @objc func SupervMissingAction() {
        Constants.LoadStoryBoardBoot(storyboard: storyBoard.Street.reasonMissing, identifier: storyBoard.Street.reasonMissing, viewC: self)
    }

    @objc func CoordiConsultEventAction() {
        Constants.LoadStoryBoard(name: storyBoard.Street.consultEventCoord, viewC: self)
    }

    @objc func CoordiAddEventAction() {
        Constants.LoadStoryBoard(name: storyBoard.Street.newEventCoord, viewC: self)
    }

    @objc func CoordiPlanningAction() {
        Constants.LoadStoryBoard(name: storyBoard.Street.planningCoord, viewC: self)
    }

    func setMenu() {
        MenuVC = UIStoryboard(name: "MenuViewController", bundle: nil).instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        MenuVC.currentProfile = "street"
        let SwipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToGesture))
        SwipeRight.direction = UISwipeGestureRecognizerDirection.right
        let SwipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToGesture))
        SwipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(SwipeRight)
        self.view.addGestureRecognizer(SwipeLeft)
        self.MenuVC.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height )
        MenuVC.mNombreLabel.text = mProfileLabel.text

    }

    @objc func respondToGesture(gesture: UISwipeGestureRecognizer) {
        switch gesture.direction {
        case UISwipeGestureRecognizerDirection.right:
            ShowMenu()
        case UISwipeGestureRecognizerDirection.left:
            CloseMenu()
        default:
            break
        }
    }

    func ShowMenu() {
        UIView.animate(withDuration: 0.5) {() -> Void in
            self.MenuVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height )
            self.MenuVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.0)
            self.addChildViewController(self.MenuVC)
            self.view.addSubview(self.MenuVC.view)
            AppDelegate.menu_bool = false
        }
        UIView.animate(withDuration: 1.00) {() -> Void in
            self.MenuVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        }
    }

    func CloseMenu() {
        self.MenuVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.0)
        UIView.animate(withDuration: 0.5, animations: {
            self.MenuVC.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        }) {(_) in
            self.MenuVC.view.removeFromSuperview()
        }
        AppDelegate.menu_bool = true
    }

}

extension HomeStreetView: CLLocationManagerDelegate {
    func configLocation() {
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        stopLocation()
        mEventViewModel.updateLocation(latitude: locValue.latitude, longitude: locValue.longitude)
        print("locations = \(locValue.latitude) \(locValue.longitude)")

    }

    func stopLocation() {
        if CLLocationManager.locationServicesEnabled() {
            locationManager.stopUpdatingLocation()
        }
    }
}
