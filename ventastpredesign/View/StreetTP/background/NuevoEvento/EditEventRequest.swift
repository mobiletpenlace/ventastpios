//
//  EditEventRequest.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta O. on 19/10/20.
//  Copyright © 2020 Juan Reynaldo Escobar Miron. All rights reserved.
//

import UIKit
import ObjectMapper

class EditEventRequest: BaseRequest {

    var MLogin: LoginApp?
    var MEditEvent: EditEvent?

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        MLogin <- map["Login"]
        MEditEvent <- map["editarEvento"]
    }

}
