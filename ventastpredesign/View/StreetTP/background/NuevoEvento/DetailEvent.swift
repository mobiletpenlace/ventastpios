//
//  DetailEvent.swift
//  StreetTpIOS
//
//  Created by Gustavo Tellez  on 19/10/20.
//  Copyright © 2020 Juan Reynaldo Escobar Miron. All rights reserved.
//

import Foundation
import RealmSwift

class DetailEvent: Object {

    @objc dynamic var idEvent: String = ""
    @objc dynamic var dateStart: String = ""
    @objc dynamic var dateEnd: String = ""
    @objc dynamic var startHour: String = ""
    @objc dynamic var endHour: String = ""
    @objc dynamic var latitude: String = ""
    @objc dynamic var length: String = ""
    @objc dynamic var locationEvent: String = ""
    @objc dynamic var typeActivity: String = ""
    @objc dynamic var idTypeActivity: String = ""
    @objc dynamic var idNameActivity: String = ""
    @objc dynamic var NameActivity: String = ""
    @objc dynamic var idSupervisor: String = ""
    @objc dynamic var nameSupervisor: String = ""
}
