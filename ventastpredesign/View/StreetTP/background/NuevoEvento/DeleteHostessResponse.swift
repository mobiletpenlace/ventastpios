//
//  DeleteHostessResponse.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta O. on 19/10/20.
//  Copyright © 2020 Juan Reynaldo Escobar Miron. All rights reserved.
//

import Foundation
import ObjectMapper

class DeleteHostessResponse: BaseResponse {
    var MResult: ResultApp?

 required init?(map: Map) {

   }
    override func mapping(map: Map) {
        MResult <- map["Result"]
    }

}
