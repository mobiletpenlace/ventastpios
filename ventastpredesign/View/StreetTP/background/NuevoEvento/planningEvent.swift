//
//  planningEvent.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta O. on 05/08/20.
//  Copyright © 2020 Juan Reynaldo Escobar Miron. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift

class planningEvent: Object, Mappable {
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  nameCampaign: String = ""
    @objc dynamic var  idCampaign: String = ""
    @objc dynamic var  nameActivity: String = ""
    @objc dynamic var  idActivity: String = ""
    @objc dynamic var  dateEvent: String = ""
    @objc dynamic var  hourStart: String = ""
    @objc dynamic var  hourFinish: String = ""
    @objc dynamic var  latitud: String = ""
    @objc dynamic var  longitud: String = ""
    @objc dynamic var  nameLocation: String = ""

    public required convenience init?(map: Map) {
        self.init()
    }

    override static func primaryKey() -> String? {
           return "uui"
    }

    func mapping(map: Map) {
        nameCampaign <- map["nameCampaign"]
        idCampaign <- map["idCampaign"]
        nameActivity <- map["nameActivity"]
        idActivity <- map["idActivity"]
        dateEvent <- map["dateEvent"]
        hourStart <- map["hourStart"]
        hourFinish <- map["hourFinish"]
        latitud <- map["latitud"]
        longitud <- map["longitud"]
        nameLocation <- map["nameLocation"]

    }

}
