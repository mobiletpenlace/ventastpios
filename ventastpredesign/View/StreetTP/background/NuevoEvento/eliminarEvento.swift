//
//  eliminarEvento.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta O. on 19/10/20.
//  Copyright © 2020 Juan Reynaldo Escobar Miron. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift

class eliminarEvento: Object, Mappable {
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  p_eeo_id: String = ""

    public required convenience init?(map: Map) {
        self.init()
    }

    override static func primaryKey() -> String? {
           return "uui"
    }

    func mapping(map: Map) {
        p_eeo_id <- map["p_eeo_id"]

    }

}
