//
//  AddEvent.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta O. on 16/10/20.
//  Copyright © 2020 Juan Reynaldo Escobar Miron. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift

class AddEvent: Object, Mappable {
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  p_eeo_date_ini: String = ""
    @objc dynamic var  p_eeo_date_fin: String = ""
    @objc dynamic var  p_eeo_descr: String = ""
    @objc dynamic var  p_eeo_coo_lat: String = ""
    @objc dynamic var  p_eeo_coo_long: String = ""
    @objc dynamic var  p_ecg_id_camp: String = ""
    @objc dynamic var  p_ecg_id_act: String = ""
    @objc dynamic var  p_eua_created_by: String = ""
    @objc dynamic var  p_EUA_ID: String = ""

    public required convenience init?(map: Map) {
        self.init()
    }

    override static func primaryKey() -> String? {
           return "uui"
    }

    func mapping(map: Map) {
        p_eeo_date_ini <- map["p_eeo_date_ini"]
        p_eeo_date_fin <- map["p_eeo_date_fin"]
        p_eeo_descr <- map["p_eeo_descr"]
        p_eeo_coo_lat <- map["p_eeo_coo_lat"]
        p_eeo_coo_long <- map["p_eeo_coo_long"]
        p_ecg_id_camp <- map["p_ecg_id_camp"]
        p_ecg_id_act <- map["p_ecg_id_act"]
        p_eua_created_by <- map["p_eua_created_by"]
        p_EUA_ID <- map["p_EUA_ID"]

    }

}
