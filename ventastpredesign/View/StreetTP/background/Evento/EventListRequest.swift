//
//  EventListRequest.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta Ortega on 10/7/19.
//  Copyright © 2019 Juan Reynaldo Escobar Miron. All rights reserved.
//

import UIKit
import ObjectMapper

class EventListRequest: BaseRequest {

    var Mlogin: LoginApp?
    var idUser: String?
    var endDate: String?
    var startDate: String?

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        Mlogin <- map["Login"]
        idUser <- map["idUser"]
        endDate <- map["endDate"]
        startDate <- map["startDate"]
    }

}
