//
//  AssignedHostessResponse.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta Ortega on 10/7/19.
//  Copyright © 2019 Juan Reynaldo Escobar Miron. All rights reserved.
//

import UIKit
import ObjectMapper

class AssignedHostessResponse: BaseResponse {

    var MResult: ResultApp?

    required init?(map: Map) {
    }

    override func mapping(map: Map) {
        MResult <- map["Result"]
    }

}
