//
//  EventoDetail.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 19/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import RealmSwift

class EventoDetail: Object {
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var dateStar: String = ""
    @objc dynamic var dateEnd: String = ""
    @objc dynamic var startHour: String = ""
    @objc dynamic var endHour: String = ""
    @objc dynamic var latitude: String = ""
    @objc dynamic var length: String = ""
    @objc dynamic var descriptionEvent: String = ""
    @objc dynamic var typeActivity: String = ""
    @objc dynamic var IdActivity: String = ""
    @objc dynamic var IdCampaign: String = ""
    @objc dynamic var NameCampaign: String = ""
    @objc dynamic var latitudeDouble: Double = 0.00
    @objc dynamic var longitudDouble: Double = 0.00

    override static func primaryKey() -> String? {
           return "uui"
       }

}
