//
//  team.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta Ortega on 10/7/19.
//  Copyright © 2019 Juan Reynaldo Escobar Miron. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift

class team: Object, Mappable {
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  id: String = ""
    @objc dynamic var  name: String = ""
    @objc dynamic var  activo: String = ""
    @objc dynamic var  puesto: String = ""
    @objc dynamic var  numeroEmpleado: String = ""
    @objc dynamic var  isSelect: String = "0"

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        activo <- map["activo"]
        puesto <- map["puesto"]
        numeroEmpleado <- map["numeroEmpleado"]
    }

    override static func primaryKey() -> String? {
        return "uui"
    }

}
