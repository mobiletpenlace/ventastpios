//
//  tree.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta Ortega on 10/7/19.
//  Copyright © 2019 Juan Reynaldo Escobar Miron. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift

class tree: Object, Mappable {

    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  id: String = ""
    @objc dynamic var  descriptionTree: String = ""
    @objc dynamic var  isSelect: String = "0"

   public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        id <- map["id"]
        descriptionTree <- map["description"]
    }

    override static func primaryKey() -> String? {
        return "uui"
    }

}
