//
//  EventListResponse.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta Ortega on 10/7/19.
//  Copyright © 2019 Juan Reynaldo Escobar Miron. All rights reserved.
//

import Foundation
import ObjectMapper

class EventListResponse: BaseResponse {

    var MResult: ResultApp?
    var MEvents: [Events] = []

 required init?(map: Map) {

   }

    override func mapping(map: Map) {
        MEvents <- map["Events"]
        MResult <- map["Result"]
    }

}
