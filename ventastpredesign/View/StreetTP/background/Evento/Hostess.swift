//
//  Hostess.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta Ortega on 10/7/19.
//  Copyright © 2019 Juan Reynaldo Escobar Miron. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import RealmSwift

class Hostess: Object, Mappable {
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  idHostess: String = ""

      public required convenience init?(map: Map) {
         self.init()
     }

     func mapping(map: Map) {
         idHostess <- map["idHostess"]
     }

     override static func primaryKey() -> String? {
         return "uui"
     }
}

class Supervisor: Object {

    @objc dynamic var id: String = ""
    @objc dynamic var name: String = ""

}
