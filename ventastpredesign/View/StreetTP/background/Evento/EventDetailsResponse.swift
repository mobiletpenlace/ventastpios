//
//  EventDetailsResponse.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta Ortega on 10/7/19.
//  Copyright © 2019 Juan Reynaldo Escobar Miron. All rights reserved.
//

import UIKit
import ObjectMapper

class EventDetailsResponse: BaseResponse {

    var MResult: ResultApp?
    var dateStar: String = ""
    var dateEnd: String = ""
    var startHour: String = ""
    var endHour: String = ""
    var latitude: String = ""
    var length: String = ""
    var descriptionEvent: String = ""
    var typeActivity: String = ""
    var IdActivity: String = ""
    var IdCampaign: String = ""
    var NameCampaign: String = ""
    var Mhostess: [hostessDetails] = []

    required init?(map: Map) {

     }

    override func mapping(map: Map) {
        MResult <- map["Result"]
        dateStar <- map["dateStar"]
        dateEnd <- map["dateEnd"]
        startHour <- map["startHour"]
        endHour <- map["endHour"]
        latitude <- map["latitude"]
        length <- map["length"]
        descriptionEvent <- map["description"]
        typeActivity <- map["typeActivity"]
        IdActivity <- map["IdActivity"]
        IdCampaign <- map["IdCampaign"]
        NameCampaign <- map["NameCampaign"]
        Mhostess <- map["hostess"]
    }

}
