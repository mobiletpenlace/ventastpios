//
//  CategoryResponse.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta Ortega on 10/7/19.
//  Copyright © 2019 Juan Reynaldo Escobar Miron. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class CategoryResponse: BaseResponse {

    var MResult: ResultApp?
    var Mtree: [tree] = []

       required init?(map: Map) {
       }

    override func mapping(map: Map) {
        MResult <- map["Result"]
        Mtree <- map["tree"]
    }

}
