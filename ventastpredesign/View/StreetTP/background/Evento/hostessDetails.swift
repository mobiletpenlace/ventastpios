//
//  hostessDetails.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta O. on 27/11/19.
//  Copyright © 2019 Juan Reynaldo Escobar Miron. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift

class hostessDetails: Object, Mappable {
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  id: String = ""
    @objc dynamic var  name: String = ""

    public required convenience init?(map: Map) {
        self.init()
    }

    override static func primaryKey() -> String? {
           return "uui"
       }

    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]

    }

}
