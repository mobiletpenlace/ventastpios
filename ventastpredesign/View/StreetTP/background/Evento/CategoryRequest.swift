//
//  CategoryRequest.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta Ortega on 10/7/19.
//  Copyright © 2019 Juan Reynaldo Escobar Miron. All rights reserved.
//

import UIKit
import ObjectMapper

class CategoryRequest: BaseRequest {

    var idGeneric: String?
    var MLogin: LoginApp?

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        idGeneric <- map["idGeneric"]
        MLogin <- map["Login"]
    }

}
