//
//  Events.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta Ortega on 10/7/19.
//  Copyright © 2019 Juan Reynaldo Escobar Miron. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift

class Events: Object, Mappable {
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  idEvent: String = ""
    @objc dynamic var  idActivity: String = ""
    @objc dynamic var  idCampaign: String = ""
    @objc dynamic var  campaign: String = ""
    @objc dynamic var  Title: String = ""
    @objc dynamic var  startDate: String = ""
    @objc dynamic var  endDate: String = ""
    @objc dynamic var  startHour: String = ""
    @objc dynamic var  endHour: String = ""

    @objc dynamic var  dayOfYear: Int = 0
    @objc dynamic var  dayOfYearHourFinal: Int = 0
    @objc dynamic var  indexColor: Int = 0
    @objc dynamic var  showDate: Bool = true
    @objc dynamic var  nameDay: String = ""
    @objc dynamic var  numberDay: String = ""

    public required convenience init?(map: Map) {
        self.init()
    }

    override static func primaryKey() -> String? {
           return "uui"
    }

    func mapping(map: Map) {
        idEvent <- map["idEvent"]
        idCampaign <- map["idCampaign"]
        idActivity <- map["idActivity"]
        campaign <- map["campaign"]
        Title <- map["activity"]
        startDate <- map["startDate"]
        endDate <- map["endDate"]
        startHour <- map["startHour"]
        endHour <- map["endHour"]
    }

}
