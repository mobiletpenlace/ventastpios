//
//  EventCalendar.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta O. on 21/11/19.
//  Copyright © 2019 Juan Reynaldo Escobar Miron. All rights reserved.
//

import UIKit
import RealmSwift

class EventCalendar: Object {
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  positionColor: Int = 0
    @objc dynamic var  positionPicker: Int = 0
    @objc dynamic var  week: String = "1 semana"
    @objc dynamic var  days: Int = 7

    override static func primaryKey() -> String? {
        return "uui"
    }

}
