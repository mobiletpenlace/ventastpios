//
//  AssignedHostessRequest.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta Ortega on 10/7/19.
//  Copyright © 2019 Juan Reynaldo Escobar Miron. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class AssignedHostessRequest: BaseRequest {

    var idEvent: String?
    var MHostess: [Hostess] = []
    var Mlogin: LoginApp?

    public override init() {
           super.init()
       }

       public required init?(map: Map) {
           super.init()
       }

    override func mapping(map: Map) {
        idEvent <- map["idEvent"]
        MHostess <- map["Hostess"]
        Mlogin <- map["Login"]
    }

}
