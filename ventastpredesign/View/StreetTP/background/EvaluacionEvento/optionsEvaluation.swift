//
//  optionsEvaluation.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta O. on 28/10/20.
//  Copyright © 2020 Juan Reynaldo Escobar Miron. All rights reserved.
//

import Foundation

struct OptionsEvaluation: Codable {
    var  fiveStar: [String]?
    var  fourStar: [String]?
    var  oneStar: [String]?
    var  threeStar: [String]?
    var  twoStar: [String]?

    enum CodingKeys: String, CodingKey {
        case fiveStar
        case fourStar
        case oneStar
        case threeStar
        case twoStar
    }
    /*required*/ init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        fiveStar = try (values.decodeIfPresent([String].self, forKey: .fiveStar) ?? [])
        fourStar = try (values.decodeIfPresent([String].self, forKey: .fourStar) ?? [])
        oneStar = try (values.decodeIfPresent([String].self, forKey: .oneStar) ?? [])
        threeStar = try (values.decodeIfPresent([String].self, forKey: .threeStar) ?? [])
        twoStar = try (values.decodeIfPresent([String].self, forKey: .twoStar) ?? [])
    }

    func encode(to encoder: Encoder) throws {

    }
}
