//
//  EventEvaluationRequest.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta O. on 21/04/20.
//  Copyright © 2020 Juan Reynaldo Escobar Miron. All rights reserved.
//
import UIKit
import ObjectMapper

class EventEvaluationRequest: BaseRequest {

    var Mlogin: LoginApp?
    var IdEvent: String?
    var Value: String?
    var Commentary: String?
    var Item: String?
    var IdModifica: String?

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        Mlogin <- map["Login"]
        IdEvent <- map["IdEvent"]
        Value <- map["Value"]
        Commentary <- map["Commentary"]
        Item <- map["Item"]
        IdModifica <- map["IdModifica"]
    }

}
