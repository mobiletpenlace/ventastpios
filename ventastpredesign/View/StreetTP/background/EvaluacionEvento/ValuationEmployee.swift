//
//  ValuationEmployee.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta O. on 21/04/20.
//  Copyright © 2020 Juan Reynaldo Escobar Miron. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift

class ValuationEmployee: Object, Mappable {
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  IdEmployee: String = "" // En employeeEvaluationRqequest
    @objc dynamic var  Item: String = ""
    @objc dynamic var  Value: String = ""
    @objc dynamic var  Commentary: String = ""

    public required convenience init?(map: Map) {
        self.init()
    }

    override static func primaryKey() -> String? {
           return "uui"
    }

    func mapping(map: Map) {
        Item <- map["Item"]
        Value <- map["Value"]
        Commentary <- map["Commentary"]
        IdEmployee <- map["IdEmployee"]
    }

}
