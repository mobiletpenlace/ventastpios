//
//  EmployeeEvaluationRequest.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta O. on 21/04/20.
//  Copyright © 2020 Juan Reynaldo Escobar Miron. All rights reserved.
//

import UIKit
import ObjectMapper

class EmployeeEvaluationRequest: BaseRequest {

    var Mlogin: LoginApp?
    var IdEmployee: String?
    var IdEvent: String?
    var IdModifica: String?
    var MValuationEmployee: [ValuationEmployee] = []

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        Mlogin <- map["Login"]
        IdEmployee <- map["IdEmployee"]
        IdEvent <- map["IdEvent"]
        IdModifica <- map["IdModifica"]
        MValuationEmployee <- map["ValuationEmployee"]
    }

}
