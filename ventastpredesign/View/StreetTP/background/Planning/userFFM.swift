//
//  userFFM.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 08/09/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation

struct userFFM: Codable {
    var  apellidoMaterno: String?
    var  apellidoPaterno: String?
    var  nombre: String?
    var  puesto: String?
    var  idUsuario: String?

    enum CodingKeys: String, CodingKey {
        case apellidoMaterno
        case apellidoPaterno
        case nombre
        case puesto
        case idUsuario
    }
    /*required*/ init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        apellidoMaterno = try (values.decodeIfPresent(String.self, forKey: .apellidoMaterno) ?? "")
        apellidoPaterno = try (values.decodeIfPresent(String.self, forKey: .apellidoPaterno) ?? "")
        nombre = try (values.decodeIfPresent(String.self, forKey: .nombre) ?? "")
        puesto = try (values.decodeIfPresent(String.self, forKey: .puesto) ?? "")
        idUsuario = try (values.decodeIfPresent(String.self, forKey: .idUsuario) ?? "")
    }

    func encode(to encoder: Encoder) throws {

    }
}
