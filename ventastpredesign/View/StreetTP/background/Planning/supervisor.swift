//
//  supervisor.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta O. on 26/10/20.
//  Copyright © 2020 Juan Reynaldo Escobar Miron. All rights reserved.
//

import Foundation

struct SupervisorEvent: Codable {
    var  descripcion: String?
    var  id: String?
    var  idSupervisor: String?
    var  numeroempleado: String?

    enum CodingKeys: String, CodingKey {
        case descripcion
        case id
        case idSupervisor
        case numeroempleado
    }
    /*required*/ init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        descripcion = try (values.decodeIfPresent(String.self, forKey: .descripcion) ?? "")
        id = try (values.decodeIfPresent(String.self, forKey: .id) ?? "")
        idSupervisor = try (values.decodeIfPresent(String.self, forKey: .idSupervisor) ?? "")
        numeroempleado = try (values.decodeIfPresent(String.self, forKey: .numeroempleado) ?? "")
    }

    init() {}

    func encode(to encoder: Encoder) throws {

    }
}
