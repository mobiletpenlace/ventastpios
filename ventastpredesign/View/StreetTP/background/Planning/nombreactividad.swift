//
//  nombreactividad.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta O. on 22/10/20.
//  Copyright © 2020 Juan Reynaldo Escobar Miron. All rights reserved.
//

import Foundation

struct Nombreactividad: Codable {
    var  editNombreActividad: Bool?
    var  object: MObject?

    enum CodingKeys: String, CodingKey {
        case editNombreActividad
        case object
    }

    /*required*/ init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        editNombreActividad = try (values.decodeIfPresent(Bool.self, forKey: .editNombreActividad) ?? false)
        object = try (values.decodeIfPresent(MObject.self, forKey: .object))
    }

    init() {}

    func encode(to encoder: Encoder) throws {

    }
}
