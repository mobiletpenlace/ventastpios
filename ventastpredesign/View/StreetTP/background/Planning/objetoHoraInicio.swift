//
//  objetoHoraInicio.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta O. on 23/10/20.
//  Copyright © 2020 Juan Reynaldo Escobar Miron. All rights reserved.
//

import Foundation

struct ObjetoHoraInicio: Codable {
    var  horabase: String?
    var  horatext: String?

    enum CodingKeys: String, CodingKey {
        case horabase
        case horatext
    }
    /*required*/ init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        horabase = try (values.decodeIfPresent(String.self, forKey: .horabase) ?? "")
        horatext = try (values.decodeIfPresent(String.self, forKey: .horatext) ?? "")
    }

    init() {}

    func encode(to encoder: Encoder) throws {

    }
}
