//
//  objectUbicacion.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta O. on 23/10/20.
//  Copyright © 2020 Juan Reynaldo Escobar Miron. All rights reserved.
//

import Foundation

struct ObjectUbicacion: Codable {
    var  editObjectUbicacion: Bool?
    var  latitudSelected: Double?
    var  longitudSelected: Double?
    var  textSelectedPlace: String?

    enum CodingKeys: String, CodingKey {
        case editObjectUbicacion
        case latitudSelected
        case longitudSelected
        case textSelectedPlace
    }
    /*required*/ init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        editObjectUbicacion = try (values.decodeIfPresent(Bool.self, forKey: .editObjectUbicacion) ?? false)
        latitudSelected = try (values.decodeIfPresent(Double.self, forKey: .latitudSelected) ?? 0.0)
        longitudSelected = try (values.decodeIfPresent(Double.self, forKey: .longitudSelected) ?? 0.0)
        textSelectedPlace = try (values.decodeIfPresent(String.self, forKey: .textSelectedPlace) ?? "")
    }

    init() {}

    func encode(to encoder: Encoder) throws {

    }
}
