//
//  tipoactividad.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta O. on 26/10/20.
//  Copyright © 2020 Juan Reynaldo Escobar Miron. All rights reserved.
//

import Foundation

struct Tipoactividad: Codable {
    var  editTipoActividad: Bool?
    var  object: MObject?

    enum CodingKeys: String, CodingKey {
        case editTipoActividad
        case object
    }

    /*required*/ init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        editTipoActividad = try (values.decodeIfPresent(Bool.self, forKey: .editTipoActividad) ?? false)
        object = try (values.decodeIfPresent(MObject.self, forKey: .object))
    }

    init() {}

    func encode(to encoder: Encoder) throws {

    }
}
