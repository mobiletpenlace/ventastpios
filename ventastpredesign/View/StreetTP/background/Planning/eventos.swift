//
//  eventos.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta O. on 22/10/20.
//  Copyright © 2020 Juan Reynaldo Escobar Miron. All rights reserved.
//
import RealmSwift
import Foundation

struct Eventos: Codable {
    var  selectedItem: Bool?
    var  nombreactividad: Nombreactividad?
    var  objectUbicacion: ObjectUbicacion?
    var  objetoFechas: ObjetoFechas?
    var  objetoHoraFin: ObjetoHoraFin?
    var  objetoHoraInicio: ObjetoHoraInicio?
    var supervisor: SupervisorEvent?
    var tipoactividad: Tipoactividad?
    // EXTRA
    var  dayOfYear: Int?
    var  dayOfYearHourFinal: Int?
    var  indexColor: Int?
    var  showDate: Bool?
    var  nameDay: String?
    var  numberDay: String?

    enum CodingKeys: String, CodingKey {
        case selectedItem
        case nombreactividad
        case objectUbicacion
        case objetoFechas
        case objetoHoraFin
        case objetoHoraInicio
        case supervisor
        case tipoactividad
        // EXTRA
        case dayOfYear
        case dayOfYearHourFinal
        case indexColor
        case showDate
        case nameDay
        case numberDay
    }

    /*required*/ init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        selectedItem = try (values.decodeIfPresent(Bool.self, forKey: .selectedItem) ?? true)
        nombreactividad = try (values.decodeIfPresent(Nombreactividad.self, forKey: .nombreactividad))
        objectUbicacion = try (values.decodeIfPresent(ObjectUbicacion.self, forKey: .objectUbicacion))
        objetoFechas = try (values.decodeIfPresent(ObjetoFechas.self, forKey: .objetoFechas))
        objetoHoraInicio = try (values.decodeIfPresent(ObjetoHoraInicio.self, forKey: .objetoHoraInicio))
        objetoHoraFin = try (values.decodeIfPresent(ObjetoHoraFin.self, forKey: .objetoHoraFin))
        supervisor = try (values.decodeIfPresent(SupervisorEvent.self, forKey: .supervisor))
        tipoactividad = try (values.decodeIfPresent(Tipoactividad.self, forKey: .tipoactividad))

        // EXTRA
        dayOfYear = try (values.decodeIfPresent(Int.self, forKey: .dayOfYear) ?? 0)
        dayOfYearHourFinal = try (values.decodeIfPresent(Int.self, forKey: .dayOfYearHourFinal) ?? 0)
        indexColor = try (values.decodeIfPresent(Int.self, forKey: .indexColor) ?? 0)
        showDate = try (values.decodeIfPresent(Bool.self, forKey: .showDate) ?? true)
        nameDay = try (values.decodeIfPresent(String.self, forKey: .nameDay) ?? "")
        numberDay = try (values.decodeIfPresent(String.self, forKey: .numberDay) ?? "")

    }

    init() {}

    func encode(to encoder: Encoder) throws {

    }
}

struct ListaEventos: Codable {
    var  eventos: [Eventos]?

    enum CodingKeys: String, CodingKey {
        case eventos = "eventos"
    }

    /*required*/ init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        eventos = try (values.decodeIfPresent([Eventos].self, forKey: .eventos))
    }

    init() {}

    func encode(to encoder: Encoder) throws {

    }
}
