//
//  objetoFechas.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta O. on 23/10/20.
//  Copyright © 2020 Juan Reynaldo Escobar Miron. All rights reserved.
//

import Foundation

struct ObjetoFechas: Codable {
    var  fechaFin: String?
    var  fechaInicio: String?

    enum CodingKeys: String, CodingKey {
        case fechaFin
        case fechaInicio
    }
    /*required*/ init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        fechaFin = try (values.decodeIfPresent(String.self, forKey: .fechaFin) ?? "")
        fechaInicio = try (values.decodeIfPresent(String.self, forKey: .fechaInicio) ?? "")
    }

    init() {}

    func encode(to encoder: Encoder) throws {

    }
}
