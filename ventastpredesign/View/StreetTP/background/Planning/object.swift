//
//  object.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta O. on 22/10/20.
//  Copyright © 2020 Juan Reynaldo Escobar Miron. All rights reserved.
//

import Foundation

struct MObject: Codable {
    var  descripcion: String?
    var  descripcionCatalogo: String?
    var  id: Int?
    var  idCatalogo: Int?

    enum CodingKeys: String, CodingKey {
        case descripcion
        case descripcionCatalogo
        case id
        case idCatalogo
    }
    /*required*/ init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        descripcion = try (values.decodeIfPresent(String.self, forKey: .descripcion) ?? "")
        descripcionCatalogo = try (values.decodeIfPresent(String.self, forKey: .descripcionCatalogo) ?? nil)
        id = try (values.decodeIfPresent(Int.self, forKey: .id) ?? 0)
        idCatalogo = try (values.decodeIfPresent(Int.self, forKey: .idCatalogo) ?? 0)
    }

    init() {}

    func encode(to encoder: Encoder) throws {

    }
}
