//
//  CheckOutRequest.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta O. on 21/04/20.
//  Copyright © 2020 Juan Reynaldo Escobar Miron. All rights reserved.
//

import UIKit
import ObjectMapper

class CheckOutRequest: BaseRequest {

    var Mlogin: LoginApp?
    var IdEmployee: String?

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        Mlogin <- map["Login"]
        IdEmployee <- map["IdEmployee"]
    }

}
