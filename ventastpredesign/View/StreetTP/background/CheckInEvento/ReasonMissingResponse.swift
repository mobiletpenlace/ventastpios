//
//  ReasonMissingResponse.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta O. on 21/04/20.
//  Copyright © 2020 Juan Reynaldo Escobar Miron. All rights reserved.
//

import Foundation
import ObjectMapper

class ReasonMissingResponse: BaseResponse {
    var MResult: ResultApp?

 required init?(map: Map) {

   }

    override func mapping(map: Map) {
        MResult <- map["Result"]
    }

}
