//
//  UpdateCheckOutRequest.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta O. on 21/04/20.
//  Copyright © 2020 Juan Reynaldo Escobar Miron. All rights reserved.
//

import UIKit
import ObjectMapper

class UpdateCheckOutRequest: BaseRequest {

    var Mlogin: LoginApp?
    var IdEmployee: String?
    var IdEvent: String?
    var TypeCheck: String?
    var Latitude: String?
    var Longitude: String?

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        Mlogin <- map["Login"]
        IdEmployee <- map["IdEmployee"]
        IdEvent <- map["IdEvent"]
        TypeCheck <- map["TypeCheck"]
        Latitude <- map["Latitude"]
        Longitude <- map["Longitude"]
    }

}
