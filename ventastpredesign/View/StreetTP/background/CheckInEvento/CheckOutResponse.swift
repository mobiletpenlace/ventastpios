//
//  CheckOutResponse.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta O. on 21/04/20.
//  Copyright © 2020 Juan Reynaldo Escobar Miron. All rights reserved.
//

import Foundation
import ObjectMapper

class CheckOutResponse: BaseResponse {
    var MResult: ResultApp?
    var CheckIn: String = ""
    var HoraCheckIn: String = ""
    var EventoAsociadoIn: String = ""
    var CheckOut: String = ""
    var HoraCheckOut: String = ""
    var EventoAsociadoOut: String = ""

 required init?(map: Map) {

   }

    override func mapping(map: Map) {
        MResult <- map["Result"]
        CheckIn <- map["CheckIn"]
        HoraCheckIn <- map["HoraCheckIn"]
        EventoAsociadoIn <- map["EventoAsociadoIn"]
        CheckOut <- map["CheckOut"]
        HoraCheckOut <- map["HoraCheckOut"]
        EventoAsociadoOut <- map["EventoAsociadoOut"]
    }

}
