//
//  ReasonMissingRequest.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta O. on 21/04/20.
//  Copyright © 2020 Juan Reynaldo Escobar Miron. All rights reserved.
//

import UIKit
import ObjectMapper

class ReasonMissingRequest: BaseRequest {

    var Mlogin: LoginApp?
    var IdMotivo: String?
    var IdUsuario: String?
    var IdModifica: String?
    var Date: String?

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        Mlogin <- map["Login"]
        IdMotivo <- map["IdMotivo"]
        IdUsuario <- map["IdUsuario"]
        IdModifica <- map["IdModifica"]
        Date <- map["Date"]
    }

}
