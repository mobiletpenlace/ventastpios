//
//  Evidence.swift
//  ventastp
//
//  Created by mhuertao on 29/01/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//
import Foundation
import UIKit

struct Evidence {

    let id: String
    let name: String
    var isExpanded: Bool = false
    var wasConsulted: Bool = false
    var photos: [PhotoEvidence]
}

struct PhotoEvidence {
    var idPhoto: String = ""
    var url: String = ""
    var photoImage: ImageForCollection = ImageForCollection()
    var idUser: String = ""
    var nameUser: String = ""
    var hour: String = ""
    var date: String = ""
}

protocol ImageForCollectionDelegate: AnyObject {
    func rechargeImage(in position: Int)
}

class ImageForCollection: UIImageView {

    weak var imageDelegate: ImageForCollectionDelegate?

       func getImage(from url: URL, position: Int) {

           let size = CGSize(width: 400.0, height: 400.0)
           tag = position

           DispatchQueue.global(qos: .userInitiated).async { [weak self] in

               let image = self?.resizedImage(at: url, for: size)

               DispatchQueue.main.sync {
                   self?.image = image
                   self?.imageDelegate?.rechargeImage(in: position)
                   Logger.println("YA SE DESCARGO LA IMAGEN!!")
               }
           }

       }

       func resizedImage(at url: URL, for size: CGSize) -> UIImage? {

           let options: [CFString: Any] = [
               kCGImageSourceCreateThumbnailFromImageIfAbsent: true,
               kCGImageSourceCreateThumbnailWithTransform: true,
               kCGImageSourceShouldCacheImmediately: true,
               kCGImageSourceThumbnailMaxPixelSize: max(size.width, size.height)
           ]

           guard let imageSource = CGImageSourceCreateWithURL(url as NSURL, nil),
                 let image = CGImageSourceCreateThumbnailAtIndex(imageSource, 0, options as CFDictionary)
           else {
               Logger.println("NO SE PUDO OBTENER LA IMAGEN!!")
               return nil
           }

           return UIImage(cgImage: image)
       }
}
