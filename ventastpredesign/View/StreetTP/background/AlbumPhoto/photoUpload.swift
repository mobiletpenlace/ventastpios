//
//  photoUpload.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta O. on 12/12/19.
//  Copyright © 2019 Juan Reynaldo Escobar Miron. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import RealmSwift

class photoUpload: Object {
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  url: String = ""

    override static func primaryKey() -> String? {
        return "uui"
    }

}
