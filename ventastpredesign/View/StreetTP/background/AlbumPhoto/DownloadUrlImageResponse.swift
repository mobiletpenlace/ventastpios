//
//  DownloadUrlImageResponse.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta Ortega on 10/7/19.
//  Copyright © 2019 Juan Reynaldo Escobar Miron. All rights reserved.
//

import UIKit
import ObjectMapper

class DownloadUrlImageResponse: BaseResponse {
    var MResult: ResultApp?
    var Mphotos: [photos] = []

    required init?(map: Map) {

      }

    override func mapping(map: Map) {
        MResult <- map["Result"]
        Mphotos <- map["photos"]
    }

}
