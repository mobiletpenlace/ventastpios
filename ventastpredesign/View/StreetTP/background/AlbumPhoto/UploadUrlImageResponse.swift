//
//  UploadUrlImageResponse.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta Ortega on 10/7/19.
//  Copyright © 2019 Juan Reynaldo Escobar Miron. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class UploadUrlImageResponse: BaseResponse {

    var MResult: ResultApp?
    var IdImage: String?

    required init?(map: Map) {

    }

    override func mapping(map: Map) {
        MResult <- map["Result"]
        IdImage <- map["IdImage"]
    }

}
