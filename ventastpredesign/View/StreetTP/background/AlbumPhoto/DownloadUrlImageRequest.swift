//
//  DownloadUrlImageRequest.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta Ortega on 10/7/19.
//  Copyright © 2019 Juan Reynaldo Escobar Miron. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class DownloadUrlImageRequest: BaseRequest {

    var idUser: String?
    var idEvent: String?
    var idCategory: String?
    var MLogin: LoginApp?

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        idUser <- map["idUser"]
        idEvent <- map["idEvent"]
        idCategory <- map["idCategory"]
        MLogin <- map["Login"]
    }

}
