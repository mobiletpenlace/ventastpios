//
//  photos.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta Ortega on 10/7/19.
//  Copyright © 2019 Juan Reynaldo Escobar Miron. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import RealmSwift

class photos: Object, Mappable {
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  idPhoto: String = ""
    @objc dynamic var  url: String = ""
    @objc dynamic var  idUser: String = ""
    @objc dynamic var  nameUser: String = ""
    @objc dynamic var  hour: String = ""
    @objc dynamic var  date: String = ""

    func mapping(map: Map) {
        idPhoto <- map["idPhoto"]
        url <- map["url"]
        idUser <- map["idUser"]
        nameUser <- map["nameUser"]
        hour <- map["hour"]
        date <- map["date"]

    }

    public required convenience init?(map: Map) {
        self.init()
    }

    override static func primaryKey() -> String? {
        return "uui"
    }

}
