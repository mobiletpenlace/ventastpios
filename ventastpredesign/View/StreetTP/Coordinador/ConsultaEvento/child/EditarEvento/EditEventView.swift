//
//  EditEventView.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 03/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

protocol AlertCustomViewDelegate {
    func rechargeView()
}
class EditEventView: BaseVentasView {

    @IBOutlet weak var supervisorTexfield: UITextField!
    @IBOutlet weak var typeActivityTexfield: UITextField!
    @IBOutlet weak var nameActivityTexfield: UITextField!
    @IBOutlet weak var dateEvent: UITextField!
    @IBOutlet weak var hourStartTexfield: UITextField!
    @IBOutlet weak var hourFinishTexfield: UITextField!
    @IBOutlet weak var locationTexfield: UITextField!
    var mEventViewModel: EventViewModel!
    var alertCustomDelegate: AlertCustomViewDelegate!
    var currentIdEvent = ""
    var currentNameActivity = ""
    var currentTypeActivity = ""
    var currentIdSupervisor = ""
    var locationEvent = ""
    var latitudeEvent = ""
    var longitudeEvent = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mEventViewModel = EventViewModel(view: self)
        mEventViewModel.atachView(observer: self)
        showDetailsEvent()
    }

    @IBAction func backAction(_ sender: Any) {
        Constants.Back(viewC: self)
    }

    @IBAction func addLocationAction(_ sender: Any) {
        goMapLocation()
    }

    @IBAction func updateEventAction(_ sender: Any) {
        showConfirmActionAlert()
    }

    func showConfirmActionAlert() {
        let confirmActionAlert: AlertNormalView = UIStoryboard(name: "AlertNormalView", bundle: nil).instantiateViewController(withIdentifier: "AlertNormalView") as! AlertNormalView

        confirmActionAlert.providesPresentationContextTransitionStyle = true
        confirmActionAlert.definesPresentationContext = true
        confirmActionAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        confirmActionAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        confirmActionAlert.confirmActionDelegate = self

        self.present(confirmActionAlert, animated: true, completion: nil)
    }

    func goMapLocation() {
        let location: LocationEventView = UIStoryboard(name: "LocationEventView", bundle: nil).instantiateViewController(withIdentifier: "LocationEventView") as! LocationEventView

        location.currentLocation = locationEvent
        location.currentLatitud = (latitudeEvent as NSString).doubleValue
        location.currentLongitud = (longitudeEvent as NSString).doubleValue
        location.getLocationDelegate = self
        self.present(location, animated: false, completion: nil)
    }

    func showDetailsEvent() {
        guard let detailEvent = mEventViewModel.getCurrentDetailEvent() else {return}

        currentIdEvent = detailEvent.idEvent
        supervisorTexfield.text = detailEvent.nameSupervisor

        nameActivityTexfield.text = detailEvent.NameActivity
        currentNameActivity = detailEvent.idNameActivity

        typeActivityTexfield.text = detailEvent.typeActivity
        currentTypeActivity = detailEvent.idTypeActivity

        dateEvent.text = detailEvent.dateStart
        hourStartTexfield.text = detailEvent.startHour
        hourFinishTexfield.text = detailEvent.endHour

        locationEvent = detailEvent.locationEvent
        locationTexfield.text = locationEvent
        latitudeEvent = detailEvent.latitude
        longitudeEvent = detailEvent.length
    }

    func validateData() {

        if currentIdEvent != "" && currentTypeActivity != "" && currentNameActivity != "" && dateEvent.text != "" && hourStartTexfield.text != "" && hourFinishTexfield.text != "" && locationEvent != "" && latitudeEvent != "" && longitudeEvent != "" {
            editCurrentEvent()
        } else {

            Constants.Alert(title: "", body: "Favor de completar todos los campos", type: type.Warning, viewC: self)
        }
    }

    func editCurrentEvent() {
        let date = dateEvent.text!
        let hourStart = hourStartTexfield.text!
        let hourEnd = hourFinishTexfield.text!
        mEventViewModel.editEvent(idEvent: currentIdEvent, idCampaign: currentNameActivity, idActivity: currentTypeActivity, date: date, hourStart: hourStart, hourFinish: hourEnd, description: locationEvent, longitude: longitudeEvent, latitude: latitudeEvent)
    }

    func updateDetailEvent() {
        let newEvent = DetailEvent()
        newEvent.idEvent = currentIdEvent
        newEvent.dateStart = dateEvent.text!
        newEvent.dateEnd = dateEvent.text!
        newEvent.startHour = hourStartTexfield.text!
        newEvent.endHour = hourFinishTexfield.text!
        newEvent.locationEvent = locationEvent
        newEvent.latitude = latitudeEvent
        newEvent.length = longitudeEvent
        newEvent.typeActivity = typeActivityTexfield.text!
        newEvent.idTypeActivity = currentTypeActivity
        newEvent.NameActivity = nameActivityTexfield.text!
        newEvent.idNameActivity = currentNameActivity
        let supervisor = mEventViewModel.getCurrentSupervisor()
        newEvent.idSupervisor = supervisor?.id ?? ""
        newEvent.nameSupervisor = supervisor?.name ?? ""
        mEventViewModel.updateCurrentDetailEvent(newEvent)
    }

    func alertSuccess() {
       let storyboard = UIStoryboard(name: "AlertSuccessView", bundle: nil)
        let alert = storyboard.instantiateViewController(withIdentifier: "AlertSuccessView") as! AlertSuccessView

        alert.providesPresentationContextTransitionStyle = true
        alert.definesPresentationContext = true
        alert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        alert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve

        alert.typeAlert = .eventUpdate
        self.present(alert, animated: true, completion: nil)

        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double((Int64)(3.0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {() -> Void in
             alert.dismiss(animated: true, completion: {() -> Void in
                self.goToDetailEvent()
         })
        })
    }

    func goToDetailEvent() {
        dismiss(animated: false, completion: nil)
        alertCustomDelegate.rechargeView()
    }

}

extension EditEventView: ConfirmActionDelegate {
    func nextAction() {
        validateData()
    }
}

extension EditEventView: getLocationDelegate {
    func getCoordinate(latitud: String, longitud: String, nameLocation: String) {

        if nameLocation == "" {
            locationTexfield.text = "Ubicación Seleccionada"
        } else {
            locationTexfield.text = nameLocation
        }

        locationEvent = locationTexfield.text!
        latitudeEvent = latitud
        longitudeEvent = longitud
    }
}

extension EditEventView: EventObserver {
    func retryEvent(id: String) { }

    func pickerOptions(value: String, id: String) {}

    func successEvents() { }

    func successDetailEvent() { }

    func successListHostes() {}

    func successAssignedHostes() {}

    func successCategory() {}

    func successDeleteEvent() {}

    func successDeleteHostess() {}

    func successEditEvent() {
        mEventViewModel.updateIdActivity(id: currentTypeActivity, title: "")
        updateDetailEvent()
        alertSuccess()
    }

    func onError(errorMessage: String) {
        Constants.Alert(title: "", body: errorMessage, type: type.Error, viewC: self)
    }
}
