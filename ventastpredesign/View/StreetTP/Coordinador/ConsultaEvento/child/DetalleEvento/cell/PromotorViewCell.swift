//
//  PromotorViewCell.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 03/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

protocol HostessViewCellDelegate {
    func deleteHosteIn(position: Int)
}

class PromotorViewCell: UITableViewCell {
    @IBOutlet weak var hostesName: UILabel!
    @IBOutlet weak var deleteHosteImage: UIImageView!
    @IBOutlet weak var deteleHosteButton: UIButton!
    var hostesViewCellDelegate: HostessViewCellDelegate!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func deleteHostes(_ sender: Any) {
        hostesViewCellDelegate.deleteHosteIn(position: self.tag)
    }

}
