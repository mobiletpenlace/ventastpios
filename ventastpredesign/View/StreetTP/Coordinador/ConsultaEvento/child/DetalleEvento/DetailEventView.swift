//
//  DetailEventView.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 03/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

class DetailEventView: BaseVentasView {
    @IBOutlet weak var idEventLabel: UILabel!
    @IBOutlet weak var descriptionEventLabel: UILabel!
    @IBOutlet weak var dateEventLabel: UILabel!
    @IBOutlet weak var scheduleEventLabel: UILabel!
    @IBOutlet weak var locationEventLabel: UILabel!
    @IBOutlet weak var supervisorNameLabel: UILabel!
    @IBOutlet weak var heightHostesView: NSLayoutConstraint!
    @IBOutlet weak var imageHostessButton: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imageEditButton: UIImageView!
    var assignedEventObserver: UpdateAssignedEventObserver!
    var mEventViewModel: EventViewModel!
    var hostess: [hostessDetails] = []
    var hostesViewIsHidden = true
    var hostessToDelete: Int = 0
    var idEvent: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mEventViewModel = EventViewModel(view: self)
        mEventViewModel.atachView(observer: self)
        configHostesView()
        consultEventsDetails()
    }

    @IBAction func backAction(_ sender: Any) {
        dismiss(animated: false, completion: nil)
        assignedEventObserver.refreshEvents() // checar
    }

    @IBAction func hostesButtonPressed(_ sender: Any) {
        hostesViewIsHidden = !hostesViewIsHidden
        showHostesView(hostesViewIsHidden)
    }

    @IBAction func evidencesButtonPressed(_ sender: Any) {
        goToEvidenceEvent()
    }

    @IBAction func editButtonPressed(_ sender: Any) {
        goToEditEvent()
    }

    func configHostesView() {
        heightHostesView.constant = 50.0
        imageHostessButton.isHidden = true
    }

    func consultEventsDetails() {
        cleanInformationView()
        mEventViewModel.consultDetailEvent(idEvent: idEvent)
    }

    func getDescriptionEvent(response: EventoDetail) {

        let detailEvent = DetailEvent()
        detailEvent.idEvent = idEvent
        detailEvent.idSupervisor = mEventViewModel.getCurrentSupervisor()?.id ?? ""
        detailEvent.nameSupervisor = mEventViewModel.getCurrentSupervisor()?.name ?? ""
        detailEvent.idNameActivity = TPCipher.shared.decode(coded: response.IdCampaign) ?? ""
        detailEvent.NameActivity = TPCipher.shared.decode(coded: response.NameCampaign) ?? ""
        detailEvent.idTypeActivity = TPCipher.shared.decode(coded: response.IdActivity) ?? ""
        detailEvent.typeActivity = TPCipher.shared.decode(coded: response.typeActivity) ?? ""
        detailEvent.dateStart = TPCipher.shared.decode(coded: response.dateStar) ?? ""
        detailEvent.dateEnd = TPCipher.shared.decode(coded: response.dateEnd) ?? ""
        detailEvent.startHour = TPCipher.shared.decode(coded: response.startHour) ?? "00:00AM"
        detailEvent.endHour = TPCipher.shared.decode(coded: response.endHour) ?? "00:00AM"
        detailEvent.locationEvent = "Ubicación Seleccionada"
        if response.descriptionEvent != nil && response.descriptionEvent != "" {
            if TPCipher.shared.decode(coded: response.descriptionEvent) != "" {
                detailEvent.locationEvent = TPCipher.shared.decode(coded: response.descriptionEvent) ?? ""
            }
        }

        detailEvent.latitude = TPCipher.shared.decode(coded: response.latitude)!
        detailEvent.length = TPCipher.shared.decode(coded: response.length)!
        hostess = mEventViewModel.getHostessEvent()
        tableView.reloadData()

        mEventViewModel.updateIdEvent(idEvent: detailEvent.idEvent, title: "")
        mEventViewModel.updateIdCategory(id: detailEvent.idTypeActivity, title: "")
        mEventViewModel.updateCurrentDetailEvent(detailEvent)
        loadDetailEvent()
    }

    func loadDetailEvent() {
        // var detailEvent = DetailEvent()
        guard let  detailEvent = mEventViewModel.getCurrentDetailEvent() else {return}

        idEventLabel.text = detailEvent.idEvent
        descriptionEventLabel.text = "\(detailEvent.NameActivity) - \(detailEvent.typeActivity)"
        dateEventLabel.text = detailEvent.dateStart
        scheduleEventLabel.text = "\(detailEvent.startHour) - \(detailEvent.endHour)"
        locationEventLabel.text = detailEvent.locationEvent
        supervisorNameLabel.text = detailEvent.nameSupervisor

        imageHostessButton.image = UIImage(named: "icon_arrow_down_addon") // Se tiene que borrar
        imageHostessButton.isHidden = false
    }

    func cleanInformationView() {
        idEventLabel.text = ""
        descriptionEventLabel.text = ""
        dateEventLabel.text = ""
        scheduleEventLabel.text = ""
        locationEventLabel.text = ""
        supervisorNameLabel.text = ""
        imageHostessButton.isHidden = true
    }

    func changeImageButton(_ value: Bool) {
        if value {
            imageEditButton.image = UIImage(named: "icon_pecil_white")
        } else {
            imageEditButton.image = UIImage(named: "icon_update_hostess")
        }
    }

    func showHostesView(_ value: Bool) {

        var heightForView: CGFloat = 0.0

        if value {
            heightForView = 50.0

        } else {

            if hostess.count == 0 {
                heightForView = 100.0
                tableView.isScrollEnabled = false

            } else if hostess.count < 3 {
                heightForView = CGFloat(50.0 * Double(hostess.count)) + 50.0
                tableView.isScrollEnabled = false

            } else {
                heightForView = 200.0
                tableView.isScrollEnabled = true
            }

        }

        heightHostesView.constant = heightForView

        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
            self.imageHostessButton.transform = self.imageHostessButton.transform.rotated(by: .pi)
        }
    }

    func goToEvidenceEvent() {
        let storyboard = UIStoryboard(name: "EvidenciasEventoView", bundle: nil)
        let evidencesEvent = storyboard.instantiateViewController(withIdentifier: "EvidenciasEventoView") as! EvidenciasEventoView
        present(evidencesEvent, animated: false, completion: nil)
    }

    func goToEditEvent() {
        let storyboard = UIStoryboard(name: "EditEventView", bundle: nil)
        let editNewEvent = storyboard.instantiateViewController(withIdentifier: "EditEventView") as! EditEventView
        editNewEvent.alertCustomDelegate = self
        present(editNewEvent, animated: false, completion: nil)
    }

    func goToAddHostess() {
     /*   let storyboard = UIStoryboard(name: "AlertSelectMultipleView", bundle: nil)
        let addHostess = storyboard.instantiateViewController(identifier: "AlertSelectMultipleView") as! AlertSelectMultipleView
        
        addHostess.providesPresentationContextTransitionStyle = true
        addHostess.definesPresentationContext = true
        addHostess.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        addHostess.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        
      //  addHostess.updateDataDelegate = self
        addHostess.hostessAssigned = hostess
        
        present(addHostess, animated: false, completion: nil)*/
    }

}

extension DetailEventView: AlertCustomViewDelegate {

    func rechargeView() {
        consultEventsDetails()
    }
}

extension DetailEventView: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if hostess.count != 0 {
            return hostess.count
        } else {
            return 1
        }
        // return hostess.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "PromotorViewCell", for: indexPath) as! PromotorViewCell

       /* let nameHoste = TPCipher.shared.decode(coded: hostess[indexPath.row].name)
        cell.hostesName.text = nameHoste
        cell.tag = indexPath.row
       cell.hostesViewCellDelegate = self*/

        // Borrar if
        if hostess.count != 0 {

            let nameHoste = TPCipher.shared.decode(coded: hostess[indexPath.row].name)
            cell.hostesName.text = nameHoste
            cell.tag = indexPath.row
            cell.hostesViewCellDelegate = self

        } else {
            cell.hostesName.text = "No hay hostess asignados"
            cell.deleteHosteImage.isHidden = true
            cell.deteleHosteButton.isEnabled = false
        }

        return cell
    }

}

extension DetailEventView: HostessViewCellDelegate {

    func deleteHosteIn(position: Int) {
        print("BORRANDO HOSTES...")
        hostessToDelete = position
        let idHostess = TPCipher.shared.decode(coded: hostess[position].id) ?? ""
        mEventViewModel.deleteHostess(idHostess: idHostess, idEvent: idEvent)
    }
}

extension DetailEventView: EventObserver {
    func successEditEvent() {}

    func retryEvent(id: String) { }

    func pickerOptions(value: String, id: String) { }

    func successEvents() {}

    func successDetailEvent() {
        guard let detail = mEventViewModel.getDetailEvent() else {return}
        getDescriptionEvent(response: detail)
    }

    func successListHostes() {}

    func successAssignedHostes() {}

    func successCategory() {}

    func successDeleteEvent() { }

    func successDeleteHostess() {
        print("HOSTESS BORRADO CON ÉXITO!!")
      //  alertMessageCustom(message: "Hostess fue borrado éxitosamente", type: typeMessage.correct)
        Constants.Alert(title: "", body: "Hostess fue borrado éxitosamente", type: type.Success, viewC: self )
        consultEventsDetails()
        hostesViewIsHidden = !hostesViewIsHidden
        showHostesView(hostesViewIsHidden)
    }

    func onError(errorMessage: String) {
        Constants.Alert(title: "", body: errorMessage, type: type.Error, viewC: self)
    }
}
