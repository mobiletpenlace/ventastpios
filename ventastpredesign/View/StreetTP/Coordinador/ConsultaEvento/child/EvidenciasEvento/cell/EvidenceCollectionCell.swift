//
//  EvidenceCollectionCell.swift
//  StreetTpIOS
//
//  Created by Gustavo Tellez  on 13/10/20.
//  Copyright © 2020 Juan Reynaldo Escobar Miron. All rights reserved.
//

import UIKit

protocol EvidenceCollectionCellDelegate: AnyObject {
    func getImageSelectedOn(collectionView row: Int, position: Int)
}

class EvidenceCollectionCell: UICollectionViewCell {

    @IBOutlet weak var imageCell: UIImageView!
    @IBOutlet weak var hourLabel: UILabel!
    @IBOutlet weak var nameHoste: UILabel!

    weak var evidenceCollectionCellDelegate: EvidenceCollectionCellDelegate!

    var position: Int!
    var collection: Int!

    override func awakeFromNib() {
        super.awakeFromNib()
        configDoubleTap()
    }

    func configDoubleTap() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(doubleTapped))
        tap.numberOfTapsRequired = 1
        self.addGestureRecognizer(tap)
    }

    @objc func doubleTapped() {
        evidenceCollectionCellDelegate.getImageSelectedOn(collectionView: collection!, position: position!)
    }

}
