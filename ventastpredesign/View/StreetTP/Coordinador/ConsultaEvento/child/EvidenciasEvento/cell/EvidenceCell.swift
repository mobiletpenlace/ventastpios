//
//  EvidenceCell.swift
//  StreetTpIOS
//
//  Created by Gustavo Tellez  on 12/10/20.
//  Copyright © 2020 Juan Reynaldo Escobar Miron. All rights reserved.
//

import UIKit

protocol EvidenceCellDelegate: AnyObject {
    func getIdOf(cell: Int)
}

class EvidenceCell: UITableViewCell {

    @IBOutlet weak var heightView: NSLayoutConstraint!
    @IBOutlet weak var titleButton: UILabel!
    @IBOutlet weak var arrowCell: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!

    weak var evidenceCellDelegate: EvidenceCellDelegate!
    var itemWidth: CGFloat!
    var itemHeight: CGFloat!
    var spaceBetweenRows: CGFloat = 20.0
    var numberOfImages: Int = 0

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configCollectionViewItem()
    }

    @IBAction func displayContent(_ sender: Any) {
        print("TOCO EL BOTON!!")
        evidenceCellDelegate.getIdOf(cell: self.tag)

    }

    func hideCollectionView() {

        if numberOfImages == 0 {
            collectionView.isHidden = true
            collectionView.backgroundColor = .none

        } else {
            collectionView.isHidden = false
            collectionView.backgroundColor = .white
        }
        Logger.blockSeparator()
        Logger.println("CELDA \(self.tag) TIENE VALOR \(collectionView.isHidden) EN COLLECTION")
        Logger.blockSeparator()
    }

    func configCollectionViewItem() {

        itemHeight = collectionView.frame.width * 0.5
        itemWidth = collectionView.frame.width * 0.45

        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()

        layout.sectionInset = UIEdgeInsets(top: spaceBetweenRows, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: itemWidth, height: itemHeight)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = spaceBetweenRows
        collectionView!.collectionViewLayout = layout
    }

    func calculateHeightForCell() -> CGFloat {

        var heightExpanded: CGFloat = 0.0
        let numberOfColumns: CGFloat = 2.0
        let heightOfTitleButton: CGFloat = 60.0

        if numberOfImages == 0 {

            heightExpanded = 280

        } else {

            let n = CGFloat(numberOfImages) / numberOfColumns
            let numberOfItems = n.rounded(.up)
            let heightCollectionView = numberOfItems * (itemHeight + spaceBetweenRows)
            heightExpanded = heightCollectionView + heightOfTitleButton
        }

        return heightExpanded
    }

    func setCollectionViewDataSourceDelegate(dataSourceDelegate: UICollectionViewDataSource & UICollectionViewDelegate, forRow row: Int) {
        collectionView.delegate = dataSourceDelegate
        collectionView.dataSource = dataSourceDelegate
        collectionView.tag = row
        collectionView.reloadData()
    }

}
