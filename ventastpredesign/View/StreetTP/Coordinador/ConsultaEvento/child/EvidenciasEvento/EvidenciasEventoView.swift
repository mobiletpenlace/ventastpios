//
//  EvidenciasEventoView.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 03/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

class EvidenciasEventoView: BaseVentasView, ImageForCollectionDelegate {
    @IBOutlet weak var tableView: UITableView!
    var mAlbumViewModel: AlbumPhotosViewModel!
    var mEventViewModel: EventViewModel!
    var evidences: [Evidence] = []
    var currentCategory: Int = 0
    var isConsultCategory  = false

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mAlbumViewModel = AlbumPhotosViewModel(view: self)
        mAlbumViewModel.atachView(observer: self)
        mEventViewModel = EventViewModel(view: self)
        mEventViewModel.atachView(observer: self)
        consultCategory()
    }

    @IBAction func backAction(_ sender: Any) {
        Constants.Back(viewC: self)
    }

    func consultCategory() {
        isConsultCategory = true
        guard let idActivity = TPCipher.shared.decode(coded: mEventViewModel.getDetailEvent()?.IdActivity ?? "") else {return}
        mEventViewModel.ConsultCategoryTree(idGeneric: idActivity)
    }

    func saveCategories() {

        let categories = mEventViewModel.getCategory()
        for category in categories {
           // let id = TPCipher.shared.decode(coded: category.id)!
           // let category = TPCipher.shared.decode(coded: category.descriptionTree)!
            let aux = Evidence(id: category.id, name: category.descriptionTree, photos: [])
            self.evidences.append(aux)
        }
        tableView.reloadData()
    }

    func saveEvidenceImages() {

        let photos = mAlbumViewModel.getPhotos()
        var position = 0

        for photo in photos {

            var aux = PhotoEvidence()

            aux.idPhoto = photo.idPhoto
            aux.photoImage.imageDelegate = self

            let urlImage = TPCipher.shared.decode(coded: photo.url)!

            if let url = URL(string: urlImage) {
                aux.photoImage.getImage(from: url, position: position)
            }

            aux.url = photo.url
            aux.idUser = photo.idUser
            aux.nameUser = photo.nameUser
            aux.date = photo.date
            aux.hour = photo.hour

            evidences[currentCategory].photos.append(aux)
            position += 1
        }
    }

}
extension EvidenciasEventoView: UITableViewDelegate, UITableViewDataSource, EvidenceCellDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return evidences.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "EvidenceCell", for: indexPath) as! EvidenceCell

        cell.tag = indexPath.row
        cell.evidenceCellDelegate = self

        cell.titleButton.text = evidences[indexPath.row].name
        cell.numberOfImages = evidences[indexPath.row].photos.count

        if evidences[indexPath.row].isExpanded {
         //   cell.arrowCell.image = UIImage(named: "icon_arrow_up_gray")
            cell.arrowCell.image = UIImage(named: "icon_arrow_up_addon")
            cell.heightView.constant = cell.calculateHeightForCell()
            cell.hideCollectionView()

        } else {
           // cell.arrowCell.image = UIImage(named: "icon_arrow_down_gray")
            cell.arrowCell.image = UIImage(named: "icon_arrow_down_addon")

            cell.heightView.constant = 60.0
        }

        return cell
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let tableViewCell = cell as? EvidenceCell else { return }
        tableViewCell.setCollectionViewDataSourceDelegate(dataSourceDelegate: self, forRow: indexPath.row)
        tableViewCell.collectionView.reloadData()
    }

    func getIdOf(cell: Int) {
        currentCategory = cell
        if evidences[cell].isExpanded == false && evidences[cell].wasConsulted == false {
            isConsultCategory = false
            mAlbumViewModel.ConsultPhotos(idCategoty: evidences[cell].id)
        } else {
            showEvidenceImages()
        }
    }

    func showEvidenceImages() {
        evidences[currentCategory].isExpanded = !evidences[currentCategory].isExpanded
        let indexPosition = IndexPath(row: currentCategory, section: 0)
        tableView.reloadRows(at: [indexPosition], with: .automatic)
    }
}

extension EvidenciasEventoView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, EvidenceCollectionCellDelegate {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return evidences[collectionView.tag].photos.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EvidenceCollectionCell", for: indexPath) as! EvidenceCollectionCell
        cell.collection = collectionView.tag
        cell.position = indexPath.item
        let imageInfo = evidences[collectionView.tag].photos[indexPath.item]
        cell.imageCell.image = imageInfo.photoImage.image
        cell.hourLabel.text = TPCipher.shared.decode(coded: imageInfo.hour)
        cell.nameHoste.text = TPCipher.shared.decode(coded: imageInfo.nameUser)
        cell.evidenceCollectionCellDelegate = self
        return cell

    }

    func getImageSelectedOn(collectionView row: Int, position: Int) {
        Logger.blockSeparator()
        Logger.println("TOUCH ON ROW: \(row) , COLUM: \(position)")
        Logger.blockSeparator()
        let storyboard = UIStoryboard(name: "PhotoZoomView", bundle: nil)
        let photoZoom = storyboard.instantiateViewController(withIdentifier: "PhotoZoomView") as! PhotoZoomView
        photoZoom.imageTake = evidences[row].photos[position].photoImage.image
        present(photoZoom, animated: false, completion: nil)
    }

    func rechargeImage(in position: Int) {

        let indexTable = IndexPath(row: currentCategory, section: 0)
        let indexCollection = IndexPath(item: position, section: 0)
        let cell = tableView.cellForRow(at: indexTable) as! EvidenceCell
        cell.collectionView.reloadItems(at: [indexCollection])
    }

}

extension EvidenciasEventoView: AlbumPhotoObserver {
    func successDownload() {
        saveEvidenceImages()
        evidences[currentCategory].wasConsulted = true
        showEvidenceImages()
    }
    func successUpLoad() {}
    func onError(errorMessage: String) {
        if errorMessage != "No se encontraron fotos" && errorMessage != "No results were found for your search." {
            Constants.Alert(title: "", body: errorMessage, type: type.Error, viewC: self)
        }
        if !isConsultCategory {
        evidences[currentCategory].wasConsulted = true
            showEvidenceImages()
        }
    }
}

extension EvidenciasEventoView: EventObserver {
    func successEditEvent() {}
    func retryEvent(id: String) {}
    func pickerOptions(value: String, id: String) {}
    func successEvents() {}
    func successDetailEvent() {}
    func successListHostes() {}
    func successAssignedHostes() {}

    func successCategory() {
        saveCategories()
    }

    func successDeleteEvent() { }
    func successDeleteHostess() {}

}
