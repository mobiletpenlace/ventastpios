//
//  NoEventsListView.swift
//  ventastp
//
//  Created by mhuertao on 21/01/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import UIKit

class NoEventsListView: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

    }

}
