//
//  EventCalendarCoordView.swift
//  ventastp
//
//  Created by mhuertao on 21/01/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import UIKit

class EventCalendarCoordView: BaseVentasView {
    @IBOutlet weak var containerView: UIView!
    var arriveEvents: Bool?
    var dateStart: String = ""
    var dateEnd: String = ""
    var idSupervisor: String = ""
    var mEventViewModel: EventViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mEventViewModel = EventViewModel(view: self)
        mEventViewModel.atachView(observer: self)
        consultEvents()

    }
    @IBAction func backAction(_ sender: Any) {
        Constants.Back(viewC: self)
    }

    func consultEvents() {
        mEventViewModel.consultListEvents(startDate: dateStart, endDate: dateEnd, idUser: idSupervisor)
    }

    func goAssignedEvents() {
        ViewEmbedder.embed(withIdentifier: "ListEventView", parent: self, container: self.containerView) { (vc) in
            let assignedEvent = vc as! ListEventView
            assignedEvent.assignedEventObserver = self
        }
    }

    func goNoAssignedEvents() {
        ViewEmbedder.embed(withIdentifier: "NoEventsListView", parent: self, container: self.containerView) { (vc) in
            let noAssignedEvent = vc as! NoEventsListView
            noAssignedEvent.titleLabel.text = "Aún no tienes eventos"
            noAssignedEvent.bodyLabel.text = "Crea y asigna eventos para tus Supervisores"
        }
    }

}

extension EventCalendarCoordView: EventObserver {
    func successEditEvent() {}

    func successDeleteHostess() {}

    func successDeleteEvent() { }

    func retryEvent(id: String) {}

    func pickerOptions(value: String, id: String) {}

    func successDetailEvent() { }

    func successListHostes() { }

    func successAssignedHostes() { }

    func successCategory() { }

    func successEvents() {
        if mEventViewModel.getEventSortDateQuality().count > 0 {
            goAssignedEvents()
        } else {
            goNoAssignedEvents()
        }
    }

    func onError(errorMessage: String) {
        goNoAssignedEvents()
    }
}

extension EventCalendarCoordView: UpdateAssignedEventObserver {
    func refreshEvents() {
        consultEvents()
    }
}
