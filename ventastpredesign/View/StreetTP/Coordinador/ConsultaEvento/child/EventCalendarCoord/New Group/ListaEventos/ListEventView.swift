//
//  ListEventView.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 03/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
protocol UpdateAssignedEventObserver {
    func refreshEvents()
}

class ListEventView: BaseVentasView {
    @IBOutlet weak var eventCountLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var mEventViewModel: EventViewModel!
    var assignedEventObserver: UpdateAssignedEventObserver!
    var dataEvent: [Events] = []
    var currenIdEvent: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mEventViewModel = EventViewModel(view: self)
        mEventViewModel.atachView(observer: self)
        setUpView()
    }

    func setUpView() {
        dataEvent = mEventViewModel.getEvents()
        if dataEvent.count == 1 {
            eventCountLabel.text = "Mostrando \(dataEvent.count) resultado:"
        } else {
            eventCountLabel.text = "Mostrando \(dataEvent.count) resultados:"
        }
    }

    func goDetail(id: String) {
        let storyboard = UIStoryboard(name: "DetailEventView", bundle: nil)
        let detailEvent = storyboard.instantiateViewController(withIdentifier: "DetailEventView") as! DetailEventView
        detailEvent.idEvent = id
        detailEvent.assignedEventObserver = assignedEventObserver
        present(detailEvent, animated: false, completion: nil)
    }

}

extension ListEventView: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataEvent.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListEventViewCell") as! ListEventViewCell
        cell.id = indexPath.row
        cell.eventDelegate = self
        let event = dataEvent[indexPath.row]
        let idEvent = TPCipher.shared.decode(coded: event.idEvent)
        cell.idEvent.text = idEvent
        let activity = TPCipher.shared.decode(coded: event.campaign)
        let typeEvent = TPCipher.shared.decode(coded: event.Title)
        let detail = "\(activity ?? " ") - \(typeEvent ?? " ")"
        cell.detailEvent.text = detail
        let date = TPCipher.shared.decode(coded: event.startDate)
        cell.dateEvent.text = date
        let scheduleEvent =  mEventViewModel.eventSchedule(startDate: event.startDate, hourStart: event.startHour, hourEnd: event.endHour)
        cell.scheduleEventLabel.text = scheduleEvent
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
}

extension ListEventView: AssignedEventCellDelegate {
    func getEventDetailsWith(id: Int) {
        currenIdEvent = TPCipher.shared.decode(coded: dataEvent[id].idEvent) ?? "0"
        goDetail(id: currenIdEvent)
    }

    func getEventToDelete(id: Int) {
        currenIdEvent = TPCipher.shared.decode(coded: dataEvent[id].idEvent) ?? "0"
        mEventViewModel.deleteEvent(idEvent: currenIdEvent)
    }
}

extension ListEventView: EventObserver {
    func successEditEvent() {}

    func successDeleteHostess() {}

    func retryEvent(id: String) {}

    func pickerOptions(value: String, id: String) {}

    func successDetailEvent() { }

    func successListHostes() { }

    func successAssignedHostes() { }

    func successCategory() { }

    func successEvents() {}

    func successDeleteEvent() {
        Constants.AlertSuccesStreet(typeAlert: .eventDelete, viewC: self)
        assignedEventObserver.refreshEvents()
    }

    func onError(errorMessage: String) {
        Constants.Alert(title: "", body: errorMessage, type: type.Error, viewC: self)
    }
}
