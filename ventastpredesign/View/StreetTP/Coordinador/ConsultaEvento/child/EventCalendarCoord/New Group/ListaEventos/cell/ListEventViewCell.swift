//
//  ListEventViewCell.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 03/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

protocol AssignedEventCellDelegate {

    func getEventDetailsWith(id: Int)
    func getEventToDelete(id: Int)
}

class ListEventViewCell: UITableViewCell {

    @IBOutlet weak var backViewCell: UIView!
    @IBOutlet weak var widthBackView: NSLayoutConstraint!
    @IBOutlet weak var frontView: UIView!
    @IBOutlet weak var frontViewConstraint: NSLayoutConstraint!

    @IBOutlet weak var idEvent: UILabel!
    @IBOutlet weak var detailEvent: UITextField!
    @IBOutlet weak var dateEvent: UILabel!
    @IBOutlet weak var scheduleEventLabel: UILabel!

    var eventDelegate: AssignedEventCellDelegate?
    var id: Int!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configSubViews()
        configGesture()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    @IBAction func showEventDetailPressed(_ sender: Any) {
        showBackView(false)
        eventDelegate?.getEventDetailsWith(id: id)
    }

    @IBAction func deleteEvent(_ sender: Any) {
        showBackView(false)
        eventDelegate?.getEventToDelete(id: id)
    }

    func configGesture() {

        let SwipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.handleSwipe))
        SwipeRight.direction = UISwipeGestureRecognizer.Direction.right

        let SwipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.handleSwipe))
        SwipeLeft.direction = UISwipeGestureRecognizer.Direction.left

        self.frontView.addGestureRecognizer(SwipeRight)
        self.frontView.addGestureRecognizer(SwipeLeft)
    }

    @objc func handleSwipe(sender: UISwipeGestureRecognizer) {

        switch sender.direction {
        case UISwipeGestureRecognizer.Direction.left:
            showBackView(false)

        case UISwipeGestureRecognizer.Direction.right:
            showBackView(true)

        default:
            break
        }
    }

    func showBackView(_ value: Bool) {
        var opacity: CGFloat = 0.0
        var corner: CGFloat = 0.0

        print(value)

        if value {

            frontViewConstraint = frontViewConstraint.changeMultiplier(multiplier: 1.8)
            widthBackView = widthBackView.changeMultiplier(multiplier: 1.0)
            corner = 0.0
            opacity = 0.9

        } else {

            frontViewConstraint = frontViewConstraint.changeMultiplier(multiplier: 1.0)
            widthBackView = widthBackView.changeMultiplier(multiplier: 0.9)
            corner = 10.0
            opacity = 1.0

        }

        UIView.animate(withDuration: 0.3, animations: {
            self.layoutIfNeeded()
            self.frontView.alpha = opacity
            self.backViewCell.layer.cornerRadius = corner

        }, completion: nil)
    }

    func configSubViews() {
        frontViewConstraint = frontViewConstraint.changeMultiplier(multiplier: 1.0)
        backViewCell.layer.cornerRadius = 10.0
    }
}
