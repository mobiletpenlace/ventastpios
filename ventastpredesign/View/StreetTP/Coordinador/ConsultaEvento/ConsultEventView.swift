//
//  ConsultEventView.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 02/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

class ConsultEventView: BaseVentasView {
    @IBOutlet weak var supervisorTextField: UITextField!
    @IBOutlet weak var dateStartTextField: UITextField!
    @IBOutlet weak var dateEndTextField: UITextField!
    var dataSupervisor: [team] = []
    var idSupervisor = ""
    var nameSupervisor = ""
    var mEventViewModel: EventViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mEventViewModel = EventViewModel(view: self)
        mEventViewModel.atachView(observer: self)
    }
    @IBAction func goBackButton(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }

    @IBAction func supervisorButtonPressed(_ sender: Any) {
        if dataSupervisor.count == 0 {
            mEventViewModel.ConsultListHostess()
        } else {
            Constants.OptionComboStreet(data: dataSupervisor.map({ $0.name }), id: "supervisor", header: .supervisor, delegate: self, viewC: self)
        }
    }

    @IBAction func dateButtonPressed(_ sender: UIButton) {

        let calendarView: CalendarView = UIStoryboard(name: "CalendarView", bundle: nil).instantiateViewController(withIdentifier: "CalendarView") as! CalendarView
        calendarView.getDateDelegate = self

        if sender.tag == 0 {
            calendarView.id = "dateStart"
            calendarView.currentDateSelected = dateStartTextField.text

        } else if sender.tag == 1 {
            calendarView.id = "dateEnd"
            calendarView.currentDateSelected = dateEndTextField.text
        }

        calendarView.providesPresentationContextTransitionStyle = true
        calendarView.definesPresentationContext = true
        calendarView.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        calendarView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(calendarView, animated: false, completion: nil)
    }

    @IBAction func cleanFieldsButtonPressed(_ sender: Any) {
        cleanFields()
    }

    @IBAction func searchEventPressed(_ sender: Any) {
        validateData()
    }

    func validateData() {
        if dateStartTextField.text != "" && dateEndTextField.text != "" && supervisorTextField.text != "" {
            updateCurrentSupervisor()

            showEventsIn(dateStart: dateStartTextField.text!, dateEnd: dateEndTextField.text!, idSupervisor: idSupervisor)

        } else {
            Constants.Alert(title: "", body: "Favor de completar todos los campos", type: type.Warning, viewC: self)
        }
    }

    func updateCurrentSupervisor() {
        let currentSupervisor = Supervisor()
        currentSupervisor.id = idSupervisor
        currentSupervisor.name = nameSupervisor
        mEventViewModel.saveCurrentSupervisor(currentSupervisor)
    }

    func showEventsIn(dateStart: String, dateEnd: String, idSupervisor: String) {
        let storyboard = UIStoryboard(name: "EventCalendarCoordView", bundle: nil)
        let detailEvent = storyboard.instantiateViewController(withIdentifier: "EventCalendarCoordView") as! EventCalendarCoordView
        detailEvent.dateStart = dateStart
        detailEvent.dateEnd = dateEnd
        detailEvent.idSupervisor = idSupervisor
        present(detailEvent, animated: false, completion: nil)
    }

    func cleanFields() {
        supervisorTextField.text = ""
        dateStartTextField.text = ""
        dateEndTextField.text = ""
    }

}

extension ConsultEventView: OptionsAlertViewDelegate {
    func getValueOptionsPicker(value: String, id: String, position: Int) {
        if id == "supervisor" {
            supervisorTextField.text = value
            idSupervisor = dataSupervisor[position].id
            nameSupervisor = dataSupervisor[position].name
        }
    }
}

extension ConsultEventView: getDateDelegate {
    func getDate(date: String, id: String) {
        print("FECHA: \(date) CON ID: \(id)")
        if id == "dateStart" {
            dateStartTextField.text = date
        } else if id == "dateEnd" {
            dateEndTextField.text = date
        }
    }
}

extension ConsultEventView: EventObserver {
    func successEditEvent() {}

    func successDeleteHostess() {}
    func successDeleteEvent() {}

    func retryEvent(id: String) { }

    func pickerOptions(value: String, id: String) {}

    func successEvents() {}

    func successDetailEvent() { }

    func successListHostes() {
        dataSupervisor = mEventViewModel.getHostessDesencripte()
        Constants.OptionComboStreet(data: dataSupervisor.map({ $0.name }), id: "supervisor", header: .supervisor, delegate: self, viewC: self)
    }

    func successAssignedHostes() {}

    func successCategory() {}

    func onError(errorMessage: String) {
        Constants.Alert(title: "", body: errorMessage, type: type.Error, viewC: self)
    }

}
