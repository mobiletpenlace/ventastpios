//
//  AlertSuccessView.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 03/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

enum TypeAlert {

    case saveInformation
    case eventSend
    case eventPlanningDelete
    case eventDelete
    case eventUpdate
    case evaluationSend
    case reasonForAbsence
}

struct CustomAlert {

    let image: UIImage?
    let title: String
    let message: String
}

class AlertSuccessView: UIViewController {

    @IBOutlet weak var imageAlert: UIImageView!
    @IBOutlet weak var titleAlert: UILabel!
    @IBOutlet weak var textAlert: UILabel!

    var typeAlert: TypeAlert!
    let optionsAlert: [CustomAlert] = [
        CustomAlert(image: UIImage(named: "icon_event_planned"), title: "Evento enviado", message: "El evento se reflejará en la consulta de eventos del supervisor correspondiente"),
        CustomAlert(image: UIImage(named: "icon_event_planned"), title: "Evento guardado", message: " El evento se reflejará en la planeación del supervisor correspondiente"),
        CustomAlert(image: UIImage(named: "icon_event_erased"), title: "Evento eliminado", message: "El evento fue eliminado de la planeación del supervisor correspondiente"),
        CustomAlert(image: UIImage(named: "icon_event_erased"), title: "Evento eliminado", message: "El evento fue eliminado correctamente"),
        CustomAlert(image: UIImage(named: "icon_event_planned"), title: "Evento actualizado", message: "El evento fue actualizado correctamente"),
        // Falta cambiar las imagenes
        CustomAlert(image: UIImage(named: "icon_event_planned"), title: "Evaluación enviada", message: "Recuerda que esta calificación ya no puede ser editada"),
        CustomAlert(image: UIImage(named: "icon_event_planned"), title: "Motivo de falta enviado", message: "La información se envío Exitosamente")
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configAlertTo(type: typeAlert)
    }

    override func viewDidAppear(_ animated: Bool) {

        perform(#selector(closeAlertView), with: nil, afterDelay: 3.0)
    }

    @objc func closeAlertView() {
        dismiss(animated: false, completion: nil)
    }

    func configAlertTo(type: TypeAlert) {

        switch type {

        case .eventSend:
            imageAlert.image = optionsAlert[0].image
            titleAlert.text = optionsAlert[0].title
            textAlert.text = optionsAlert[0].message

        case .saveInformation:
            imageAlert.image = optionsAlert[1].image
            titleAlert.text = optionsAlert[1].title
            textAlert.text = optionsAlert[1].message

        case .eventPlanningDelete:
            imageAlert.image = optionsAlert[2].image
            titleAlert.text = optionsAlert[2].title
            textAlert.text = optionsAlert[2].message

        case .eventDelete:
            imageAlert.image = optionsAlert[3].image
            titleAlert.text = optionsAlert[3].title
            textAlert.text = optionsAlert[3].message

        case .eventUpdate:
            imageAlert.image = optionsAlert[4].image
            titleAlert.text = optionsAlert[4].title
            textAlert.text = optionsAlert[4].message

        case .evaluationSend:
            imageAlert.image = optionsAlert[5].image
            titleAlert.text = optionsAlert[5].title
            textAlert.text = optionsAlert[5].message

        case .reasonForAbsence:
            imageAlert.image = optionsAlert[6].image
            titleAlert.text = optionsAlert[6].title
            textAlert.text = optionsAlert[6].message
        }

    }
}
