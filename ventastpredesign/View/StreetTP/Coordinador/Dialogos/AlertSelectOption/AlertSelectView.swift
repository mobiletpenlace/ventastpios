//
//  AlertSelectView.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 03/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

enum TypeOfHeader {
    case supervisor
    case activity
    case event
    case typeActivity
}

struct HeaderAlert {
    let image: UIImage?
    let title: String
    let subtitle: String
}

protocol OptionsAlertViewDelegate {

    func getValueOptionsPicker(value: String, id: String, position: Int)
}
class AlertSelectView: UIViewController {
    @IBOutlet weak var imageAlert: UIImageView!
    @IBOutlet weak var titleAlert: UILabel!
    @IBOutlet weak var subtitleAlert: UILabel!
    @IBOutlet weak var heightViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var picker: UIPickerView!

    var optionsAlertViewDelegate: OptionsAlertViewDelegate?
    var maxHeightView: CGFloat!
    var dataPicker: [Any] = []
    var id: String = ""
    var defaultOption: String?

    var typeOfAlert: TypeOfHeader!

    let options: [HeaderAlert] = [
        HeaderAlert(image: UIImage(named: "icon_profile"), title: "Evento asignado a:", subtitle: "Seleccionar el supervisor a cargo"),
        HeaderAlert(image: UIImage(named: "icon_type_activity_gray"), title: "Nombre de la Actividad", subtitle: "Seleccionar actividad a realizar"),
        HeaderAlert(image: UIImage(named: "icon_type_event_gray"), title: "Asignar evento a:", subtitle: "Seleccionar el tipo de evento"), HeaderAlert(image: UIImage(named: "icon_type_event_gray"), title: "Tipo de la Actividad", subtitle: "Seleccionar el tipo de actividad a realizar")
    ]

    override func viewDidLoad() {
        super.viewDidLoad()

        configHeaderAlertWith(typeOfAlert)

        picker.delegate = self
        picker.dataSource = self
    }

    override func viewDidAppear(_ animated: Bool) {
        configDefaultOptionPicker()
        showOptionsView()
        picker.reloadAllComponents()

    }

    @IBAction func continueButtonPressed(_ sender: Any) {

        if dataPicker.count != 0 {
            let optionSelected = dataPicker[picker.selectedRow(inComponent: 0)] as! String

            optionsAlertViewDelegate?.getValueOptionsPicker(value: optionSelected, id: id, position: picker.selectedRow(inComponent: 0))
            dismiss(animated: false, completion: nil)

        } else {
            print("EL DATA PICKER NO TIENE VALORES")
            dismiss(animated: false, completion: nil)
        }

    }

    @IBAction func cancelButtonPressed(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }

    func configHeaderAlertWith(_ type: TypeOfHeader) {

        switch type {
        case .supervisor:
            imageAlert.image = options[0].image
            titleAlert.text = options[0].title
            subtitleAlert.text = options[0].subtitle

        case .activity:
            imageAlert.image = options[1].image
            titleAlert.text = options[1].title
            subtitleAlert.text = options[1].subtitle

        case .event:
            imageAlert.image = options[2].image
            titleAlert.text = options[2].title
            subtitleAlert.text = options[2].subtitle
        case .typeActivity:
            imageAlert.image = options[3].image
            titleAlert.text = options[3].title
            subtitleAlert.text = options[3].subtitle
        }

        maxHeightView = heightViewConstraint.multiplier
        heightViewConstraint = heightViewConstraint.changeMultiplier(multiplier: 0.000001)
        buttonView.isHidden = true
    }

    func configDefaultOptionPicker() {

        if let option = defaultOption {
            Logger.blockSeparator()
            Logger.println("OPCION POR DEFECTO: \(option)")
            Logger.blockSeparator()

            for index in 0...dataPicker.count-1 {

                if option == dataPicker[index] as! String {
                    picker.selectRow(index, inComponent: 0, animated: true)
                }
            }
        }
    }

    func showOptionsView() {
        UIView.animate(withDuration: 0.06, animations: {
            self.heightViewConstraint = self.heightViewConstraint.changeMultiplier(multiplier: self.maxHeightView)
            self.view.layoutIfNeeded()

        }, completion: nil)

        self.buttonView.isHidden = false
    }

}

extension AlertSelectView: UIPickerViewDataSource, UIPickerViewDelegate {

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dataPicker.count
    }

    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40.0
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {

        return dataPicker[row] as? String
    }

    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {

        let rowSize = pickerView.rowSize(forComponent: component)
        let width = rowSize.width
        let height = rowSize.height
        let frame = CGRect(x: 0, y: 0, width: width, height: height)

        let label = UILabel(frame: frame)
        label.textAlignment = .center
        label.layer.cornerRadius = 8
        label.text = dataPicker[row] as? String

        return label
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        pickerView.reloadAllComponents()
//        guard let label = pickerView.view(forRow: row, forComponent: component) as? UILabel else {
//                return
//            }
//        label.backgroundColor = UIColor(named: "color_primary")
//        label.textColor = .white
    }

}
