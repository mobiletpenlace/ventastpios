//
//  AlertNormalView.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 03/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

protocol ConfirmActionDelegate {
    func nextAction()
}
class AlertNormalView: UIViewController {
    var confirmActionDelegate: ConfirmActionDelegate!

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    @IBAction func acceptButtonPressed(_ sender: Any) {
        dismiss(animated: false, completion: nil)
        confirmActionDelegate.nextAction()
    }

    @IBAction func cancelButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
