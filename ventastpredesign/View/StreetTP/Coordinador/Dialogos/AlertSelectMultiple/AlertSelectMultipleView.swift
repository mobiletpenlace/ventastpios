//
//  AlertSelectMultipleView.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 03/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

class AlertSelectMultipleView: UIViewController {
    @IBOutlet weak var heightViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var buttonView: UIView!
    var maxHeightView: CGFloat!
    let checkedImage = UIImage(named: "icon_check_green")
    let uncheckedImage = UIImage(named: "icon_uncheck_gray")
   // var updateDataDelegate: updateDataDelegate?
    var hostessAssigned: [hostessDetails] = []
    var selectableHostess: [team] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        configHostessView()
        consultHostess()
    }

    @IBAction func cancelButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func confirmButtonPressed(_ sender: Any) {

       /* if viewModelHostess.isSelectOnlyHostess() {
            
            if hostessAssigned.count != 0{
                alertMessageCustom(message: "Servicio no disponible", type: typeMessage.info)
                
            }else{
                showLoading()
                viewModelHostess.AddAssignedHostess()
            }
            
        }else{
            alertMessageCustom(message: "Debes seleccionar por lo menos una opción", type: typeMessage.warning)
        }*/
    }

    func configHostessView() {
        maxHeightView = heightViewConstraint.constant
        heightViewConstraint.constant = 0.0
        buttonView.isHidden = true
    }

    func consultHostess() {
      /*  let profileUser = viewModel.getEmployee().profileUser
        if profileUser == "1"{
            viewModel.ConsultHostessBoss(idUser: viewModel.getCurrentSupervisor().id)
        }else{
            viewModel.ConsultHostessBoss()
        }*/

    }

    func showHostess() {

        // Filtra los hostess que ya estan seleccionados
     /*   if hostessAssigned.count != 0{
            
            let hostess = viewModelHostess.getHostessOptional()
            
            for hoste in hostess{
                
                var isAssigned = false
                
                for hosteAssigned in hostessAssigned{
                    
                    if hoste.id != hosteAssigned.id{
                        isAssigned = false
                        
                    }else{
                        isAssigned = true
                        break
                    }
                }
                
                if isAssigned == false{
                    selectableHostess.append(hoste)
                }
            }
            
        }else{
            selectableHostess = viewModelHostess.getHostessOptional()
        }
        designView(num: selectableHostess.count)
        tableView.reloadData()
        showOptionsView()*/
    }

    func designView(num: Int) {
        if num < 4 {
            // maxHeightView = 130.0 + CGFloat(50.0 * Double(num))
            maxHeightView = 300.0
            tableView.isScrollEnabled = false
        }
    }

    func showOptionsView() {
        UIView.animate(withDuration: 0.5, animations: {
            self.heightViewConstraint.constant = self.maxHeightView
            self.view.layoutIfNeeded()

        }, completion: nil)

        self.buttonView.isHidden = false
    }

}

/*extension AlertSelectMultipleView: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        selectableHostess.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddHostessViewCell") as! AddHostessViewCell
        
        checkButton(isSelect: selectableHostess[indexPath.row].isSelect, buttonImage: cell.checkButtonImage)
        cell.optionLabel.text = TPCipher.shared.decode(coded: selectableHostess[indexPath.row].name)
        cell.checkButton.tag = indexPath.row
        cell.checkButton.addTarget(self, action: #selector(handleButtonTapped(sender: )), for: .touchUpInside)
        
        return cell
    }
    
    @objc func handleButtonTapped(sender: UIButton) {
        if (selectableHostess[sender.tag].isSelect == "0"){
            viewModelHostess.updateIsSelectHostess(id: selectableHostess[sender.tag].id, isSelect: "1")
        }else{
            viewModelHostess.updateIsSelectHostess(id: selectableHostess[sender.tag].id, isSelect: "0")
        }
        tableView.reloadData()
    }
    
    func checkButton(isSelect: String, buttonImage: UIImageView){
        
        if (isSelect == "0"){
            buttonImage.image = uncheckedImage
            
        }else{
            buttonImage.image = checkedImage
        }
    }
}*/
