//
//  ListEventPlanificacionView.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 03/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
protocol updateListEventsObserver {
    func refreshEvents(nameSupervisor: String, numberSupervisor: String)
}

class ListEventPlanificacionView: BaseVentasView {
    @IBOutlet weak var eventPlanningTable: UITableView!
    var mPlanningViewModel: EventPlanningViewModel!
    var mCreateEventViewModel: CreateEventViewModel!
    var updateListEvents: updateListEventsObserver?
    var currentSupervisor = ""
    var currentName = ""
    var currentPosition = 0
    var listaEventos: ListaEventos?
    var currentType: TypeAlert = .eventPlanningDelete

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mPlanningViewModel = EventPlanningViewModel(view: self)
        mPlanningViewModel.atachView(observer: self)
        mCreateEventViewModel = CreateEventViewModel(view: self)
        mCreateEventViewModel.atachView(observer: self)
        getEventos()
    }

    func getEventos() {
        print(listaEventos?.eventos)
        print(listaEventos?.eventos?.count)
    }

    func deleteEvent(position: Int) {
        mPlanningViewModel.DeleteEventsPlanning(numSupervisor: listaEventos?.eventos![position].supervisor!.numeroempleado ?? "0", eventos: listaEventos!, position: position)
    }

    func sendEvent(position: Int) {
        mCreateEventViewModel.createEvent(
            idSupervisor: listaEventos?.eventos![position].supervisor!.id ?? "0",
            idCampaign: String(listaEventos?.eventos![position].nombreactividad?.object?.id ?? 0),
            idActivity: String(listaEventos?.eventos![position].tipoactividad?.object?.id ?? 0),
            date: listaEventos?.eventos![position].objetoFechas?.fechaInicio ?? "",
            hourStart: listaEventos?.eventos![position].objetoHoraInicio!.horabase ?? "00:00",
            hourFinish: listaEventos?.eventos![position].objetoHoraFin!.horabase ?? "00:00",
            description: listaEventos?.eventos![position].objectUbicacion!.textSelectedPlace  ?? "",
            longitude: String(listaEventos?.eventos![position].objectUbicacion!.longitudSelected ?? 0.0),
            latitude: String(listaEventos?.eventos![position].objectUbicacion!.latitudSelected ?? 0.0))
    }

    func alertSuccess() {
        let storyboard = UIStoryboard(name: "AlertSuccessView", bundle: nil)
        let alert = storyboard.instantiateViewController(withIdentifier: "AlertSuccessView") as! AlertSuccessView

        alert.providesPresentationContextTransitionStyle = true
        alert.definesPresentationContext = true
        alert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        alert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve

        alert.typeAlert = currentType
        self.present(alert, animated: true, completion: nil)

        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double((Int64)(2.0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {() -> Void in
             alert.dismiss(animated: true, completion: {() -> Void in
                 self.updateListEvents?.refreshEvents(nameSupervisor: self.currentName, numberSupervisor: self.currentSupervisor)

         })
        })
    }

}

extension ListEventPlanificacionView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaEventos?.eventos?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventPlanningViewCell", for: indexPath) as! EventPlanningViewCell

        cell.activityCampaignLabel.text = "\(listaEventos?.eventos![indexPath.row].nombreactividad!.object!.descripcion ?? "" ) - \(listaEventos?.eventos![indexPath.row].tipoactividad!.object!.descripcion ?? "" )"
        cell.dateLabel.text = listaEventos?.eventos![indexPath.row].objetoFechas!.fechaInicio ?? "00/00/0000"
        cell.hourLabel.text = "\(listaEventos?.eventos![indexPath.row].objetoHoraInicio!.horatext ?? "00:00 AM" ) - \(listaEventos?.eventos![indexPath.row].objetoHoraFin!.horatext ?? "00:00 AM" )"
        cell.locationLabel.text = listaEventos?.eventos![indexPath.row].objectUbicacion!.textSelectedPlace ?? "Ubicación seleccionada"
        cell.supervisorLabel.text = listaEventos?.eventos![indexPath.row].supervisor!.descripcion ?? ""
        cell.sendEventButton.tag = indexPath.row
        cell.deleteEventButton.tag = indexPath.row

        cell.sendEventButton.addTarget(self, action: #selector(sendEventButtonTapped(sender: )), for: .touchUpInside)

        cell.deleteEventButton.addTarget(self, action: #selector(deleteEventButtonTapped(sender: )), for: .touchUpInside)
        return cell
    }

    @objc func sendEventButtonTapped(sender: UIButton) {
        currentType = .eventSend
        currentPosition = sender.tag
        sendEvent(position: sender.tag)
    }

    @objc func deleteEventButtonTapped(sender: UIButton) {
        currentType = .eventPlanningDelete
        deleteEvent(position: sender.tag)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (UIScreen.main.bounds.size.height * 0.33)
    }
}

extension ListEventPlanificacionView: EventPlanningObserver {
    func successSearchEventPlanning() {}

    func successCategoryEvaluation() { }

    func successAddEvent() { }

    func successDeleteEvent() {
        alertSuccess()
        // Constants.AlertSuccesStreet(typeAlert: currentType , viewC: self)
    }

    func successDataUser() {}

    func onError(errorMessage: String) {
        Constants.Alert(title: "", body: errorMessage, type: type.Error, viewC: self)
    }

}
extension ListEventPlanificacionView: CreateEventSuccessObserver {
    func successCreateEvent() {
        deleteEvent(position: currentPosition)
        print("SE CREO EL EVENTO  ")
    }

}
