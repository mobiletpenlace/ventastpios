//
//  EventPlanningViewCell.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 03/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

class EventPlanningViewCell: UITableViewCell {

    @IBOutlet weak var activityCampaignLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var hourLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var supervisorLabel: UILabel!

    @IBOutlet weak var eventPlanningView: UIView!
    @IBOutlet weak var optionsBackgroundView: UIView!
    @IBOutlet weak var sendEventButton: UIButton!
    @IBOutlet weak var deleteEventButton: UIButton!
    @IBOutlet weak var principalContainerView: UIView!
    @IBOutlet weak var optionsSubview: UIView!

    @IBOutlet weak var rightConstraintEventPlaningView: NSLayoutConstraint!
    @IBOutlet weak var leftConstraintEventPlanningView: NSLayoutConstraint!
    @IBOutlet weak var rightConstraintOptionsView: NSLayoutConstraint!
    @IBOutlet weak var leftConstraintOptionsView: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        configGesture()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

    func configGesture() {
        let SwipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.handleSwipe))
        SwipeRight.direction = UISwipeGestureRecognizer.Direction.right
        let SwipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.handleSwipe))
        SwipeLeft.direction = UISwipeGestureRecognizer.Direction.left
        principalContainerView.addGestureRecognizer(SwipeRight)
        principalContainerView.addGestureRecognizer(SwipeLeft)
    }

    @objc func handleSwipe(sender: UISwipeGestureRecognizer) {
        switch sender.direction {
        case UISwipeGestureRecognizer.Direction.left:
            closeCard()
        case UISwipeGestureRecognizer.Direction.right:
            openCard()
        default:
            break
        }
    }

    func openCard() {
        optionsSubview.isHidden = false
        UIView.animate(withDuration: 0.5) {() -> Void in
            self.eventPlanningView.backgroundColor = #colorLiteral(red: 0.9610000253, green: 0.9649999738, blue: 0.9689999819, alpha: 1).withAlphaComponent(0.8)
            self.leftConstraintEventPlanningView.constant = (UIScreen.main.bounds.size.width/3)
            self.rightConstraintEventPlaningView.constant = -(UIScreen.main.bounds.size.width/3) - 20
            self.rightConstraintOptionsView.constant = 0
           // self.leftConstraintOptionsView.constant = 0

        }
    }

    func closeCard() {
        optionsSubview.isHidden = true
        UIView.animate(withDuration: 0.5) {() -> Void in
            self.eventPlanningView.backgroundColor = #colorLiteral(red: 0.9610000253, green: 0.9649999738, blue: 0.9689999819, alpha: 1).withAlphaComponent(1)
            self.leftConstraintEventPlanningView.constant = 20
            self.rightConstraintEventPlaningView.constant = 20
            self.rightConstraintOptionsView.constant = 20
         //   self.leftConstraintOptionsView.constant = 20

        }
    }

}
