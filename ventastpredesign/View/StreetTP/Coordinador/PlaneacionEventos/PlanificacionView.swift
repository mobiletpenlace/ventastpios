//
//  PlanificacionView.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 03/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import CoreLocation

class PlanificacionView: BaseVentasView, OptionsAlertViewDelegate {
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var supervisorTexfield: UITextField!
    var mEventViewModel: EventViewModel!
    var mPlanningViewModel: EventPlanningViewModel!
    var isUpdate = false
    var supervisor: [team] = []
    var numberCurrent = ""
    var nameCurrent = ""
    var isConsultPlanning = false
    var numberSupervisor: String  = "" {
            didSet {
                isConsultPlanning = true
                mPlanningViewModel.ConsultEventsPlanning(numSupervisor: numberSupervisor )
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mEventViewModel = EventViewModel(view: self)
        mEventViewModel.atachView(observer: self)
        mPlanningViewModel = EventPlanningViewModel(view: self)
        mPlanningViewModel.atachView(observer: self)
        consultSupervisor()
    }
    @IBAction func goBack(_ sender: Any) {
        Constants.Back(viewC: self)
    }

    @IBAction func showSuperviser(_ sender: Any) {
        if  supervisor.count == 0 {
            consultSupervisor()
        } else {
            showPickerListSupervisor()
        }
    }

    func consultSupervisor() {
        mEventViewModel.ConsultListHostess()
    }

    func showPickerListSupervisor() {
        Constants.OptionComboStreet(data: supervisor.map({ $0.name }), id: "supervisor", header: .supervisor, delegate: self, viewC: self)
    }

    func getValueOptionsPicker(value: String, id: String, position: Int) {
        print("EL VALOR QUE LLEGO FUE \(value) con id \(id)")
        supervisorTexfield.text = value
        numberSupervisor = supervisor[position].numeroEmpleado
    }

    func goAssignedEventPlanningView(listaEventos: ListaEventos?) {
        let hostess: ListEventPlanificacionView = UIStoryboard(name: "ListEventPlanificacionView", bundle: nil).instantiateViewController(withIdentifier: "ListEventPlanificacionView") as! ListEventPlanificacionView
        hostess.listaEventos = listaEventos
        hostess.updateListEvents = self
        hostess.currentSupervisor = numberSupervisor
        hostess.currentName = supervisorTexfield.text ?? ""
         ViewEmbedder.embed(
             parent: self,
             container: self.viewContainer,
             child: hostess,
            previous: self.childViewControllers.first
         )
    }
}
extension PlanificacionView: updateListEventsObserver {
    func refreshEvents(nameSupervisor: String, numberSupervisor: String) {
        isUpdate = true
        numberCurrent = numberSupervisor
        nameCurrent = nameSupervisor
        consultSupervisor()
    }

}
extension PlanificacionView: EventObserver {
    func successEditEvent() {}

    func successDeleteHostess() {
    }

    func retryEvent(id: String) {}

    func pickerOptions(value: String, id: String) {}

    func successEvents() {}

    func successDetailEvent() {}

    func successListHostes() {
        supervisor = mEventViewModel.getHostessDesencripte()
        if isUpdate == false {
            ViewEmbedder.embed(withIdentifier: "NoAssignedEventView", parent: self, container: viewContainer)
            print(supervisor)
        } else {
            supervisorTexfield.text = nameCurrent
            if  numberCurrent != "" {
                numberSupervisor = numberCurrent
            }
        }
    }

    func successAssignedHostes() {}

    func successCategory() {  }

    func onError(errorMessage: String) {
        if errorMessage != "No se encontraron eventos en planificación" {
            Constants.Alert(title: "", body: errorMessage, type: type.Error, viewC: self)
        }
        if isConsultPlanning {

            ViewEmbedder.embed(withIdentifier: "NoAssignedEventView", parent: self, container: viewContainer)
        }
    }

}
extension PlanificacionView: EventPlanningObserver {
    func successSearchEventPlanning() {
        let events = mPlanningViewModel.getEvents()
        goAssignedEventPlanningView(listaEventos: events)
    }

    func successCategoryEvaluation() {}

    func successAddEvent() {  }

    func successDeleteEvent() {}

    func successDataUser() {}

}
