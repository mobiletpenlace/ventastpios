//
//  LocationEventView.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 02/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import MapKit

protocol getLocationDelegate {
    func getCoordinate(latitud: String, longitud: String, nameLocation: String)
}

class LocationEventView: BaseVentasView, UIGestureRecognizerDelegate {
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var acceptView: UIView!
    var annotation: MKAnnotation!
    var localSearchRequest: MKLocalSearch.Request!
    var localSearch: MKLocalSearch!
    var localSearchResponse: MKLocalSearch.Response!
    var error: NSError!
    var pointAnnotation: MKPointAnnotation!
    var pinAnnotationView: MKPinAnnotationView!
    var searchSource: [String] = []
    var searchSourceSubtitle: [String] = []
    var getLocationDelegate: getLocationDelegate?
    var currentLatitud = 0.00
    var currentLongitud = 0.00
    var currentLocation = ""
    var mEventViewModel: EventViewModel!
    var mCreateEventViewModel: CreateEventViewModel!
    lazy var searchCompleter: MKLocalSearchCompleter = {
        let sC = MKLocalSearchCompleter()
        sC.delegate = self
        return sC
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mEventViewModel = EventViewModel(view: self)
        mCreateEventViewModel = CreateEventViewModel(view: self)
        longPress()
        zoomUserLocation()
        configDefaultLocation()
    }

    @IBAction func goback(_ sender: Any) {
        Constants.Back(viewC: self)
    }

    @IBAction func showDetailCreateEvent(_ sender: Any) {
        if currentLongitud == 0.00 && currentLatitud == 0.00 {
            Constants.Alert(title: "", body: "Porfavor, selecciona una ubicación", type: type.Warning, viewC: self)
        } else {
            if (mEventViewModel.getEmploye()?.profileUser ?? "") == "2" {
                // save location planeacion 
                mCreateEventViewModel.updateLocationEvent(latitude: currentLatitud, longitude: currentLongitud, location: currentLocation)
                let detail: NewEventDetailView = UIStoryboard(name: "NewEventDetailView", bundle: nil).instantiateViewController(withIdentifier: "NewEventDetailView") as! NewEventDetailView

                self.present(detail, animated: false, completion: nil)
            } else {
                getLocationDelegate?.getCoordinate(latitud: String(currentLatitud), longitud: String(currentLongitud), nameLocation: searchBar.text ?? "")
                dismiss(animated: false, completion: nil)
            }
        }
    }

    func zoomUserLocation() {
        let employee =  mEventViewModel.getEmploye()
        let location = CLLocationCoordinate2D(latitude: employee?.currentLatitude ?? 0, longitude: employee?.currentLongitude ?? 0)
        let region = MKCoordinateRegionMakeWithDistance(location, 600.0, 600.0)
        self.mapView.setRegion(region, animated: true)
    }

    func longPress() {
        let lpgr = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress))
        lpgr.minimumPressDuration = 1
        lpgr.delaysTouchesBegan = true
        lpgr.delegate = self
        self.mapView.addGestureRecognizer(lpgr)
    }

    @objc func handleLongPress(gestureRecognizer: UILongPressGestureRecognizer) {
        if gestureRecognizer.state != UIGestureRecognizer.State.ended {
            return
        } else if gestureRecognizer.state != UIGestureRecognizer.State.began {
            let touchPoint = gestureRecognizer.location(in: self.mapView)
            removeAnnotations()
            clearSearch()
            clearData()

            let touchMapCoordinate =  self.mapView.convert(touchPoint, toCoordinateFrom: mapView)
            let coordinateTemp: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude:
                touchMapCoordinate.latitude, longitude: touchMapCoordinate.longitude)
            currentLatitud = touchMapCoordinate.latitude
            currentLongitud = touchMapCoordinate.longitude
            currentLocation = ""
            addAnotation(coordinate: coordinateTemp, title: "Evento")
        }
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        tableView.isHidden = true
        searchBar.resignFirstResponder()
        removeAnnotations()
        clearData()
        localSearchRequest = MKLocalSearch.Request()
        localSearchRequest.naturalLanguageQuery = searchBar.text
        localSearch = MKLocalSearch(request: localSearchRequest)
        localSearch.start { (localSearchResponse, _) -> Void in

            if localSearchResponse == nil {
                Constants.Alert(title: "", body: "No se encontro el lugar", type: type.Error, viewC: self)
                return
            }
            let coordinatePlace = CLLocationCoordinate2D(latitude: localSearchResponse!.boundingRegion.center.latitude, longitude: localSearchResponse!.boundingRegion.center.longitude)
            self.currentLatitud = localSearchResponse!.boundingRegion.center.latitude
            self.currentLongitud = localSearchResponse!.boundingRegion.center.longitude
            self.currentLocation = searchBar.text ?? ""
            self.addAnotation(coordinate: coordinatePlace, title: "Evento")
            self.addRegion(coordinate: coordinatePlace)

        }
    }

    func configDefaultLocation() {

        if currentLatitud != 0.00 && currentLongitud != 0.00 {

            removeAnnotations()
            acceptView.isHidden = false

            if currentLocation == "Ubicación seleccionada" {
                currentLocation = ""
            }
            searchBar.text = currentLocation

            let coordinateTemp: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude:
                currentLatitud, longitude: currentLongitud)

            if CLLocationCoordinate2DIsValid(coordinateTemp) {
                addAnotation(coordinate: coordinateTemp, title: "Evento")
                addRegion(coordinate: coordinateTemp)

            } else {
                Constants.Alert(title: "", body: "Las coordenadas no son validas.", type: type.Warning, viewC: self)

            }

        }
    }

    func removeAnnotations() {
        if mapView.annotations.count > 0 {
            mapView.removeAnnotations(mapView.annotations)
        }
    }

    func clearSearch() {
        searchBar.text = ""
        tableView.isHidden = true
    }

    func clearData() {
        currentLatitud = 0.00
        currentLongitud = 0.00
        currentLocation = ""
    }

    func addAnotation(coordinate: CLLocationCoordinate2D, title: String = "") {
        self.pointAnnotation = MKPointAnnotation()
        self.pointAnnotation.coordinate = coordinate
        self.pointAnnotation.title = title
        self.pinAnnotationView = MKPinAnnotationView(annotation: self.pointAnnotation, reuseIdentifier: nil)
        self.mapView.centerCoordinate = self.pointAnnotation.coordinate
        self.mapView.addAnnotation(self.pinAnnotationView.annotation!)
    }

    func addRegion(coordinate: CLLocationCoordinate2D) {
        let region = MKCoordinateRegionMakeWithDistance(coordinate, 600.0, 600.0)
        self.mapView.setRegion(region, animated: true)
    }

}

extension LocationEventView: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

        if !searchText.isEmpty {
            self.tableView.isHidden = false
            searchCompleter.queryFragment = searchText
        } else {
            if mapView.annotations.count > 0 {
                mapView.removeAnnotations(mapView.annotations)
            }
             clearData()
            self.tableView.isHidden = true
        }
    }
}

extension LocationEventView: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 48
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchSource.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = self.tableView.dequeueReusableCell(withIdentifier: "LocationViewCell", for: indexPath) as! LocationViewCell
        cell.name.text = self.searchSource[indexPath.row]
        cell.subname.text = self.searchSourceSubtitle[indexPath.row]
        cell.selectButton.tag = indexPath.row
        cell.selectButton.addTarget(self, action: #selector(handleButtonTapped(sender:)), for: .touchUpInside)

        return cell
    }

    @objc func handleButtonTapped(sender: UIButton) {
        let selectedItem = searchSource[sender.tag] + " " + searchSourceSubtitle[sender.tag]
        searchBar.text = selectedItem
        searchBarSearchButtonClicked(searchBar)
    }

}

extension LocationEventView: MKLocalSearchCompleterDelegate {

    func completerDidUpdateResults(_ completer: MKLocalSearchCompleter) {

        searchSource = completer.results.map { $0.title }
        searchSourceSubtitle = completer.results.map { $0.subtitle }
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }

    func completer(_ completer: MKLocalSearchCompleter, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
}
