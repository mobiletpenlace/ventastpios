//
//  LocationViewCell.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 02/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

class LocationViewCell: UITableViewCell {
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var subname: UILabel!
    @IBOutlet weak var selectButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
