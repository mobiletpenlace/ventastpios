//
//  NewEventView.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 02/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

class NewEventView: BaseVentasView {

    @IBOutlet weak var supervisorTexfield: UITextField!
    @IBOutlet weak var activityTexfield: UITextField!
    @IBOutlet weak var typeActivityTexfield: UITextField!
    @IBOutlet weak var dateEvent: UITextField!
    @IBOutlet weak var hourStartTexfield: UITextField!
    @IBOutlet weak var hourFinishTexfield: UITextField!
    @IBOutlet weak var locationTexfield: UITextField!
    var listaEventos: ListaEventos?
    var arrayActivity: [tree] = []
    var arrayCampaign: [tree] = []
    var currentIdCampaign = ""
    var currentIdActivity = ""

    var supervisor: [team] = []
    var currentNameActivity = ""
    var currentTypeActivity = ""
    var currentIdSupervisor = ""
    var currentNameSupervisor = ""
    var currentNumberSupervisor = ""
    var currentLatitude = ""
    var currentLongitude = ""
    var isActivity = false
    var currentType: TypeAlert = .eventSend
    var mEventViewModel: EventViewModel!
    var mCreateEventViewModel: CreateEventViewModel!
    var mEventPlanningViewModel: EventPlanningViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mEventViewModel = EventViewModel(view: self)
        mEventViewModel.atachView(observer: self)
        mEventPlanningViewModel = EventPlanningViewModel(view: self)
        mEventPlanningViewModel.atachView(observer: self)
        mCreateEventViewModel = CreateEventViewModel(view: self)
        mCreateEventViewModel.atachView(observer: self)
        clearData()
    }

    @IBAction func goHome(_ sender: Any) {
        Constants.Back(viewC: self)
    }

    @IBAction func nameSupervisorAction(_ sender: Any) {
        if  supervisor.count == 0 {
            mEventViewModel.ConsultListHostess()
        } else {
            showPickerListSupervisor()
        }
    }

    @IBAction func nameActivityAction(_ sender: Any) {
        if  arrayActivity.count == 0 {
            isActivity = true
            mEventViewModel.ConsultCategoryTree(idGeneric: "1")
        } else {
            showPickerListActivity()
        }
    }

    @IBAction func typeActivityAction(_ sender: Any) {
        if  arrayCampaign.count == 0 {
            isActivity = false
            mEventViewModel.ConsultCategoryTree(idGeneric: "2")
        } else {
            showPickerListTypeActivity()
        }
    }

    @IBAction func calendarAction(_ sender: Any) {
        goCalendar()
    }

    @IBAction func hourStartAction(_ sender: Any) {
        Constants.OptionComboDate(origen: "hourStart", viewC: self)
    }

    @IBAction func hourFinishAction(_ sender: Any) {
        Constants.OptionComboDate(origen: "hourFinish", viewC: self)
    }

    @IBAction func addLocationAction(_ sender: Any) {
        goMapLocation()
    }

    @IBAction func saveEventAction(_ sender: Any) {
        currentType = .saveInformation
        validateDataForm(isPlanning: true)
    }

    @IBAction func sendEventAction(_ sender: Any) {
        currentType = .eventSend
        validateDataForm(isPlanning: false)
    }

    func validateDataForm(isPlanning: Bool) {
        if currentIdSupervisor != "" && currentIdCampaign != "" && currentIdActivity != "" && dateEvent.text != "" && hourStartTexfield.text != "" && hourFinishTexfield.text != "" && locationTexfield.text != "" && currentLongitude != "" && currentLatitude != "" {
            if isPlanning {
                consultListEvent()
            } else {
                consultCreateEvent()
            }
        } else {
            Constants.Alert(title: "", body: "Favor de completar todos los campos", type: type.Warning, viewC: self)
        }
    }

    func consultCreateEvent() {
        mCreateEventViewModel.createEvent(idSupervisor: currentIdSupervisor, idCampaign: currentIdActivity, idActivity: currentIdCampaign, date: dateEvent.text ?? "", hourStart: hourStartTexfield.text ?? "", hourFinish: hourFinishTexfield.text ?? "", description: locationTexfield.text ?? "", longitude: currentLongitude, latitude: currentLatitude)
    }

    func consultListEvent() {
        mEventPlanningViewModel.ConsultEventsPlanning(numSupervisor: currentNumberSupervisor)
    }

    func consultSaveEvent() {
        mEventPlanningViewModel.AddEventsPlanning(listaEvento: listaEventos, numSupervisor: currentNumberSupervisor, idSupervisor: currentIdSupervisor, nameSupervisor: currentNameSupervisor, idCampaign: currentIdActivity, nameCampaign: currentNameActivity, idActivity: currentIdCampaign, nameActivity: currentTypeActivity, date: dateEvent.text ?? "", hourStart: hourStartTexfield.text ?? "", hourFinish: hourFinishTexfield.text ?? "", description: locationTexfield.text ?? "", longitude: currentLongitude, latitude: currentLatitude)
    }

    func goMapLocation() {
        let location: LocationEventView = UIStoryboard(name: "LocationEventView", bundle: nil).instantiateViewController(withIdentifier: "LocationEventView") as! LocationEventView
        location.getLocationDelegate = self
        self.present(location, animated: false, completion: nil)
    }

    func goCalendar() {
        let calendarView: CalendarView = UIStoryboard(name: "CalendarView", bundle: nil).instantiateViewController(withIdentifier: "CalendarView") as! CalendarView
        calendarView.getDateDelegate = self
        calendarView.providesPresentationContextTransitionStyle = true
        calendarView.definesPresentationContext = true
        calendarView.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        calendarView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(calendarView, animated: false, completion: nil)
    }

    func showPickerListActivity() {
        Constants.OptionComboStreet(data: arrayActivity.map({ $0.descriptionTree }), id: "activity", header: .activity, delegate: self, viewC: self)
    }

    func showPickerListTypeActivity() {
        Constants.OptionComboStreet(data: arrayCampaign.map({ $0.descriptionTree }), id: "campaign", header: .typeActivity, delegate: self, viewC: self)
    }

    func showPickerListSupervisor() {
        Constants.OptionComboStreet(data: supervisor.map({ $0.name }), id: "supervisor", header: .supervisor, delegate: self, viewC: self)
    }

    func clearData() {
        supervisorTexfield.text = ""
        activityTexfield.text = ""
        typeActivityTexfield.text = ""
        dateEvent.text = ""
        hourStartTexfield.text = ""
        hourFinishTexfield.text = ""
        locationTexfield.text = ""
    }
}

extension NewEventView: getLocationDelegate {
    func getCoordinate(latitud: String, longitud: String, nameLocation: String) {
        currentLatitude = latitud
        currentLongitude = longitud
        if nameLocation == "" {
            locationTexfield.text = "Ubicación seleccionada"
        } else {
            locationTexfield.text = nameLocation
        }
    }
}

extension NewEventView: getDateDelegate {
    func getDate(date: String, id: String) {
        dateEvent.text = date
    }
}

extension NewEventView: EventPlanningObserver {
    func successSearchEventPlanning() {
        listaEventos = mEventPlanningViewModel.getEvents()
        consultSaveEvent()
    }

    func successCategoryEvaluation() {}

    func successAddEvent() {
        clearData()
        Constants.AlertSuccesStreet(typeAlert: currentType, dismissParent: true, viewC: self)
    }

    func successDeleteEvent() {}

    func successDataUser() {}

}

extension NewEventView: CreateEventSuccessObserver {
    func successCreateEvent() {
        clearData()
        Constants.AlertSuccesStreet(typeAlert: currentType, dismissParent: true, viewC: self)
    }
}

extension NewEventView: OptionsAlertViewDelegate {
    func getValueOptionsPicker(value: String, id: String, position: Int) {
        if id == "activity" {
            currentNameActivity = value
            currentIdActivity = arrayActivity[position].id
            activityTexfield.text = value
        } else if id == "campaign" {
            currentTypeActivity = value
            currentIdCampaign = arrayCampaign[position].id
            typeActivityTexfield.text = value
        } else if id == "supervisor" {
            supervisorTexfield.text = value
            currentIdSupervisor = supervisor[position].id
            currentNameSupervisor = supervisor[position].name
            currentNumberSupervisor = supervisor[position].numeroEmpleado
        }
    }

}
extension NewEventView: EventObserver {
    func successEditEvent() {}

    func successDeleteHostess() {}

    func retryEvent(id: String) { }

    func pickerOptions(value: String, id: String) {
        if id == "hourStart" {
            hourStartTexfield.text = value
        } else if id == "hourFinish" {
            hourFinishTexfield.text = value
        }
    }

    func successEvents() { }

    func successDetailEvent() { }

    func successListHostes() {
        supervisor = mEventViewModel.getHostessDesencripte()
        showPickerListSupervisor()
    }

    func successAssignedHostes() {}

    func successCategory() {
        if isActivity {
            arrayActivity  = mEventViewModel.getCategory()
            Constants.OptionComboStreet(data: arrayActivity.map({ $0.descriptionTree }), id: "activity", header: .activity, delegate: self, viewC: self)

        } else {
            arrayCampaign  = mEventViewModel.getCategory()
            Constants.OptionComboStreet(data: arrayCampaign.map({ $0.descriptionTree }), id: "campaign", header: .typeActivity, delegate: self, viewC: self)
        }
    }

    func onError(errorMessage: String) {

        if  errorMessage == "No se encontraron eventos en planificación" {
            listaEventos = mEventPlanningViewModel.getEvents()
            consultSaveEvent()
        } else {
            Constants.Alert(title: "", body: errorMessage, type: type.Error, viewC: self)
        }
    }
}
