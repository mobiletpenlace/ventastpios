//
//  CategoryEvidenciaView.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 06/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
protocol CategoryDelegate {
    func refreshPage()
}
class CategoryEvidenciaView: BaseVentasView {
    @IBOutlet weak var CategoryTable: UITableView!
    @IBOutlet weak var titleEvent: UILabel!
    @IBOutlet weak var camButton: UIButton!
    var imagePicker: UIImagePickerController!
    var category: [tree] = []
    var imageTake: UIImageView!
    var mEventViewModel: EventViewModel!
    var categoryDelegate: CategoryDelegate!

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mEventViewModel = EventViewModel(view: self)
        mEventViewModel.atachView(observer: self)
        setUpView()
    }

    func setUpView() {
        titleEvent.text = ""
        camButton.isHidden = mEventViewModel.getEmploye()?.profileUser  == "3" ? true : false
          consultCategory()
    }

    @IBAction func BackAction(_ sender: Any) {
        Constants.Back(viewC: self)
        categoryDelegate.refreshPage()
    }

    @IBAction func takePhotoAction(_ sender: Any) {
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        present(imagePicker, animated: true, completion: nil)
    }

    func consultCategory() {
        guard let id = TPCipher.shared.decode(coded: mEventViewModel.getDetailEvent()?.IdActivity ?? "") else {return}
        mEventViewModel.ConsultCategoryTree(idGeneric: id)
    }

    func goPhotoView() {
        let albumView: PhotosViewContainer = UIStoryboard(name: "PhotosViewContainer", bundle: nil).instantiateViewController(withIdentifier: "PhotosViewContainer") as! PhotosViewContainer
        self.present(albumView, animated: false, completion: nil)
    }

    func goSelectCategory(image: UIImage) {
        let selectCategory: DialogCategoryView = UIStoryboard(name: "DialogCategoryView", bundle: nil).instantiateViewController(withIdentifier: "DialogCategoryView") as! DialogCategoryView
        selectCategory.imagePhoto = image
        selectCategory.providesPresentationContextTransitionStyle = true
        selectCategory.definesPresentationContext = true
        selectCategory.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        selectCategory.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(selectCategory, animated: false, completion: nil)
    }

}

extension CategoryEvidenciaView: UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String: Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        picker.dismiss(animated: true, completion: nil)
        goSelectCategory(image: chosenImage)
      //  picker.dismiss(animated:true, completion: nil)
    }

    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {self.dismiss(animated: true, completion: nil)
    }

}

extension CategoryEvidenciaView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  category.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryEvidenciaViewCell", for: indexPath) as! CategoryEvidenciaViewCell
       // TPCipher.shared.setKey(b64Key: Strings.SHA256_KEY)
        cell.selectButton.tag = indexPath.row
        cell.selectButton.addTarget(self, action: #selector(handleButtonTapped(sender: )), for: .touchUpInside)
        cell.name.text = category[indexPath.row].descriptionTree // TPCipher.shared.decode(coded: category[indexPath.row].descriptionTree)

        if indexPath.row < category.count {
            let separatorView = UIView.init(frame: CGRect(x: 15, y: cell.frame.size.height - 1, width: cell.frame.size.width - 15, height: 1))
            separatorView.backgroundColor = .lightGray
            cell.contentView.addSubview(separatorView)
        }
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75

    }

    @objc func handleButtonTapped(sender: UIButton) {
        mEventViewModel.updateIdCategory(id: category[sender.tag].id, title: category[sender.tag].descriptionTree)
        goPhotoView()
    }
}

extension CategoryEvidenciaView: EventObserver {
    func successEditEvent() {}

    func successDeleteEvent() {}

    func successDeleteHostess() {}

    func retryEvent(id: String) { }

    func pickerOptions(value: String, id: String) {}

    func successEvents() { }

    func successDetailEvent() {}

    func successListHostes() {}

    func successAssignedHostes() {}

    func successCategory() {
        category = mEventViewModel.getCategory()
        titleEvent.text = mEventViewModel.getEmploye()?.currentTitleEvent
        CategoryTable.reloadData()
    }

    func onError(errorMessage: String) {
        Constants.Alert(title: "", body: errorMessage, type: type.Error, viewC: self)
    }

}
