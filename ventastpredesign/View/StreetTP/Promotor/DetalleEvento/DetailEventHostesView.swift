//
//  DetailEventHostesView.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 03/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import MapKit

class DetailEventHostesView: BaseVentasView {

    @IBOutlet weak var titleEvent: UILabel!
    @IBOutlet weak var dateEvent: UILabel!
    @IBOutlet weak var typeEvent: UILabel!
    @IBOutlet weak var locationEvent: UILabel!
    @IBOutlet weak var SuperviseButton: UIButton!
    @IBOutlet weak var eventDetailContainerView: UIView!
    @IBOutlet weak var HostessViewTopContraints: NSLayoutConstraint!

    var mEventViewModel: EventViewModel!
    var mCheckInViewModel: CheckInViewModel!
    var hostessAssigned = false
    var latitude = "0.0"
    var lenght = "0.0"

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mEventViewModel = EventViewModel(view: self)
        mEventViewModel.atachView(observer: self)
        mCheckInViewModel = CheckInViewModel(view: self)
        mCheckInViewModel.atachView(observer: self)
        setupView()
    }

    func setupView() {
        clearData()
        consultEventDetail()
        setDataDefault()
    }

    @IBAction func BackAction(_ sender: Any) {
        Constants.Back(viewC: self)
    }

    @IBAction func superviseAction(_ sender: Any) {
        if mEventViewModel.getEmploye()?.profileUser == "3" || hostessAssigned {
            goCategory()
        } else {
            goAlertNoAssignedHostess()
        }
    }

    @IBAction func startNavigationAction(_ sender: Any) {
        mapsGogl()
    }

    func consultEventDetail() {
        mEventViewModel.consultDetailEvent(idEvent: mEventViewModel.getEmploye()?.currentIdEvent ?? "")
    }

    func goCategory() {
        let viewAlertVC: CategoryEvidenciaView = UIStoryboard(name: "CategoryEvidenciaView", bundle: nil).instantiateViewController(withIdentifier: "CategoryEvidenciaView") as! CategoryEvidenciaView
        viewAlertVC.categoryDelegate = self
        self.present(viewAlertVC, animated: false, completion: nil)
    }

    func goAlertNoAssignedHostess() {
        let viewAlertVC: DialogDontHaveHostessView = UIStoryboard(name: "DialogDontHaveHostessView", bundle: nil).instantiateViewController(withIdentifier: "DialogDontHaveHostessView") as! DialogDontHaveHostessView
        viewAlertVC.providesPresentationContextTransitionStyle = true
        viewAlertVC.definesPresentationContext = true
        viewAlertVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        viewAlertVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(viewAlertVC, animated: false, completion: nil)
    }

    func goMessageNoHostess() {
        ViewEmbedder.embed( withIdentifier: "DialogNoHostessAssignedView", parent: self, container: self.eventDetailContainerView)
    }

    func goCheckinHostess() {
     ViewEmbedder.embed( withIdentifier: "DialogCheckinView", parent: self, container: self.eventDetailContainerView)
    }

    func goHostessLeader() {
        ViewEmbedder.embed( withIdentifier: "DialogHostessLiderView", parent: self, container: self.eventDetailContainerView)
    }

    func superviserIsHaveHostess() {
        if mEventViewModel.getHostessEvent().count > 0 {
            hostessAssigned = true
            HostessViewTopContraints.constant = CGFloat(120 + (mEventViewModel.getHostessEvent().count * 25))
            goHostessLeader()
        } else {
            hostessAssigned = false
            HostessViewTopContraints.constant = 130
            goMessageNoHostess()
        }

    }

    func mapsGogl() {
        if UIApplication.shared.canOpenURL(URL(string: "comgooglemaps://")!) {
            UIApplication.shared.open(URL(string: "comgooglemaps://?center=\(self.latitude),\(self.lenght)")!, options: [:], completionHandler: nil)
        } else {
            mapsDefault()
        }
    }

    func mapsDefault() {
        guard let lat = Double(latitude) else {return}
        guard let log = Double(lenght) else {return}
        let coordinate = CLLocationCoordinate2DMake(lat, log)
        let region = MKCoordinateRegion(center: coordinate, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.02))
        let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: region.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: region.span)]
        mapItem.name = locationEvent.text
        mapItem.openInMaps(launchOptions: options)
    }

    func setDataDefault() {
        titleEvent.text = mEventViewModel.getEmploye()?.currentTitleEvent ?? ""
        if mEventViewModel.getEmploye()?.profileUser == "3" {
            SuperviseButton.setTitle("Evidencias", for: .normal)
            validateCheckIn()
        } else {
            SuperviseButton.setTitle("Supervisar", for: .normal)
            HostessViewTopContraints.constant = 130
            mCheckInViewModel.cleanCheck()
        }
    }

    func validateCheckIn() {
        let employe = mEventViewModel.getEmploye()
        if  (employe?.firstIdEvent == employe?.currentIdEvent) || (employe?.lasttIdEvent == employe?.currentIdEvent) {
            HostessViewTopContraints.constant = 200
            mCheckInViewModel.consultCheckIn()
        } else {
            HostessViewTopContraints.constant = -20
            mCheckInViewModel.cleanCheck()
        }
    }

    func clearData() {
        dateEvent.text = "-"
        typeEvent.text = "-"
        locationEvent.text = "-"
    }

    func setDataEventDetail(data: EventoDetail?) {
        guard let event = data else {return}
        dateEvent.text = mEventViewModel.dateEventDetail(startDate: event.dateStar, hourStart: event.startHour, hourFinish: event.endHour)
        locationEvent.text = TPCipher.shared.decode(coded: event.descriptionEvent) != "" ? "Ubicación seleccionada" : TPCipher.shared.decode(coded: event.descriptionEvent)
        typeEvent.text = TPCipher.shared.decode(coded: event.typeActivity)
        latitude = TPCipher.shared.decode(coded: event.latitude) ?? "0.0"
        lenght = TPCipher.shared.decode(coded: event.length) ?? "0.0"
        mEventViewModel.updateLocationEvent(latitude: latitude, longitude: lenght)
        mEventViewModel.validateDistanceBetweenEventUser()
        validateisSuperviser()
    }

    func validateisSuperviser() {
        if mEventViewModel.getEmploye()?.profileUser != "3" {
            superviserIsHaveHostess()
        }
    }

}

extension DetailEventHostesView: EventObserver {
    func successDeleteEvent() {}
    func successEditEvent() {}
    func successDeleteHostess() {}

    func retryEvent(id: String) {
        if id == "eventDetail" {
            consultEventDetail()
        }
    }

    func pickerOptions(value: String, id: String) {}

    func successEvents() {}

    func successDetailEvent() {
        let eventDetail = mEventViewModel.getDetailEvent()
        setDataEventDetail(data: eventDetail)
    }

    func successListHostes() {}

    func successAssignedHostes() {}

    func successCategory() {}

    func onError(errorMessage: String) {
        Constants.Alert(title: "", body: errorMessage, type: type.Error, viewC: self)
    }

}

extension DetailEventHostesView: CheckInObserver {

    func successReasonMissing() {}

    func successConsultCheck() {
        goCheckinHostess()
    }

    func successUpdateCheck() {}
}

extension DetailEventHostesView: CategoryDelegate {
    func refreshPage() {
        setupView()
    }
}
