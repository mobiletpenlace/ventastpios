//
//  PhotoViewCell.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 06/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

class PhotoViewCell: UICollectionViewCell {

    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var hour: UILabel!
    @IBOutlet weak var nameUser: UILabel!
    @IBOutlet weak var zoomPhoto: UIButton!

}
