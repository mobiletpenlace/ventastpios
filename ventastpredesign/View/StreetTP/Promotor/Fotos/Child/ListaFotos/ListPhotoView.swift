//
//  ListPhotoView.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 06/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

class ListPhotoView: BaseVentasView {
    @IBOutlet weak var photosViewCollection: UICollectionView!
    var photos: [photos] = []
    var mAlbumViewModel: AlbumPhotosViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mAlbumViewModel = AlbumPhotosViewModel(view: self)
        setUpView()
    }

    func setUpView() {
        photos = mAlbumViewModel.getPhotos()
        photosViewCollection.reloadData()
    }

    func goZoomPhoto(url: String) {
        let photoZoom: PhotoZoomView = UIStoryboard(name: "PhotoZoomView", bundle: nil).instantiateViewController(withIdentifier: "PhotoZoomView") as! PhotoZoomView
        photoZoom.url = url
        self.present(photoZoom, animated: false, completion: nil)
    }

}

extension ListPhotoView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoViewCell", for: indexPath) as! PhotoViewCell
        cell.nameUser.text = TPCipher.shared.decode(coded: photos[indexPath.row].nameUser)
        cell.hour.text = TPCipher.shared.decode(coded: photos[indexPath.row].hour)
        cell.zoomPhoto.tag = indexPath.row
        cell.zoomPhoto.addTarget(self, action: #selector(handleButtonTapped(sender: )), for: .touchUpInside)
        if let url = URL(string: TPCipher.shared.decode(coded: photos[indexPath.row].url)!) {
            cell.photo.downloaded(from: url)
        }
        return cell
    }

    @objc func handleButtonTapped(sender: UIButton) {
        goZoomPhoto(url: TPCipher.shared.decode(coded: photos[sender.tag].url) ?? "")
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if (mAlbumViewModel.getEmploye()?.profileUser ?? "") != "3" {
            return CGSize(width: (self.view.size.width/2)-10, height: 192)

        } else {
            return CGSize(width: self.view.size.width, height: self.view.size.height )
        }
    }

}
