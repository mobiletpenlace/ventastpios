//
//  PhotosViewContainer.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 06/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

class PhotosViewContainer: BaseVentasView {

    @IBOutlet weak var categoryPhotoContainerView: UIView!
    @IBOutlet weak var camButton: UIButton!
    @IBOutlet weak var titleCategory: UILabel!
    var imagePicker: UIImagePickerController!
    var mAlbumViewModel: AlbumPhotosViewModel!
    var isConsultPhoto = true

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mAlbumViewModel = AlbumPhotosViewModel(view: self)
        mAlbumViewModel.atachView(observer: self)
        setUpView()
    }

    func setUpView() {
        titleCategory.text = mAlbumViewModel.getEmploye()?.currentTitleCategory ?? ""
        consultPhoto()
    }

    @IBAction func BackAction(_ sender: Any) {
        Constants.Back(viewC: self)
    }

    @IBAction func takePhotoAction(_ sender: Any) {
        let employee = mAlbumViewModel.getEmploye()
        let titleEvent = employee?.currentTitleEvent
        let titleCategory = employee?.currentTitleCategory
        let distance = employee?.distanceUserToEvent ?? 0

        if distance < 401 || (titleEvent == "VOLANTEO MASIVO" && titleCategory != "Check in") {
            openCamera()
        } else {
            validateLocation()
        }
    }

    func validateLocation() {
        if mAlbumViewModel.getDetailEvent()?.latitudeDouble != 0.0 {
            if mAlbumViewModel.getEmploye()?.currentLatitude == 0.00 {
                Constants.Alert(title: "", body: "No es posible tomar fotos si la aplicación no tiene permisos para obtener su ubicación", type: type.Error, viewC: self)
            } else {
                goLocationOut()
            }
        } else {
            Constants.Alert(title: "", body: "No es posible hacer check si las coordenadas del evento están vacias.", type: type.Error, viewC: self)
        }
    }

    func openCamera() {
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
            present(imagePicker, animated: true, completion: nil)
    }

    func goLocationOut() {
        let viewAlertVC: DialogLocationView = UIStoryboard(name: "DialogLocationView", bundle: nil).instantiateViewController(withIdentifier: "DialogLocationView") as! DialogLocationView
        viewAlertVC.providesPresentationContextTransitionStyle = true
        viewAlertVC.definesPresentationContext = true
        viewAlertVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        viewAlertVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(viewAlertVC, animated: false, completion: nil)
    }

    func consultPhoto() {
        isConsultPhoto = true
        mAlbumViewModel.ConsultPhotos()
    }

    func showPictures() {
        ViewEmbedder.embed(withIdentifier: "ListPhotoView", parent: self, container: categoryPhotoContainerView)
    }

    func showNoPictures() {
        ViewEmbedder.embed(withIdentifier: "NoPhotoView", parent: self, container: categoryPhotoContainerView)
    }

    func activeCamButton(withPhotos: Bool) {
        camButton.isEnabled = (withPhotos && (mAlbumViewModel.getEmploye()?.profileUser ?? "") == "3") ? false : true
    }

}

extension PhotosViewContainer: AlbumPhotoObserver {
    func successDownload() {
        activeCamButton(withPhotos: true)
        showPictures()
    }

    func successUpLoad() {
        setUpView()
    }

    func onError(errorMessage: String) {
        if isConsultPhoto {
            activeCamButton(withPhotos: false)
            showNoPictures()
            if errorMessage != "No se encontraron fotos" && errorMessage != "No results were found for your search." {
                Constants.Alert(title: "", body: errorMessage, type: type.Error, viewC: self)
            }
        } else {
            Constants.Alert(title: "", body: errorMessage, type: type.Error, viewC: self)
        }
    }

}

extension PhotosViewContainer: UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String: Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        guard let imageData = UIImageJPEGRepresentation(chosenImage, 0.6) else { return }
        isConsultPhoto = false
        mAlbumViewModel.uploadProfileImage(imageData: imageData)
        picker.dismiss(animated: true, completion: nil)
    }

    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {self.dismiss(animated: true, completion: nil)
    }

}
