//
//  EventTableViewCell.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 03/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

class EventTableViewCell: UITableViewCell {
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var numberDayLabel: UILabel!
    @IBOutlet weak var hourStartEndLabel: UILabel!
    @IBOutlet weak var folioLabel: UILabel!

    @IBOutlet weak var nameEventLabel: UILabel!
    @IBOutlet weak var dayWeekLabel: UILabel!
    @IBOutlet weak var barColorView: UIView!
    @IBOutlet weak var selectButton: UIButton!
    var idEvent: String!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
