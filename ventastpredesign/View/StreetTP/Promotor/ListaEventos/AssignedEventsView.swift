//
//  AssignedEventsView.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 03/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

class AssignedEventsView: BaseVentasView {
    @IBOutlet weak var calendarTable: UITableView!
    var mEventViewModel: EventViewModel!
    var events: [Events] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mEventViewModel = EventViewModel(view: self)
        mEventViewModel.atachView(observer: self)

    }

    override func viewDidAppear(_ animated: Bool) {
        events = mEventViewModel.getEvents()
        calendarTable.reloadData()
    }

    func goEventDetail() {
        Constants.LoadStoryBoard(name: storyBoard.Street.detailEvent, viewC: self)
    }

}

extension AssignedEventsView: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventTableViewCell", for: indexPath) as! EventTableViewCell
        cell.barColorView.backgroundColor = mEventViewModel.getColor(position: events[indexPath.row].indexColor)
        cell.folioLabel.text = "Folio #" + (TPCipher.shared.decode(coded: events[indexPath.row].idEvent) ?? "-")
        cell.dateView.isHidden = events[indexPath.row].showDate
        cell.numberDayLabel.text = events[indexPath.row].numberDay
        cell.dayWeekLabel.text =  events[indexPath.row].nameDay
        cell.selectButton.tag = indexPath.row
        cell.idEvent = TPCipher.shared.decode(coded: events[indexPath.row].idEvent)
        cell.selectButton.addTarget(self, action: #selector(handleButtonTapped(sender: )), for: .touchUpInside)
        cell.nameEventLabel.text =  TPCipher.shared.decode(coded: events[indexPath.row].Title)
        cell.hourStartEndLabel.text =  mEventViewModel.hourEventFormat(hour: TPCipher.shared.decode(coded: events[indexPath.row].startHour)!) + " - " +   mEventViewModel.hourEventFormat(hour: TPCipher.shared.decode(coded: events[indexPath.row].endHour)!)// mEventViewModel.hourAMPM(hourStart: events[indexPath.row].startHour, hourFinish: events[indexPath.row].endHour)
        return cell
    }

    @objc func handleButtonTapped(sender: UIButton) {
        let selectedIndex = IndexPath(row: sender.tag, section: 0)
        calendarTable.selectRow(at: selectedIndex, animated: true, scrollPosition: .none)
        mEventViewModel.updateIdEvent(idEvent: TPCipher.shared.decode(coded: events[sender.tag].idEvent)!, title: TPCipher.shared.decode(coded: events[sender.tag].Title) ?? "")
        goEventDetail()
    }

}

extension AssignedEventsView: EventObserver {
    func successEditEvent() {}

    func successDeleteHostess() {}
    func successDeleteEvent() { }

    func retryEvent(id: String) {}

    func pickerOptions(value: String, id: String) {}

    func successEvents() {}

    func successDetailEvent() {}

    func successListHostes() {}

    func successAssignedHostes() {}

    func successCategory() {}

    func onError(errorMessage: String) {}

}
