//
//  NoAssignedEventsView.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 16/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus
import Amplitude

class NoAssignedEventsView: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func retryAction(_ sender: Any) {
        SwiftEventBus.post("RetryEventHostess", sender: nil)
        Amplitude.instance().logEvent("Action_EventsScreenReload_Button")
    }

}
