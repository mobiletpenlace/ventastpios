//
//  DialogCheckinView.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 05/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

class DialogCheckinView: BaseVentasView {
    @IBOutlet weak var titleCheck: UILabel!
    @IBOutlet weak var Message: UILabel!
    @IBOutlet weak var CheckButton: UIButton!
    @IBOutlet weak var titleButton: UILabel!
    @IBOutlet weak var viewButton: UIView!
    var mCheckInViewModel: CheckInViewModel!
    var typeCheck = "0"
    var checkIn = "0"
    var checkOut = "0"
    var titleEvent = ""
    var isReasonMissing = "0"

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mCheckInViewModel = CheckInViewModel(view: self)
        mCheckInViewModel.atachView(observer: self)
    }

    override func viewDidAppear(_ animated: Bool) {
        setUpView()
    }

    func setUpView() {
        let employee = mCheckInViewModel.getEmploye()
        typeCheck = employee?.typeCheck ?? "0"
        checkIn = employee?.checkIn ?? "0"
        checkOut = employee?.checkOut ?? "0"
        titleEvent = employee?.currentTitleEvent ?? "-"
        isReasonMissing = employee?.isReasonMissing ?? "0"
        setDefaultData()
    }

    @IBAction func doCheckInOutAction(_ sender: Any) {
        doCheckInOut()
    }

    func goOutLocation() {
        let viewAlertVC: DialogLocationView = UIStoryboard(name: "DialogLocationView", bundle: nil).instantiateViewController(withIdentifier: "DialogLocationView") as! DialogLocationView
        viewAlertVC.providesPresentationContextTransitionStyle = true
        viewAlertVC.definesPresentationContext = true
        viewAlertVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        viewAlertVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(viewAlertVC, animated: false, completion: nil)
    }

    func updateCheckIn() {
        mCheckInViewModel.updateCheckin()
    }

    func doCheckInOut() {
        if validateFirstCheckIn() {
            if validateLocation() {
                updateCheckIn()
            }
        } else {
            Constants.Alert(title: "", body: "No tenemos registro que hayas hecho Check In", type: type.Warning, viewC: self)
        }
    }

    func validateFirstCheckIn() -> Bool {
        let result = (typeCheck == "47" ||  (typeCheck == "48" && checkIn == "1")) ? true : false
        return result
    }

    func validateLocation() -> Bool {
        if mCheckInViewModel.getEmploye()?.currentTitleEvent == "VOLANTEO MASIVO" {return true} else {
            if mCheckInViewModel.getDetailEvent()?.latitudeDouble != 0.0 {
                if mCheckInViewModel.getEmploye()?.distanceUserToEvent ?? 0 < 401 { return true } else {
                    if mCheckInViewModel.getEmploye()?.currentLatitude == 0 {
                        Constants.Alert(title: "", body: "No es posible hacer check si la aplicación no tiene permisos para obtener su ubicación", type: type.Warning, viewC: self)
                    } else {
                        goOutLocation()
                    }
                }
            } else {
                Constants.Alert(title: "", body: "No es posible hacer check si las coordenadas del evento están vacias", type: type.Warning, viewC: self)
            }
        }
        return false
    }

    func setDefaultData() {
        if typeCheck == "47" {
            titleCheck.text = "Checkin del día"
            Message.text = "Realiza el primer checkin del día cuando te encuentres en el lugar de la activación."
            if isReasonMissing == "1" {
                reasonMissingButton()
            } else {
                checkIn == "0" ? OFFCheckInButton() : ONCheckInButton()
            }
        } else {
            titleCheck.text = "Checkout del día"
            Message.text = titleEvent == "VOLANTEO MASIVO" ? "Realiza el checkout del día.": "Realiza el checkout del día cuando te encuentres en el lugar de la activación."
            if isReasonMissing == "1" {
                reasonMissingButton()
            } else {
                checkOut == "0" ? OFFCheckOutButton(): ONCheckOutButton()
            }
        }
    }

    func reasonMissingButton() {
        CheckButton.isEnabled = false
        titleButton.text = "Falta justificada"
        viewButton.backgroundColor = UIColor.lightGray
    }

    func ONCheckInButton() {
        CheckButton.isEnabled = false
        titleButton.text = "Checkin realizado"
        viewButton.backgroundColor = UIColor.lightGray
    }

    func ONCheckOutButton() {
        CheckButton.isEnabled = false
        titleButton.text = "Checkout realizado"
        viewButton.backgroundColor = UIColor.lightGray
    }

    func OFFCheckInButton() {
        CheckButton.isEnabled = true
        titleButton.text = "Realizar checkin"
        viewButton.backgroundColor = Colors.Base_purple_names
    }

    func OFFCheckOutButton() {
        CheckButton.isEnabled = true
        titleButton.text = "Realizar checkout"
        viewButton.backgroundColor = Colors.Base_purple_names
    }

}

extension  DialogCheckinView: CheckInObserver {
    func successReasonMissing() {}

    func successConsultCheck() {}

    func successUpdateCheck() {
        if typeCheck == "47" {
            ONCheckInButton()
        } else if typeCheck == "48" {
            ONCheckOutButton()
        }
    }

    func onError(errorMessage: String) {
        Constants.Alert(title: "", body: errorMessage, type: type.Error, viewC: self)
    }

}
