//
//  DialogCategoryView.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 06/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

class DialogCategoryView: BaseVentasView {

    @IBOutlet weak var selectCategoryViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var selectCategoryView: UIView!
    @IBOutlet weak var selectCategoryTable: UITableView!
    var mEventViewModel: EventViewModel!
    var mAlbumViewModel: AlbumPhotosViewModel!
    var imagePhoto: UIImage?
    var category: [tree] = []
    var isSelect: [Int] = []
    var arrButton: [RadioButton] = []
    var currentCategory = ""
    var currentId = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mEventViewModel = EventViewModel(view: self)
        mAlbumViewModel = AlbumPhotosViewModel(view: self)
        mAlbumViewModel.atachView(observer: self)

        setUpView()
    }

    func setUpView() {
        category = mEventViewModel.getCategory()
        designView(num: category.count)
        selectCategoryTable.reloadData()
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first as! UITouch
        if touch.view != selectCategoryView {
            self.dismiss(animated: false, completion: nil)
        }
    }

    @IBAction func AcceptAction(_ sender: Any) {
        category = mEventViewModel.getCategory()
        if isSelect.contains(1) {
            mEventViewModel.updateIdCategory(id: currentId, title: currentCategory)
            setPhoto()
        } else {
            Constants.Alert(title: "", body: "Debes seleccionar por lo menos una opción", type: type.Warning, viewC: self)
        }
    }

    func setPhoto() {
        guard let imageData = imagePhoto else {return}
        guard let imageJPG = UIImageJPEGRepresentation(imageData, 0.6) else {return}
        mAlbumViewModel.uploadProfileImage(imageData: imageJPG)
    }

    func designView(num: Int) {
        selectCategoryViewTopConstraint.constant = num > 5 ? 260 : (CGFloat(125 + (40 * num)))
    }

//    func radioButton(isSelect: String, button: UIButton){
//        let checkedImage = UIImage(named: "icon_radiobutton_on")! as UIImage
//        let uncheckedImage = UIImage(named: "icon_radiobutton_off")! as UIImage
//        if (isSelect == "0"){
//            button.setImage(uncheckedImage, for: UIControl.State.normal)
//        }else{
//            button.setImage(checkedImage, for: UIControl.State.normal)
//        }
//    }

}

extension DialogCategoryView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return category.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectOptionViewCell", for: indexPath) as! SelectOptionViewCell
        isSelect.append(0)
        arrButton.append(cell.selectButton)
        // radioButton(isSelect: category[indexPath.row].isSelect, button: cell.selectButton)
        cell.name.text =  category[indexPath.row].descriptionTree
        cell.selectButton.tag = indexPath.row
        cell.selectButton.addTarget(self, action: #selector(handleButtonTapped(sender: )), for: .touchUpInside)

        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }

    @objc func handleButtonTapped(sender: UIButton) {
        for  (index, _) in isSelect.enumerated() {
            if index == sender.tag {
                if isSelect[sender.tag] == 0 {
                    isSelect[sender.tag] = 1
                    currentId = category[sender.tag].id
                    currentCategory = category[sender.tag].descriptionTree
                    arrButton[sender.tag].isChecked = true
                } else {
                    isSelect[sender.tag] = 0
                    arrButton[sender.tag].isChecked = false
                }
            } else {
                isSelect[index] = 0
                arrButton[index].isChecked = false
            }
        }

    }

}

extension DialogCategoryView: AlbumPhotoObserver {
    func successDownload() {

    }

    func successUpLoad() {
        Constants.Back(viewC: self)
    }

    func onError(errorMessage: String) {
        Constants.Alert(title: "", body: errorMessage, type: type.Error, viewC: self)
    }

}
