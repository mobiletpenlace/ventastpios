//
//  DialogLocationView.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 05/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import MapKit

class DialogLocationView: BaseVentasView, CLLocationManagerDelegate, MKMapViewDelegate {

    @IBOutlet weak var TitleLabel: UILabel!
    @IBOutlet var mapKitView: MKMapView!
    var locationManager: CLLocationManager = CLLocationManager()
    var mCheckInViewModel: CheckInViewModel!
    var mAlbumViewModel: AlbumPhotosViewModel!
    var circle: MKCircle!
    var message = ""
    var latitudEvent = 0.0
    var longitudEvent = 0.0
    var latitudUser = 0.0
    var longitudUser = 0.0

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mCheckInViewModel = CheckInViewModel(view: self)
        mAlbumViewModel = AlbumPhotosViewModel(view: self)
        configureInit()
    }

    func configureInit() {
        latitudEvent = mAlbumViewModel.getDetailEvent()?.latitudeDouble ?? 0
        longitudEvent = mAlbumViewModel.getDetailEvent()?.longitudDouble ?? 0
        latitudUser = mCheckInViewModel.getEmploye()?.currentLatitude ?? 0
        longitudUser = mCheckInViewModel.getEmploye()?.currentLongitude ?? 0
        TitleLabel.text = "No es posible hacer check si su ubicación no se encuentra cerca del evento"
        mapKitView.delegate = self
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        designMap()
    }

    func designMap() {
        let location = CLLocationCoordinate2DMake(latitudEvent, longitudEvent)
        let locationUSer = CLLocationCoordinate2DMake(latitudUser, longitudUser)
        addCircleRadio(location: location)
        addAnotation(coordinate: location, title: "Evento")
        addAnotation(coordinate: locationUSer, title: "Tu ubicación")
        mapKitView.showAnnotations(mapKitView.annotations, animated: false)
    }

    @IBAction func acceptAction(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }

    func addPinLocationEvent(location: CLLocationCoordinate2D) {
        let dropPin = MKPointAnnotation()
        dropPin.coordinate = location
        dropPin.title = "Evento"
        self.mapKitView.addAnnotation(dropPin)
    }

    func addCircleRadio(location: CLLocationCoordinate2D) {
        let region = MKCoordinateRegionMakeWithDistance(location, 1400.0, 1400.0)
        self.mapKitView.setRegion(region, animated: true)
        circle = MKCircle(center: location, radius: 400)
        self.mapKitView.add(circle)
    }

    func addAnotation(coordinate: CLLocationCoordinate2D, title: String = "") {
        var pointAnnotation: MKPointAnnotation!
        var pinAnnotationView: MKPinAnnotationView!
        pointAnnotation = MKPointAnnotation()
        pointAnnotation.coordinate = coordinate
        pointAnnotation.title = title
        pinAnnotationView = MKPinAnnotationView(annotation: pointAnnotation, reuseIdentifier: nil)
        mapKitView.centerCoordinate = pointAnnotation.coordinate
        mapKitView.addAnnotation(pinAnnotationView.annotation!)
    }

    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if let overlay = overlay as? MKCircle {
            let circleRenderer = MKCircleRenderer(circle: overlay)
            circleRenderer.fillColor = UIColor.init(red: 182, green: 219, blue: 253)
            circleRenderer.lineWidth = 2
            circleRenderer.strokeColor = UIColor.init(red: 46, green: 154, blue: 254)
            circleRenderer.alpha = 0.7
            return circleRenderer
        } else {
            return MKOverlayRenderer(overlay: overlay)
        }
    }

}
