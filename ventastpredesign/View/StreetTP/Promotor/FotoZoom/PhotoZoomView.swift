//
//  PhotoZoomView.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 06/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

class PhotoZoomView: UIViewController {
    @IBOutlet weak var photo: UIImageView!
    var imageTake: UIImage!
    var url: String?

    override func viewDidLoad() {
        super.viewDidLoad()
       getPhoto()
    }

    @IBAction func BackAction(_ sender: Any) {
        Constants.Back(viewC: self)
      }

    func getPhoto() {
        if let url = URL(string: url ?? "") {
            photo.downloaded(from: url)
        } else {
            photo.image = imageTake
        }
    }

}
