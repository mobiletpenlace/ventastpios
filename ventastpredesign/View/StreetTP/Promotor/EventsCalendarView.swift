//
//  EventsCalendarView.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 03/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import Amplitude
// import CoreLocation

class EventsCalendarView: BaseVentasView {
    @IBOutlet weak var eventsContainerView: UIView!
    @IBOutlet weak var weekTextfield: UITextField!

    var mEventViewModel: EventViewModel!
   // let locationManager = CLLocationManager()
    var positionPicker: Int = 0
    var eventCalendar: EventCalendar!
    var pickerSelectWeek: String = "1 semana"

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mEventViewModel = EventViewModel(view: self)
        mEventViewModel.atachView(observer: self)
        setUpView()
    }

    override func viewDidAppear(_ animated: Bool) {
        Amplitude.instance().logEvent("Show_EventsScreen")
    }

    func setUpView() {
     //   configLocation()
        validateFirebase()
        mEventViewModel.initEventCalendar()
        getEventCalendar()
    }

    func getEventCalendar() {
        guard let calendar = mEventViewModel.getEventCalendar() else {return }
        eventCalendar = calendar
        positionPicker = calendar.positionPicker
        weekTextfield.text = calendar.week
        pickerSelectWeek = calendar.week
        consultEvent()
    }

    func validateFirebase() {
        mEventViewModel.authUser(customToken: mEventViewModel.getEmploye()?.tokenFirebase ?? "")
        if  mEventViewModel.getEmploye()?.connectFirebase == "0" {
            Constants.Alert(title: "", body: "No se pudo conectar a la base de datos de Firebase", type: type.Warning, viewC: self)
        }
    }

    func goAssignedEvents() {
        ViewEmbedder.embed(withIdentifier: "AssignedEventsView", parent: self, container: self.eventsContainerView) { _ in }
    }

    func goNoAssignedEvents() {
        ViewEmbedder.embed(withIdentifier: "NoAssignedEventsView", parent: self, container: self.eventsContainerView) { _ in }
    }

    func consultEvent() {
        mEventViewModel.isEventQuality = false
        let startDate = mEventViewModel.dateToday()
        let endDate = mEventViewModel.futureDate(startDate: startDate, days: self.mEventViewModel.numDays[self.positionPicker])
        let idUser = mEventViewModel.getEmploye()?.userId ?? ""
        mEventViewModel.consultListEvents(startDate: startDate, endDate: endDate, idUser: idUser)
    }

    @IBAction func backAction(_ sender: Any) {
        Constants.Back(viewC: self)
        Amplitude.instance().logEvent("Action_EventsScreenBack_Button")
    }

    @IBAction func ChangePeriodAction(_ sender: Any) {
        Constants.OptionCombo(origen: "Week", array: mEventViewModel.numWeek, viewC: self)
        self.positionPicker = 0
        Amplitude.instance().logEvent("Action_EventsScreenChangeDates_Button")
    }

}

extension EventsCalendarView: EventObserver {
    func successEditEvent() {}

    func successDeleteHostess() {}

    func successDeleteEvent() {}

    func retryEvent(id: String) {
        if id == "eventHostess" {
            getEventCalendar()
        }
    }
    func pickerOptions(value: String, id: String) {
        weekTextfield.text = value
        self.positionPicker = mEventViewModel.getPositionWeekSelect(value: value)
        if self.positionPicker != self.eventCalendar.positionPicker {
            mEventViewModel.updateWeekDay(numberDay: mEventViewModel.numDays[self.positionPicker], week: value, positionPicker: self.positionPicker)
            self.getEventCalendar()
        }
    }

    func successEvents() {
        goAssignedEvents()
    }

    func successDetailEvent() { }

    func successListHostes() { }

    func successAssignedHostes() { }

    func successCategory() { }

    func onError(errorMessage: String) {
        goNoAssignedEvents()
    }

}
