//
//  ResponseSaveInformation.swift
//  SalesCloud
//
//  Created by Hector Alfonso Moreno Santa Cruz on 02/06/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import ObjectMapper

class ResponseSaveInformation: NSObject, Mappable {

    required init(map: Map) {
        super.init()
    }
    func mapping(map: Map) {

        resultDescription <- map["resultDescription"]
        result <- map["result"]
        numEmpleado <- map["numEmpleado"]
        info <- map["info"]
        idRegistro <- map["idRegistro"]
    }

    @objc dynamic var resultDescription: String?
    @objc dynamic var result: String?
    @objc dynamic var numEmpleado: Int = 0
    @objc dynamic var info: Any?
    @objc dynamic var idRegistro: String?
}
