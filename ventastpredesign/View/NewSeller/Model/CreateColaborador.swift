//
//  CreateColaborador.swift
//  SalesCloud
//
//  Created by Hector Alfonso Moreno Santa Cruz on 01/06/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import ObjectMapper

class CreateColaborador: NSObject, Mappable {

    required init(map: Map) {
        super.init()
    }

    func mapping(map: Map) {
        accion <- map["accion"]
        infoEmpleado <- map["infoEmpleado"]
    }

    @objc dynamic var accion: String?
    @objc dynamic var infoEmpleado: SendInfoEmpleado?
}

class SendInfoEmpleado: NSObject, Mappable {

    required init(map: Map) {
        super.init()
    }

    func mapping(map: Map) {
         AceptaTerminosyCondiciones <- map["AceptaTerminosyCondiciones"]
         claveInterbancaria <- map["claveInterbancaria"]
         unidadNegocio <- map["unidadNegocio"]
         celular <- map["celular"]
         canal <- map["canal"]
         pais <- map["pais"]
         fechaNacimiento <- map["fechaNacimiento"]
         idJefePlaza <- map["idJefePlaza"]
         correo <- map["correo"]
         nombre <- map["nombre"]
         AceptaPagos <- map["AceptaPagos"]
         fotografia <- map["fotografia"]
         posicion <- map["posicion"]
         ranking <- map["ranking"]
    }

    @objc dynamic var AceptaTerminosyCondiciones: Bool = false
    @objc dynamic var claveInterbancaria: String?
    @objc dynamic var unidadNegocio: String?
    @objc dynamic var celular: String?
    @objc dynamic var canal: String?
    @objc dynamic var pais: String?
    @objc dynamic var fechaNacimiento: String?
    @objc dynamic var idJefePlaza: String?
    @objc dynamic var correo: String?
    @objc dynamic var nombre: String?
    @objc dynamic var AceptaPagos: Bool = false
    @objc dynamic var fotografia: String?
    @objc dynamic var posicion: String?
    @objc dynamic var ranking: String?

}

class InfoEmpleadoDocuments: NSObject, Mappable {
    required init(map: Map) {}

    func mapping(map: Map) {
        ineReversa <- map["ineReversa"]
        ineFrontal <- map["ineFrontal"]
        fotografia <- map["fotografia"]
        id <- map["id"]
    }
    @objc dynamic var ineReversa: String? // base64
    @objc dynamic var ineFrontal: String? // base64
    @objc dynamic var fotografia: String? // base64
    @objc dynamic var id: String? // id
}
