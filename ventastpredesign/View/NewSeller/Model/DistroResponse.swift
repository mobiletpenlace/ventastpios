//
//  DistroResponse.swift
//  SalesCloud
//
//  Created by Hector Alfonso Moreno Santa Cruz on 12/06/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import ObjectMapper
import UIKit

class DistroResponse: NSObject, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        name <- map["Nombre"]
        plaza <- map["plaza"]
        region <- map["Region"]
        idDistrito <- map["id"]
    }

    @objc dynamic var name: String?
    @objc dynamic var region: String?
    @objc dynamic var plaza: PlazaDistro?
    @objc dynamic var idDistrito: Int = 0

}

class PlazaDistro: NSObject, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {

        namePlaza  <- map["NombrePlaza"]
        plazaInt <- map["plaza"]

    }

    @objc dynamic var namePlaza: String?
    @objc dynamic var plazaInt: Int = 0

}

// {
// Nombre = COACALCO;
// Region = "Metro Nor Oriente";
// "created_at" = "2023-06-08T17:54:13.286Z";
// id = 1;
// "numero_distrito" = 1;
// plaza = {
//    NombrePlaza = "CIUDAD DE MEXICO";
//    "created_at" = "2022-03-08T17:24:17.984Z";
//    id = 8;
//    plaza = 106;
//    "updated_at" = "2022-03-08T17:24:17.984Z";
// };
// "updated_at" = "2023-06-08T17:57:46.691Z";
// }
