//
//  ResponseNewSeller.swift
//  SalesCloud
//
//  Created by Hector Alfonso Moreno Santa Cruz on 18/05/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

class ResponseNewSeller: Object, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {

        fullName <- map["fullName"]
        address <- map["address"]
        dateOfBirth <- map["dateOfBirth"]
        message <- map["message"]
    }

    @objc dynamic var fullName: String = ""
    @objc dynamic var address: String = ""
    @objc dynamic var dateOfBirth: String = ""
    @objc dynamic var message: String = ""

}
