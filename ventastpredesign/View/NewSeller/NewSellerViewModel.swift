//
//  NewSellerViewModel.swift
//  SalesCloud
//
//  Created by Hector Alfonso Moreno Santa Cruz on 01/06/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import ObjectMapper
import SwiftEventBus
import UIKit

class NewSellerViewModel {

    func downloadOCRAndInformation(token: String, id: String, completion: @escaping (ResponsePersonalData) -> Void) {
        let headers = ["Accept": "application/json; charset=UTF-8",
                       "Content-Type": "application/json; charset=UTF-8",
                       "Authorization": "Bearer " + token]
        DispatchQueue.main.async {
            let url = AppDelegate.API_SYSTEMSTP + ApiDefinition.API_COMPLETION + id
            Alamofire.request(url, method: .get, headers: headers).responseJSON { [weak self] responseJson in
                switch responseJson.result {
                case .success( let value):
                    let json = JSON(value)
                    completion(ResponsePersonalData(JSONString: json.rawString()!)!)
                case .failure(let error):
                    print(error)
                }
            }
        }
    }

    func putAndSaveImages(responsePersonalData: ResponsePersonalData) -> [String: Any] {

        var valuesPost: [String: Any] = [:]
        responsePersonalData.data?.steps?.forEach({ steps in
            if  steps.data?.selfiePhotoUrl != nil {
                let dataImage = try! NSData(contentsOf: URL(string: (steps.data?.selfiePhotoUrl)!)!) as! Data
                let compressionImage: UIImage = UIImage(data: dataImage)!
                let imageCompress = UIImageJPEGRepresentation(compressionImage, 0.3)
                valuesPost.updateValue(imageCompress!.base64EncodedString(options: .lineLength64Characters), forKey: "fotografia")
            }
        })
        var status = "frontal"
        responsePersonalData.data!.documents?.forEach({ documentos in
            if documentos.photos != nil {
                documentos.photos.forEach { photos in
                    let data = try! NSData(contentsOf: URL(string: photos)!) as! Data
                    let uiimage: UIImage = UIImage(data: data)!
                    let imageData = UIImageJPEGRepresentation(uiimage, 0.3)
                    if status == "frontal" {
                        status = "reverso"
                        valuesPost.updateValue(imageData!.base64EncodedString(options: .lineLength64Characters), forKey: "ineFrontal")
                    } else if status == "reverso" {
                        valuesPost.updateValue(imageData!.base64EncodedString(options: .lineLength64Characters), forKey: "ineReversa")
                    }
                }
            }
        })
        return valuesPost
    }

    func getPlazas(completion: @escaping ([SquareModel]) -> Void) {
        let url = ApiDefinition.API_PLAZAS
        Alamofire.request(url, method: .get).responseJSON { response in
            switch response.result {
            case .success:
                let mapper: [SquareModel] = Mapper<SquareModel>().mapArray(JSONObject: response.result.value)!
                completion(mapper)
            case .failure(let error):
                print(error)
            }
        }
    }

    func getDistritos(completion: @escaping([DistroResponse]) -> Void) {

        let url = ApiDefinition.API_GETDISTRO
        Alamofire.request(url, method: .get).responseJSON { response in
            switch response.result {
            case .success:
                let object: [DistroResponse] = Mapper<DistroResponse>().mapArray(JSONObject: response.result.value)!
                completion(object)
            case .failure(let error):
                print("this is distro error \(error)")
            }
        }
    }

    func  putImageView(datas: ResponsePersonalData) -> UIImage {
        var urlImage: String =  ""
         datas.data?.steps?.forEach({ index in
            if index.data?.selfiePhotoUrl != nil {
                urlImage = (index.data?.selfiePhotoUrl)!
            }
        })
        let url: URL = URL(string: urlImage)!
        return UIImage(data: try! NSData(contentsOf: url) as Data)!
    }

    func sendInformation(completion: @escaping (ResponseSaveInformation) -> Void) {
        let creationUser: Parameters = [
            "accion": "Alta",
            "infoEmpleado": NewSellerView.colaboradorFull]
        let url = AppDelegate.API_SALESFORCE + ApiDefinition.API_CONSULTA_CREACION_EMPLEADO
        Alamofire.request(url, method: .post, parameters: creationUser,
                          encoding: JSONEncoding.default,
                          headers: ApiDefinition.headersToken).responseJSON { response in

            switch response.result {
            case .success(let value):
                let json = JSON(value)
                completion(ResponseSaveInformation(JSONString: json.rawString()!)!)
            case .failure(let error):
                print("Error sendInformation:  \(error)  ")
            }
        }
    }

    func sendDocumentsBase64(registroId: String, completion: @escaping (ResponseSaveInformation) -> Void) {
       NewSellerView.imagenesBase64.updateValue(registroId, forKey: "Id")
       let parameters: Parameters = [
            "accion": "Guadar fotos",
            "infoEmpleado": NewSellerView.imagenesBase64
        ]
        let url = AppDelegate.API_SALESFORCE + ApiDefinition.API_CONSULTA_CREACION_EMPLEADO
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: ApiDefinition.headersToken).responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                completion(ResponseSaveInformation(JSONString: json.rawString()!)!)
            case .failure(let error):
                print("Error sendInformation:  \(error)  ")
            }
        }
    }

}
