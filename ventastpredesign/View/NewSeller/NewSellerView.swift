//
//  NewSellerController.swift
//  SalesCloud
//
//  Created by Hector Alfonso Moreno Santa Cruz on 17/04/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import SwiftEventBus
// import Identity

class NewSellerView: UIViewController {

    @objc dynamic var identificationUser: String = ""
    @objc dynamic var tokenIdentity: String = ""

    @IBOutlet weak var picture: UIImageView!
    @IBOutlet weak var uiViewContainer: UIView!
    @IBOutlet weak var arrowBefore: UIImageView!
    @IBOutlet weak var buttonReturn: UIButton!

    private var currentFormStatus: String = ""

    static public var colaboradorFull: [String: Any] = [:]
    static public var imagenesBase64: [String: Any] = [:]

    private let viewModel = NewSellerViewModel()

    @IBAction func returnMenu(_ sender: Any) {
        self.dismiss(animated: false)
    }

    @IBAction func nextButtonFunc(_ sender: Any) {
        self.validationInformation()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        SwiftEventBus.onMainThread(self, name: "changeData") { [unowned self] result in
            if (result?.object as? Bool)! {
                if currentFormStatus == "salesData" {
                    saveInformation()
                } else {
                    self.showMenu()
                }
            }
        }
    }

    func saveInformation() {
        ProgressHUD.show("Enviando Información..")
        self.viewModel.sendInformation { response in
            if response.idRegistro != nil {
                self.viewModel.sendDocumentsBase64(registroId: response.idRegistro!) { response in
                    ProgressHUD.dismiss()
                    if response.result != nil {
                        let alert = UIAlertController(title: nil, message: response.result, preferredStyle: UIAlertControllerStyle.alert)
                        let urlAction: UIAlertAction = UIAlertAction(title: "Aceptar", style: .default) {_ in
                            self.dismiss(animated: false)
                        }
                        alert.addAction(urlAction)
                        self.present(alert, animated: false)
                    }
                }
            } else if (response.result?.contains(string: "El Empleado ya existe")) != nil {
                ProgressHUD.dismiss()
                Constants.Alert(title: "Alerta", body: response.result!, type: "normal", viewC: self)
            }
        }
    }

    func AlamoFirePetition() {
        ProgressHUD.show("Espere un momento por favor..")
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            self.viewModel.downloadOCRAndInformation(token: self.tokenIdentity, id: self.identificationUser) { datas in
                SwiftEventBus.post("GetPersonalData", sender: datas)
                NewSellerView.imagenesBase64 = self.viewModel.putAndSaveImages(responsePersonalData: datas)
                self.picture.image = self.viewModel.putImageView(datas: datas)
                self.picture.ovalImage()
                ProgressHUD.dismiss()
            }
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AlamoFirePetition()
        self.showMenu()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        SwiftEventBus.unregister(self)
    }

   func showMenu() {
        switch currentFormStatus {
        case "personalData":
            currentFormStatus = "contactData"
            ViewEmbedder.embed(withIdentifier: "ContactDataView", parent: self, container: self.uiViewContainer)
            break
        case "contactData":
            currentFormStatus = "salesData"
            ViewEmbedder.embed(withIdentifier: "SalesDataView", parent: self, container: self.uiViewContainer)
            break
        default:
            currentFormStatus  = "personalData"
            ViewEmbedder.embed(withIdentifier: "PersonalDataView", parent: self, container: self.uiViewContainer)
            break
        }
    }

    func validationInformation() {
        switch currentFormStatus {
        case "personalData":
            SwiftEventBus.post("validationPersonalData")
            break
        case "salesData":
            SwiftEventBus.post("validationSalesData")
            break
        case "contactData":
            SwiftEventBus.post("validationContactData")
            break
        default:
            break
        }
    }
}

extension UIView {
    func oval() {
        let path = UIBezierPath(ovalIn: layer.bounds)
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
       layer.mask = maskLayer
    }
}

extension UIImageView {
    func ovalImage() {
        let path = UIBezierPath(ovalIn: layer.bounds)
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
       layer.mask = maskLayer
    }
}
