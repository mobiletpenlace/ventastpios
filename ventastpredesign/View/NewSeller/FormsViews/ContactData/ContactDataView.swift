//
//  ContactDataController.swift
//  SalesCloud
//
//  Created by Hector Alfonso Moreno Santa Cruz on 18/04/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import SwiftEventBus
import MaterialComponents.MaterialTextControls_FilledTextAreas
import MaterialComponents.MaterialTextControls_FilledTextFields

class ContactDataView: BaseViewController {

    @IBOutlet weak var whatsApp: MDCFilledTextField!
    @IBOutlet weak var correoElectronico: MDCFilledTextField!
    @IBOutlet weak var confirmarCorreoElectronico: MDCFilledTextField!

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    @IBAction func phoneChanged(_ sender: Any) {
        if let phoneNumber = self.whatsApp.text {
            if let errorMessage = self.invalidPhoneNumber(phoneNumber) {
               self.whatsApp.errorTheme(message: errorMessage)
            }
        }
    }

    @IBAction func emailValidationChanged(_ sender: Any) {
        if let emailValidation = self.correoElectronico.text {
            if let errorMessage = self.invalidEmail(emailValidation) {
                self.correoElectronico.errorTheme(message: errorMessage)
            }
        }
    }

    @IBAction func confirmEmailValidationChanged(_ sender: Any) {
        if let confirmarValidation = self.confirmarCorreoElectronico.text {
            if let errorMessage = self.invalidEmail(confirmarValidation) {
                self.confirmarCorreoElectronico.errorTheme(message: errorMessage)
            }
        }
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.whatsApp.setTextColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .normal)
        self.whatsApp.setTextColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .editing)

    }

    func invalidPhoneNumber(_ value: String) -> String? {
        let set = CharacterSet(charactersIn: value)
        if !CharacterSet.decimalDigits.isSuperset(of: set) {
            return "El numero no debe contener decimales"
        }
        if value.count != 10 {
            return "El numero debe contener 10 caracteres"
        }
        return nil
    }

    func invalidEmail(_ value: String) -> String? {
        if !value.contains(string: "@") {
            return "No tiene el formato de un correo electrónico"
        }
        if !value.contains(string: ".com") {
            return "No tiene el formato de un correo electrónico"
        }
        return nil
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        self.design()
        SwiftEventBus.onMainThread(self, name: "validationContactData") { _ in
            if self.validation() {
                self.saveData()
                SwiftEventBus.post("changeData", sender: true)
            }
        }
    }

    func saveData() {
        NewSellerView.colaboradorFull.updateValue(self.correoElectronico.text, forKey: "correo")
        NewSellerView.colaboradorFull.updateValue(self.whatsApp.text, forKey: "celular")

    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }

    func validation() -> Bool {
        var counter: Int = 0
        if self.whatsApp.text!.isEmpty || self.whatsApp.text == "" {
            self.whatsApp.errorTheme(message: "Este campo es Obligatorio")
            counter += 1
        }
        if self.correoElectronico.text!.isEmpty || self.correoElectronico.text == "" {
            self.correoElectronico.errorTheme(message: "Este campo es Obligatorio")
            counter += 1
        }
        if self.confirmarCorreoElectronico.text!.isEmpty || self.confirmarCorreoElectronico.text == "" {
            self.confirmarCorreoElectronico.errorTheme(message: "Este campo es Obligatorio")
            counter += 1
        }
        if !self.confirmarCorreoElectronico.text!.isEqual(self.correoElectronico.text) {
            self.confirmarCorreoElectronico.errorTheme(message: "El correo electrónico no coincide")
            counter += 1
        }
        return (counter == 0) ? true : false
    }

    func design() {
        self.whatsApp.delegate = self
        self.correoElectronico.delegate = self
        self.confirmarCorreoElectronico.delegate = self
    }

}
