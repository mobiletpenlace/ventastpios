//
//  SalesDataController.swift
//  SalesCloud
//
//  Created by Hector Alfonso Moreno Santa Cruz on 18/04/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import DropDown
import SwiftEventBus
import MaterialComponents.MaterialTextControls_FilledTextAreas
import MaterialComponents.MaterialTextControls_FilledTextFields

class SalesDataView: BaseViewController {

    @IBOutlet weak var buttonDropDown: UIButton!
    @IBOutlet weak var buttonAceptoDeposito: UIButton!

    @IBOutlet weak var titleDRop: UILabel!
    @IBOutlet weak var textDropDown: UILabel!
    @IBOutlet weak var imageDropDown: UIImageView!
    @IBOutlet weak var referenciaPago: MDCFilledTextField!

    @IBOutlet weak var titleDropDrist: UILabel!
    @IBOutlet weak var textDropDownDrist: UILabel!
    @IBOutlet weak var imageDropDownDrist: UIImageView!
    @IBOutlet weak var buttonDropDownDrist: UIButton!

    var idDistro: String?
    var listDistro: [DistroResponse] = []
    var listBySquare: [String] = []

    let viewModel = NewSellerViewModel()
    var squareList: [SquareModel] = []
    let dropdown = DropDown()
    let dropdownDistro = DropDown()
    var states: [String] = []
    var statesDistro: [String] = []

    @IBOutlet weak var terminosCondiciones: UIButton!

    private var cuentaDeposito: Bool = false
    private var termsCheck: Bool = false

    @IBAction func isTappedDropDownButtonDristro(_ sender: Any) {
        self.changeImageDropDristro(isActive: true)
        if listBySquare.count > 0 {
            dropdownDistro.show()
        } else {
            self.changeImageDropDristro(isActive: false)
            self.alertMessage(message: "La plaza seleccionada no tiene distritos asignados.")
        }
    }

    @IBAction func isTappedDropDownButton(_ sender: Any) {
        self.changeImageDrop(isActive: true)
        dropdown.show()
    }

    @IBAction func isTappedCheckAceptButton(_ sender: Any) {
        self.changeImageCheckButton(isActive: !cuentaDeposito)

    }

    @IBAction func isTappedCheckAceptTermis(_ sender: Any) {
        self.changeImageTerms(isActive: !termsCheck)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

    }

    override func viewDidLoad() {
        super.viewDidLoad()
        referenciaPago.delegate = self
        self.initInformation()

    }

    func initInformation() {
        SwiftEventBus.onMainThread(self, name: "validationSalesData") { _ in
            if self.validationInformation() {
                self.getListEmployedApprovals()

            }
        }

        self.viewModel.getPlazas { mapper in
            self.squareList = mapper
            for string in mapper {
                self.states.append(string.squareName)
            }
            self.initialDropDown()
        }
        self.viewModel.getDistritos {  mapper in
            self.listDistro = mapper
        }

    }

    func getListEmployedApprovals() {
        if RealManager.findFirst(object: Empleado.self) != nil {
            let employe: Empleado = Empleado(value: RealManager.findFirst(object: Empleado.self)!)
            saveData(employedId: employe)
        } else {
            print("Error al traer al usuario")
        }
    }

    func saveData(employedId: Empleado) {
        NewSellerView.colaboradorFull.updateValue(self.cuentaDeposito, forKey: "AceptaPagos")
        NewSellerView.colaboradorFull.updateValue(self.termsCheck, forKey: "AceptaTerminosyCondiciones")
        NewSellerView.colaboradorFull.updateValue(self.referenciaPago.text, forKey: "claveInterbancaria")
        NewSellerView.colaboradorFull.updateValue("Totalplay", forKey: "unidadNegocio")
        NewSellerView.colaboradorFull.updateValue("Experto", forKey: "posicion")
        NewSellerView.colaboradorFull.updateValue("PLATA", forKey: "ranking")
        NewSellerView.colaboradorFull.updateValue("MX", forKey: "pais")
        if self.textDropDownDrist.text != nil || self.textDropDownDrist.text != "Seleccione un distrito" || !self.idDistro!.isEmpty || self.idDistro != nil {
            NewSellerView.colaboradorFull.updateValue(self.idDistro, forKey: "idDistrital")
        }
        NewSellerView.colaboradorFull.updateValue("Autoempresarios Autorizados", forKey: "canal")
        if self.textDropDown.text != nil || self.textDropDown.text != "Seleccione una plaza" {
            NewSellerView.colaboradorFull.updateValue(self.textDropDown.text, forKey: "plaza")
        }
        NewSellerView.colaboradorFull.updateValue(employedId.Id, forKey: "idJefePlaza")
        SwiftEventBus.post("changeData", sender: true)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }

    func validationInformation() -> Bool {
        if self.referenciaPago.text!.isEmpty || self.referenciaPago.text == "" {
            self.referenciaPago.errorTheme(message: "Este campo es obligatorio")
            return false
        } else if  self.referenciaPago.text!.count != 18 {
            self.referenciaPago.errorTheme(message: "Este campo debe contener 18 caracteres")
            return false
        }
        return true
    }

    func initialDropDown() {
        self.dropdown.anchorView = buttonDropDown
        self.dropdown.dataSource = self.states
        self.dropdown.cancelAction = {[unowned self] in
            self.changeImageDrop(isActive: false)
        }
        self.dropdown.bottomOffset = CGPoint(x: 0, y: (dropdown.anchorView?.plainView.bounds.height)!)
        self.dropdown.topOffset  = CGPoint(x: 0, y: -(dropdown.anchorView?.plainView.bounds.height)!)
        self.dropdown.direction = .bottom
        self.dropdown.selectionAction  = { (_: Int, item: String) in
            self.changeImageDrop(isActive: false)
            self.dropdown.hide()
            self.textDropDown.text = item
            self.initialSecondDropDown(plazaSelected: item)
        }
    }

    func setIdDistrito(nameDistrito: String) {
        let itemDistrito: [DistroResponse]  =  listDistro.filter({$0.name == nameDistrito})
        self.idDistro = String(itemDistrito[0].idDistrito)
    }

    func initialSecondDropDown(plazaSelected: String) {
        listBySquare = []
        listDistro.filter({$0.plaza?.namePlaza == plazaSelected}).forEach { it in
            self.listBySquare.append(it.name!)
        }
        initDropDownDristWithList()
    }

    func initDropDownDristWithList() {

            self.dropdownDistro.anchorView = buttonDropDownDrist
            self.dropdownDistro.dataSource = self.listBySquare
            self.dropdownDistro.cancelAction = {[unowned self] in
                self.changeImageDropDristro(isActive: false)
            }
            self.dropdownDistro.bottomOffset = CGPoint(x: 0, y: (dropdownDistro.anchorView?.plainView.bounds.height)!)
            self.dropdownDistro.topOffset  = CGPoint(x: 0, y: -(dropdownDistro.anchorView?.plainView.bounds.height)!)
            self.dropdownDistro.direction = .bottom
            self.dropdownDistro.selectionAction  = { (_: Int, item: String) in
                self.changeImageDropDristro(isActive: false)
                self.dropdownDistro.hide()
                self.textDropDownDrist.text = item
                self.setIdDistrito(nameDistrito: item)
            }

    }

    func changeImageTerms(isActive: Bool) {
        termsCheck = isActive
        self.terminosCondiciones.setImage( UIImage(named: (isActive) ? "icon_check_green" : "icon_uncheck_gray"), for: .normal)
    }
    func changeImageDrop(isActive: Bool) {
        self.imageDropDown.image = UIImage(named: (isActive) ?  "icon_arrow_up_gray" : "icon_arrow_down_gray")
    }

    func changeImageDropDristro(isActive: Bool) {
        self.imageDropDownDrist.image = UIImage(named: (isActive) ?  "icon_arrow_up_gray" : "icon_arrow_down_gray")
    }
    func changeImageCheckButton(isActive: Bool) {
        cuentaDeposito = isActive
        self.buttonAceptoDeposito.setImage( UIImage(named: (isActive) ? "icon_check_green" : "icon_uncheck_gray"), for: .normal)
    }
}

// TODO: Se implementa limitación de textfield a 18 caracteres
extension SalesDataView {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if range.location >= 18 {
            return false
        } else {
            return true
        }
    }
}
