//
//  PersonalDataController.swift
//  SalesCloud
//
//  Created by Hector Alfonso Moreno Santa Cruz on 18/04/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import SwiftEventBus
import MaterialComponents.MaterialTextControls_FilledTextAreas
import MaterialComponents.MaterialTextControls_FilledTextFields

class PersonalDataView: BaseViewController {

    @IBOutlet weak var nombreCompleto: MDCFilledTextField!
    @IBOutlet weak var fechaNacimiento: MDCFilledTextField!
    @IBOutlet weak var rfc: MDCFilledTextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        SwiftEventBus.onMainThread(self, name: "validationPersonalData") {_ in
            if  self.validation() {
                self.saveData()
                ViewEmbedder.removeFromParent(vc: self)
                SwiftEventBus.post("changeData", sender: true)
            }
        }

        SwiftEventBus.onMainThread(self, name: "GetPersonalData") { resultado   in
            let data: ResponsePersonalData = resultado?.object as! ResponsePersonalData
                    data.data?.documents?.forEach({ document in
                       document.fields?.forEach({ (key: String, value: Field) in
                           if key.contains(string: "fullName") {
                               self.nombreCompleto.text = value.value
                           }
                           if key.contains(string: "dateOfBirth") {
                               self.fechaNacimiento.text = value.value
                           }
                       })
        })

        }

    }

    func saveData() {
        if !self.nombreCompleto.text!.isEmpty || self.nombreCompleto.text != "" {
            NewSellerView.colaboradorFull.updateValue(self.nombreCompleto.text, forKey: "nombre")
        }
        if !self.fechaNacimiento.text!.isEmpty || self.fechaNacimiento.text != "" {
            NewSellerView.colaboradorFull.updateValue(self.fechaNacimiento.text, forKey: "fechaNacimiento")
        }
        if !self.rfc.text!.isEmpty || self.rfc.text != "" {
            NewSellerView.colaboradorFull.updateValue(self.rfc.text, forKey: "rfc")
        }
    }
    func validation() -> Bool {
        var counter = 0
        if self.nombreCompleto.text!.isEmpty || self.nombreCompleto.text == "" {
            self.nombreCompleto.errorTheme(message: "Este campo es Obligatorio")
            counter += 1
        }
        if self.fechaNacimiento.text!.isEmpty || self.fechaNacimiento.text == "" {
            self.fechaNacimiento.errorTheme(message: "Este campo es Obligatorio")
            counter += 1
        }
        return (counter == 0) ? true : false
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.design()
    }

    func design() {
        self.nombreCompleto.delegate = self
        self.fechaNacimiento.delegate = self
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

    }

}
