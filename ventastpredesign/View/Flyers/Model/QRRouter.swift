//
//  QRRouter.swift
//  SalesCloud
//
//  Created by aestevezn on 30/03/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import UIKit

protocol QRProtocolImage {
    func onSuccess(qr: UIImage)
    func onError(error: String)
}
class QRRouterServer {

    var delegate: QRProtocolImage!

    func makerResponse() {
        let urlString = "https://apiservice.sistemastp.com.mx/v1/iptv/QrCode"
        var url = URLComponents(string: urlString)!
        let queryName = URLQueryItem(name: "name", value: statics.name)
        let queryPhone = URLQueryItem(name: "phone", value: statics.phone)
        let queryMail = URLQueryItem(name: "email", value: statics.email)
        let querySellerId = URLQueryItem(name: "sellerId", value: faseComisiones.numEmpleado)
        let queryColumns = URLQueryItem(name: "columns", value: "10")
        let queryDifId = URLQueryItem(name: "difId", value: statics.difId)

        url.queryItems = [queryName, queryPhone, queryMail, querySellerId, queryColumns, queryDifId]

        var request = URLRequest(url: url.url!)
        request.httpMethod = "GET"
        request.setValue("Bearer \(statics.TOKEN_SYSTEMS_TP)", forHTTPHeaderField: "Authorization")
        let task = URLSession.shared.dataTask(with: request) { data, _, error in
            if error != nil {
                print(error!)
            }
            if let data = data {
                let str = String(decoding: data, as: UTF8.self)
                print(str)
                let imageData = UIImage(data: data)
                self.delegate?.onSuccess(qr: imageData!)
            } else {
                print("Error al obtener qr")
            }
        }
        task.resume()
    }
}
