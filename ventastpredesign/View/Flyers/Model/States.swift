//
//  States.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera on 14/03/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class States: BaseRequest {

    var sec_distrito: String?
    var nombre_distrito: String?
    var cve_distrito: String?

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        sec_distrito <- map["sec_distrito"]
        cve_distrito <- map["cve_distrito"]
        nombre_distrito <- map["nombre_distrito"]
    }

}
