//
//  Images.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera on 24/03/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class Images: BaseRequest {

    var FlyerVertical: FlyerVertical?

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        FlyerVertical <- map["FLYER_VERTICAL"]
    }
}
