//
//  FlyerVertical.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera on 24/03/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class FlyerVertical: BaseRequest {

    var ext: String?
    var misId: Int?
    var category: String?
    var isCdn: Bool?

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        ext <- map["ext"]
        misId <- map["misId"]
        category <- map["category"]
        isCdn <- map["isCdn"]
    }

}
