//
//  DigitalFlyerType.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera on 24/03/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class DigitalFlyerType: BaseRequest {

    var name: String?
    var width: Int?
    var height: Int?

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        name <- map["name"]
        width <- map["width"]
        height <- map["height"]
    }

}
