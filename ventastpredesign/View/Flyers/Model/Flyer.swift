//
//  Flyer.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera on 24/03/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class Flyer: BaseRequest {
    var difId: Int = 0
    var name: String = ""
    var images: Images?
    var digitalFlyerType: DigitalFlyerType?

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        self.difId <- map["difId"]
        self.name <- map["name"]
        self.images <- map["images"]
        self.digitalFlyerType <- map["digitalFlyerType"]
    }

}
