//
//  PDFRouter.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera on 30/03/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import PDFKit

protocol PDFProtocol {
    func onSuccess(pdf: PDFDocument)
    func onError(error: String)
}
class PDFRouterServer {

    var delegate: PDFProtocol!

    func makerResponse() {
        let urlString = "https://apiservice.sistemastp.com.mx/v1/iptv/Flyer"
        var url = URLComponents(string: urlString)!
        let queryName = URLQueryItem(name: "name", value: statics.name)
        let queryPhone = URLQueryItem(name: "phone", value: statics.phone)
        let queryMail = URLQueryItem(name: "email", value: statics.email)
        let querySellerId = URLQueryItem(name: "sellerId", value: faseComisiones.numEmpleado)
        let queryColumns = URLQueryItem(name: "columns", value: "10")
        let queryDifId = URLQueryItem(name: "difId", value: statics.difId)
        url.queryItems = [queryName, queryPhone, queryMail, querySellerId, queryColumns, queryDifId]
        var request = URLRequest(url: url.url!)
        request.httpMethod = "GET"
        request.setValue("Bearer \(statics.TOKEN_SYSTEMS_TP)", forHTTPHeaderField: "Authorization")
        let task = URLSession.shared.dataTask(with: request) { data, _, error in
            if error != nil {
                print(error!)
                statics.isPDF = false
            }
            if let data = data {
                let pdfData = PDFDocument(data: data)
                let fileStream: String = data.base64EncodedString(options: NSData.Base64EncodingOptions.init(rawValue: 0))
                statics.PDF = fileStream
                statics.isPDF = true
                self.delegate?.onSuccess(pdf: pdfData!)
            } else {
                statics.isPDF = false
            }
        }
        task.resume()
    }
}
