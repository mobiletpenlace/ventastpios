//
//  FlyersViewController.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera on 09/03/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit
import PDFKit
import Foundation
import SwiftEventBus

class FlyersViewController: BaseVentasView, FlyersObserver {

    func successSendPDF() {
        statics.sendWA = false
    }

    var mFlyersViewModel: FlyersViewModel!
    var districts: [States]? = []
    var offers: [GetFlyersResponse]? = []
    var codigoDistrito: String?
    var qrRouter = QRRouterServer()
    var pdfRouter = PDFRouterServer()
    @IBOutlet weak var employeeNumberLabel: UILabel!
    @IBOutlet weak var pullDownDistrictButton: UIButton!
    @IBOutlet weak var pullDownOfferButton: UIButton!
    @IBOutlet weak var downloadFlyerButton: UIButton!
    @IBOutlet weak var seeQRButton: UIButton!
    @IBOutlet weak var sendWAButton: UIButton!

    override func viewDidLoad() {
        super.setView(view: self.view)
        employeeNumberLabel.text = faseComisiones.numEmpleado
        mFlyersViewModel = FlyersViewModel(view: self)
        mFlyersViewModel.attachView(observer: self)
        mFlyersViewModel.getTokenSystems()
        qrRouter.delegate = self
        pdfRouter.delegate = self
        fillDropDownDistrictButton()
        SwiftEventBus.onMainThread(self, name: "closeFlyerPopUpMailViewController") { [weak self] _ in
            self?.backAction()
        }

    }

    override func viewDidAppear(_ animated: Bool) {
        if statics.sendWA == true {
            mFlyersViewModel.sendPDF()
        }

        let viewAlert: UIStoryboard = UIStoryboard(name: "PopUpMailView", bundle: nil)
        let viewAlertVC = viewAlert.instantiateViewController(withIdentifier: "PopUpMailView")as! PopUpMailViewController
        viewAlertVC.providesPresentationContextTransitionStyle = true
        viewAlertVC.definesPresentationContext = true
        viewAlertVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        viewAlertVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(viewAlertVC, animated: false, completion: {})
    }

    @IBAction func backButton(_ sender: Any) {
        self.backAction()
    }

    func backAction() {
        self.dismiss(animated: false, completion: nil)
    }

    @objc func alertControllerBackgroundTapped() {
        self.dismiss(animated: true, completion: nil)
    }

    func fillDropDownDistrictButton() {
        if #available(iOS 14.0, *) {
            var distritosMenu: [UIAction] = []
            guard let distritos = districts else {return}
            pullDownDistrictButton.setTitle("Selecciona un distrito", for: .normal)
            pullDownDistrictButton.showsMenuAsPrimaryAction = true
            let popUpButtonClosure = { [weak self] (action: UIAction) in
                print("District selected = \(action.title) District code: \(action.identifier.rawValue)")
                self?.pullDownOfferButton.isHidden = false
                self?.pullDownDistrictButton.setTitle(action.title, for: .normal)
                self?.mFlyersViewModel.getOffers(code: "\(action.identifier.rawValue)")
            }
            for distrito in distritos {
                distritosMenu.append(UIAction(title: distrito.nombre_distrito ?? "", identifier: UIAction.Identifier(distrito.cve_distrito ?? ""), handler: popUpButtonClosure))
            }
            pullDownDistrictButton.menu = UIMenu(children: distritosMenu )
        } else {
            // Fallback on earlier versions
        }
    }

    func fillDropDownOffersButton() {
            // Fill menu
            if #available(iOS 14.0, *) {
                guard let flyers = offers else {return}
                pullDownOfferButton.setTitle("Selecciona un flyer", for: .normal)
                self.downloadFlyerButton.isHidden = true
                self.seeQRButton.isHidden = true
                self.sendWAButton.isHidden = true
                pullDownOfferButton.showsMenuAsPrimaryAction = true

                // Selected button closure
                let popUpButtonClosure = { [weak self] (action: UIAction) in
                    print("Flyer selected = \(action.title) difId code: \(action.identifier.rawValue)")
                    statics.difId = action.identifier.rawValue
                    self?.downloadFlyerButton.isHidden = false
                    self?.seeQRButton.isHidden = false
                    self?.sendWAButton.isHidden = false
                    self?.pullDownOfferButton.setTitle(action.title, for: .normal)
                }
                // Selected button closure

                var flyersMenu: [UIAction] = []
                for flyer in flyers {
                    flyersMenu.append(UIAction(title: flyer.name ?? "", identifier: UIAction.Identifier(String(flyer.difId!)), handler: popUpButtonClosure))

                }
                pullDownOfferButton.menu = UIMenu(children: flyersMenu )
            } else {
                // Fallback on earlier versions
            }
    }

    func successGetTokenSystem() {
        mFlyersViewModel.getTokenCariai()
    }

    func successGetTokenCariai() {
        mFlyersViewModel.getAllStates()
    }

    func successGetFlyers(array: [GetFlyersResponse]) {
        offers = array
        fillDropDownOffersButton()
    }

    func successGetAllStates(object: [States]) {
        districts = object
        fillDropDownDistrictButton()
    }

    func onError(errorMessage: String) {
        Constants.LoadStoryBoard(storyboard: "HomeScreenRedesign", identifier: "HomeScreenRedesign", viewC: self)
        showToast2(message: "Fallo getToken", font: UIFont(name: "Helvetica Neue", size: 11)!, time: 4.0)

    }

    @IBAction func downloadFlyerAction(_ sender: Any) {
        pdfRouter.makerResponse()
        showToast2(message: "Descargando...", font: UIFont(name: "Helvetica Neue", size: 11)!, time: 4.0)
    }

    @IBAction func qrAction(_ sender: Any) {
        qrRouter.makerResponse()
    }

    @IBAction func sendWAAction(_ sender: Any) {
        if statics.isPDF == false {
            showToast2(message: "Descargue el PDF Primero", font: UIFont(name: "Helvetica Neue", size: 11)!, time: 4.0)
        } else {
            let viewAlert: UIStoryboard = UIStoryboard(name: "PopUpTelephoneView", bundle: nil)
              let viewAlertVC = viewAlert.instantiateViewController(withIdentifier: "PopUpTelephoneView")as! PopUpTelephoneViewController
              viewAlertVC.providesPresentationContextTransitionStyle = true
              viewAlertVC.definesPresentationContext = true
              viewAlertVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
              viewAlertVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
              self.present(viewAlertVC, animated: false, completion: {})
        }
    }

}

extension FlyersViewController: QRProtocolImage, PDFProtocol {
    func onSuccess(pdf: PDFDocument) {
        DispatchQueue.main.async {
            let viewAlert: UIStoryboard = UIStoryboard(name: "PDFViewController", bundle: nil)
            let viewAlertVC = viewAlert.instantiateViewController(withIdentifier: "PDFViewController")as! PDFViewController
            viewAlertVC.setPDFDocument(document: pdf)
            viewAlertVC.providesPresentationContextTransitionStyle = true
            viewAlertVC.definesPresentationContext = true
            viewAlertVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            viewAlertVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            self.present(viewAlertVC, animated: true, completion: {})
        }
    }

    func onSuccess(qr: UIImage) {
        DispatchQueue.main.async {
            let controller = ModalQRViewController(image: qr)
            self.present(controller, animated: true)
        }
    }

    func onError(error: String) {
        print("ha ocurrido un error")
    }
}

extension FlyersViewController: managerInteractor {

    func modalManager(message: String) {
        showToast2(message: message, font: UIFont(name: "Helvetica Neue", size: 11)!, time: 10.0)
    }
}
