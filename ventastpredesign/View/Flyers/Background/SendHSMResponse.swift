//
//  SendHSMResponse.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera on 30/03/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class SendHSMResponse: BaseResponse {

    var status: Int = 0
    var message: String = ""

    required init?(map: Map) {
}
    override func mapping(map: Map) {
        self.status <- map["status"]
        self.message <- map["message"]
    }
}
