//
//  GetAllStatesResponse.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera on 14/03/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

    import UIKit
    import ObjectMapper

    class GetAllStatesResponse: BaseResponse {

        required init?(map: Map) {

        }

        override func mapping(map: Map) {
            Code <- map["Code"]
            Result <- map["Result"]
            ResultDescription <- map["ResultDescription"]
            States <- map["States"]

        }
        override init() {

        }

        var Code: String?
        var Result: String?
        var ResultDescription: String?
        var States: [States] = []

    }
