//
//  GetFlyersResponse.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera on 24/03/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

    import UIKit
    import ObjectMapper

    class GetFlyersResponse: BaseResponse {

        required init?(map: Map) {

        }

        override func mapping(map: Map) {
            difId <- map["difId"]
            name <- map["name"]
            digitalFlyerType <- map["digitalFlyerType"]

        }
        override init() {

        }

        var difId: Int?
        var name: String?
        var digitalFlyerType: DigitalFlyerType?

    }
