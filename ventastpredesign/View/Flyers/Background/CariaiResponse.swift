//
//  CariaiResponse.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera on 29/03/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class CariaiResponse: BaseResponse {

    var mCariSec: String = ""
    var mExpiresIn: String = ""

    required init?(map: Map) {
}
    override func mapping(map: Map) {
        self.mCariSec <- map["cariSec"]
        self.mExpiresIn <- map["expiresIn"]
    }
}
