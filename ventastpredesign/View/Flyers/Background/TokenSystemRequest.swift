//
//  TokenSystemRequest.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera on 16/03/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class TokenSystemRequest: BaseRequest {
    var mGrantType: String = ""
    var mClient_id: String = ""
    var mClient_secret: String = ""
    var mUsername: String = ""
    var mPassword: String = ""

    init(mGrantType: String, mClient_id: String, mClient_secret: String, mUsername: String, mPassword: String) {
        super.init()
        self.mGrantType = mGrantType
        self.mClient_id = mClient_id
        self.mClient_secret = mClient_secret
        self.mUsername = mUsername
        self.mPassword = mPassword
    }

    override init() {
        super.init()
    }

    public required init?(map: Map) {
        fatalError("init(map:) has not been implemented")
    }

    override func mapping(map: Map) {
        self.mGrantType <- map["grant_type"]
        self.mClient_id <- map["client_id"]
        self.mClient_secret <- map["client_secret"]
        self.mUsername <- map["username"]
        self.mPassword <- map["password"]
    }
}
