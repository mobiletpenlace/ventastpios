//
//  QRResponse.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera on 29/03/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

    import UIKit
    import ObjectMapper

    class QRResponse: BaseResponse {

        required init?(map: Map) {

        }

        override func mapping(map: Map) {

            bit <- map["PNG"]

        }
        override init() {

        }

        var bit: String?

    }
