//
//  TokenSystemResponse.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera on 13/03/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class TokenSystemResponse: BaseResponse {

    var mAccess_token: String = ""
    var mExpires_in: String = ""
    var mRefresh_count: String = ""
    var mStatus: String = ""
    var mRefresh_token_expires_in: String = ""
    var mApi_product_list: String = ""
    var mOrganization_name: String = ""
    var mDeveloperEmail: String = ""
    var mToken_type: String = ""
    var mIssued_at: String = ""
    var mClient_id: String = ""

    required init?(map: Map) {
}
    override func mapping(map: Map) {
        self.mRefresh_token_expires_in <- map["refresh_token_expires_in"]
        self.mApi_product_list <- map["api_product_list"]
        self.mOrganization_name <- map["organization_name"]
        self.mDeveloperEmail <- map["developer.email"]
        self.mToken_type <- map["token_type"]
        self.mIssued_at <- map["issued_at"]
        self.mClient_id <- map["client_id"]
        self.mAccess_token <- map["access_token"]
        self.mExpires_in <- map["expires_in"]
        self.mRefresh_count <- map["refresh_count"]
        self.mStatus <- map["status"]
    }
}
