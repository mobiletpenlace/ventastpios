//
//  CariaiRequest.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera on 29/03/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class CariaiRequest: BaseRequest {
    var credentials: String = ""

    init(credentials: String) {
        super.init()
        self.credentials = credentials
    }

    override init() {
        super.init()
    }

    public required init?(map: Map) {
        fatalError("init(map:) has not been implemented")
    }

    override func mapping(map: Map) {
        self.credentials <- map["credentials"]
    }
}
