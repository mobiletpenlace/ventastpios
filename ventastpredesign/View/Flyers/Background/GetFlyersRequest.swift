//
//  GetOffersRequest.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera on 24/03/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import ObjectMapper

public class GetOffersRequest: BaseRequest {

    var districtCode: String?

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override public func mapping(map: Map) {
        districtCode <- map["districtCode"]
    }

}
