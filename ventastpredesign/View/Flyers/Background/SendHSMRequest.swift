//
//  SendHSMRequest.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera on 30/03/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class SendHSMRequest: BaseRequest {
    var channel: String = "WHATSAPP"
    var hsm: String = "hsm_pruebas_flyer_v5"
    var numbers: [String] = []
    var parameters: [String] = []
    var pdfData: String = ""

    init(channel: String, hsm: String, numbers: [String], parameters: [String], pdfData: String ) {
        super.init()
        self.channel
        self.hsm
        self.numbers
        self.parameters
        self.pdfData
    }

    override init() {
        super.init()
    }

    public required init?(map: Map) {
        fatalError("init(map:) has not been implemented")
    }

    override func mapping(map: Map) {
        self.channel <- map["channel"]
        self.hsm <- map["hsm"]
        self.numbers <- map["numbers"]
        self.parameters <- map["parameters"]
        self.pdfData <- map["pdfData"]
    }
}
