//
//  SendPDFModel.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera on 31/03/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

protocol SendPDFModelObserver {
    func successSendPDF()
    func onError(errorMessage: String)
}

class SendPDFModel: BaseModel {
    var viewModel: SendPDFModelObserver?
    var serverSendHSM: ServerDataManager2<SendHSMResponse>?
    var urlSendPDF = AppDelegate.API_SEND_HSM

    init (observer: BaseViewModelObserver) {
        super.init(observerVM: observer)
        serverSendHSM = ServerDataManager2<SendHSMResponse>()
    }

    func attachModel(viewModel: SendPDFModelObserver) {
        self.viewModel = viewModel
    }

    func sendPDF() {
        let request = SendHSMRequest()
        request.numbers.append(statics.WA)
        request.parameters.append(statics.nameClient)
        request.pdfData = statics.PDF
        serverSendHSM?.setValues(requestUrl: urlSendPDF, delegate: self, headerType: .headersSendPDF)
        serverSendHSM?.request(requestModel: request)
    }

    override func onErrorLoadResponse(requestUrl: String, messageError: String) {
        viewModel?.onError(errorMessage: messageError)
    }

    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if requestUrl == urlSendPDF {
            print(response)
            dump(response)
            let sendHSMResponse = response as! SendHSMResponse
            if sendHSMResponse.status == 200 {
                viewModel?.successSendPDF()
            } else {
                viewModel?.onError(errorMessage: "Error al enviar PDF")
            }
        }
    }

}
