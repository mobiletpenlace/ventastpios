//
//  FlyersModel.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera on 10/03/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

protocol FlyersModelObserver {
    func successGetAllStates (states: GetAllStatesResponse)
    func successGetFlyers (flyers: [GetFlyersResponse])
    func successGetTokenSystem()
    func successGetTokenCariai()
    func onError(errorMessage: String)
    func successSendPDF()
}

class FlyersModel: BaseModel {
    var viewModel: FlyersModelObserver?
    var serverTokenSystems: ServerDataManager2<TokenSystemResponse>?
    var serverTokenCariai: ServerDataManager2<CariaiResponse>?
    var serverStates: ServerDataManager2<GetAllStatesResponse>?
    var serverFlyers: ServerDataManager2<GetFlyersResponse>?
    var serverSendHSM: ServerDataManager2<SendHSMResponse>?
    var urlTokenSystemsTP = AppDelegate.API_SYSTEMSTP + ApiDefinition.API_SYSTEMASTP_TOKEN
    var urlTokenCariai = AppDelegate.API_CARIAI
    var urlGetAllStates = AppDelegate.API_SYSTEMSTP + ApiDefinition.API_SYSTEMS_GET_ALL_STATES
    var urlGetFlyers = AppDelegate.API_SYSTEMSTP + ApiDefinition.API_SYSTEMS_GET_OFFERS
    var urlSendPDF = AppDelegate.API_SEND_HSM

    init (observer: BaseViewModelObserver) {
        super.init(observerVM: observer)
        serverTokenSystems = ServerDataManager2<TokenSystemResponse>()
        serverTokenCariai = ServerDataManager2<CariaiResponse>()
        serverStates = ServerDataManager2<GetAllStatesResponse>()
        serverFlyers = ServerDataManager2<GetFlyersResponse>()
        serverSendHSM = ServerDataManager2<SendHSMResponse>()

    }

    func attachModel(viewModel: FlyersModelObserver) {
        self.viewModel = viewModel
    }

    func getTokenSystems() {
        ApiDefinition.oAuthSystemToken["Authorization"]! = "Basic "
        print(ApiDefinition.oAuthSystemToken["Authorization"]!)
        let request = TokenSystemRequest()
        request.mGrantType = ""
        request.mClient_id = ""
        request.mClient_secret = ""
        request.mUsername = ""
        request.mPassword = ""
        ApiDefinition.oAuthSystemToken["Authorization"]! += statics.TOKEN_SYSTEM_AUTHORIZATION_PROD.toBase64()!
        print(ApiDefinition.oAuthSystemToken["Authorization"]!)
        serverTokenSystems?.setValues(requestUrl: urlTokenSystemsTP, delegate: self, headerType: .headersSystemsTP)
        serverTokenSystems?.request(requestModel: request)
    }

    func getTokenCariai() {
        let request = CariaiRequest()
        request.credentials = "cVhlaTdqekZaZkkyL1VZRDc5VjFiUWRwb2tWbjdsQi9LWC9za2oyQllVLzNPWmNkWEhVeUtPdDVwL1RSdFRscTBMU010bTVLZHVnPQ====="
        serverTokenCariai?.setValues(requestUrl: urlTokenCariai, delegate: self, headerType: .headersCariaiToken)
        serverTokenCariai?.request(requestModel: request)
    }

    func getAllStates() {
        ApiDefinition.headersSystemToken["Authorization"]! = "Bearer "
        let request = AlfaRequest()
        ApiDefinition.headersSystemToken["Authorization"]! += statics.TOKEN_SYSTEMS_TP
        print(ApiDefinition.headersSystemToken["Authorization"]!)
        serverStates?.setValues(requestUrl: urlGetAllStates, delegate: self, headerType: .headersSystemToken)
        serverStates?.request(requestModel: request)
    }

    func getOffers(districtCode: String) {
        let request = GetOffersRequest()
        request.districtCode = districtCode
        serverFlyers?.setValues(requestUrl: urlGetFlyers, delegate: self, headerType: .getHeadersFlyersSystemsTP)
        serverFlyers?.request(requestModel: request)
    }

    func sendPDF() {
        let request = SendHSMRequest()
        request.numbers.append(statics.WA)
        request.parameters.append(statics.nameClient)
        request.pdfData = statics.PDF
        serverSendHSM?.setValues(requestUrl: urlSendPDF, delegate: self, headerType: .headersSendPDF)
        serverSendHSM?.request(requestModel: request)
    }

    override func onErrorLoadResponse(requestUrl: String, messageError: String) {
//        viewModel?.onError(errorMessage: messageError)
    }

    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if requestUrl == urlTokenSystemsTP {
            let tokenSystemResponse = response as! TokenSystemResponse
            if tokenSystemResponse.mAccess_token.isEmpty == false {
                viewModel?.successGetTokenSystem()
            } else {
                viewModel?.onError(errorMessage: "Favor de intentar mas tarde")
            }
        } else if requestUrl == urlTokenCariai {
            let cariaiResponse = response as! CariaiResponse
            if cariaiResponse.mCariSec.isEmpty == false {
                viewModel?.successGetTokenCariai()
            } else {
                viewModel?.onError(errorMessage: "Favor de intentar mas tarde")
            }
        } else if requestUrl == urlGetAllStates {
            let getAllStatesResponse = response as! GetAllStatesResponse
            if getAllStatesResponse.Code == "200" {
                viewModel?.successGetAllStates(states: getAllStatesResponse)
            } else {
                viewModel?.onError(errorMessage: "Error al obtener los Distritos")
            }
        } else if requestUrl == urlSendPDF {
            let sendHSMResponse = response as! SendHSMResponse
            if sendHSMResponse.status == 200 {
                viewModel?.successSendPDF()
            } else {
                viewModel?.onError(errorMessage: "Error al enviar PDF")
            }
        }
    }

    override func onSuccessLoadResponse(requestUrl: String, response: [BaseResponse]) {
        if requestUrl == urlGetFlyers {
            let getFlyersResponse = response as! [GetFlyersResponse]
            viewModel?.successGetFlyers(flyers: getFlyersResponse)
        }
    }
}
