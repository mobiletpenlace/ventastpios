//
//  FlyersViewModel.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera on 10/03/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import SwiftEventBus

protocol FlyersObserver {
    func successGetAllStates(object: [States])
    func successGetFlyers(array: [GetFlyersResponse])
    func successGetTokenSystem()
    func successGetTokenCariai()
    func successSendPDF()
    func onError(errorMessage: String)
}

class FlyersViewModel: BaseViewModel, FlyersModelObserver {
    func onError(errorMessage: String) {
        view.hideLoading()
        observer?.onError(errorMessage: errorMessage)
    }

    func successSendPDF() {
        view.hideLoading()
        observer?.successSendPDF()
    }

    func successGetFlyers(flyers: [GetFlyersResponse]) {
        view.hideLoading()
        var mFlyers = [GetFlyersResponse]()
        mFlyers = flyers
        observer?.successGetFlyers(array: mFlyers)
    }

    func sendPDF() {
        view.showLoading(message: "")
        model.sendPDF()
    }

    func successGetAllStates(states: GetAllStatesResponse) {
        view.hideLoading()
        var mStates = [States]()
        mStates = states.States
        observer?.successGetAllStates(object: mStates)
    }

    func successGetTokenSystem() {
        view.hideLoading()
        observer.successGetTokenSystem()
    }

    func successGetTokenCariai() {
        view.hideLoading()
        observer.successGetTokenCariai()
    }

    func successQR(image: String) {
        view.hideLoading()
    }

    var observer: FlyersObserver!
    var model: FlyersModel!

    override init(view: BaseVentasView) {
        super.init(view: view)
        model = FlyersModel(observer: self)
        model.attachModel(viewModel: self)
    }

    func attachView(observer: FlyersObserver) {
        self.observer = observer
    }

    func viewWillDisappear() {
        SwiftEventBus.unregister(self)
    }

    func getAllStates() {
        model.getAllStates()
    }

    func getOffers(code: String) {
        model.getOffers(districtCode: code)
    }

    func getTokenSystems() {
        model.getTokenSystems()
    }

    func getTokenCariai() {
        model.getTokenCariai()
    }

}
