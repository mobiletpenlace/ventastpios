//
//  SendPDFViewModel.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera on 31/03/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import SwiftEventBus

protocol SendPDFObserver {
    func successSendPDF()
    func onError(errorMessage: String)
}

class SendPDFViewModel: BaseViewModel, SendPDFModelObserver {
    func successSendPDF() {
        view.hideLoading()
        observer?.successSendPDF()
    }

    func onError(errorMessage: String) {
        view.hideLoading()
        observer?.onError(errorMessage: errorMessage)
    }

    var observer: SendPDFObserver!
    var model: SendPDFModel!

    override init(view: BaseVentasView) {
        super.init(view: view)
        model = SendPDFModel(observer: self)
        model.attachModel(viewModel: self)
    }

    func attachView(observer: SendPDFObserver) {
        self.observer = observer
    }

    func viewWillDisappear() {
        SwiftEventBus.unregister(self)
    }

    func sendPDF() {
        view.showLoading(message: "")
        model.sendPDF()
    }
}
