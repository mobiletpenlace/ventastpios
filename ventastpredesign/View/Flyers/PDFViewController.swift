//
//  PDFViewController.swift
//  SalesCloud
//
//  Created by Armando Isais Olguin Cabrera on 31/03/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit
import PDFKit
import Foundation

class PDFViewController: UIViewController {

    var pdf: PDFDocument!
    @IBOutlet weak var pdfViewContainer: PDFView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.createPDFView()
    }

    init(pdf: PDFDocument) {
        self.pdf = pdf
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    @IBAction func backButton(_ sender: Any) {
        Constants.Back(viewC: self)
    }

    func setPDFDocument(document: PDFDocument) {
        self.pdf = document
    }

    func createPDFView() {
        let pdfView = PDFView(frame: self.view.bounds)
        pdfView.translatesAutoresizingMaskIntoConstraints = true
        pdfView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        pdfView.autoScales = true
        pdfView.displayMode = .singlePageContinuous
        pdfView.displaysPageBreaks = true
        pdfView.height = self.pdfViewContainer.height - 35
        self.pdfViewContainer.addSubview(pdfView)
        pdfView.document = pdf
    }

}
