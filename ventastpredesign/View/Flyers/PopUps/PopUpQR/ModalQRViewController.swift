//
//  ModalQRViewController.swift
//  SalesCloud
//
//  Created by aestevezn on 30/03/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

class ModalQRViewController: UIViewController {

    var image: UIImage!
    @IBOutlet weak var nombreLabel: UILabel!
    @IBOutlet weak var nombreLabelValue: UILabel!
    @IBOutlet weak var noEmpleadoLabel: UILabel!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var noEmpleadoLabelValue: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var closeButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
        self.imageView.image = image
    }

    init(image: UIImage) {
        self.image = image
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    func setupView() {
        contentView.layer.cornerRadius = 25
        nombreLabel.text = "Nombre"
        nombreLabelValue.text = statics.name
        noEmpleadoLabel.text = "No. Empleado"
        noEmpleadoLabelValue.text = faseComisiones.numEmpleado
        closeButton.setTitle("Cerrar", for: .normal)
        closeButton.setTitleColor(.white, for: .normal)
    }

    @IBAction func closeView(_ sender: Any) {
        self.dismiss(animated: true)
    }

}
