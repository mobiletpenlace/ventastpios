//
//  PopUpTelephoneViewController.swift
//  SalesCloud
//
//  Created by aestevezn on 14/03/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import MaterialComponents

protocol managerInteractor {
    func modalManager(message: String)
}

class PopUpTelephoneViewController: BaseVentasView, SendPDFObserver {

    var manager: managerInteractor!
    var validName = false
    var validNumber = false
    var mSendPDFViewModel: SendPDFViewModel!
    @IBOutlet weak var cortinaView: UIView!
    @IBOutlet weak var numberTextField: NumberTextField!
    @IBOutlet weak var nameTextField: MDCFilledTextField!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var closeViewButton: UIButton!
    @IBOutlet weak var sendMessageButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        self.setUpView()
        mSendPDFViewModel = SendPDFViewModel(view: self)
        mSendPDFViewModel.attachView(observer: self)
    }

    @IBAction func closeViewAction(_ sender: Any) {
        self.dismiss(animated: true)
    }

    @IBAction func sendMessageAction(_ sender: Any) {
        statics.sendWA = true
        statics.WA = numberTextField.text!
        statics.nameClient = nameTextField.text!
        contentView.isHidden = false
        mSendPDFViewModel.sendPDF()

    }

    func successSendPDF() {
        self.dismiss(animated: true)
        manager?.modalManager(message: "Se ha enviado correctamente")
        sendMessageButton.isUserInteractionEnabled = true
        sendMessageButton.alpha = 1.0
    }

    func onError(errorMessage: String) {
        sendMessageButton.isUserInteractionEnabled = true
        sendMessageButton.alpha = 1.0
    }

    func deferExample() {
        mSendPDFViewModel.sendPDF()
    }

    @IBAction func numberDidEndEditing(_ sender: NumberTextField) {
        guard let number = sender.getNumber() else { return }
        sender.validateNumber()
        if sender.isValid {
            validNumber = true
            if validName && validNumber {
                sendMessageButton.isEnabled = true
            }
        } else {
            // DO INVALID STUFF
            print("IS invalid")
            sendMessageButton.isEnabled = false
            validNumber = false
        }
    }

    @IBAction func nameDidEndEditing(_ sender: MDCFilledTextField) {
        if !(sender.text?.count ?? 0 > 0) {
            validName = false
            sendMessageButton.isEnabled = false
            sender.leadingAssistiveLabel.isHidden = false
            sender.setTextColor(UIColor.red, for: .normal)
            sender.setNormalLabelColor(UIColor.red, for: .normal)
            sender.setFloatingLabelColor(UIColor.red, for: .normal)
            sender.setLeadingAssistiveLabelColor(UIColor.red, for: .normal)
            sender.leadingAssistiveLabel.text = "Ingresa un nombre"
        } else {
            validName = true
            if validName && validNumber {
                sendMessageButton.isEnabled = true
            }
            sender.leadingAssistiveLabel.isHidden = true
            sender.leadingAssistiveLabel.text = ""
            sender.setFilledBackgroundColor(UIColor.clear, for: .normal)
            sender.setFilledBackgroundColor(UIColor.clear, for: .editing)
            sender.setTextColor(UIColor(named: "Base_rede_ligth_gray_titles")!, for: .normal)
            sender.setNormalLabelColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .normal)
            sender.setFloatingLabelColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .normal)
            sender.setUnderlineColor(UIColor(named: "Base_rede_ligth_gray_titles")!, for: .normal)
            sender.setUnderlineColor(UIColor(named: "Base_rede_ligth_gray_titles")!, for: .editing)
        }
    }

    func configTextFields() {
        numberTextField.leadingAssistiveLabel.set(text: "Ingresa tu número")
        nameTextField.setFilledBackgroundColor(UIColor.clear, for: .normal)
        nameTextField.setFilledBackgroundColor(UIColor.clear, for: .disabled)
        nameTextField.setFilledBackgroundColor(UIColor.clear, for: .editing)
        nameTextField.setTextColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .normal)
        nameTextField.setTextColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .editing)
        nameTextField.setNormalLabelColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .normal)
        nameTextField.setNormalLabelColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .editing)
        nameTextField.setFloatingLabelColor(UIColor(named: "Base_rede_ligth_gray_titles")!, for: .normal)
        nameTextField.setFloatingLabelColor(UIColor(named: "Base_rede_ligth_gray_titles")!, for: .editing)
    }

    func setUpView() {
        contentView.layer.cornerRadius = 15
        labelTitle.text = "Ingresa datos del cliente"
        closeViewButton.setTitle("", for: .normal)
        closeViewButton.setTitle("", for: .selected)
        sendMessageButton.setTitle("Enviar por WhatsApp", for: .normal)
        cortinaView.isHidden = true
        configTextFields()
    }
}
