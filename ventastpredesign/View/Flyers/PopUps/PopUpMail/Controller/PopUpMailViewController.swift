//
//  PopUpMailViewController.swift
//  SalesCloud
//
//  Created by aestevezn on 14/03/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit
import Foundation
import MaterialComponents.MaterialTextControls_FilledTextAreas
import MaterialComponents.MaterialTextControls_FilledTextFields
import SwiftEventBus

class PopUpMailViewController: BaseVentasView {

    @IBOutlet weak var closeButton: UIButton!

    @IBOutlet weak var formButton: UIButton!

    @IBOutlet weak var nameTextfield: MDCFilledTextField!
    @IBOutlet weak var numberPhoneTextfield: NumberTextField!
    @IBOutlet weak var emailTextfield: MDCFilledTextField!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var contentView: UIView!

    var validPhone = false
    var validEmail = false
    var validName = false

    override func viewDidLoad() {
        configTextFields()
        super.viewDidLoad()
        super.setView(view: self.view)
        self.setUpView()
    }

    @IBAction func closeViewAction(_ sender: Any) {
        dismiss(animated: true)
        SwiftEventBus.post("closeFlyerPopUpMailViewController", sender: nil)
    }

    @IBAction func formButtonAction(_ sender: Any) {
        statics.name = nameTextfield.text!
        statics.phone = numberPhoneTextfield.getNumber()!
        statics.email = emailTextfield.text!
        self.dismiss(animated: true)
    }

    @IBAction func numberDidEndEditing(_ sender: NumberTextField) {
        print("Entro al didend")
        guard let number = sender.getNumber() else { return }
        sender.validateNumber()
        print("Number: \(number) Valid: \(sender.isValid)")
        if sender.isValid {
            // DO VALID STUFF
            print("IS valid")
            validPhone = true
            if validName && validPhone && validEmail {
                formButton.isEnabled = true
            }
        } else {
            // DO INVALID STUFF
            print("IS invalid")
            validPhone = false
            formButton.isEnabled = false
        }
    }

    @IBAction func nameDidEndEditing(_ sender: MDCFilledTextField) {
        if !(sender.text?.count ?? 0 > 0) {
            validName = false
            formButton.isEnabled = false
            sender.leadingAssistiveLabel.isHidden = false
            sender.setTextColor(UIColor.red, for: .normal)
            sender.setNormalLabelColor(UIColor.red, for: .normal)
            sender.setFloatingLabelColor(UIColor.red, for: .normal)
            sender.setLeadingAssistiveLabelColor(UIColor.red, for: .normal)
            sender.leadingAssistiveLabel.text = "Ingresa un nombre"
        } else {
            validName = true
            if validName && validPhone && validEmail {
                formButton.isEnabled = true
            }
            sender.leadingAssistiveLabel.isHidden = true
            sender.leadingAssistiveLabel.text = ""
            sender.setFilledBackgroundColor(UIColor.clear, for: .normal)
            sender.setFilledBackgroundColor(UIColor.clear, for: .editing)
            sender.setTextColor(UIColor(named: "Base_rede_ligth_gray_titles")!, for: .normal)
            sender.setNormalLabelColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .normal)
            sender.setFloatingLabelColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .normal)
            sender.setUnderlineColor(UIColor(named: "Base_rede_ligth_gray_titles")!, for: .normal)
            sender.setUnderlineColor(UIColor(named: "Base_rede_ligth_gray_titles")!, for: .editing)
        }
    }

    @IBAction func emailDidEndEditing(_ sender: MDCFilledTextField) {
        if !isValidEmail(email: sender.text ?? "") {
            validEmail = false
            formButton.isEnabled = false
            sender.leadingAssistiveLabel.isHidden = false
            sender.setTextColor(UIColor.red, for: .normal)
            sender.setNormalLabelColor(UIColor.red, for: .normal)
            sender.setFloatingLabelColor(UIColor.red, for: .normal)
            sender.setLeadingAssistiveLabelColor(UIColor.red, for: .normal)
            sender.leadingAssistiveLabel.text = "Ingresa un correo válido"
        } else {
            validEmail = true
            if validName && validPhone && validEmail {
                formButton.isEnabled = true
            }
            sender.leadingAssistiveLabel.isHidden = true
            sender.leadingAssistiveLabel.text = ""
            sender.setFilledBackgroundColor(UIColor.clear, for: .normal)
            sender.setFilledBackgroundColor(UIColor.clear, for: .editing)
            sender.setTextColor(UIColor(named: "Base_rede_ligth_gray_titles")!, for: .normal)
            sender.setNormalLabelColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .normal)
            sender.setFloatingLabelColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .normal)
            sender.setUnderlineColor(UIColor(named: "Base_rede_ligth_gray_titles")!, for: .normal)
            sender.setUnderlineColor(UIColor(named: "Base_rede_ligth_gray_titles")!, for: .editing)
        }
    }

    func isValidEmail(email: String) -> Bool {
        let emailRegex = "(?:[\\p{L}0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[\\p{L}0-9!#$%\\&'*+/=?\\^_`{|}" +
        "~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\" +
        "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[\\p{L}0-9](?:[a-" +
        "z0-9-]*[\\p{L}0-9])?\\.)+[\\p{L}0-9](?:[\\p{L}0-9-]*[\\p{L}0-9])?|\\[(?:(?:25[0-5" +
        "]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-" +
        "9][0-9]?|[\\p{L}0-9-]*[\\p{L}0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21" +
        "-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: email)
    }

    func setUpView() {
        formButton.setTitle("Generar Materiales", for: .normal)
        formButton.layer.cornerRadius = 15
        labelTitle.text = "Ingrese sus datos"
        contentView.layer.cornerRadius = 15
        closeButton.setTitle("", for: .normal)
        closeButton.setTitle("", for: .selected)
    }

    func configTextFields() {
        nameTextfield.setFilledBackgroundColor(UIColor.clear, for: .normal)
        nameTextfield.setFilledBackgroundColor(UIColor.clear, for: .disabled)
        nameTextfield.setFilledBackgroundColor(UIColor.clear, for: .editing)
        nameTextfield.setTextColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .normal)
        nameTextfield.setTextColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .editing)
        nameTextfield.setNormalLabelColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .normal)
        nameTextfield.setNormalLabelColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .editing)
        nameTextfield.setFloatingLabelColor(UIColor(named: "Base_rede_ligth_gray_titles")!, for: .normal)
        nameTextfield.setFloatingLabelColor(UIColor(named: "Base_rede_ligth_gray_titles")!, for: .editing)
        numberPhoneTextfield.leadingAssistiveLabel.set(text: "Ingresa tu número")
        emailTextfield.setFilledBackgroundColor(UIColor.clear, for: .normal)
        emailTextfield.setFilledBackgroundColor(UIColor.clear, for: .disabled)
        emailTextfield.setFilledBackgroundColor(UIColor.clear, for: .editing)
        emailTextfield.setTextColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .normal)
        emailTextfield.setTextColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .editing)
        emailTextfield.setNormalLabelColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .normal)
        emailTextfield.setNormalLabelColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .editing)
        emailTextfield.setFloatingLabelColor(UIColor(named: "Base_rede_ligth_gray_titles")!, for: .normal)
        emailTextfield.setFloatingLabelColor(UIColor(named: "Base_rede_ligth_gray_titles")!, for: .editing)
    }
    func enableButton() {
        formButton.backgroundColor = UIColor(named: "Base_rede_button_dark")
        formButton.isEnabled = true
    }

    func disableButton() {
        formButton.backgroundColor = .gray
        formButton.isEnabled = false
    }

}
