//
//  PickerViewReusable.swift
//  SalesCloud
//
//  Created by aestevezn on 16/04/24.
//  Copyright © 2024 TotalPlay. All rights reserved.
//

import UIKit

protocol sendDataSelectedObserver {
    func receiveData(optionSelected: Any)
}

class PickerViewReusable: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var selectedButton: UIButton!

    var observer: sendDataSelectedObserver?
    var dataOptions: [String] = []

    override init(frame: CGRect) {
        super.init(frame: frame)
        internalInit()
        initPicker()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func internalInit() {
        Bundle.main.loadNibNamed("\(PickerViewReusable.self)", owner: self)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }

    private func initPicker() {
        pickerView.delegate = self
        pickerView.dataSource = self
        selectedButton.addTarget(self, action: #selector(selectOption), for: .touchUpInside)
    }

}

extension PickerViewReusable: UIPickerViewDelegate, UIPickerViewDataSource {

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
            return 1 // Una sola columna en el picker
        }

        func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            return dataOptions.count
        }

        func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            return dataOptions[row]
        }

        // MARK: - Actions
        @objc func selectOption() {
            let selectedOption = dataOptions[pickerView.selectedRow(inComponent: 0)]
            observer?.receiveData(optionSelected: selectedOption)
            self.removeFromSuperview()
        }

}
