//
//  ListComissionsTableViewCell.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 07/07/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

class ListComissionsTableViewCell: UITableViewCell {
    @IBOutlet var name: UILabel!
    @IBOutlet var idLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var selectButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
