//
//  DetailsComissionView.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 07/07/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

class DetailsComissionView: BaseVentasView {
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var folioLabel: UILabel!
    @IBOutlet var planLabel: UILabel!
    @IBOutlet var adicionalesLabel: UILabel!
    @IBOutlet var rentaSinIvaLabel: UILabel!
    @IBOutlet var fechaInstalacionLabel: UILabel!
    var mComisionesViewModel: ComisionesEmbajadorMinisitroViewModel!
    var anio: String = ""
    var month: String = ""
    var cuenta: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mComisionesViewModel = ComisionesEmbajadorMinisitroViewModel(view: self)
        mComisionesViewModel.atachView(observer: self)
        setUpview()
    }

    func setUpview() {
        clearData()
        consultDetailEmbajador()
    }

    @IBAction func backAction(_ sender: Any) {
        Constants.Back(viewC: self)
    }

    func consultDetailEmbajador() {
        mComisionesViewModel.detailEmbajador(cuenta: cuenta, mes: month, anio: anio)
    }

    func clearData() {
        nameLabel.text  = "-"
        folioLabel.text  = "-"
        planLabel.text  = "-"
        adicionalesLabel.text  = "-"
        rentaSinIvaLabel.text  = "$ 0"
        fechaInstalacionLabel.text  = "-"
    }

    func setDataEmbajador() {
        let detailEmbajador = mComisionesViewModel.consultDetailEmbajador()
        nameLabel.text  = detailEmbajador.nb_cliente
        folioLabel.text  = detailEmbajador.cd_cuenta
        planLabel.text  = detailEmbajador.nb_paquete
        adicionalesLabel.text  = detailEmbajador.descripcion_addons
        rentaSinIvaLabel.text  = detailEmbajador.ct_rentasin.asLocaleCurrency
        fechaInstalacionLabel.text  = detailEmbajador.fh_activacion
    }

}

extension DetailsComissionView: ComisionesObserver {
    func successGetEmbajador() {
        setDataEmbajador()
    }

    func refreshInfo() {

    }

    func successGetMinistro() {
    }

    func successVentasMinistro() {

    }

    func successMinistroComisionEmbajador() {

    }

    func successEmbajadorComisionMinistro() {

    }

    func successVentasEmbajador() {

    }

    func onError(errorMessage: String) {
        Constants.Alert(title: "", body: errorMessage, type: type.Error, viewC: self)
    }

}
