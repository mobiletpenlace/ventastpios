//
//  ListComissionEmbajadorView.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 07/07/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class ListComissionEmbajadorView: BaseVentasView {
    @IBOutlet var datePayLabel: UILabel!
    @IBOutlet var priceTotalLabel: UILabel!
    @IBOutlet var ListComissionEmbajadorTable: UITableView!
    var mComisionesViewModel: ComisionesEmbajadorMinisitroViewModel!
    var comisiones: [embajadoresComisiones] = []
    var anio: String = ""
    var month: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mComisionesViewModel = ComisionesEmbajadorMinisitroViewModel(view: self)
        mComisionesViewModel.atachView(observer: self)
        setUpview()
    }

    func setUpview() {
       addPullToRefresh()
        clearData()
        setDataDefault()
        consultComisionesEmbajador()
    }

    @IBAction func backAction(_ sender: Any) {
        Constants.Back(viewC: self)
    }

    func addPullToRefresh() {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.clear
        refreshControl.addTarget(self, action: #selector(reloadTableData), for: .valueChanged)
        ListComissionEmbajadorTable.refreshControl = refreshControl
    }

    @objc func reloadTableData(refreshControl: UIRefreshControl) {
        SwiftEventBus.post("RefreshComisiones", sender: nil)
        refreshControl.endRefreshing()
    }

    func goDetailComissionsEmbajador(embajador: embajadoresComisiones) {
        let detailComision: DetailComissionEmbajadorView = UIStoryboard(name: "DetailComissionEmbajadorView", bundle: nil).instantiateViewController(withIdentifier: "DetailComissionEmbajadorView") as!  DetailComissionEmbajadorView
        detailComision.anio = anio
        detailComision.month = month
        detailComision.embajador = embajador
        self.present(detailComision, animated: false, completion: nil)
    }

    func consultComisionesEmbajador() {
        let empleado = mComisionesViewModel.getCurrentNumEmployee()
        mComisionesViewModel.detailMinistro(numEmpleado: empleado, mes: month, anio: anio, tipoConsulta: "3")
    }

    func clearData() {
        comisiones  = []
        datePayLabel.text = "-"
        priceTotalLabel.text = "$ 0.0"
    }

    func setDataDefault() {
        let detailMinistro = mComisionesViewModel.consultDetailMinistro()
        datePayLabel.text = mComisionesViewModel.convertFechaPago(day: detailMinistro.fecha_pago)
    }

}

extension ListComissionEmbajadorView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comisiones.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListComissionsEmbajadorTableViewCell", for: indexPath) as! ListComissionsEmbajadorTableViewCell
        cell.nameLabel.text = comisiones[indexPath.row].nb_embajador
        cell.numSellLabel.text = String(comisiones[indexPath.row].cont_paq)
        cell.priceLabel.text = comisiones[indexPath.row].suma_restantes.asLocaleCurrency
        cell.selectButton.tag = indexPath.row
        cell.selectButton.addTarget(self, action: #selector(handleButtonTapped(sender: )), for: .touchUpInside)
        return cell

    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110

    }

    @objc func handleButtonTapped(sender: UIButton) {
        goDetailComissionsEmbajador(embajador: comisiones[sender.tag])
    }
}

extension ListComissionEmbajadorView: ComisionesObserver {
    func successGetEmbajador() {

    }

    func refreshInfo() {
        consultComisionesEmbajador()
    }

    func successGetMinistro() {
    }

    func successVentasMinistro() {

    }

    func successMinistroComisionEmbajador() {
        let comisionesEmbajador = mComisionesViewModel.consultMinistroComisionEmbajdor()
        self.comisiones = comisionesEmbajador
        let sumTotalVenta = comisionesEmbajador.map({$0.suma_restantes}).reduce(0, +)
        priceTotalLabel.text = sumTotalVenta.asLocaleCurrency
        ListComissionEmbajadorTable.reloadData()
    }

    func successEmbajadorComisionMinistro() {

    }

    func successVentasEmbajador() {

    }

    func onError(errorMessage: String) {
        Constants.Alert(title: "", body: errorMessage, type: type.Error, viewC: self)
    }

}
