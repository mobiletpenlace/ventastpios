//
//  DetailComissionEmbajadorView.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 07/07/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class DetailComissionEmbajadorView: BaseVentasView {
    @IBOutlet var priceTotalLabel: UILabel!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var numSellLabel: UILabel!
    @IBOutlet var ListComissionTable: UITableView!
    @IBOutlet var numFactorLabel: UILabel!
    var mComisionesViewModel: ComisionesEmbajadorMinisitroViewModel!
    var comisiones: [embajadorComisionesMinistro] = []
    var anio: String = ""
    var month: String = ""
    var embajador: embajadoresComisiones?

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mComisionesViewModel = ComisionesEmbajadorMinisitroViewModel(view: self)
        mComisionesViewModel.atachView(observer: self)
        setUpview()
    }

    func setUpview() {
        addPullToRefresh()
        clearData()
        setDataDefault()
        consultComisionesEmbajadorMinistro()
    }

    @IBAction func backAction(_ sender: Any) {
        Constants.Back(viewC: self)
    }

    func addPullToRefresh() {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.clear
        refreshControl.addTarget(self, action: #selector(reloadTableData), for: .valueChanged)
        ListComissionTable.refreshControl = refreshControl
    }

    @objc func reloadTableData(refreshControl: UIRefreshControl) {
        SwiftEventBus.post("RefreshComisiones", sender: nil)
        refreshControl.endRefreshing()
    }

    func goDetailComission(numCuenta: String) {
        let detailComision: DetailsComissionView = UIStoryboard(name: "DetailsComissionView", bundle: nil).instantiateViewController(withIdentifier: "DetailsComissionView") as!  DetailsComissionView
        detailComision.anio = anio
        detailComision.month = month
        detailComision.cuenta = numCuenta
        self.present(detailComision, animated: false, completion: nil)
    }

    func consultComisionesEmbajadorMinistro() {
        mComisionesViewModel.detailMinistro(numEmpleado: embajador?.nu_vendedor ?? "", mes: month, anio: anio, tipoConsulta: "4")
    }

    func clearData() {
        comisiones  = []
        nameLabel.text = "-"
        numSellLabel.text = "0"
        numFactorLabel.text = "X 1"
        priceTotalLabel.text = "$ 0"
    }

    func setDataDefault() {
        guard let emb = embajador else {return }
        nameLabel.text = embajador?.nb_embajador
        numSellLabel.text =  String(emb.cont_paq)
        priceTotalLabel.text = emb.suma_restantes.asLocaleCurrency
    }

}

extension DetailComissionEmbajadorView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comisiones.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailComissionEmbajadorTableViewCell", for: indexPath) as! DetailComissionEmbajadorTableViewCell
        cell.selectButton.tag = indexPath.row
        cell.comisionEjecutivoLabel.text = comisiones[indexPath.row].comisionEmbajador.asLocaleCurrency
        cell.comisionMinistroLabel.text = comisiones[indexPath.row].comisionMinistro.asLocaleCurrency
        cell.name.text = comisiones[indexPath.row].nb_cliente
        cell.idLabel.text = comisiones[indexPath.row].cd_cuenta
        cell.selectButton.addTarget(self, action: #selector(handleButtonTapped(sender: )), for: .touchUpInside)
        return cell

    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110

    }

    @objc func handleButtonTapped(sender: UIButton) {
        goDetailComission(numCuenta: comisiones[sender.tag].cd_cuenta)
    }
}

extension DetailComissionEmbajadorView: ComisionesObserver {
    func successGetEmbajador() {

    }

    func refreshInfo() {
        consultComisionesEmbajadorMinistro()
    }

    func successGetMinistro() {
    }

    func successVentasMinistro() {

    }

    func successMinistroComisionEmbajador() {

    }

    func successEmbajadorComisionMinistro() {
        let comisionesEmbajador = mComisionesViewModel.consultEmbajadorComisionMinistro()
        let arrayComisiones = mComisionesViewModel.consultArrayEmbajadorComisionMinistro()
        self.comisiones = arrayComisiones
        numFactorLabel.text = "X \(comisionesEmbajador.factor)"
        ListComissionTable.reloadData()
    }

    func successVentasEmbajador() {

    }

    func onError(errorMessage: String) {
        Constants.Alert(title: "", body: errorMessage, type: type.Error, viewC: self)
    }

}
