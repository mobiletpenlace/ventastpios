//
//  ListComissionsView.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 07/07/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class ListComissionsView: BaseVentasView {
    @IBOutlet var priceTotalComissionsLabel: UILabel!
    @IBOutlet var datePayLabel: UILabel!
    @IBOutlet var ListComissionTable: UITableView!
    @IBOutlet var numFactorLabel: UILabel!
    var mComisionesViewModel: ComisionesEmbajadorMinisitroViewModel!
    var anio: String = ""
    var month: String = ""
    var comisiones: [ministroComisiones] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mComisionesViewModel = ComisionesEmbajadorMinisitroViewModel(view: self)
        mComisionesViewModel.atachView(observer: self)
        setUpview()
    }

    func setUpview() {
        addPullToRefresh()
        clearData()
        setDataDefault()
        consultComisionesMinistro()
    }

    @IBAction func backAction(_ sender: Any) {
        Constants.Back(viewC: self)
    }

    func goDetailComissions(numCuenta: String) {
        let detailComision: DetailsComissionView = UIStoryboard(name: "DetailsComissionView", bundle: nil).instantiateViewController(withIdentifier: "DetailsComissionView") as!  DetailsComissionView
        detailComision.anio = anio
        detailComision.month = month
        detailComision.cuenta = numCuenta
        self.present(detailComision, animated: false, completion: nil)
    }

    func consultComisionesMinistro() {
        let empleado = mComisionesViewModel.getCurrentNumEmployee()
        mComisionesViewModel.detailMinistro(numEmpleado: empleado, mes: month, anio: anio, tipoConsulta: "2")
    }

    func addPullToRefresh() {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.clear
        refreshControl.addTarget(self, action: #selector(reloadTableData), for: .valueChanged)
        ListComissionTable.refreshControl = refreshControl
    }

    @objc func reloadTableData(refreshControl: UIRefreshControl) {
        refreshControl.endRefreshing()
        SwiftEventBus.post("RefreshComisiones", sender: nil)

    }

    func clearData() {
        comisiones  = []
        datePayLabel.text = "-"
        priceTotalComissionsLabel.text = "$ 0"
    }

    func setDataDefault() {
        let detailMinistro = mComisionesViewModel.consultDetailMinistro()
        datePayLabel.text = mComisionesViewModel.convertFechaPago(day: detailMinistro.fecha_pago)
    }

}

extension ListComissionsView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comisiones.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListComissionsTableViewCell", for: indexPath) as! ListComissionsTableViewCell
        cell.name.text = comisiones[indexPath.row].nb_cliente
        cell.priceLabel.text = comisiones[indexPath.row].comision.asLocaleCurrency
        cell.idLabel.text = comisiones[indexPath.row].cd_cuenta
        cell.selectButton.tag = indexPath.row
        cell.selectButton.addTarget(self, action: #selector(handleButtonTapped(sender: )), for: .touchUpInside)
        return cell

    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100

    }

    @objc func handleButtonTapped(sender: UIButton) {
        goDetailComissions(numCuenta: comisiones[sender.tag].cd_cuenta)
    }
}

extension ListComissionsView: ComisionesObserver {
    func refreshInfo() {
        consultComisionesMinistro()
    }

    func successGetEmbajador() {

    }

    func successGetMinistro() {
    }

    func successVentasMinistro() {
        let ventaComisiones = mComisionesViewModel.consultVentasMinistro()
        self.comisiones = ventaComisiones
        let sumTotalVenta = ventaComisiones.map({$0.comision}).reduce(0, +)
        priceTotalComissionsLabel.text = sumTotalVenta.asLocaleCurrency
        ListComissionTable.reloadData()
    }

    func successMinistroComisionEmbajador() {

    }

    func successEmbajadorComisionMinistro() {

    }

    func successVentasEmbajador() {

    }

    func onError(errorMessage: String) {
        Constants.Alert(title: "", body: errorMessage, type: type.Error, viewC: self)
    }

}
