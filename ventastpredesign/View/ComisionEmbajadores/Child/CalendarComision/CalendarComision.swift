//
//  CalendarComision.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 27/07/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

protocol getDateComisionDelegate {
    func getDate(date: Date )
}
class CalendarComision: UIViewController {
    @IBOutlet var datePickerm: UIDatePicker!
    @IBOutlet var viewPrincipal: UIView!
    var getDateComisionDelegate: getDateComisionDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        config()

    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first as! UITouch
        if touch.view != viewPrincipal {
            Constants.Back(viewC: self)
        }
    }

    func config() {
        datePickerm.datePickerMode = .date
        if #available(iOS 13.4, *) {
            datePickerm.preferredDatePickerStyle = .wheels
        }
        guard let dateFinish = DateCalendar.pastDate(date: Date(), value: 1, component: .month) else { return }
        datePickerm.maximumDate = dateFinish
    }

    @IBAction func accept(_ sender: Any) {
        print( datePickerm.date)
        getDateComisionDelegate?.getDate(date: datePickerm.date )
        Constants.Back(viewC: self)
    }

}
