//
//  HomeComissionsEmbajadorView.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 07/07/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class HomeComissionsEmbajadorView: BaseVentasView, getDateComisionDelegate {
    @IBOutlet var dateMonthLabel: UILabel!
    @IBOutlet var dateYearLabel: UILabel!
    @IBOutlet var priceTotalComissionsLabel: UILabel!
    @IBOutlet var datePayLabel: UILabel!
    @IBOutlet var ListComissionTable: UITableView!
    @IBOutlet var numFactorLabel: UILabel!
    @IBOutlet var numVentaLabel: UILabel!
    @IBOutlet var totalVentaLabel: UILabel!
    var mComisionesViewModel: ComisionesEmbajadorMinisitroViewModel!
    var comision: [ministroComisiones] = []
    var monthCurrent: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mComisionesViewModel = ComisionesEmbajadorMinisitroViewModel(view: self)
        mComisionesViewModel.atachView(observer: self)
        setUpview()
    }

    func setUpview() {
        addPullToRefresh()
        clearData()
        setDate()
        consultComisionEmbajadores()
    }

    @IBAction func backAction(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }

    @IBAction func selectDate(_ sender: Any) {
        let listaComision: CalendarComision = UIStoryboard(name: "CalendarComision", bundle: nil).instantiateViewController(withIdentifier: "CalendarComision") as!  CalendarComision
        listaComision.getDateComisionDelegate = self
        listaComision.providesPresentationContextTransitionStyle = true
        listaComision.definesPresentationContext = true
        listaComision.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        listaComision.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(listaComision, animated: true, completion: nil)
    }

    func getDate(date: Date) {
        let (month, year, monthInt) =  mComisionesViewModel.getMonthYearCalendar(selectDate: date)
        if dateMonthLabel.text != month.uppercased() || dateYearLabel.text != year {
            dateMonthLabel.text = month.uppercased()
            dateYearLabel.text = year
            monthCurrent = String(monthInt)
            consultComisionEmbajadores()
        } else {
            print("no cambio la fecha")
        }

    }

    func addPullToRefresh() {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.clear
        refreshControl.addTarget(self, action: #selector(reloadTableData), for: .valueChanged)
        ListComissionTable.refreshControl = refreshControl
    }

    @objc func reloadTableData(refreshControl: UIRefreshControl) {
        SwiftEventBus.post("RefreshComisiones", sender: nil)
        refreshControl.endRefreshing()
    }

    func goDetailComissions(numCuenta: String) {
        let detailComision: DetailsComissionView = UIStoryboard(name: "DetailsComissionView", bundle: nil).instantiateViewController(withIdentifier: "DetailsComissionView") as!  DetailsComissionView
        detailComision.anio =  dateYearLabel.text ?? "2021"
        detailComision.month =  monthCurrent
        detailComision.cuenta = numCuenta
        self.present(detailComision, animated: false, completion: nil)
    }

    func consultComisionEmbajadores() {
        let empleado = mComisionesViewModel.getCurrentNumEmployee()
        mComisionesViewModel.detailMinistro(numEmpleado: empleado, mes: monthCurrent, anio: dateYearLabel.text ?? "2021", tipoConsulta: "6")
    }

    func clearData() {
        numVentaLabel.text = "0"
        totalVentaLabel.text = "$ 0"
        datePayLabel.text = "-"
        numFactorLabel.text = "X 1"
        priceTotalComissionsLabel.text = "$ 0"
        comision = []
        ListComissionTable.reloadData()
    }

    func setDate() {
        let (month, year, monthInt) =  mComisionesViewModel.getMonthYearDay()
        dateMonthLabel.text = month.uppercased()
        dateYearLabel.text = year
        monthCurrent = String(monthInt)

    }

}

extension HomeComissionsEmbajadorView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comision.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListComissionsTableViewCell", for: indexPath) as! ListComissionsTableViewCell
        cell.name.text = comision[indexPath.row].nb_cliente
        cell.priceLabel.text = comision[indexPath.row].comision.asLocaleCurrency
        cell.idLabel.text = comision[indexPath.row].cd_cuenta
        cell.selectButton.tag = indexPath.row
        cell.selectButton.addTarget(self, action: #selector(handleButtonTapped(sender: )), for: .touchUpInside)
        return cell

    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100

    }

    @objc func handleButtonTapped(sender: UIButton) {
        goDetailComissions(numCuenta: comision[sender.tag].cd_cuenta)
    }
}

extension HomeComissionsEmbajadorView: ComisionesObserver {
    func successGetEmbajador() {

    }

    func refreshInfo() {
        consultComisionEmbajadores()
    }

    func successGetMinistro() {
    }

    func successVentasMinistro() {

    }

    func successMinistroComisionEmbajador() {

    }

    func successEmbajadorComisionMinistro() {

    }

    func successVentasEmbajador() {
        let ventaEmbajador = mComisionesViewModel.consultVentasEmbajador()
        let arrayComisiones = mComisionesViewModel.consultVentasMinistro()
       self.comision = arrayComisiones
        numVentaLabel.text = String(arrayComisiones.count)
        numFactorLabel.text = "X \(ventaEmbajador.factor)"
        let sumTotalVenta = arrayComisiones.map({$0.comision}).reduce(0, +)
        totalVentaLabel.text = sumTotalVenta.asLocaleCurrency
        datePayLabel.text = mComisionesViewModel.convertFechaPago(day: ventaEmbajador.fh_pago)
        priceTotalComissionsLabel.text = sumTotalVenta.asLocaleCurrency
        ListComissionTable.reloadData()
    }

    func onError(errorMessage: String) {
        clearData()
        Constants.Alert(title: "", body: errorMessage, type: type.Error, viewC: self)
//        Constants.Alert(title: "", body: errorMessage, type: type.Error, viewC: self, dimissParent: true)
    }

}
