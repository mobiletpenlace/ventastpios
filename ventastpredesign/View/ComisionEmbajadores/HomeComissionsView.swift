//
//  HomeComissionsView.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 07/07/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

class HomeComissionsView: BaseVentasView, getDateComisionDelegate {

    @IBOutlet var priceMyComissionsLabel: UILabel!
    @IBOutlet var priceMyComissionsEmbajadorLabel: UILabel!
    @IBOutlet var priceTotalComissionsLabel: UILabel!
    @IBOutlet var datePayLabel: UILabel!
    @IBOutlet var dateMonthLabel: UILabel!
    @IBOutlet var dateYearLabel: UILabel!
    var mComisionesViewModel: ComisionesEmbajadorMinisitroViewModel!
    var monthCurrent: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mComisionesViewModel = ComisionesEmbajadorMinisitroViewModel(view: self)
        mComisionesViewModel.atachView(observer: self)
        setUpview()
    }

    func setUpview() {

        clearData()
        setDate()
        consultDetailMinistro()
    }

    @IBAction func backAction(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }

    @IBAction func goMyComissions(_ sender: Any) {
        let listaComision: ListComissionsView = UIStoryboard(name: "ListComissionsView", bundle: nil).instantiateViewController(withIdentifier: "ListComissionsView") as!  ListComissionsView
        listaComision.anio = dateYearLabel.text ?? "2021"
        listaComision.month = monthCurrent
        self.present(listaComision, animated: false, completion: nil)

    }

    @IBAction func goMyComissionsEmbajadores(_ sender: Any) {
        let listaComisionE: ListComissionEmbajadorView = UIStoryboard(name: "ListComissionEmbajadorView", bundle: nil).instantiateViewController(withIdentifier: "ListComissionEmbajadorView") as!  ListComissionEmbajadorView
        listaComisionE.anio = dateYearLabel.text ?? "2021"
        listaComisionE.month = monthCurrent
        self.present(listaComisionE, animated: false, completion: nil)
    }

    @IBAction func selectDate(_ sender: Any) {
        let listaComision: CalendarComision = UIStoryboard(name: "CalendarComision", bundle: nil).instantiateViewController(withIdentifier: "CalendarComision") as!  CalendarComision
        listaComision.getDateComisionDelegate = self
        listaComision.providesPresentationContextTransitionStyle = true
        listaComision.definesPresentationContext = true
        listaComision.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        listaComision.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(listaComision, animated: true, completion: nil)
    }

    func getDate(date: Date) {
        let (month, year, monthInt) =  mComisionesViewModel.getMonthYearCalendar(selectDate: date)
        if dateMonthLabel.text != month.uppercased() || dateYearLabel.text != year {
            dateMonthLabel.text = month.uppercased()
            dateYearLabel.text = year
            monthCurrent = String(monthInt)
            consultDetailMinistro()
        } else {
            print("no cambio la fecha")
        }

    }

    func consultDetailMinistro() {
        let empleado = mComisionesViewModel.getCurrentNumEmployee()
        mComisionesViewModel.detailMinistro(numEmpleado: empleado, mes: monthCurrent, anio: dateYearLabel.text ?? "2021", tipoConsulta: "1")
    }

    func setDate() {
        let (month, year, monthInt) =  mComisionesViewModel.getMonthYearDay()
        dateMonthLabel.text = month.uppercased()
        dateYearLabel.text = year
        monthCurrent = String(monthInt)

    }

    func clearData() {
        priceMyComissionsLabel.text = "$ 0"
        priceTotalComissionsLabel.text = "$ 0"
        priceMyComissionsEmbajadorLabel.text = "$ 0"
        datePayLabel.text = ""
    }

    func setDataComisiones() {
       let comisiones =   mComisionesViewModel.consultDetailMinistro()
        priceMyComissionsLabel.text = comisiones.mis_comisiones.asLocaleCurrency
        priceTotalComissionsLabel.text = comisiones.total_comisiones.asLocaleCurrency
        priceMyComissionsEmbajadorLabel.text = comisiones.comisiones_embajadores.asLocaleCurrency
        datePayLabel.text =  mComisionesViewModel.convertFechaPago(day: comisiones.fecha_pago)
    }

}

extension HomeComissionsView: ComisionesObserver {
    func refreshInfo() {

    }

    func successGetEmbajador() {

    }

    func successGetMinistro() {
        self.setDataComisiones()
    }

    func successVentasMinistro() {

    }

    func successMinistroComisionEmbajador() {

    }

    func successEmbajadorComisionMinistro() {

    }

    func successVentasEmbajador() {

    }

    func onError(errorMessage: String) {
        clearData()
        Constants.Alert(title: "", body: errorMessage, type: type.Error, viewC: self)
//        Constants.Alert(title: "", body: errorMessage, type: type.Error, viewC: self, dimissParent: true)

    }

}
