//
//  DetailMinistroRequest.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 23/07/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class DetailMinistroRequest: BaseRequest {

    var numEmpleado: String = ""
    var mes: String = ""
    var anio: String = ""
    var tipoConsulta: String = ""

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        numEmpleado <- map["numEmpleado"]
        mes <- map["mes"]
        anio <- map["anio"]
        tipoConsulta <- map["tipoConsulta"]
    }

}
