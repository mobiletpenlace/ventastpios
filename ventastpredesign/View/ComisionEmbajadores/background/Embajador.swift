//
//  Embajador.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 26/07/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import RealmSwift

class Embajador: Object, Mappable {
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  nb_cliente: String = ""
    @objc dynamic var  ct_rentasin: Double = 0.0
    @objc dynamic var  fh_activacion: String = ""
    @objc dynamic var  nb_paquete: String = ""
    @objc dynamic var  cd_cuenta: String = ""
    @objc dynamic var  descripcion_addons: String = ""

    public required convenience init?(map: Map) {
        self.init()
    }

    override static func primaryKey() -> String? {
           return "uui"
    }

    func mapping(map: Map) {
        nb_cliente <- map["nb_cliente"]
        ct_rentasin <- map["ct_rentasin"]
        fh_activacion <- map["fh_activacion"]
        nb_paquete <- map["nb_paquete"]
        cd_cuenta <- map["cd_cuenta"]
        descripcion_addons <- map["descripcion_addons"]

    }
}
