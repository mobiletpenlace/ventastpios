//
//  embajadoresComisiones.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 23/07/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import RealmSwift

class embajadoresComisiones: Object, Mappable {
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  nb_embajador: String = ""
    @objc dynamic var  suma_restantes: Double = 0.0
    @objc dynamic var  nu_vendedor: String = ""
    @objc dynamic var  cont_paq: Int = 0

    public required convenience init?(map: Map) {
        self.init()
    }

    override static func primaryKey() -> String? {
           return "uui"
    }

    func mapping(map: Map) {
        nb_embajador <- map["nb_embajador"]
        suma_restantes <- map["suma_restantes"]
        nu_vendedor <- map["nu_vendedor"]
        cont_paq <- map["cont_paq"]

    }
}
