//
//  embajadorComisionesMinistro.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 23/07/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import RealmSwift

class embajadorComisionesMinistro: Object, Mappable {
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  nb_cliente: String = ""
    @objc dynamic var  comisionMinistro: Double = 0.0
    @objc dynamic var  comisionEmbajador: Double = 0.0
    @objc dynamic var  cd_cuenta: String = ""

    public required convenience init?(map: Map) {
        self.init()
    }

    override static func primaryKey() -> String? {
           return "uui"
    }

    func mapping(map: Map) {
        nb_cliente <- map["nb_cliente"]
        comisionMinistro <- map["comisionMinistro"]
        comisionEmbajador <- map["comisionEmbajador"]
        cd_cuenta <- map["cd_cuenta"]

    }
}
