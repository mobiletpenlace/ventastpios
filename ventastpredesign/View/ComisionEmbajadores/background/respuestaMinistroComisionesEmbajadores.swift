//
//  respuestaMinistroComisionesEmbajadores.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 23/07/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift

class respuestaMinistroComisionesEmbajadores: Object, Mappable {
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  result: String = ""
 //   var mEmbajadoresComisiones : List<embajadoresComisiones> = List<embajadoresComisiones>()
    var mEmbajadoresComisiones: [embajadoresComisiones] = []
    @objc dynamic var  resultDescription: String = ""

    public required convenience init?(map: Map) {
        self.init()
    }

    override static func primaryKey() -> String? {
           return "uui"
    }

    func mapping(map: Map) {
        result <- map["result"]
        mEmbajadoresComisiones <- map["embajadoresComisiones"]
        resultDescription <- map["resultDescription"]

    }
}
