//
//  respuestaMinistroComisiones.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 23/07/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift

class respuestaMinistroComisiones: Object, Mappable {
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  result: String = ""
    var mMinistroComisiones: [ministroComisiones] = []
  //  var mMinistroComisiones : List<ministroComisiones> = List<ministroComisiones>()
    @objc dynamic var  resultDescription: String = ""

    public required convenience init?(map: Map) {
        self.init()
    }

    override static func primaryKey() -> String? {
           return "uui"
    }

    func mapping(map: Map) {
        result <- map["result"]
        mMinistroComisiones <- map["ministroComisiones"]
        resultDescription <- map["resultDescription"]

    }
}
