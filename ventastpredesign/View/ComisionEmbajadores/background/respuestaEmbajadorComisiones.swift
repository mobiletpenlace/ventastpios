//
//  respuestaEmbajadorComisiones.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 23/07/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift

class respuestaEmbajadorComisiones: Object, Mappable {
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  result: String = ""
    @objc dynamic var  resultDescription: String = ""
    @objc dynamic var  factor: Double = 0.0
    @objc dynamic var  fh_pago: String = ""
   // var mMinistroComisiones : List<ministroComisiones> = List<ministroComisiones>()
    var mMinistroComisiones: [ministroComisiones] = []

    public required convenience init?(map: Map) {
        self.init()
    }

    override static func primaryKey() -> String? {
           return "uui"
    }

    func mapping(map: Map) {
        result <- map["result"]
        factor <- map["factor"]
        fh_pago <- map["fh_pago"]
        mMinistroComisiones <- map["ministroComisiones"]
        resultDescription <- map["resultDescription"]

    }
}
