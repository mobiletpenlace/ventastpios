//
//  DetailEmbajadorResponse.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 23/07/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class DetailEmbajadorResponse: BaseResponse {
    var ct_rentasin: Double = 0.0
    var mResult: String = ""
    var nb_cliente: String = ""
    var fh_activacion: String = ""
    var nb_paquete: String = ""
    var cd_cuenta: String = ""
    var descripcion_addons: String = ""
    var mResultDescription: String?

     required init?(map: Map) {

       }

    override func mapping(map: Map) {
        ct_rentasin <- map["ct_rentasin"]
        mResult <- map["result"]
        nb_cliente <- map["nb_cliente"]
        fh_activacion <- map["fh_activacion"]
        nb_paquete <- map["nb_paquete"]
        cd_cuenta <- map["cd_cuenta"]
        descripcion_addons <- map["descripcion_addons"]
        mResultDescription <- map["resultDescription"]

    }

}
