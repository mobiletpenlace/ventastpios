//
//  DetailMinistroResponse.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 23/07/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class DetailMinistroResponse: BaseResponse {
    var mRespuestaMinistroGnrlVo: respuestaMinistroGnrlVo?
    var mRespuestaMinistroComisiones: respuestaMinistroComisiones?
    var mRespuestaMinistroComisionesEmbajadores: respuestaMinistroComisionesEmbajadores?
    var mRespuestaEmbajadoresComisionesMinistro: respuestaEmbajadoresComisionesMinistro?
    var mRespuestaEmbajadorComisiones: respuestaEmbajadorComisiones?

     required init?(map: Map) {

       }

    override func mapping(map: Map) {
        mRespuestaMinistroGnrlVo <- map["respuestaMinistroGnrlVo"]
        mRespuestaMinistroComisiones <- map["respuestaMinistroComisiones"]
        mRespuestaMinistroComisionesEmbajadores <- map["respuestaMinistroComisionesEmbajadores"]
        mRespuestaEmbajadoresComisionesMinistro <- map["respuestaEmbajadoresComisionesMinistro"]
        mRespuestaEmbajadorComisiones <- map["respuestaEmbajadorComisiones"]

    }

}
