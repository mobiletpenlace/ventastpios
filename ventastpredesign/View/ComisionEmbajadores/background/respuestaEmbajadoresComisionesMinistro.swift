//
//  respuestaEmbajadoresComisionesMinistro.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 23/07/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift

class respuestaEmbajadoresComisionesMinistro: Object, Mappable {
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  result: String = ""
    @objc dynamic var  resultDescription: String = ""
    @objc dynamic var  factor: Double = 0.0
   // var mEmbajadorComisionesMinistro : List<embajadorComisionesMinistro> = List<embajadorComisionesMinistro>()
    var mEmbajadorComisionesMinistro: [embajadorComisionesMinistro] = []

    public required convenience init?(map: Map) {
        self.init()
    }

    override static func primaryKey() -> String? {
           return "uui"
    }

    func mapping(map: Map) {
        result <- map["result"]
        factor <- map["factor"]
        mEmbajadorComisionesMinistro <- map["embajadorComisionesMinistro"]
        resultDescription <- map["resultDescription"]

    }
}
