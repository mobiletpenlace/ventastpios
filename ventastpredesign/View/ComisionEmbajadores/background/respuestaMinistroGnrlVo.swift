//
//  respuestaMinistroGnrlVo.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 23/07/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift

class respuestaMinistroGnrlVo: Object, Mappable {
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  result: String = ""
    @objc dynamic var  mis_comisiones: Double = 0.0
    @objc dynamic var  resultDescription: String?
    @objc dynamic var  total_comisiones: Double = 0.0
    @objc dynamic var  comisiones_embajadores: Double = 0.0
    @objc dynamic var  fecha_pago: String = ""

    public required convenience init?(map: Map) {
        self.init()
    }

    override static func primaryKey() -> String? {
           return "uui"
    }

    func mapping(map: Map) {
        result <- map["result"]
        mis_comisiones <- map["mis_comisiones"]
        resultDescription <- map["resultDescription"]
        total_comisiones <- map["total_comisiones"]
        comisiones_embajadores <- map["comisiones_embajadores"]
        fecha_pago <- map["fecha_pago"]

    }
}
