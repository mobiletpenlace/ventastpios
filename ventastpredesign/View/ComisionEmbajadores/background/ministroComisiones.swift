//
//  ministroComisiones.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 23/07/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import RealmSwift

class ministroComisiones: Object, Mappable {
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  nb_cliente: String = ""
    @objc dynamic var  comision: Double = 0.0
    @objc dynamic var  cd_cuenta: String = ""

    public required convenience init?(map: Map) {
        self.init()
    }

    override static func primaryKey() -> String? {
           return "uui"
    }

    func mapping(map: Map) {
        nb_cliente <- map["nb_cliente"]
        comision <- map["comision"]
        cd_cuenta <- map["cd_cuenta"]

    }
}
