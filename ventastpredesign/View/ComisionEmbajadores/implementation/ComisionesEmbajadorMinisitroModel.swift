//
//  ComisionesEmbajadorMinisitroModel.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 26/07/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import RealmSwift

protocol ComisionesModelObserver {
    func successGetEmbajador()
    func successGetMinistro()
    func successVentasMinistro()
    func successMinistroComisionEmbajador()
    func successEmbajadorComisionMinistro()
    func successVentasEmbajador()
    func onError(errorMessage: String)
}

class ComisionesEmbajadorMinisitroModel: BaseModel {

    var viewModel: ComisionesModelObserver?
    var server: ServerDataManager2<DetailMinistroResponse>?
    var server2: ServerDataManager2<DetailEmbajadorResponse>?
    var urlMinistro = AppDelegate.API_TOTAL + ApiDefinition.API_DETAIL_MINISTRO
    var urlEmbajador = AppDelegate.API_TOTAL + ApiDefinition.API_DETAIL_ACCOUNT_EMBAJADOR
    var typeService: String = ""

    init(observer: BaseViewModelObserver) {
        super.init(observerVM: observer)
        server = ServerDataManager2<DetailMinistroResponse>()
        server2 = ServerDataManager2<DetailEmbajadorResponse>()
    }

    func atachModel(viewModel: ComisionesModelObserver) {
        self.viewModel = viewModel
    }

    func detailEmbajador(request: DetailEmbajadorRequest) {
        urlEmbajador = AppDelegate.API_TOTAL + ApiDefinition.API_DETAIL_ACCOUNT_EMBAJADOR
        server2?.setValues(requestUrl: urlEmbajador, delegate: self, headerType: .headersFfmap)
        server2?.request(requestModel: request)
    }

    func detailMinistro(request: DetailMinistroRequest) {
        urlMinistro = AppDelegate.API_TOTAL + ApiDefinition.API_DETAIL_MINISTRO
        self.typeService = request.tipoConsulta
        server?.setValues(requestUrl: urlMinistro, delegate: self, headerType: .headersFfmap)
        server?.request(requestModel: request)
    }

    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        print(requestUrl)
        print(response)
        if requestUrl == urlEmbajador {
            print("url embajador")
            let embajadorResponse = response as! DetailEmbajadorResponse
            if embajadorResponse.mResult == "0" {
                saveDetailEmbajador(response: embajadorResponse)
                self.viewModel?.successGetEmbajador()
            } else {
                viewModel?.onError(errorMessage: embajadorResponse.mResultDescription ?? "Error al consultar el embajador")
            }
        } else if requestUrl == urlMinistro {
            print("url ministro")
            let ministroResponse = response as! DetailMinistroResponse
            OnSuccesDetailMinistro(response: ministroResponse)

        }
    }

    func OnSuccesDetailMinistro(response: DetailMinistroResponse) {
        switch self.typeService {
        case "1":
            if response.mRespuestaMinistroGnrlVo != nil && response.mRespuestaMinistroGnrlVo?.result == "0" {
                saveDetailMinistro(response: response)
                self.viewModel?.successGetMinistro()
            } else {
                if response.mRespuestaMinistroGnrlVo?.total_comisiones == 0 {
                    viewModel?.onError(errorMessage: "No se encontraron comisiones en el rango de fechas" )

                } else {
                    viewModel?.onError(errorMessage: "Error al consultar las comisiones" )
                }
            }
        case "2":
            if response.mRespuestaMinistroComisiones != nil && response.mRespuestaMinistroComisiones?.result == "0" {
                saveVentaMinistro(response: response)
                self.viewModel?.successVentasMinistro()
            } else {
                viewModel?.onError(errorMessage: "Error al consultar las comisiones" )
            }
        case "3":
            if response.mRespuestaMinistroComisionesEmbajadores != nil && response.mRespuestaMinistroComisionesEmbajadores?.result == "0" {
                saveMinistroComisionEmbajador(response: response)
                self.viewModel?.successMinistroComisionEmbajador()
            } else {
                viewModel?.onError(errorMessage: "Error al consultar las comisiones" )
            }
        case "4":
            if response.mRespuestaEmbajadoresComisionesMinistro != nil && response.mRespuestaEmbajadoresComisionesMinistro?.result == "0" {
                saveEmbajadorComisionMinistro(response: response)
                self.viewModel?.successEmbajadorComisionMinistro()
            } else {
                viewModel?.onError(errorMessage: "Error al consultar las comisiones" )
            }
        case "6":
            if response.mRespuestaEmbajadorComisiones != nil && response.mRespuestaEmbajadorComisiones?.result == "0" {
                saveVentaEmbajador(response: response)
                self.viewModel?.successVentasEmbajador()
            } else {
                viewModel?.onError(errorMessage: "Error al consultar las comisiones" )
            }
        default:
            break
        }
    }

    func saveDetailEmbajador(response: DetailEmbajadorResponse) {
        RealManager.deleteAll(object: Embajador.self)
        let detailEmb = Embajador()
        detailEmb.ct_rentasin = response.ct_rentasin
        detailEmb.nb_cliente = response.nb_cliente
        detailEmb.fh_activacion = response.fh_activacion
        detailEmb.nb_paquete = response.nb_paquete
        detailEmb.cd_cuenta = response.cd_cuenta
        detailEmb.descripcion_addons = response.descripcion_addons
        _ = RealManager.insert(object: detailEmb)
      //  print(RealManager.getAll(object: Embajador.self))
    }

    func saveDetailMinistro(response: DetailMinistroResponse) {
        RealManager.deleteAll(object: respuestaMinistroGnrlVo.self)
        guard let ministro = response.mRespuestaMinistroGnrlVo else {return }
        var detailMinistro = respuestaMinistroGnrlVo()
        detailMinistro =  ministro

        _ = RealManager.insert(object: detailMinistro)
      //  print(RealManager.getAll(object: respuestaMinistroGnrlVo.self))
    }

    func saveVentaMinistro(response: DetailMinistroResponse) {
        RealManager.deleteAll(object: ministroComisiones.self)
        guard let ventaMinistro = response.mRespuestaMinistroComisiones else {return }
        var listaComisiones = [ministroComisiones]()
        listaComisiones.append(contentsOf: ventaMinistro.mMinistroComisiones)
        RealManager.insertCollection(objects: listaComisiones)
      //  print(RealManager.getAll(object: ministroComisiones.self))
    }

    func saveVentaEmbajador(response: DetailMinistroResponse) {
        RealManager.deleteAll(object: respuestaEmbajadorComisiones.self)
        RealManager.deleteAll(object: ministroComisiones.self)
        guard let ventaEmbajador = response.mRespuestaEmbajadorComisiones else {return }
        var comisionesEmbajador = respuestaEmbajadorComisiones()
        comisionesEmbajador = ventaEmbajador
        var listaComisiones = [ministroComisiones]()
        listaComisiones.append(contentsOf: ventaEmbajador.mMinistroComisiones)
        RealManager.insertCollection(objects: listaComisiones)
        _ = RealManager.insert(object: comisionesEmbajador)
       // print(RealManager.getAll(object: respuestaEmbajadorComisiones.self))
      //  print(RealManager.getAll(object: ministroComisiones.self))
    }

    func saveMinistroComisionEmbajador(response: DetailMinistroResponse) {
        RealManager.deleteAll(object: embajadoresComisiones.self)
        guard let ministroComision = response.mRespuestaMinistroComisionesEmbajadores else {return }
        var listaComisiones = [embajadoresComisiones]()
        listaComisiones.append(contentsOf: ministroComision.mEmbajadoresComisiones)
        RealManager.insertCollection(objects: listaComisiones)
      //  print(RealManager.getAll(object: embajadoresComisiones.self))
    }

    func saveEmbajadorComisionMinistro(response: DetailMinistroResponse) {
        RealManager.deleteAll(object: respuestaEmbajadoresComisionesMinistro.self)
        RealManager.deleteAll(object: embajadorComisionesMinistro.self)
        guard let embajadorComision = response.mRespuestaEmbajadoresComisionesMinistro else {return }
        var comisionesEmbajador = respuestaEmbajadoresComisionesMinistro()
        comisionesEmbajador = embajadorComision
        var listaComisiones = [embajadorComisionesMinistro]()
        listaComisiones.append(contentsOf: comisionesEmbajador.mEmbajadorComisionesMinistro)
        RealManager.insertCollection(objects: listaComisiones)
        _ = RealManager.insert(object: comisionesEmbajador)
      //  print(RealManager.getAll(object: respuestaEmbajadoresComisionesMinistro.self))
      //  print(RealManager.getAll(object: embajadorComisionesMinistro.self))
    }

    func getDetailEmbajador() -> Embajador {
        let embajador = RealManager.findFirst(object: Embajador.self)
        return embajador!
    }

    func getDetailMinistro() -> respuestaMinistroGnrlVo {
        let detailMinistro = RealManager.findFirst(object: respuestaMinistroGnrlVo.self)
        return detailMinistro!
    }

    func getVentasEmbajador() -> respuestaEmbajadorComisiones {
        let ventasEmbajador = RealManager.findFirst(object: respuestaEmbajadorComisiones.self)
        return ventasEmbajador!
    }

    func getVentasMinistro() -> [ministroComisiones] {
        let ventasMinistro = RealManager.getAll(object: ministroComisiones.self)
        return ventasMinistro
    }

    func getEmbajadorComisionMinistro() -> respuestaEmbajadoresComisionesMinistro {
        let comision = RealManager.findFirst(object: respuestaEmbajadoresComisionesMinistro.self)
        return comision!
    }

    func getEmbajadorComisionMinistroArray() -> [embajadorComisionesMinistro] {
        let comision = RealManager.getAll(object: embajadorComisionesMinistro.self)
        return comision
    }

    func getMinistroComisionEmbajdor() -> [embajadoresComisiones] {
        let comision = RealManager.getAll(object: embajadoresComisiones.self)
        return comision
    }

    func getEmploye() -> Empleado {
        let employee = RealManager.findFirst(object: Empleado.self)
        return employee!
    }

    override func onErrorLoadResponse(requestUrl: String, messageError: String) {
        viewModel?.onError(errorMessage: messageError)
    }

}
