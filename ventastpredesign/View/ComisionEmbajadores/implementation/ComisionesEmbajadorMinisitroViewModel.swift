//
//  ComisionesEmbajadorMinisitroViewModel.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 26/07/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus

protocol ComisionesObserver {
    func refreshInfo()
    func successGetEmbajador()
    func successGetMinistro()
    func successVentasMinistro()
    func successMinistroComisionEmbajador()
    func successEmbajadorComisionMinistro()
    func successVentasEmbajador()
    func onError(errorMessage: String)
}

class ComisionesEmbajadorMinisitroViewModel: BaseViewModel, ComisionesModelObserver {

    var observer: ComisionesObserver?
    var model: ComisionesEmbajadorMinisitroModel!

    override init(view: BaseVentasView) {
        super.init(view: view)
        model = ComisionesEmbajadorMinisitroModel(observer: self)
        model.atachModel(viewModel: self)
    }

    func atachView(observer: ComisionesObserver) {
        self.observer = observer
        SwiftEventBus.onMainThread(self, name: "RefreshComisiones") {[unowned self] _ in
            self.observer?.refreshInfo()
        }
    }

    func detailMinistro(numEmpleado: String, mes: String, anio: String, tipoConsulta: String) {

        let ministroRequest = DetailMinistroRequest()
        ministroRequest.anio = anio
        ministroRequest.mes = mes
        ministroRequest.numEmpleado = numEmpleado
        ministroRequest.tipoConsulta = tipoConsulta
        model.detailMinistro(request: ministroRequest)
    }

    func detailEmbajador(cuenta: String, mes: String, anio: String) {
        view.showLoading(message: "")
        let embajadorRequest = DetailEmbajadorRequest()
        embajadorRequest.anio = anio
        embajadorRequest.mes = mes
        embajadorRequest.cuenta = cuenta
        model.detailEmbajador(request: embajadorRequest)
    }

    func successGetEmbajador() {
        view.hideLoading()
        observer?.successGetEmbajador()
    }

    func successGetMinistro() {
        view.hideLoading()
        observer?.successGetMinistro()
    }

    func successVentasMinistro() {
        view.hideLoading()
        observer?.successVentasMinistro()
    }

    func successMinistroComisionEmbajador() {
        view.hideLoading()
        observer?.successMinistroComisionEmbajador()
    }

    func successEmbajadorComisionMinistro() {
        view.hideLoading()
        observer?.successEmbajadorComisionMinistro()
    }

    func successVentasEmbajador() {
        view.hideLoading()
        observer?.successVentasEmbajador()
    }

    func onError(errorMessage: String) {
        view.hideLoading()
        observer?.onError(errorMessage: errorMessage)
    }

    func getCurrentNumEmployee() -> String {
        let employee = model.getEmploye()
        return employee.NoEmpleado
      //  return "41"
    }

    func consultDetailEmbajador() -> Embajador {
        let data = model.getDetailEmbajador()
        return data
    }

    func consultDetailMinistro() -> respuestaMinistroGnrlVo {
        let data = model.getDetailMinistro()
        return data
    }

    func consultVentasEmbajador() -> respuestaEmbajadorComisiones {
        let data = model.getVentasEmbajador()
        return data
    }

    func consultVentasMinistro() -> [ministroComisiones] {
        let data = model.getVentasMinistro()
        return data
    }

    func consultEmbajadorComisionMinistro() -> respuestaEmbajadoresComisionesMinistro {
        let data = model.getEmbajadorComisionMinistro()
        return data
    }

    func consultArrayEmbajadorComisionMinistro() -> [embajadorComisionesMinistro] {
        let data = model.getEmbajadorComisionMinistroArray()
        return data
    }

    func consultMinistroComisionEmbajdor() -> [embajadoresComisiones] {
        let data = model.getMinistroComisionEmbajdor()
        return data
    }

    func getMonthYearCalendar(selectDate: Date) -> (String, String, Int) {
        let month =  DateCalendar.convertDateString(date: selectDate, formatter: Formate.MMM)
        let year = DateCalendar.convertDateString(date: selectDate, formatter: Formate.yyyy)
        let monthInt = DateCalendar.convertDateInt(date: selectDate, component: .month)
        return (month, year, monthInt)
    }

    func getMonthYearDay() -> (String, String, Int) {
        if let datePass = DateCalendar.pastDate(date: Date(), value: 30, component: .day) {
            let month =  DateCalendar.convertDateString(date: datePass, formatter: Formate.MMM)
            let year = DateCalendar.convertDateString(date: datePass, formatter: Formate.yyyy)
            let monthInt = DateCalendar.convertDateInt(date: datePass, component: .month)
            return (month, year, monthInt)
        }
        return ("6", "2021", 1)
    }

    func convertFechaPago(day: String) -> String {
        let date =  DateCalendar.convertStringDate(date: day, formatter: Formate.dd_MM_yy)
        let fechaPago =  DateCalendar.convertDateString(date: date ?? Date(), formatter: Formate.d_MMMM)
        return fechaPago
    }

}
