//
//  SellerCollectionReusableView.swift
//  SalesCloud
//
//  Created by Hector Alfonso Moreno Santa Cruz on 15/05/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//
import Foundation
import UIKit

class SellerCollectionReusableView: UITableViewCell {

    @IBOutlet weak var promoterName: UILabel!
    @IBOutlet weak var statusValidation: UILabel!
    @IBOutlet weak var noEmpleado: UILabel!
    @IBOutlet weak var noEmpleatoTitle: UILabel!

    @IBOutlet weak var viewBackGround: UIView!

    var listaUsuarios: ResponseConsultaModel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func printValues() {
        promoterName.text = listaUsuarios.name
        noEmpleado.text = listaUsuarios.noEmployed
        statusValidation.text = listaUsuarios.status
        setColorStatus()
    }
    
    func setColorStatus() {
        var statusEmployed = listaUsuarios.status
        if let status = TPStatusEmployedCases(rawValue: statusEmployed) {
            switch status {
                
            case .ACTIVO:
                viewBackGround.backgroundColor = UIColor(named: TPColorsForStatus.ACTIVO)
            case .INACTIVO:
                viewBackGround.backgroundColor = UIColor(named: TPColorsForStatus.INACTIVO)
            case .RECHAZADO:
                viewBackGround.backgroundColor = UIColor(named: TPColorsForStatus.RECHAZADO)
            case .POR_VALIDAR:
                viewBackGround.backgroundColor = UIColor(named: TPColorsForStatus.POR_VALIDAR)
            case .POR_AUTORIZAR:
                viewBackGround.backgroundColor = UIColor(named: TPColorsForStatus.POR_AUTORIZAR)
            }
        }
    }

}
