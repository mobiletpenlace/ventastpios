//
//  ResponsePersonalData.swift
//  SalesCloud
//
//  Created by Hector Alfonso Moreno Santa Cruz on 31/05/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import AlamofireObjectMapper
import RealmSwift

class ResponsePersonalData: NSObject, Mappable {
    required init?(map: Map) {
        super.init()
    }

    func mapping(map: Map) {
        message <- map["message"]
        data <- map["data"]
    }

    @objc dynamic var message: String?
    @objc dynamic var data: DataModelDocuments?
}

class DataModelDocuments: NSObject, Mappable {
    required init?(map: Map) {
        super.init()
    }

    func mapping(map: Map) {
        documents <- map["documents"]
        steps <- map["steps"]

    }

    @objc dynamic var documents: [Document]?
    @objc dynamic var steps: [DataStep]?

}

class Document: NSObject, Mappable {

    required init?(map: Map) {
        super.init()
    }

    func mapping(map: Map) {
        fields <- map["fields"]
        steps <- map["steps"]
        photos <- map["photos"]

    }

    @objc dynamic var steps: [DocumentStep] = []
    @objc dynamic var fields: [String: Field]?
    @objc dynamic var photos: [String] = []
}

class Field: NSObject, Mappable {
    required init?(map: Map) {
        super.init()
    }

    func mapping(map: Map) {
        value <- map["value"]
    }
    @objc dynamic var value: String?

}

class DocumentStep: NSObject, Mappable {
    required init?(map: Map) {
        super.init()
    }

    func mapping(map: Map) {
        data <- map["data"]
    }
    @objc dynamic var data: PurpleData?

}

class PurpleData: NSObject, Mappable {

    required init?(map: Map) {
        super.init()
    }

    func mapping(map: Map) {
        fullName <- map["fullName"]
        dateOfBirth <- map["dateOfBirth"]

    }

    @objc dynamic var fullName: Address?
    @objc dynamic var dateOfBirth: Address?

}

class Address: NSObject, Mappable {

    required init?(map: Map) {
        super.init()
    }

    func mapping(map: Map) {
        value <- map["value"]
        label <- map["label"]
        format <- map["format"]

    }

    @objc dynamic var value: String?
    @objc dynamic var label: String?
    @objc dynamic var format: String?
}

class DataStep: NSObject, Mappable {

    required init?(map: Map) {
        super.init()
    }

    func mapping(map: Map) {
        status <- map["status"]
        data <- map["data"]

    }

    @objc dynamic var status: Int = 0
    @objc dynamic var data: FluffyData?
}

class FluffyData: NSObject, Mappable {

    required init?(map: Map) {
        super.init()
    }

    func mapping(map: Map) {
        facematchScore <- map["facematchScore"]
        sources <- map["sources"]
        selfiePhotoUrl <- map["selfiePhotoUrl"]

    }

    @objc dynamic var facematchScore: String?
    @objc dynamic var sources: [Source]?
    @objc dynamic var selfiePhotoUrl: String?
}

class Source: NSObject, Mappable {

    required init?(map: Map) {
        super.init()
    }

    func mapping(map: Map) {
        type <- map["type"]
        url <- map["url"]

    }

    @objc dynamic var type: String?
    @objc dynamic var url: String?
}
