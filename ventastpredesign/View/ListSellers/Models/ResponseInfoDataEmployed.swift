//
//  ResponseInfoDataEmployed.swift
//  SalesCloud
//
//  Created by Axlestevez-trabajo on 23/01/24.
//  Copyright © 2024 TotalPlay. All rights reserved.
//

import Foundation
import ObjectMapper

public class ResponseInfoEmpEmployed: NSObject, Mappable {
    public required convenience init?(map: Map) {
        self.init()
    }

    public func mapping(map: Map) {
        numeroEmpleado <- map["numeroEmpleado"]
        nombre <- map["nombre"]
        rfc <- map["rfc"]
        recontratable <- map["recontratable"]
        posicion <- map["posicion"]
        motivoRechazo <- map["motivoderechazo"]
        motivoDeNoRecontratable <- map["motivodenorecontratable"]
        motivoiDeBaja <- map["motivodebaja"]
        ineReversa <- map["ineReversa"]
        ineFrontal <- map["ineFrontal"]
        idJefeVentas <- map["idJefeVentas"]
        idJefePlaza <- map["idJefePlaza"]
        idJefeDistritalPlaza <- map["idDistritalPlaza"]
        idJefeDistrital <- map["idDistrital"]
        fotografia <- map["fotografia"]
        fechaNacimiento <- map["fechaNacimiento"]
        estatus <- map["estatus"]
        estadoDeCuenta <- map["estadodecuenta"]
        entidadBancaria <- map["entidadBancaria"]
        correo <- map["correo"]
        celular <- map["celular"]
        claveInterbancaria <- map["claveInterbancaria"]

    }

    @objc dynamic var numeroEmpleado: String                = ""
    @objc dynamic var nombre: String                        = ""
    @objc dynamic var rfc: String                           = ""
    @objc dynamic var recontratable: Bool                   = false
    @objc dynamic var posicion: String                      = ""
    @objc dynamic var motivoRechazo: String                 = ""
    @objc dynamic var motivoDeNoRecontratable: String       = ""
    @objc dynamic var motivoiDeBaja: String                 = ""
    @objc dynamic var ineReversa: String                    = ""
    @objc dynamic var ineFrontal: String                    = ""
    @objc dynamic var idJefeVentas: String                  = ""
    @objc dynamic var idJefePlaza: String                   = ""
    @objc dynamic var idJefeDistritalPlaza: String          = ""
    @objc dynamic var idJefeDistrital: String               = ""
    @objc dynamic var fotografia: String                    = ""
    @objc dynamic var fechaNacimiento: String               = ""
    @objc dynamic var estatus: String                       = ""
    @objc dynamic var estadoDeCuenta: String                = ""
    @objc dynamic var entidadBancaria: String               = ""
    @objc dynamic var correo: String                        = ""
    @objc dynamic var celular: String                       = ""
    @objc dynamic var claveInterbancaria: String            = ""

}

public class ResponseInfoDataEmployed: NSObject, Mappable {
    public required convenience init?(map: Map) {
        self.init()
    }

    public func mapping(map: Map) {
        infoEmp <- map["infoEmp"]
    }

    @objc dynamic var infoEmp: [ResponseInfoEmpEmployed] = []
}
