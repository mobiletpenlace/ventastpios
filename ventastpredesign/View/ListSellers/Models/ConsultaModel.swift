//
//  ConsultaModel.swift
//  SalesCloud
//
//  Created by Hector Alfonso Moreno Santa Cruz on 16/05/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper
import RealmSwift

class ConsultaModel: Object, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        accion <- map["accion"]
        infoEmpleado <- map["infoEmpleado"]
    }

    @objc dynamic var accion: String = ""
    @objc dynamic var infoEmpleado: OtherTeam?
}

class OtherTeam: Object, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        aceptaTerminosCondiciones <- map["AceptaTerminosyCondiciones"]
        id <- map["Id"]
        aceptaPagos <- map["AceptaPagos"]
    }

    @objc dynamic var aceptaTerminosCondiciones: Bool = false
    @objc dynamic var id: String = ""
    @objc dynamic var aceptaPagos: Bool = false
}
