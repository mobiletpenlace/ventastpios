//
//  ResponseConsultaModel.swift
//  SalesCloud
//
//  Created by Hector Alfonso Moreno Santa Cruz on 18/05/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import ObjectMapper

class ResponseConsultaModel: NSObject, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        name <- map["nombre"]
        noEmployed <- map["numeroEmpleado"]
        status <- map["estatus"]
        idEmployed <- map["Id"]
    }
    @objc dynamic var name: String = ""
    @objc dynamic var noEmployed: String = ""
    @objc dynamic var status: String = ""
    @objc dynamic var idEmployed: String = ""

}

class ResponseAllConsultaUsuario: NSObject, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        registerID <- map["idRegistro"]
        infoUsuarios <- map["info"]
        result <- map["result"]

    }

    @objc dynamic var registerID: String = ""
    @objc dynamic var infoUsuarios: [ResponseConsultaModel]?
    @objc dynamic var result: String = ""

}
