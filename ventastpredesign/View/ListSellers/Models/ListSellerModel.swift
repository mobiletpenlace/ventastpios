//
//  ListSellerModel.swift
//  SalesCloud
//
//  Created by Hector Alfonso Moreno Santa Cruz on 16/05/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import RealmSwift

protocol ListSellerModelObserver {
    func successList(responseModel: [ResponseConsultaModel])
}

class ListSellerModel: BaseModel {

//    var vModel: ListSellerModelObserver?
//    var server: ServerDataManager2<> // TODO: aquí ira la respuesta del servicio
//
//
//
//
//    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse)(
//

    init(observer: BaseViewModelObserver) {
        // server = ServerDataManager2()
        super.init(observerVM: observer)
    }

}
