//
//  TPListSellerConstants.swift
//  SalesCloud
//
//  Created by Axlestevez on 19/01/24.
//  Copyright © 2024 TotalPlay. All rights reserved.
//


public enum TPStatusEmployedCases: String {
    case ACTIVO         = "Activo"
    case INACTIVO       = "Inactivo"
    case RECHAZADO      = "Rechazado"
    case POR_VALIDAR    = "Por Validar"
    case POR_AUTORIZAR  = "Por Autorizar"
}

public enum TPStatusEmployed {
    static let ACTIVO         = "Activo"
    static let INACTIVO       = "Inactivo"
    static let RECHAZADO      = "Rechazado"
    static let POR_VALIDAR    = "Por Validar"
    static let POR_AUTORIZAR  = "Por Autorizar"
}

public enum TPColorsForStatus {
    static let ACTIVO           = "StatusEmployedActive"
    static let INACTIVO         = "base_rede_init_20_micronegocios"
    static let RECHAZADO        = "StatusRejectedEmployed"
    static let POR_VALIDAR      = "statusEmployedToValidate"
    static let POR_AUTORIZAR    = "Base_rede_end_diviertete"
}

public enum TPActionsServices {
    static let ACTIVOS          = "Consulta activos"
    static let NO_ACTIVOS       = "Consulta no activos"
    static let EMPLOYED_INFO    = "Consulta Emp"
    static let UPDATE_ACTION    = "Actualiza"
}

public enum TPColorsButtonActiveNoActive {
    static let ACTIVE               = "white-background-color"
    static let NO_ACTIVE            = "Base_blue_text"
}

public enum TPCasesResponse: String {
    case success             = "0"
    case failed              = "1"
    case noneResult          = ""
}

public enum TPReasonUnsubscribeEmployed {
    static let ORIGIN_UNSUBSCRIBE       = "BajaEmployed"
    static let VOLUNTARY_DISCHARGE      = "Baja voluntaria"
    static let BAD_REPUTATION           = "Malas practivas"
    static let OPTIONS: [String] = ["Seleccione un motivo",VOLUNTARY_DISCHARGE, BAD_REPUTATION]
}

public enum TPReasonUnsubscribeEmployedCases: String {
    case BAD_PARACTICS      = "Malas practivas"
    case DOW_VOLUNTARY      = "Baja voluntaria"
    case NO_SELECTED        = "Seleccione un motivo"
}
