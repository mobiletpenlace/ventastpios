//
//  ListSellerController.swift
//  SalesCloud
//
//  Created by Hector Alfonso Moreno Santa Cruz on 15/05/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import SwiftEventBus

class ListSellerController: UIViewController {

    @IBOutlet weak var returnImage: UIImageView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var uiTable: UITableView!
    @IBOutlet weak var uiViewColaboradores: UIView!
    @IBOutlet weak var noActiveButton: UIButton!
    @IBOutlet weak var activeButton: UIButton!

    var filteredText: [ResponseConsultaModel] = []
    var originalList: [ResponseConsultaModel] = []
    let viewModel  = ListSellerViewModel()
    var rejectedEmployedView: TPRejectedEmployedView!
    var employedIDSelected: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        initEventBus()
    }

    func mostrarOcultarTabla(isActive: Bool) {
        Constants.removeSpinnerView()
        self.uiViewColaboradores.isHidden = isActive
        self.uiTable.isHidden = !isActive
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Constants.showSpinnerView(title: "Consultando informacion", view: self.view)
        viewModel.getListEmployedApprovals()
        searchBar.delegate = self
        configurationTableColaboradores()
    }

    @IBAction func backToPrincipalPage(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }

    @IBAction func getEmployedInactive(_ sender: Any) {
        Constants.showSpinnerView(title: "Consultando Empleados", view: self.view)
        viewModel.getListEmployedApprovals()
        noActiveButton.backgroundColor = UIColor(named: TPColorsButtonActiveNoActive.NO_ACTIVE)
        noActiveButton.setTitleColor(UIColor(named: TPColorsButtonActiveNoActive.ACTIVE), for: .normal)
        inavtiveButtonColor(button: activeButton)
    }

    @IBAction func getEmployedActive(_ sender: Any) {
        Constants.showSpinnerView(title: "Consultando Empleados", view: self.view)
        viewModel.getListEmployedActive()
        activeButton.backgroundColor = UIColor(named: TPColorsButtonActiveNoActive.NO_ACTIVE)
        activeButton.setTitleColor(UIColor(named: TPColorsButtonActiveNoActive.ACTIVE), for: .normal)
        inavtiveButtonColor(button: noActiveButton)
    }

    func inavtiveButtonColor(button: UIButton) {
        button.backgroundColor = UIColor(named: TPColorsButtonActiveNoActive.ACTIVE)
        button.setTitleColor(UIColor(named: TPColorsButtonActiveNoActive.NO_ACTIVE), for: .normal)
    }

    func configurationTableColaboradores() {
        uiTable.register(UINib(nibName: "SellerCollectionReusableView", bundle: nil), forCellReuseIdentifier: "SellerCollectionReusableView")
        uiTable.delegate = self
        uiTable.dataSource = self
    }

    func showRejectedEmployedView(idEmployed: String) {
        rejectedEmployedView = TPRejectedEmployedView(frame: CGRect(
            x: 0,
            y: 0,
            width: self.view.bounds.width,
            height: self.view.bounds.height
        ))
        rejectedEmployedView.idEmployedToSend = idEmployed
        rejectedEmployedView.observer = self
        self.view.addSubview(rejectedEmployedView)
    }

    func initEventBus() {
        SwiftEventBus.onMainThread(self, name: "reload_table") { [weak self] result in
            guard let newList = result?.object as? [ResponseConsultaModel] else {return}
            if newList.count != 0 {
                self?.originalList = newList
                self?.filteredText = newList
                self?.uiTable.reloadData()
                self?.mostrarOcultarTabla(isActive: true)
            } else {
                self?.mostrarOcultarTabla(isActive: false)
            }
        }

        SwiftEventBus.onMainThread(self, name: "filter_table") { [weak self] result in
            guard let newList = result?.object as? [ResponseConsultaModel] else {return}
            if newList.count != 0 {
                self?.filteredText = newList
                self?.uiTable.reloadData()
                self?.mostrarOcultarTabla(isActive: true)
            } else {
                self?.mostrarOcultarTabla(isActive: false)
            }

        }

        SwiftEventBus.onMainThread(self, name: "showRejectedEmployedView") { result in
            guard let idEmployed = result?.object as? String else { return }
            self.showRejectedEmployedView(idEmployed: idEmployed)
        }
    }
}

extension ListSellerController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let text = searchBar.text?.lowercased() else {return}
        viewModel.filterListSellers(searchText: text, listFilter: self.originalList)
    }
}

extension ListSellerController: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return self.filteredText.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SellerCollectionReusableView", for: indexPath) as! SellerCollectionReusableView
        cell.listaUsuarios = self.filteredText[indexPath.section]
        cell.printValues()
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let userSelected: ResponseConsultaModel =  self.filteredText[indexPath.section]
        self.employedIDSelected = userSelected.idEmployed
        viewModel.openConfirmDialog(employed: userSelected, view: self, self)

    }
}

extension ListSellerController: TPRejectedEmployedViewObserver {

    func receiveRejectedReason(rejectedReason: String, idEmployedToSend: String) {
        viewModel.rejectedEmployed(employedID: idEmployedToSend, rejectedReason: rejectedReason) { responseJson, urlResponse in
            if !checkForError(httpUrlResponse: urlResponse as! HTTPURLResponse) {
                DispatchQueue.main.async {
                    let responseStatus = responseJson["result"] as? String ?? String.emptyString
                    let resultDescription = responseJson["resultDescription"] as? String ?? String.emptyString
                    self.reacToResponse(responseCase: responseStatus, resultDescription)
                }
            } else {
                Constants.Alert(title: "Error", body: "Ha ocurrido un error", type: type.Error, viewC: self)
            }
        }
    }

    func reacToResponse(responseCase: String, _ resultDescription: String = String.empty) {
        let resultResponse = TPCasesResponse(rawValue: responseCase)
        switch resultResponse {
        case .success:
            Constants.showSpinnerView(title: "Consultando Empleados", view: self.view)
            self.viewModel.getListEmployedApprovals()
        case .failed:
            Constants.Alert(title: "Error", body: resultDescription, type: type.Error, viewC: self)
        case .noneResult:
            Constants.Alert(title: "Error", body: "Error interno", type: type.Error, viewC: self)
        case .none:
            Constants.Alert(title: "Error", body: "Error interno", type: type.Error, viewC: self)
        }
    }

}

extension ListSellerController: TPReceiveUnsubscribeEmployedData {
    func receiveData(isReaffiliationAviable: Bool, motivoRechazo: String) {
        viewModel.unsubscribeEmployed(employedID: self.employedIDSelected, motivo: motivoRechazo, isReactive: isReaffiliationAviable) { responseJson, urlResponse in
            if !checkForError(httpUrlResponse: urlResponse as! HTTPURLResponse) {
                DispatchQueue.main.async {
                    let response = responseJson["result"] as? String ?? String.empty
                    let resultDescription = responseJson["resultDescription"] as? String ?? String.empty
                    self.reacToResponse(responseCase: response, resultDescription)
                }
            } else {
                Constants.Alert(title: "Error", body: "Ha ocurrido un error", type: type.Error, viewC: self)
            }
        }
    }
}
