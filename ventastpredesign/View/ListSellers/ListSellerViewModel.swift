//
//  ListSellerViewModel.swift
//  SalesCloud
//
//  Created by Hector Alfonso Moreno Santa Cruz on 15/05/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import SwiftEventBus

public typealias ResponseInfoEmployed = (_ responseJson: ResponseInfoDataEmployed) -> ()
public typealias ResponseUpdateStatusEmployed = (_ responseJson: [String: Any], _ urlResponse: URLResponse? ) -> ()

class ListSellerViewModel {

    var employe: Empleado?

    func filterListSellers(searchText: String, listFilter: [ResponseConsultaModel] ) {
        var newFiltered: [ResponseConsultaModel]?
        if !searchText.isEmpty {
            newFiltered = listFilter.filter {
                $0.name.lowercased().contains(string: searchText) || $0.noEmployed.lowercased().contains(string: searchText)
            }
        } else {
            newFiltered = listFilter
        }
        SwiftEventBus.post("filter_table", sender: newFiltered)
    }

    func getListEmployedApprovals() {
        if RealManager.findFirst(object: Empleado.self) != nil {
        employe = Empleado(value: RealManager.findFirst(object: Empleado.self)!)
            httpEmployedApprovals(employedId: employe!)
        } else {
            print("Error al traer al usuario")
        }
    }

    func getListEmployedActive() {
        if RealManager.findFirst(object: Empleado.self) != nil {
            employe = Empleado(value: RealManager.findFirst(object: Empleado.self)!)
            httpEmployedApprovals(employedId: employe!, TPActionsServices.ACTIVOS)
        } else {
            print("Error al traer activos")
        }
    }

    func httpEmployedApprovals(employedId: Empleado, _ actionParameter: String = TPActionsServices.NO_ACTIVOS) {
        if ConnectionUtils.isConnectedToNetwork() {
            let parameters: Parameters = ["accion": actionParameter, "infoEmpleado": ["id": employedId.Id,"numeroEmpleado": employedId.NoEmpleado]]
            httpEmployed(parameters: parameters) { it in
                print(parameters)
                print(it)
                if it.infoUsuarios != nil {
                    if actionParameter == TPActionsServices.NO_ACTIVOS {
                        let usuarios = self.getEmployedsToBeApproved(rawUser: it.infoUsuarios ?? [], status: TPStatusEmployed.INACTIVO)
                        SwiftEventBus.post("reload_table", sender: usuarios)
                    } else if actionParameter == TPActionsServices.ACTIVOS {
                        let usuarios = self.getEmployedsToBeApproved(rawUser: it.infoUsuarios ?? [], status: TPStatusEmployed.ACTIVO)
                        SwiftEventBus.post("reload_table", sender: usuarios)
                    }
                } else {
                    SwiftEventBus.post("reload_table", sender: [])

                }
            }
        }
    }

    func httpEmployedData(employedId: String) {
        if ConnectionUtils.isConnectedToNetwork() {
            let parameters: Parameters = ["accion": TPActionsServices.EMPLOYED_INFO, "infoEmpleado": ["id": employedId]]
            httpEmployed(parameters: parameters) { response in
                guard let infoUsuario = response.infoUsuarios else { return }
            }
        }
    }

    func getEmployedsToBeApproved(rawUser: [ResponseConsultaModel], status: String) -> [ResponseConsultaModel] {
        var employedsToBeApprover: [ResponseConsultaModel] = []

        for user in rawUser {
            let statusUser = TPStatusEmployedCases(rawValue: user.status)
            if (status == TPStatusEmployed.INACTIVO && user.noEmployed.isEmpty == false) {
                switch statusUser {
                case .ACTIVO:
                    print("")
                case .INACTIVO:
                    print("")
                case .RECHAZADO:
                    print("")
                case .POR_VALIDAR:
                    employedsToBeApprover.append(user)
                case .POR_AUTORIZAR:
                    employedsToBeApprover.append(user)
                case .none:
                    print("")
                }
            }

            if (status == TPStatusEmployed.ACTIVO && user.noEmployed.isEmpty == false) {
                switch statusUser {
                case .ACTIVO:
                    employedsToBeApprover.append(user)
                case .INACTIVO:
                    print("")
                case .RECHAZADO:
                    print("")
                case .POR_VALIDAR:
                    print("")
                case .POR_AUTORIZAR:
                    print("")
                case .none:
                    print("")
                }
            }

        }
        return employedsToBeApprover
    }

    func httpEmployed(parameters: Parameters, completion: @escaping((ResponseAllConsultaUsuario) -> Void)) {
        let url = AppDelegate.API_SALESFORCE + ApiDefinition.API_CONSULTA_CREACION_EMPLEADO

        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: ApiDefinition.headersToken).responseJSON {[weak self] response in
            switch response.result {
            case .success( let value):
                let json = JSON(value)
                let responseConsulta = ResponseAllConsultaUsuario(JSONString: json.rawString()!)
                completion(responseConsulta!)
            case .failure(let error):
                print(error)
            }
        }

    }

    func httpEmployedInfo(parameters: Parameters, completionHandler: @escaping ResponseInfoEmployed) {
        let url = AppDelegate.API_SALESFORCE + ApiDefinition.API_CONSULTA_CREACION_EMPLEADO
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: ApiDefinition.headersToken).responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                let responseConsulta = ResponseInfoDataEmployed(JSONString: json.rawString()!)
                completionHandler(responseConsulta!)
            case .failure(let error):
                print(error)
            }
        }

    }

    func httpEmployedUpdateStatus(parameters: Parameters, completionHandler: @escaping ResponseUpdateStatusEmployed) {
        let url = AppDelegate.API_SALESFORCE + ApiDefinition.API_CONSULTA_CREACION_EMPLEADO
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: ApiDefinition.headersToken).responseJSON { response in
            let jsonResponse = response.value as! [String: Any]
            print(jsonResponse)
            debugPrint(response)
            completionHandler(jsonResponse, response.response)
        }
    }

    func openConfirmDialog(employed: ResponseConsultaModel, view: UIViewController, _ obsever: TPReceiveUnsubscribeEmployedData?) {

        guard let employedStatus = TPStatusEmployedCases(rawValue: employed.status) else { return }
        switch employedStatus {
        case .ACTIVO:
            unsubscribeEmployedView(view: view,employedID: employed.idEmployed, observerView: obsever!)
        case .RECHAZADO:
            print("")
        case .POR_AUTORIZAR:
            rejectedOrAproveEmployedView(employed: employed, view: view)
        case .POR_VALIDAR:
            rejectedOrAproveEmployedView(employed: employed, view: view)
        case .INACTIVO:
            print("")
        }
    }

    func rejectedEmployed(employedID: String, rejectedReason: String, completionHandler: @escaping ResponseUpdateStatusEmployed) {
        let parameters: Parameters = [ "accion": TPActionsServices.UPDATE_ACTION,
                                       "infoEmpleado": ["id": employedID,
                                                        "motivoderechazo": rejectedReason,
                                                        "estatus": TPStatusEmployed.RECHAZADO
                                                       ]
        ]
        httpEmployedUpdateStatus(parameters: parameters) { responseJson, urlResponse in
            completionHandler(responseJson, urlResponse)
        }
    }

    func unsubscribeEmployedView(view: UIViewController,employedID: String, observerView: TPReceiveUnsubscribeEmployedData) {
        let nextView = TPUnsubscribeEmployedView(frame: CGRect(
            x: 0,
            y: 0,
            width: view.view.bounds.width,
            height: view.view.bounds.height
        ))
        nextView.observer = observerView
        view.view.addSubview(nextView)
    }

    func rejectedOrAproveEmployedView(employed: ResponseConsultaModel, view: UIViewController) {
        let storyBoard = UIStoryboard(name: "ActiveCoworker", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ActiveCoworkerController") as! ActiveCoworkerController
        viewController.idEmployedToSend = employed.idEmployed
        viewController.modalPresentationStyle = .overFullScreen
        viewController.isRejectedEmployedView = true
        view.present(viewController, animated: false)
    }

    func unsubscribeEmployed(employedID: String, motivo: String, isReactive: Bool, completionHandler: @escaping ResponseUpdateStatusEmployed) {

        let parameters: Parameters = ["accion": TPActionsServices.UPDATE_ACTION,
                                      "infoEmpleado": ["id": employedID,
                                                       "motivodebaja": motivo,
                                                       "motivodenorecontratable": "",
                                                       "estatus": TPStatusEmployed.INACTIVO,
                                                       "recontratable": isReactive
                                                      ]
        ]

        httpEmployedUpdateStatus(parameters: parameters) { responseJson, urlResponse in
            completionHandler(responseJson, urlResponse)
        }
    }

}
