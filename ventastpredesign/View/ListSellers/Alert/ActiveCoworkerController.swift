//
//  ActiveCoworkerController.swift
//  SalesCloud
//
//  Created by Hector Alfonso Moreno Santa Cruz on 18/05/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import ObjectMapper
import SwiftyJSON
import SwiftEventBus
import TextFieldEffects

class ActiveCoworkerController: UIViewController {
    var idEmployedToSend: String?
    let vM = ListSellerViewModel()
    var rejectedEmployedView: TPRejectedEmployedView!
    var titleScreen: String = ""
    var isEmployedView: Bool = false
    var isRejectedEmployedView: Bool = false
    var isEditDataOn: Bool = false

    @IBOutlet weak var scrollTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var titleScreenLabel: UILabel!
    @IBOutlet weak var employedStatusView: UIView!
    @IBOutlet weak var motivoRechazoView: UIView!
    @IBOutlet weak var closeButtonwRejectedFlow: UIButton!
    @IBOutlet weak var buttonsRejectedView: UIView!

    @IBOutlet weak var avatarEmployedImage: UIImageView!
    @IBOutlet weak var nameEployedLabel: UILabel!
    @IBOutlet weak var statusEmployedLabel: UILabel!
    @IBOutlet weak var posicionEmployedLabel: UILabel!
    @IBOutlet weak var motivoRechazoLabel: UILabel!
    @IBOutlet weak var editDataInfoView: UIView!

    @IBOutlet weak var noEmployedTextField: HoshiTextField!
    @IBOutlet weak var rfcEmployedTextField: HoshiTextField!
    @IBOutlet weak var emailEmployedTextField: HoshiTextField!
    @IBOutlet weak var phoneEmployedTextField: HoshiTextField!

    @IBOutlet weak var idCardFrontEmployedImage: UIImageView!
    @IBOutlet weak var idCardBackEmployedImage: UIImageView!
    @IBOutlet weak var infoChangeLabel: UILabel!

    @IBOutlet weak var entidadBancariaLabel: UILabel!
    @IBOutlet weak var estadoDeCuentaImage: UIImageView!

    @IBOutlet weak var changeIdCardFrontButton: UIButton!
    @IBOutlet weak var changeIdCardBackButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        Constants.showSpinnerView(title: "Consultando informacion", view: self.view)
        setupView()
    }

    @IBAction func continueActivationEmployed(_ sender: UIButton) {
        let parameters: Parameters = ["accion": TPActionsServices.UPDATE_ACTION, "infoEmpleado": ["id": idEmployedToSend, "estatus": TPStatusEmployed.ACTIVO]]
        vM.httpEmployed(parameters: parameters) { it in
            if it != nil {
                self.vM.getListEmployedApprovals()
                ProgressHUD.showSucceed(it.result)
                self.dismiss(animated: false)
            }
        }
    }

    @IBAction func actionCloseAlert(_ sender: UIButton) {
        self.dismiss(animated: false)
    }

    @IBAction func rejectedEmployedAction(_ sender: Any) {
        self.dismiss(animated: true)
        SwiftEventBus.post("showRejectedEmployedView", sender: idEmployedToSend)
    }

    @IBAction func editaDataEmployedAction(_ sender: Any) {
        if !isEditDataOn {
            editInfoData()
        } else {
            // TODO: add function for update employed info
        }
    }

    @IBAction func changeImageIdCardAction(_ sender: Any) {
        showMediaSource()

    }

    @IBAction func changeIdCardBackAction(_ sender: Any) {
        showMediaSource()
    }

    @IBAction func changeBankDocumentAction(_ sender: Any) {
        showMediaSource()
    }

    func setImages(sourceImages: ResponseInfoEmpEmployed) {
        let ineFrontDataImage = sourceImages.ineFrontal.isEmpty ? "" : sourceImages.ineFrontal
        let ineBackDataImage = sourceImages.ineFrontal.isEmpty ? "" : sourceImages.ineReversa
        let estadoBancarioDataImage = sourceImages.ineFrontal.isEmpty ? "" : sourceImages.estadoDeCuenta
        let avatarEmployed = sourceImages.fotografia.isEmpty ? "" : sourceImages.fotografia

        if !ineFrontDataImage.isEmpty {
            if let dataIneFront = Data(base64Encoded: ineFrontDataImage){
                self.idCardFrontEmployedImage.image = UIImage(data: dataIneFront)
            }
        } else {
            self.idCardFrontEmployedImage.image = UIImage(named: "Image_rede_scanIDWait")
        }

        if !ineBackDataImage.isEmpty {
            if let dataIneBack = Data(base64Encoded: ineBackDataImage) {
                self.idCardBackEmployedImage.image = UIImage(data: dataIneBack)
            }
        } else {
            self.idCardBackEmployedImage.image = UIImage(named: "Image_rede_scanIDWait")
        }

        if !estadoBancarioDataImage.isEmpty{
            if let dataImage = Data(base64Encoded: estadoBancarioDataImage) {
                self.estadoDeCuentaImage.image = UIImage(data: dataImage)
            }
        } else {
            self.estadoDeCuentaImage.image = UIImage(named: "documento")
        }

        if !avatarEmployed.isEmpty {
            if let dataImage = Data(base64Encoded: avatarEmployed){
                self.avatarEmployedImage.image = UIImage(data: dataImage)
            } else {
                self.avatarEmployedImage.image = UIImage(named: "Icon_avatar_man")
            }
        }
    }

}

extension ActiveCoworkerController {
    func getDataInfoEmployed() {
        let parameters: Parameters = ["accion": TPActionsServices.EMPLOYED_INFO, "infoEmpleado": ["id": idEmployedToSend]]
        vM.httpEmployedInfo(parameters: parameters) { responseJson in
            if responseJson != nil {
                let infoDataEmployed = responseJson.infoEmp[0]
                DispatchQueue.main.async {
                    if self.isRejectedEmployedView {
                        self.noEmployedTextField.text = infoDataEmployed.numeroEmpleado.isEmpty ? "Sin información": infoDataEmployed.numeroEmpleado
                        self.rfcEmployedTextField.text = infoDataEmployed.rfc.isEmpty ? "Sin información": infoDataEmployed.rfc
                        self.emailEmployedTextField.text = infoDataEmployed.correo.isEmpty ? "Sin información": infoDataEmployed.correo
                        self.phoneEmployedTextField.text = infoDataEmployed.celular.isEmpty ? "Sin información": infoDataEmployed.celular
                        self.entidadBancariaLabel.text = infoDataEmployed.entidadBancaria.isEmpty ? "Sin información": infoDataEmployed.entidadBancaria
                        self.setImages(sourceImages: infoDataEmployed)
                        Constants.removeSpinnerView()
                    }
                    if self.isEmployedView {
                        self.nameEployedLabel.text = infoDataEmployed.nombre.isEmpty ? "Sin información": infoDataEmployed.nombre
                        self.posicionEmployedLabel.text = infoDataEmployed.posicion.isEmpty ? "null": infoDataEmployed.posicion
                        self.statusEmployedLabel.text = "Estatus: \(infoDataEmployed.estatus.isEmpty ? "null": infoDataEmployed.estatus)"
                        self.motivoRechazoLabel.text = infoDataEmployed.motivoRechazo.isEmpty ? "null": infoDataEmployed.motivoRechazo
                        self.noEmployedTextField.text = infoDataEmployed.numeroEmpleado.isEmpty ? "Sin información": infoDataEmployed.numeroEmpleado
                        self.rfcEmployedTextField.text = infoDataEmployed.rfc.isEmpty ? "Sin información": infoDataEmployed.rfc
                        self.emailEmployedTextField.text = infoDataEmployed.correo.isEmpty ? "Sin información": infoDataEmployed.correo
                        self.phoneEmployedTextField.text = infoDataEmployed.celular.isEmpty ? "Sin información": infoDataEmployed.celular
                        self.entidadBancariaLabel.text = infoDataEmployed.entidadBancaria.isEmpty ? "Sin información": infoDataEmployed.entidadBancaria
                        self.setImages(sourceImages: infoDataEmployed)
                        Constants.removeSpinnerView()
                    }
                }
            } else {
                // TODO: alert de usuario no encontrado o error generico.
            }
        }
    }
}

// MARK: customize view
extension ActiveCoworkerController {
    func setupView() {

        nameEployedLabel.text = "..."
        posicionEmployedLabel.text = "..."
        motivoRechazoLabel.text = "..."
        noEmployedTextField.isEnabled = false
        rfcEmployedTextField.isEnabled = false
        emailEmployedTextField.isEnabled = false
        phoneEmployedTextField.isEnabled = false
        infoChangeLabel.isHidden = true
        changeIdCardBackButton.isEnabled = false
        changeIdCardFrontButton.isEnabled = false

        if isRejectedEmployedView {
            scrollTopConstraint.constant = 0
            headerView.isHidden = true
            employedStatusView.isHidden = true
            motivoRechazoView.isHidden = true
            editDataInfoView.isHidden = true
            getDataInfoEmployed()
        }

        if isEmployedView {
            closeButtonwRejectedFlow.isHidden = true
            titleScreenLabel.text = "Vendedor"
            getDataInfoEmployed()
            buttonsRejectedView.isHidden = true

        }
    }

    func editInfoData() {
        noEmployedTextField.isEnabled = false
        rfcEmployedTextField.isEnabled = true
        emailEmployedTextField.isEnabled = true
        phoneEmployedTextField.isEnabled = true
        infoChangeLabel.isHidden = false
        changeIdCardFrontButton.isEnabled = true
        changeIdCardBackButton.isEnabled = true
        noEmployedTextField.borderActiveColor = UIColor(named: "Base_blue_text")
        rfcEmployedTextField.borderActiveColor = UIColor(named: "Base_blue_text")
        emailEmployedTextField.borderActiveColor = UIColor(named: "Base_blue_text")
        phoneEmployedTextField.borderActiveColor = UIColor(named: "Base_blue_text")
    }

    func setupTextField() {
        if isEditDataOn {
            noEmployedTextField.borderActiveColor = UIColor(named: "Base_blue_text")
            noEmployedTextField.borderWidth = 0.5
            rfcEmployedTextField.borderActiveColor = UIColor(named: "Base_blue_text")
            rfcEmployedTextField.borderWidth = 0.5
            emailEmployedTextField.borderActiveColor = UIColor(named: "Base_blue_text")
            emailEmployedTextField.borderWidth = 0.5
            phoneEmployedTextField.borderActiveColor = UIColor(named: "Base_blue_text")
            phoneEmployedTextField.borderWidth = 0.5

        } else {
            noEmployedTextField.borderWidth = 0
            rfcEmployedTextField.borderWidth = 0
            emailEmployedTextField.borderWidth = 0
            phoneEmployedTextField.borderWidth = 0
        }
    }

    func showMediaSource() {
        let view = UIStoryboard(name: "MediaSourceViewController", bundle: nil)
        let controller = view.instantiateViewController(withIdentifier: "MediaSourceViewController") as! MediaSourceViewController
        controller.delegate = self
        controller.modalPresentationStyle = .overFullScreen
        self.present(controller, animated: true)
    }
}

extension ActiveCoworkerController: TPMediaDocumentsSourceProtocol {

    func receiveImageToIDCardFront(image: UIImage) {
        self.idCardFrontEmployedImage.image = image
    }

    func receiveImageToIDCardBack(image: UIImage) {
        self.idCardBackEmployedImage.image = image
    }

    func reciveBankDocument(image: UIImage) {
        self.estadoDeCuentaImage.image = image
    }
}
