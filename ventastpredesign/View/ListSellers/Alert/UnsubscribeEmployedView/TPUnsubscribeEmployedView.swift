//
//  UnsubscribeEmployedView.swift
//  SalesCloud
//
//  Created by Axlestevez on 01/02/24.
//  Copyright © 2024 TotalPlay. All rights reserved.
//

import UIKit
import TextFieldEffects

protocol TPReceiveUnsubscribeEmployedData {
    func receiveData(isReaffiliationAviable: Bool, motivoRechazo: String)
}

class TPUnsubscribeEmployedView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var buttonSelectedOption: UIButton!
    @IBOutlet weak var reafiliacionCheckup: UIView!
    @IBOutlet weak var motivoNoReafiliacion: UIView!
    @IBOutlet weak var noReafiliacionReasonTXT: HoshiTextField!
    @IBOutlet weak var checkButton: UIButton!

    var pickerView: TPUnsubscribeReasonPicker!
    var isReafiliacionAviable: Bool = false
    var observer: TPReceiveUnsubscribeEmployedData?

    let imageUncheck = UIImage(named: "icon_uncheck_addons")
    let imagecCheck = UIImage(named: "Image_rede_blue_check")

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
        setupView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func commonInit() {
        Bundle.main.loadNibNamed("\(TPUnsubscribeEmployedView.self)", owner: self)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }

    @IBAction func checkupNoAfiliacion(_ sender: Any) {
        if isReafiliacionAviable {
            isReafiliacionAviable = false
            checkButton.setImage(imageUncheck, for: .normal)
            motivoNoReafiliacion.isHidden = false
        } else {
            isReafiliacionAviable = true
            checkButton.setImage(imagecCheck, for: .normal)
            motivoNoReafiliacion.isHidden = true
        }
    }

    @IBAction func showOptionCombo(_ sender: Any) {
        showPicker()
    }

    @IBAction func closeView(_ sender: Any) {
        self.removeFromSuperview()
    }

    @IBAction func sendInfoAction(_ sender: Any) {
        if isReafiliacionAviable {
            observer?.receiveData(
                isReaffiliationAviable: isReafiliacionAviable,
                motivoRechazo: noReafiliacionReasonTXT.text ?? String.emptyString
            )
        } else {
            if noReafiliacionReasonTXT.text!.isEmpty {
                noReafiliacionReasonTXT.borderInactiveColor = .red
            } else {
                observer?.receiveData(
                    isReaffiliationAviable: isReafiliacionAviable,
                    motivoRechazo: noReafiliacionReasonTXT.text ?? String.emptyString
                )
            }
        }
        self.removeFromSuperview()

    }

    func evalOptionSelected(optionSelected: String) {
        guard let option = TPReasonUnsubscribeEmployedCases(rawValue: optionSelected) else { return }

        switch option {
        case .BAD_PARACTICS:
            motivoNoReafiliacion.isHidden = false
            isReafiliacionAviable = false
        case .DOW_VOLUNTARY:
            motivoNoReafiliacion.isHidden = false
            reafiliacionCheckup.isHidden = false
        case .NO_SELECTED:
            motivoNoReafiliacion.isHidden = true
            reafiliacionCheckup.isHidden = true
        }
    }
}

// MARK: SetupView

extension TPUnsubscribeEmployedView {

    func setupView() {
        reafiliacionCheckup.isHidden = true
        motivoNoReafiliacion.isHidden = true
    }
}

// MARK: PickerView

extension TPUnsubscribeEmployedView: TPSendDataToViewFromPicker {

    func receiveData(optionSelected: String) {
        self.buttonSelectedOption.setTitle(optionSelected, for: .normal)
        self.evalOptionSelected(optionSelected: optionSelected)
    }

    func showPicker () {
        pickerView = TPUnsubscribeReasonPicker(frame: CGRect(
            x: 0,
            y: 0,
            width: self.bounds.width,
            height: self.bounds.height
        ))
        pickerView.observer = self
        self.addSubview(pickerView)
    }
}
