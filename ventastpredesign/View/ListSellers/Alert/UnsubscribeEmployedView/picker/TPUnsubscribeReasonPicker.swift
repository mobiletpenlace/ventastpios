//
//  TPUnsubscribeReasonPicker.swift
//  SalesCloud
//
//  Created by Axlestevez-trabajo on 13/02/24.
//  Copyright © 2024 TotalPlay. All rights reserved.
//

import UIKit

protocol TPSendDataToViewFromPicker {
    func receiveData(optionSelected: String)
}

class TPUnsubscribeReasonPicker: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var selectedButton: UIButton!

    var observer: TPSendDataToViewFromPicker?

    override init(frame: CGRect) {
        super.init(frame: frame)
        internalInit()
        initPicker()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func internalInit() {
        Bundle.main.loadNibNamed("\(TPUnsubscribeReasonPicker.self)", owner: self)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }

    private func initPicker() {
        pickerView.delegate = self
        pickerView.dataSource = self

        selectedButton.addTarget(self, action: #selector(selectOption), for: .touchUpInside)
    }
}

extension TPUnsubscribeReasonPicker: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
            return 1 // Una sola columna en el picker
        }

        func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            return TPReasonUnsubscribeEmployed.OPTIONS.count
        }

        func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            return TPReasonUnsubscribeEmployed.OPTIONS[row]
        }

        // MARK: - Actions

        @objc func selectOption() {
            let selectedOption = TPReasonUnsubscribeEmployed.OPTIONS[pickerView.selectedRow(inComponent: 0)]
            observer?.receiveData(optionSelected: selectedOption)
            self.removeFromSuperview()
        }
}
