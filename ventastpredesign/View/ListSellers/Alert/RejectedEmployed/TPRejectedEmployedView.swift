//
//  TPRejectedEmployedView.swift
//  SalesCloud
//
//  Created by Axlestevez on 22/01/24.
//  Copyright © 2024 TotalPlay. All rights reserved.
//

import SwiftEventBus
import UIKit
import Alamofire
import ObjectMapper
import SwiftyJSON

protocol TPRejectedEmployedViewObserver {
    func receiveRejectedReason(rejectedReason: String, idEmployedToSend: String)
}

class TPRejectedEmployedView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var descriptionRejectedTXT: UITextField!

    var idEmployedToSend: String!
    var viewModel = ListSellerViewModel()
    var observer: TPRejectedEmployedViewObserver?

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    private func commonInit() {
        Bundle.main.loadNibNamed("\(TPRejectedEmployedView.self)", owner: self)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }

    @IBAction func rejectedAction(_ sender: Any) {
        rejectedEmployed()
    }

    @IBAction func closeView(_ sender: Any) {
        self.removeFromSuperview()
    }

    private func rejectedEmployed() {
        let rejectedReasonString = descriptionRejectedTXT.text ?? String.emptyString
        observer?.receiveRejectedReason(rejectedReason: rejectedReasonString, idEmployedToSend: idEmployedToSend)
        self.removeFromSuperview()
    }
}
