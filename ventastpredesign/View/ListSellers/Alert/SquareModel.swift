//
//  SquareModel.swift
//  SalesCloud
//
//  Created by Hector Alfonso Moreno Santa Cruz on 19/05/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import RealmSwift

class SquareModel: Object, Mappable {

    @objc dynamic var square: Int = 0
    @objc dynamic var squareName: String = ""
    @objc dynamic var idSquare: Int = 0

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        square <- map["plaza"]
        squareName <- map["NombrePlaza"]
        idSquare <- map["id"]
    }

}
