//
//  CreateCommentView.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 15/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

class CreateCommentView: BaseVentasView, CreateLeadObserver, UITextViewDelegate {

    @IBOutlet var textArea: UITextView!
    var mCreateLeadViewModel: CreateLeadViewModel!
    var idLeadCurrent: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mCreateLeadViewModel = CreateLeadViewModel(view: self)
        mCreateLeadViewModel.atachView(observer: self)
        textArea.delegate = self
        textArea.text = "Escribe tu mensaje aqui"
        textArea.textColor = UIColor.lightGray
    }

    @IBAction func goBack(_ sender: Any) {
        Constants.Back(viewC: self)
    }

    @IBAction func sendComment(_ sender: Any) {
        if textArea.text != "" {
            mCreateLeadViewModel.createCommentary(msj: textArea.text ?? "", idLead: idLeadCurrent)
        } else {
            Constants.Alert(title: "", body: dialogs.Error.FillFields, type: type.Error, viewC: self)
        }
    }

    func textViewDidBeginEditing(_ textView: UITextView) {
        if textArea.textColor == UIColor.lightGray {
            textArea.text = nil
            textArea.textColor = UIColor(named: "Base_blue_text")!
        }
    }

    func successCreate(id: String) {

    }

    func successEstatus() {

    }

    func successCommentary() {

    }

    func successCreateCommentary() {
        clearData()
        Constants.Alert(title: "", body: dialogs.Success.createCommentaryLead, type: type.Success, viewC: self, dimissParent: true)
    }

    func onError(errorMessage: String) {
        Constants.Alert(title: "", body: errorMessage, type: type.Error, viewC: self)
    }

    func clearData() {
        textArea.text = ""
    }

}
