//
//  CommentaryView.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 07/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

class CommentaryView: BaseVentasView, CreateLeadObserver {

    @IBOutlet weak var commentsTable: UITableView!
    @IBOutlet weak var constraintheight: NSLayoutConstraint!
    @IBOutlet weak var numberLeadLabel: UILabel!
    @IBOutlet weak var motivoLabel: UILabel!
    var listaComentarios: [Comentarios] = []
    var mCreateLeadViewModel: CreateLeadViewModel!
    var numLead = ""
    var Motivo = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mCreateLeadViewModel = CreateLeadViewModel(view: self)
        mCreateLeadViewModel.atachView(observer: self)
        getListTable()
    }

    @IBAction func goBack(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }

    func getListTable() {
        numberLeadLabel.text = numLead
        if Motivo == "" {
            motivoLabel.text = "Motivo no registrado"
        } else {
            motivoLabel.text = Motivo
        }
        listaComentarios = mCreateLeadViewModel.getComentarios()
        if listaComentarios.count > 2 {
            constraintheight.constant = 400
        } else {
            constraintheight.constant = 300
        }
    }
}

extension CommentaryView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        listaComentarios.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentaryViewCell", for: indexPath) as! CommentaryViewCell
        cell.createByLabel.text = listaComentarios[indexPath.row].creadoPor
        cell.dateLabel.text = listaComentarios[indexPath.row].fechaCreacion
        cell.commentLabel.text = mCreateLeadViewModel.clearText(text: listaComentarios[indexPath.row].texto)

        return cell

    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110

    }

    func successCreate(id: String) {

    }

    func successEstatus() {

    }

    func successCommentary() {

    }

    func onError(errorMessage: String) {

    }

    func successCreateCommentary() {

    }

}
