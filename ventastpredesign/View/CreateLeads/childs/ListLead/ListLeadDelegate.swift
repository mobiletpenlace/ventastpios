//
//  ListLeadDelegate.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 10/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
extension ListLeadView: CreateLeadObserver {
    func successCreateCommentary() {

    }

    func successCreate(id: String) {

    }

    func successEstatus() {

    }

    func successCommentary() {
        listaComentarios = []
        listaComentarios = mCreateLeadViewModel.getComentarios()
        motivo = mCreateLeadViewModel.getMotivos().motivoDescartadoFuturo
        if listaComentarios.count == 0 {
            Constants.Alert(title: "", body: "Sin comentarios para mostrar", type: type.Info, viewC: self)
        } else {
            goComentarios()
        }
    }

    func onError(errorMessage: String) {
        Constants.Alert(title: "", body: errorMessage, type: type.Error, viewC: self)
    }

}
