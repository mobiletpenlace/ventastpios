//
//  DetalleLeadView.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 07/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

class ListLeadView: BaseVentasView {
    @IBOutlet weak var LeadsTable: UITableView!
    var mCreateLeadViewModel: CreateLeadViewModel!
    var listaLead: [leadRecomendado] = []
    var listaComentarios: [Comentarios] = []
    var tipoLead = ""
    var numberLead = ""
    var motivo: String = "Motivo no registrado"
    var hideButtonAddComment = false

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mCreateLeadViewModel = CreateLeadViewModel(view: self)
        mCreateLeadViewModel.atachView(observer: self)
        getListTable()
    }

    @IBAction func goBack(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }

    func consultComments(idLead: String) {
        mCreateLeadViewModel.consultComments(tipoRegistro: idLead)
    }

    func getListTable() {
        switch tipoLead {
        case "SIN EXITO":
            listaLead = mCreateLeadViewModel.getSinExito()
        case "POR CONTACTAR":
            listaLead = mCreateLeadViewModel.getPorContactar()
        case "POR INSTALAR":
            listaLead = mCreateLeadViewModel.getPorInstalar()
        case "INSTALADOS":
            listaLead = mCreateLeadViewModel.getInstaladas()
        default:
            break
        }
        validateShowComment(tipo: tipoLead)
    }

    func goComentarios() {
        let listaCommentView: CommentaryView = UIStoryboard(name: "CommentaryView", bundle: nil).instantiateViewController(withIdentifier: "CommentaryView") as! CommentaryView
        listaCommentView.numLead = "Lead " + numberLead
        listaCommentView.Motivo = motivo
        self.present(listaCommentView, animated: true, completion: nil)
    }

    func goAddComentario(id: String) {
        let viewAlert: UIStoryboard = UIStoryboard(name: "CreateCommentView", bundle: nil)
        let viewAlertVC = viewAlert.instantiateViewController(withIdentifier: "CreateCommentView")as! CreateCommentView
        viewAlertVC.idLeadCurrent = id
        viewAlertVC.providesPresentationContextTransitionStyle = true
        viewAlertVC.definesPresentationContext = true
        viewAlertVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        viewAlertVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(viewAlertVC, animated: false, completion: {})
    }

    func validateShowComment(tipo: String) {
        if tipo == "POR CONTACTAR" || tipo == "SIN EXITO" {
            hideButtonAddComment = false
        } else {
            hideButtonAddComment = true
        }
    }

}

extension ListLeadView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        listaLead.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListLeadViewCell", for: indexPath) as! ListLeadViewCell
        cell.tipo.text = listaLead[indexPath.row].tipo
        if listaLead[indexPath.row].tipo == "INSTALADOS" ||  listaLead[indexPath.row].tipo == "POR INSTALAR" {
            cell.OsLabel.text = listaLead[indexPath.row].folioOS
            cell.OsView.isHidden = false
        } else {
            cell.OsView.isHidden = true
        }
        cell.addCommentView.isHidden = hideButtonAddComment
        cell.Nombre.text = listaLead[indexPath.row].nombre
        cell.fechaCreacion.text = listaLead[indexPath.row].fechaEnvio
        cell.numeroLead.text = listaLead[indexPath.row].codigoVenta
        cell.selectButton.tag = indexPath.row
        cell.buttonAddComment.tag = indexPath.row
        cell.buttonAddComment.addTarget(self, action: #selector(addCommentButtonTapped(sender: )), for: .touchUpInside)
        cell.selectButton.addTarget(self, action: #selector(handleButtonTapped(sender: )), for: .touchUpInside)
        return cell

    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if hideButtonAddComment {
            return 270
        }
        return 300

    }

    @objc func handleButtonTapped(sender: UIButton) {
        let id = listaLead[sender.tag].id
        numberLead = listaLead[sender.tag].codigoVenta
        consultComments(idLead: id)
    }

    @objc func addCommentButtonTapped(sender: UIButton) {
        let id = listaLead[sender.tag].id
      goAddComentario(id: id)
    }

}
