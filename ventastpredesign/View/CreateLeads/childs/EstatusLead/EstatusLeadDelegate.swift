//
//  EstatusLeadDelegate.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 10/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation

extension EstatusLeadView: CreateLeadObserver {
    func successCreateCommentary() {

    }

    func successCreate(id: String) {

    }

    func successEstatus() {
        fullDataStatus()
    }

    func successCommentary() {

    }

    func onError(errorMessage: String) {
        Constants.Alert(title: "", body: errorMessage, type: type.Error, viewC: self)
    }

}

extension EstatusLeadView: getDateDelegate {
    func getDate(date: String, id: String) {
        if id == "dateStart" {
            dateStartTypeString =  mCreateLeadViewModel.convertirFechaCalendar(fecha: date ).0
            dateStartTypeDate =  mCreateLeadViewModel.convertirFechaCalendar(fecha: date ).1
            dateStartLabel.text = dateStartTypeString
            consultStatusLead()
        } else if id == "dateFinish" {
            dateFinishTypeString =  mCreateLeadViewModel.convertirFechaCalendar(fecha: date ).0
            dateFinishTypeDate =  mCreateLeadViewModel.convertirFechaCalendar(fecha: date ).1
            dateFinishLabel.text = dateFinishTypeString
            consultStatusLead()
        }
    }
}
