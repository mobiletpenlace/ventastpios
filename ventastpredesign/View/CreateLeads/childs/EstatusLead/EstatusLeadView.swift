//
//  EstatusLeadView.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 07/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import Amplitude

class EstatusLeadView: BaseVentasView {
    @IBOutlet  var numSinExitoLabel: UILabel!
    @IBOutlet  var numPorInstalarLabel: UILabel!
    @IBOutlet var nameEmployeeLabel: UILabel!
    @IBOutlet  var numPorContactarLabel: UILabel!
    @IBOutlet  var numInstaladosLabel: UILabel!
    @IBOutlet  var dateStartLabel: UITextField!
    @IBOutlet  var dateFinishLabel: UITextField!
    var mCreateLeadViewModel: CreateLeadViewModel!
    var dateStartTypeDate: Date?
    var dateFinishTypeDate: Date?
    var dateStartTypeString: String = ""
    var dateFinishTypeString: String = ""
    var currentEmployee = ""
    var isHostess = false
    var nameEmployee: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mCreateLeadViewModel = CreateLeadViewModel(view: self)
        mCreateLeadViewModel.atachView(observer: self)
        setDateDefault()
        consultStatusLead()
    }

    override func viewDidAppear(_ animated: Bool) {
        Amplitude.instance().logEvent("Show_StatusLeadScreen")
    }

    @IBAction func sinExitoAction(_ sender: Any) {
        nextPage(id: "SIN EXITO", textoLabel: numSinExitoLabel.text ?? "0")
    }

    @IBAction func porInstalarAction(_ sender: Any) {
        nextPage(id: "POR INSTALAR", textoLabel: numPorInstalarLabel.text ?? "0")
    }

    @IBAction func porContactarAction(_ sender: Any) {
        nextPage(id: "POR CONTACTAR", textoLabel: numPorContactarLabel.text ?? "0")
    }

    @IBAction func InstaladosAction(_ sender: Any) {
        nextPage(id: "INSTALADOS", textoLabel: numInstaladosLabel.text ?? "0")
    }

    @IBAction func goBack(_ sender: Any) {
        Constants.Back(viewC: self)
        Amplitude.instance().logEvent("Action_StatusLeadScreenBack_Button")
    }

    @IBAction func showCalendarStart(_ sender: Any) {
        let calendarView: CalendarView = UIStoryboard(name: "CalendarView", bundle: nil).instantiateViewController(withIdentifier: "CalendarView") as! CalendarView
        calendarView.getDateDelegate = self
        calendarView.id = "dateStart"
        calendarView.maximum = Date()
        calendarView.minimum = DateCalendar.futureDate(date: Date(), value: -1, component: .year)
        calendarView.providesPresentationContextTransitionStyle = true
        calendarView.definesPresentationContext = true
        calendarView.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        calendarView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(calendarView, animated: false, completion: nil)
        Amplitude.instance().logEvent("Action_ChangeDates_Button")
    }

    @IBAction func showCalendarFinish(_ sender: Any) {
        let calendarView: CalendarView = UIStoryboard(name: "CalendarView", bundle: nil).instantiateViewController(withIdentifier: "CalendarView") as! CalendarView
        calendarView.getDateDelegate = self
        calendarView.maximum = Date()
        calendarView.id = "dateFinish"
        calendarView.minimum = dateStartTypeDate
        calendarView.providesPresentationContextTransitionStyle = true
        calendarView.definesPresentationContext = true
        calendarView.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        calendarView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(calendarView, animated: false, completion: nil)
        Amplitude.instance().logEvent("Action_ChangeDates_Button")
    }

    func consultStatusLead() {
        let numEmployee = isHostess == false ? mCreateLeadViewModel.getCurrentNumEmployee() : currentEmployee
        mCreateLeadViewModel.consultStatusLead( dateStart: dateStartTypeString, dateFinish: dateFinishTypeString, user: numEmployee)
    }

    func setDateDefault() {
        nameEmployeeLabel.text =  nameEmployee == "" ? mCreateLeadViewModel.getCurrentNameEmployee() : nameEmployee
        if nameEmployeeLabel.text == "" { nameEmployeeLabel.text = "- -"}
        dateFinishTypeString = mCreateLeadViewModel.getToday()
        dateStartTypeString = mCreateLeadViewModel.getDateLastThirtyDays()

        dateFinishTypeDate = Date()
        dateStartTypeDate = mCreateLeadViewModel.getDateLastThirtyDaysDate()
        dateStartLabel.text = dateStartTypeString
        dateFinishLabel.text = dateFinishTypeString
    }

    func nextPage(id: String, textoLabel: String) {
        if textoLabel != "0" {
            goListLead(idButton: id)
        } else {
            Constants.Alert(title: "", body: dialogs.Info.WithOutLead, type: type.Info, viewC: self)
        }

    }

    func goListLead(idButton: String) {
        let listaLead: ListLeadView = UIStoryboard(name: "ListLeadView", bundle: nil).instantiateViewController(withIdentifier: "ListLeadView") as!  ListLeadView
        listaLead.tipoLead = idButton
        self.present(listaLead, animated: false, completion: nil)
    }

    func fullDataStatus() {
        numInstaladosLabel.text = String(mCreateLeadViewModel.getInstaladas().count)
        numSinExitoLabel.text = String(mCreateLeadViewModel.getSinExito().count)
        numPorContactarLabel.text = String(mCreateLeadViewModel.getPorContactar().count)
        numPorInstalarLabel.text = String(mCreateLeadViewModel.getPorInstalar().count)
    }

}
