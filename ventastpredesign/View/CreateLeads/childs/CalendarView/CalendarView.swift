//
//  CalendarView.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 10/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import FSCalendar

protocol getDateDelegate {
    func getDate(date: String, id: String)
}
class CalendarView: UIViewController, FSCalendarDelegate, FSCalendarDataSource {

    @IBOutlet weak var calendarView: FSCalendar!
    var getDateDelegate: getDateDelegate?
    var minimum: Date?
    var maximum: Date?
    var id: String = ""
    var currentDateSelected: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        configCalendar()
    }

    @IBAction func goBack(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }

    @IBAction func goNext(_ sender: Any) {
        if (calendarView?.selectedDate) != nil {
            let newDate = DateCalendar.convertDateString(date: calendarView.selectedDate!, formatter: Formate.dd_MM_yyyy)
            getDateDelegate?.getDate(date: newDate, id: id)
            dismiss(animated: false, completion: nil)
        } else {
            // alertMessage(message: "Favor de elegir una fecha valida", title: "AVISO")
            Constants.Alert(title: "AVISO", body: "Favor de elegir una fecha valida", type: type.Warning, viewC: self)
        }
    }

    func minimumDate(for calendar: FSCalendar) -> Date {
        var date: Date?
        if  minimum != nil {
            date = minimum
        } else {
            date = Date()
        }
        return date!
    }

    func maximumDate(for calendar: FSCalendar) -> Date {
        var date: Date?
        if  maximum != nil {
            date = maximum
        } else {
            date = DateCalendar.futureDate(date: Date(), value: 1, component: .year)
        }
        return date!
    }

    func configCalendar() {

        calendarView.locale = Locale(identifier: "Es")

        if let date = currentDateSelected {
            calendarView.select(DateCalendar.convertStringDate(date: date, formatter: Formate.dd_MM_yyyy))
        }
    }

}
