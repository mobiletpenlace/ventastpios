//
//  ListPromotorView.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 27/09/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

class ListPromotorView: BaseVentasView {
    @IBOutlet weak var ListHostessTable: UITableView!
    var mEventViewModel: EventViewModel!
    var hostess: [team] = []
    var currentEmployeeName: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mEventViewModel = EventViewModel(view: self)
        mEventViewModel.atachView(observer: self)
        consultHostess()
    }

    @IBAction func goBack(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }

    func consultHostess() {
        mEventViewModel.ConsultListHostess()
    }

    func goStatusLead(numEmploye: String) {
        let statusLeadView: EstatusLeadView = UIStoryboard(name: "EstatusLeadView", bundle: nil).instantiateViewController(withIdentifier: "EstatusLeadView") as! EstatusLeadView
        statusLeadView.currentEmployee = numEmploye
        statusLeadView.nameEmployee = currentEmployeeName
        statusLeadView.isHostess = true
        self.present(statusLeadView, animated: false, completion: nil)
    }

}

extension ListPromotorView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        hostess.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListHostessTableViewCell", for: indexPath) as! ListHostessTableViewCell
        cell.nameHostesLabel.text = hostess[indexPath.row].name
        cell.selectButton.tag = indexPath.row
        cell.selectButton.addTarget(self, action: #selector(handleButtonTapped(sender: )), for: .touchUpInside)
        return cell

    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90

    }

    @objc func handleButtonTapped(sender: UIButton) {
        let employe = hostess[sender.tag].numeroEmpleado
        currentEmployeeName = hostess[sender.tag].name
        goStatusLead(numEmploye: employe)
    }

}
extension ListPromotorView: EventObserver {
    func successDeleteEvent() { }
    func successEditEvent() {}

    func successDeleteHostess() {}
    func retryEvent(id: String) {}

    func pickerOptions(value: String, id: String) {}

    func successEvents() {}

    func successDetailEvent() { }

    func successListHostes() {
        hostess = mEventViewModel.getHostessDesencripte()
        ListHostessTable.reloadData()
    }

    func successAssignedHostes() {}

    func successCategory() { }

    func onError(errorMessage: String) {
        if errorMessage != "Error al cargar los hostess para la falta" {
            Constants.Alert(title: "", body: errorMessage, type: type.Error, viewC: self)
        }
    }

}
