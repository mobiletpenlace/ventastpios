//
//  ListHostessTableViewCell.swift
//  StreetTpIOS
//
//  Created by Marisol Huerta O. on 30/03/21.
//  Copyright © 2021 Juan Reynaldo Escobar Miron. All rights reserved.
//

import UIKit

class ListHostessTableViewCell: UITableViewCell {
    @IBOutlet weak var nameHostesLabel: UILabel!
    @IBOutlet weak var selectButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
