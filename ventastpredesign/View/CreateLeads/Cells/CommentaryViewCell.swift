//
//  CommentaryViewCell.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 07/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

class CommentaryViewCell: UITableViewCell {
    @IBOutlet weak var createByLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
