//
//  ListLeadViewCell.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 07/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

class ListLeadViewCell: UITableViewCell {
    @IBOutlet weak var tipo: UILabel!
    @IBOutlet weak var numeroLead: UILabel!
    @IBOutlet weak var selectButton: UIButton!
    @IBOutlet weak var Nombre: UILabel!
    @IBOutlet weak var fechaCreacion: UILabel!
    @IBOutlet weak var OsView: UIView!
    @IBOutlet weak var OsLabel: UILabel!
    @IBOutlet var addCommentView: UIView!
    @IBOutlet var buttonAddComment: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
