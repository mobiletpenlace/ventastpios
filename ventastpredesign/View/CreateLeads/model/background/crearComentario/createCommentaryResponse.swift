//
//  createCommentaryResponse.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 14/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class createCommentaryResponse: BaseResponse {
    var message: String = ""
    var status: String = ""

     required init?(map: Map) {

       }

    override func mapping(map: Map) {
        message <- map["mensaje"]
        status <- map["estatus"]

    }

}
