//
//  createCommentaryRequest.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 14/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class createCommentaryRequest: BaseRequest {

    var idLead: String = ""
    var comment: String = ""

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        idLead <- map["idObject"]
        comment <- map["mensajeChatter"]
    }

}
