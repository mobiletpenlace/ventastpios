//
//  Comentarios.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 08/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift

class Comentarios: Object, Mappable {
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  tipoPost: String =  ""
    @objc dynamic var  texto: String =  ""
    @objc dynamic var  Id: String =  ""
    @objc dynamic var  fechaCreacion: String =  ""
    @objc dynamic var  creadoPor: String =  ""

    public required convenience init?(map: Map) {
        self.init()
    }

    override static func primaryKey() -> String? {
           return "uui"
    }

    func mapping(map: Map) {
        tipoPost <- map["tipoPost"]
        texto <- map["texto"]
        Id <- map["Id"]
        fechaCreacion <- map["fechaCreacion"]
        creadoPor <- map["creadoPor"]
    }
}
