//
//  MotivoDetalle.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 08/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift

class MotivoDetalle: Object, Mappable {
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  motivosOpp: MotivosOpp?
    @objc dynamic var  mMotivosLead: MotivosLead?

    public required convenience init?(map: Map) {
        self.init()
    }

    override static func primaryKey() -> String? {
           return "uui"
    }

    func mapping(map: Map) {
        motivosOpp <- map["motivosOpp"]
        mMotivosLead <- map["motivosLead"]
    }
}
