//
//  CommentLeadRequest.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 08/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class CommentLeadRequest: BaseRequest {
    var idRegistro: String = ""
    var tipoObjeto: String = ""

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        idRegistro <- map["idRegistro"]
        tipoObjeto <- map["tipoObjeto"]
    }

}
