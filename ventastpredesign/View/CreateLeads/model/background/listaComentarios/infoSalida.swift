//
//  infoSalida.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 08/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift

class infoSalida: Object, Mappable {
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  mMotivosDetalle: MotivoDetalle?
    var mComentarios: [Comentarios] = []

    public required convenience init?(map: Map) {
        self.init()
    }

    override static func primaryKey() -> String? {
           return "uui"
    }

    func mapping(map: Map) {
        mMotivosDetalle <- map["motivosDetalle"]
        mComentarios <- map["comentarios"]
    }
}
