//
//  CommentLeadResponse.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 08/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class CommentLeadResponse: BaseResponse {
    var mInfoSalida: infoSalida?

     required init?(map: Map) {

       }

    override func mapping(map: Map) {
        mInfoSalida <- map["infoSalida"]
        resultDescription <- map["resultDescription"]
        result <- map["result"]
    }

}
