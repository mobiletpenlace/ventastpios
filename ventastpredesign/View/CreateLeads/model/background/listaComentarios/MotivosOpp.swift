//
//  MotivosOpp.swift
//  ventastp
//
//  Created by Ulises Ortega on 18/08/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift

class MotivosOpp: Object, Mappable {
    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        motivoRechazoMC <- map["motivoRechazoMC"]
        motivoPendienteMC <- map["motivoPendienteMC"]
        detalleMotivoRechazo <- map["detalleMotivoRechazo"]
        comentariosMC <- map["comentariosMC"]
    }

    var motivoRechazoMC: String?
    var motivoPendienteMC: String?
    var detalleMotivoRechazo: String?
    var comentariosMC: String?
}
