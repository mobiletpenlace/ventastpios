//
//  informacion.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 08/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift

class informacion: Object, Mappable {
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  nombre: String = ""
    @objc dynamic var  telefono: String = ""
    @objc dynamic var  email: String = ""
    @objc dynamic var  codigoPostal: String = ""
    @objc dynamic var  estatus: String = ""
    @objc dynamic var  giro: String = ""
    @objc dynamic var  estado: String = ""
    @objc dynamic var  delMunicipio: String = ""
    @objc dynamic var  calle: String = ""
    @objc dynamic var  numeroCalleInt: String = ""
    @objc dynamic var  numeroCalleExt: String = ""
    @objc dynamic var  origen: String = ""
    @objc dynamic var  campaniaId: String = ""
    @objc dynamic var  latitud: String = ""
    @objc dynamic var  longitud: String = ""
    @objc dynamic var  gclid: String = ""
    @objc dynamic var  tipoRegistro: String = ""
    @objc dynamic var  canalReferido: String = ""
    @objc dynamic var  cuentaReferente: String = ""
    @objc dynamic var  nombreTecnico: String = ""
    @objc dynamic var  numEmpleadoTecnico: String = ""
    @objc dynamic var  nombreAuxiliar: String = ""
    @objc dynamic var  numEmpleadoAuxiliar: String = ""
    @objc dynamic var  subCanal: String = ""
    @objc dynamic var  ventaOrigen: String = ""

    public required convenience init?(map: Map) {
        self.init()
    }

    override static func primaryKey() -> String? {
           return "uui"
    }

    func mapping(map: Map) {
        nombre <- map["nombre"]
        telefono <- map["telefono"]
        email <- map["email"]
        codigoPostal <- map["codigoPostal"]
        estatus <- map["estatus"]
        giro <- map["giro"]
        estado <- map["estado"]
        delMunicipio <- map["delMunicipio"]
        calle <- map["calle"]
        numeroCalleInt <- map["numeroCalleInt"]
        numeroCalleExt <- map["numeroCalleExt"]
        origen <- map["origen"]
        campaniaId <- map["campaniaId"]
        latitud <- map["latitud"]
        longitud <- map["longitud"]
        gclid <- map["gclid"]
        tipoRegistro <- map["tipoRegistro"]
        canalReferido <- map["canalReferido"]
        cuentaReferente <- map["cuentaReferente"]
        nombreTecnico <- map["nombreTecnico"]
        numEmpleadoTecnico <- map["numEmpleadoTecnico"]
        nombreAuxiliar <- map["nombreAuxiliar"]
        numEmpleadoAuxiliar <- map["numEmpleadoAuxiliar"]
        subCanal <- map["subCanal"]
        ventaOrigen <- map["ventaOrigen"]

    }

}
