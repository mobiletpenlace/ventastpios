//
//  creacionLeadRequest.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 08/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class creacionLeadRequest: BaseRequest {

    var MInformacion: informacion?

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        MInformacion <- map["informacion"]
    }

}
