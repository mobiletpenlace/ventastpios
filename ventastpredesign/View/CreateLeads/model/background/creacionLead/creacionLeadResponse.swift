//
//  creacionLeadResponse.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 08/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class creacionLeadResponse: BaseResponse {

    var IdLead: String = ""

     required init?(map: Map) {

       }

    override func mapping(map: Map) {
        resultDescription <- map["resultDescription"]
        result <- map["result"]
        IdLead <- map["IdLead"]
    }

}
