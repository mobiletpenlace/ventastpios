//
//  leadRecomendado.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 08/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift

class leadRecomendado: Object, Mappable {
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  tipoVenta: String = ""
    @objc dynamic var  tipo: String = ""
    @objc dynamic var  telefono: String = ""
    @objc dynamic var  nombre: String = ""
    @objc dynamic var  mesEnvio: String = ""
    @objc dynamic var  id: String = ""
    @objc dynamic var  fechaEnvio: String = ""
    @objc dynamic var  fechaActivacion: String = ""
    @objc dynamic var  codigoVenta: String = ""
    @objc dynamic var  anioEnvio: String = ""
    @objc dynamic var  folioOS: String = ""
    @objc dynamic var  estatusOS: String = ""

    public required convenience init?(map: Map) {
        self.init()
    }

    override static func primaryKey() -> String? {
           return "uui"
    }

    func mapping(map: Map) {
        tipoVenta <- map["tipoVenta"]
        tipo <- map["tipo"]
        telefono <- map["telefono"]
        nombre <- map["nombre"]
        mesEnvio <- map["mesEnvio"]
        id <- map["id"]
        folioOS <- map["folioOS"]
        fechaEnvio <- map["fechaEnvio"]
        fechaActivacion <- map["fechaActivacion"]
        estatusOS <- map["estatusOS"]
        codigoVenta <- map["codigoVenta"]
        anioEnvio <- map["anioEnvio"]

    }
}
