//
//  statusLeadResponse.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 08/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class statusLeadResponse: BaseResponse {
    var mDatos: DatosLeads?

    required init?(map: Map) {
    }

    override func mapping(map: Map) {
        mDatos <- map["datos"]
        resultDescription <- map["resultDescription"]
        result <- map["result"]
    }

}
