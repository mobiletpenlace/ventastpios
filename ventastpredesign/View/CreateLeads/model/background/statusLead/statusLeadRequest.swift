//
//  statusLeadRequest.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 08/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class statusLeadRequest: BaseRequest {

    var strFechaInicio: String = ""
    var strFechaFin: String = ""
    var numEmpleado: String = ""
    var tipo: String = ""

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        strFechaInicio <- map["strFechaInicio"]
        strFechaFin <- map["strFechaFin"]
        numEmpleado <- map["numEmpleado"]
        tipo <- map["tipo"]
    }

}
