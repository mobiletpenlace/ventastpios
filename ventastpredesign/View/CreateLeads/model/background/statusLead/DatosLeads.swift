//
//  DatosLeads.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 08/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift

class DatosLeads: Object, Mappable {
    @objc dynamic var  uui: String = UUID().uuidString
    var sinExito: [leadRecomendado] = []
    var porInstalar: [leadRecomendado] = []
    var porContactar: [leadRecomendado] = []
    var instalados: [leadRecomendado] = []
    var contactados: [leadRecomendado] = []
    @objc dynamic var  msjInfo: String = ""

    public required convenience init?(map: Map) {
        self.init()
    }

    override static func primaryKey() -> String? {
           return "uui"
    }

    func mapping(map: Map) {
        sinExito <- map["sinExito"]
        porInstalar <- map["porInstalar"]
        porContactar <- map["porContactar"]
        instalados <- map["instalados"]
        contactados <- map["contactados"]
        msjInfo <- map["msjInfo"]

    }
}
