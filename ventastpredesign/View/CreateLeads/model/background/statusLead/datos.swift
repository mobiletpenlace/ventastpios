//
//  datos.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 08/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift

class datos: Object, Mappable {
    @objc dynamic var  uui: String = UUID().uuidString
    var sinExito: List<leadRecomendado> = List<leadRecomendado>()
    var porInstalar: List<leadRecomendado> = List<leadRecomendado>()
    var porContactar: List<leadRecomendado> = List<leadRecomendado>()
    var instalados: List<leadRecomendado> = List<leadRecomendado>()

    @objc dynamic var  msjInfo: String = ""

    public required convenience init?(map: Map) {
        self.init()
    }

    override static func primaryKey() -> String? {
           return "uui"
    }

    func mapping(map: Map) {
        sinExito <- map["sinExito"]
        porInstalar <- map["porInstalar"]
        porContactar <- map["porContactar"]
        instalados <- map["instalados"]
        msjInfo <- map["msjInfo"]

    }
}
