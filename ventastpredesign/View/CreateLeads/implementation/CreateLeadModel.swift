//
//  CreateLeadModel.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 08/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import RealmSwift

protocol CreateLeadModelObserver {
    func successCreateLead(idLead: String)
    func successCreateComment()
    func successEstatusLead()
    func successCommentaryLead()
    func onError(errorMessage: String)
}

class CreateLeadModel: BaseModel {

    var viewModel: CreateLeadModelObserver?
    var server: ServerDataManager2<creacionLeadResponse>?
    var server2: ServerDataManager2<statusLeadResponse>?
    var server3: ServerDataManager2<CommentLeadResponse>?
    var server4: ServerDataManager2<createCommentaryResponse>?
    var urlCreateLead = AppDelegate.API_SALESFORCE + ApiDefinition.API_CREATE_LEAD
    var urlEstatusLead = AppDelegate.API_SALESFORCE + ApiDefinition.API_STATUS_LEAD
    var urlCommentary =  AppDelegate.API_SALESFORCE + ApiDefinition.API_COMMENTS_LEAD
    var urlCreateCommentary =  AppDelegate.API_SALESFORCE + ApiDefinition.API_CREATE_COMMENTS_LEAD
    var idComent: String = ""
    var listaCommentarios = [Comentarios]()
    var motivo = "No hay motivo"
    var isCommnetOportunity = false
    var msjComentario = ""

    init(observer: BaseViewModelObserver) {
        super.init(observerVM: observer)

        server = ServerDataManager2<creacionLeadResponse>()
        server2 = ServerDataManager2<statusLeadResponse>()
        server3 = ServerDataManager2<CommentLeadResponse>()
        server4 = ServerDataManager2<createCommentaryResponse>()
    }

    func atachModel(viewModel: CreateLeadModelObserver) {
        self.viewModel = viewModel
    }

    func createComment(request: createCommentaryRequest) {
        urlCreateCommentary = AppDelegate.API_SALESFORCE + ApiDefinition.API_CREATE_COMMENTS_LEAD
        server4?.setValues(requestUrl: urlCreateCommentary, delegate: self, headerType: .headersSalesforce)
        server4?.request(requestModel: request)
    }

    func createLead(request: creacionLeadRequest) {
        urlCreateLead = AppDelegate.API_SALESFORCE + ApiDefinition.API_CREATE_LEAD
        server?.setValues(requestUrl: urlCreateLead, delegate: self, headerType: .headersSalesforce)
        server?.request(requestModel: request)
    }

    func estatusLead(request: statusLeadRequest) {
        urlEstatusLead = AppDelegate.API_SALESFORCE + ApiDefinition.API_STATUS_LEAD
        server2?.setValues(requestUrl: urlEstatusLead, delegate: self, headerType: .headersSalesforce)
        server2?.request(requestModel: request)
    }

    func commentaryLead(request: CommentLeadRequest) {
        self.idComent = request.idRegistro
        self.isCommnetOportunity = false
        urlCommentary =  AppDelegate.API_SALESFORCE + ApiDefinition.API_COMMENTS_LEAD
        server3?.setValues(requestUrl: urlCommentary, delegate: self, headerType: .headersSalesforce)
        server3?.request(requestModel: request)
    }

    func consultCommentLead() {
        let model: CommentLeadRequest = CommentLeadRequest()
        model.idRegistro = idComent
        model.tipoObjeto = "Oportunidad"
        self.isCommnetOportunity = true
        urlCommentary =  AppDelegate.API_SALESFORCE + ApiDefinition.API_COMMENTS_LEAD
        server3?.setValues(requestUrl: urlCommentary, delegate: self, headerType: .headersSalesforce)
        server3?.request(requestModel: model)
    }

    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if requestUrl == urlCreateLead {
            let crearResponse = response as! creacionLeadResponse
            if crearResponse.result == "1" {
                self.viewModel?.successCreateLead(idLead: crearResponse.IdLead)
            } else {
                viewModel?.onError(errorMessage: crearResponse.resultDescription )
            }
        } else if requestUrl == urlCreateCommentary {
            let crearResponse = response as! createCommentaryResponse
            if crearResponse.status == "0" {
                self.viewModel?.successCreateComment()
            } else {
                viewModel?.onError(errorMessage: crearResponse.message )
            }

        } else if requestUrl == urlEstatusLead {
            let estatusResponse = response as! statusLeadResponse
            if estatusResponse.result == "1" {
                self.saveStatusLead(status: estatusResponse)
                self.viewModel?.successEstatusLead()
            } else {
                viewModel?.onError(errorMessage: estatusResponse.resultDescription )
            }

        } else if requestUrl == urlCommentary {
            let commentaryResponse = response as! CommentLeadResponse
            if self.isCommnetOportunity {
                if commentaryResponse.result  == "0" {
                    self.saveCommentsOportunity(comment: commentaryResponse)
                }
                self.viewModel?.successCommentaryLead()
//                else{
//                    self.viewModel?.onError(errorMessage: commentaryResponse.resultDescription ?? "Error al consultar los comentarios, intenta de nuevo.")
//                }
            } else {
                if commentaryResponse.result  == "0" {
                    self.listaCommentarios = commentaryResponse.mInfoSalida!.mComentarios
                    self.motivo = commentaryResponse.mInfoSalida!.mMotivosDetalle?.mMotivosLead!.motivoDescartadoFuturo ?? "Motivo no registrado"
                    self.saveCommentsLead(comment: commentaryResponse)
                    self.consultCommentLead()
                } else {
                    self.viewModel?.onError(errorMessage: commentaryResponse.resultDescription )
                }
            }
        }
    }

    func saveStatusLead(status: statusLeadResponse) {
        RealManager.deleteAll(object: leadRecomendado.self)
        var listaLeads = [leadRecomendado]()
        listaLeads.append(contentsOf: status.mDatos!.sinExito)
        listaLeads.append(contentsOf: status.mDatos!.instalados)
        listaLeads.append(contentsOf: status.mDatos!.porInstalar)
        listaLeads.append(contentsOf: status.mDatos!.porContactar)
        RealManager.insertCollection(objects: listaLeads)
    }

    func getEmploye() -> Empleado {
        let employee = RealManager.findFirst(object: Empleado.self)
        return employee!
    }

    func getStatusPorContactar() -> [leadRecomendado] {
        let data = RealManager.getFromAll(object: leadRecomendado.self, field: "tipo", equalsTo: "POR CONTACTAR")!
        return data.reversed()
    }

    func getStatusSinExito() -> [leadRecomendado] {
        let data = RealManager.getFromAll(object: leadRecomendado.self, field: "tipo", equalsTo: "SIN EXITO")!
        return data.reversed()
    }

    func getStatusPorInstalar() -> [leadRecomendado] {
        let data = RealManager.getFromAll(object: leadRecomendado.self, field: "tipo", equalsTo: "POR INSTALAR")!
        return data.reversed()
    }

    func getStatusInstalados() -> [leadRecomendado] {
        let data = RealManager.getFromAll(object: leadRecomendado.self, field: "tipo", equalsTo: "INSTALADOS")!
        return data.reversed()
    }

    func getStatusContactados() -> [leadRecomendado] {
        let data = RealManager.getFromAll(object: leadRecomendado.self, field: "tipo", equalsTo: "CONTACTADO")!
        return data.reversed()
    }

    func saveCommentsLead(comment: CommentLeadResponse) {
        RealManager.deleteAll(object: Comentarios.self)
        RealManager.deleteAll(object: MotivosLead.self)
        var listaComments = [Comentarios]()
        let motivos = MotivosLead()
        motivos.motivoDescartadoFuturo = motivo
        // listaComments.append(contentsOf: listaCommentarios)
        listaComments.append(contentsOf: comment.mInfoSalida!.mComentarios)
        RealManager.insertCollection(objects: listaComments)
        RealManager.insert(object: motivos)
    }

    func saveCommentsOportunity(comment: CommentLeadResponse) {
        var listaComments = [Comentarios]()
        listaComments.append(contentsOf: comment.mInfoSalida!.mComentarios)
        RealManager.insertCollection(objects: listaComments)
    }

    func getCommentsLead() -> [Comentarios] {
        return RealManager.getAll(object: Comentarios.self)
    }

    func getMotivoComments() -> MotivosLead {
        return RealManager.findFirst(object: MotivosLead.self)!
    }

    override func onErrorLoadResponse(requestUrl: String, messageError: String) {
        viewModel?.onError(errorMessage: messageError)
    }

}
