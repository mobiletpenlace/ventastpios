//
//  CreateLeadViewModel.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 08/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation

protocol CreateLeadObserver {
    func successCreate(id: String)
    func successEstatus()
    func successCommentary()
    func successCreateCommentary()
    func onError(errorMessage: String)
}

class CreateLeadViewModel: BaseViewModel, CreateLeadModelObserver {

    var observer: CreateLeadObserver?
    var model: CreateLeadModel!

    override init(view: BaseVentasView) {
        super.init(view: view)
        model = CreateLeadModel(observer: self)
        model.atachModel(viewModel: self)
    }

    func atachView(observer: CreateLeadObserver) {
        self.observer = observer
    }

    func consultCreateLead(name: String, lastName: String, lastNameMother: String, telephone: String, email: String) {
                view.showLoading(message: "")
                let creacionRequest = creacionLeadRequest()
                creacionRequest.MInformacion = informacion()
                creacionRequest.MInformacion?.nombre = name + " " + lastName + " " + lastNameMother
                creacionRequest.MInformacion?.email = email
                creacionRequest.MInformacion?.telefono = telephone
                creacionRequest.MInformacion?.estatus = "Candidato"
                creacionRequest.MInformacion?.ventaOrigen = "vendedorAppVentas"
                creacionRequest.MInformacion?.nombreTecnico = getCurrentNumEmployee()
                creacionRequest.MInformacion?.numEmpleadoTecnico = getCurrentNumEmployee()
                if UserDefinitions.isEmbajador {
                    creacionRequest.MInformacion?.canalReferido = "Embajador"
                    creacionRequest.MInformacion?.origen = "Autoempresarios"// "Embajadores"
                    creacionRequest.MInformacion?.subCanal = "Lead Embajador"
                } else {
                    creacionRequest.MInformacion?.canalReferido = "AppPromotor"
                    creacionRequest.MInformacion?.origen = "Call Center BTL"
                    creacionRequest.MInformacion?.subCanal =  "Promotor"
                }
                model.createLead(request: creacionRequest)
            }

    func createCommentary(msj: String, idLead: String) {
        view.showLoading(message: "")
        let creacionRequest = createCommentaryRequest()
        creacionRequest.comment = msj
        creacionRequest.idLead = idLead
        model.createComment(request: creacionRequest)
    }

    func consultStatusLead( dateStart: String, dateFinish: String, user: String) {
        view.showLoading(message: "")
        let statusRequest = statusLeadRequest()
        statusRequest.numEmpleado = user
        statusRequest.tipo = "RECOMENDADO"
        statusRequest.strFechaInicio =  dateStart
        statusRequest.strFechaFin = dateFinish
        model.estatusLead(request: statusRequest)
    }

    func consultComments(tipoRegistro: String) {
        let commentRequest = CommentLeadRequest()
        commentRequest.idRegistro = tipoRegistro
        commentRequest.tipoObjeto = "Lead"
        model.commentaryLead(request: commentRequest)
    }

    func successCreateLead(idLead: String) {
        view.hideLoading()
        observer?.successCreate(id: idLead)
    }

    func successCreateComment() {
        view.hideLoading()
        observer?.successCreateCommentary()
    }

    func successEstatusLead() {
        view.hideLoading()
        observer?.successEstatus()
    }

    func successCommentaryLead() {
        view.hideLoading()
        observer?.successCommentary()
    }

    func getToday() -> String {
        return DateCalendar.convertTodayString(formatter: Formate.yyyy_MM_dd_dash)
    }

    func getDateLastThirtyDays() -> String {
        let today = DateCalendar.convertStringDate(date: getToday(), formatter: Formate.yyyy_MM_dd_dash)!
        let newDate = DateCalendar.futureDate(date: today, value: -30, component: .day)
        return DateCalendar.convertDateString(date: newDate!, formatter: Formate.yyyy_MM_dd_dash)
    }

    func getDateLastThirtyDaysDate() -> Date {
        let today = DateCalendar.convertStringDate(date: getToday(), formatter: Formate.yyyy_MM_dd_dash)!
        let newDate = DateCalendar.futureDate(date: today, value: -30, component: .day)
        return newDate!
    }

    func convertirFechaCalendar(fecha: String) -> (String, Date) {
        let date =  DateCalendar.convertStringDate(date: fecha, formatter: Formate.dd_MM_yyyy)!
        let stringDate = DateCalendar.convertDateString(date: date, formatter: Formate.yyyy_MM_dd_dash)
        return (stringDate, date)
    }

    func getInstaladas() -> [leadRecomendado] {
        let data = model.getStatusInstalados()
        return data
    }

    func getSinExito() -> [leadRecomendado] {
        let data = model.getStatusSinExito()
        return data
    }

    func getPorInstalar() -> [leadRecomendado] {
        let data = model.getStatusPorInstalar()
         return data
    }

    func getPorContactar() -> [leadRecomendado] {
        let data = model.getStatusPorContactar()
         return data
    }

    func getContactado() -> [leadRecomendado] {
        let data = model.getStatusContactados()
         return data
    }

    func getComentarios() -> [Comentarios] {
        let data = model.getCommentsLead()
        return data
    }

    func getMotivos() -> MotivosLead {
        let data = model.getMotivoComments()
        return data
    }

    func getCurrentNumEmployee() -> String {
        let employee = model.getEmploye()
        return employee.NoEmpleado
    }

    func getCurrentNameEmployee() -> String {
        let employee = model.getEmploye()
        return employee.Nombre
    }

    func onError(errorMessage: String) {
        view.hideLoading()
        observer?.onError(errorMessage: errorMessage)
    }

    func clearText(text: String) -> String {
        var result = ""
        result = text.replacingOccurrences(of: "<p>", with: "")
        result = result.replacingOccurrences(of: "</p>", with: "")
        result = result.replacingOccurrences(of: "</i>", with: "")
        result = result.replacingOccurrences(of: "<i>", with: "")
        result = result.replacingOccurrences(of: "</b>", with: "")
        result = result.replacingOccurrences(of: "<b>", with: "")
        result = result.replacingOccurrences(of: "</u>", with: "")
        result = result.replacingOccurrences(of: "<u>", with: "")
        result = result.replacingOccurrences(of: "</strike>", with: "")
        result = result.replacingOccurrences(of: "<strike>", with: "")
        result = result.replacingOccurrences(of: "</li>", with: "")
        result = result.replacingOccurrences(of: "<li>", with: "")
        result = result.replacingOccurrences(of: "</ul>", with: "")
        result = result.replacingOccurrences(of: "<ul>", with: "")
        result = result.replacingOccurrences(of: "</s>", with: "")
        result = result.replacingOccurrences(of: "<s>", with: "")
        result = result.replacingOccurrences(of: "</ol>", with: "")
        result = result.replacingOccurrences(of: "<ol>", with: "")
        result = result.replacingOccurrences(of: "</hr>", with: "")
        result = result.replacingOccurrences(of: "<hr>", with: "")
        result = result.replacingOccurrences(of: "</pre>", with: "")
        result = result.replacingOccurrences(of: "<pre>", with: "")
        result = result.replacingOccurrences(of: "</a>", with: "")
        result = result.replacingOccurrences(of: "<a>", with: "")
        result = result.replacingOccurrences(of: "</br>", with: "")
        result = result.replacingOccurrences(of: "<br>", with: "")
        return result
    }
}
