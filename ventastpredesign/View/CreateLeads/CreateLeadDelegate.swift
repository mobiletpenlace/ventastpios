////
////  CreateLeadDelegate.swift
////  ventastpredesign
////
////  Created by Marisol Huerta O. on 09/06/21.
////  Copyright © 2021 TotalPlay. All rights reserved.
////
//
//import Foundation
//
//extension CreateLeadView: CreateLeadObserver {
//    func successCreate(id: String) {
//        if mCommentaryTextfield.text != "" {
//            mCreateLeadViewModel.createCommentary(msj: mCommentaryTextfield.text ?? "", idLead: id)
//        } else {
//            clearData()
//            Constants.Alert(title: "", body: dialogs.Success.createLead, type: type.Success, viewC: self, dimissParent: true)
//        }
//    }
//
//    func successCreateCommentary() {
//        clearData()
//        Constants.Alert(title: "", body: dialogs.Success.createLead, type: type.Success, viewC: self, dimissParent: true)
//    }
//
//    func successEstatus() {
//
//    }
//
//    func successCommentary() {
//
//    }
//
//    func onError(errorMessage: String) {
//        if errorMessage == "No se insertó correctamente" {
//                    Constants.Alert(title: "", body: "Favor de validar los datos ingresados", type: type.Error, viewC: self)
//                } else {
//                    Constants.Alert(title: "", body: errorMessage, type: type.Error, viewC: self)
//                }
//    }
//
//}
