//
//  CreateLeadView.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 07/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTextControls_FilledTextFields
import Amplitude

//class CreateLeadViewUK: BaseVentasView {
//    @IBOutlet var mNameTextfield: MDCFilledTextField!
//    @IBOutlet var mLastNameTextfield: MDCFilledTextField!
//    @IBOutlet var mMotherLastNameTextfield: MDCFilledTextField!
//    @IBOutlet var mTelephoneTextfield: MDCFilledTextField!
//    @IBOutlet var mEmailTextfield: MDCFilledTextField!
//    @IBOutlet var mCommentaryTextfield: MDCFilledTextField!
//    var mCreateLeadViewModel: CreateLeadViewModel!
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        super.setView(view: self.view)
//       // clearData()
//        mCreateLeadViewModel = CreateLeadViewModel(view: self)
//        mCreateLeadViewModel.atachView(observer: self)
//        configTextFields()
//        clearData()
//    }
//
//    override func viewDidAppear(_ animated: Bool) {
//        Amplitude.instance().logEvent("Show_CreateLeadScreen")
//    }
//
//    @IBAction func createProspectoAction(_ sender: Any) {
//        if validateDataFull() {
//            mCreateLeadViewModel.consultCreateLead(name: mNameTextfield.text ?? "", lastName: mLastNameTextfield.text ?? "", lastNameMother: mMotherLastNameTextfield.text ?? "", telephone: mTelephoneTextfield.text ?? "", email: mEmailTextfield.text ?? "")
//            Amplitude.instance().logEvent("Action_SendLead_Button", withEventProperties: ["Success": "True"] )
//
//        } else {
//            Constants.Alert(title: "", body: dialogs.Error.FillFields, type: type.Error, viewC: self)
//            Amplitude.instance().logEvent("Action_SendLead_Button", withEventProperties: ["Success": "False"] )
//        }
//    }
//
//    @IBAction func backAction(_ sender: Any) {
//        Constants.Back(viewC: self)
//        Amplitude.instance().logEvent("Action_CreateLeadScreenBack_Button")
//    }
//
//    func validateDataFull() -> Bool {
//        if mNameTextfield.text != "" && mLastNameTextfield.text != "" && mMotherLastNameTextfield.text != "" && mTelephoneTextfield.text != "" &&
//            mEmailTextfield.text != "" {
//            return true
//        }
//        return false
//    }
//
//    func clearData() {
//        mNameTextfield.text = ""
//        mLastNameTextfield.text = ""
//        mMotherLastNameTextfield.text = ""
//        mTelephoneTextfield.text = ""
//        mEmailTextfield.text = ""
//        mCommentaryTextfield.text = ""
//    }
//
//    func configTextFields() {
//        mNameTextfield.setFilledTP()
//        mNameTextfield.setTextTP()
//        mLastNameTextfield.setFilledTP()
//        mLastNameTextfield.setTextTP()
//        mMotherLastNameTextfield.setFilledTP()
//        mMotherLastNameTextfield.setTextTP()
//        mTelephoneTextfield.setFilledTP()
//        mTelephoneTextfield.setTextTP()
//        mEmailTextfield.setFilledTP()
//        mEmailTextfield.setTextTP()
//        mCommentaryTextfield.setFilledTP()
//        mCommentaryTextfield.setTextTP()
//    }
//
//}
