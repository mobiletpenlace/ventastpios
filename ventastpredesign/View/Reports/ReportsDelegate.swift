//
//  ReportsDelegate.swift
//  ventastpredesign
//
//  Created by Gustavo Tellez Diaz on 02/02/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

enum CategoryReports {
    case sales
    case controlDesk
    case installationProcess
    case installed
}

extension ReportsViewController: DateFilterCalendarDelegate {
    func getDatesSelected(firstDate: String, lastDate: String) {
        dateFilterTextField.text = "\(firstDate) / \(lastDate)"
        consultReports(fromDate: firstDate, toDate: lastDate)
    }
}

extension ReportsViewController: ReportsObserver {
    func onSuccesInstalledReports() {
        showView(withReports: viewModel!.getInstalledReports())
    }

    func onSuccesSalesReports() {
        showView(withReports: viewModel!.getSalesReports())
    }

    func showView(withReports reports: Any) {
        switch currentCategoryReports {
        case .sales:
                changeButtonState(label: salesLabel, selector: salesSelector, wasSelected: true)
                ViewEmbedder.embed(withIdentifier: "SalesReportViewController", parent: self, container: typeReportContainer) { (vc) in
                    let salesVC = vc as! SalesReportViewController
                    salesVC.reports = reports as! [SaleReport]
                }

        case .controlDesk:
                changeButtonState(label: controlDeskLabel, selector: controlDeskSelector, wasSelected: true)
                ViewEmbedder.embed(withIdentifier: "ControlDeskReportViewController", parent: self, container: typeReportContainer) { (vc) in
                    let controlDeskVC = vc as! ControlDeskReportViewController
                    controlDeskVC.reports = reports as! [SaleReport]
                }

        case .installationProcess:
                changeButtonState(label: installationProcessLabel, selector: installationProcessSelector, wasSelected: true)
                ViewEmbedder.embed(withIdentifier: "InstallationProcessReportsViewController", parent: self, container: typeReportContainer) { (vc) in
                    let installationProcessdVC = vc as! InstallationProcessReportsViewController
                    installationProcessdVC.reports = reports as! [InstalledReport]
                    print(installationProcessdVC.sinDatos)
                }

        case .installed:
                changeButtonState(label: installedLabel, selector: installedSelector, wasSelected: true)
                ViewEmbedder.embed(withIdentifier: "InstalledReportsViewController", parent: self, container: typeReportContainer) { (vc) in
                    let installedVC = vc as! InstalledReportsViewController
                    installedVC.reports = reports as! [InstalledReport]
                    print(installedVC.sinDatos)
                }
        }
    }

    func deselectButtons() {
        configFilterView()
        changeButtonState(label: controlDeskLabel, selector: controlDeskSelector, wasSelected: false)
        changeButtonState(label: salesLabel, selector: salesSelector, wasSelected: false)
        changeButtonState(label: installationProcessLabel, selector: installationProcessSelector, wasSelected: false)
        changeButtonState(label: installedLabel, selector: installedSelector, wasSelected: false)
    }

    func changeButtonState(label: UILabel, selector: UIView, wasSelected: Bool) {
        hideKeyboard()
        if wasSelected {
            selector.isHidden = false
            label.textColor = UIColor(named: "Base_blue_text")
            label.font = controlDeskLabel.font.withSize(22.0)
        } else {
            selector.isHidden = true
            label.textColor = UIColor(named: "Base_ligth_gray_text")
            label.font = controlDeskLabel.font.withSize(18.0)
        }
    }

    func disableSearch() {
        buttonSearch.isEnabled = false
    }

}
