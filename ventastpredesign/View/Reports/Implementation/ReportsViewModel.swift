//
//  ControlDeskReportsViewModel.swift
//  ventastpredesign
//
//  Created by Gustavo Tellez Diaz on 25/01/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

protocol ReportsObserver {
    func onSuccesSalesReports()
    func onSuccesInstalledReports()
    func disableSearch()
}

class ReportsViewModel: BaseViewModel, ReportsModelObserver {

    var observer: ReportsObserver?
    var model: ReportsModel!

    var installedReports: [InstalledReport] = []
    var salesReports: [SaleReport] = []

    override init(view: BaseVentasView) {
        super.init(view: view)
        model = ReportsModel(observer: self)
        model.atachModel(viewModel: self)
    }

    func atachView(observer: ReportsObserver) {
        self.observer = observer
    }

    func consultSalesReports(fromDate dateStart: String, toDate dateEnd: String) {
        print("El numero de empleado es: \(model.getEmployeeNumber())")
        let noEmployee = "65018744" // model.getEmployeeNumber()
        let url = "\(ApiDefinition.API_REPORTES_VENTAS)\(noEmployee)/\(dateStart)/\(dateEnd)"
        model.getSaleReports(withUrl: url)
    }

    func consultInstalledReports(fromDate dateStart: String, toDate dateEnd: String) {
        print("El numero de empleado es: \(model.getEmployeeNumber())")
        let noEmployee = "65018744" // model.getEmployeeNumber()
        let url = "\(ApiDefinition.API_REPORTES_INSTALADAS)\(noEmployee)/\(dateStart)/\(dateEnd)"
        model.getInstalledReports(withUrl: url)
    }

    func onSuccesSalesReports(_ salesReports: SaleReportsResponse) {
        view.hideLoading()
        self.salesReports = salesReports.colaboradores
        observer?.onSuccesSalesReports()
    }

    func onSuccesInstalledReports(_ installedReports: InstalledReportsResponse) {
        view.hideLoading()
        self.installedReports = installedReports.colaboradores
        observer?.onSuccesInstalledReports()
    }

    func onError(errorMessage: String) {
        view.hideLoading()
        Logger.println(errorMessage)
    }

    func getSalesReports() -> [SaleReport] {
        return salesReports
    }

    func getInstalledReports() -> [InstalledReport] {
        return installedReports
    }
}
