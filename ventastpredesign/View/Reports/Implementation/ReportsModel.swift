//
//  ControlDeskReportsModel.swift
//  ventastpredesign
//
//  Created by Gustavo Tellez Diaz on 25/01/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift
import SwiftEventBus

protocol ReportsModelObserver {
    func onSuccesSalesReports(_ salesReports: SaleReportsResponse)
    func onSuccesInstalledReports(_ installedReports: InstalledReportsResponse)
    func onError(errorMessage: String)
}

class ReportsModel: BaseModel {

    var viewModel: ReportsModelObserver?
    var server: ServerDataManager2<SaleReportsResponse>?
    var saleReportsUrl = ""
    var server2: ServerDataManager2<InstalledReportsResponse>?
    var installedReportsUrl = ""

    init(observer: BaseViewModelObserver) {
        super.init(observerVM: observer)
        server = ServerDataManager2<SaleReportsResponse>()
        server2 = ServerDataManager2<InstalledReportsResponse>()
    }

    func atachModel(viewModel: ReportsModelObserver ) {
        self.viewModel = viewModel
    }

    func getSaleReports(withUrl url: String) {
        saleReportsUrl = url
        server?.setValues(requestUrl: url, delegate: self, headerType: .headersReportes)
        server?.requestStrappi()
    }

    func getInstalledReports(withUrl url: String) {
        installedReportsUrl = url
        server2?.setValues(requestUrl: url, delegate: self, headerType: .headersReportes)
        server2?.requestStrappi()
    }

    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if requestUrl == saleReportsUrl {
            let saleReportsResponse = response as! SaleReportsResponse
            viewModel?.onSuccesSalesReports(saleReportsResponse)
        } else if requestUrl == installedReportsUrl {
            let installedReportsResponse = response as! InstalledReportsResponse
            viewModel?.onSuccesInstalledReports(installedReportsResponse)
        }
    }

    func getEmployeeNumber() -> String {
        var noEmployee = ""
        if RealManager.findFirst(object: Empleado.self) != nil {
            let employee = RealManager.findFirst(object: Empleado.self)!
            noEmployee = employee.NoEmpleado
        }
        return noEmployee
    }
}
