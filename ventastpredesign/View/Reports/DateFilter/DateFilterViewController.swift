//
//  ReportingDateViewController.swift
//  ventastpredesign
//
//  Created by Gustavo Tellez Diaz on 05/01/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

protocol DateFilterDelegate {
    func getDateSelected(_ dateSelected: String)
}

class DateFilterViewController: UIViewController {

    @IBOutlet weak var picker: UIPickerView!

    var previousDates = [String]()
    var dateFilterDelegate: DateFilterDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        picker.delegate = self
        picker.dataSource = self
        getPreviousDates()
    }

    @IBAction func cancelAction(_ sender: Any) {
        Constants.Back(viewC: self)
    }

    @IBAction func confirmButtonPressed(_ sender: Any) {
        let dateSelected = previousDates[picker.selectedRow(inComponent: 0)]
        dateFilterDelegate?.getDateSelected(dateSelected)
        Constants.Back(viewC: self)
    }

    func getPreviousDates() {
        for month in 1...12 {
            previousDates.append(createDateWithDifferenceOf(months: -month))
        }
    }

    func createDateWithDifferenceOf(months: Int) -> String {
        var dateComponents = DateComponents()
        dateComponents.setValue(months, for: .month)

        let currentDate = Date()
        let previousDate = Calendar.current.date(byAdding: dateComponents, to: currentDate)

        return DateCalendar.convertDateString(date: previousDate!, formatter: Formate.dd_MM_yyyy_dash)
    }
}

extension DateFilterViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return previousDates.count
    }

    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40.0
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {

        let date = DateCalendar.convertStringDate(date: previousDates[row], formatter: Formate.dd_MM_yyyy)
        let formattedDate = DateCalendar.convertDateString(date: date!, formatter: Formate.MMMM_yyyy)

        return formattedDate.capitalizingFirstLetter()
    }
}
