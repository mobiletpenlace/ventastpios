//
//  DateFilterCalendarVC.swift
//  ventastpredesign
//
//  Created by Gustavo Tellez Diaz on 02/02/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import FSCalendar

protocol DateFilterCalendarDelegate {
    func getDatesSelected(firstDate: String, lastDate: String)
}

class DateFilterCalendarVC: UIViewController {

    @IBOutlet weak var calendarView: FSCalendar!
    @IBOutlet weak var actionButton: UIButton!

    var dateFilterCalendarDelegate: DateFilterCalendarDelegate?
    var currentFirstDate: Date?
    var currentLastDate: Date?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        calendarView.locale = Locale(identifier: "Es")
        calendarView.appearance.caseOptions = [.headerUsesUpperCase]
    }

    @IBAction func NextAction(_ sender: Any) {
        if let firstDate = currentFirstDate, let lastDate = currentLastDate {
            let startDate = DateCalendar.convertDateString(date: firstDate, formatter: Formate.dd_MM_yyyy_dash)
            let endDate = DateCalendar.convertDateString(date: lastDate, formatter: Formate.dd_MM_yyyy_dash)
            dateFilterCalendarDelegate?.getDatesSelected(firstDate: startDate, lastDate: endDate)
            Constants.Back(viewC: self)
        } else {
            Constants.Back(viewC: self)
        }
    }
}

extension DateFilterCalendarVC: FSCalendarDelegate, FSCalendarDataSource, FSCalendarDelegateAppearance {

    func minimumDate(for calendar: FSCalendar) -> Date {
        let pastDate = DateCalendar.pastDate(date: Date(), value: 6, component: .month)
        return pastDate!
    }

    func maximumDate(for calendar: FSCalendar) -> Date {
        return Date()
    }

    func calendar(_ calendar: FSCalendar, didDeselect date: Date, at monthPosition: FSCalendarMonthPosition) {
        if currentFirstDate == date {
            deselectDates(exceptDate: date)
            currentLastDate = date
            calendarView.select(date)
            calendarView.appearance.selectionColor = UIColor(named: "Base_low_purple")
            actionButton.setTitle("Aceptar", for: .normal)
        } else {
            deselectDates(exceptDate: date)
        }
    }

    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        return checkDateIsInAllowedRange(date)
    }

    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        if currentFirstDate != nil {
            if currentLastDate != nil {
                deselectDates(exceptDate: date)
            } else {
                currentLastDate = date
                selectDates()
            }
        } else {
            currentFirstDate = date
        }
    }

    func deselectDates(exceptDate date: Date) {
        for dateSelected in calendarView.selectedDates {
            calendarView.deselect(dateSelected)
        }
        calendarView.select(date)
        currentFirstDate = date
        currentLastDate = nil
        actionButton.setTitle("Omitir", for: .normal)
        calendarView.appearance.selectionColor = UIColor(named: "basePurple")
    }

    func selectDates() {
        organizeDates()
        var dateToSelect = DateCalendar.futureDate(date: currentFirstDate!, value: 1, component: .day)
        while dateToSelect != currentLastDate {
            calendarView.select(dateToSelect)
            dateToSelect = DateCalendar.futureDate(date: dateToSelect!, value: 1, component: .day)
        }
        actionButton.setTitle("Aceptar", for: .normal)
        calendarView.appearance.selectionColor = UIColor(named: "basePurple")
    }

    func organizeDates() {
        let firstDate = DateCalendar.convertTimeInterval(date: currentFirstDate!)
        let lastDate = DateCalendar.convertTimeInterval(date: currentLastDate!)

        if firstDate > lastDate {
            let aux = currentLastDate
            currentLastDate = currentFirstDate
            currentFirstDate = aux
        }
    }

    func checkDateIsInAllowedRange(_ date: Date?) -> Bool {
        if currentFirstDate != nil {
            if currentLastDate != nil {
                return true
            } else {
                let minDate = DateCalendar.pastDate(date: currentFirstDate!, value: 1, component: .month)
                let maxDate = DateCalendar.futureDate(date: currentFirstDate!, value: 1, component: .month)

                if minDate! <= date! && date! <= maxDate! {
                    return true
                } else {
                    return false
                }
            }
        } else {
            return true
        }
    }
}
