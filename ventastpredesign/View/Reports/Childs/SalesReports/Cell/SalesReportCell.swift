//
//  SalesReportCell.swift
//  ventastpredesign
//
//  Created by Gustavo Tellez Diaz on 04/01/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

class SalesReportCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var reportDetailsButton: UIButton!
    @IBOutlet weak var numberOfSales: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
