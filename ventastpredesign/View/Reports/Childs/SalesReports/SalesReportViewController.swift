//
//  SalesReportViewController.swift
//  ventastpredesign
//
//  Created by Gustavo Tellez Diaz on 30/12/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class SalesReportViewController: BaseVentasView {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var withoutDataLabel: UILabel!

    var reports: [SaleReport] = []
    var searchedReports: [SaleReport] = []
    var reporte1 = SaleReport()
    var reporte2 = SaleReport()
    var reporte3 = SaleReport()
    var reporte4 = SaleReport()
    var reporte5 = SaleReport()
    var reporte6 = SaleReport()
    var reporte7 = SaleReport()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        SwiftEventBus.onMainThread(self, name: "sales") { [weak self] result in
            if let busqueda = result?.object as? String {
                self?.reloadTable(index: busqueda)
            }

        }
    }

    override func viewDidAppear(_ animated: Bool) {
        addReports()

        if reports.count == 0 {
            withoutDataLabel.isHidden = false
            faseReportes.emptyReports = true
            print(faseReportes.emptyReports)
        } else {
            faseReportes.emptyReports = false
            withoutDataLabel.isHidden = true
            print(faseReportes.emptyReports)
        }
        tableView.reloadData()
    }

    func addReports() {
        reporte1.numEmpleado = "3123123-123"
        reporte1.nombreEmpleado = "Arturo Ceron Epigmenio"
        reporte1.sumaVentas = 3
        reporte1.sumaAprobadas = 4
        reporte1.sumaPendientes = 6
        reporte1.sumaRechazadas = 51

        reporte2.numEmpleado = "4123123-123"
        reporte2.nombreEmpleado = "Juan Reynaldo Escobar"
        reporte2.sumaVentas = 13
        reporte2.sumaAprobadas = 4
        reporte2.sumaPendientes = 123
        reporte2.sumaRechazadas = 5

        reporte3.numEmpleado = "123123-412"
        reporte3.nombreEmpleado = "Armando Isais Olguin Cabrera"
        reporte3.sumaVentas = 1
        reporte3.sumaAprobadas = 44
        reporte3.sumaPendientes = 3
        reporte3.sumaRechazadas = 44

        reporte4.numEmpleado = "141232-123"
        reporte4.nombreEmpleado = "Juan Pérez"
        reporte4.sumaVentas = 5
        reporte4.sumaAprobadas = 8
        reporte4.sumaPendientes = 5
        reporte4.sumaRechazadas = 7

        reporte5.numEmpleado = "141232-123"
        reporte5.nombreEmpleado = "José José"
        reporte5.sumaVentas = 5
        reporte5.sumaAprobadas = 8
        reporte5.sumaPendientes = 5
        reporte5.sumaRechazadas = 7

        reporte6.numEmpleado = "141232-123"
        reporte6.nombreEmpleado = "Saúl Olguín"
        reporte6.sumaVentas = 5
        reporte6.sumaAprobadas = 8
        reporte6.sumaPendientes = 5
        reporte6.sumaRechazadas = 7

        reporte7.numEmpleado = "141232-123"
        reporte7.nombreEmpleado = "José Alberto Rodríguez"
        reporte7.sumaVentas = 5
        reporte7.sumaAprobadas = 8
        reporte7.sumaPendientes = 5
        reporte7.sumaRechazadas = 7

        reports.append(reporte1)
        reports.append(reporte2)
        reports.append(reporte3)
        reports.append(reporte4)
        reports.append(reporte5)
        reports.append(reporte6)
        reports.append(reporte7)
    }

}

extension SalesReportViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if faseReportes.searchButton {
            return searchedReports.count
        } else {
            return reports.count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SalesReportCell") as! SalesReportCell

        if faseReportes.searchButton {
            cell.nameLabel.text = searchedReports[indexPath.row].nombreEmpleado
            cell.numberOfSales.text = String(searchedReports[indexPath.row].sumaVentas ?? 0)

        } else {
            cell.nameLabel.text = reports[indexPath.row].nombreEmpleado
            cell.numberOfSales.text = String(reports[indexPath.row].sumaVentas ?? 0)

        }

        return cell
    }
    func reloadTable(index: String) {
        // print(index)

        if index == "" {
            searchedReports = reports
        } else {
            searchedReports = reports.filter { $0.nombreEmpleado.lowercased().folding(options: .diacriticInsensitive, locale: Locale.current).contains(index.lowercased().folding(options: .diacriticInsensitive, locale: Locale.current))}
        }

        if faseReportes.searchButton == true {
            if searchedReports.isEmpty {
                withoutDataLabel.isHidden = false
            } else {
                withoutDataLabel.isHidden = true
            }

        } else {
            withoutDataLabel.isHidden = true
        }

        // dump(searchedReports)
        tableView.reloadData()

    }
}
