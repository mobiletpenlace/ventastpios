//
//  InstalledReportsCell.swift
//  ventastpredesign
//
//  Created by Gustavo Tellez Diaz on 14/01/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

class InstalledReportsCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var numberOfSalesLabel: UILabel!
    @IBOutlet weak var reportDetailsButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
