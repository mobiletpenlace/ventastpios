//
//  InstalledReportsViewController.swift
//  ventastpredesign
//
//  Created by Gustavo Tellez Diaz on 14/01/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus
class InstalledReportsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var withoutDataLabel: UILabel!
    @IBOutlet weak var withoutDataReportsLabel: UILabel!

    var reports: [InstalledReport] = []
    var searchedReports: [InstalledReport] = []
    var reporte1 = InstalledReport()
    var reporte2 = InstalledReport()
    var reporte3 = InstalledReport()
    var reporte4 = InstalledReport()
    var reporte5 = InstalledReport()
    var reporte6 = InstalledReport()
    var reporte7 = InstalledReport()
    var sinDatos = Bool()

    override func viewDidLoad() {
        // prueba()
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        SwiftEventBus.onMainThread(self, name: "installedReports") { [weak self] result in
            if let busqueda = result?.object as? String {
                self?.reloadTable(index: busqueda)
            }

        }
    }

    override func viewDidAppear(_ animated: Bool) {
        addReports()

        if reports.count == 0 {
            withoutDataReportsLabel.isHidden = false
            sinDatos = true
        } else {
            withoutDataReportsLabel.isHidden = true
            sinDatos = false
        }
        tableView.reloadData()
    }

    func addReports() {
        reporte1.numEmpleado = "3123123-123"
        reporte1.nombreEmpleado = "Arturo Ceron Epigmenio"

        reporte2.numEmpleado = "4123123-123"
        reporte2.nombreEmpleado = "Juan Reynaldo Escobar"

        reporte3.numEmpleado = "123123-412"
        reporte3.nombreEmpleado = "Armando Isais Olguin Cabrera"

        reporte4.numEmpleado = "141232-123"
        reporte4.nombreEmpleado = "Juan Pérez"

        reporte5.numEmpleado = "141232-123"
        reporte5.nombreEmpleado = "José José"

        reporte6.numEmpleado = "141232-123"
        reporte6.nombreEmpleado = "Saúl Olguín"

        reporte7.numEmpleado = "141232-123"
        reporte7.nombreEmpleado = "José Alberto Rodríguez"

        reports.append(reporte1)
        reports.append(reporte2)
        reports.append(reporte3)
        reports.append(reporte4)
        reports.append(reporte5)
        reports.append(reporte6)
        reports.append(reporte7)
    }

}

extension InstalledReportsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if faseReportes.searchButton {
            return searchedReports.count
        } else {
            return reports.count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InstalledReportsCell") as! InstalledReportsCell
        let report = reports[indexPath.row]

        if faseReportes.searchButton {
            let reportF = searchedReports[indexPath.row]
            cell.nameLabel.text = reportF.nombreEmpleado
            let totalInstalled = reportF.completado.count + reportF.confirmado.count + reportF.agendado.count + reportF.rescate.count + reportF.cancelado.count
            cell.numberOfSalesLabel.text = String(totalInstalled)

        } else {
            cell.nameLabel.text = report.nombreEmpleado
            let totalInstalled = report.completado.count + report.confirmado.count + report.agendado.count + report.rescate.count + report.cancelado.count
            cell.numberOfSalesLabel.text = String(totalInstalled)

        }

        return cell
    }

    func reloadTable(index: String) {
        print(index)
        if index == "" {
            searchedReports = reports
        } else {
            searchedReports = reports.filter { $0.nombreEmpleado.lowercased().folding(options: .diacriticInsensitive, locale: Locale.current).contains(index.lowercased().folding(options: .diacriticInsensitive, locale: Locale.current))}
        }

        if faseReportes.searchButton == true {
            if searchedReports.isEmpty {
                withoutDataLabel.isHidden = false
            } else {
                withoutDataLabel.isHidden = true
            }
        } else {
            withoutDataLabel.isHidden = true
        }

//        po(searchedReports)
        tableView.reloadData()

    }

}
