//
//  InstallationProcessReportsViewController.swift
//  ventastpredesign
//
//  Created by Gustavo Tellez Diaz on 14/01/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class InstallationProcessReportsViewController: BaseVentasView {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var withoutDataLabel: UILabel!

    var reports: [InstalledReport] = []
    var searchedReports: [InstalledReport] = []
    var reporte1 = InstalledReport()
    var reporte2 = InstalledReport()
    var reporte3 = InstalledReport()
    var reporte4 = InstalledReport()
    var reporte5 = InstalledReport()
    var reporte6 = InstalledReport()
    var reporte7 = InstalledReport()
    var sinDatos = Bool()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        SwiftEventBus.onMainThread(self, name: "intallationProcessReports") { [weak self] result in
            if let busqueda = result?.object as? String {
                self?.reloadTable(index: busqueda)
            }
        }
    }

    override func viewDidAppear(_ animated: Bool) {

        addReports()
        if reports.count == 0 {
            withoutDataLabel.isHidden = false
            sinDatos = true
        } else {
            withoutDataLabel.isHidden = true
            sinDatos = false
        }
        tableView.reloadData()

    }

    func addReports() {
        reporte1.numEmpleado = "3123123-123"
        reporte1.nombreEmpleado = "Arturo Ceron Epigmenio"
        reporte2.numEmpleado = "4123123-123"
        reporte2.nombreEmpleado = "Juan Reynaldo Escobar"
        reporte3.numEmpleado = "123123-412"
        reporte3.nombreEmpleado = "Armando Isais Olguin Cabrera"
        reporte4.numEmpleado = "141232-123"
        reporte4.nombreEmpleado = "Juan Pérez"
        reporte5.numEmpleado = "141232-123"
        reporte5.nombreEmpleado = "José José"
        reporte6.numEmpleado = "141232-123"
        reporte6.nombreEmpleado = "Saúl Olguín"
        reporte7.numEmpleado = "141232-123"
        reporte7.nombreEmpleado = "José Alberto Rodríguez"
        reports.append(reporte1)
        reports.append(reporte2)
        reports.append(reporte3)
        reports.append(reporte4)
        reports.append(reporte5)
        reports.append(reporte6)
        reports.append(reporte7)
    }

}

extension InstallationProcessReportsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if faseReportes.searchButton {
            return searchedReports.count
        } else {
            return reports.count
        }

    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "InstallationReportsCell") as! InstallationReportsCell
        let installationReport = reports[indexPath.row]

        if faseReportes.searchButton {
            let installationReportSearched = searchedReports[indexPath.row]
            cell.nameSellerLabel.text = installationReportSearched.nombreEmpleado
            cell.numberOfScheduledLabel.text = String(installationReportSearched.sumaAgendado ?? 0)
            cell.numberOfRescueLabel.text = String(installationReportSearched.sumaRescate ?? 0)
            cell.numberOfConfirmedLabel.text = String(installationReportSearched.sumaConfirmado ?? 0)
            cell.numberOfCompletedLabel.text = String(installationReportSearched.sumaCompletado ?? 0)
            cell.numberOfCancelledLabel.text = String(installationReportSearched.sumaCancelado ?? 0)
        } else {
            cell.nameSellerLabel.text = installationReport.nombreEmpleado
            cell.numberOfScheduledLabel.text = String(installationReport.sumaAgendado ?? 0)
            cell.numberOfRescueLabel.text = String(installationReport.sumaRescate ?? 0)
            cell.numberOfConfirmedLabel.text = String(installationReport.sumaConfirmado ?? 0)
            cell.numberOfCompletedLabel.text = String(installationReport.sumaCompletado ?? 0)
            cell.numberOfCancelledLabel.text = String(installationReport.sumaCancelado ?? 0)
        }
        cell.detailsReportButton.tag = indexPath.row
        cell.detailsReportButton.addTarget(self, action: #selector(handleButtonTapped(sender:)), for: .touchUpInside)
        return cell
    }

    @objc func handleButtonTapped(sender: UIButton) {
        goToInstallationProcessReportDetails(withReports: reports[sender.tag])
    }

    func goToInstallationProcessReportDetails(withReports report: InstalledReport) {
        let storyboard = UIStoryboard.init(name: "InstallationProcessReportDetailsViewController", bundle: nil)
        let installationProcessReportDetails = storyboard.instantiateViewController(withIdentifier: "InstallationProcessReportDetailsViewController") as! InstallationProcessReportDetailsViewController
        installationProcessReportDetails.report = report
        present(installationProcessReportDetails, animated: true, completion: nil)
    }

    func reloadTable(index: String) {
        print(index)
        if index == "" {
            searchedReports = reports
        } else {
            searchedReports = reports.filter { $0.nombreEmpleado.lowercased().folding(options: .diacriticInsensitive, locale: Locale.current).contains(index.lowercased().folding(options: .diacriticInsensitive, locale: Locale.current))}
        }
        if faseReportes.searchButton == true {
            if searchedReports.isEmpty {
                withoutDataLabel.isHidden = false
            } else {
                withoutDataLabel.isHidden = true
            }
        } else {
            withoutDataLabel.isHidden = true
        }
        tableView.reloadData()
    }

}
