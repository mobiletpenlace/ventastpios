//
//  InstallationProcessReportDetailsViewController.swift
//  ventastpredesign
//
//  Created by Gustavo Tellez Diaz on 18/01/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

class InstallationProcessReportDetailsViewController: UIViewController {

    @IBOutlet weak var sellerNameLabel: UILabel!

    @IBOutlet weak var installationScheduleLabel: UILabel!
    @IBOutlet weak var installationScheduleView: UIView!
    @IBOutlet weak var scheduleTableView: UITableView!
    @IBOutlet weak var heigthScheduleTableView: NSLayoutConstraint!

    @IBOutlet weak var installationConfirmedLabel: UILabel!
    @IBOutlet weak var installationConfirmedView: UIView!
    @IBOutlet weak var confirmedTableView: UITableView!
    @IBOutlet weak var heigthConfirmedTableView: NSLayoutConstraint!

    @IBOutlet weak var installationCompletedLabel: UILabel!
    @IBOutlet weak var installationCompletedView: UIView!
    @IBOutlet weak var completedTableView: UITableView!
    @IBOutlet weak var heigthCompletedTableView: NSLayoutConstraint!

    @IBOutlet weak var installationRescueLabel: UILabel!
    @IBOutlet weak var installationRescueView: UIView!
    @IBOutlet weak var rescueTableView: UITableView!
    @IBOutlet weak var heigthRescueTableView: NSLayoutConstraint!

    @IBOutlet weak var installationCancelledLabel: UILabel!
    @IBOutlet weak var installationCancelledView: UIView!
    @IBOutlet weak var cancelledTableView: UITableView!
    @IBOutlet weak var heigthCancelledTableView: NSLayoutConstraint!

    var report: InstalledReport!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        sellerNameLabel.text = report.nombreEmpleado
        configContainers()
    }

    @IBAction func backAction(_ sender: Any) {
        Constants.Back(viewC: self)
    }

    func configContainers() {
        configReportView(totalReports: report.sumaAgendado ?? 0, totalReportlabel: installationScheduleLabel, containerView: installationScheduleView, tableView: scheduleTableView, heigthTableView: heigthScheduleTableView)
        configReportView(totalReports: report.sumaConfirmado ?? 0, totalReportlabel: installationConfirmedLabel, containerView: installationConfirmedView, tableView: confirmedTableView, heigthTableView: heigthConfirmedTableView)
        configReportView(totalReports: report.sumaCompletado ?? 0, totalReportlabel: installationCompletedLabel, containerView: installationCompletedView, tableView: completedTableView, heigthTableView: heigthCompletedTableView)
        configReportView(totalReports: report.sumaRescate ?? 0, totalReportlabel: installationRescueLabel, containerView: installationRescueView, tableView: rescueTableView, heigthTableView: heigthRescueTableView)
        configReportView(totalReports: report.sumaCancelado ?? 0, totalReportlabel: installationCancelledLabel, containerView: installationCancelledView, tableView: cancelledTableView, heigthTableView: heigthCancelledTableView, hasAComment: true)
    }

    func configReportView(totalReports: Int, totalReportlabel: UILabel, containerView: UIView, tableView: UITableView, heigthTableView: NSLayoutConstraint, hasAComment: Bool = false) {

        totalReportlabel.text = String(totalReports)
        var heigthCell: CGFloat = 0.0
        if hasAComment {
            heigthCell = 70.0
            tableView.register(UINib(nibName: "CommentedReportDetailsCell", bundle: nil), forCellReuseIdentifier: "CommentedReportDetailsCell")
        } else {
            heigthCell = 30.0
            tableView.register(UINib(nibName: "ReportDetailsCell", bundle: nil), forCellReuseIdentifier: "ReportDetailsCell")
        }

        if totalReports != 0 {
            heigthTableView.constant = CGFloat(totalReports) * heigthCell
        } else {
            heigthTableView.constant = 0.0
            containerView.isHidden = true
        }
    }
}

extension InstallationProcessReportDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView.tag {
        case 0:
            return report.sumaAgendado ?? 0
        case 1:
            return report.sumaConfirmado ?? 0
        case 2:
            return report.sumaCompletado ?? 0
        case 3:
            return report.sumaRescate ?? 0
        case 4:
            return report.sumaCancelado ?? 0
        default:
            return 0
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView.tag == 4 {
            return 70.0
        } else {
            return 30.0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView.tag {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReportDetailsCell") as! ReportDetailsCell
            cell.descriptionLabel.text = "Número de contrato"
            cell.indentifierLabel.text = report.agendado[indexPath.row].idVenta
            return cell

        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReportDetailsCell") as! ReportDetailsCell
            cell.descriptionLabel.text = "Número de Solicitud"
            cell.indentifierLabel.text = report.confirmado[indexPath.row].idVenta

            return cell

        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReportDetailsCell") as! ReportDetailsCell
            cell.descriptionLabel.text = "Número de Solicitud"
            cell.indentifierLabel.text = report.completado[indexPath.row].idVenta
            return cell

        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReportDetailsCell") as! ReportDetailsCell
            cell.descriptionLabel.text = "Número de Solicitud"
            cell.indentifierLabel.text = report.rescate[indexPath.row].idVenta
            return cell

        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CommentedReportDetailsCell") as! CommentedReportDetailsCell
            let cancelledReport = report.cancelado[indexPath.row]

            cell.identifierLabel.text = cancelledReport.idVenta
            cell.totalComments = cancelledReport.comentario.count
            if cancelledReport.tieneComentario {
                cell.commentsButton.tag = indexPath.row
                cell.commentsButton.addTarget(self, action: #selector(handleButtonPressed(sender:)), for: .touchUpInside)
            }

            return cell

        default:
            return UITableViewCell()
        }
    }

    @objc func handleButtonPressed(sender: UIButton) {
        let cancelledReport = report.cancelado[sender.tag]
        showCommentAlert(requestNumber: String(cancelledReport.idVenta), comments: cancelledReport.comentario)
    }

    func showCommentAlert(requestNumber: String, comments: [String]) {
        let storyboard: UIStoryboard = UIStoryboard(name: "CommentAlertViewController", bundle: nil)
        let commentAlert = storyboard.instantiateViewController(withIdentifier: "CommentAlertViewController")as! CommentAlertViewController

        commentAlert.identifierNumber = requestNumber
        commentAlert.comments = comments
        commentAlert.providesPresentationContextTransitionStyle = true
        commentAlert.definesPresentationContext = true
        commentAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        commentAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve

        self.present(commentAlert, animated: false, completion: {})
    }
}
