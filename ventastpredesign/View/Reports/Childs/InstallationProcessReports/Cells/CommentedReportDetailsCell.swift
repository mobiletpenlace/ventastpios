//
//  CommentedReportDetailsCell.swift
//  ventastpredesign
//
//  Created by Gustavo Tellez Diaz on 20/01/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

class CommentedReportDetailsCell: UITableViewCell {

    @IBOutlet weak var identifierLabel: UILabel!
    @IBOutlet weak var commentsLabel: UILabel!
    @IBOutlet weak var commentsButton: UIButton!

    var totalComments: Int = 0

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configCommentsLabel()
    }

    func configCommentsLabel() {
        if totalComments == 1 {
            commentsLabel.text = "1 Cometario"
        } else {
            commentsLabel.text = "\(totalComments) Comentarios"
        }
    }
}
