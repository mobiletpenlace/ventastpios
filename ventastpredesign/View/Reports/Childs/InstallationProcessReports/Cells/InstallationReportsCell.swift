//
//  InstallationReportsCell.swift
//  ventastpredesign
//
//  Created by Gustavo Tellez Diaz on 18/01/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

class InstallationReportsCell: UITableViewCell {

    @IBOutlet weak var nameSellerLabel: UILabel!
    @IBOutlet weak var numberOfScheduledLabel: UILabel!
    @IBOutlet weak var numberOfConfirmedLabel: UILabel!
    @IBOutlet weak var numberOfCompletedLabel: UILabel!
    @IBOutlet weak var numberOfRescueLabel: UILabel!
    @IBOutlet weak var numberOfCancelledLabel: UILabel!
    @IBOutlet weak var detailsReportButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
