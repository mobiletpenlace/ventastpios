//
//  ReportDetailsCell.swift
//  ventastpredesign
//
//  Created by Gustavo Tellez Diaz on 20/01/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

class ReportDetailsCell: UITableViewCell {

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var indentifierLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
