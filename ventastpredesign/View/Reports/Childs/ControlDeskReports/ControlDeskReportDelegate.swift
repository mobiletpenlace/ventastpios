//
//  ControlDeskReportDelegate.swift
//  ventastpredesign
//
//  Created by Gustavo Tellez Diaz on 25/01/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

extension ControlDeskReportViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if faseReportes.searchButton {
            return searchedReports.count
        } else {
            return reports.count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ControlDeskReportCell") as! ControlDeskReportCell
        let report = reports[indexPath.row]

        if faseReportes.searchButton {
            let reportF = searchedReports[indexPath.row]
            cell.nameLabel.text = reportF.nombreEmpleado
            cell.numberOfApproved.text = String(reportF.sumaAprobadas ?? 0)
            cell.numberOfSlopes.text = String(reportF.sumaPendientes ?? 0)
            cell.numberOfRejected.text = String(reportF.sumaRechazadas ?? 0)
        } else {
            cell.nameLabel.text = report.nombreEmpleado
            cell.numberOfApproved.text = String(report.sumaAprobadas ?? 0)
            cell.numberOfSlopes.text = String(report.sumaPendientes ?? 0)
            cell.numberOfRejected.text = String(report.sumaRechazadas ?? 0)
        }
        cell.reportDetailsButton.tag = indexPath.row
        cell.reportDetailsButton.addTarget(self, action: #selector(handleButton(sender:)), for: .touchUpInside)
        return cell
    }

    @objc func handleButton(sender: UIButton) {
        let report = reports[sender.tag]
        goToReportDetails(report)
    }

    func goToReportDetails(_ report: SaleReport) {
        let storyboard = UIStoryboard(name: "ControlDeskReportDetailsViewController", bundle: nil)
        let reportDetailsVC = storyboard.instantiateViewController(withIdentifier: "ControlDeskReportDetailsViewController") as! ControlDeskReportDetailsViewController
        reportDetailsVC.reportDetails = report
        present(reportDetailsVC, animated: true, completion: nil)
    }

    func reloadTable(index: String) {
        if index == "" {
            searchedReports = reports
        } else {
            searchedReports = reports.filter { $0.nombreEmpleado.lowercased().folding(options: .diacriticInsensitive, locale: Locale.current).contains(index.lowercased().folding(options: .diacriticInsensitive, locale: Locale.current))}
        }
        if faseReportes.searchButton == true {
            if searchedReports.isEmpty {
                withoutDataLabel.isHidden = false
            } else {
                withoutDataLabel.isHidden = true
            }
        } else {
            withoutDataLabel.isHidden = true
        }
        tableView.reloadData()
    }
}
