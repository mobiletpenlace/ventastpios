//
//  ControlDeskReportCell.swift
//  ventastpredesign
//
//  Created by Gustavo Tellez Diaz on 30/12/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

class ControlDeskReportCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var numberOfApproved: UILabel!
    @IBOutlet weak var numberOfSlopes: UILabel!
    @IBOutlet weak var numberOfRejected: UILabel!
    @IBOutlet weak var reportDetailsButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
