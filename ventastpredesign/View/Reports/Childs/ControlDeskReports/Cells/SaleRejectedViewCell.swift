//
//  SaleRejectedViewCell.swift
//  ventastpredesign
//
//  Created by Gustavo Tellez Diaz on 07/01/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

class SaleRejectedViewCell: UITableViewCell {

    @IBOutlet weak var contractNumber: UILabel!
    @IBOutlet weak var numberOfCommentaries: UILabel!
    @IBOutlet weak var commentaryButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
