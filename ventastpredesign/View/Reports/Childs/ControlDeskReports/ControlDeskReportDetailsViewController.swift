//
//  ReportDetailsViewController.swift
//  ventastpredesign
//
//  Created by Gustavo Tellez Diaz on 06/01/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

class ControlDeskReportDetailsViewController: UIViewController {

    @IBOutlet weak var sellerNameTextField: UILabel!

    @IBOutlet weak var numberOfApprovedLabel: UILabel!
    @IBOutlet weak var approvedContainerView: UIView!
    @IBOutlet weak var approvedTableView: UITableView!
    @IBOutlet weak var heigthApprovedTableView: NSLayoutConstraint!

    @IBOutlet weak var numberOfSlopesLabel: UILabel!
    @IBOutlet weak var slopesContainerView: UIView!
    @IBOutlet weak var slopesTableView: UITableView!
    @IBOutlet weak var heigthSlopesTableView: NSLayoutConstraint!

    @IBOutlet weak var numberOfRejectedLabel: UILabel!
    @IBOutlet weak var rejectedContainerView: UIView!
    @IBOutlet weak var rejectedTableView: UITableView!
    @IBOutlet weak var heigthRejectedTableView: NSLayoutConstraint!

    var reportDetails: SaleReport!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configTablesView()
    }

    @IBAction func backAction(_ sender: Any) {
        Constants.Back(viewC: self)
    }

    func configTablesView() {
        approvedTableView.tag = 0
        slopesTableView.tag = 1
        rejectedTableView.tag = 2

        sellerNameTextField.text = reportDetails.nombreEmpleado
        let totalAprobadas = reportDetails.sumaAprobadas ?? 0
        numberOfApprovedLabel.text = String(totalAprobadas)
        if totalAprobadas != 0 {
            heigthApprovedTableView.constant = CGFloat(totalAprobadas) * 30.0
        } else {
            heigthApprovedTableView.constant = 0.0
            approvedContainerView.isHidden = true
        }

        let totalPendientes = reportDetails.sumaPendientes ?? 0
        numberOfSlopesLabel.text = String(totalPendientes)
        if totalPendientes != 0 {
            heigthSlopesTableView.constant = CGFloat(totalPendientes) * 30.0
        } else {
            heigthSlopesTableView.constant = 0.0
            slopesContainerView.isHidden = true
        }

        let totalRechazadas = reportDetails.sumaRechazadas ?? 0
        numberOfRejectedLabel.text = String(totalRechazadas)
        if totalRechazadas != 0 {
            heigthRejectedTableView.constant = CGFloat(totalRechazadas) * 70.0
        } else {
            heigthRejectedTableView.constant = 0.0
            rejectedContainerView.isHidden = true
        }
    }
}

extension ControlDeskReportDetailsViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView.tag == 2 {
            return 70.0
        } else {
            return 30.0
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        switch tableView.tag {
        case 0:
            return reportDetails.sumaAprobadas ?? 0
        case 1:
            return reportDetails.sumaPendientes ?? 0
        case 2:
            return reportDetails.sumaRechazadas ?? 0
        default:
            return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        switch tableView.tag {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SaleApprovedViewCell") as! SaleApprovedViewCell
            cell.contractNumber.text = reportDetails.aprobadas[indexPath.row].idVenta

            return cell

        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SaleSlopeViewCell") as! SaleSlopeViewCell
            cell.contractNumber.text = reportDetails.pendientes[indexPath.row].idVenta

            return cell

        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SaleRejectedViewCell") as! SaleRejectedViewCell
             let saleRejected = reportDetails.rechazadas[indexPath.row]

            cell.contractNumber.text = saleRejected.idVenta
            if saleRejected.tieneComentario {
                cell.numberOfCommentaries.text = "1 Comentario"
                cell.commentaryButton.tag = indexPath.row
                cell.commentaryButton.addTarget(self, action: #selector(handleButtonPressed(sender:)), for: .touchUpInside)
            } else {
                cell.numberOfCommentaries.text = "0 Comentarios"
            }

            return cell

        default:
            return UITableViewCell()
        }
    }

    @objc func handleButtonPressed(sender: UIButton) {
        let saleReportDetails = reportDetails.rechazadas[sender.tag]
        showCommentAlert(withReport: saleReportDetails)
    }

    func showCommentAlert(withReport saleReportDetails: SaleReportDetails) {
        let storyboard: UIStoryboard = UIStoryboard(name: "CommentAlertViewController", bundle: nil)
        let commentAlert = storyboard.instantiateViewController(withIdentifier: "CommentAlertViewController")as! CommentAlertViewController
        let newView: UIView = UIView()

        commentAlert.identifierNumber = saleReportDetails.idVenta
        let comments = [saleReportDetails.comentario]
        commentAlert.comments = comments
        commentAlert.providesPresentationContextTransitionStyle = true
        commentAlert.definesPresentationContext = true
        commentAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        commentAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve

        self.present(commentAlert, animated: false, completion: {})
    }
}
