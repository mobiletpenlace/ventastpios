//
//  ReportesViewController.swift
//  ventastpredesign
//
//  Created by Gustavo Tellez Diaz on 30/12/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class ReportsViewController: BaseVentasView {

    @IBOutlet weak var controlDeskLabel: UILabel!
    @IBOutlet weak var controlDeskSelector: UIView!
    @IBOutlet weak var salesLabel: UILabel!
    @IBOutlet weak var salesSelector: UIView!
    @IBOutlet weak var installationProcessLabel: UILabel!
    @IBOutlet weak var installationProcessSelector: UIView!
    @IBOutlet weak var installedLabel: UILabel!
    @IBOutlet weak var installedSelector: UIView!
    @IBOutlet weak var typeReportContainer: UIView!
    @IBOutlet weak var dateFilterTextField: UITextField!
    @IBOutlet weak var dateFilterViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchButtonImage: UIImageView!
    @IBOutlet weak var searchBar: UITextField!
    @IBOutlet weak var buttonSearch: UIButton!

    var viewModel: ReportsViewModel?
    var currentCategoryReports: CategoryReports = .sales
    var isSearchViewHide = false

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.setView(view: self.view)
        viewModel = ReportsViewModel(view: self)
        viewModel?.atachView(observer: self)
        deselectButtons()
         configFilterView()
        consultReportsWithDefaultDates()
        searchBar.addTarget(self, action: #selector(ReportsViewController.textFieldDidChange(_:)),
                                  for: .editingChanged)
    }

    @IBAction func backAction(_ sender: Any) {
        Constants.Back(viewC: self)
    }

    @IBAction func textFieldDidChange(_ sender: Any) {
        switch currentCategoryReports {
        case .controlDesk:

            SwiftEventBus.post("deskreports", sender: searchBar.text!)
        case .installationProcess:

            SwiftEventBus.post("intallationProcessReports", sender: searchBar.text!)
            if faseReportes.emptyReports == true {
                buttonSearch.isEnabled = false
            }
        case .installed:
            SwiftEventBus.post("installedReports", sender: searchBar.text!)
            if faseReportes.emptyReports == true {
                buttonSearch.isEnabled = false
            }
        case .sales:
            SwiftEventBus.post("sales", sender: searchBar.text!)
            if faseReportes.emptyReports == true {
                buttonSearch.isEnabled = false
            }
        default:
            break
        }
    }

    @IBAction func showViewInContainer(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            currentCategoryReports = .sales
        case 1:
            currentCategoryReports = .controlDesk
        case 2:
            currentCategoryReports = .installationProcess
        case 3:
            currentCategoryReports = .installed
        default:
            print("Valor fuera de rango: \(sender.tag)")
        }
        deselectButtons()
        consultReportsWithDefaultDates()
    }

    @IBAction func dateFilterButtonPressed(_ sender: Any) {
        showDateFilterCalendar()
    }

    @IBAction func searchButtonPressed(_ sender: Any) {
        isSearchViewHide = !isSearchViewHide
        if isSearchViewHide {
            faseReportes.searchButton = true
            searchBar.isHidden = false
            dateFilterViewConstraint.constant = 0.0
            searchViewConstraint.constant = 5.0
            searchButtonImage.image = UIImage(named: "ImageClose")
            searchBar.becomeFirstResponder()
        } else {
            faseReportes.searchButton = false

            SwiftEventBus.post("deskreports", sender: "")
            SwiftEventBus.post("intallationProcessReports", sender: "")
            SwiftEventBus.post("installedReports", sender: "")
            SwiftEventBus.post("sales", sender: "")
            dateFilterViewConstraint.constant = 240.0
            searchViewConstraint.constant = UIScreen.main.bounds.size.width
            searchButtonImage.image = UIImage(named: "icon_search_gray")
            hideKeyboard()
            searchBar.text! = ""
            searchBar.isHidden = true
        }

        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }

    func consultReportsWithDefaultDates() {
        let lastweek = DateCalendar.pastDate(date: Date(), value: 1, component: .weekOfMonth)
        let startDate = DateCalendar.convertDateString(date: lastweek!, formatter: Formate.dd_MM_yyyy_dash)
        let endDate = DateCalendar.convertDateString(date: Date(), formatter: Formate.dd_MM_yyyy_dash)
        dateFilterTextField.text = "\(startDate) / \(endDate)"
        consultReports(fromDate: startDate, toDate: endDate)
    }

    func consultReports(fromDate startDate: String, toDate endDate: String) {
        showLoading(message: "")
        switch currentCategoryReports {
        case .sales, .controlDesk:
            viewModel?.consultSalesReports(fromDate: startDate, toDate: endDate)
        case .installationProcess, .installed:
            viewModel?.consultInstalledReports(fromDate: startDate, toDate: endDate)
        }
    }

    func configFilterView() {
        faseReportes.searchButton = false
        dateFilterViewConstraint.constant = 240.0
        searchViewConstraint.constant = UIScreen.main.bounds.size.width
        searchButtonImage.image = UIImage(named: "icon_search_gray")
    }

    func showDateFilterCalendar() {
        let storyboard = UIStoryboard(name: "DateFilterCalendarVC", bundle: nil)
        let dateFilter = storyboard.instantiateViewController(withIdentifier: "DateFilterCalendarVC") as! DateFilterCalendarVC
        dateFilter.providesPresentationContextTransitionStyle = true
        dateFilter.definesPresentationContext = true
        dateFilter.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        dateFilter.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        dateFilter.dateFilterCalendarDelegate = self
        self.present(dateFilter, animated: false, completion: {})
    }
}
