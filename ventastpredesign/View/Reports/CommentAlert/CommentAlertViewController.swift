//
//  CommentAlertViewController.swift
//  ventastpredesign
//
//  Created by Gustavo Tellez Diaz on 07/01/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

class CommentAlertViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var heigthView: NSLayoutConstraint!
    @IBOutlet weak var contractNumberLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!

    var identifierNumber = ""
    var comments: [String] = []
    var maxHeightToView: CGFloat = 0.0

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        heigthView.constant = maxHeightToView
        contractNumberLabel.text = identifierNumber
    }

    override func viewDidAppear(_ animated: Bool) {
        showCommentsView()
    }

    @IBAction func backAction(_ sender: Any) {
        Constants.Back(viewC: self)
    }

    func showCommentsView() {
        if comments.count == 0 {
            maxHeightToView = 190.0
            tableView.isScrollEnabled = false
        } else if comments.count <= 3 {
            maxHeightToView = 260.0
            tableView.isScrollEnabled = false
        } else {
            maxHeightToView = 300.0
        }
        heigthView.constant = maxHeightToView
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40.0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentAlertCell") as! CommentAlertCell
        cell.commentLabel.text = comments[indexPath.row]

        return cell
    }
}
