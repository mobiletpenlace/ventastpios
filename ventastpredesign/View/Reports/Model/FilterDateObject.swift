//
//  FilterDateObject.swift
//  ventastpredesign
//
//  Created by Gustavo Tellez Diaz on 29/01/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation

struct FilterDateObject {
    var startDate: String = ""
    var endDate: String = ""
}
