//
//  InstalledReport.swift
//  ventastpredesign
//
//  Created by Gustavo Tellez Diaz on 02/02/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class InstalledReport: NSObject, Mappable {

    var numEmpleado: String = ""
    var nombreEmpleado: String = ""
    var sumaAgendado: Int?
    var sumaConfirmado: Int?
    var sumaCompletado: Int?
    var sumaRescate: Int?
    var sumaCancelado: Int?
    var agendado: [InstalledReportDetails] = []
    var confirmado: [InstalledReportDetails] = []
    var completado: [InstalledReportDetails] = []
    var rescate: [InstalledReportDetails] = []
    var cancelado: [InstalledReportDetails] = []

    required init?(map: Map) {
    }
    override init() {

    }
    func mapping(map: Map) {
        numEmpleado <- map["noEmpleado"]
        nombreEmpleado <- map["name"]
        sumaAgendado <- map["sumaAgendado"]
        sumaConfirmado <- map["sumaConfirmado"]
        sumaCompletado <- map["sumaCompletado"]
        sumaRescate <- map["sumaRescate"]
        sumaCancelado <- map["sumaCancelado"]
        agendado <- map["agendado"]
        confirmado <- map["confirmado"]
        completado <- map["completado"]
        rescate <- map["rescate"]
        cancelado <- map["cancelado"]
    }
}
