//
//  ControlDeskCollaborator.swift
//  ventastpredesign
//
//  Created by Gustavo Tellez Diaz on 25/01/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class SaleReport: NSObject, Mappable {

    var numEmpleado: String = ""
    var nombreEmpleado: String = ""
    var sumaVentas: Int?
    var sumaAprobadas: Int?
    var sumaPendientes: Int?
    var sumaRechazadas: Int?
    var aprobadas: [SaleReportDetails] = []
    var pendientes: [SaleReportDetails] = []
    var rechazadas: [SaleReportDetails] = []

    required init?(map: Map) {
    }
    override init() {

    }
    func mapping(map: Map) {
        numEmpleado <- map["noEmpleado"]
        nombreEmpleado <- map["name"]
        sumaVentas <- map["sumaVentas"]
        sumaAprobadas <- map["sumaAprobadas"]
        sumaPendientes <- map["sumaPendientes"]
        sumaRechazadas <- map["sumarechazadas"]
        aprobadas <- map["aprobadas"]
        pendientes <- map["pendientes"]
        rechazadas <- map["rechazadas"]
    }
}
