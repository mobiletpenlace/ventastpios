//
//  InstalledReportsResponse.swift
//  ventastpredesign
//
//  Created by Gustavo Tellez Diaz on 02/02/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class InstalledReportsResponse: BaseResponse {

    var colaboradores: [InstalledReport] = []
    var error: String = ""

    required init?(map: Map) {
    }

    override func mapping(map: Map) {
        colaboradores <- map["colaboradores"]
        error <- map["error"]
    }
}
