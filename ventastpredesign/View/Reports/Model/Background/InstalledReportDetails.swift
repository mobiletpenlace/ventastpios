//
//  InstalledReportDetails.swift
//  ventastpredesign
//
//  Created by Gustavo Tellez Diaz on 02/02/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class InstalledReportDetails: NSObject, Mappable {
    var idVenta: String = ""
    var fecha: String = ""
    var comentario: [String] = []
    var tieneComentario: Bool = false
    var motivo: String = ""
    var tieneMotivo: Bool = false

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        idVenta <- map["idventa"]
        fecha <- map["fecha"]
        comentario <- map["comentario"]
        tieneComentario <- map["flagComentario"]
        motivo <- map["motivo"]
        tieneMotivo <- map["flagMotivo"]
    }
}
