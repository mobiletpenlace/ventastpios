//
//  TokenReportRequest.swift
//  ventastpredesign
//
//  Created by Gustavo Tellez Diaz on 25/01/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class TokenReportRequest: BaseRequest {

    var parameter1: String = "testAppVentas1"
    var parameter2: String = "appTPVentas"

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        parameter1 <- map["parameter1"]
        parameter2 <- map["parameter2"]
    }
}
