//
//  ControlDeskReportResponse.swift
//  ventastpredesign
//
//  Created by Gustavo Tellez Diaz on 25/01/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class SaleReportsResponse: BaseResponse {

    var colaboradores: [SaleReport] = []
    var error: String = ""

    required init?(map: Map) {
    }

    override func mapping(map: Map) {
        colaboradores <- map["colaboradores"]
        error <- map["error"]
    }
}
