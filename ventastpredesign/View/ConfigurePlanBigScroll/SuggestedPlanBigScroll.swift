//
//  SuggestedPlanBigScroll.swift
//  ventastp
//
//  Created by Armando Isais Olguin Cabrera on 19/11/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus
import BaseClases
import PKHUD

class SuggestedPlanBigScroll: BaseVentasView {

// OUTLET'S FIRST SECTION
    @IBOutlet weak var firstSectionView: UIView!
        // Contents Views
        @IBOutlet weak var benefitFreeView: UIView!
        @IBOutlet weak var benefitFreeCellsContainer: UIView!
        // Constraints
    @IBOutlet weak var benefitFreeCellsContainerHeigth: NSLayoutConstraint!

    // Totalplay TV
    // Content Views
    @IBOutlet weak var totalplayTvView: UIView!
    @IBOutlet weak var totalplayTvCellsContainer: UIView!
    @IBOutlet weak var totalplayChannelsViewContainer: UIView!
    // Constraints
    @IBOutlet weak var TotalplayTvCellsContainerHeigth: NSLayoutConstraint!
    @IBOutlet weak var totalplayChannelsViewContainerHeigth: NSLayoutConstraint!

    // STREAMING
    @IBOutlet weak var streamingView: UIView!
    @IBOutlet weak var heightStreaming: NSLayoutConstraint!
    @IBOutlet weak var streamingTitle: UILabel!
    @IBOutlet weak var devicesStack: UIStackView!
    @IBOutlet weak var noCombinarStack: UIStackView!
    @IBOutlet weak var noCombinarView: UIView!
    @IBOutlet weak var streamingStack: UIStackView!
    @IBOutlet weak var netflixView: UIView!
    @IBOutlet weak var amazonView: UIView!
    @IBOutlet weak var netflixButton: UIButton!
    @IBOutlet weak var amazonButton: UIButton!
    @IBOutlet weak var notthingButtton: UIButton!
    @IBOutlet weak var twoDevicesView: UIView!
    @IBOutlet weak var fourDevicesView: UIView!
    @IBOutlet weak var twoDevicesButton: UIButton!
    @IBOutlet weak var fourDevicesButton: UIButton!
    @IBOutlet weak var selectionStreamingLabel: UILabel!
    @IBOutlet weak var cambiarStreamingButton: UIButton!

// END FIRST SECTION

    // Totalplay TV Addons
    // Contents Views
    @IBOutlet weak var TotalplayTVAddonsView: UIView!
    @IBOutlet weak var TotalplayTVAddonsCellsContainer: UIView!
    // Constraints
    @IBOutlet weak var TotalplayTVViewHeigth: NSLayoutConstraint!
    @IBOutlet weak var TotalplayTVAddonsCellsContainerHeigth: NSLayoutConstraint!

    // WiFi extender Addons
    // Contents Views
    @IBOutlet weak var WiFiExtenderAddonsView: UIView!
    @IBOutlet weak var WiFiExtenderAddonsCellsContainer: UIView!
    // Constraints
    @IBOutlet weak var WiFiExtenderAddonsCellsContainerHeigth: NSLayoutConstraint!

    // TV Premium Addons
    // Contents Views
    @IBOutlet weak var TVPremiumAddonsView: UIView!
    @IBOutlet weak var TVPremiumAddonsCellsContainer: UIView!
    // Constraints
    @IBOutlet weak var TVPremiumAddonsCellsContainerHeigth: NSLayoutConstraint!

    var lastContentOffset: CGFloat = 0
    var prueba = ["", "", ""]
    var prueba2 = [""]
    var prueba3 = [""]
    var alturaTVOriginal: CGFloat!
    var alturaInternetOriginal: CGFloat!
    var alturaStreamingOriginal: CGFloat!
    var mBestFitViewModel: BestFitViewModel!
    var oferta: String = "micronegocios"
    var velocitySelected: String = ""
    var planSelected: Plan!
    var isDoble: Bool = false
    var isMatchEstandar: Bool = false
    var isMatchPremium: Bool = false
    var isUnbox: Bool = false
    var isStreaming: Bool = false
    let checkImage = UIImage(named: "Image_rede_blue_check.png")
    let myImageView: UIImageView = UIImageView()
    var heightTvbuttons: CGFloat = 91.5
    var alturaTv: CGFloat = 0
    var alturaTvVSB: CGFloat = 0
    var alturaSinTv: CGFloat = 0
    var velocidadSelected = false
    var tipeTvSelected = false
    var streamingSelected = false

    let numberFormatter = NumberFormatter()

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: view)
        showStreaming()
        // TEMPORAL
        selectionStreamingLabel.alpha = 0.5
        cambiarStreamingButton.isHidden = true
        llenaViewBenefitFree(typeCell: 1, arreglo: prueba, identifier: "BenefitFreeCell", anchoCelda: UIScreen.main.bounds.width, altoCelda: 86, container: self.benefitFreeCellsContainer)
        llenaViewBenefitFree(typeCell: 2, arreglo: prueba2, identifier: "BenefitFreeCell", anchoCelda: UIScreen.main.bounds.width, altoCelda: 86, container: self.totalplayTvCellsContainer)
        llenaViewBenefitFree(typeCell: 3, arreglo: prueba3, identifier: "BenefitFreeCell", anchoCelda: UIScreen.main.bounds.width, altoCelda: 86, container: self.totalplayChannelsViewContainer)
        llenaViewBenefitFree(typeCell: 4, arreglo: prueba3, identifier: "BenefitFreeCell", anchoCelda: UIScreen.main.bounds.width, altoCelda: 86, container: self.TotalplayTVAddonsCellsContainer)
        llenaViewBenefitFree(typeCell: 5, arreglo: prueba3, identifier: "BenefitFreeCell", anchoCelda: UIScreen.main.bounds.width, altoCelda: 86, container: self.WiFiExtenderAddonsCellsContainer)

    }

    override func viewWillDisappear(_ animated: Bool) {

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func viewDidDisappear(_ animated: Bool) {

    }

//    func llenaViewBenefitFree(typeCell: Int, arreglo : [String], identifier :  String, anchoCelda : CGFloat,altoCelda : CGFloat, container : UIView, clase : Int){
//
//        switch typeCell {
//        case 1:
//
//            benefitFreeCellsContainerHeigth.constant = CGFloat(contador) * altoCelda
//        case 2:
//            var contador = 0
//            contador =  arreglo.count
//            for index in 0 ..< contador{
//                if let view : BaseCollectionViewCell = Bundle.main.loadNibNamed(identifier, owner: self, options: nil)?.first as? BaseCollectionViewCell{
//                    view.setData(arreglo[index],typeCell)
//    //                view.setData(arreglo[index])
//                    container.addSubview(view)
//                    view.frame.size.height = CGFloat(altoCelda)
//                    view.frame.size.width = CGFloat(anchoCelda)
//                    view.frame.origin.y = CGFloat(index) * CGFloat(altoCelda)
//
//
//                }
//            }
//            TotalplayTvCellsContainerHeigth.constant = CGFloat(contador) * altoCelda
//        default:
//            print("default")
//        }
//        SwiftEventBus.post("sendTypeCell", sender: typeCell)
//
////        insideOtherProductsHeigth.constant = CGFloat(contador) * altoCelda
////        otherProductsHeigth.constant = otherProductsHeigth.constant + insideOtherProductsHeigth.constant
//    }

    func llenaViewBenefitFree(typeCell: Int, arreglo: [String], identifier: String, anchoCelda: CGFloat, altoCelda: CGFloat, container: UIView) {
        var contador = 0
        contador =  arreglo.count
        for index in 0 ..< contador {
            if let view = Bundle.main.loadNibNamed("BenefitFreeCell", owner: self, options: nil)?.first as? BenefitFreeCell {
                view.setData(arreglo[index] as NSObject, typeCell)
                container.addSubview(view)
                view.frame.size.height = CGFloat(altoCelda)
                view.frame.size.width = CGFloat(anchoCelda)
                view.frame.origin.y = CGFloat(index) * CGFloat(altoCelda)
            }
        }

        switch typeCell {
        case 1:
            benefitFreeCellsContainerHeigth.constant = CGFloat(contador) * altoCelda
        case 2:
            TotalplayTvCellsContainerHeigth.constant = CGFloat(contador) * altoCelda
        case 3:
            TotalplayTvCellsContainerHeigth.constant = CGFloat(contador) * altoCelda
        case 4:
            TotalplayTVAddonsCellsContainerHeigth.constant = CGFloat(contador) * altoCelda
            TotalplayTVViewHeigth.constant = CGFloat(77) + TotalplayTVAddonsCellsContainerHeigth.constant
        case 5:
            WiFiExtenderAddonsCellsContainerHeigth.constant = CGFloat(contador) * altoCelda
        default:
            print("default")
        }
    }

    func buttonsStreamingLogic() {
        self.netflixButton.isSelected = false
        self.amazonButton.isSelected = false
        self.notthingButtton.isSelected = false
        self.netflixButton.borderColor = UIColor(named: "Base_rede_semiclear_gray")
        self.netflixButton.borderWidth = 2
        self.amazonButton.borderColor = UIColor(named: "Base_rede_semiclear_gray")
        self.amazonButton.borderWidth = 2
        self.notthingButtton.borderColor = UIColor(named: "Base_rede_semiclear_gray")
        self.notthingButtton.borderWidth = 2
    }

    func buttonsDevicesLogic() {
        self.twoDevicesButton.isSelected = false
        self.fourDevicesButton.isSelected = false
        self.twoDevicesButton.borderColor = UIColor(named: "Base_rede_semiclear_gray")
        self.twoDevicesButton.borderWidth = 2
        self.fourDevicesButton.borderColor = UIColor(named: "Base_rede_semiclear_gray")
        self.fourDevicesButton.borderWidth = 2
    }

    @IBAction func cambiarStreamingAction(_ sender: Any) {
        showStreaming()
    }

    func showStreaming() {
            cambiarStreamingButton.isHidden = true
            streamingTitle.isHidden = true
            selectionStreamingLabel.isHidden = true
            if selectionStreamingLabel.text!.contains(string: "NETFLIX") == true {
                showStack(show1: false, show2: true)
            } else {
                showStack(show1: true, show2: false)
            }
        }

    func showStack(show1: Bool, show2: Bool) {
        devicesStack.isHidden = show1
        noCombinarStack.isHidden = show2
    }

    func colapseStreaming(seleccion: String) {
        if streamingSelected {
            selectionStreamingLabel.text = seleccion
            cambiarStreamingButton.isHidden = false
        } else {
            cambiarStreamingButton.isHidden = true
            selectionStreamingLabel.text = "Netflix o Amazon Prime"
        }
        selectionStreamingLabel.alpha = 1
        selectionStreamingLabel.isHidden = false
        streamingTitle.isHidden = true
        heightStreaming.constant = 60
        devicesStack.isHidden = true
        noCombinarStack.isHidden = true
    }

//    // MARK: - Botones de seleccion Streaming
//
    @IBAction func netflixAction(_ sender: Any) {
        buttonsDevicesLogic()
        devicesStack.isHidden = false
        noCombinarStack.isHidden = true
        buttonsStreamingLogic()
        seleccionaBoton(viewParent: netflixView, boton: netflixButton)
        isUnbox = false
    }
//
    @IBAction func amazonAction(_ sender: Any) {
        streamingSelected = true
        devicesStack.isHidden = true
        noCombinarStack.isHidden = false
        buttonsStreamingLogic()
        seleccionaBoton(viewParent: amazonView, boton: amazonButton)
        colapseStreaming(seleccion: "AMAZON PRIME")// AMAZON
        cambiarStreamingButton.isHidden = false
        isMatchEstandar = false
        isMatchPremium = false
        isUnbox = true

    }
//
    @IBAction func nothingAction(_ sender: Any) {
        buttonsStreamingLogic()
        seleccionaBoton(viewParent: noCombinarView, boton: notthingButtton)
        colapseStreaming(seleccion: "Sin streaming")
        isMatchEstandar = false
        isMatchPremium = false
        isUnbox = false
        cambiarStreamingButton.isHidden = false
    }
//
//
    @IBAction func twoDevicesAction(_ sender: Any) {
        streamingSelected = true
        colapseStreaming(seleccion: "NETFLIX 2 pantallas")// NETFLIX ESTANDAR
        buttonsDevicesLogic()
        seleccionaBoton(viewParent: twoDevicesView, boton: twoDevicesButton)
        isMatchEstandar = true
        isMatchPremium = false
        cambiarStreamingButton.isHidden = false
    }

    @IBAction func fourDevicesAction(_ sender: Any) {
        streamingSelected = true
    colapseStreaming(seleccion: "NETFLIX 4 pantallas")// NETFLIX PREMIUM
        buttonsDevicesLogic()
        seleccionaBoton(viewParent: fourDevicesView, boton: fourDevicesButton)
        isMatchEstandar = false
        isMatchPremium = true
        cambiarStreamingButton.isHidden = false
    }

    func seleccionaBoton(viewParent: UIView, boton: UIButton) {
        boton.borderColor = UIColor(named: "Base_rede_button_dark")
        boton.borderWidth = 3
        boton.isSelected  = true
        myImageView.contentMode = UIView.ContentMode.scaleAspectFit
        myImageView.frame.size.width = 20
        myImageView.frame.size.height = 20
        myImageView.frame.origin.y = boton.frame.origin.y - 5
        myImageView.frame.origin.x = boton.frame.origin.x + boton.width - 15
        myImageView.image = checkImage
        viewParent.addSubview(myImageView)
    }

}
