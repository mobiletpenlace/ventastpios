//
//  PromoCode.swift
//  ventastp
//
//  Created by Armando Isais Olguin Cabrera on 17/11/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus
import MaterialComponents.MDCFilledTextField

class PromoCode: BaseVentasView {

    @IBOutlet weak var PromoCodeTextField: MDCFilledTextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        configTextField()
    }

    func configTextField() {
        PromoCodeTextField.normalTheme(type: 2)
    }

}
