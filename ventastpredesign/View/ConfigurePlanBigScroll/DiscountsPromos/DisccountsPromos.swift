//
//  DisccountsPromos.swift
//  ventastp
//
//  Created by Armando Isais Olguin Cabrera on 16/11/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class DisccountsPromos: BaseVentasView {

    @IBAction func OpenCode(_ sender: Any) {
        let viewV = UIStoryboard(name: "PromoCode", bundle: nil)
        let controller = viewV.instantiateViewController(withIdentifier: "PromoCode") as! PromoCode
        controller.providesPresentationContextTransitionStyle = true
        controller.definesPresentationContext = true
        controller.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        UIApplication.topViewController()?.present(controller, animated: false, completion: nil)
    }

}
