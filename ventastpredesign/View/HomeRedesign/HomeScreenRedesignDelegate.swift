//
//  HomeScreenRedesignDelegate.swift
//  ventastp
//
//  Created by Armando Isais Olguin Cabrera on 19/12/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import Foundation
import ImageSlideshow
import Amplitude

extension HomeScreenRedesign: ImageSlideshowDelegate, HomeObserver {
    func onSuccesBanners(_ banners: [BannersHome]) {
        ImageBanners = []
        ImageBannersTitles = []
        let dispatchGroup = DispatchGroup()

        for ban in banners {
            if let imageUrl = ban.imagen.first?.url {
                dispatchGroup.enter()
                UIImage.URLimage(url: imageUrl) { image in
                    if let imageBanner = image {
                        self.ImageBanners.append(ImageSource(image: imageBanner))
                        self.ImageBannersTitles.append(ban.imagen.first?.name ?? "no title")
                    }
                    dispatchGroup.leave()
                }
            }
        }

        dispatchGroup.notify(queue: .main) { [self] in
            self.setSlide(image: ImageBanners)
            isDownloadedBanners = true
        }
    }

    func IniciaVenta(id: String) {
        IdOp = id
        CloseMenu()
        ventaNueva()
    }

    func OcultaMenu() {
        CloseMenu()
    }

    func imageSlideshow(_ imageSlideshow: ImageSlideshow, didChangeCurrentPageTo page: Int) {
        // por el moemnto no se ocupa
    }

    func setSlide(image: [InputSource]) {
        pageControl.numberOfPages = ImageBanners.count
        slideshow.currentPageChanged = { [self] page in
            self.pageControl.currentPage = page}
        slideshow.slideshowInterval = 3.0
        slideshow.contentScaleMode = UIViewContentMode.scaleAspectFill
        slideshow.pageIndicator = nil
        slideshow.activityIndicator = DefaultActivityIndicator()
        slideshow.delegate = self
        slideshow.setImageInputs(image)
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(HomeVC.didTap))
        slideshow.addGestureRecognizer(recognizer)
    }

    @objc func didTap() {
//        if slideshow.currentPage == 0 {
//            Constants.LoadStoryBoard(name: storyBoard.HumanResources, viewC: self)
//        } else {
//            let fullScreenController = slideshow.presentFullScreenController(from: self)
//            fullScreenController.slideshow.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
//        }
        Amplitude.instance().logEvent("Banner_Tapped", withEventProperties: ["Banner_Title": ImageBannersTitles[slideshow.currentPage], "Banner_Position": slideshow.currentPage + 1])
    }

    func setMenu() {
        MenuVC = (UIStoryboard(name: "MenuViewController", bundle: nil).instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController)
//        if (employed?.Canales.count)! <= 1{
            for canal in employed!.Canales {
                if canal.nombre == "Embajadores" || canal.nombre == "Autoempresarios" {
                    UserDefinitions.isEmbajador = true
                    UserDefinitions.isPromotor = false
                    UserDefinitions.isEmpresarial = false
                    UserDefinitions.isResidencial = false
                }
                if canal.nombre.uppercased() == "Autoempresarios Autorizados".uppercased() {
                    UserDefaults.standard.set(true, forKey: "isAutoentrepreneur")
                } else {
                    UserDefaults.standard.set(false, forKey: "isAutoentrepreneur")
                }
            }
        if UserDefinitions.isEmbajador {
            if employed?.Posicion != nil {
                if (employed?.Posicion.uppercased().contains(string: "MINISTRO"))! {
                    UserDefinitions.isMinistro = true
                } else {
                    UserDefinitions.isMinistro = false
                }
            }
        } else {
            UserDefinitions.isEmbajador = false
            UserDefinitions.isPromotor = false
            UserDefinitions.isEmpresarial = false
            UserDefinitions.isResidencial = true
            UserDefinitions.isMinistro = false
        }

        MenuVC.isEmbajador = UserDefinitions.isEmbajador
        let SwipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToGesture))
        SwipeRight.direction = UISwipeGestureRecognizerDirection.right
        let SwipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToGesture))
        SwipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(SwipeRight)
        self.view.addGestureRecognizer(SwipeLeft)
        self.MenuVC.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height )
        let fullName = employed?.Nombre
        let arrFullName = fullName?.components(separatedBy: " ")
        let arrDistritalName = employed?.DistritalName.components(separatedBy: "-")
        let arrCanal = employed?.Canal.components(separatedBy: ";")
        let surName = arrFullName![0].capitalizingFirstLetter()
        let lastName = arrFullName![1].capitalizingFirstLetter()
        let distrital = arrDistritalName![0].uppercased()
        let canal = arrCanal![0].uppercased()
        let posicion = employed!.Posicion
        if employed?.Nombre != nil {
            mNameLabel.text = "¡Hola " + surName + " " + lastName + "!"
        } else {
            mNameLabel.text = "No name"
        }
        MenuVC.mNombreLabel.text = employed?.Nombre ?? "No name"
        mCityLabel.text =  distrital + " - " + canal + ", " + posicion + "      "
        mCityLabel.layer.cornerRadius = 5
        MenuVC.mPositionLabel.text = posicion + "  "
        MenuVC.mPositionLabel.layer.cornerRadius = 5
    }

    func ShowMenu() {
        UIView.animate(withDuration: 0.5) {() -> Void in
            self.MenuVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height )
            self.MenuVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.0)
            self.addChildViewController(self.MenuVC)
            self.view.addSubview(self.MenuVC.view)
            AppDelegate.menu_bool = false
        }
        UIView.animate(withDuration: 1.00) {() -> Void in
            self.MenuVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        }
    }

    func CloseMenu() {
        self.MenuVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.0)
        UIView.animate(withDuration: 0.5, animations: {
            self.MenuVC.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        }) {(_) in
            self.MenuVC.view.removeFromSuperview()
        }
        AppDelegate.menu_bool = true
    }

    func ventaNueva() {
        Constants.LoadStoryBoard(name: storyBoard.Start, viewC: self)
    }

    @objc func respondToGesture(gesture: UISwipeGestureRecognizer) {
        switch gesture.direction {
        case UISwipeGestureRecognizerDirection.right:
            ShowMenu()
        case UISwipeGestureRecognizerDirection.left:
            CloseMenu()
        default:
            break
        }
    }

    func clearDataNewSell() {
        fasePreguntas.editar = false
        fasePreguntas.idCliente = ""
        fasePreguntas.numberOfPeople = 2
        fasePreguntas.numberOfRooms = 2
        fasePreguntas.tvProvider = ""
        fasePreguntas.internetProvider = ""
        fasePreguntas.phoneProvider = ""
        fasePreguntas.favoriteApps = ""
        fasePreguntas.usesInternet = ""
        faseSelectedPlan.plan1 = LlenadoObjeto()
        faseSelectedPlan.plan2 = LlenadoObjeto()
    }
}
