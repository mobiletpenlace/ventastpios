//
//  HomeScreenRedesign.swift
//  ventastp
//
//  Created by Armando Isais Olguin Cabrera on 07/12/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import UIKit
import ImageSlideshow
import PDFKit
import Amplitude

class HomeScreenRedesign: BaseVentasView, CanalEmpleadoDelegate {

    func setCanal(canal: String) {
    }

    func setSubCanal(subCanal: String) {
    }

    func didDismissCanalEmpleadoView() {
        getEmploye()
    }

    @IBOutlet weak var mNameLabel: UILabel!
    @IBOutlet weak var mCityLabel: UILabel!
    @IBOutlet var slideshow: ImageSlideshow!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var topLogoImageView: UIImageView!
    @IBOutlet weak var newOrderView: UIView!
    @IBOutlet weak var newOrderButton: UIButton!

    var MenuVC: MenuViewController!
    var rankingS: String = ""
    var employed: Empleado?
    var viewModel: HomeViewModel?
    var IdOp: String = ""
    var ImageBanners: [InputSource] = []
    var ImageBannersTitles: [String] = []
    var isDownloadedBanners: Bool = false
    var canSell: Bool = false

    override func viewDidLoad() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleCanalEmpleadoViewClosed), name: Notification.Name("CanalEmpleadoViewClosed"), object: nil)
        super.setView(view: self.view)
        viewModel = HomeViewModel(view: self)
        viewModel?.atachView(observer: self)
        getEmploye()
        setMenu()
        if UserDefaults.standard.bool(forKey: "isAutoentrepreneur") {
            topLogoImageView.image = UIImage(named: "autoentrepreneurs_logo")
            slideshow.isHidden = true
            pageControl.isHidden = true
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        Amplitude.instance().logEvent("Show_HomeScreen")
        faseDataOK.isSended = false
        if isDownloadedBanners == false {
            viewModel?.getBanners()
        }
        clearDataNewSell()
    }

    func getEmploye() {
        if RealManager.findFirst(object: Empleado.self) != nil {
            employed = Empleado(value: RealManager.findFirst(object: Empleado.self)!)
            faseComisiones.numEmpleado = employed!.NoEmpleado
            let canalText = employed?.CanalSelect.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
            let subCanalText = employed?.SubCanalSelect.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
            if (canalText == "Cambaceo" || canalText == "Desarrollos"  || canalText == "Recuperación" || (canalText == "Punto de Venta" && !subCanalText.isEmpty) || (canalText == "Convenios" && !subCanalText.isEmpty)) {
                canSell = true
                configNewOrderButton(canSell: canSell)
            } else {
                canSell = false
                configNewOrderButton(canSell: canSell)
            }
        }
    }

    @objc func handleCanalEmpleadoViewClosed() {
         getEmploye()
     }

    deinit {
        // Elimina el observador al liberar la vista
        NotificationCenter.default.removeObserver(self)
    }

    //    Header Section
    @IBAction func hamburguerMenuAction(_ sender: Any) {
        if AppDelegate.menu_bool {
            ShowMenu()
        } else {
            CloseMenu()
        }
    }

    @IBAction func myProfileAction(_ sender: Any) {
        print("myProfile Button")
    }

    //    Second Section
    @IBAction func sellsMonitorAction(_ sender: Any) {
        let navigation = UINavigationController()
        navigation.modalPresentationStyle = .fullScreen
        AES128.shared.setKey(key: statics.key)
        if RealManager.findFirst(object: Empleado.self) != nil {
            // Empleado exisits in Realm
            let empleado = Empleado(value: RealManager.findFirst(object: Empleado.self)!)
            let statusTrayVc = MonitoringTrayView(employeeID: empleado.Id, employeeName: empleado.Nombre)
            //            statusTrayVc.employee = empleado
            navigation.pushViewController(statusTrayVc, animated: false)
            present(navigation, animated: false)
        } else {
            // DO Nothing
            print("Error, no se encontró el empleado en realm")
        }
    }

    @IBAction func newClientAction(_ sender: Any) {
        Constants.LoadStoryBoard(name: storyBoard.DigitalTray, viewC: self)
    }

    //    Third Section
    @IBAction func myCommissionsAction(_ sender: Any) {
        if UserDefinitions.isEmbajador {
            if UserDefinitions.isMinistro {
                Constants.LoadStoryBoard(name: storyBoard.ComissionsEmbajador.WelcomeMinistro, viewC: self)
            } else {
                Constants.LoadStoryBoard(name: storyBoard.ComissionsEmbajador.WelcomeEmbajador, viewC: self)
            }
        } else {
            Constants.LoadStoryBoard(name: storyBoard.Comissions.Welcome, viewC: self)
        }
    }

    @IBAction func employeeChannelAction(_ sender: Any) {
        let viewAlert: UIStoryboard = UIStoryboard(name: "CanalEmpleadoView", bundle: nil)
        let viewAlertVC = viewAlert.instantiateViewController(withIdentifier: "CanalEmpleadoView") as! CanalEmpleadoView
        viewAlertVC.delegate = self
        viewAlertVC.providesPresentationContextTransitionStyle = true
        viewAlertVC.definesPresentationContext = true
        viewAlertVC.modalPresentationStyle = .overCurrentContext
        viewAlertVC.modalTransitionStyle = .crossDissolve
        self.present(viewAlertVC, animated: false, completion: nil)
    }

    @IBAction func sellToolsAction(_ sender: Any) {
        Constants.LoadStoryBoard(name: storyBoard.FlyersView, viewC: self)
    }

    @IBAction func startSell(_ sender: Any) {
        if !canSell {
            createAlert()
        } else {
            ventaNueva()
        }
    }

    @objc func alertControllerBackgroundTapped() {
        self.dismiss(animated: true, completion: nil)
    }

    func configNewOrderButton(canSell: Bool) {
        if canSell == true {
            newOrderView.backgroundColor = .systemBlue
        } else {
            newOrderView.backgroundColor = .systemGray
        }
    }

    func createAlert() {
        let dialogMessage = UIAlertController(title: "Alerta", message: "Elige un subcanal antes de continuar", preferredStyle: .alert)

           let ok = UIAlertAction(title: "Elegir...", style: .default, handler: { (action) -> Void in
               let viewAlert: UIStoryboard = UIStoryboard(name: "CanalEmpleadoView", bundle: nil)
               let viewAlertVC = viewAlert.instantiateViewController(withIdentifier: "CanalEmpleadoView") as! CanalEmpleadoView
               viewAlertVC.delegate = self
               viewAlertVC.providesPresentationContextTransitionStyle = true
               viewAlertVC.definesPresentationContext = true
               viewAlertVC.modalPresentationStyle = .overCurrentContext
               viewAlertVC.modalTransitionStyle = .crossDissolve
               self.present(viewAlertVC, animated: false, completion: nil)
           })

        let cancel = UIAlertAction(title: "Cancelar", style: .cancel) { (action) -> Void in
               print("Cancel button tapped")
           }

           dialogMessage.addAction(ok)
           dialogMessage.addAction(cancel)

           self.present(dialogMessage, animated: true, completion: nil)
    }

}
