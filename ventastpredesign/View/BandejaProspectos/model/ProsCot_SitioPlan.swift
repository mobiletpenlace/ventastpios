//
//  ProsCot_SitioPlan.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 18/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import UIKit

import ObjectMapper
import RealmSwift

class ProsCot_SitioPlan: Object, Mappable {

    func mapping(map: Map) {
        DP_Plan <- map["DP_Plan"]
        NombrePlan <- map["NombrePlan"]
    }

    public required convenience init?(map: Map) {
        self.init()
    }

    override static func primaryKey() -> String? {
        return "uui"
    }

    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  DP_Plan: String = ""
    @objc dynamic var  NombrePlan: String = ""

}
