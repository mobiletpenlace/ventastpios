//
//  ProsCot_PlanPromocion.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 18/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import UIKit

import ObjectMapper
import RealmSwift

class ProsCot_PlanPromocion: Object, Mappable {

    func mapping(map: Map) {
        mDP_PromocionPlan <- map["DP_PromocionPlan"]
    }

    public required convenience init?(map: Map) {
        self.init()
    }

    override static func primaryKey() -> String? {
        return "uui"
    }

    @objc dynamic var  uui: String = UUID().uuidString
    var mDP_PromocionPlan: List<ProsDP_PromocionPlan> = List<ProsDP_PromocionPlan>()

}
