//
//  BandejaPropuesta.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 12/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper
import RealmSwift

class BandejaPropuesta: Object {

    override static func primaryKey() -> String? {
        return "uui"
    }

    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  NombreCompleto: String = ""
    @objc dynamic var  Direccion: String = ""
    @objc dynamic var  Paquete = ""
    @objc dynamic var  Oferta = ""
    @objc dynamic var  AplicaEstimuloFiscal = ""
    @objc dynamic var  Colonia = ""
    @objc dynamic var  TipoPago = ""
    @objc dynamic var  ProsContacto: ProsContacto?
    @objc dynamic var  ProsNIP: ProsNIP?
    @objc dynamic var  ProsDireccion: ProsDireccion?
    @objc dynamic var  ProsDatosAdicionales: ProsDatosAdicionales?
    @objc dynamic var  ProsVendedor: ProsVendedor?
    @objc dynamic var  ProsMetodoPago: ProsMetodoPago?
    @objc dynamic var  ProsDireccionFactu: ProsDireccionFactu?
    @objc dynamic var  ProsImgContrato: ProsImgContrato?
    @objc dynamic var  ProsImgIDFront: ProsImgIDFront?
    @objc dynamic var  ProsImgIDBack: ProsImgIDBack?
    @objc dynamic var  ProsImgComprobante: ProsImgComprobante?
    @objc dynamic var  ProsImgFirma: ProsImgFirma?
    @objc dynamic var  ProsImgRFC: ProsImgRFC?
    @objc dynamic var  ProsImgActa1: ProsImgActa1?
    @objc dynamic var  ProsImgActa2: ProsImgActa2?
    @objc dynamic var  ProsImgActa3: ProsImgActa3?
    @objc dynamic var  ProsImgIDFrontCard: ProsImgIDFrontCard?
    @objc dynamic var  ProsImgIDBackCard: ProsImgIDBackCard?
    @objc dynamic var  ProsImgFirma2: ProsImgFirma2?
    var mDP_PromocionPlan: List<ProsDP_PromocionPlan> = List<ProsDP_PromocionPlan>()
    var mCot_PlanServicio: List<ProsCot_PlanServicio> = List<ProsCot_PlanServicio>()
    var ProsNombreProducto: List<ProsNombreProducto> =  List<ProsNombreProducto>()
    var ProsNombreServicioA: List<ProsNombreServicioA> =  List<ProsNombreServicioA>()
    var ProsNombrePromocion: List<ProsNombrePromocion> =  List<ProsNombrePromocion>()
    var ProsNombreServicioInc: List<ProsNombreServicioInc> = List<ProsNombreServicioInc>()
    var ProsNombreServicioIncProductos: List<ProsNombreServicioIncProductos> = List<ProsNombreServicioIncProductos>()
    var ProsPrecioProducto: List<ProsPrecioProducto> = List<ProsPrecioProducto>()
    var ProsPrecioPromocion: List<ProsPrecioPromocion> = List<ProsPrecioPromocion>()
    var ProsPrecioServicioA: List<ProsPrecioServicioA> = List<ProsPrecioServicioA>()
    var ProsCantidadServA: List<ProsCantidadServA> = List<ProsCantidadServA>()
    var ProsNombreTemp: List<ProsNombreTemp> = List<ProsNombreTemp>()
    @objc dynamic var  ProsCot_SitioPlan: ProsCot_SitioPlan?

}
