//
//  ProsCot_PlanServicio.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 18/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import UIKit

import RealmSwift

class ProsCot_PlanServicio: Object {

    override static func primaryKey() -> String? {
        return "uui"
    }

    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  DP_PlanServicio: String = ""
    @objc dynamic var  NombreServicio: String = ""
    @objc dynamic var  EsServicioAdicional: String = ""
    @objc dynamic var  DP_PromocionPlan: String = ""
    @objc dynamic var  Tipo: String = ""
    var mProsCot_ServicioProducto: List<ProsCot_ServicioProducto> = List<ProsCot_ServicioProducto>()
}

// SUMMARY SCREEN
class ProsNombreServicioA: Object {
    override static func primaryKey() -> String? {
        return "uui"
    }
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  nombreServicioA: String = ""
}
class ProsNombreProducto: Object {
    override static func primaryKey() -> String? {
        return "uui"
    }
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  nombreProd: String = ""
}
class ProsNombrePromocion: Object {
    override static func primaryKey() -> String? {
        return "uui"
    }
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  nombrePromo: String = ""
}
class ProsNombreServicioInc: Object {
    override static func primaryKey() -> String? {
        return "uui"
    }
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  nombreServInc: String = ""
}
class ProsNombreServicioIncProductos: Object {
    override static func primaryKey() -> String? {
        return "uui"
    }
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  nombreServIncProductos: String = ""
}
class ProsNombreTemp: Object {
    override static func primaryKey() -> String? {
        return "uui"
    }
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  nombreTemp: String = ""
}
class ProsPrecioProducto: Object {
    override static func primaryKey() -> String? {
        return "uui"
    }
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  precioProducto: String = ""
}
class ProsCantidadServA: Object {
    override static func primaryKey() -> String? {
        return "uui"
    }
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  cantidad: String = ""
}
class ProsPrecioPromocion: Object {
    override static func primaryKey() -> String? {
        return "uui"
    }
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  precioPromocion: String = ""
}
class ProsPrecioServicioA: Object {
    override static func primaryKey() -> String? {
        return "uui"
    }
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  precioServicioA: String = ""
}
