//
//  BandejaEnviadas.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 12/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper
import RealmSwift

class BandejaEnviadas: Object {

    override static func primaryKey() -> String? {
        return "uui"
    }

    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  NombreCompleto: String = ""
    @objc dynamic var  Direccion: String = ""
    @objc dynamic var  Paquete = ""

}
