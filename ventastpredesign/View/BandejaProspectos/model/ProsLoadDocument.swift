//
//  ProsLoadDocument.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 17/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper
import RealmSwift

class ProsLoadDocument: NSObject {
}

class ProsImgContrato: Object {
    override static func primaryKey() -> String? {
        return "uui"
    }
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  contrato: String = ""
    @objc dynamic var  contratoThumbnail: String = ""
    @objc dynamic var  nombre = ""
    @objc dynamic var  tipo = ""
}
class ProsImgIDFront: Object {
    override static func primaryKey() -> String? {
        return "uui"
    }
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  idFront: String = ""
    @objc dynamic var  idFrontThumbnail: String = ""
    @objc dynamic var  nombre = ""
    @objc dynamic var  tipo = ""
}
class ProsImgIDBack: Object {
    override static func primaryKey() -> String? {
        return "uui"
    }
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  idBack: String = ""
    @objc dynamic var  idBackThumbnail: String = ""
    @objc dynamic var  nombre = ""
    @objc dynamic var  tipo = ""
}
class ProsImgComprobante: Object {
    override static func primaryKey() -> String? {
        return "uui"
    }
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  comprobante: String = ""
    @objc dynamic var  comprobanteThumbnail: String = ""
    @objc dynamic var  nombre = ""
    @objc dynamic var  tipo = ""

}
class ProsImgFirma: Object {
    override static func primaryKey() -> String? {
        return "uui"
    }
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  firma: String = ""
    @objc dynamic var  firmaThumbnail: String = ""
    @objc dynamic var  nombre = ""
    @objc dynamic var  tipo = ""
}
class ProsImgRFC: Object {
    override static func primaryKey() -> String? {
        return "uui"
    }
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  rfc: String = ""
    @objc dynamic var  rfcThumbnail: String = ""
    @objc dynamic var  nombre = ""
    @objc dynamic var  tipo = ""
}
class ProsImgActa1: Object {
    override static func primaryKey() -> String? {
        return "uui"
    }
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  acta1: String = ""
    @objc dynamic var  acta1Thumbnail: String = ""
    @objc dynamic var  nombre = ""
    @objc dynamic var  tipo = ""
}
class ProsImgActa2: Object {
    override static func primaryKey() -> String? {
        return "uui"
    }
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  acta2: String = ""
    @objc dynamic var  acta2Thumbnail: String = ""
    @objc dynamic var  nombre = ""
    @objc dynamic var  tipo = ""
}
class ProsImgActa3: Object {
    override static func primaryKey() -> String? {
        return "uui"
    }
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  acta3: String = ""
    @objc dynamic var  acta3Thumbnail: String = ""
    @objc dynamic var  nombre = ""
    @objc dynamic var  tipo = ""
}
class ProsImgIDFrontCard: Object {
    override static func primaryKey() -> String? {
        return "uui"
    }
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  idFront: String = ""
    @objc dynamic var  idFrontThumbnail: String = ""
    @objc dynamic var  nombre = ""
    @objc dynamic var  tipo = ""
}
class ProsImgIDBackCard: Object {
    override static func primaryKey() -> String? {
        return "uui"
    }
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  idBack: String = ""
    @objc dynamic var  idBackThumbnail: String = ""
    @objc dynamic var  nombre = ""
    @objc dynamic var  tipo = ""
}
class ProsImgFirma2: Object {
    override static func primaryKey() -> String? {
        return "uui"
    }
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  firma2: String = ""
    @objc dynamic var  firma2Thumbnail: String = ""
    @objc dynamic var  nombre = ""
    @objc dynamic var  tipo = ""
}
