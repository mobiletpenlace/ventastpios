//
//  ProsVendedor.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 17/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import UIKit

import ObjectMapper
import RealmSwift

class ProsVendedor: Object, Mappable {

    func mapping(map: Map) {
        AprobarVentaExpress <- map["AprobarVentaExpress"]
        Canal <- map["Canal"]
        ModeloCelular <- map["ModeloCelular"]
        Empleado_Distribuidor <- map["Empleado_Distribuidor"]
        IdDispositivo <- map["IdDispositivo"]
        IdDistrital <- map["IdDistrital"]
        IdEmpleado <- map["IdEmpleado"]
        IdEmpleadoDistrital <- map["IdEmpleadoDistrital"]
        IMEI <- map["IMEI"]
        OrigenApp <- map["OrigenApp"]
        EsComisionable <- map["EsComisionable"]
        Usuario_Logueado <- map["Usuario_Logueado"]
        VentaExpress <- map["VentaExpress"]
        SubCanal <- map["SubCanal"]
        FolioContrato <- map["FolioContrato"]
    }
    public required convenience init?(map: Map) {
        self.init()
    }
    override static func primaryKey() -> String? {
        return "uui"
    }
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  AprobarVentaExpress: String = ""
    @objc dynamic var  Canal: String = ""
    @objc dynamic var  ModeloCelular: String = ""
    @objc dynamic var  Empleado_Distribuidor: String = ""
    @objc dynamic var  IdDispositivo: String = ""
    @objc dynamic var  IdDistrital: String = ""
    @objc dynamic var  IdEmpleado: String = ""
    @objc dynamic var  IdEmpleadoDistrital: String = ""
    @objc dynamic var  IMEI: String = ""
    @objc dynamic var  OrigenApp: String = ""
    @objc dynamic var  EsComisionable: String = ""
    @objc dynamic var  Usuario_Logueado: String = ""
    @objc dynamic var  VentaExpress: String = ""
    @objc dynamic var  SubCanal: String = ""
    @objc dynamic var  FolioContrato: String = ""
}
