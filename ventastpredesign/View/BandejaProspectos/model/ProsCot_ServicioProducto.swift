//
//  ProsCot_ServicioProducto.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 18/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import UIKit

import ObjectMapper
import RealmSwift

class ProsCot_ServicioProducto: Object {

    override static func primaryKey() -> String? {
        return "uui"
    }

    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  PrecioUnitarioBase: String = ""
    @objc dynamic var  DP_ServicioProducto: String = ""
    @objc dynamic var  DP_PromocionPlan: String = ""
    @objc dynamic var  DP_PromocionServicioProducto: String = ""
    @objc dynamic var  ProductoPadre: String = ""
    @objc dynamic var  Descuento: String = ""
    @objc dynamic var  EsProductoAdicional: String = ""
    @objc dynamic var  EsDescuento: String = ""
    @objc dynamic var  EsProntoPago: String = ""
    @objc dynamic var  EsCargoUnico: String = ""
    @objc dynamic var  MesInicio: String = ""
    @objc dynamic var  NombreProducto: String = ""
    @objc dynamic var  Cantidad: String = ""
    @objc dynamic var  Impuesto1: String = ""
    @objc dynamic var  Impuesto2: String = ""
    @objc dynamic var  TipoProducto: String = ""
    @objc dynamic var  PrecioUnitario: String = ""
    @objc dynamic var  PrecioUnitario_ProntoPago: String = ""
    @objc dynamic var  AplicaEstimuloFiscal: String = ""
    @objc dynamic var  IVA_Fronterizo: String = ""

}
