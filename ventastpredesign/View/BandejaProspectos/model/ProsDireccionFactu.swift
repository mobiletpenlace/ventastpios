//
//  ProsDireccionFactu.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 17/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import UIKit

import ObjectMapper
import RealmSwift

class ProsDireccionFactu: Object, Mappable {

    func mapping(map: Map) {
        Ciudad <- map["Ciudad"]
        Colonia <- map["Colonia"]
        DelegacionMunicipio <- map["DelegacionMunicipio"]
        NumeroExterior <- map["NumeroExterior"]
        NumeroInterior <- map["NumeroInterior"]
        MismaDireccionInstalacion <- map["MismaDireccionInstalacion"]
        Estado <- map["Estado"]
        Calle <- map["Calle"]
        FolioContrato <- map["FolioContrato"]
        CodigoPostal <- map["CodigoPostal"]
        ColoniaCP <- map["ColoniaCP"]

    }

    public required convenience init?(map: Map) {
        self.init()
    }

    override static func primaryKey() -> String? {
        return "uui"
    }

    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  Ciudad: String = ""
    @objc dynamic var  Colonia: String = ""
    @objc dynamic var  DelegacionMunicipio: String = ""
    @objc dynamic var  NumeroExterior: String = ""
    @objc dynamic var  NumeroInterior: String = ""
    @objc dynamic var  MismaDireccionInstalacion: String = ""
    @objc dynamic var  Estado: String = ""
    @objc dynamic var  Calle: String = ""
    @objc dynamic var  FolioContrato: String = ""
    @objc dynamic var  CodigoPostal: String = ""
    @objc dynamic var  ColoniaCP: String = ""

}
