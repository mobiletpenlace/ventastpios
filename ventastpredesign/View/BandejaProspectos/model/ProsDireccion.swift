//
//  ProsDireccion.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 17/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import UIKit

import ObjectMapper
import RealmSwift

class ProsDireccion: Object, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        AutorizacionSinCobertura <- map["AutorizacionSinCobertura"]
        EntreCalles <- map["EntreCalles"]
        Edificio <- map["Edificio"]
        CategoryService <- map["CategoryService"]
        Ciudad <- map["Ciudad"]
        Cluster <- map["Cluster"]
        Colonia <- map["Colonia"]
        Delegacion <- map["Delegacion"]
        Distrito <- map["Distrito"]
        Factibilidad <- map["Factibilidad"]
        Factible <- map["Factible"]
        RegionId <- map["RegionId"]
        Latitude <- map["Latitude"]
        Longitude <- map["Longitude"]
        NumeroExterior <- map["NumeroExterior"]
        NumeroInterior <- map["NumeroInterior"]
        Plaza <- map["Plaza"]
        Region <- map["Region"]
        Estado <- map["Estado"]
        Calle <- map["Calle"]
        ReferenciaUrbana <- map["ReferenciaUrbana"]
        CodigoPostal <- map["CodigoPostal"]
        zona <- map["zona"]

    }
    override static func primaryKey() -> String? {
        return "uui"
    }
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  AutorizacionSinCobertura: String = ""
    @objc dynamic var  EntreCalles: String = ""
    @objc dynamic var  Edificio: String = ""
    @objc dynamic var  CategoryService: String = ""
    @objc dynamic var  Ciudad: String = ""
    @objc dynamic var  Cluster: String = ""
    @objc dynamic var  Colonia: String = ""
    @objc dynamic var  Delegacion: String = ""
    @objc dynamic var  Distrito: String = ""
    @objc dynamic var  Factibilidad: String = ""
    @objc dynamic var  Factible: String = ""
    @objc dynamic var  RegionId: String = ""
    @objc dynamic var  Latitude: String = ""
    @objc dynamic var  Longitude: String = ""
    @objc dynamic var  NumeroExterior: String = ""
    @objc dynamic var  NumeroInterior: String = ""
    @objc dynamic var  Plaza: String = ""
    @objc dynamic var  Region: String = ""
    @objc dynamic var  Estado: String = ""
    @objc dynamic var  Calle: String = ""
    @objc dynamic var  ReferenciaUrbana: String = ""
    @objc dynamic var  CodigoPostal: String = ""
    @objc dynamic var  zona: String = ""

}
