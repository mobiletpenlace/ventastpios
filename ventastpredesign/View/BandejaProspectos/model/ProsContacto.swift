//
//  ProsContacto.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 17/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import UIKit

import ObjectMapper
import RealmSwift

class ProsContacto: Object, Mappable {

    func mapping(map: Map) {
        FechaNacimiento <- map["FechaNacimiento"]
        Celular <- map["Celular"]
        contactMeans <- map["contactMeans"]
        CorreoElectronico <- map["CorreoElectronico"]
        OtroCorreoElectronico <- map["  OtroCorreoElectronico"]
        ApellidoPaterno <- map["ApellidoPaterno"]
        MSociales <- map["Sociales"]
        ApellidoMaterno <- map["ApellidoMaterno"]
        Nombre <- map["Nombre"]
        OtroTelefono <- map["OtroTelefono"]
        Telefono <- map["Telefono"]
        RFC <- map["RFC"]
        RazonSocial <- map["RazonSocial"]
        TipoPersona <- map["TipoPersona"]

    }

    public required convenience init?(map: Map) {
        self.init()
    }
    override static func primaryKey() -> String? {
        return "uui"
    }
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  FechaNacimiento: String = ""
    @objc dynamic var  Celular: String = ""
    @objc dynamic var  contactMeans: String = ""
    @objc dynamic var  CorreoElectronico: String = ""
    @objc dynamic var  OtroCorreoElectronico: String = ""
    @objc dynamic var  ApellidoPaterno: String = ""
    @objc dynamic var  ApellidoMaterno: String = ""
    @objc dynamic var  Nombre: String = ""
    @objc dynamic var  OtroTelefono: String = ""
    @objc dynamic var  Telefono: String = ""
    @objc dynamic var  RFC: String = ""
    @objc dynamic var  RazonSocial: String = ""
    @objc dynamic var  TipoPersona: String = ""
    var MSociales: List<ProsSociales> = List<ProsSociales>()

}
