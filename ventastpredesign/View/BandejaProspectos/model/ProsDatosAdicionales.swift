//
//  ProsDatosAdicionales.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 17/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import UIKit

import ObjectMapper
import RealmSwift

class ProsDatosAdicionales: Object, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        IdentificacionOficial <- map["IdentificacionOficial"]
        TipoIdentificacion <- map["TipoIdentificacion"]
    }

    override static func primaryKey() -> String? {
        return "uui"
    }
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  IdentificacionOficial: String = ""
    @objc dynamic var  TipoIdentificacion: String = ""
}
