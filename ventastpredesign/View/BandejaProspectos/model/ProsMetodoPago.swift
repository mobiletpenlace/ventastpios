//
//  ProsMetodoPago.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 17/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import UIKit

import ObjectMapper
import RealmSwift

class ProsMetodoPago: Object, Mappable {

    func mapping(map: Map) {
        Metodo <- map["Metodo"]
        NumeroTarjeta <- map["NumeroTarjeta"]
        VencimientoAnio <- map["VencimientoAnio"]
        VencimientoMes <- map["VencimientoMes"]
        ApellidoPaternoTitular <- map["ApellidoPaternoTitular"]
        ApellidoMaternoTitular <- map["ApellidoMaternoTitular"]
        NombreTitutlar <- map["NombreTitular"]
        TipoTarjeta <- map["TipoTarjeta"]
    }

    public required convenience init?(map: Map) {
        self.init()
    }

    override static func primaryKey() -> String? {
        return "uui"
    }

    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  NumeroTarjeta: String = ""
    @objc dynamic var  VencimientoAnio: String = ""
    @objc dynamic var  VencimientoMes: String = ""
    @objc dynamic var  ApellidoPaternoTitular: String = ""
    @objc dynamic var  ApellidoMaternoTitular: String = ""
    @objc dynamic var  Metodo: String = ""
    @objc dynamic var  NombreTitutlar: String = ""
    @objc dynamic var  TipoTarjeta: String = ""

}
