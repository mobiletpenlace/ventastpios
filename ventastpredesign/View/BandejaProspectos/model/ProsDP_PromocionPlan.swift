//
//  ProsDP_PromocionPlan.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 18/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper
import RealmSwift

class ProsDP_PromocionPlan: Object, Mappable {

    func mapping(map: Map) {
        Id <- map["Id"]

    }
    public required convenience init?(map: Map) {
        self.init()
    }

    override static func primaryKey() -> String? {
        return "uui"
    }

    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  Id: String = ""

}
