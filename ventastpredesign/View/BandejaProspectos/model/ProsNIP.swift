//
//  ProsNIP.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 7/9/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper
import RealmSwift

class ProsNIP: Object, Mappable {

    func mapping(map: Map) {
        NIPGenerado <- map["NIPGenerado"]
        NIPSolicitado <- map["NIPSolicitado"]
    }

    public required convenience init?(map: Map) {
        self.init()
    }

    override static func primaryKey() -> String? {
        return "uui"
    }

    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  NIPGenerado: String = ""
    @objc dynamic var  NIPSolicitado: String = ""

}
