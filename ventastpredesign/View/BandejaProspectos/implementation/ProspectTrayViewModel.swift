//
//  ProspectTrayViewModel.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 18/05/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import Foundation

protocol ProspectTrayObserver {
    func succesGetProspects(oportunidades: [Oportunidad])
    func onErrorEmploye()
}

class ProspectTrayViewModel: BaseViewModel, ProspectTrayModelObserver {
    var observer: ProspectTrayObserver?
    var model: ProspectTrayModel!
    var empleado: Empleado?

    override init(view: BaseVentasView) {
        super.init(view: view)
        model = ProspectTrayModel(observer: self)
        model.atachModel(viewModel: self)
    }

    func atachView(_ observer: ProspectTrayObserver) {
        self.observer = observer
    }

    func getProspects() {
        let mOportunidadesRequest: OportunidadesRequest = OportunidadesRequest()
        AES128.shared.setKey(key: statics.key)
        if RealManager.findFirst(object: Empleado.self) != nil {
            empleado = Empleado(value: RealManager.findFirst(object: Empleado.self)!)
            mOportunidadesRequest.idEmpleado = empleado?.Id// AES128.shared.code(plainText: empleado?.Id ?? "")
            Logger.println("id      -     \(empleado?.Id)")
            model.getProspectsWS(mOportunidadesRequest)
        } else {
            observer?.onErrorEmploye()
        }
    }

    func succesProspect(oportunidades: [Oportunidad]) {
        observer?.succesGetProspects(oportunidades: oportunidades)
        view.hideLoading()
    }
}
