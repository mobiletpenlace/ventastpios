//
//  ProspectTrayModel.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 18/05/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import RealmSwift

protocol ProspectTrayModelObserver {
    func succesProspect(oportunidades: [Oportunidad])
}

class ProspectTrayModel: BaseModel {

    var viewModel: ProspectTrayModelObserver?
    var server: ServerDataManager2<OportunidadResponse>?
    var url = AppDelegate.API_SALESFORCE + ApiDefinition.API_CONSULTA_OPORTUNIDADES

    init (observer: BaseViewModelObserver) {
        server = ServerDataManager2<OportunidadResponse>()
        super.init(observerVM: observer)
    }

    func atachModel(viewModel: ProspectTrayModelObserver) {
        self.viewModel = viewModel
    }

    func getProspectsWS(_ request: OportunidadesRequest) {
        url = AppDelegate.API_SALESFORCE + ApiDefinition.API_CONSULTA_OPORTUNIDADES
        server?.setValues(requestUrl: url, delegate: self, headerType: .headersSalesforce)
        server?.request(requestModel: request)
    }

    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        let mOportunidadesResponse = response as! OportunidadResponse

        if mOportunidadesResponse.result == "0" {
            AES128.shared.setKey(key: statics.key)
            let json: String = AES128.shared.decode(coded: mOportunidadesResponse.oportunidades!)!
            var oportunidades: [Oportunidad] = [Oportunidad].init(JSONString: json)!
            Logger.println("\(AES128.shared.decode(coded: mOportunidadesResponse.oportunidades!))")
            viewModel?.succesProspect(oportunidades: oportunidades)
        }
    }

}
