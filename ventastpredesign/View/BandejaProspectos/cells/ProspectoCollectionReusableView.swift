//
//  ProspectoCollectionReusableView.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 12/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

// EJEMPLO A SEGUIR PARA DETECTAR PRESION DE BOTON

import UIKit

class ProspectoCollectionReusableView: UICollectionReusableView {
    @IBOutlet weak var mButonResuable: UIButton!
    @IBOutlet weak var mViewProspecto: UIView!
    @IBOutlet weak var mNombre: UILabel!
    @IBOutlet weak var mEstatus: UILabel!
    var mOportunidad: Oportunidad?
    var view: BaseVentasView?

    override func awakeFromNib() {
        super.awakeFromNib()

    }

    func setData(_ op: Oportunidad, _ view: BaseVentasView) {
        mOportunidad = op
        self.view = view
        mNombre.text = mOportunidad?.nombre
        mEstatus.text = mOportunidad?.etapa
    }

    @IBAction func onClick(_ sender: Any) {

        if mOportunidad?.etapa == "Propuesta" {
            let viewAlert: UIStoryboard = UIStoryboard(name: "ProspectInfoAlert", bundle: nil)
            let viewAlertVC = viewAlert.instantiateViewController(withIdentifier: "ProspectInfoAlert") as! ProspectInfoAlertVC
            viewAlertVC.op = mOportunidad
                viewAlertVC.providesPresentationContextTransitionStyle = true
                viewAlertVC.definesPresentationContext = true
                viewAlertVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                viewAlertVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            view!.present(viewAlertVC, animated: false, completion: nil)
            // Constants.Alert(title: "Alerta", body: "Tu oportunidad esta en fase de propuesta", type: type.Normal, viewC: view!)
        } else {
            Constants.Alert(title: "Alerta", body: "Tu oportunidad no esta en fase de propuesta", type: type.Error, viewC: view!)
        }
        Logger.println("Oportunidad     -   \(String(describing: mOportunidad?.nombre))")
    }

}
