//
//  Oportunidad.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 18/05/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper
import RealmSwift

class Oportunidad: NSObject, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        pais <- map["paisOpp"]
        numeroOpp <- map["numeroOpp"]
        numeroCuenta <- map["numeroCuenta"]
        nombre <- map["nombreOpp"]
        id <- map["idOpp"]
        fecha <- map["fechaCreacionOpp"]
        etapa <- map["etapaOpp"]
        subcanalVenta <- map["subcanalVenta"]
        celular <- map["celular"]
        canalVenta <- map["canalVenta"]

    }

    var  numeroOpp: String = ""
    var  numeroCuenta: String = ""
    var  pais: String = ""
    var  nombre: String = ""
    var  id: String = ""
    var  fecha: String = ""
    var  etapa: String = ""
    var subcanalVenta: String = ""
    var celular: String = ""
    var canalVenta: String = ""

}
