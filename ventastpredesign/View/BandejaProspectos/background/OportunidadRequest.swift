//
//  OportunidadesRequest.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 19/05/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

public class OportunidadesRequest: AlfaRequest {
    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override public func mapping(map: Map) {
        idEmpleado <- map["idEmpleado"]
    }

    var idEmpleado: String?
}
