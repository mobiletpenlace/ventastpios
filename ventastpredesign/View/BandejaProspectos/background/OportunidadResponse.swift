//
//  OportunidadSLF.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 18/05/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class OportunidadResponse: BaseResponse {

    public required convenience init?(map: Map) {
        self.init()
    }

    override func mapping(map: Map) {
        resultDescription <- map["resultDescription"]
        result <- map["result"]
        oportunidades <- map["oportunidades"]
    }

    var oportunidades: String?

}
