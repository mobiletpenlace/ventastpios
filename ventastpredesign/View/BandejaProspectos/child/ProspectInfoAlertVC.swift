//
//  ProspectInfoAlertVC.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 25/05/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class ProspectInfoAlertVC: UIViewController {

    @IBOutlet weak var mIdOportunidad: UILabel!
    @IBOutlet weak var mNombre: UILabel!
    @IBOutlet weak var mFecha: UILabel!
    @IBOutlet weak var mFase: UILabel!

    var op: Oportunidad?

    override func viewDidLoad() {
        super.viewDidLoad()
        let fecha: String = op!.fecha
        let separaciones: [String] = fecha.split(character: "T")
        mIdOportunidad.text = op?.id
        mNombre.text = op?.nombre
        mFecha.text = separaciones[0]
        mFase.text = op?.etapa
    }

    @IBAction func CancelAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }

    @IBAction func CargaOpAction(_ sender: Any) {
        if RealManager.findFirst(object: Sitio.self) != nil {
            RealManager.deleteAll(object: Sitio.self)
        }
        if RealManager.findFirst(object: NProspecto.self) != nil {
            RealManager.deleteAll(object: Sitio.self)
        }
        if RealManager.findFirst(object: ObjetoModelado.self) != nil {
            RealManager.deleteAll(object: ObjetoModelado.self)
        }
        /*if RealManager.findFirst(object: InfoAdicionalesCreacion.self) != nil {
            RealManager.deleteAll(object: InfoAdicionalesCreacion.self)
        }*/
        if TPAddonsSelectedShared.shared.findAddonsPrev() != nil {
            TPAddonsSelectedShared.shared.cleanAddonsCreacion()
        }
        if RealManager.findFirst(object: ProductosAdd.self) != nil {
            RealManager.deleteAll(object: ProductosAdd.self)
        }
        if RealManager.findFirst(object: PromocionesAdd.self) != nil {
            RealManager.deleteAll(object: PromocionesAdd.self)
        }
        if RealManager.findFirst(object: ServiciosAdd.self) != nil {
            RealManager.deleteAll(object: ServiciosAdd.self)
        }
        if RealManager.findFirst(object: ProductosCreacion.self) != nil {
            RealManager.deleteAll(object: ProductosCreacion.self)
        }
        if RealManager.findFirst(object: PromocionesCreacion.self) != nil {
            RealManager.deleteAll(object: PromocionesCreacion.self)
        }
        if RealManager.findFirst(object: ServiciosCreacion.self) != nil {
            RealManager.deleteAll(object: ServiciosCreacion.self)
        }
        if RealManager.findFirst(object: ImgComprobante.self) != nil {
            RealManager.deleteAll(object: ImgComprobante.self)
        }
        if RealManager.findFirst(object: ImgContrato.self) != nil {
            RealManager.deleteAll(object: ImgContrato.self)
        }
        if RealManager.findFirst(object: ImgIDFront.self) != nil {
            RealManager.deleteAll(object: ImgIDFront.self)
        }
        if RealManager.findFirst(object: ImgIDBack.self) != nil {
            RealManager.deleteAll(object: ImgIDBack.self)
        }
        if RealManager.findFirst(object: ImgIDFrontCard.self) != nil {
            RealManager.deleteAll(object: ImgIDFrontCard.self)
        }
        if RealManager.findFirst(object: ImgIDBackCard.self) != nil {
            RealManager.deleteAll(object: ImgIDBackCard.self)
        }
        if RealManager.findFirst(object: ImgRFC.self) != nil {
            RealManager.deleteAll(object: ImgRFC.self)
        }
        if RealManager.findFirst(object: ImgActa1.self) != nil {
           RealManager.deleteAll(object: ImgActa1.self)
        }
        if RealManager.findFirst(object: ImgActa2.self) != nil {
            RealManager.deleteAll(object: ImgActa2.self)
        }
        if RealManager.findFirst(object: ImgActa3.self) != nil {
            RealManager.deleteAll(object: ImgActa3.self)
        }
        if RealManager.findFirst(object: ImgFirma.self) != nil {
           RealManager.deleteAll(object: ImgFirma.self)
        }
        if RealManager.findFirst(object: ImgFirma2.self) != nil {
            RealManager.deleteAll(object: ImgFirma2.self)
        }
        if RealManager.findFirst(object: PuntosControl.self) != nil {
            RealManager.deleteAll(object: PuntosControl.self)
        }
        if RealManager.findFirst(object: BanderasObject.self) != nil {
            RealManager.deleteAll(object: BanderasObject.self)
        }

        let B = self.presentingViewController
               self.dismiss(
                   animated: false, completion: {
                       B?.dismiss(animated: false, completion: nil)
                    SwiftEventBus.post("CargaOp", sender: self.op?.id)
               })

    }

}
