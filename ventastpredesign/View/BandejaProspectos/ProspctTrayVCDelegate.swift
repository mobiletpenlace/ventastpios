//
//  ProspctTrayVCDelegate.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 19/05/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

extension ProspectsTrayViewController: ProspectTrayObserver {
    func onErrorEmploye() {
    }

    func succesGetProspects(oportunidades: [Oportunidad]) {
        self.mOportunidadesBase = oportunidades
        self.mOportunidades = oportunidades
        loadDataPropuestas(mOportunidades)
    }

}
