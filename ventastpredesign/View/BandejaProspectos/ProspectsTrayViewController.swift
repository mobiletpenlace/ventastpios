//
//  ProspectsTrayViewController.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 11/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import RealmSwift

import SwiftEventBus

class ProspectsTrayViewController: BaseVentasView {

    @IBOutlet weak var mSerarchBar: UITextField!
    @IBOutlet weak var ScrollView: UIScrollView!
    var mPrspectTrayViewModel: ProspectTrayViewModel!
    let SelectColor = UIColor(red: 108/255, green: 36/255, blue: 251/255, alpha: 1.0 )
    var mOportunidadesBase: [Oportunidad] = []
    var mOportunidades: [Oportunidad] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        mSerarchBar.delegate = self
        mPrspectTrayViewModel = ProspectTrayViewModel(view: self)
        mPrspectTrayViewModel.atachView(self)
        mPrspectTrayViewModel.getProspects()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func ActionBack(_ sender: Any) {
       Constants.Back(viewC: self)
    }

    func loadDataPropuestas(_ oportunidades: [Oportunidad]) {
        var contador = 0
        contador =  oportunidades.count
        ScrollView.contentSize = CGSize(width: 100, height: CGFloat(70) * CGFloat(contador))
        for index in 0 ..< contador {
            if let view = Bundle.main.loadNibNamed("ProspectoCollectionReusableView", owner: self, options: nil)?.first as? ProspectoCollectionReusableView {
                view.setData(oportunidades[index], self)
                ScrollView.addSubview(view)
                view.frame.size.height = CGFloat(70)
                view.frame.size.width = self.view.bounds.size.width
                view.frame.origin.y = CGFloat(index) * CGFloat(70)
            }
        }
    }

    func rellenaPlanes(_ oportunidades: [Oportunidad]) {
        var contador = 0
        contador = oportunidades.count / 2
        let width = self.view.bounds.size.width / 2
        ScrollView.contentSize = CGSize(width: width * 2, height: CGFloat(width) * CGFloat(contador))
        for index in 1 ..< contador + 1 {
           if let view1 = Bundle.main.loadNibNamed("ProspectoCollectionReusableView", owner: self, options: nil)?.first as? ProspectoCollectionReusableView {
            view1.setData(oportunidades[(index * 2) - 1], self)
            ScrollView.addSubview(view)
            view.frame.size.height = width
            view.frame.size.width = width
            view.frame.origin.y = CGFloat(index) * CGFloat(width)
            view.frame.origin.x = CGFloat(20)
            }
            if let view2 = Bundle.main.loadNibNamed("ProspectoCollectionReusableView", owner: self, options: nil)?.first as? ProspectoCollectionReusableView {
             view2.setData(oportunidades[(index * 2)], self)
             ScrollView.addSubview(view)
             view.frame.size.height = width
             view.frame.size.width = width
             view.frame.origin.y = CGFloat(index) * CGFloat(width)
             view.frame.origin.x = CGFloat(40) + CGFloat(width)
             }
        }
    }

    func delete() {
        let subViews = self.ScrollView.subviews
        for subview in subViews {
            subview.removeFromSuperview()
        }
        ScrollView.contentOffset.y = 0
    }

    func nextPage() {
        let viewAlert: UIStoryboard = UIStoryboard(name: "StartSaleViewController", bundle: nil)
        let viewAlertVC = viewAlert.instantiateViewController(withIdentifier: "StartSaleViewController")as! StartSaleViewController
        viewAlertVC.isFirst = true
        self.present(viewAlertVC, animated: false, completion: {})
    }

    func filtrado(_ text: String) {
        delete()
        mOportunidades = []
        for oportunidad in mOportunidadesBase {
            if oportunidad.nombre.uppercased().trim().contains(string: text.uppercased()) {
                mOportunidades.append(oportunidad)
            }
        }
        loadDataPropuestas(mOportunidades)
    }

    @IBAction func deleteSearchAction(_ sender: Any) {
        mSerarchBar.text = ""
        delete()
        loadDataPropuestas(mOportunidadesBase)
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.text?.trim().count)! > 0 {
            filtrado((textField.text?.trim())!)
        }
        if (textField.text?.trim().count)! < 1 {
            delete()
            mOportunidades = mOportunidadesBase
            loadDataPropuestas(mOportunidades)
        }
        return true
    }
}
