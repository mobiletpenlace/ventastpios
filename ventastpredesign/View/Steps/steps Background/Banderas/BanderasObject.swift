//
//  BanderasObject.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 22/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper
import RealmSwift

class BanderasObject: Object {

    public required convenience init?(map: Map) {
        self.init()
    }

    override static func primaryKey() -> String? {
        return "uui"
    }

    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  aceptaTerminos: Bool = true
    @objc dynamic var  aceptaPrivacidad: Bool = true
    @objc dynamic var  aceptaBuro: Bool = true
    @objc dynamic var  aceptaEstadistica: Bool = true
    @objc dynamic var  aceptaTerceros: Bool = true
    @objc dynamic var  aceptaDerechosMinimos: Bool = true
    @objc dynamic var  aceptaPagoRecurrente: Bool = false
    @objc dynamic var  direccionFacturacion: Bool = true
    @objc dynamic var  isIdentificacionComprobante: Bool = false
    @objc dynamic var  firma1: Bool = false
    @objc dynamic var  firma2: Bool = false
    @objc dynamic var  identificacionFrente: Bool = false
    @objc dynamic var  identificacionTrasera: Bool = false
    @objc dynamic var  mismoPropietario: Bool = false
    @objc dynamic var  identificacionTarjetaFrente: Bool = false
    @objc dynamic var  identificacionTarjetaTrasera: Bool = false
    @objc dynamic var  comprobante: Bool = false

}
