//
//  ConsultaConvivenciaResponse.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 18/06/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class ConsultaConvivenciaResponse: BaseResponse {

    required init?(map: Map) {

    }

    override func mapping(map: Map) {
        resultDescription <- map["resultDescription"]
        result <- map["result"]
    }

}
