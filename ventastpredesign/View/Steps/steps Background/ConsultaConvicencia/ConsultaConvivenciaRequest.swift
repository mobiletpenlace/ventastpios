//
//  ConsultaConvivenciaRequest.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 18/06/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

public class ConsultaConvivenciaRequest: BaseRequest {
    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override public func mapping(map: Map) {
        cuenta <- map["numCuentaBRM"]
        idPlan <- map["idPlan"]
        promocionesNew <- map["promocionesSeleccionadasNew"]
        promocionesOld <- map["promocionesSeleccionadasOld"]
    }

    var cuenta: String?
    var idPlan: String?
    var promocionesNew: [String] = []
    var promocionesOld: [String] = []
}
