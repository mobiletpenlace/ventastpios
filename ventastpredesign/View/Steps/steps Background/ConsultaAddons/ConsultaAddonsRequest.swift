//
//  RequestConsultaAddons.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 29/05/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

public class ConsultaAddonsRequest: BaseRequest {
    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override public func mapping(map: Map) {
        idPlan <- map["idPlan"]
        info <- map["info"]
    }

    var idPlan: String?
    var info: InfoAddonsRequest = InfoAddonsRequest()
}
