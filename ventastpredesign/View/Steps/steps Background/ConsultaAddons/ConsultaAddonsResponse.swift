//
//  ConsultaAddonsResponse.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 29/05/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class ConsultaAddonsResponse: BaseResponse {

    required init?(map: Map) {

    }

    override func mapping(map: Map) {
        resultDescription <- map["resultDescription"]
        result <- map["result"]
        infoAddons <- map["infoAddons"]
    }

    var infoAddons: InfoAddons = InfoAddons()

}
