//
//  InfoAddonsRequest.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 29/05/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class InfoAddonsRequest: NSObject, Mappable {
    required init?(map: Map) {

    }

    func mapping(map: Map) {
        canal <- map["canal"]
        canalFront <- map["canalFront"]
        cluster <- map["cluster"]
        colonia <- map["colonia"]
        distrito <- map["distrito"]
        estimulo <- map["estimuloFiscal"]
        metodoPago <- map["metodoPago"]
        plaza <- map["plaza"]
        codigoPostal <- map["codigoPostal"]
        consultaAddons <- map["consultaAddons"]
        consultaPromo <- map["consultaPromo"]
        consultaServ <- map["consultaServ"]
        subcanalVenta <- map["subcanalVenta"]
        subcanal <- map["subcanal"]

    }

    override init() {

    }
    var canal: String?
    var canalFront: String?
    var cluster: String?
    var colonia: String?
    var distrito: String?
    var metodoPago: String?
    var plaza: String?
    var codigoPostal: String?
    var consultaAddons: Bool?
    var consultaPromo: Bool?
    var consultaServ: Bool?
    var subcanalVenta: String?
    var subcanal: String?
    var estimulo: Bool?

}
