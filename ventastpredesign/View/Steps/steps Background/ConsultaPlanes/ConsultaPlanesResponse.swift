//
//  ConsultaPlanesResponse.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 29/05/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class ConsultaPlanesResponse: BaseResponse {

    required init?(map: Map) {

    }

    override func mapping(map: Map) {
        resultDescription <- map["resultDescription"]
        result <- map["result"]
        planesTotalplay <- map["planesTotalplay"]
        serviciosStriming <- map["serviciosStreaming"]
    }

    var planesTotalplay: [Plan] = []
    var serviciosStriming: [ServiciosStriming] = []

}
