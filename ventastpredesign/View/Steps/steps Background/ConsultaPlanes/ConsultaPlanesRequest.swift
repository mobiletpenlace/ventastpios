//
//  ConsultaPlanesRequest.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 29/05/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

public class ConsultaPlanessRequest: BaseRequest {
    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override public func mapping(map: Map) {
        canal <- map["canal"]
        canalFront <- map["canalFront"]
        plazaTP <- map["plazaTP"]
        cluster <- map["cluster"]
        estimuloFiscal <- map["estimuloFiscal"]
        subcanal <- map["subcanal"]
        distrito <- map["distrito"]
    }

    var canal: String?
    var canalFront: String?
    var plazaTP: String?
    var cluster: String?
    var estimuloFiscal: Bool?
    var subcanal: String?
    var distrito: String?
}
