//
//  PuntosControl.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 29/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper
import RealmSwift

class PuntosControl: Object {

    public required convenience init?(map: Map) {
        self.init()
    }

    override static func primaryKey() -> String? {
        return "uui"
    }

    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  existeProspecto: Bool = false
    @objc dynamic var  existeModelado: Bool = false
    @objc dynamic var  existenAddons: Bool = false
    @objc dynamic var  addonsCargados: Bool = false
    @objc dynamic var  documentosCargados: Bool = false
    @objc dynamic var  idOP: String = ""
    @objc dynamic var  idPlan: String = ""
    @objc dynamic var  cuentaBRM: String = ""

}
