//
//  ConsultaOportunidadRequest.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 14/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

public class ConsultaOportunidadRequest: BaseRequest {
    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override public func mapping(map: Map) {
        accion <- map["accion"]
        idOportunidad <- map["idOportunidad"]
    }

    var accion: String?
    var idOportunidad: String?
}
