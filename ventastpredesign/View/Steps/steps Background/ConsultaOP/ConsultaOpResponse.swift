//
//  ConsultaOpResponse.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 14/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class ConsultaOpResponse: BaseResponse {

    required init?(map: Map) {

    }

    override func mapping(map: Map) {
        resultDescription <- map["resultDescription"]
        result <- map["result"]
        infoModelado <- map ["infoModelado"]
        infoEncript <- map ["infoEncript"]
        info <- map["info"]
    }

    var infoModelado: InfoModeladoResponse = InfoModeladoResponse()
    var infoEncript: String = ""
    var info: InfoEncript?

}
