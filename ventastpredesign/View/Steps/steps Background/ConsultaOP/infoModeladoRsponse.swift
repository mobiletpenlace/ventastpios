//
//  infoModeladoRsponse.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 14/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class InfoModeladoResponse: NSObject, Mappable {
    required init?(map: Map) {

    }

    func mapping(map: Map) {
        idPlanSelecciondado <- map["idPlanSelecciondado"]
        serviciosAdd <- map["serviciosAdd"]
        promocionesAdd <- map["promocionesAdd"]
        productosConsulta <- map["productosAdd"]
    }

    override init() {

    }
    var idPlanSelecciondado: String?
    var productosConsulta: ProductosConsulta = ProductosConsulta()
    var promocionesAdd: PromocionesAddRequest = PromocionesAddRequest()
    var serviciosAdd: ServiciosAddResponse = ServiciosAddResponse()

}
