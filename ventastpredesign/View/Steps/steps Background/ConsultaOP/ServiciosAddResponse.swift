//
//  ServiciosAddResponse.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 16/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper
import RealmSwift

class ServiciosAddResponse: NSObject, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        servicios <- map["servicios"]
    }

    var  servicios: [ServiciosCreacionResponse] = []

}
