//
//  InfoEncrirpt.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 14/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import Foundation

import UIKit

import ObjectMapper

class InfoEncript: NSObject, Mappable {
    required init?(map: Map) {

    }

    func mapping(map: Map) {
        sitio <- map["sitio"]
        prospecto <- map["prospecto"]
        referencias <- map["referencias"]
        redesSociales <- map["redesSociales"]
        origen <- map["origen"]
        metodoPago <- map["metodoPago"]
        motivoDeRescate <- map["motivoDeRescate"]
        infoEncuesta <- map["infoEncuesta"]
        docs <- map["docs"]
        direccionFacturacion <- map["direccionFacturacion"]
        datosAdicionales <- map["datosAdicionales"]
        arch <- map["arch"]
    }

    override init() {

    }
    var motivoDeRescate: String?
    var sitio: Sitio = Sitio()
    var prospecto: NProspecto = NProspecto()
    var referencias: String?
    var redesSociales: String?
    var origen: String?
    var metodoPago: String?
    var infoEncuesta: String?
    var docs: Docs?
    var direccionFacturacion: String?
    var datosAdicionales: String?
    var arch: String?

}
