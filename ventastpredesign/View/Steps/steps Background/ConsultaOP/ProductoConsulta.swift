//
//  ProductoConsulta.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 15/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class ProductosConsulta: NSObject, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        productos <- map["adicional"]
    }

    var  productos: [ProductosCreacionRequest] = []

}
