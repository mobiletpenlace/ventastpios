//
//  DatosAdicionalesRealm.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 29/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper
import RealmSwift

class DatosAdicionalesRealm: Object {

    public required convenience init?(map: Map) {
        self.init()
    }

    override static func primaryKey() -> String? {
        return "uui"
    }

    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var nipGenerado: String = ""
    @objc dynamic var nipSolicitado: String = ""
    @objc dynamic var idDispositivo: String = ""
    @objc dynamic var tipoIdentificacion: String = ""
    @objc dynamic var identificacionOficial: String = ""
    @objc dynamic var medioContacto: String = ""
}
