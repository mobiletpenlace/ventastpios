//
//  DatosAdicionalesObject.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 27/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class DatosAdicionalesObject: NSObject, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        nipGenerado <- map["nipGenerado"]
        idDispositivo <- map["idDispositivo"]
        tipoIdentificacion <- map["tipoIdentificacion"]
        nipSolicitado <- map["nipSolicitado"]
        identificacionOficial <- map["identificacionOficial"]
        medioContacto <- map["medioContacto"]
    }

    var nipGenerado: String = ""
    var nipSolicitado: String = ""
    var idDispositivo: String = ""
    var tipoIdentificacion: String = ""
    var identificacionOficial: String = ""
    var medioContacto: String = ""
}
