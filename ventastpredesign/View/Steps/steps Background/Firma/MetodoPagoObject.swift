//
//  MetodoPagoObject.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 27/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class MetodoPagoObject: NSObject, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        digitosTarjeta <- map["digitosTarjeta"]
        mes <- map["mes"]
        anio <- map["anio"]
        nombreTitularTarjeta <- map["nombreTitularTarjeta"]
        apellidoPaternoTitular <- map["apellidoPaternoTitular"]
        apellidoMaternoTitular <- map["apellidoMaternoTitular"]
        metodoPago <- map["metodoPago"]
        tipoTarjeta <- map["tipoTarjeta"]
    }

    var digitosTarjeta: String = ""
    var mes: String = ""
    var anio: String = ""
    var nombreTitularTarjeta: String = ""
    var apellidoPaternoTitular: String = ""
    var apellidoMaternoTitular: String = ""
    var metodoPago: String = ""
    var tipoTarjeta: String = ""
}
