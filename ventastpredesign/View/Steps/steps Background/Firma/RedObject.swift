//
//  RedObject.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 27/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import Foundation
import UIKit

import ObjectMapper

class RedObject: NSObject, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        profileId <- map["profileId"]
        provider <- map["provider"]
    }

    var profileId: String = ""
    var provider: String = ""
}
