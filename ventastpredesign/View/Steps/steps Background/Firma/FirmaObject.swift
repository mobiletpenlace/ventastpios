//
//  FirmaObject.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 27/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class FirmaObject: NSObject, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        datosAdicionales <- map["datosAdicionales"]
        direccionFacturacion <- map["dirFact"]
        metodoPago <- map["metodoPago"]
        infoReferenciasFirma <- map["infoReferenciasFirma"]
        infoRedes <- map["infoRedesSociales"]
    }

    var datosAdicionales: DatosAdicionalesObject = DatosAdicionalesObject()
    var direccionFacturacion: DireccionFaturacionObject = DireccionFaturacionObject()
    var metodoPago: MetodoPagoObject = MetodoPagoObject()
    var infoReferenciasFirma: InfoReferenciaFirmaObject = InfoReferenciaFirmaObject()
    var infoRedes: InfoRedesObject = InfoRedesObject()
}
