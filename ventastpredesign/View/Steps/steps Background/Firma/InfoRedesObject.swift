//
//  InfoRedesObject.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 27/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class InfoRedesObject: NSObject, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        socialPerson <- map["socialPerson"]
    }

    var socialPerson: [RedObject] = []
}
