//
//  DireccionFaturacionObject.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 27/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class DireccionFaturacionObject: NSObject, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        codigoPostalFacturacion <- map["codigoPostalFacturacion"]
        colonia <- map["colonia"]
        numExterior <- map["numExterior"]
        numInterior <- map["numInterior"]
        delegacionMunicipio <- map["delegacionMunicipio"]
        ciudad <- map["ciudad"]
        mismaDirInst <- map["mismaDirInst"]
        estado <- map["estado"]
        calle <- map["calle"]
    }

    var codigoPostalFacturacion: String = ""
    var colonia: String = ""
    var numExterior: String = ""
    var numInterior: String = ""
    var delegacionMunicipio: String = ""
    var ciudad: String = ""
    var mismaDirInst: Bool = false
    var estado: String = ""
    var calle: String = ""
}
