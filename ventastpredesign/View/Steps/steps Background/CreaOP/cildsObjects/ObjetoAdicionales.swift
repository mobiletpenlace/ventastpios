//
//  ObjetoAdicionales.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 01/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class ObjetoAdicionales: NSObject, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        informacionAdicionales <- map["informacionAdicionales"]
    }

    var  informacionAdicionales: InfoAddonsCreacionRequest = InfoAddonsCreacionRequest()

}
