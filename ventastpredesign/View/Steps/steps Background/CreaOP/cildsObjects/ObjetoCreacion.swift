//
//  ObjetoCreacion.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 01/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class ObjetoCreacion: NSObject, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        origen <- map["origen"]
        prospecto <- map["prospecto"]
        sitio <- map["sitio"]
    }

    var  origen: String = "AppMovil"
    var  prospecto: NProspecto = NProspecto()
    var  sitio: Sitio = Sitio()

}
