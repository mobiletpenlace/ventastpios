//
//  PromocionesCreacion.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 01/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper
import RealmSwift

class PromocionesCreacion: Object, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        id <- map["Id"]
        esAutomatica <- map["esAutomatica"]
        requiereAutomatica <- map["requiereAutomatica"]
    }

    @objc dynamic var  id: String?
    @objc dynamic var  esAutomatica: Bool = false
    @objc dynamic var  requiereAutomatica: Bool = false

}
