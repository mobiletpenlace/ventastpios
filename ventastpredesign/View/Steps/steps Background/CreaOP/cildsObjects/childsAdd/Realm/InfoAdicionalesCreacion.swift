//
//  InfoAdicionalesCreacion.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 01/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper
import RealmSwift

class InfoAdicionalesCreacion: Object, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        productosAdd <- map["productosAdd"]
        promocionesAdd <- map["promocionesAdd"]
        serviciosAdd <- map["serviciosAdd"]
    }

    override static func primaryKey() -> String? {
        return "uui"
    }

    @objc dynamic var  uui: String = ""
    @objc dynamic var  productosAdd: ProductosAdd?
    @objc dynamic var  promocionesAdd: PromocionesAdd?
    @objc dynamic var  serviciosAdd: ServiciosAdd?

}
