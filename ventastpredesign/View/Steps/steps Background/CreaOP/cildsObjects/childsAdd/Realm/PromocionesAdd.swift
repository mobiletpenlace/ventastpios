//
//  PromocionesAdd.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 01/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import RealmSwift
import UIKit

import ObjectMapper

class PromocionesAdd: Object, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        promociones <- map["promociones"]
    }

    var  promociones: List<PromocionesCreacion> = List<PromocionesCreacion>()

}
