//
//  ServiciosCreacion.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 01/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift

class ServiciosCreacion: Object, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        id <- map["Id"]
        cantidad <- map["cantidad"]
        cantidadInt <- map["cantidadInt"]
        // TEST BEER SERVICE
        idExternoGM <- map["idExternoGM"]
        // TEST BEER SERVICE
    }

    @objc dynamic var  id: String?
    @objc dynamic var  cantidad: String?
    var  cantidadInt: Int?

    // TEST BEER SERVICE
    var idExternoGM: String?
    // TEST BEER SERVICE

}
