//
//  ServiciosCreacionRequest.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 16/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class ServiciosCreacionRequest: NSObject, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        id <- map["Id"]
        cantidad <- map["cantidad"]

        // TEST BEER SERVICE
        idExternoGM <- map["idExternoGM"]
        // TEST BEER SERVICE
    }

    var  id: String?
    var  cantidad: String?
    // TEST BEER SERVICE
    var idExternoGM: String?
    // TEST BEER SERVICE
}
