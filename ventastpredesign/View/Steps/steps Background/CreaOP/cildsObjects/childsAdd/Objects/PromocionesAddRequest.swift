//
//  PromocionesAddRequest.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 16/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class PromocionesAddRequest: NSObject, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        promociones <- map["promociones"]
    }

    var  promociones: [PromocionesCreacionRequest] = []

}
