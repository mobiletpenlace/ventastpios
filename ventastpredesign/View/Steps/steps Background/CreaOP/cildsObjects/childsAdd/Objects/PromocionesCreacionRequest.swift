//
//  PromocionesCreacionRequest.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 16/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class PromocionesCreacionRequest: NSObject, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        id <- map["Id"]
        esAutomatica <- map["esAutomatica"]
        requiereAutomatica <- map["requiereAutomatica"]
    }

    var  id: String?
    var  esAutomatica: Bool = false
    var  requiereAutomatica: Bool = false

}
