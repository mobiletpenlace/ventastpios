//
//  InfoAddonsResponse.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 16/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift

class InfoAddonsCreacionRequest: NSObject, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        productosAdd <- map["productosAdd"]
        promocionesAdd <- map["promocionesAdd"]
        serviciosAdd <- map["serviciosAdd"]
    }

    var  productosAdd: ProductosAddRequest = ProductosAddRequest()
    var  promocionesAdd: PromocionesAddRequest = PromocionesAddRequest()
    var  serviciosAdd: ServiciosAddRequest = ServiciosAddRequest()

}
