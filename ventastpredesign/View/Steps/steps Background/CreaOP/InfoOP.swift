//
//  InfoOP.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 01/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class InfoOP: NSObject, Mappable {
    required init?(map: Map) {

    }

    func mapping(map: Map) {
        numeroOportunidad <- map["numeroOportunidad"]
        numeroCuenta <- map["numeroCuenta"]
        nombreCliente <- map["nombreCliente"]
        idOportunidad <- map["IdOportunidad"]
        idDocumento <- map["IdDocumento"]
        bloqueaEfectivo <- map["bloqueaEfectivo"]
    }

    override init() {

    }
    var numeroOportunidad: String?
    var numeroCuenta: String?
    var nombreCliente: String?
    var idOportunidad: String?
    var idDocumento: String?
    var bloqueaEfectivo: Bool = false

}
