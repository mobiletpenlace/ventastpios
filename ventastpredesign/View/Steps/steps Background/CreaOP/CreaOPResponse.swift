//
//  CreaOPResponse.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 01/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class CreaOPResponse: BaseResponse {

    required init?(map: Map) {

    }

    override func mapping(map: Map) {
        resultDescription <- map["resultDescription"]
        result <- map["result"]
        info <- map ["info"]
    }

    var info: InfoOP = InfoOP()

}
