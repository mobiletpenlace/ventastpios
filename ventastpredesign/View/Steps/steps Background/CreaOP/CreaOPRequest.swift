//
//  CreaOPRequest.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 01/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

public class CreaOPRequest: BaseRequest {
    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override public func mapping(map: Map) {
        accion <- map["accion"]
        ventaTP <- map["ventaTP"]
    }

    var accion: String?
    var ventaTP: String?
}
