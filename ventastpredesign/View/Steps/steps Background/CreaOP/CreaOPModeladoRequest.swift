//
//  CreaOPModeladoRequest.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 02/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

public class CreaOPModeladoRequest: BaseRequest {
    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override public func mapping(map: Map) {
        accion <- map["accion"]
        idOportunidad <- map["idOportunidad"]
        ventaTP <- map["ventaTP"]
    }

    var accion: String?
    var idOportunidad: String?
    var ventaTP: String?
}
