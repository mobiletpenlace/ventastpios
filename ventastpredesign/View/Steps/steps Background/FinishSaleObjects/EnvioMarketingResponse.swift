//
//  EnvioMarketingResponse.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 06/07/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class EnvioMarketingResponse: BaseResponse {

    required init?(map: Map) {

    }

    override func mapping(map: Map) {
        Result <- map["result"]
        ResultDescription <- map["resultDescription"]
    }

    var Result: String?
    var ResultDescription: String?

}
