//
//  EnvioMarketingRequest.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 06/07/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

public class EnvioMarketingRequest: BaseRequest {
    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override public func mapping(map: Map) {
        idRegistro <- map["idRegistro"]
        tipoMensaje <- map["tipoMensaje"]
    }

    var idRegistro: String?
    var tipoMensaje: String = "EnvioPreregistro"
}
