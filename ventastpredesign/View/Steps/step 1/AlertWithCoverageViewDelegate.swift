//
//  AlertWithCoverageViewDelegate.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 27/03/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import SwiftEventBus
import MaterialComponents.MaterialTextControls_FilledTextAreas
import MaterialComponents.MaterialTextControls_FilledTextFields

extension AlertWithCoverageViewController: CoberturaDelegate, QueryCPDelegate {
    func changeDirecctionColony(changeDirection: ChangeDirecction) {
        calle = changeDirection.calle!
        setDirecction(changeDirection.colonia!)
    }

    func configTextFields() {
        NumExtTextField.normalTheme(type: 2)
        NumIntTextField.normalTheme(type: 1)
    }

    func OnErrorCP() {
        Constants.Back(viewC: self)
        SwiftEventBus.post("ErrorCP", sender: nil)
    }

    func OnSuccessQueryCP(queryCPResponse: QueryCPResponse, CP: String) {
        queryResponseCp = queryCPResponse
        for colony in queryCPResponse.MArrColonias {
            colonyResponse.append(colony)
        }
        codigoPostal = CP
        for col in queryCPResponse.MArrColonias {
            colonias.append(col.Colonia!)
        }

        colonyResponse = colonyResponse.removingDuplicates()
        colonias = colonias.removingDuplicates()

        setDirecction(colonias[0])
    }

    func setDirecction(_ colonia: String) {
        var aux = 0
        var pos = 0
        for col in colonyResponse {
            if col.Colonia == colonia {
                pos = aux
            }
            aux += 1
        }

        if colonyResponse.isEmpty {
            // SOMETHING
        } else {
            LabelPlace.text = String("\(calle), \(colonyResponse[pos].Colonia!), \(colonyResponse[pos].DelegacionMunicipio!), \(colonyResponse[pos].Ciudad!), \(colonyResponse[pos].Estado!), \(codigoPostal)")
            direccion.Calle = calle
            direccion.CodigoPostal = codigoPostal
            direccion.Colonia = colonyResponse[pos].Colonia ?? ""
            direccion.Delegacion = colonyResponse[pos].DelegacionMunicipio ?? ""
            direccion.Ciudad = colonyResponse[pos].Ciudad ?? ""
            direccion.Estado = colonyResponse[pos].Estado ?? ""
            estimulo = colonyResponse[pos].AplicaEstimuloFiscal ?? "false"
            idCP = colonyResponse[pos].Id ?? ""

            colonyTextField.text = direccion.Colonia
        }

    }

    func OnSuccessGetAddres(Addres: String) {
        LabelPlace.text = Addres
    }

    func OnSuccessCobertura(mCoberturaResponse: CoberturaResponse) {
        if mCoberturaResponse.MCalculaFactibilidad?.factibilidad == "1" {
            mFactibilidad = mCoberturaResponse.MCalculaFactibilidad
            mQueryCPPresenter.getDirectionForLatLng(latitude: latitud, longitude: longitud, mQueryCPDelegate: self)
            ViewAlert.isHidden = false
            num = mCoberturaResponse.MCalculaFactibilidad?.factibilidad
            if mCoberturaResponse.MCalculaFactibilidad?.factible != nil {
                factible =  (mCoberturaResponse.MCalculaFactibilidad?.factible)!
            }
            if mCoberturaResponse.MCalculaFactibilidad?.Region != nil {
                region = (mCoberturaResponse.MCalculaFactibilidad?.Region)!
            }
            if mCoberturaResponse.MCalculaFactibilidad?.IdRegion != nil {
                regionId = (mCoberturaResponse.MCalculaFactibilidad?.IdRegion)!
            }
            if mCoberturaResponse.MCalculaFactibilidad?.CategoryService != nil {
                categoryService = (mCoberturaResponse.MCalculaFactibilidad?.CategoryService)!
            }
        } else {
            back()
            SwiftEventBus.post("ErrorCobertura", sender: nil)
        }

    }

    func llenaObj() {
        mProspecto = NProspecto()
        mSitio = Sitio()
        let employeeRM = RealManager.findFirst(object: Empleado.self)
    // llena la parte del prospecto
        mProspecto.codigoPostal = direccion.CodigoPostal
        mProspecto.codigoPostal = direccion.plusCode
        mProspecto.colonia = direccion.Colonia
        mProspecto.delegacionMunicipio = direccion.Delegacion
        mProspecto.ciudad = direccion.Ciudad
        mProspecto.estado = direccion.Estado
        mProspecto.calle = direccion.Calle
        mProspecto.numExterior = NumExtTextField.text ?? ""
        mProspecto.numInterior = NumIntTextField.text ?? ""
        let street1 = street1TextField.text ?? ""
        let street2 = street2TextField.text ?? ""
        if !street1.isEmpty && !street2.isEmpty {
            mProspecto.refCalle = "\(street1) y \(street2)"
        } else if !street1.isEmpty {
            mProspecto.refCalle = street1
        } else if !street2.isEmpty {
            mProspecto.refCalle = street2
        } else {
            mProspecto.refCalle = ""
        }
        mProspecto.refUrbana = refTextField.text ?? ""
        mProspecto.canalEmpleado = employeeRM?.CanalSelect ?? "Cambaceo"
        mProspecto.idCodigoPostal = idCP
//        if(mProspecto.canalEmpleado == "Distribuidor"){
//            mProspecto.subCanalEmpleado = mDistribuidor.text!
//        }else if(mProspecto.canalEmpleado == "Punto de Venta"){
//            mProspecto.subCanalEmpleado = mPuntoDeVenta.text!
//        }else if(mProspecto.canalEmpleado == "Convenios"){
//            mProspecto.subCanalEmpleado = mSubCanal.text!
//        }else{
//            mProspecto.subCanalEmpleado = "Cambaceo"
//        } igual va con logica de mary
        mProspecto.subCanalEmpleado = employeeRM?.SubCanalSelect ?? ""
        if estimulo == "false" {
            mProspecto.estimuloFiscal = false
        } else {
            mProspecto.estimuloFiscal = true
        }
        mSitio.categoryService = direccion.CategoryService
        mSitio.cluster = direccion.Cluster
        mSitio.tipoCobertura = "Sólo fibra"
        mSitio.distrito = direccion.Distrito
        mSitio.factibilidad = direccion.Factibilidad
        mSitio.latitud = direccion.Latitude
        mSitio.longitud = direccion.Longitude
        mSitio.plaza = direccion.Plaza
        mSitio.region = direccion.Region
        mSitio.regionId = direccion.RegionId
        mSitio.zona = direccion.zona
        mSitio.isEstimulo = estimulo
        mSitio.zona = mFactibilidad?.zona ?? ""
        mSitio.loteId = mFactibilidad?.id_lote ?? ""
        mSitio.codigoLote = mFactibilidad?.codigo_lote ?? ""
        mSitio.viviendaId = mFactibilidad?.id_vivienda ?? ""
        mSitio.tipoPredio = mFactibilidad?.tipo_predio ?? ""
        mSitio.numViviendas = mFactibilidad?.n_viviendas ?? ""
        mSitio.viviendaId = mFactibilidad?.id_vivienda ?? ""
        mSitio.tipoLote = mFactibilidad?.tipo_lote ?? ""
        mSitio.numNiveles = mFactibilidad?.n_niveles ?? ""
        mSitio.areaPertenencia = mFactibilidad?.area_pertenencia ?? ""
        mSitio.numTorres = mFactibilidad?.n_torres ?? ""
        mSitio.nombre = mFactibilidad?.nombre ?? ""
        mSitio.sectorComercial = mFactibilidad?.sector_comercial ?? ""
        mSitio.numViviendasLocales = mFactibilidad?.n_viviendas_locales ?? ""
        mSitio.clusterComercial = mFactibilidad?.nombre_cluster ?? ""
        mSitio.tipoInmueble =  ""
        mSitio.estatusInmueble =  ""
        mSitio.AnchoBandaSubida = ""
        mSitio.AnchoBandaBajada = ""
        mSitio.Tecnologia = ""
        mSitio.PuertoDisponible = ""
        mSitio.AsignaPuerto = ""
        mSitio.CreacionTKPlantaExt = ""
        // CargaInfoStep1Sitio    CargaInfoStep1Prospecto
        SwiftEventBus.post("CargaInfoStep1Sitio", sender: mSitio)
        SwiftEventBus.post("CargaInfoStep1Prospecto", sender: mProspecto)
    }

}
