//
//  CalculaFactibilidad.swift
//  ventastpredesign
//
//  Created by Claudia Isamar Delgado Vasquez on 19/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class CalculaFactibilidad: NSObject, Mappable {
    required init?(map: Map) {

    }
    func mapping(map: Map) {
        factible <- map["factible"]
        factibilidad <- map["factibilidad"]
        direccion <- map["direccion"]
        porcentaje <- map["porcentaje"]
        criterios <- map["criterios"]
        comentarios <- map["comentarios"]
        IdRegion <- map["IdRegion"]
        Region <- map["Region"]
        Cuidad <- map["Cuidad"]
        distrito <- map["distrito"]
        zona <- map["zona"]
        nombre_cluster <- map["nombre_cluster"]
        CategoryService <- map["CategoryService"]
        NSE <- map["NSE"]
        id_lote <- map["id_lote"]
        codigo_lote <- map["codigo_lote"]
        tipo_lote <- map["tipo_lote"]
        id_vivienda <- map["id_vivienda"]
        tipo_predio <- map["tipo_predio"]
        n_viviendas <- map["n_viviendas"]
        n_niveles <- map["n_niveles"]
        area_pertenencia <- map["area_pertenencia"]
        n_torres <- map["n_torres"]
        nombre <- map["nombre"]
        sector_comercial <- map["sector_comercial"]
        n_viviendas_locales <- map["n_viviendas_locales"]
        Detalle_Respuesta <- map["Detalle_Respuesta"]

    }
    override init() {

    }
    var factible: String?
    var factibilidad: String?
    var direccion: String?
    var porcentaje: String?
    var criterios: String?
    var comentarios: String?
    var IdRegion: String?
    var Region: String?
    var Cuidad: String?
    var distrito: String?
    var zona: String?
    var nombre_cluster: String?
    var CategoryService: String?
    var NSE: String?
    var id_lote: String?
    var codigo_lote: String?
    var tipo_lote: String?
    var id_vivienda: String?
    var tipo_predio: String?
    var n_viviendas: String?
    var n_niveles: String?
    var area_pertenencia: String?
    var n_torres: String?
    var nombre: String?
    var sector_comercial: String?
    var n_viviendas_locales: String?
    var Detalle_Respuesta: Detalle_Respuesta?
}
