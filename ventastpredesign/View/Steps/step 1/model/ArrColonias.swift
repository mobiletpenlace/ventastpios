//
//  ArrColonias.swift
//  ventastpredesign
//
//  Created by Claudia Isamar Delgado Vasquez on 16/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class ArrColonias: NSObject, Mappable {
    required init?(map: Map) {

    }

    override init() {
    }

    func mapping(map: Map) {
        // enlazar
        Id <- map["Id"]
        Nombre <- map["Nombre"]
        Colonia <- map["Colonia"]
        Ciudad <- map["Ciudad"]
        Clave <- map["Clave"]
        Cluster <- map["Cluster"]
        DelegacionMunicipio <- map["DelegacionMunicipio"]
        Estado <- map["Estado"]
        IdEstadoEN <- map["IdEstadoEN"]
        IdEstadoTP <- map["IdEstadoTP"]
        IdMunicipioEN <- map["IdMunicipioEN"]
        IdMunicipioTP <- map["IdMunicipioTP"]
        idOrigen <- map["idOrigen"]
        IdPoblacionEN <- map["IdPoblacionEN"]
        IdPoblacionTP <- map["IdPoblacionTP"]
        IdRegionEN <- map["IdRegionEN"]
        IdRegionTP <- map["IdRegionTP"]
        Limite <- map["Limite"]
        Limite1 <- map["Limite1"]
        AplicaEstimuloFiscal <- map["AplicaEstimuloFiscal"]
    }
    var Id: String?
    var Nombre: String?
    var Colonia: String?
    var Ciudad: String?
    var Clave: String?
    var Cluster: String?
    var DelegacionMunicipio: String?
    var Estado: String?
    var IdEstadoEN: String?
    var IdEstadoTP: String?
    var IdMunicipioEN: String?
    var IdMunicipioTP: String?
    var idOrigen: String?
    var IdPoblacionEN: String?
    var IdPoblacionTP: String?
    var IdRegionEN: String?
    var IdRegionTP: String?
    var Limite: String?
    var Limite1: String?
    var AplicaEstimuloFiscal: String?
}
