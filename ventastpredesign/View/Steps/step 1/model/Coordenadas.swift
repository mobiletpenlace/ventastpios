//
//  Coordenadas.swift
//  ventastpredesign
//
//  Created by Claudia Isamar Delgado Vasquez on 19/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class Coordenadas: NSObject, Mappable {
    required init?(map: Map) {

    }
    func mapping(map: Map) {
        TipoCliente <- map["TipoCliente"]
        latitud <- map["latitud"]
        longitud <- map["longitud"]
    }

    override init() {

    }
    var TipoCliente: String?
    var latitud: String?
    var longitud: String?
}
