//
//  Detalle_Respuesta.swift
//  ventastpredesign
//
//  Created by Claudia Isamar Delgado Vasquez on 19/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class Detalle_Respuesta: NSObject, Mappable {
    required init?(map: Map) {

    }
    func mapping(map: Map) {
        FechaRespuesta <- map["FechaRespuesta"]
        CodigoRespuesta <- map["CodigoRespuesta"]
        CodigoError <- map["CodigoError"]
        DescripcionError <- map["DescripcionError"]
        MensajeError <- map["MensajeError"]
    }
    override init() {

    }
    var FechaRespuesta: String?
    var CodigoRespuesta: String?
    var CodigoError: String?
    var DescripcionError: String?
    var MensajeError: String?
}
