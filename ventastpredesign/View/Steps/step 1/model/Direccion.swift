//
//  Direccion.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 30/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import UIKit

import ObjectMapper
import RealmSwift

class Direccion: Object, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        plusCode <- map["plusCode"]
        AutorizacionSinCobertura <- map["AutorizacionSinCobertura"]
        EntreCalles <- map["EntreCalles"]
        Edificio <- map["Edificio"]
        CategoryService <- map["CategoryService"]
        Ciudad <- map["Ciudad"]
        Cluster <- map["Cluster"]
        Colonia <- map["Colonia"]
        Delegacion <- map["Delegacion"]
        Distrito <- map["Distrito"]
        Factibilidad <- map["Factibilidad"]
        Factible <- map["Factible"]
        RegionId <- map["RegionId"]
        Latitude <- map["Latitude"]
        Longitude <- map["Longitude"]
        NumeroExterior <- map["NumeroExterior"]
        NumeroInterior <- map["NumeroInterior"]
        Plaza <- map["Plaza"]
        Region <- map["Region"]
        Estado <- map["Estado"]
        Calle <- map["Calle"]
        ReferenciaUrbana <- map["ReferenciaUrbana"]
        CodigoPostal <- map["CodigoPostal"]
        zona <- map["zona"]
        // cliente unico
        tipoInmueble <- map["tipoInmueble"]
        estatusInmueble <- map["estatusInmueble"]
        // lotes y atributos
        loteId <- map["loteId"]
        codigoLote <- map["codigoLote"]
        tipoLote <- map["tipoLote"]
        viviendaId <- map["viviendaId"]
        tipoPredio <- map["tipoPredio"]
        numViviendas <- map["numViviendas"]
        numNiveles <- map["numNiveles"]
        areaPertenencia <- map["areaPertenencia"]
        numTorres <- map["numTorres"]
        nombre <- map["nombre"]
        sectorComercial <- map["sectorComercial"]
        numViviendasLocales <- map["numViviendasLocales"]
        clusterComercial <- map["clusterComercial"]
    }
    override static func primaryKey() -> String? {
        return "uui"
    }
    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  Calle: String = ""
    @objc dynamic var  plusCode: String = ""
    @objc dynamic var  NumeroExterior: String = ""
    @objc dynamic var  NumeroInterior: String = ""
    @objc dynamic var  AutorizacionSinCobertura: String = ""
    @objc dynamic var  EntreCalles: String = ""
    @objc dynamic var  Edificio: String = ""
    @objc dynamic var  CategoryService: String = ""
    @objc dynamic var  Ciudad: String = ""
    @objc dynamic var  Cluster: String = ""
    @objc dynamic var  Colonia: String = ""
    @objc dynamic var  Delegacion: String = ""
    @objc dynamic var  Distrito: String = ""
    @objc dynamic var  Factibilidad: String = ""
    @objc dynamic var  Factible: String = ""
    @objc dynamic var  RegionId: String = ""
    @objc dynamic var  Latitude: String = ""
    @objc dynamic var  Longitude: String = ""
    @objc dynamic var  Plaza: String = ""
    @objc dynamic var  Region: String = ""
    @objc dynamic var  Estado: String = ""
    @objc dynamic var  ReferenciaUrbana: String = ""
    @objc dynamic var  CodigoPostal: String = ""
    @objc dynamic var  zona: String = ""
    @objc dynamic var  idCP: String = ""

    // cliente unico
    @objc dynamic var  tipoInmueble: String = ""
    @objc dynamic var  estatusInmueble: String = ""
    // Lotes y atributos
    @objc dynamic var  loteId: String = ""
    @objc dynamic var  codigoLote: String = ""
    @objc dynamic var  tipoLote: String = ""
    @objc dynamic var  viviendaId: String = ""
    @objc dynamic var  tipoPredio: String = ""
    @objc dynamic var  numViviendas: String = ""
    @objc dynamic var  numNiveles: String = ""
    @objc dynamic var  areaPertenencia: String = ""
    @objc dynamic var  numTorres: String = ""
    @objc dynamic var  nombre: String = ""
    @objc dynamic var  sectorComercial: String = ""
    @objc dynamic var  numViviendasLocales: String = ""
    @objc dynamic var  clusterComercial: String = ""

    func concatenatedProperties() -> String {
        let mirror = Mirror(reflecting: self)
        var propertyValues: [String] = []
        var calleNumero: String?

        for child in mirror.children {
            guard let label = child.label, let value = child.value as? String, !value.isEmpty else { continue }

            switch label {
            case "Calle":
                calleNumero = value
            case "NumeroExterior":
                if let calle = calleNumero {
                    calleNumero = "\(calle) \(value)"
                } else {
                    calleNumero = value
                }
            case "uui":
                continue
            default:
                propertyValues.append(value)
            }
        }

        if let calleNumero = calleNumero {
            propertyValues.insert(calleNumero, at: 0)
        }

        return propertyValues.joined(separator: ", ")
    }

}
