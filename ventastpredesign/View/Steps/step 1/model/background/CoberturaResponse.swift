//
//  CoberturaResponse.swift
//  ventastpredesign
//
//  Created by Claudia Isamar Delgado Vasquez on 19/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class CoberturaResponse: BaseResponse {
    required init?(map: Map) {

    }

    override func mapping(map: Map) {
        MCalculaFactibilidad <- map["CalculaFactibilidad"]
    }

    var MCalculaFactibilidad: CalculaFactibilidad?

}
