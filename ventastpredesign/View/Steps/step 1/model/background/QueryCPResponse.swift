//
//  QueryCPResponse.swift
//  ventastpredesign
//
//  Created by Claudia Isamar Delgado Vasquez on 16/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class QueryCPResponse: BaseResponse {
    required init?(map: Map) {

    }
    override func mapping(map: Map) {
        MResult <- map["Result"]
        MArrColonias <- map["ArrColonias"]

    }
    var MResult: Result?
    var MArrColonias: [ArrColonias] = []
}
