//
//  CoberturaRequest.swift
//  ventastpredesign
//
//  Created by Claudia Isamar Delgado Vasquez on 19/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class CoberturaRequest: BaseRequest {

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        MCoordenadas <- map["Coordenadas"]
        MLogin <- map["Login"]
    }
    var MCoordenadas: Coordenadas?
    var MLogin: Login?
}
