//
//  QueryCPRequest.swift
//  ventastpredesign
//
//  Created by Claudia Isamar Delgado Vasquez on 16/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class QueryCPRequest: BaseRequest {

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        CodigoPostal <- map["CodigoPostal"]
        MLogin <- map["Login"]

    }

    var CodigoPostal: String?
    var MLogin: Login?
}
