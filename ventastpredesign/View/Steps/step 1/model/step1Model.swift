//
//  step1Model.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 24/01/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import Foundation

class Step1Model: NSObject {
    var CodigoPostal: String!
    var Colonia: String!
    var Delegacion: String!
    var Cuidad: String!
    var Estado: String!
    var Edificio: Bool!
    var Calle: String!
    var NumExt: String!
    var NumInt: String!
    var EntreCalles: String!
    var Observaciones: String!
    var CanalVenta: String!
    var Distribuidor: String!
    var PuntoVenta: String!
    var SubCanal: String!
    var CategoryService: String!
    var Cluster: String!
    var Distrito: String!
    var Factibilidad: String!
    var Latitud: String!
    var Longitud: String!
    var Plaza: String!
    var Region: String!
    var RegionId: String!
    var TipoCobertura: String!
    var IdCodigoPostal: String!

    func setValues(_ CodigoPostal: String, _ Colonia: String, _ Delegacion: String, _ Ciudad: String, _ Estado: String, _ Edificio: Bool, _ Calle: String, _ NumExt: String, _ NumInt: String, _ EntreCalles: String, _ Observaciones: String, _ CanalVenta: String, _ Distribuidor: String, _ PuntoVenta: String, _ SubCanal: String, _ CategoryService: String, _ Cluster: String, _ Distrito: String, _ Factibilidad: String, _ Latitud: String, _ Longitud: String, _ Plaza: String, _ Region: String, _ RegionId: String, _ TipoCobertura: String, _ IdCodigoPostal: String) {
        self.CodigoPostal = CodigoPostal
        self.Colonia = Colonia
        self.Delegacion =  Delegacion
        self.Cuidad = Ciudad
        self.Estado = Estado
        self.Edificio = Edificio
        self.Calle = Calle
        self.NumExt = NumExt
        self.NumInt = NumInt
        self.EntreCalles = EntreCalles
        self.Observaciones = Observaciones
        self.CanalVenta = CanalVenta
        self.Distribuidor = Distribuidor
        self.PuntoVenta = PuntoVenta
        self.SubCanal = SubCanal
        self.CategoryService = CategoryService
        self.Cluster = Cluster
        self.Distrito = Distrito
        self.Factibilidad = Factibilidad
        self.Latitud = Latitud
        self.Longitud = Longitud
        self.Plaza = Plaza
        self.Region = Region
        self.RegionId = RegionId
        self.TipoCobertura = TipoCobertura
        self.IdCodigoPostal = IdCodigoPostal

    }

}
