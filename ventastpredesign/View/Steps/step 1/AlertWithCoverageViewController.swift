//
//  AlertWithCoverageViewController.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 15/03/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

import SwiftEventBus
import MaterialComponents.MaterialTextControls_FilledTextAreas
import MaterialComponents.MaterialTextControls_FilledTextFields

class AlertWithCoverageViewController: BaseVentasView {

    @IBOutlet weak var ViewAlert: UIView!
    @IBOutlet weak var ButtonAceptar: UIButton!
    @IBOutlet weak var LabelPlace: UILabel!
    @IBOutlet weak var ViewHeader: UIView!
    @IBOutlet weak var Cancel: UIButton!
    @IBOutlet weak var NumExtTextField: MDCFilledTextField! {
        didSet {
            NumExtTextField.normalTheme(type: 2)
        }
    }
    @IBOutlet weak var NumIntTextField: MDCFilledTextField! {
        didSet {
            NumIntTextField.normalTheme(type: 1)
        }
    }

    @IBOutlet weak var colonyTextField: MDCFilledTextField! {
        didSet {
            colonyTextField.normalTheme(type: 1)
            colonyTextField.delegate = self
            let arrowButton = UIButton(type: .custom)
            let arrowImage = UIImage(systemName: "chevron.down")
            arrowButton.setImage(arrowImage, for: .normal)
            arrowButton.tintColor = .gray
            arrowButton.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
            colonyTextField.trailingView = arrowButton
            colonyTextField.trailingViewMode = .always
        }
    }

    @IBOutlet weak var street1TextField: MDCFilledTextField!

    @IBOutlet weak var street2TextField: MDCFilledTextField!

    @IBOutlet weak var refTextField: MDCFilledTextField!

    var num: String?
    var mCoberturaPresenter: CoberturaPresenter!
    var mQueryCPPresenter: QueryCPPresenter!
    var txtLatitud: Double?
    var txtLongitud: Double?
    var latitud: String = ""
    var longitud: String = ""
    var region: String = ""
    var regionId: String = ""
    var factible: String = ""
    var categoryService: String = ""
    var numeroExterior: String = ""
    var calle: String = ""
    var codigoPostal: String = ""
    var idCP: String = ""
    var estimulo: String = ""
    var colonias: [String] = []
    var colonyResponse: [ArrColonias] = []
    let mFormValidator = UtilsTp()
    var mSitio: Sitio = Sitio()
    var mProspecto: NProspecto = NProspecto()
    var mFactibilidad: CalculaFactibilidad?
    var direccion: Direccion = Direccion()
    var queryResponseCp: QueryCPResponse?

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        //        colonyTextField.setTrailingIcon(icon: (UIImage(systemName: "chevron.down") ?? UIImage(systemName: "chevron.down"))!)
        latitud = String(txtLatitud!)
        longitud = String(txtLongitud!)
        let tipo = "Totalplay"
        ViewAlert.isHidden = true
        NumExtTextField.normalTheme(type: 2)
        NumIntTextField.normalTheme(type: 1)

        //        configTextFields()
        //         NumExtTextField.delegate = self
        mCoberturaPresenter.getCobertura(mTipoCliente: tipo, mlatitud: latitud, mlongitud: longitud, mCoberturaDelegate: self)
        SwiftEventBus.onMainThread(self, name: "back") { [weak self] result in
            if (result?.object as? Int) != nil {
                self?.back()
            }
        }

        SwiftEventBus.onMainThread(self, name: "set_calle") { [weak self] result in
            if let value = result?.object as? String {
                self?.calle = value
            }
        }

        SwiftEventBus.onMainThread(self, name: "set_numero_exterior") { [weak self] result in
            if let value = result?.object as? String {
                self?.numeroExterior = value
            }
        }

        SwiftEventBus.onMainThread(self, name: "ChangeDirection_colony") { [weak self] result in
            if let value = result?.object as? ChangeDirecction {
                self!.changeDirecctionColony(changeDirection: value)
            }
        }

        SwiftEventBus.onMainThread(self, name: "Set_ColonyPicker") { [weak self] result in
            if let value = result?.object as? String {
                self?.setDirecction(value)
            }
        }

    }


    func setColonia(_ colonia: String) {
        colonyTextField.text = colonia
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        SwiftEventBus.unregister(self, name: "ChangeDirection_colony")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func viewDidAppear(_ animated: Bool) {
        if RealManager.findFirst(object: Sitio.self) != nil && RealManager.findFirst(object: NProspecto.self) != nil {
            mProspecto = RealManager.findFirst(object: NProspecto.self)!
            NumExtTextField.text = mProspecto.numExterior
            NumIntTextField.text = mProspecto.numInterior
        }
        if faseConfigurarPlan.ResidencialisEmpty || faseConfigurarPlan.MicronegocioisEmpty {
            faseConfigurarPlan.ResidencialisEmpty = false
            faseConfigurarPlan.MicronegocioisEmpty = false
            Constants.Alert(title: "", body: "No se encontraron planes", type: type.Warning, viewC: self)
        }
        if (NumExtTextField.text != "" && colonyTextField.text != "" && street1TextField.text != "" && refTextField.text != "" ) {
            ButtonAceptar.isEnabled = true
        }

//        let arrowButton = UIButton(type: .custom)
//        let arrowImage = UIImage(systemName: "chevron.down")
//        arrowButton.setImage(arrowImage, for: .normal)
//        arrowButton.tintColor = .gray
//        arrowButton.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
//        colonyTextField.trailingView = arrowButton
//        colonyTextField.trailingViewMode = .always

    }

    override func getPresenters() -> [BasePresenter]? {
        mCoberturaPresenter = CoberturaPresenter(view: self)
        mQueryCPPresenter = QueryCPPresenter(view: self)
        return [mCoberturaPresenter, mQueryCPPresenter]
    }

    // aqui se debe de mover para adecuar el flujo
    @IBAction func ActionAccept(_ sender: Any) {
        direccion.Cluster = mFactibilidad?.nombre_cluster ?? ""
        direccion.Distrito = mFactibilidad?.distrito ?? ""
        direccion.zona =  mFactibilidad?.zona ?? ""
        direccion.Latitude = latitud
        direccion.Longitude = longitud
        direccion.Plaza = mFactibilidad?.Cuidad ?? ""
        direccion.Region = region
        direccion.RegionId = regionId
        direccion.Factible = factible
        direccion.Factibilidad = num!
        direccion.CategoryService = categoryService
        faseDataConsult.direction = direccion
        if AppDelegate.isValidateCoverageFromMenu {
            acceptValidateFromMenu()
        } else {
            acceptValidateHome()
        }
    }

    func acceptValidateHome() {
        switch num {
        case "0"?:
            back()
        case "1"?:

            if NumExtTextField.text == "" {
                NumExtTextField.errorTheme(message: "*Obligatorio")
            } else {
                llenaObj()
                Constants.LoadStoryBoardBoot(storyboard: "TypePropertyViewController", identifier: "TypePropertyViewController", viewC: self)

            }
        default:
            back()
        }
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text?.count == 0 {
            recolor()
        }
        return true
    }

    func recolor() {
        NumExtTextField.setTextColor(UIColor(named: "Base_rede_ligth_gray_titles")!, for: .normal)
        NumExtTextField.setNormalLabelColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .normal)
        NumExtTextField.setFloatingLabelColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .normal)
        NumExtTextField.setLeadingAssistiveLabelColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .normal)
    }

    func acceptValidateFromMenu() {
        switch num {
        case "0"?:
            back()
        case "1"?:
            AppDelegate.isValidateCoverageFromMenu = true
            let viewAlert: UIStoryboard = UIStoryboard(name: "StartSaleViewController", bundle: nil)
            let viewAlertVC = viewAlert.instantiateViewController(withIdentifier: "StartSaleViewController")as! StartSaleViewController
            viewAlertVC.isFirst = true
            self.present(viewAlertVC, animated: false, completion: {})
        default:
            break
        }
    }

    func back() {
        if RealManager.findFirst(object: Sitio.self) != nil && RealManager.findFirst(object: NProspecto.self) != nil {
            RealManager.deleteAll(object: Sitio.self)
            RealManager.deleteAll(object: NProspecto.self)
        }
        RealManager.deleteAll(object: Sitio.self)
        SwiftEventBus.post("Cambia_paso_container", sender: 1)
    }

    @IBAction func CancelValidate(_ sender: Any) {
        let viewAlert: UIStoryboard = UIStoryboard(name: "EditDirectionVC", bundle: nil)
        let viewAlertVC = viewAlert.instantiateViewController(withIdentifier: "EditDirectionVC")as! EditDirectionVC
        viewAlertVC.colonias = colonias
        viewAlertVC.direccion = direccion
        viewAlertVC.providesPresentationContextTransitionStyle = true
        viewAlertVC.definesPresentationContext = true
        viewAlertVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        viewAlertVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(viewAlertVC, animated: false, completion: {})
        // back()
    }

    @IBAction func checkNoExt(_ sender: Any) {
        if NumExtTextField.text?.isEmpty != true {
            NumExtTextField.normalTheme(type: 2)
        }
    }

    @IBAction func backToHome(_ sender: Any) {
        // Constants.Back(viewC: self)
        back()
    }

    @IBAction func selectColony(_ sender: Any) {
        Constants.OptionCombo(origen: "Set_Colony", array: Array(colonias), viewC: self)
    }

    func ButtonClose() {
        let B = self.presentingViewController
        self.dismiss(
            animated: false, completion: {
                B?.dismiss(animated: false, completion: nil)
            })
    }

    @IBAction func editingChanged(_ sender: Any) {
        if (NumExtTextField.text != "" && colonyTextField.text != "" && street1TextField.text != "" && refTextField.text != "" ) {
            ButtonAceptar.isEnabled = true
        } else {
            ButtonAceptar.isEnabled = false
        }
    }
}
