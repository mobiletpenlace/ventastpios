//
//  TypePropertyViewController.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 14/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus
import Amplitude

class TypePropertyViewController: BaseVentasView {

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: view)
    }

    override func viewDidAppear(_ animated: Bool) {
        Amplitude.instance().logEvent("Show_TypeAddressScreen")
    }
    @IBAction func homeAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: false, completion: nil )
        faseConfigurarPlan.isNegocio = false
        SwiftEventBus.post("Cambia_paso_container", sender: 3)
        faseConfigurarPlan.adivinaQuien = false
    }

    @IBAction func bussinessAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: false, completion: nil )
        faseConfigurarPlan.isNegocio = true
        SwiftEventBus.post("Cambia_paso_container", sender: 4)
        faseConfigurarPlan.flujo = 1
        // Constants.Alert(title: "Alerta", body: dialogs.Error.Construccion, type: type.Info, viewC: self)
    }

    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }

    override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
    }

    func closeMenu() {
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.0)
        UIView.animate(withDuration: 0.5, animations: {
            self.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        }) {(_) in
            self.view.removeFromSuperview()
        }
    }

}
