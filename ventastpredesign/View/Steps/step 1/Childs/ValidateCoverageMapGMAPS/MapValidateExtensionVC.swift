//
//  MapValidateExtensionVC.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 12/02/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//
import UIKit
import MapKit

import GoogleMaps
import GooglePlaces
import CoreLocation

extension MapValidateCoverageViewController: PredictionAddressDelegate, TableViewCellClickDelegate, GMSMapViewDelegate {

    func agregarMarcador(coordinate: CLLocationCoordinate2D, titulo: String, icono: UIImage) {
        if marker != nil {
            marker!.map = nil
        }
        lat = coordinate.latitude
        lon = coordinate.longitude
        marker = GMSMarker(position: coordinate)
        marker?.title = titulo
        marker?.map = self.mGoogleMapView
        marker?.icon = icono

        enableValidate()
    }

    func onSendingPrediction() {
    }

    func onSuccessPrediction(collectionAddress: [PredictionAddress]) {
                UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveEaseOut, animations: {
                    self.heightTable.constant = 190
                    self.view.layoutIfNeeded()
                }, completion: nil)
               mAddressDataSource.update(items: collectionAddress)
    }

    func initVC() {
        mAddressDataSource = BaseDataSourceVentas(tableView: resulTableView, delegate: self)
    }

    func onErrorPrediction(errorMessage: String) {
        print(errorMessage)
    }

    func onErrorConnection() {
    }

    func onSendingGeometry() {

    }

    func onSuccessGeometry(geoLatLon: Geometry, formatAddress: [AddressComponent]) {
                let camera = GMSCameraPosition.camera(withLatitude: (geoLatLon.location?.lat)!, longitude: (geoLatLon.location?.lng)!, zoom: 16.0)
                mGoogleMapView.animate(to: camera)
                self.heightTable.constant = 0
        let location = CLLocationCoordinate2D(latitude: (geoLatLon.location?.lat)!, longitude: (geoLatLon.location?.lng)!)
        lat = location.latitude
        lon = location.longitude
        predictionPresenter.didTapAt(coordinate: location)
    }

    func onErrorGeometry(errorMessage: String) {

    }

    func onErrorConnectionGeometry() {

    }

    func onTableViewCellClick(item: NSObject, cell: UITableViewCell) {
        let predictionAddress: PredictionAddress = item as! PredictionAddress
        predictionPresenter?.getLatLon(placeId: predictionAddress.place_id!)
        BarSearch.text = predictionAddress.description_!
    }

    func iniciarGoogleMaps() {
        self.mGoogleMapView.delegate = self
        self.mGoogleMapView.isMyLocationEnabled = true
        self.mGoogleMapView.settings.myLocationButton = true
        self.mGoogleMapView.settings.compassButton = true
        self.mGoogleMapView.settings.setAllGesturesEnabled(false)
        getLocation()
    }

    // Memory dealloc
    func finalizarGoogleMaps() {
        if self.mGoogleMapView != nil {
            self.mGoogleMapView.clear()
            self.mGoogleMapView.removeFromSuperview()
            self.mGoogleMapView.delegate = nil
            self.mGoogleMapView = nil
        }
    }

    func enableValidate() {
        validateLabel.text = "Validar cobertura"
        nextView.backgroundColor = UIColor(named: "Base_rede_button_dark")
        validateButton.isEnabled = true
    }

    func disableValidate() {
        validateLabel.text = "Selecciona una dirección"
        nextView.backgroundColor = .gray
        validateButton.isEnabled = false
    }

    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        self.mGoogleMapView.isMyLocationEnabled = true
    }

    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        self.mGoogleMapView.isMyLocationEnabled = true
        if gesture {
            mapView.selectedMarker = nil
        }
    }

    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        if let coordinate = mGoogleMapView.myLocation?.coordinate {
            mGoogleMapView.animate(to: GMSCameraPosition.camera(withLatitude: coordinate.latitude, longitude: coordinate.longitude, zoom: 17.0))
            view.endEditing(true)
            predictionPresenter.didTapAt(coordinate: coordinate)
            return true
        }
        return false
    }
// Deshabilitar el marcador al hacer un tap
//    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
//        view.endEditing(true)
//        predictionPresenter.didTapAt(coordinate: coordinate)
//    }

}
