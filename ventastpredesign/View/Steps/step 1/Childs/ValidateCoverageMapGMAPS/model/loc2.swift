//
//  loc2.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 15/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation

class loc2: NSObject {

    var lat: Double!
    var long: Double!

    init(_ latitutd: Double, _ longitud: Double) {
        self.lat = latitutd
        self.long = longitud
    }
}
