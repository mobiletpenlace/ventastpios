//
//  PredictionAddress.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 27/06/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import ObjectMapper

public class DirectionResponse: NSObject, Mappable {

    public var predictionData: [PredictionAddress] = []

    public required convenience init?(map: Map) {
        self.init()
    }

    public func mapping(map: Map) {
        predictionData		<- map["predictions"]
    }

}
