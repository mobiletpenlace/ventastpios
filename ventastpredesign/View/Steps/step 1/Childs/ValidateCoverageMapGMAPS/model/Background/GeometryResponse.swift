//
//  File.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 28/06/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import ObjectMapper
public class GeometryResponse: NSObject, Mappable {

    public var result: ResultLocation?

    public required convenience init?(map: Map) {
        self.init()
    }

    public func mapping(map: Map) {
        result		<- map["result"]
    }

}
