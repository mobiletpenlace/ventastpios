//
//  CellDirectionTableViewCell.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 27/06/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit

public class CellDirectionTableViewCell: BaseTableViewCell {

    @IBOutlet weak var titleLocationLabel: UILabel!
    @IBOutlet weak var directionLabel: UILabel!

    override public func pupulate(object: NSObject) {
        let prediction: PredictionAddress = object as! PredictionAddress
        titleLocationLabel.text = prediction.terms[0].value
        directionLabel.text = prediction.description_
    }

    override public func toString() -> String {
        return "CellDirectionTableViewCell"
    }

    override public func awakeFromNib() {
        super.awakeFromNib()
    }

    override public func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
