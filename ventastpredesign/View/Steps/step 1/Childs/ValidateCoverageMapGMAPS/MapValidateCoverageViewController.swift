//
//  MapValidateCoverageViewController.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 15/03/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import MapKit
import GoogleMaps
import GooglePlaces
import CoreLocation
import SwiftEventBus

class MapValidateCoverageViewController: BaseVentasView, CLLocationManagerDelegate, UISearchBarDelegate {

    var mGoogleMapView: GMSMapView!
    @IBOutlet weak var heightTable: NSLayoutConstraint!
    @IBOutlet weak var ActionValidateC: UIView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var ActionBack: UIButton!
    @IBOutlet weak var BarSearch: UISearchBar!
    @IBOutlet weak var resulTableView: UITableView!
    @IBOutlet weak var maskView: UIView!
    @IBOutlet weak var titleText: UILabel!
    @IBOutlet weak var nextView: UIView!
    @IBOutlet weak var validateButton: UIButton!
    @IBOutlet weak var validateLabel: UILabel!
    @IBOutlet weak var validateArrowImage: UIImageView!

    var mAddressDataSource: BaseDataSourceVentas<PredictionAddress, CellDirectionTableViewCell>!
    var predictionPresenter: PredictionAddressPresenter!
    var marker: GMSMarker?
    var predictionAddress: PredictionAddress!
    var firstCoordinates: Bool?
    var mUserPosition: CLLocationCoordinate2D!
    var locationManager: CLLocationManager!
    var searchB: String?
    var lat: Double = 0
    var lon: Double = 0
    var camera: GMSCameraPosition?

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        BarSearch.resignFirstResponder()
        heightTable.constant = 0
        firstCoordinates = true
        mGoogleMapView = GMSMapView(frame: self.view.frame)
        mGoogleMapView.delegate = self
        BarSearch.delegate = self
        self.view.addSubview(self.mGoogleMapView)
        self.view.addSubview(self.maskView)
        self.view.addSubview(self.ActionValidateC)
        self.view.addSubview(self.resulTableView)
        iniciarGoogleMaps()
        disableValidate()
        mGoogleMapView.settings.myLocationButton = false
        mGoogleMapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: 120, right: 0)
        Constants.Border(view: ActionValidateC, radius: 25, width: 0)
        initVC()
        view.endEditing(true)
        SwiftEventBus.onMainThread(self, name: "ErrorCP") { [weak self] _ in
            Constants.Alert(title: "", body: dialogs.Error.ErrorCP, type: type.Error, viewC: self!)
        }
        SwiftEventBus.onMainThread(self, name: "ErrorCobertura") { [weak self] _ in
            Constants.Alert(title: "", body: dialogs.Error.ErrorCovergage, type: type.Error, viewC: self!)
        }
        SwiftEventBus.onMainThread(self, name: "ReceiveDirection") { [weak self] result in
            let direccion = result?.object as? Direccion
            self?.BarSearch.text = direccion?.concatenatedProperties()
            self?.predictionPresenter?.getPredictionAddress(keyword: direccion?.concatenatedProperties() ?? "")
         }
    }

    override func viewDidAppear(_ animated: Bool) {
        if RealManager.findFirst(object: Sitio.self) != nil && RealManager.findFirst(object: NProspecto.self) != nil {
            var mSitio: Sitio = Sitio()
            mSitio = RealManager.findFirst(object: Sitio.self)!
            let location: loc2 = loc2(mSitio.latitud.doubleValue, mSitio.longitud.doubleValue)
            SwiftEventBus.post("SetLocationForSearch", sender: location)
            SwiftEventBus.post("Cambia_paso_container", sender: 2)
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        finalizarGoogleMaps()

    }

    override public func getPresenter() -> BasePresenter? {
        predictionPresenter = PredictionAddressPresenter(delegate: self)
        return predictionPresenter
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func getLocation() {
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.requestWhenInUseAuthorization()
        locationManager?.startUpdatingLocation()
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if(firstCoordinates)! {
            if mGoogleMapView != nil {
                lat = locations[0].coordinate.latitude
                lon = locations[0].coordinate.longitude
                camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lon, zoom: 17.0)
                mGoogleMapView.animate(to: camera!)
                locationManager.stopUpdatingLocation()
            }
        }
    }

    @IBAction func ActionCoverage(_ sender: Any) {
        if lat != 0 && lon != 0 {
            let location: loc2 = loc2(lat, lon)
            SwiftEventBus.post("SetLocationForSearch", sender: location)
            SwiftEventBus.post("Cambia_paso_container", sender: 2)
            finalizarGoogleMaps()
        } else {
             Constants.Alert(title: "", body: dialogs.Error.SearchDirection, type: type.Error, viewC: self)
        }
    }

    @IBAction func ActionBack(_ sender: Any) {
        Constants.Back(viewC: self)
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if (textField.text?.trim().count)! > 2 {
            predictionPresenter?.getPredictionAddress(keyword: (textField.text?.trim() as! NSString) as String)
        }
        return true
    }

    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        // cancel button becomes disabled when search bar isn't first responder, force it back enabled
        let deleteButton = BarSearch.value(forKey: "clearButton") as? UIButton
        DispatchQueue.main.async {
            if searchBar.text?.isEmpty == true {
                deleteButton?.isHidden = true
            } else {
                deleteButton?.isHidden = false
            }
        }
        return true
    }

    @IBAction func touchDownSearchBar(_ sender: Any) {
        Constants.LoadStoryBoard(storyboard: "AddressView", identifier: "AddressView", viewC: self)
    }
}
