//
//  PredictionAddressDelegate.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 27/06/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import CoreLocation

public protocol PredictionAddressDelegate: NSObjectProtocol {

    func onSendingPrediction()

    func onSuccessPrediction(collectionAddress: [PredictionAddress])

    func onErrorPrediction(errorMessage: String)

    func onErrorConnection()

    func onSendingGeometry()

    func onSuccessGeometry(geoLatLon: Geometry, formatAddress: [AddressComponent])

    func onErrorGeometry(errorMessage: String)

    func onErrorConnectionGeometry()

    func agregarMarcador(coordinate: CLLocationCoordinate2D, titulo: String, icono: UIImage)

}
