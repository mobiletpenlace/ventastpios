//
//  PredictionAddressPresenter.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 27/06/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import GoogleMaps
import GooglePlaces
import Foundation

import AlamofireObjectMapper

public class PredictionAddressPresenter: BasePresenter {

    public var mPredictionDelegate: PredictionAddressDelegate?
    public var request: Alamofire.Request?

    private let baseURLString = "https://maps.googleapis.com/maps/api/place/autocomplete/json"
    private let baseDetailtsURLString = "https://maps.googleapis.com/maps/api/place/details/json"
    public static let GOOGLE_MAP_KEY = "AIzaSyBeHJpR37gbG_MWrtk5qNeaYeQ7IeoSi_w"

    public init(delegate: PredictionAddressDelegate) {
        super.init(viewController: BaseViewController())
        self.mPredictionDelegate = delegate
    }

    public func getPredictionAddress(keyword: String) {
        if ConnectionUtils.isConnectedToNetwork() {
            self.mPredictionDelegate?.onSendingPrediction()
            let encodedString = keyword.urlEncoded() ?? ""
            let urlString = "\(baseURLString)?key=\(PredictionAddressPresenter.GOOGLE_MAP_KEY)&language=es&input="+encodedString+"&components=country%3Amx"
            request = Alamofire.request(urlString, method: HTTPMethod.get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseObject {(response: DataResponse<DirectionResponse>) in
                switch response.result {
                case .success:
                    let objectReponse: DirectionResponse = response.result.value!
                    if !objectReponse.predictionData.isEmpty {
                        self.mPredictionDelegate?.onSuccessPrediction(collectionAddress: objectReponse.predictionData)
                    } else {
                        self.mPredictionDelegate?.onErrorPrediction(errorMessage: "Ocurrio un error al consultar google maps")
                    }
                case .failure:
                    self.mPredictionDelegate?.onErrorPrediction(errorMessage: "Ocurrio un error al consultar la BD")
                }
            }

        } else {
            self.mPredictionDelegate?.onErrorConnection()
        }
    }

    func didTapAt(coordinate: CLLocationCoordinate2D) {
        let icono = scaleImage(
            image: UIImage(named: "Icon_filled_coverage")!,
            scaledToSize: CGSize(width: 30.0, height: 40.0)
        )
        self.mPredictionDelegate?.agregarMarcador(
            coordinate: coordinate,
            titulo: "ubicacion de instalacion",
            icono: icono)
    }

    public func getLatLon(placeId: String) {
        if ConnectionUtils.isConnectedToNetwork() {
            self.mPredictionDelegate?.onSendingGeometry()

            let urlString = "\(baseDetailtsURLString)?placeid=\(placeId)&key=\(PredictionAddressPresenter.GOOGLE_MAP_KEY)"

            request = Alamofire.request(urlString, method: HTTPMethod.get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseObject {(response: DataResponse<GeometryResponse>) in

                switch response.result {

                case .success:

                    let objectReponse: GeometryResponse = response.result.value!
                    if objectReponse.result?.geometry?.location?.lat != nil {
                        self.mPredictionDelegate?.onSuccessGeometry(geoLatLon: (objectReponse.result?.geometry!)!, formatAddress: (objectReponse.result?.formattedAddress)!)
                    } else {
                        self.mPredictionDelegate?.onErrorGeometry(errorMessage: "Ocurrio un error al consultar google maps")
                    }
                case .failure:
                    self.mPredictionDelegate?.onErrorGeometry(errorMessage: "Ocurrio un error al consultar la BD")
                }
            }

        } else {
            self.mPredictionDelegate?.onErrorConnectionGeometry()
        }
    }

    func scaleImage(image: UIImage, scaledToSize newSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }

}
