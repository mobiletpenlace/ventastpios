//
//  EditDirectionVC.swift
//  ventastp
//
//  Created by Juan Reynaldo Escobar Miron on 03/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTextControls_FilledTextAreas
import MaterialComponents.MaterialTextControls_FilledTextFields
import SwiftEventBus

class EditDirectionVC: BaseVentasView {

    @IBOutlet weak var cpTextField: MDCFilledTextField!
    @IBOutlet weak var coloniaTextField: MDCFilledTextField!
    @IBOutlet weak var municipioTextField: MDCFilledTextField!
    @IBOutlet weak var ciudadTextField: MDCFilledTextField!
    @IBOutlet weak var estadoTextField: MDCFilledTextField!
    @IBOutlet weak var calleTextField: MDCFilledTextField!
    var colonias: [String] = []
    var direccion: Direccion = Direccion()

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        setText()
        SwiftEventBus.onMainThread(self, name: "Set_ColonyPicker") { [weak self] result in
            if let value = result?.object as? String {
                self?.setDirecction(value)
            }
        }
    }

    func setDirecction(_ colonia: String) {
        coloniaTextField.text = colonia
    }

    func setText() {
        cpTextField.text = direccion.CodigoPostal
        coloniaTextField.text = direccion.Colonia
        municipioTextField.text = direccion.Delegacion
        ciudadTextField.text = direccion.Ciudad
        estadoTextField.text = direccion.Estado
        calleTextField.text = direccion.Calle
        cpTextField.clearColorFill()
        coloniaTextField.clearColorFill()
        municipioTextField.clearColorFill()
        ciudadTextField.clearColorFill()
        estadoTextField.clearColorFill()
        calleTextField.clearColorFill()
    }

    @IBAction func selectColony(_ sender: Any) {
        Constants.OptionCombo(origen: "Set_Colony", array: colonias, viewC: self)
    }

    @IBAction func CancelarAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }

    @IBAction func AceptarAction(_ sender: Any) {
        var changeDirection: ChangeDirecction?
        changeDirection = ChangeDirecction(calleTextField.text ?? "", coloniaTextField.text ?? "")
        SwiftEventBus.post("ChangeDirection_colony", sender: changeDirection)
        self.dismiss(animated: false, completion: nil)
    }

}

class ChangeDirecction: NSObject {

    var calle: String?
    var colonia: String?

    init(_ calle: String, _ colonia: String) {
        self.calle = calle
        self.colonia = colonia
    }
}
