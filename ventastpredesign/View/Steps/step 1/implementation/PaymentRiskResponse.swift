//
//  PaymentRiskResponse.swift
//  ventastp
//
//  Created by Armando Isais Olguin Cabrera on 8/19/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//
import Foundation
import UIKit
import ObjectMapper

class PaymentRiskResponse: BaseResponse {

    required init?(map: Map) {
    }

    override func mapping(map: Map) {
        Results <- map["Results"]
        resultsCobranza<-map["ResultsCobranza"]
    }

    override init() {
    }

    var Results: Response?
    var resultsCobranza: [CobranzaObject]?
}
