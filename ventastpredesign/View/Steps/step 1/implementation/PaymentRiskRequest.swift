//
//  PaymentRiskRequest.swift
//  ventastp
//
//  Created by Armando Isais Olguin Cabrera on 8/19/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class PaymentRiskRequest: BaseRequest {

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        mData <- map["Datos"]
        MLogin <- map["Login"]

    }

    var mData: Client?
    var MLogin: Login?

}
