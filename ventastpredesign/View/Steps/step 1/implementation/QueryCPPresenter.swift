//
//  QueryCPPresenter.swift
//  ventastpredesign
//
//  Created by Claudia Isamar Delgado Vasquez on 16/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

import SwiftEventBus

protocol QueryCPDelegate: NSObjectProtocol {
    func OnSuccessQueryCP(queryCPResponse: QueryCPResponse, CP: String)
    func OnSuccessGetAddres(Addres: String)
    func OnErrorCP()
}

class QueryCPPresenter: BaseVentastpredesignPresenter {
    var mQueryCPDelegate: QueryCPDelegate!
    var Cp: String = ""

    func getAddressForLatLng(_ latitude: String, _ longitude: String, _ mQueryCPDelegate: QueryCPDelegate) {
        let url = NSURL(string: "\(ApiDefinition.GEO_CODING_BASE_URL)latlng=\(latitude),\(longitude)&result_type=postal_code&key=\(ApiDefinition.GEO_CODING_API_KEY)")
        let data = NSData(contentsOf: url! as URL)
        let json = try! JSONSerialization.jsonObject(with: data! as Data, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
        if let status = json["status"] as? String {
            if status  == "ZERO_RESULTS" {
                mQueryCPDelegate.OnErrorCP()
            } else {
                if let result = json["results"] as? NSArray {
                    if let addressComponent = result[0] as? [String: Any] {
                        if let address = addressComponent["address_components"] as? [Any] {
                            if let arrayAddress = address[0] as? NSDictionary {
                                Cp = arrayAddress["short_name"] as! String
                                getQueryCP(mCodigoPostal: Cp, mQueryCPDelegate: mQueryCPDelegate)

                            }
                        }
                    }
                }
            }
        }
    }

    func getDirectionForLatLng(latitude: String, longitude: String, mQueryCPDelegate: QueryCPDelegate) {
        var calle: String = ""
        let url = NSURL(string: "\(ApiDefinition.GEO_CODING_BASE_URL)latlng=\(latitude),\(longitude)&result_type=route&key=\(ApiDefinition.GEO_CODING_API_KEY)")
        Logger.println("\(url)")
        let data = NSData(contentsOf: url! as URL)
        let json = try! JSONSerialization.jsonObject(with: data! as Data, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
        if let result = json["results"] as? NSArray {
            if let status = json["status"] as? String {
                if status == "ZERO_RESULTS" {
                    mQueryCPDelegate.OnErrorCP()
                } else {
                    if let addressComponent = result[0] as? [String: Any] {
                        if let forcalle = addressComponent["address_components"] as? [Any] {
                            for n in forcalle {
                                if let arrayAddress = n as? NSDictionary {
                                    if let tipos = arrayAddress["types"] as? [String] {
                                        for tipo in tipos {
                                            if tipo == "street_number" {
                                                calle = arrayAddress["short_name"] as! String
                                                SwiftEventBus.post("set_numero_exterior", sender: calle)
                                            }
                                            if tipo == "route" {
                                                calle = arrayAddress["long_name"] as! String
                                                mQueryCPDelegate.OnSuccessGetAddres(Addres: calle)
                                                SwiftEventBus.post("set_calle", sender: calle)
                                            }
                                        }
                                    }
                                }
                            }
                        }
        //                if let address = addressComponent["formatted_address"] as? String{
        //                    //sin uso por el momento pero arroja la direccion ya armada
        //                    //mQueryCPDelegate.OnSuccessGetAddres(Addres: address)
        //                }
                        getAddressForLatLng(latitude, longitude, mQueryCPDelegate)
                    }
                }
            }
        }
    }

    func getQueryCP(mCodigoPostal: String, mQueryCPDelegate: QueryCPDelegate) {
        self.mQueryCPDelegate = mQueryCPDelegate
        let queryCPRequest = QueryCPRequest()
        queryCPRequest.MLogin = Login()
        queryCPRequest.MLogin?.Ip = "1.1.1.1"
        queryCPRequest.MLogin?.User = "25631"
        queryCPRequest.MLogin?.SecretWord = "Middle100$"
        queryCPRequest.CodigoPostal = mCodigoPostal
        Cp = mCodigoPostal
        RetrofitManager<QueryCPResponse>.init(requestUrl: ApiDefinition.API_CP, delegate: self).request(requestModel: queryCPRequest)
    }
    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if requestUrl == ApiDefinition.API_CP {
            OnSuccessQueryCP(queryCPResponse: response as! QueryCPResponse)
        }
    }
    func OnSuccessQueryCP(queryCPResponse: QueryCPResponse) {
        if queryCPResponse.MArrColonias.count != 0 {
            mQueryCPDelegate.OnSuccessQueryCP(queryCPResponse: queryCPResponse, CP: Cp)
        } else {
            Constants.Alert(title: "", body: dialogs.Error.CpNoExist, type: type.Error, viewC: mViewController)
            view.hideLoading()
        }
    }
}
