//
//  Client.swift
//  ventastp
//
//  Created by Armando Isais Olguin Cabrera on 8/19/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class Client: NSObject, Mappable {

    var NombreCliente: String?
    var Calle: String?
    var Numero: String?
    var CodigoPostal: String?
    var Colonia: String?
    var CorreoElectronico: String?
    var Telefono: String?
    var RFC: String?
    var Delegacion: String?

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        NombreCliente <- map["NombreCliente"]
        Calle <- map["Calle"]
        Numero <- map["Numero"]
        CodigoPostal <- map["CodigoPostal"]
        Colonia <- map["Colonia"]
        CorreoElectronico <- map["CorreoElectronico"]
        Telefono <- map["Telefono"]
        RFC <- map["RFC"]
        Delegacion <- map["Delegacion"]
    }

    override init() {
    }

}
