//
//  CoberturaPresenter.swift
//  ventastpredesign
//
//  Created by Claudia Isamar Delgado Vasquez on 19/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

protocol CoberturaDelegate: NSObjectProtocol {
    func OnSuccessCobertura(mCoberturaResponse: CoberturaResponse)
}

class CoberturaPresenter: BaseVentastpredesignPresenter {

    var mCoberturaDelegate: CoberturaDelegate!

    func atachView(coberturaDelegate: CoberturaDelegate) {
        mCoberturaDelegate = coberturaDelegate
    }

    func getCoberturaCheck(mTipoCliente: String, mlatitud: String, mlongitud: String) {
        let coberturaRequest = CoberturaRequest()

        coberturaRequest.MLogin = Login()
        coberturaRequest.MLogin?.Ip = "1.1.1.1"
        coberturaRequest.MLogin?.User = "25631"
        coberturaRequest.MLogin?.SecretWord = "Middle100$"

        coberturaRequest.MCoordenadas = Coordenadas()
        coberturaRequest.MCoordenadas?.TipoCliente = mTipoCliente
        coberturaRequest.MCoordenadas?.latitud = mlatitud
        coberturaRequest.MCoordenadas?.longitud = mlongitud

        RetrofitManager<CoberturaResponse>.init(requestUrl: ApiDefinition.API_COVERAGE, delegate: self).request(requestModel: coberturaRequest)
    }

    func getCobertura(mTipoCliente: String, mlatitud: String, mlongitud: String, mCoberturaDelegate: CoberturaDelegate) {

        self.mCoberturaDelegate = mCoberturaDelegate
        let coberturaRequest = CoberturaRequest()

        coberturaRequest.MLogin = Login()
        coberturaRequest.MLogin?.Ip = "1.1.1.1"
        coberturaRequest.MLogin?.User = "25631"
        coberturaRequest.MLogin?.SecretWord = "Middle100$"

        coberturaRequest.MCoordenadas = Coordenadas()
        coberturaRequest.MCoordenadas?.TipoCliente = mTipoCliente
        coberturaRequest.MCoordenadas?.latitud = mlatitud
        coberturaRequest.MCoordenadas?.longitud = mlongitud

        RetrofitManager<CoberturaResponse>.init(requestUrl: ApiDefinition.API_COVERAGE, delegate: self).request(requestModel: coberturaRequest)
    }
    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {

        if requestUrl == ApiDefinition.API_COVERAGE {
            // view.hideLoading()
            OnSuccessCobertura(coberturaResponse: response as! CoberturaResponse)
        }
    }
    func OnSuccessCobertura(coberturaResponse: CoberturaResponse) {
        if coberturaResponse.MCalculaFactibilidad?.factibilidad != nil {
            mCoberturaDelegate.OnSuccessCobertura(mCoberturaResponse: coberturaResponse)
        } else {
            view.hideLoading()
            Constants.Alert(title: "", body: dialogs.Error.InternService, type: type.Error, viewC: mViewController)
        }

    }
}
