//
//  CobranzaObject.swift
//  ventastp
//
//  Created by Armando Isais Olguin Cabrera on 8/19/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class CobranzaObject: NSObject, Mappable {

    var Poid: String?
    var NumeroCuenta: String?
    var StatusCuenta: String?
    var NombreCliente: String?
    var Estado: String?
    var Ciudad: String?
    var Direccion: String?
    var CodigoPostal: String?
    var CorreoElectronico: String?
    var RFC: String?
    var Telefono: String?
    var SaldoCuenta: String?
    var NivelRiesgo: String?
    var Coincidencia: String?

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        Poid<-map["Poid"]
        NumeroCuenta<-map["NumeroCuenta"]
        StatusCuenta<-map["StatusCuenta"]
        NombreCliente<-map["NombreCliente"]
        Estado<-map["Estado"]
        Ciudad<-map["Ciudad"]
        Direccion<-map["Direccion"]
        CodigoPostal<-map["CodigoPostal"]
        CorreoElectronico<-map["CorreoElectronico"]
        RFC<-map["RFC"]
        Telefono<-map["Telefono"]
        SaldoCuenta<-map["SaldoCuenta"]
        NivelRiesgo<-map["NivelRiesgo"]
        Coincidencia<-map["Coincidencia"]
    }

    override init() {
    }

}
