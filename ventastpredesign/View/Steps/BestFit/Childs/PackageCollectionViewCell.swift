//
//  PackageCollectionViewCell.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 06/01/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class PackageCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var MegasView: UIView!
    @IBOutlet weak var megasLabel: UILabel!
    @IBOutlet weak var labelx: UILabel!
    var velocidad: String = "0"
    var oferta: String = ""

    override func awakeFromNib() {

        if faseConfigurarPlan.isNegocio == false {
            oferta = "residencial"
        } else {
            oferta = "micronegocios"
        }

        super.awakeFromNib()
        SwiftEventBus.onMainThread(self, name: "RemoveEventBus") { [weak self] _ in
            self?.removeEventBus()
        }

        SwiftEventBus.onMainThread(self, name: "apagaBoton") { [weak self] _ in
            self?.apagar()
        }
    }

    func removeEventBus() {
        SwiftEventBus.unregister(self)
    }

    func setData(_ velocidad: String, _ oferta: String) {
        self.velocidad = velocidad
        self.oferta = oferta
        apagar()
    }

    func apagar() {
        megasLabel.text = velocidad
        megasLabel.textColor = UIColor(named: "Base_rede_init_" + velocidad + "_" + oferta)
        labelx.textColor = UIColor(named: "Base_rede_init_" + velocidad + "_" + oferta)
        MegasView.borderColor = UIColor(named: "Base_rede_init_" + velocidad + "_" + oferta)
        MegasView.borderWidth = 2
        MegasView.alpha = 0.3
    }

    @IBAction func onClick(_ sender: Any) {
        SwiftEventBus.post("apagaBoton", sender: nil)
        MegasView.borderColor = UIColor(named: "Base_rede_init_" + velocidad + "_" + oferta)
        MegasView.borderWidth = 4
        MegasView.alpha = 1
        SwiftEventBus.post("VelocidadSelected", sender: velocidad)
        Logger.println(velocidad)
    }

}
