//
//  BestFitView.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 09/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus
import Amplitude

class BestFitView: BaseVentasView {
    // TV
    @IBOutlet weak var tvView: UIView!
    @IBOutlet weak var heightTV: NSLayoutConstraint!
    @IBOutlet weak var tvTitle: UILabel!
    @IBOutlet weak var ButtonsTV: UIView!
    @IBOutlet weak var tvPlusButton: UIButton!
    @IBOutlet weak var tvButton: UIButton!
    @IBOutlet weak var withoutTVButon: UIButton!
    @IBOutlet weak var selectionTVLabel: UILabel!
    @IBOutlet weak var cambiarTVButton: UIButton!
    @IBOutlet weak var vsbView: UIView!
    @IBOutlet weak var tv4kView: UIView!
    @IBOutlet weak var wiouthTvView: UIView!
    @IBOutlet weak var buttonsHeight: NSLayoutConstraint!
    @IBOutlet weak var vsbViewHeight: NSLayoutConstraint!
    @IBOutlet weak var tvViewHeight: NSLayoutConstraint!
    @IBOutlet weak var sinTvViewHeight: NSLayoutConstraint!
    // INERNET
    @IBOutlet weak var internetView: UIView!
    @IBOutlet weak var heightInternet: NSLayoutConstraint!
    @IBOutlet weak var internetTitle: UILabel!
    @IBOutlet weak var viewContentMegas: UIView!
    @IBOutlet weak var selectionInternetLabel: UILabel!
    @IBOutlet weak var cambiarInternetButton: UIButton!
    // STREAMING
    @IBOutlet weak var streamingView: UIView!
    @IBOutlet weak var heightStreaming: NSLayoutConstraint!
    @IBOutlet weak var streamingTitle: UILabel!
    @IBOutlet weak var devicesStack: UIStackView!
    @IBOutlet weak var noCombinarStack: UIStackView!
    @IBOutlet weak var noCombinarView: UIView!
    @IBOutlet weak var streamingStack: UIStackView!
    @IBOutlet weak var netflixView: UIView!
    @IBOutlet weak var amazonView: UIView!
    @IBOutlet weak var netflixButton: UIButton!
    @IBOutlet weak var amazonButton: UIButton!
    @IBOutlet weak var notthingButtton: UIButton!
    @IBOutlet weak var twoDevicesView: UIView!
    @IBOutlet weak var fourDevicesView: UIView!
    @IBOutlet weak var twoDevicesButton: UIButton!
    @IBOutlet weak var fourDevicesButton: UIButton!
    @IBOutlet weak var selectionStreamingLabel: UILabel!
    @IBOutlet weak var cambiarStreamingButton: UIButton!
    // CONSTRAINT
    @IBOutlet weak var separatorLastView: UIView!
    @IBOutlet weak var topPriceLabelMiddleConstraint: NSLayoutConstraint!
    @IBOutlet weak var topPriceLabelLastConstraint: NSLayoutConstraint!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var PlanLabel: UILabel!
    @IBOutlet weak var continueButton: UIButton!
    var alturaTVOriginal: CGFloat!
    var alturaInternetOriginal: CGFloat!
    var alturaStreamingOriginal: CGFloat!
    var mBestFitViewModel: BestFitViewModel!
    var oferta: String = "micronegocios"
    var velocitySelected: String = ""
    var planSelected: Plan!
    var isDoble: Bool = false
    var isMatchEstandar: Bool = false
    var isMatchPremium: Bool = false
    var isUnbox: Bool = false
    var isStreaming: Bool = false
    let checkImage = UIImage(named: "Image_rede_blue_check.png")
    let myImageView: UIImageView = UIImageView()
    var heightTvbuttons: CGFloat = 91.5
    var alturaTv: CGFloat = 0
    var alturaTvVSB: CGFloat = 0
    var alturaSinTv: CGFloat = 0
    var velocidadSelected = false
    var tipeTvSelected = false
    var streamingSelected = false
    let numberFormatter = NumberFormatter()

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        configNegocio()
        totalLabel.isHidden = true
        PlanLabel.isHidden = true
        netflixView.isHidden = true
        mBestFitViewModel = BestFitViewModel(view: self)
        mBestFitViewModel.atachView(observer: self)
        mBestFitViewModel.chargePlanes()
        alturaTVOriginal = heightTV.constant
        alturaInternetOriginal = heightInternet.constant
        alturaStreamingOriginal = heightStreaming.constant
        cambiarTVButton.isHidden = true
        showInternet()
        colapseTV(seleccion: "")
        colapseStreaming(seleccion: "")
        // TEMPORAL
        selectionTVLabel.alpha = 0.5
        selectionInternetLabel.alpha  =  0.5
        selectionStreamingLabel.alpha = 0.5
        cambiarInternetButton.isHidden = true
        cambiarStreamingButton.isHidden = true
        cambiarTVButton.isHidden = true
        //        if (faseConfigurarPlan.MicronegocioisEmpty == true || faseConfigurarPlan.ResidencialisEmpty == true) {
        //            Constants.Back(viewC: self)
        //        }
    }

    override func viewDidAppear(_ animated: Bool) {
        faseConfigurarPlan.adivinaQuien = true
        switch faseConfigurarPlan.flujo {
        case 0:
            separatorLastView.isHidden = false
            streamingView.isHidden = false
            view.layoutIfNeeded()
        case 1:
            separatorLastView.isHidden = true
            streamingView.isHidden = true
            view.layoutIfNeeded()
        default:
            break
        }
    }
    @IBAction func backAction(_ sender: Any) {
        SwiftEventBus.post("Cambia_paso_container", sender: 2)
    }
    // MARK: - Configuracion para Negocio

    func configNegocio() {
        if faseConfigurarPlan.isNegocio {
            streamingView.isHidden = true
            heightStreaming.constant = 0
            separatorLastView.isHidden = true
            oferta = "micronegocios"
        } else {
            streamingView.isHidden = false
            heightStreaming.constant = 301
            separatorLastView.isHidden = false
            oferta = "residencial"
        }
    }

    // MARK: - botones al seleccionar tipo tv
    @IBAction func tvPlusAction(_ sender: Any) {
        tipeTvSelected = true
        colapseTV(seleccion: "Totalplay TV+")
        buttonsLogic()
        seleccionaBoton(viewParent: vsbView, boton: tvPlusButton)
        isDoble = false
        var auxBanderas: [Bool] = [false, false, false]
        auxBanderas = mBestFitViewModel.filterForTV(tipeTV: "TRIPLE", isVsb: true)
        selectedTipeTV(auxBanderas)
    }

    @IBAction func tvAction(_ sender: UIButton) {
        tipeTvSelected = true
        colapseTV(seleccion: "Totalplay TV")
        buttonsLogic()
        seleccionaBoton(viewParent: tv4kView, boton: tvButton)
        isDoble = false
        var auxBanderas: [Bool] = [false, false, false]
        auxBanderas = mBestFitViewModel.filterForTV(tipeTV: "TRIPLE", isVsb: false)
        selectedTipeTV(auxBanderas)
    }

    @IBAction func withoutTVAction(_ sender: UIButton) {
        tipeTvSelected = true
        colapseTV(seleccion: "Sin TV")
        buttonsLogic()
        seleccionaBoton(viewParent: wiouthTvView, boton: withoutTVButon)
        isDoble = true
        var auxBanderas: [Bool] = [false, false, false]
        auxBanderas = mBestFitViewModel.filterForTV(tipeTV: "DOBLEPLAY", isVsb: false)
        selectedTipeTV(auxBanderas)
    }

    func selectedTipeTV(_ banderas: [Bool]) {
        if banderas[0] || banderas[1] || banderas[2] {
            streamingSelected = false
            showStreaming()
        } else {
//            streamingView.isHidden = true
            heightStreaming.constant = 0
            separatorLastView.isHidden = true
            isMatchEstandar = false
            isMatchPremium = false
            isUnbox = false
            cambiarStreamingButton.isHidden = false
            planSelected = mBestFitViewModel.filterForStreaming(isStreaming: false, Streaming: "")[0]
            updatePrice()
        }
    }
    // MARK: - logica de botones
    func buttonsLogic() {
        self.tvPlusButton.isSelected = false
        self.tvButton.isSelected = false
        self.withoutTVButon.isSelected = false
        myImageView.removeFromSuperview()
        self.tvPlusButton.borderColor = UIColor(named: "Base_rede_semiclear_gray")
        self.tvPlusButton.borderWidth = 2
        self.tvButton.borderColor = UIColor(named: "Base_rede_semiclear_gray")
        self.tvButton.borderWidth = 2
        self.withoutTVButon.borderColor = UIColor(named: "Base_rede_semiclear_gray")
        self.withoutTVButon.borderWidth = 2
    }

    func buttonsStreamingLogic() {
        self.netflixButton.isSelected = false
        self.amazonButton.isSelected = false
        self.notthingButtton.isSelected = false
        self.netflixButton.borderColor = UIColor(named: "Base_rede_semiclear_gray")
        self.netflixButton.borderWidth = 2
        self.amazonButton.borderColor = UIColor(named: "Base_rede_semiclear_gray")
        self.amazonButton.borderWidth = 2
        self.notthingButtton.borderColor = UIColor(named: "Base_rede_semiclear_gray")
        self.notthingButtton.borderWidth = 2
    }

    func buttonsDevicesLogic() {
        self.twoDevicesButton.isSelected = false
        self.fourDevicesButton.isSelected = false
        self.twoDevicesButton.borderColor = UIColor(named: "Base_rede_semiclear_gray")
        self.twoDevicesButton.borderWidth = 2
        self.fourDevicesButton.borderColor = UIColor(named: "Base_rede_semiclear_gray")
        self.fourDevicesButton.borderWidth = 2
    }

    func configureButton(button: UIButton, view: UIView) {
        seleccionaBoton(viewParent: view, boton: button)
    }

    // MARK: - funciones para cambiar la eleccion de las secciones

    @IBAction func cambiarAction(_ sender: Any) {
        streamingSelected = false
        tipeTvSelected = false
        showTV(mBestFitViewModel.filterForVelocity(megas: velocitySelected))
        if !faseConfigurarPlan.isNegocio {
            colapseStreaming(seleccion: "")
            buttonsStreamingLogic()
        }
        dismissPlan()
    }

    @IBAction func cambiarInternetAction(_ sender: Any) {
        tipeTvSelected = false
        streamingSelected = false
        showInternet()
        colapseTV(seleccion: "")
        buttonsLogic()
        if !faseConfigurarPlan.isNegocio {
            colapseStreaming(seleccion: "")
            buttonsStreamingLogic()
        }
        dismissPlan()
    }

    @IBAction func cambiarStreamingAction(_ sender: Any) {
        showStreaming()
        dismissPlan()
    }
    // MARK: - logica para mostrar u ocultar las secciones

    func showTV(_ banderas: [Bool]) {
        if banderas[0] {
            alturaSinTv = 80
            wiouthTvView.isHidden = false
        } else {
            alturaSinTv = 0
            wiouthTvView.isHidden = true
        }
        if banderas[1] {
            alturaTv = 80
            tv4kView.isHidden = false
        } else {
            alturaTv = 0
            tv4kView.isHidden = true
        }
        if banderas[2] {
            alturaTvVSB = 80
            vsbView.isHidden = false
        } else {
            alturaTvVSB = 0
            vsbView.isHidden = true
        }
        vsbViewHeight.constant = alturaTvVSB
        print("vsbViewHeight")
        print(vsbViewHeight.constant)
        tvViewHeight.constant = alturaTv
        print("tvViewHeight")
        print(tvViewHeight.constant)
        sinTvViewHeight.constant = alturaSinTv
        print("sinTvViewHeight")
        print(sinTvViewHeight.constant)
        buttonsHeight.constant = alturaTv + alturaTvVSB + alturaSinTv
        print("buttonsHeight")
        print(buttonsHeight.constant)
        myImageView.isHidden = false
        cambiarTVButton.isHidden = true
        ButtonsTV.isHidden = false
        tvTitle.isHidden = false
        heightTV.constant = heightTvbuttons + alturaTv + alturaTvVSB + alturaSinTv
        print("heightTV")
        print(heightTV.constant)
        selectionTVLabel.isHidden = true

    }

    func colapseTV(seleccion: String) {
        if tipeTvSelected {
            selectionTVLabel.text = seleccion
            cambiarTVButton.isHidden = false
            selectionTVLabel.alpha = 1
        } else {
            selectionTVLabel.text =  "Televisión"
            cambiarTVButton.isHidden = true
        }
        myImageView.isHidden = true
        selectionTVLabel.isHidden = false
        tvTitle.isHidden = true
        vsbView.isHidden = true
        tv4kView.isHidden = true
        wiouthTvView.isHidden = true
        ButtonsTV.isHidden = true
        heightTV.constant = 60
    }

    func showInternet() {
        cambiarInternetButton.isHidden = true
        internetTitle.isHidden = false
        heightInternet.constant = alturaInternetOriginal
        selectionInternetLabel.isHidden = true
        cambiarTVButton.isHidden = true
        cambiarStreamingButton.isHidden = true
    }

    func colapseInternet(seleccion: String, color: UIColor, Banderas: [Bool]) {
        selectionInternetLabel.alpha  =  1
        cambiarInternetButton.isHidden = false
        selectionInternetLabel.isHidden = false
        selectionInternetLabel.text = seleccion
        selectionInternetLabel.textColor = color
        internetTitle.isHidden = true
        heightInternet.constant = 41
        showTV(Banderas)
    }

    func showStreaming() {
        if faseConfigurarPlan.isNegocio {
            planSelected = mBestFitViewModel.filterForStreaming(isStreaming: false, Streaming: "")[0]
            updatePrice()
        } else {
            cambiarStreamingButton.isHidden = true
            streamingTitle.isHidden = false
            heightStreaming.constant = alturaStreamingOriginal
            selectionStreamingLabel.isHidden = true
            if selectionStreamingLabel.text!.contains(string: "NETFLIX") == true {
                showStack(show1: false, show2: true)
            } else {
                showStack(show1: true, show2: false)
            }
        }
    }

    func colapseStreaming(seleccion: String) {
        if streamingSelected {
            selectionStreamingLabel.text = seleccion
            cambiarStreamingButton.isHidden = false
        } else {
             cambiarStreamingButton.isHidden = true
            selectionStreamingLabel.text = "Sin streaming"
        }
        selectionStreamingLabel.alpha = 1
        selectionStreamingLabel.isHidden = false
        streamingTitle.isHidden = true
        heightStreaming.constant = 60
        devicesStack.isHidden = true
        noCombinarStack.isHidden = true
    }
    // MARK: - Botones de seleccion Streaming
    @IBAction func netflixAction(_ sender: Any) {
        buttonsDevicesLogic()
        devicesStack.isHidden = false
        noCombinarStack.isHidden = true
        buttonsStreamingLogic()
        seleccionaBoton(viewParent: netflixView, boton: netflixButton)
        isUnbox = false
    }
    @IBAction func amazonAction(_ sender: Any) {
        streamingSelected = true
        devicesStack.isHidden = true
        noCombinarStack.isHidden = false
        buttonsStreamingLogic()
        seleccionaBoton(viewParent: amazonView, boton: amazonButton)
        colapseStreaming(seleccion: "AMAZON PRIME")// AMAZON
        cambiarStreamingButton.isHidden = false
        isMatchEstandar = false
        isMatchPremium = false
        isUnbox = true
        planSelected = mBestFitViewModel.filterForStreaming(isStreaming: true, Streaming: "AMAZON")[0]
        updatePrice()
    }
    @IBAction func nothingAction(_ sender: Any) {
        streamingSelected = false
        buttonsStreamingLogic()
        seleccionaBoton(viewParent: noCombinarView, boton: notthingButtton)
        colapseStreaming(seleccion: "Sin streaming")
        isMatchEstandar = false
        isMatchPremium = false
        isUnbox = false
        cambiarStreamingButton.isHidden = false
        planSelected = mBestFitViewModel.filterForStreaming(isStreaming: false, Streaming: "")[0]
        updatePrice()
    }

    @IBAction func twoDevicesAction(_ sender: Any) {
        colapseStreaming(seleccion: "NETFLIX 2 pantallas")// NETFLIX ESTANDAR
        buttonsDevicesLogic()
        seleccionaBoton(viewParent: twoDevicesView, boton: twoDevicesButton)
        isMatchEstandar = true
        isMatchPremium = false
        cambiarStreamingButton.isHidden = false
        planSelected = mBestFitViewModel.filterForStreaming(isStreaming: true, Streaming: "NETFLIX ESTANDAR")[0]
        updatePrice()
    }

    @IBAction func fourDevicesAction(_ sender: Any) {
        colapseStreaming(seleccion: "NETFLIX 4 pantallas")// NETFLIX PREMIUM
        buttonsDevicesLogic()
        seleccionaBoton(viewParent: fourDevicesView, boton: fourDevicesButton)
        isMatchEstandar = false
        isMatchPremium = true
        cambiarStreamingButton.isHidden = false
        planSelected = mBestFitViewModel.filterForStreaming(isStreaming: true, Streaming: "NETFLIX PREMIUM")[0]
        updatePrice()
    }

    func showStack(show1: Bool, show2: Bool) {
        devicesStack.isHidden = show1
        noCombinarStack.isHidden = show2
    }

    private func verifyStreamingPlan() {
    }

    func dismissPlan() {
        totalLabel.isHidden = true
        PlanLabel.isHidden = true
        planSelected = nil
        continueButton.isEnabled = false
    }

    func updatePrice() {
        if planSelected == nil {
            totalLabel.isHidden = true
            PlanLabel.isHidden = true
        } else {
            totalLabel.isHidden = false
            PlanLabel.isHidden = false
            totalLabel.text = Double(planSelected.precioProntoPago).asLocaleCurrency + " /mes"
            PlanLabel.text = String("\(planSelected.nombrePaquete)")
            continueButton.isEnabled = true
        }
    }

    func seleccionaBoton(viewParent: UIView, boton: UIButton) {
        boton.borderColor = UIColor(named: "Base_rede_button_dark")
        boton.borderWidth = 3
        boton.isSelected  = true
        myImageView.contentMode = UIView.ContentMode.scaleAspectFit
        myImageView.frame.size.width = 20
        myImageView.frame.size.height = 20
        myImageView.frame.origin.y = boton.frame.origin.y - 5
        myImageView.frame.origin.x = boton.frame.origin.x + boton.width - 15
        myImageView.image = checkImage
        viewParent.addSubview(myImageView)
    }

    @IBAction func nextButtonAction(_ sender: Any) {

        fase.bug = true
        if planSelected != nil {
            faseConfigurarPlan.adivinaQuien = true
            SwiftEventBus.post("Cambia_paso_container", sender: 5)
            SwiftEventBus.post("receiveIDPlanbBestFit", sender: planSelected.id)
            faseSelectedPlan.plan1.idPlan = planSelected.id
            Amplitude.instance().logEvent("AdivinaQuien_Flow")
        }
    }
}
