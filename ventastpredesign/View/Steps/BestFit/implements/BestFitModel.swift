//
//  BestFitModel.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 27/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import RealmSwift

protocol BestFitModelObserver {
    func onSuccesPlanes(consultaPlanesResponse: ConsultaPlanesResponse)
    func onErrorPlanes(message: String)
}

class BestFitModel: BaseModel {

    var viewModel: BestFitModelObserver?
    var server: ServerDataManager2<ConsultaPlanesResponse>?
    var url = AppDelegate.API_SALESFORCE + ApiDefinition.API_PLANES_TP

    init(observer: BaseViewModelObserver) {
        super.init(observerVM: observer)
        server = ServerDataManager2<ConsultaPlanesResponse>()
    }

    func atachModel(viewModel: BestFitModelObserver) {
        self.viewModel = viewModel
    }

    func chargePlanes(_ request: ConsultaPlanessRequest) {
        url = AppDelegate.API_SALESFORCE + ApiDefinition.API_PLANES_TP
        server?.setValues(requestUrl: url, delegate: self, headerType: .headersSalesforce)
        server?.request(requestModel: request)
    }

    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if requestUrl == url {
            let planes = response as! ConsultaPlanesResponse
            if planes.result == "0" {
                viewModel?.onSuccesPlanes(consultaPlanesResponse: planes)
            } else {
                    viewModel?.onErrorPlanes(message: "No se encontraron datos")
            }
        }
    }

    override func onErrorLoadResponse(requestUrl: String, messageError: String) {

    }

}
