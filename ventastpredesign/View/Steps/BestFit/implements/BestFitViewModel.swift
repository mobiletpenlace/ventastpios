//
//  BestFitViewModel.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 27/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus

protocol BestFitObserver {
    func onSuccesChargePlanes(Velocidades: [String])
    func selectedVelocity(Velocidad: String)
    func backPropierties()
    func PrintMessage(message: String)
}

class BestFitViewModel: BaseViewModel, BestFitModelObserver {

    var observer: BestFitObserver?
    var model: BestFitModel!
    var mSitio: Sitio!
    var mProspecto: NProspecto!
    var estimulo: String?
    var isResidencial: Bool = true
    var planes: [Plan] = []
    var Striming: [ServiciosStriming] = []
    var residenciales: [Plan] = []
    var negocio: [Plan] = []
    var prepago: [Plan] = []
    var Familia: FamilyObject!
    var planSelected: Plan!
    var idPlanSelected: String!
    var existeModelado: Bool = false
    var infoAddons: InfoAdicionalesCreacion?
    var isOfertaNueva: Bool = true
    // new implementation
    var arrFilteredForVel: [Plan] = []
    var arrFilteredForTv: [Plan] = []
    var arrFilteredforStreaming: [Plan] = []

    // old implementation
    var arrTripleTradicional: [Plan] = []
    var arrTripleMatchBasico: [Plan] = []
    var arrTripleMatchEstandar: [Plan] = []
    var arrTripleMatchPremium: [Plan] = []
    var arrTripleUnbox: [Plan] = []
    var arrDobleTradicional: [Plan] = []
    var arrDobleMatch: [Plan] = []
    var arrDobleMatchEstandar: [Plan] = []
    var arrDobleMatchPremium: [Plan] = []
    var arrDobleUnbox: [Plan] = []
    var velocidades: [String] = []
    var striming: [ServiciosStriming] = []

    override init(view: BaseVentasView) {
        super.init(view: view)
        model = BestFitModel(observer: self)
        model.atachModel(viewModel: self)
    }

    func atachView(observer: BestFitObserver) {
        self.observer = observer
        SwiftEventBus.onMainThread(self, name: "VelocidadSelected") { [weak self] result in
            if let value = result?.object as? String {
                self?.observer?.selectedVelocity(Velocidad: value)
            }
        }

        SwiftEventBus.onMainThread(self, name: "backToPropierties") { [weak self] _ in
            // self?.view.dismiss(animated: true)
        }

    }

    func chargePlanes() {
        getSitio()
        getProspecto()
        let request: ConsultaPlanessRequest = ConsultaPlanessRequest()
        request.canal = mProspecto?.canalEmpleado
        request.subcanal = mProspecto?.subCanalEmpleado
        request.canalFront = "SalesforceWeb"
        request.cluster = mSitio?.cluster
        request.plazaTP = mSitio?.plaza
        request.distrito = mSitio?.distrito
        if mSitio?.isEstimulo == "true" {
            request.estimuloFiscal = true
        } else {
            request.estimuloFiscal = false
        }
        model.chargePlanes(request)
    }

    func getSitio() {
       // Intento de solucion
        if case let sitio: Sitio? = Sitio(value: RealManager.findFirst(object: Sitio.self)!) {
            mSitio = sitio
        } else {
            observer?.PrintMessage(message: "Fallo la carga del sitio")
        }
    }

    func getProspecto() {
        // Intento de solucion
        if case let prospecto: NProspecto? = NProspecto(value: RealManager.findFirst(object: NProspecto.self)!) {
            mProspecto = prospecto
        } else {
            observer?.PrintMessage(message: "Fallo la carga del prospecto")
        }
    }

    func onSuccesPlanes(consultaPlanesResponse: ConsultaPlanesResponse) {
        view.hideLoading()
        residenciales = []
        negocio = []
        planes = consultaPlanesResponse.planesTotalplay
        Striming = consultaPlanesResponse.serviciosStriming
        for plan in planes {
            if plan.tipoPlan == "Residencial" {
                if !plan.servicioStreaming.contains(string: "HS") {
                    residenciales.append(plan)
                }
            } else if plan.tipoPlan == "Micronegocios" {
                negocio.append(plan)
            }
        }
        if !faseConfigurarPlan.isNegocio {
            clasificaPlanes(Planes: residenciales)
            if residenciales.isEmpty {
                SwiftEventBus.post("Cambia_paso_container", sender: 2)
                faseConfigurarPlan.ResidencialisEmpty = true
            }
        } else {
            clasificaPlanes(Planes: negocio)
            if negocio.isEmpty {
                SwiftEventBus.post("Cambia_paso_container", sender: 2)
                faseConfigurarPlan.MicronegocioisEmpty = true
            }
        }

    }

    // Arreglo banderas posicion 0 = existen dobleplay, 1 = existe triple 4k, 2 = existe triple VideoSoundBox
    func filterForVelocity(megas: String) -> [Bool] {
        arrFilteredForVel = []
        var auxArr: [Plan] = []
        var banderas: [Bool] = [false, false, false]
        if faseConfigurarPlan.isNegocio {
            auxArr = negocio
        } else {
            auxArr = residenciales
        }
        for plan in auxArr {
            if plan.megas == megas {
                arrFilteredForVel.append(plan)
            }
        }
        for plan in arrFilteredForVel {
            if plan.familiaPaquete.uppercased().contains(string: "DOBLE") {
                banderas[0] = true
            }
            if plan.familiaPaquete.uppercased().contains(string: "TRIPLE") {
                banderas[1] = true
            }
            if plan.tipoTV.uppercased().contains(string: "VIDEO SOUNDBOX") {
                banderas[2] = true
            }
        }

        return banderas
    }

    // Arreglo banderas posicion 0 = existe Netflix 2 pantallas, 1 = existen Netflix 4 pantallas, 2 = existe amazon
    func filterForTV(tipeTV: String, isVsb: Bool) -> [Bool] {
        arrFilteredForTv = []
        var banderas: [Bool] = [false, false, false]
        for plan in arrFilteredForVel {
            if plan.familiaPaquete.uppercased().contains(string: tipeTV) {
                print(tipeTV)
                if isVsb {
                    if plan.tipoTV == "VIDEO SOUNDBOX" {
                        arrFilteredForTv.append(plan)
                    }
                } else {
                    if plan.tipoTV == "" {
                        arrFilteredForTv.append(plan)
                    }
                }
            }
        }

        for plan in arrFilteredForTv {
            if plan.familiaPaquete.uppercased().contains(string: "NETFLIX") {
                if plan.servicioStreaming.uppercased().contains(string: "ESTANDAR") {
                    banderas[0] = true
                }
                if plan.servicioStreaming.uppercased().contains(string: "PREMIUM") {
                banderas[1] = true
                }
            }
            if plan.familiaPaquete.uppercased().contains(string: "AMAZON") {
                banderas[2] = true
            }
        }
        return banderas
    }

    func filterForStreaming(isStreaming: Bool, Streaming: String) -> [Plan] {
        arrFilteredforStreaming = []
        for plan in arrFilteredForTv {
            if isStreaming {
                if plan.servicioStreaming.uppercased().contains(string: Streaming) {
                    arrFilteredforStreaming.append(plan)
                }
            } else if !plan.familiaPaquete.uppercased().contains(string: "NETFLIX")  && !plan.familiaPaquete.uppercased().contains(string: "AMAZON") {
                arrFilteredforStreaming.append(plan)
            }
        }
        return arrFilteredforStreaming
    }

    func onErrorPlanes(message: String) {
        view.hideLoading()
        observer?.PrintMessage(message: message)
    }

    func clasificaPlanes(Planes: [Plan]) {
        velocidades = []
        for plan in Planes {
            if plan.megas != "" {
                velocidades.append(plan.megas)// VIDEO SOUNDBOX
            }
        }
        clasificaVelocidades(arrVel: velocidades)
    }

    func clasificaVelocidades(arrVel: [String]) {
        var auxVel: [String] = []
        auxVel = arrVel
        velocidades = []

        for vel in auxVel {
            if velocidades.count > 0 {
                var equals = false
                for velocidad in velocidades {
                    if velocidad == vel {
                        equals = true
                    }
                }
                if !equals {
                        velocidades.append(vel)
                }
            } else {
                    velocidades.append(vel)
            }
        }
        observer?.onSuccesChargePlanes(Velocidades: velocidades)
    }

    func searchPlan(isDoble: Bool, isMatch: Bool, isUnbox: Bool, megas: String, Striming: Int) -> Plan {
    var auxPlan: Plan = arrTripleTradicional[0]
        auxPlan.detallePlan = "Default"
        var planAux: [Plan] = []
        if isDoble {
            if isMatch {
                if Striming == 1 {
                    planAux = arrDobleMatch
                } else if Striming == 2 {
                    planAux = arrDobleMatchEstandar
                } else if Striming == 4 {
                    planAux = arrDobleMatchPremium
                } else {
                    planAux = arrDobleTradicional
                }
            } else if isUnbox {
                planAux = arrDobleUnbox
            } else {
                planAux = arrDobleTradicional
            }
        } else {
            if isMatch {
                if Striming == 1 {
                    planAux = arrTripleMatchBasico
                } else if Striming == 2 {
                    planAux = arrTripleMatchEstandar
                } else if Striming == 4 {
                    planAux = arrTripleMatchPremium
                } else {
                    planAux = arrTripleTradicional
                }
            } else if isUnbox {
                planAux = arrTripleUnbox
            } else {
                planAux = arrTripleTradicional
            }
        }
        for plan in planAux {
            if plan.megas == megas {
                auxPlan = plan
            }
        }
        return auxPlan
    }

    func getVel() -> [String] {
        return velocidades
    }
}
