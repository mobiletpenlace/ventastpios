//
//  BestFitDelegate.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 27/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import SwiftEventBus
import SwiftUI

extension BestFitView: BestFitObserver {
    func selectedVelocity(Velocidad: String) {
       velocitySelected = Velocidad
        var auxBanderas: [Bool] = []
        auxBanderas = mBestFitViewModel.filterForVelocity(megas: Velocidad)
        colapseInternet(seleccion: String("\(Velocidad) megas"), color: UIColor(named: "Base_rede_init_" + Velocidad + "_" + oferta) ?? UIColor(netHex: 0), Banderas: auxBanderas)
        velocidadSelected = true
        // updatePrice()
    }

    func backPropierties() {
        Constants.Back(viewC: self)
    }
    func onSuccesChargePlanes(Velocidades: [String]) {
        let VelocidadesToInt = Velocidades.map { Int($0)!}
        rellenaPlanes(VelocidadesToInt.sorted(by: >).map { String($0)})
    }

    func PrintMessage(message: String) {
        AlertDialog.show(title: "Error", body: message, view: self)
    }

    func rellenaPlanes(_ planes: [String]) {
        var contador = 0
        let residuo = planes.count % 2
        var cellDisplay = planes.count

        if residuo != 0 {
            contador = (planes.count / 2) + 1
        } else {
            contador = planes.count / 2
        }

        let width = (self.view.bounds.size.width - 30) / 2
        let cellHeight: CGFloat = width / 1.5
        let totalHeight = CGFloat(contador) * cellHeight
        for index in 1 ..< contador + 1 {
           if let view1 = Bundle.main.loadNibNamed("PackageCollectionViewCell", owner: self, options: nil)?.first as? PackageCollectionViewCell {
               print(index)
            view1.setData(planes[(index * 2) - 2], oferta )
            viewContentMegas.addSubview(view1)
            view1.frame.size.height = width / 1.5
            view1.frame.size.width = width
            view1.frame.origin.y = CGFloat(index - 1) * CGFloat(width / 1.5)
            view1.frame.origin.x = CGFloat(10)
               cellDisplay -= 1
            }
            if (index ) == contador && cellDisplay == 0 {
                self.hideLoading()
            } else {
                if let view2 = Bundle.main.loadNibNamed("PackageCollectionViewCell", owner: self, options: nil)?.first as? PackageCollectionViewCell {
                    view2.setData(planes[(index * 2) - 1], oferta)
                    viewContentMegas.addSubview(view2)
                    view2.frame.size.height = cellHeight
                    view2.frame.size.width = width
                    view2.frame.origin.y = CGFloat(index - 1) * cellHeight
                    view2.frame.origin.x = CGFloat(20) + width
                    cellDisplay -= 1
                 }
                if cellDisplay == 0 {
                    self.hideLoading()
                }
            }
        }
        heightInternet.constant = totalHeight + 41
        internetView.height = totalHeight + 41
        alturaInternetOriginal = totalHeight + 41
        viewContentMegas.height = totalHeight + 41
    }
}
