//
//  Servicio.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 28/05/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper
import RealmSwift

class Servicio: Object, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        Id <- map["Id"]
        precio <- map["precio"]
        nombre <- map["nombre"]
        mostrarAddon <- map["mostrarAddon"]
        maximoAgregar <- map["maximoAgregar"]
        llevaATA <- map["llevaATA"]
        imagen <- map["imagen"]
        id_DP_PlanServicioEquipo <- map["id_DP_PlanServicioEquipo"]
    }

    override static func primaryKey() -> String? {
        return "uui"
    }

    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  Id: String = ""
    @objc dynamic var  precio: Float = 0.0
    @objc dynamic var  nombre: String = ""
    @objc dynamic var  mostrarAddon: Bool = false
    @objc dynamic var  maximoAgregar: Int = 0
    @objc dynamic var  llevaATA: Bool = false
    @objc dynamic var  imagen: String = ""
    @objc dynamic var  id_DP_PlanServicioEquipo: String = ""
    var cantidad: Int = 0

}
