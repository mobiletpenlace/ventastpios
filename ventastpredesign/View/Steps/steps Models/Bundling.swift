//
//  Bundling.swift
//  SalesCloud
//
//  Created by aestevezn on 21/07/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

class Bundling: NSObject, Mappable {

    var id: String = ""
    var promociones: [Promocion] = []
    var nombre: String = ""
    var descuento: Float = 0.0
    var imagen: String = ""
    var descripcion: String = ""

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        id <- map["id"]
        promociones <- map["promociones"]
        nombre <- map["nombre"]
        descuento <- map["montoDescuento"]
        imagen <- map["imagen"]
        descripcion <- map["descripcion"]
    }
}
