//
//  Plan.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 29/05/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper
import RealmSwift

class Plan: Object, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        tipoPlan <- map["tipoPlan"]
        tipoOferta <- map["tipoOferta"]
        tipoContrato <- map["tipoContrato"]
        precioProntoPago <- map["precioProntoPago"]
        precioLista <- map["precioLista"]
        plazo <- map["plazo"]
        nombrePaquete <- map["nombrePaquete"]
        megas <- map["megas"]
        imagenPaquete <- map["imagenPaquete"]
        id <- map["id"]
        familiaPaquete <- map["familiaPaquete"]
        detallePlan <- map["detallePlan"]
        servicioStreaming <- map["servicioStreaming"]
        tipoTV <- map["tipoTV"]
        addonBundle <- map["addonBundle"]
    }

    override static func primaryKey() -> String? {
        return "uui"
    }

    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  tipoPlan: String = ""
    @objc dynamic var  tipoOferta: String = ""
    @objc dynamic var  tipoContrato: String = ""
    @objc dynamic var  precioProntoPago: Float = 0.0
    @objc dynamic var  precioLista: Float = 0.0
    @objc dynamic var  plazo: String = ""
    @objc dynamic var  nombrePaquete: String = ""
    @objc dynamic var  megas: String = ""
    @objc dynamic var  imagenPaquete: String = ""
    @objc dynamic var  id: String = ""
    @objc dynamic var  familiaPaquete: String = ""
    @objc dynamic var  detallePlan: String = ""
    @objc dynamic var  servicioStreaming: String = ""
    @objc dynamic var  tipoTV: String = ""
    @objc dynamic var addonBundle: String = ""
}
