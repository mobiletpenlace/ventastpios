//
//  Membresia.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 08/02/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper
import RealmSwift

class Membresia: NSObject, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        urlImagenM <- map["urlImagenM"]
        numDispositivos <- map["numDispositivos"]
        nombreMembresia <- map["nombreMembresia"]
    }

   var  urlImagenM: String = ""
   var  numDispositivos: String = ""
   var  nombreMembresia: String = ""

}
