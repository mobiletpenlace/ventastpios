//
//  Bundlings.swift
//  SalesCloud
//
//  Created by aestevezn on 21/07/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

class Bundlings: NSObject, Mappable {

    var bundlings: [Bundling] = []

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        bundlings <- map["bundlings"]
    }
}
