//
//  Promociones.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 28/05/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper
import RealmSwift

class Promociones: NSObject, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        promociones <- map["promociones"]
    }

    var  promociones: [Promocion] = []

}
