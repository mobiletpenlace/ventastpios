//
//  File.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 28/05/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//
import UIKit

import ObjectMapper
import RealmSwift

class NProspecto: Object, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        codigoPostal <- map["codigoPostal"]
        idCodigoPostal <- map["idCodigoPostal"]
        colonia <- map["colonia"]
        ciudad <- map["ciudad"]
        estado <- map["estado"]
        delegacionMunicipio <- map["delegacionMunicipio"]
        calle <- map["calle"]
        numExterior <- map["numExterior"]
        numInterior <- map["numInterior"]
        plusCode <- map["plusCode"]
        refCalle <- map["refCalle"]
        refUrbana <- map["refUrbana"]
        canalEmpleado <- map["canalEmpleado"]
        subCanalEmpleado <- map["subcanalEmpleado"]
        esEdificio <- map["esEdificio"]
        esVentaExpress <- map["esVentaExpress"]
        tipoPersona <- map["tipoPersona"]
        generoPersona <- map ["generoPersona"]
        firstName <- map["firstName"]
        midleName <- map["midleName"]
        lastName <- map["lastName"]
        company <- map["company"]
        fechaNacimiento <- map["fechaNacimiento"]
        rfc <- map["rfc"]
        telefono <- map["telefono"]
        celular <- map["celular"]
        otroTelefono <- map["otroTelefono"]
        correo <- map["correo"]
        otroCoreo <- map["otroCoreo"]
        idEmpleado <- map["idEmpleado"]
        tipoMoneda <- map["tipoMoneda"]
        estimuloFiscal <- map["estimuloFiscal"]
        aceptaPromoModelo <- map["aceptaPromoModelo"]
        esNegocio <- map["EsNegocio"]
        esExtranjero <- map["esExtranjero"]
        esVIP <- map["esVIP"]
        idioma <- map["idioma"]
        aforo <- map["Aforo"]
        sector <- map["sector"]
        subsector <- map["subsector"]
        nombreNegocio <- map["NombreNegocio"]
        validarFactibilidad <- map["validarFactibilidad"]
    }

    override static func primaryKey() -> String? {
        return "uui"
    }

    @objc dynamic var uui: String = ""
    @objc dynamic var codigoPostal: String = ""
    @objc dynamic var idCodigoPostal: String = ""
    @objc dynamic var colonia: String = ""
    @objc dynamic var ciudad: String = ""
    @objc dynamic var estado: String = ""
    @objc dynamic var delegacionMunicipio: String = ""
    @objc dynamic var calle: String = ""
    @objc dynamic var numExterior: String = ""
    @objc dynamic var numInterior: String = ""
    @objc dynamic var plusCode: String = ""
    @objc dynamic var refCalle: String = ""
    @objc dynamic var refUrbana: String = ""
    @objc dynamic var canalEmpleado: String = ""
    @objc dynamic var subCanalEmpleado: String = ""
    @objc dynamic var esEdificio: Bool = false
    @objc dynamic var esVentaExpress: Bool = faseDataConsult.isExpressAcepted
    @objc dynamic var tipoPersona: String = ""
    @objc dynamic var generoPersona: String = ""
    @objc dynamic var firstName: String = ""
    @objc dynamic var midleName: String = ""
    @objc dynamic var lastName: String = ""
    @objc dynamic var company: String = ""
    @objc dynamic var fechaNacimiento: String = ""
    @objc dynamic var rfc: String = ""
    @objc dynamic var curp: String = ""
    @objc dynamic var telefono: String = ""
    @objc dynamic var celular: String = ""
    @objc dynamic var nip: String = ""
    @objc dynamic var otroTelefono: String = ""
    @objc dynamic var correo: String = ""
    @objc dynamic var otroCoreo: String = ""
    @objc dynamic var idEmpleado: String = ""
    @objc dynamic var tipoMoneda: String = ""
    @objc dynamic var estimuloFiscal: Bool = false
    @objc dynamic var aceptaPromoModelo: Bool = false
    @objc dynamic var esNegocio: Bool = false
    @objc dynamic var esExtranjero: Bool = false
    @objc dynamic var esVIP: Bool = false
    @objc dynamic var idioma: String = String.emptyString
    @objc dynamic var aforo: String = String.emptyString
    @objc dynamic var sector: String = String.emptyString
    @objc dynamic var validarFactibilidad: Bool = false
    @objc dynamic var nombreNegocio: String = String.emptyString
    @objc dynamic var subsector: String = String.emptyString
}
