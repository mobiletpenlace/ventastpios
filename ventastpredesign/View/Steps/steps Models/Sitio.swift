//
//  Sitio.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 28/05/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper
import RealmSwift

class Sitio: Object, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        categoryService <- map["categoryService"]
        cluster <- map["cluster"]
        tipoCobertura <- map["tipoCobertura"]
        distrito <- map["distrito"]
        factibilidad <- map["factibilidad"]
        latitud <- (map["latitud"], StringCastTransform())
        longitud <- (map["longitud"], StringCastTransform())
        plaza <- map["plaza"]
        region <- map["region"]
        regionId <- map["regionId"]
        zona <- map["zona"]
        // cliente unico
        tipoInmueble <- map["tipoInmueble"]
        estatusInmueble <- map["estatusInmueble"]
        // lotes y atributos
        loteId <- map["loteId"]
        codigoLote <- map["codigoLote"]
        tipoLote <- map["tipoLote"]
        viviendaId <- map["viviendaId"]
        tipoPredio <- map["tipoPredio"]
        numViviendas <- map["numViviendas"]
        numNiveles <- map["numNiveles"]
        areaPertenencia <- map["areaPertenencia"]
        numTorres <- map["numTorres"]
        nombre <- map["nombre"]
        sectorComercial <- map["sectorComercial"]
        numViviendasLocales <- map["numViviendasLocales"]
        clusterComercial <- map["clusterComercial"]
        AnchoBandaSubida <- map["AnchoBandaSubida"]
        AnchoBandaBajada <- map["AnchoBandaBajada"]
        PuertoDisponible <- map["PuertoDisponible"]
        AsignaPuerto <- map["AsignaPuerto"]
        Tecnologia <- map["Tecnologia"]
        CreacionTKPlantaExt <- map["CreacionTKPlantaExt"]
    }

    // @SerializedName("AnchoBandaSubida")
//    public String speedUpload;
//    @SerializedName("AnchoBandaBajada")
//    public String speedDownload;
//    @SerializedName("PuertoDisponible")
//    public String enablePorts;
//    @SerializedName("AsignaPuerto")
//    public String assingPort;
//    @SerializedName("Tecnologia")
//    public String technology;
//    @SerializedName("CreacionTKPlantaExt")
//    public Boolean createTKExtPlant;

    override static func primaryKey() -> String? {
        return "uui"
    }

    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  categoryService: String = ""
    @objc dynamic var  cluster: String = ""
    @objc dynamic var  tipoCobertura: String = ""
    @objc dynamic var  distrito: String = ""
    @objc dynamic var  factibilidad: String = ""
    @objc dynamic var  latitud: String = ""
    @objc dynamic var  longitud: String = ""
    @objc dynamic var  plaza: String = ""
    @objc dynamic var  region: String = ""
    @objc dynamic var  regionId: String = ""
    @objc dynamic var  zona: String = ""
    @objc dynamic var  isEstimulo: String = ""
    // cliente unico
    @objc dynamic var  tipoInmueble: String = ""
    @objc dynamic var  estatusInmueble: String = ""
    // Lotes y atributos
    @objc dynamic var  loteId: String = ""
    @objc dynamic var  codigoLote: String = ""
    @objc dynamic var  tipoLote: String = ""
    @objc dynamic var  viviendaId: String = ""
    @objc dynamic var  tipoPredio: String = ""
    @objc dynamic var  numViviendas: String = ""
    @objc dynamic var  numNiveles: String = ""
    @objc dynamic var  areaPertenencia: String = ""
    @objc dynamic var  numTorres: String = ""
    @objc dynamic var  nombre: String = ""
    @objc dynamic var  sectorComercial: String = ""
    @objc dynamic var  numViviendasLocales: String = ""
    @objc dynamic var  clusterComercial: String = ""
    @objc dynamic var  AnchoBandaSubida: String = ""
    @objc dynamic var  AnchoBandaBajada: String = ""
    @objc dynamic var  PuertoDisponible: String = ""
    @objc dynamic var  AsignaPuerto: String = ""
    @objc dynamic var  Tecnologia: String = ""
    @objc dynamic var  CreacionTKPlantaExt: String = ""
    @objc dynamic var  typePlan: String = ""
    @objc dynamic var  action: String = ""

}
