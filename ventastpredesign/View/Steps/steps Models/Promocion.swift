//
//  Promocion.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 28/05/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper
import RealmSwift

class Promocion: Object, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        Id <- map["Id"]
        tipoReferencia <- map["tipoReferencia"]
        requiereOpp <- map["requiereOpp"]
        nombre <- map["nombre"]
        montoDescuento <- map["montoDescuento"]
        imagen <- map["imagen"]
        esAutomatica <- map["esAutomatica"]
        bloqueaEfectivo <- map["bloqueaEfectivo"]
        adicionalProductoNombre <- map["adicionalProductoNombre"]
        adicionalProductoId <- map["adicionalProductoId"]
    }

    override static func primaryKey() -> String? {
        return "uui"
    }

    @objc dynamic var  uui: String = UUID().uuidString
    @objc dynamic var  Id: String = ""
    @objc dynamic var  tipoReferencia: String = ""
    @objc dynamic var  requiereOpp: String = ""
    @objc dynamic var  nombre: String = ""
    @objc dynamic var  montoDescuento: Float = 0.0
    @objc dynamic var  imagen: String = ""
    @objc dynamic var  esAutomatica: String = ""
    @objc dynamic var  bloqueaEfectivo: String = ""
    @objc dynamic var  adicionalProductoNombre: String = ""
    @objc dynamic var  adicionalProductoId: String = ""

}
