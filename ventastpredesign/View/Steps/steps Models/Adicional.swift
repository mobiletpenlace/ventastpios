//
//  Adicional.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 28/05/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift

class Adicional: NSObject, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        Id <- map["Id"]
        precio <- map["precio"]
        nombre <- map["nombre"]
        maximoAgregar <- map["maximoAgregar"]
        imagen <- map["imagen"]
        tipoProducto <- map["tipoProducto"]
        grupoycategoria <- map["grupoycategoria"]
    }

    var  Id: String = ""
    var  precio: Float = 0.0
    var  nombre: String = ""
    var  maximoAgregar: Int = 0
    var  imagen: String = ""
    var  tipoProducto: String = ""
    var  grupoycategoria: [Grupoycategoria] = []
}

class Grupoycategoria: NSObject, Mappable {

    public required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        posicion <- map["posicion"]
        grupo <- map["grupo"]
        categoria <- map["categoria"]
        adicional <- map["adicional"]
    }

    var  posicion: String = ""
    var  grupo: String = ""
    var  categoria: String = ""
    var  adicional: String = ""
}
