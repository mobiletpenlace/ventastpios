//
//  serviciosStriming.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 08/02/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper
import RealmSwift

class ServiciosStriming: NSObject, Mappable {

    required init?(map: Map) {

    }

    func mapping(map: Map) {
        urlImagen <- map["urlImagen"]
        nombreServicio <- map["nombreServicio"]
        membresias <- map["membresias"]
    }

    var  urlImagen: String = ""
    var  nombreServicio: String = ""
    var  membresias: [Membresia] = []
}
