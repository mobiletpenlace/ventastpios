//
//  scanCard.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 09/03/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//
import UIKit
import Foundation

// protocol GetDataIneDelegate {
//    func getInfoINE(_ ineFront: String, _ frontINEImage:UIImage, _ ineBack: String, _ backINEImage:UIImage)
// }

class ScanCard: BaseVentasView, UIImagePickerControllerDelegate & UINavigationControllerDelegate, ConsultINEDataDelegate {
    func OnSuccessConsultINEData(mConsultINEDataResponse: ConsultINEDataResponse) {
        Logger.println("on succes consult ine data")
    }

    @IBOutlet weak var frontINEImage: UIImageView!
    @IBOutlet weak var backINEImage: UIImageView!
    @IBOutlet var viewGral: UIView!
    @IBOutlet weak var sendIDLabel: UILabel!

    @IBAction func closeAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)

    }
    @IBAction func idFrontAction(_ sender: Any) {
        self.alertTakePhoto(value: "1")
        self.view.layoutIfNeeded()
    }

    @IBAction func idBackAction(_ sender: Any) {
        self.view.layoutIfNeeded()
        self.alertTakePhoto(value: "2")

    }

    var getDataIneDelegate: GetDataIneDelegate!
    var mConsultINEDataPresenter: ConsultINEDataPresenter!
    var imagePickerController: UIImagePickerController!
    var num = "0"
    var imagenID = ""
    var imagenIdReverso = ""
    var frontImage: UIImage!
    var backImage: UIImage!

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)

    }

    @IBAction func uploadAction(_ sender: Any) {
        if imagenID != "" && imagenIdReverso != "" {
            getDataIneDelegate!.getInfoINE(imagenID, frontImage, imagenIdReverso, backImage)

        } else {
            Logger.println("NO CONSULTAR")
        }
        self.dismiss(animated: true, completion: nil)
    }

    func alertTakePhoto(value: String) {

        let alert = UIAlertController(title: "Elige una opción", message: "", preferredStyle: .actionSheet)
        let action1 = UIAlertAction(title: "Tomar foto", style: .default, handler: { (_) -> Void in
            self.takePhoto(number: value)
        })
        let action2 = UIAlertAction(title: "Abrir de Galeria", style: .default, handler: { (_) -> Void in
            self.takeGalery(number: value)
        })

        let action3 = UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.cancel, handler: nil)
        action3.setValue(UIColor.red, forKey: "titleTextColor")
        alert.view.tintColor = UIColor.black
        alert.addAction(action1)
        alert.addAction(action2)
        alert.addAction(action3)

        present(alert, animated: true, completion: {
            alert.view.superview?.isUserInteractionEnabled = true
            alert.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertControllerBackgroundTapped)))
            alert.view.layoutIfNeeded()
        })
    }

    @objc func alertControllerBackgroundTapped() {
        self.dismiss(animated: true, completion: nil)
    }

    func takePhoto(number: String) {
        num = number
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.sourceType = .camera
            self.present(imagePickerController, animated: true, completion: nil)
            imagePickerController.title = "FOTO FRONTAL"
        }
    }

    func takeGalery(number: String) {
        num = number
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
            imagePickerController.title = "FOTO FRONTAL"
        }
    }
}

extension ScanCard {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String: Any]) {
        imagePickerController.dismiss(animated: true, completion: nil)

        if num == "1" {
            frontINEImage.alpha = 1
            frontINEImage.image = (info[UIImagePickerControllerOriginalImage] as! UIImage)
            imagenID = convertImageTobase64(image: (info[UIImagePickerControllerOriginalImage] as? UIImage)!)!
            frontImage = info[UIImagePickerControllerOriginalImage] as? UIImage

        } else {
            backINEImage.alpha = 1
            backINEImage.image = info[UIImagePickerControllerOriginalImage] as? UIImage
            imagenIdReverso = convertImageTobase64(image: (info[UIImagePickerControllerOriginalImage] as? UIImage)!)!
            backImage = info[UIImagePickerControllerOriginalImage] as? UIImage

        }
        if imagenID == "" || imagenIdReverso == "" {
            sendIDLabel.text = "OMITIR"
        } else {
            sendIDLabel.text = "ENVIAR"
        }
    }

    public func imageWithSize(image: UIImage, size: CGSize) -> UIImage {
        var scaledImageRect = CGRect.zero
        let aspectWidth: CGFloat = size.width / image.size.width
        let aspectHeight: CGFloat = size.height / image.size.height
        let aspectRatio: CGFloat = min(aspectWidth, aspectHeight)
        scaledImageRect.size.width = image.size.width * aspectRatio
        scaledImageRect.size.height = image.size.height * aspectRatio
        scaledImageRect.origin.x = (size.width - scaledImageRect.size.width) / 2.0
        scaledImageRect.origin.y = (size.height - scaledImageRect.size.height) / 2.0
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        image.draw(in: scaledImageRect)
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return scaledImage!
    }

    func convertBase64ToImage(imageString: String) -> UIImage {
        let imageData = Data(base64Encoded: imageString, options: Data.Base64DecodingOptions.ignoreUnknownCharacters)!
        return UIImage(data: imageData)!
    }

    func convertImageTobase64(image: UIImage) -> String? {
        var imageData: Data?
        var imageDataantes: Data?
        imageDataantes  = UIImageJPEGRepresentation(image, 1)
        let widthInPixels = image.size.width * image.scale
        let heightInPixels = image.size.height * image.scale
        print("tamaño de la imagen original en pixeles \(widthInPixels) x \(heightInPixels)")
        let imageSize1: Int = imageDataantes!.count
        let heightTemp = (300 * heightInPixels) / widthInPixels
        print(heightTemp)
        print("size of image antes de in KB: %f ", Double(imageSize1) / 1024.0)
        imageData  = UIImageJPEGRepresentation(imageWithSize(image: image, size: CGSize(width: 300, height: heightTemp)), 1)
        // size image
        let imageSize: Int = imageData!.count
        print("size of image despues de in KB: %f ", Double(imageSize) / 1024.0)
        //
        return imageData?.base64EncodedString()
    }
}
