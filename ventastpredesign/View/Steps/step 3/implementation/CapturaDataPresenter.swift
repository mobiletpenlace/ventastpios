//
//  CapturaDataPresenter.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 17/04/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus

protocol CapturaDataDelegate {
    func capturePersonPicker(index: String)
    func captureIdentificationPicker(index: String)
    func captureFavoriteChannelPicker(index: String)
    func captureDomain1Picker(index: String)
    func captureDomain2Picker(index: String)
    func captureChannelPicker(index: String)
    func capturePointSalePicker(index: String)
}

class CapturaDataPresenter {
    var view: CapturaDataDelegate?

    init() {
    }

    func attachView(view: CapturaDataDelegate) {
        self.view = view
        SwiftEventBus.onMainThread(self, name: "CapturePersonPicker") { [weak self] result in
            if let value = result?.object as? String {
                self?.view?.capturePersonPicker(index: value)
            }
        }

        SwiftEventBus.onMainThread(self, name: "CaptureIdentificationPicker") { [weak self] result in
            if let value = result?.object as? String {
                self?.view?.captureIdentificationPicker(index: value)
            }
        }

        SwiftEventBus.onMainThread(self, name: "CaptureFavoriteChannelPicker") { [weak self] result in
            if let value = result?.object as? String {
                self?.view?.captureFavoriteChannelPicker(index: value)
            }
        }

        SwiftEventBus.onMainThread(self, name: "CaptureDomain1Picker") { [weak self] result in
            if let value = result?.object as? String {
                self?.view?.captureDomain1Picker(index: value)
            }
        }

        SwiftEventBus.onMainThread(self, name: "CaptureDomain2Picker") { [weak self] result in
            if let value = result?.object as? String {
                self?.view?.captureDomain2Picker(index: value)
            }
        }

        SwiftEventBus.onMainThread(self, name: "CaptureChannelPicker") { [weak self] result in
            if let value = result?.object as? String {
                self?.view?.captureChannelPicker(index: value)
            }
        }

        SwiftEventBus.onMainThread(self, name: "CapturePointSalePicker") { [weak self] result in
            if let value = result?.object as? String {
                self?.view?.capturePointSalePicker(index: value)
            }
        }
    }

    func viewDidAppear() {

    }

    func viewDidShow() {

    }

    func detachView() {

    }

}
