//
//  ConsultPointSalePresenter.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 16/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

protocol ConsultPointSaleDelegate: NSObjectProtocol {
    func OnSuccessConsultPointSale(mConsultPointSaleResponse: ConsultPointSaleResponse)
}
class ConsultPointSalePresenter: BaseVentastpredesignPresenter {
    var mConsultPointSaleDelegate: ConsultPointSaleDelegate!

    func GetConsultPointSale(mConsultPointSaleDelegate: ConsultPointSaleDelegate) {
        self.mConsultPointSaleDelegate = mConsultPointSaleDelegate

        let pointSaleRequest = ConsultPointSaleRequest()
        pointSaleRequest.MLogin = Login()
        pointSaleRequest.MLogin?.Ip = "1.1.1.1"
        pointSaleRequest.MLogin?.User = "25631"
        pointSaleRequest.MLogin?.SecretWord = "Middle100$"

        RetrofitManager<ConsultPointSaleResponse>.init(requestUrl: ApiDefinition.API_SALES_POINT, delegate: self).request(requestModel: pointSaleRequest)
    }
    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if requestUrl == ApiDefinition.API_SALES_POINT {
            view.hideLoading()
            OnSuccessConsultPointSale(consultPointSaleResponse: response as! ConsultPointSaleResponse)
        }
    }

    func OnSuccessConsultPointSale(consultPointSaleResponse: ConsultPointSaleResponse) {
        if consultPointSaleResponse.MResult?.IdResult != "1" {
            if consultPointSaleResponse.MArrSubCanal.count > 0 {
                mConsultPointSaleDelegate.OnSuccessConsultPointSale(mConsultPointSaleResponse: consultPointSaleResponse)
            }
        } else if consultPointSaleResponse.MResult?.IdResult == nil {
            Constants.Alert(title: "", body: dialogs.Error.InternService, type: type.Error, viewC: mViewController)
        } else {
            Constants.Alert(title: "", body: dialogs.Error.InternService, type: type.Error, viewC: mViewController)
        }

    }

}
