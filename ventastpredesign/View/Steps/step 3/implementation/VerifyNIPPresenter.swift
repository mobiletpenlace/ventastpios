//
//  VerifyNIPPresenter.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 7/9/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import UIKit

protocol CreacionNIPDelegate: NSObjectProtocol {
    func OnSuccessCreacionNIP(mCreacionNIPResponse: CreacionNIPResponse)
}
class VerifyNIPPresenter: BaseVentastpredesignPresenter {
    weak var mCreacionNIPDelegate: CreacionNIPDelegate!

    func GetCreacionNIP(mTelefono: String, mCreacionNIPDelegate: CreacionNIPDelegate) {
        self.mCreacionNIPDelegate = mCreacionNIPDelegate
        let consultRequest = CreacionNIPRequest()
        consultRequest.McreacionNIP = CreacionNIP()
        consultRequest.McreacionNIP?.telefono = Security.crypt(text: mTelefono)
        consultRequest.MLogin = Login()
        consultRequest.MLogin?.Ip = "1.1.1.1"
        consultRequest.MLogin?.User = "25631"
        consultRequest.MLogin?.SecretWord = "Middle100$"

        RetrofitManager<CreacionNIPResponse>.init(requestUrl: ApiDefinition.API_NIP, delegate: self).request(requestModel: consultRequest)
    }
    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if requestUrl == ApiDefinition.API_NIP {
            view.hideLoading()
            OnSuccessCreacionNIP(creacionNIPResponse: response as! CreacionNIPResponse)
        }
    }

    func OnSuccessCreacionNIP(creacionNIPResponse: CreacionNIPResponse) {
        if creacionNIPResponse.MWS_creacionNIP_SF_Response?.nip != "" && creacionNIPResponse.MWS_creacionNIP_SF_Response?.nip  != nil {
            mCreacionNIPDelegate.OnSuccessCreacionNIP(mCreacionNIPResponse: creacionNIPResponse)
        } else {
            Constants.Alert(title: "", body: dialogs.Error.NIP, type: type.Error, viewC: mViewController)
        }

    }

}
