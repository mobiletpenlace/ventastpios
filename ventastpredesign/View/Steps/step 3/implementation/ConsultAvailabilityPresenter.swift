//
//  ConsultAvailabilityPresenter.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 02/05/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

protocol ConsultAvailabilityDelegate: NSObjectProtocol {
    func OnSuccessConsultAvailability(mConsultAvailabilityResponse: ConsultAvailabilityResponse)
    func notAvailability()
}
class ConsultAvailabilityPresenter: BaseVentastpredesignPresenter {
    weak var mConsultAvailabilityDelegate: ConsultAvailabilityDelegate!

    func GetConsultAvailability(mCiudad: String, mDistrito: String, mRegion: String, mZona: String, mCluster: String, mConsultAvailabilityDelegate: ConsultAvailabilityDelegate) {
        self.mConsultAvailabilityDelegate = mConsultAvailabilityDelegate

        let consultRequest = ConsultAvailabilityRequest()

        consultRequest.UnidadNegocio = "1"
        consultRequest.Ciudad = mCiudad
        consultRequest.cluster = mCluster
        consultRequest.Distrito = mDistrito
        consultRequest.TipoDeIntalacion = "51"
        consultRequest.MLogin = Login()
        consultRequest.MLogin?.Ip = "1.1.1.1"
        consultRequest.MLogin?.User = "25631"
        consultRequest.MLogin?.SecretWord = "Middle100$"
        consultRequest.Region = mRegion
        consultRequest.Zona = mZona

        RetrofitManager<ConsultAvailabilityResponse>.init(requestUrl: ApiDefinition.availability, delegate: self).request(requestModel: consultRequest)
    }
    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if requestUrl == ApiDefinition.availability {
            view.hideLoading()
            OnSuccessConsultAvailability(consultAvailabilityResponse: response as! ConsultAvailabilityResponse)
        }
    }

    func OnSuccessConsultAvailability(consultAvailabilityResponse: ConsultAvailabilityResponse) {
        if consultAvailabilityResponse.IdResult != "0" && consultAvailabilityResponse.IdResult != nil {
            mConsultAvailabilityDelegate.notAvailability()

        } else {
            mConsultAvailabilityDelegate.OnSuccessConsultAvailability(mConsultAvailabilityResponse: consultAvailabilityResponse)

        }

    }

}
