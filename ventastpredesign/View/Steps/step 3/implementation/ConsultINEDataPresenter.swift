//
//  ConsultINEDataPresenter.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 08/03/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

protocol ConsultINEDataDelegate: NSObjectProtocol {
    func OnSuccessConsultINEData(mConsultINEDataResponse: ConsultINEDataResponse)
}
class ConsultINEDataPresenter: BaseVentastpredesignPresenter {
    weak var mConsultINEDataDelegate: ConsultINEDataDelegate!

    override init(view: BaseVentasView) {
        super.init(view: view)
    }

    override func onRequestWs() {

    }

    func GetConsultINEData(mId: String, mIdReverso: String, mConsultINEDataDelegate: ConsultINEDataDelegate) {
     // view.showLoading(message: "Consultado datos de identificación")
        self.mConsultINEDataDelegate = mConsultINEDataDelegate
        let consultRequest = ConsultINEDataRequest()

        consultRequest.id = mId
        consultRequest.idReverso = mIdReverso

        RetrofitManager<ConsultINEDataResponse>.init(requestUrl: ApiDefinition.ine, delegate: self).request(requestModel: consultRequest)
    }
    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if requestUrl == ApiDefinition.ine {
            OnSuccessConsultINEData(consultINEDataResponse: response as! ConsultINEDataResponse)
            view.hideLoading()
        }
    }

    func OnSuccessConsultINEData(consultINEDataResponse: ConsultINEDataResponse) {
        if consultINEDataResponse.codigoValidacion == nil {
            Constants.Alert(title: "", body: dialogs.Info.NoExpressSale, type: type.Info, viewC: mViewController)
        } else {
            mConsultINEDataDelegate.OnSuccessConsultINEData(mConsultINEDataResponse: consultINEDataResponse)
        }

    }

}
