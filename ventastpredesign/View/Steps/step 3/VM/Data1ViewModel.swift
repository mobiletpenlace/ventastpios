//
//  Data1ViewModel.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 14/01/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus

protocol Data1Observer {
    func validar()
    func setTipoPersona(tipo: String)
    func setGenero(genero: String)
    func setEstadoCivil(estadoCivil: String)
    func setJob(job: String)
    func setNationality(nationality: String)
    func loadFromRealm(prospecto: NProspecto)

}

class Data1ViewModel {
    var observer: Data1Observer?
    init() {
    }

    func attachView(observer: Data1Observer) {
        self.observer = observer
        SwiftEventBus.onMainThread(self, name: "Validate_Data1") { [weak self] _ in
            self!.observer!.validar()
        }

        SwiftEventBus.onMainThread(self, name: "Set_TipoPersonaPicker") { [weak self] result in
            if let value = result?.object as? String {
                self?.observer?.setTipoPersona(tipo: value)
            }
        }

        SwiftEventBus.onMainThread(self, name: "Set_JobsPicker") { [weak self] result in
            if let value = result?.object as? String {
                self?.observer?.setJob(job: value)
            }
        }

        SwiftEventBus.onMainThread(self, name: "Set_NationalityPicker") { [weak self] result in
            if let value = result?.object as? String {
                self?.observer?.setNationality(nationality: value)
            }
        }

        SwiftEventBus.onMainThread(self, name: "Set_EstadoCivilPicker") { [weak self] result in
            if let value = result?.object as? String {
                self?.observer?.setEstadoCivil(estadoCivil: value)
            }
        }

        SwiftEventBus.onMainThread(self, name: "RemoveEventBusP3") { [weak self] _ in
                self?.removeEventBus()
            }

        }

    func removeEventBus() {
        SwiftEventBus.unregister(self)
    }

    func Siguiente() {
        SwiftEventBus.post("Siguiente-Data1", sender: nil)
    }

    func loadFromRealm() {
        if RealManager.findFirst(object: NProspecto.self) != nil {
            let prospecto = NProspecto(value: RealManager.findFirst(object: NProspecto.self)!)
            observer?.loadFromRealm(prospecto: prospecto)
        }
    }

    func guardaData1(_ nombre: String, _ apellidoP: String, _ apellidoM: String, _ fecha: String, _ rfc: String, _ curp: String, _ razon: String, _ tipoPersona: String) {
        let mProspecto = NProspecto()
        mProspecto.firstName = nombre
        mProspecto.lastName = apellidoM
        mProspecto.fechaNacimiento = fecha
        mProspecto.rfc = rfc
        mProspecto.curp = curp
        mProspecto.company = razon
        mProspecto.tipoPersona = tipoPersona
        SwiftEventBus.post("Datos_Data1", sender: mProspecto)
        Siguiente()
    }

    func viewDidAppear() {

    }

    func viewDidShow() {

    }

    func detachView() {

    }

}
