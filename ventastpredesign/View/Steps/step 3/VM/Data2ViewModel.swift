//
//  Data2ViewModel.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 14/01/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus

protocol Data2Observer {
    func validar()
    func loadFromRealm(prospecto: NProspecto)
}

class Data2ViewModel: BaseViewModel {
    var observer: Data2Observer?

    override init(view: BaseVentasView) {
        super.init(view: view)
    }

    func attachView(observer: Data2Observer) {
        self.observer = observer
        self.observer = observer
        SwiftEventBus.onMainThread(self, name: "Validate_Data2") { [weak self] _ in
            self!.observer!.validar()
        }
        SwiftEventBus.onMainThread(self, name: "RemoveEventBusP3") { [weak self] _ in
            self?.removeEventBus()
        }

    }

    func removeEventBus() {
        SwiftEventBus.unregister(self)
    }

    func guardaData2(_ correo: String, _ correoSecundario: String, _ telefono: String, _ celular: String, _ nip: String) {
        let mProspecto = NProspecto()
        mProspecto.correo = correo
        mProspecto.otroCoreo = correoSecundario
        mProspecto.telefono = telefono
        mProspecto.celular = celular
        mProspecto.nip = nip

        SwiftEventBus.post("Datos_Data2", sender: mProspecto)
    }

    func loadFromRealm() {
        if RealManager.findFirst(object: NProspecto.self) != nil {
           observer?.loadFromRealm(prospecto: NProspecto(value: RealManager.findFirst(object: NProspecto.self)!))
        }
    }

    func viewDidAppear() {

    }

    func viewDidShow() {

    }

    func detachView() {

    }

}
