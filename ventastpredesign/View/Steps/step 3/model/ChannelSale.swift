//
//  ChannelSale.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 12/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class ChannelSale: NSObject, Mappable {
    required init?(map: Map) {

    }

    func mapping(map: Map) {
        Canal <- map["Canal"]

    }

    var Canal: String?

}
