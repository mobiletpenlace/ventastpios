//
//  Dia.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 02/05/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import UIKit

import ObjectMapper

class Dia: NSObject, Mappable {
    required init?(map: Map) {

    }

    func mapping(map: Map) {
        Fecha <- map["Fecha"]
        Matutino <- map["Matutino"]
        Vespertino <- map["Vespertino"]
    }

    override init() {

    }
    var Fecha: String?
    var Matutino: String?
    var Vespertino: String?

}
