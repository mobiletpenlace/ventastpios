//
//  Disponibilidad.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 02/05/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class Disponibilidad: NSObject, Mappable {
    required init?(map: Map) {
    }

    func mapping(map: Map) {
        MDia <- map["Dia"]
    }

    override init() {

    }
    var MDia: Dia?

}
