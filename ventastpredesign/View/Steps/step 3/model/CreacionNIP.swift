//
//  CreacionNIP.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 7/9/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//
import UIKit

import ObjectMapper

class CreacionNIP: NSObject, Mappable {

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    func mapping(map: Map) {
        telefono <- map["telefono"]

    }

    var telefono: String?

}
