//
//  ArrSubCanal.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 16/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import UIKit

import ObjectMapper

class ArrSubCanal: NSObject, Mappable {
    required init?(map: Map) {

    }

    func mapping(map: Map) {
        SubCanal <- map["SubCanal"]

    }

    override init() {

    }
    var SubCanal: String?

}
