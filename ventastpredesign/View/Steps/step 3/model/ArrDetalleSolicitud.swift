//
//  ArrDetalleSolicitud.swift
//  ventastpredesign
//
//  Created by Arturo Ceron Epigmenio on 22/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class ArrDetalleSolicitud: NSObject, Mappable {
    required init?(map: Map) {

    }
    func mapping(map: Map) {
        NoContrato <- map["NoContrato"]
        IdOportunidad <- map["IdOportunidad"]
        MotivoRechazoMC <- map["MotivoRechazoMC"]
        FechaLiberacion <- map["FechaLiberacion"]
        Estatus <- map["Estatus"]
        SubEstatus <- map["SubEstatus"]
    }
    override init() {

    }
    var NoContrato: String?
    var IdOportunidad: String?
    var MotivoRechazoMC: String?
    var FechaLiberacion: String?
    var Estatus: String?
    var SubEstatus: String?
}
