//
//  WS_creacionNIP_SF_Response.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 7/9/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class WS_creacionNIP_SF_Response: NSObject, Mappable {
    required init?(map: Map) {

    }

    func mapping(map: Map) {
        nip <- map["nip"]
        result <- map["result"]
        rtesultDescription <- map["rtesultDescription"]
    }

    override init() {

    }
    var nip: String?
    var result: String?
    var rtesultDescription: String?

}
