//
//  ConsultINEDataResponse.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 08/03/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class ConsultINEDataResponse: BaseResponse {

    required init?(map: Map) {

    }

    override func mapping(map: Map) {

         tipo <- map["tipo"]
         subTipo <- map["subTipo"]
         folio <- map["folio"]
         registro <- map["registro"]
         claveElector <- map["claveElector"]
         curp <- map["curp"]
         estado <- map["estado"]
         municipio <- map["municipio"]
         localidad <- map["localidad"]
         seccion <- map["seccion"]
         emision <- map["emision"]
         vigencia <- map["vigencia"]
        fechaNacimiento <- map["fechaNacimiento"]
        primerApellido <- map["primerApellido"]
         segundoApellido <- map["segundoApellido"]
         nombres <- map["nombres"]
         edad <- map["edad"]
         sexo <- map["sexo"]
         calle <- map["calle"]
         colonia <- map["colonia"]
         ciudad <- map["ciudad"]
         codigoValidacion <- map["codigoValidacion"]
         ocr <- map["ocr"]

    }

    var tipo: String?
    var subTipo: String?
    var folio: String?
    var registro: String?
    var claveElector: String?
    var curp: String?
    var estado: String?
    var municipio: String?
    var localidad: String?
    var seccion: String?
    var emision: String?
    var vigencia: String?
    var fechaNacimiento: String?
    var primerApellido: String?
    var segundoApellido: String?
    var nombres: String?
    var edad: String?
    var sexo: String?
    var calle: String?
    var colonia: String?
    var ciudad: String?
    var codigoValidacion: String?
    var ocr: String?

}
