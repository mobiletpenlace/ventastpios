//
//  CreacionNIPResponse.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 7/9/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class CreacionNIPResponse: BaseResponse {

    required init?(map: Map) {

    }

    override func mapping(map: Map) {
        MResponse <- map["response"]
        MWS_creacionNIP_SF_Response <- map["WS_creacionNIP_SF_Response"]

    }
    override init() {

    }

    var MResponse: Response?
    var MWS_creacionNIP_SF_Response: WS_creacionNIP_SF_Response?

}
