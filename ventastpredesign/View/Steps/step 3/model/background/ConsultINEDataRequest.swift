//
//  ConsultINEDataRequest.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 08/03/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class ConsultINEDataRequest: BaseRequest {

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }
    override func mapping(map: Map) {
        id <- map["id"]
        idReverso <- map["idReverso"]
    }

    var id: String?
    var idReverso: String?
}
