//
//  CreacionNIPRequest.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 7/9/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class CreacionNIPRequest: BaseRequest {

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }
    override func mapping(map: Map) {
        MLogin <- map["Login"]
        McreacionNIP <- map["creacionNIP"]
    }

    var MLogin: Login?
    var McreacionNIP: CreacionNIP?

}
