//
//  ConsultAvailabilityRequest.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 02/05/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class ConsultAvailabilityRequest: BaseRequest {

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }
    override func mapping(map: Map) {
        UnidadNegocio <- map["UnidadNegocio"]
        Ciudad <- map["Ciudad"]
        cluster <- map["cluster"]
        Distrito <- map["Distrito"]
        TipoDeIntalacion <- map["TipoDeIntalacion"]
        MLogin <- map["Login"]
        Region <- map["Region"]
        Zona <- map["Zona"]
    }

    var UnidadNegocio: String?
    var Ciudad: String?
    var Distrito: String?
    var TipoDeIntalacion: String?
    var MLogin: Login?
    var Region: String?
    var Zona: String?
    var cluster: String?
}
