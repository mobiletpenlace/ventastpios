//
//  ConsultPointSaleResponse.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 16/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class ConsultPointSaleResponse: BaseResponse {

    required init?(map: Map) {

    }

    override func mapping(map: Map) {
        MResult <- map["Result"]
        MArrSubCanal <- map["ArrSubCanal"]

    }
    override init() {

    }

    var MResult: Result?
    var MArrSubCanal: [ArrSubCanal] = []

}
