//
//  ConsultAvailabilityResponse.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 02/05/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class ConsultAvailabilityResponse: BaseResponse {
    required init?(map: Map) {

    }

    override func mapping(map: Map) {
        IdResult <- map["IdResult"]
        ResultDescription <- map["ResultDescription"]
        UnidadNegocio <- map["UnidadNegocio"]
        TipoDeIntalacion <- map["TipoDeIntalacion"]
        Region <- map["Region"]
        Ciudad <- map["Ciudad"]
        Distrito <- map["Distrito"]
        Zona <- map["Zona"]
        MDisponibilidad <- map["Disponibilidad"]
    }

    var IdResult: String?
    var ResultDescription: String?
    var UnidadNegocio: String?
    var TipoDeIntalacion: String?
    var Region: String?
    var Ciudad: String?
    var Distrito: String?
    var Zona: String?
    var MDisponibilidad: Disponibilidad?

}
