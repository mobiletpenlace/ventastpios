//
//  EndSaleViewController.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 06/07/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class EndSaleViewController: BaseVentasView {
    @IBOutlet weak var CuentaBRM: UILabel!

    var noCuenta: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        CuentaBRM.attributedText = NSAttributedString(string: noCuenta, attributes: [.kern: 7])
    }

    @IBAction func goToHomeAction(_ sender: Any) {
        SwiftEventBus.post("Limpieza_general")
        Constants.LoadStoryBoard(storyboard: "HomeScreenRedesign", identifier: "HomeScreenRedesign", viewC: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func viewDidAppear(_ animated: Bool) {
        SwiftEventBus.unregister(self, name: "Set_TipoPersonaPicker")
        SwiftEventBus.unregister(self, name: "Set_SectorPicker")
        SwiftEventBus.unregister(self, name: "Set_SubsectorPicker")
        SwiftEventBus.unregister(self, name: "Envia_data_toStart")
        SwiftEventBus.unregister(self, name: "CheckFields")
        SwiftEventBus.unregister(self, name: "SetName_Data")
        SwiftEventBus.unregister(self, name: "EnviaComprobante")
        SwiftEventBus.unregister(self, name: "EnviaData_DataVC")
    }

}
