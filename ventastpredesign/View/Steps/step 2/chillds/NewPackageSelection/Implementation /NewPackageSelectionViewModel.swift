//
//  NewPackageSelectionViewModel.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 04/02/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import SwiftEventBus

protocol NewPackageSelectionObserver {
    func printVelocidades()
    func DefineMegas(megas: String)
}

class NewPackageSelectionViewModel: BaseViewModel, NewPackageSelectionModelObserver {

    var mObserver: NewPackageSelectionObserver!
    var model: NewPackageSelectionModel!
    var arrTripleTradicional: [Plan] = []
    var arrTripleMatchBasico: [Plan] = []
    var arrTripleMatchEstandar: [Plan] = []
    var arrTripleMatchPremium: [Plan] = []
    var arrTripleUnbox: [Plan] = []
    var arrDobleTradicional: [Plan] = []
    var arrDobleMatch: [Plan] = []
    var arrDobleMatchEstandar: [Plan] = []
    var arrDobleMatchPremium: [Plan] = []
    var arrDobleUnbox: [Plan] = []
    var velocidades: [String] = []
    var striming: [ServiciosStriming] = []

    override init(view: BaseVentasView) {
        super.init(view: view)
        model = NewPackageSelectionModel(observer: self)
        model.atachModel(viewModel: self)
    }

    func atachView(observer: NewPackageSelectionObserver) {
        self.mObserver = observer
        SwiftEventBus.onMainThread(self, name: "Define_megas") { [weak self] result in
            if let Megas = result?.object as? String {
                self?.mObserver.DefineMegas(megas: Megas)
            }
        }
    }

   func removeEventBus() {
       SwiftEventBus.unregister(self)
   }

    func clasificaPlanes(Planes: [Plan], Striming: [ServiciosStriming]) {
        arrTripleUnbox = []
        arrTripleTradicional = []
        arrTripleMatchEstandar = []
        arrDobleUnbox = []
        arrDobleTradicional = []
        arrDobleMatch = []
        velocidades = []
        striming = Striming

        for plan in Planes {
            if plan.megas != "" {
                velocidades.append(plan.megas)
            }
            if plan.familiaPaquete.uppercased().contains(string: "DOBLE") {
                if plan.familiaPaquete.uppercased().contains(string: "NETFLIX") {
                    if plan.servicioStreaming.uppercased().contains(string: "BASICO") {
                        arrDobleMatch.append(plan)
                    }
                    if plan.servicioStreaming.uppercased().contains(string: "ESTANDAR") {
                        arrDobleMatchEstandar.append(plan)
                    }
                    if plan.servicioStreaming.uppercased().contains(string: "PREMIUM") {
                        arrDobleMatchPremium.append(plan)
                    }
                }
                if plan.familiaPaquete.uppercased().contains(string: "AMAZON") {
                    arrDobleUnbox.append(plan)
                }
                if !plan.familiaPaquete.uppercased().contains(string: "NETFLIX") && !plan.familiaPaquete.uppercased().contains(string: "AMAZON") {
                    arrDobleTradicional.append(plan)
                }
            } else if plan.familiaPaquete.uppercased().contains(string: "TRIPLE") {
                if plan.familiaPaquete.uppercased().contains(string: "NETFLIX") {
                    if plan.servicioStreaming.uppercased().contains(string: "BASICO") {
                        arrTripleMatchBasico.append(plan)
                    }
                    if plan.servicioStreaming.uppercased().contains(string: "ESTANDAR") {
                        arrTripleMatchEstandar.append(plan)
                    }
                    if plan.servicioStreaming.uppercased().contains(string: "PREMIUM") {
                        arrTripleMatchPremium.append(plan)
                    }
                }
                if plan.familiaPaquete.uppercased().contains(string: "AMAZON") {
                    arrTripleUnbox.append(plan)
                }
                if !plan.familiaPaquete.uppercased().contains(string: "NETFLIX")  && !plan.familiaPaquete.uppercased().contains(string: "AMAZON") {
                    arrTripleTradicional.append(plan)
                }
            }
        }
        clasificaVelocidades(arrVel: velocidades)
        Logger.println("\(velocidades)")
    }

    func clasificaVelocidades(arrVel: [String]) {
        var auxVel: [String] = []
        auxVel = arrVel
        velocidades = []

        for vel in auxVel {
            if velocidades.count > 0 {
                var equals = false
                for velocidad in velocidades {
                    if velocidad == vel {
                        equals = true
                    }
                }
                if !equals {
                    velocidades.append(vel)
                }
            } else {
                velocidades.append(vel)
            }
        }
        mObserver.printVelocidades()
    }

    func searchPlan(isDoble: Bool, isMatch: Bool, isUnbox: Bool, megas: String, Striming: Int) -> Plan {
    var auxPlan: Plan = arrTripleTradicional[0]
        auxPlan.detallePlan = "Default"
        var planAux: [Plan] = []
        if isDoble {
            if isMatch {
                if Striming == 1 {
                    planAux = arrDobleMatch
                } else if Striming == 2 {
                    planAux = arrDobleMatchEstandar
                } else if Striming == 4 {
                    planAux = arrDobleMatchPremium
                } else {
                    planAux = arrDobleTradicional
                }
            } else if isUnbox {
                planAux = arrDobleUnbox
            } else {
                planAux = arrDobleTradicional
            }
        } else {
            if isMatch {
                if Striming == 1 {
                    planAux = arrTripleMatchBasico
                } else if Striming == 2 {
                    planAux = arrTripleMatchEstandar
                } else if Striming == 4 {
                    planAux = arrTripleMatchPremium
                } else {
                    planAux = arrTripleTradicional
                }
            } else if isUnbox {
                planAux = arrTripleUnbox
            } else {
                planAux = arrTripleTradicional
            }
        }
        for plan in planAux {
            if plan.megas == megas {
                auxPlan = plan
            }
        }
        return auxPlan
    }

    func getVel() -> [String] {
        return velocidades
    }
}
