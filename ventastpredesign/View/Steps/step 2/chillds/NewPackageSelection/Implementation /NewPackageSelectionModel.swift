//
//  NewPackageSelectionModel.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 04/02/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import RealmSwift

protocol NewPackageSelectionModelObserver {
}

class NewPackageSelectionModel: BaseModel {

    var viewModel: NewPackageSelectionModelObserver?

    init (observer: BaseViewModelObserver) {
        super.init(observerVM: observer)
    }

    func atachModel(viewModel: NewPackageSelectionModelObserver) {
        self.viewModel = viewModel
    }

}
