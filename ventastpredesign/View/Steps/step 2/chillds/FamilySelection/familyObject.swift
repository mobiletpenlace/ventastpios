//
//  familyObject.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 12/01/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class FamilyObject: NSObject {

    var Family: [Plan] = []
    var Nombre: String!
    var Deal: String!
    var isNegocio: Bool!

    init(_ family: [Plan], _ nombre: String, _ deal: String, _ isNegocio: Bool) {
        self.Family = family
        self.Nombre = nombre
        self.Deal = deal
        self.isNegocio = isNegocio
    }
}
