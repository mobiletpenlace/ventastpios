//
//  ConfigurePlanViewModel.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 15/06/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import SwiftEventBus

protocol ConfigurePlanObserver {
    func onSuccesAddons()
    func actualizaTotalPrecio(precio: Float)
    func actualizaTotalProductos(total: Int)
    func actualizaServiciosResumen(servicios: [Servicio])
    func actualizaProductosResumen(productos: [Adicional])
    func actualizaPromocionesResumen(promociones: [Promocion])
    func ocultaServicios()
    func ocultaProductos()
    func ocultaPromociones()
    func retiraResumen()
}

class ConfigurePlanViewModel: BaseViewModel, ConfigurePlanModelObserver {

    var mObserver: ConfigurePlanObserver!
    var model: ConfigurePlanModel!
    var mPlan: Plan!
    var mSitio: Sitio!
    var mProspecto: NProspecto!
    var mServicios: [Servicio] = []
    var mProductos: [Adicional] = []
    var mPromociones: [Promocion] = []
    var selectedServicios: [Servicio] = []
    var selectedProductos: [Adicional] = []
    var selectedPromociones: [Promocion] = []
    var infoAddons: InfoAdicionalesCreacion = InfoAdicionalesCreacion()
    var totalPrice: Float = 0.0
    var totalDiscount: Float = 0.0
    var totalProductos: Int = 0
    var CostoInstalacion: Float = 0.0
    var promoValidacion: Promocion!
    var mProductosAdd = ProductosAdd()
    var mPromocionesAdd = PromocionesAdd()
    var mServiciosAdd = ServiciosAdd()

    override init(view: BaseVentasView) {
        super.init(view: view)
        model = ConfigurePlanModel(observer: self)
        model.atachModel(viewModel: self)
    }

    func atachView(observer: ConfigurePlanObserver) {
        self.mObserver = observer
        SwiftEventBus.onMainThread(self, name: "Agrega_servicio") { [weak self] result in
            if let servicio = result?.object as? Servicio {
                self?.selectedServicios.append(servicio)
                self?.actualiza()
            }
        }

        SwiftEventBus.onMainThread(self, name: "Retira_servicio") { [weak self] result in
            if let servicio = result?.object as? Servicio {
                self?.eliminaServicio(servicio)
            }
        }

        SwiftEventBus.onMainThread(self, name: "Agrega_Addon") { [weak self] result in
            if let addon = result?.object as? Adicional {
                self?.selectedProductos.append(addon)
                self?.actualiza()
            }
        }
        SwiftEventBus.onMainThread(self, name: "Retira_Addon") { [weak self] result in
            if let addon = result?.object as? Adicional {
                self?.eliminaAddon(addon)
                self?.actualiza()
            }
        }

        SwiftEventBus.onMainThread(self, name: "Agrega_Promocion2") { [weak self] result in
            if let promocion = result?.object as? Promocion {
                self?.validaConvivencia(promocion: promocion)
            }
        }

        SwiftEventBus.onMainThread(self, name: "Retira_Promocion") { [weak self] result in
            if let promocion = result?.object as? Promocion {
                self?.eliminaPromo(promocion)
            }
        }

        SwiftEventBus.onMainThread(self, name: "RetiraResumen") { [weak self] _ in
            self?.mObserver.retiraResumen()
        }

        SwiftEventBus.onMainThread(self, name: "RemoveEventBus") { [weak self] _ in
            self?.removeEventBus()
        }
    }

   func removeEventBus() {
       SwiftEventBus.unregister(self)
   }

    // MARK: - devoluciones al view

    func getServicios() -> [Servicio] {
        return mServicios
    }

    func getProductos() -> [Adicional] {
        return mProductos
    }

    func getPromociones() -> [Promocion] {
        return mPromociones
    }

    func getCosto() -> Float {
        return CostoInstalacion
    }

    // MARK: - peticiones al model

    func BuscaAdoons(_ plan: Plan) {
        mPlan = plan
        mSitio = model.getSitio()
        mProspecto = model.getProspecto()
        let request = ConsultaAddonsRequest()
        dump(mProspecto)
        request.idPlan = plan.id
        request.info.canal = mProspecto.canalEmpleado
        request.info.cluster = mSitio.cluster
        request.info.colonia = mProspecto.colonia
        request.info.distrito = mSitio.distrito
        request.info.metodoPago = "Efectivo"
        request.info.plaza = mSitio.plaza
        request.info.codigoPostal = mProspecto.codigoPostal
        request.info.subcanal = mProspecto.subCanalEmpleado
        request.info.subcanalVenta = "Cambaceo"
        request.info.consultaAddons = true
        request.info.consultaServ = true
        request.info.consultaPromo = true
        if mSitio.isEstimulo == "true" {
            request.info.estimulo = true
        } else {
            request.info.estimulo = false
        }
        model.chargeAddons(request)
    }

    func validaConvivencia(promocion: Promocion) {
        promoValidacion = promocion
        let request: ConsultaConvivenciaRequest = ConsultaConvivenciaRequest()
        request.idPlan = "0pr3C000000002vQAA"
        request.promocionesNew.append(promocion.Id)
        for promo in selectedPromociones {
            request.promocionesOld.append(promo.Id)
        }
        model.getConvivencia(request)
    }

    func creaModelado() {
        if selectedServicios.count != 0 || selectedProductos.count != 0 || selectedPromociones.count != 0 {
            infoAddons = InfoAdicionalesCreacion()
            mProductosAdd = ProductosAdd()
            mPromocionesAdd = PromocionesAdd()
            mServiciosAdd = ServiciosAdd()
            mProductosAdd.productos.removeAll()
            mPromocionesAdd.promociones.removeAll()
            mServiciosAdd.servicios.removeAll()
            var acumulatedServicios: [Servicio] = []
            for servicio in selectedServicios {
                var conteo: Int = 0
                for serv in selectedServicios {
                    if servicio.Id == serv.Id {
                        conteo += 1
                    }
                }
                if conteo > 1 {
                    if acumulatedServicios.count == 0 {
                        let auxServ = servicio
                        auxServ.cantidad = conteo
                        acumulatedServicios.append(auxServ)
                    } else {
                        var exist: Bool = false
                        for serv in acumulatedServicios {
                            if serv.Id == servicio.Id {
                                exist = true
                            }
                        }
                        if !exist {
                            let auxServ = servicio
                            auxServ.cantidad = conteo
                            acumulatedServicios.append(auxServ)
                        }
                    }
                } else {
                    let auxServ = servicio
                    auxServ.cantidad = 1
                    acumulatedServicios.append(auxServ)
                }

            }

            for servicio in acumulatedServicios {
                let servicioCrea: ServiciosCreacion = ServiciosCreacion()
                servicioCrea.cantidad = String("\(servicio.cantidad)")
                servicioCrea.id = servicio.Id
                mServiciosAdd.servicios.append(servicioCrea)
                dump(mServiciosAdd.servicios)
            }

            for producto in selectedProductos {
                let productoCrea: ProductosCreacion =  ProductosCreacion()
                productoCrea.id = producto.Id
                mProductosAdd.productos.append(productoCrea)
            }

            for promocion in selectedPromociones {
                let promocionCrea: PromocionesCreacion = PromocionesCreacion()
                promocionCrea.id = promocion.Id
                promocionCrea.requiereAutomatica = false
                if promocion.esAutomatica == "true" {
                    promocionCrea.esAutomatica = true
                } else {
                    promocionCrea.esAutomatica = false
                }
                mPromocionesAdd.promociones.append(promocionCrea)
            }
            infoAddons.productosAdd = mProductosAdd
            infoAddons.serviciosAdd = mServiciosAdd
            infoAddons.promocionesAdd = mPromocionesAdd
            model.setInfoAddons(infoAddons)
            SwiftEventBus.post("Addons_cargados", sender: infoAddons)
            SwiftEventBus.post("Modelado_cargado", sender: mPlan)
            SwiftEventBus.post("Valida_Oportunidad", sender: nil)
        } else {
            SwiftEventBus.post("Modelado_cargado", sender: mPlan)
            SwiftEventBus.post("Valida_Oportunidad", sender: nil)
        }
    }

    // MARK: - Load from Realm

    func LoadFromRealm() {

        if TPAddonsSelectedShared.shared.findAddonsPrev() != nil {
            infoAddons = InfoAdicionalesCreacion()
            infoAddons = model.getInfo()
            recuperaAddons()
        } else {
            view.hideLoading()
        }

        /*
        if RealManager.findFirst(object: InfoAdicionalesCreacion.self) != nil {
            infoAddons = InfoAdicionalesCreacion()
            infoAddons = model.getInfo()
            recuperaAddons()
        } else {
            view.hideLoading()
        }*/
    }

    func recuperaAddons() {
        selectedServicios = []
        selectedProductos = []
        selectedPromociones = []

        for servicioRecuperado in infoAddons.serviciosAdd!.servicios {
            let serv: ServiciosCreacion = ServiciosCreacion()
            serv.id = servicioRecuperado.id
            serv.cantidad = servicioRecuperado.cantidad!
            let auxString = NumberFormatter().number(from: servicioRecuperado.cantidad!)
            for _ in 1...auxString!.intValue {
                SwiftEventBus.post("AgregaServicioCelda", sender: servicioRecuperado.id)
            }
        }

        if infoAddons.productosAdd == nil {
            infoAddons.productosAdd = ProductosAdd()
        }
        for productoRecuperado in infoAddons.productosAdd!.productos {
            SwiftEventBus.post("AgregaProductoCelda", sender: productoRecuperado.id)
        }

        for promocionRecuperada in infoAddons.promocionesAdd!.promociones {
            SwiftEventBus.post("AgregaPromoCelda", sender: promocionRecuperada.id)
        }
        view.hideLoading()
    }

    // MARK: - model delegate
    func onSuccesAddons(response: ConsultaAddonsResponse) {
        mServicios = response.infoAddons.serviciosAdd!.servicios
        for agrupacion in response.infoAddons.productosAdd!.productos {
            if agrupacion.Agrupacion != "HOGAR SEGURO" {
                for adicional in agrupacion.adicional {
                    mProductos.append(adicional)
                }
            }
        }
        CostoInstalacion = 0.0
        for agrupacion in response.infoAddons.costoInstalacionAdd!.productos {
            if agrupacion.Agrupacion != "HOGAR SEGURO" {
                for adicional in agrupacion.adicional {
                    CostoInstalacion = CostoInstalacion + adicional.precio
                }
            }
        }
        mPromociones = response.infoAddons.promocionesAdd!.promociones
        mObserver.onSuccesAddons()
        actualiza()
        LoadFromRealm()

        dump(mProductos)

    }

    func onSuccesConvivencia() {
        view.hideLoading()
        selectedPromociones.append(promoValidacion)

        if promoValidacion.adicionalProductoId != "" {
            agregaProductoIncluido(promoValidacion.adicionalProductoId)
        }
        actualiza()
    }

    func onNotConvivencia(message: String) {
        view.hideLoading()
        Constants.Alert(title: "Aviso", body: message, type: type.Error, viewC: view)
        SwiftEventBus.post("RetiraPromoCelda", sender: promoValidacion.Id)
    }

    // MARK: - funciones

    func agregaProductoIncluido(_ producto: String) {
        SwiftEventBus.post("AgregaProductoCelda", sender: producto)
    }

    func retiraProductoIncluido(_ producto: String) {
        SwiftEventBus.post("RetiraProductoCelda", sender: producto)
    }

    func eliminaServicio(_ servicio: Servicio) {
        var count = 1
        let aux = selectedServicios
        selectedServicios = []
        for mServicio in aux {
            if count > 0 {
                if mServicio.Id == servicio.Id {
                    count = 0
                } else {
                    selectedServicios.append(mServicio)
                }
            } else {
                selectedServicios.append(mServicio)
            }
        }
        actualiza()
    }

    func eliminaAddon(_ addon: Adicional) {
        let aux = selectedProductos
        selectedProductos = []
        for mAddon in aux {
            if mAddon.Id == addon.Id {
            } else {
                selectedProductos.append(mAddon)
            }
        }
        actualiza()
    }

    func eliminaPromo(_ promo: Promocion) {
        let aux = selectedPromociones
        selectedPromociones = []
        for mAddon in aux {
            if mAddon.Id == promo.Id {
                if promo.adicionalProductoId != "" {
                    retiraProductoIncluido(promo.adicionalProductoId)
                }
            } else {
                selectedPromociones.append(mAddon)
            }
        }
        actualiza()
    }

    func actualiza() {

        dump(selectedProductos)
        totalPrice = 0.0
        totalProductos = 0
        totalDiscount = 0.0

        for servicio in selectedServicios {
            totalPrice += servicio.precio.rounded()
        }

        for producto in selectedProductos {
            totalPrice += producto.precio.rounded()
        }

        for promociones in selectedPromociones {
            totalDiscount += promociones.montoDescuento.rounded()
        }

        totalPrice += mPlan.precioProntoPago.rounded()
        totalPrice = (totalPrice + CostoInstalacion) - totalDiscount
        totalProductos = selectedServicios.count + selectedProductos.count + selectedPromociones.count + 1

        if selectedServicios.count > 0 {
            var acumulatedServicios: [Servicio] = []
            for servicio in selectedServicios {
                var conteo: Int = 0
                for serv in selectedServicios {
                    if servicio.Id == serv.Id {
                        conteo += 1
                    }
                }
                if conteo > 1 {
                    if acumulatedServicios.count == 0 {
                        let auxServ = servicio
                        auxServ.cantidad = conteo
                        acumulatedServicios.append(auxServ)
                    } else {
                        var exist: Bool = false
                        for serv in acumulatedServicios {
                            if serv.Id == servicio.Id {
                                exist = true
                            }
                        }
                        if !exist {
                            let auxServ = servicio
                            auxServ.cantidad = conteo
                            acumulatedServicios.append(auxServ)
                        }
                    }
                } else {
                    let auxServ = servicio
                    auxServ.cantidad = 1
                    acumulatedServicios.append(auxServ)
                }
            }
            mObserver.actualizaServiciosResumen(servicios: acumulatedServicios)
        } else {
            mObserver.ocultaServicios()
        }
        if selectedProductos.count > 0 {
            mObserver.actualizaProductosResumen(productos: selectedProductos)
        } else {
            mObserver.ocultaProductos()
        }
        if selectedPromociones.count > 0 {
            mObserver.actualizaPromocionesResumen(promociones: selectedPromociones)
        } else {
            mObserver.ocultaPromociones()
        }
        mObserver.actualizaTotalPrecio(precio: totalPrice)
        mObserver.actualizaTotalProductos(total: totalProductos)
    }

}
