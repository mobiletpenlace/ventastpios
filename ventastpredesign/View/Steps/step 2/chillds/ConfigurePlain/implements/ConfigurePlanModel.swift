//
//  ConfigurePlanModel.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 15/06/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import RealmSwift

protocol ConfigurePlanModelObserver {
    func onSuccesAddons(response: ConsultaAddonsResponse)
    func onSuccesConvivencia()
    func onNotConvivencia(message: String)
}

class ConfigurePlanModel: BaseModel {

    var viewModel: ConfigurePlanModelObserver?
    var server: ServerDataManager2<ConsultaAddonsResponse>?
    var server2: ServerDataManager2<ConsultaConvivenciaResponse>?
    var url = AppDelegate.API_SALESFORCE + ApiDefinition.API_ADDONS_VENTA
    var url2 = AppDelegate.API_SALESFORCE + ApiDefinition.API_CONVIVENCIA_PROMOCIONES
    var mInfo: InfoAdicionalesCreacion?

    init (observer: BaseViewModelObserver) {
        server = ServerDataManager2<ConsultaAddonsResponse>()
        server2 = ServerDataManager2<ConsultaConvivenciaResponse>()
        super.init(observerVM: observer)
    }

    func atachModel(viewModel: ConfigurePlanModelObserver) {
        self.viewModel = viewModel
    }

    func chargeAddons(_ request: ConsultaAddonsRequest) {
        url = AppDelegate.API_SALESFORCE + ApiDefinition.API_ADDONS_VENTA
        server?.setValues(requestUrl: url, delegate: self, headerType: .headersSalesforce)
        server?.request(requestModel: request)
    }

    func getConvivencia(_ request: ConsultaConvivenciaRequest) {
        url2 = AppDelegate.API_SALESFORCE + ApiDefinition.API_CONVIVENCIA_PROMOCIONES
        server2?.setValues(requestUrl: url2, delegate: self, headerType: .headersSalesforce)
        server2?.request(requestModel: request)
    }

    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if requestUrl == url2 {
            let mResponse = response as! ConsultaConvivenciaResponse
            if mResponse.result == "0" {
                viewModel?.onSuccesConvivencia()
            } else {
                viewModel?.onNotConvivencia(message: mResponse.resultDescription)
            }
        } else {
            let mResponse = response as! ConsultaAddonsResponse
            if mResponse.result == "0" {
                viewModel?.onSuccesAddons(response: mResponse)
            } else {
                observerVM?.onErrorLoadResponse(requestUrl: requestUrl, messageError: "Fallo al consultar los addons")
            }
        }

    }

    func getSitio() -> Sitio {
        var mSitio = Sitio()
        if RealManager.findFirst(object: Sitio.self) != nil {
            mSitio = Sitio(value: RealManager.findFirst(object: Sitio.self)!)
        }
        return mSitio
    }

    func getProspecto() -> NProspecto {
        var mProspecto = NProspecto()
        if RealManager.findFirst(object: NProspecto.self) != nil {
            mProspecto = NProspecto(value: RealManager.findFirst(object: NProspecto.self)!)
        }
        return mProspecto
    }

    func   getInfo() -> InfoAdicionalesCreacion {
        var mInfo = InfoAdicionalesCreacion()

        if TPAddonsSelectedShared.shared.findAddonsPrev() != nil {
            mInfo = TPAddonsSelectedShared.shared.infoAddonsCreacion!
        }

        /*
        if RealManager.findFirst(object: InfoAdicionalesCreacion.self) != nil {
            mInfo = InfoAdicionalesCreacion(value: RealManager.findFirst(object: InfoAdicionalesCreacion.self)!)
        }*/
        return mInfo
    }

    func setInfoAddons(_ infoAddons: InfoAdicionalesCreacion) {
        mInfo = InfoAdicionalesCreacion()
        mInfo = infoAddons
        if TPAddonsSelectedShared.shared.findAddonsPrev() != nil {
            TPAddonsSelectedShared.shared.cleanAddonsCreacion()
            mInfo?.uui = "1"
            TPAddonsSelectedShared.shared.infoAddonsCreacion = mInfo!
        } else {
            mInfo?.uui = "1"
            TPAddonsSelectedShared.shared.infoAddonsCreacion = mInfo!
        }

        /*/
        if RealManager.findFirst(object: InfoAdicionalesCreacion.self) != nil {
            RealManager.deleteAll(object: InfoAdicionalesCreacion.self)
            mInfo?.uui = "1"
            _ = RealManager.insert(object: mInfo!)
        } else {
            mInfo?.uui = "1"
            _ = RealManager.insert(object: mInfo!)
        }*/

    }

}
