//
//  PackageSelectionModel.swift
//  ventastpredesign
//
//  Created by Gustavo Tellez Diaz on 20/01/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//
import UIKit
import ObjectMapper
import RealmSwift
import SwiftEventBus

protocol PackageSelectionModelObserver {
    func onSuccesBannersPlanes(banners: [BannersPlanesResponse])
}

class PackageSelectionModel: BaseModel {

    var viewModel: PackageSelectionModelObserver?
    var server: ServerDataManager2<BannersPlanesResponse>?
    let urlBanners = "https://cmsapp.totalplay.dev/banners-planes"

    init(observer: BaseViewModelObserver) {
        super.init(observerVM: observer)
        server = ServerDataManager2<BannersPlanesResponse>()
    }

    func atachModel(viewModel: PackageSelectionModelObserver ) {
        self.viewModel = viewModel
    }

    func getBannersPlanes() {
        server?.setValues(requestUrl: urlBanners, delegate: self, headerType: .headersMiddle)
        server?.requestArrayLiteral()
    }

    override func onSuccessLoadResponse(requestUrl: String, response: [BaseResponse]) {
        if requestUrl == urlBanners {
            let BannerResponse = response as! [BannersPlanesResponse]
            viewModel?.onSuccesBannersPlanes(banners: BannerResponse)
        }
    }
}
