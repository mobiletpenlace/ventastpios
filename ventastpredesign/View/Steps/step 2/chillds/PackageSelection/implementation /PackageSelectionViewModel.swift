//
//  PackageSelectionViewModel.swift
//  ventastpredesign
//
//  Created by Gustavo Tellez Diaz on 20/01/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit

protocol PackageSelectionObserver {
    func onSuccesBannersPlanes()
}

class PackageSelectionViewModel: BaseViewModel, PackageSelectionModelObserver {

    var observer: PackageSelectionObserver?
    var model: PackageSelectionModel!
    var mBannersPlanes: [BannerPlan] = []

    override init(view: BaseVentasView) {
        super.init(view: view)
        model = PackageSelectionModel(observer: self)
        model.atachModel(viewModel: self)
    }

    func atachView(observer: PackageSelectionObserver) {
        self.observer = observer
    }

    func getBannersPlanes() {
        model.getBannersPlanes()
    }

    func onSuccesBannersPlanes(banners: [BannersPlanesResponse]) {
        for banner in banners {
            if banner.visible == true {
                var bannerAux = BannerPlan()
                bannerAux.id = banner.id ?? 0
                bannerAux.name = banner.imagen[0].name ?? ""
                bannerAux.tipo = banner.tipo
                bannerAux.oferta = banner.oferta
                bannerAux.negocio = banner.negocio
                bannerAux.urlImage = banner.imagen[0].formats?.small?.url ?? ""
                mBannersPlanes.append(bannerAux)
            }
        }
        Logger.blockSeparator()
        Logger.println("BANNERS PLANES: \(mBannersPlanes)")
        view.hideLoading()
        observer?.onSuccesBannersPlanes()
    }

    func getImageBannerPlan(tipo: String, oferta: String, negocio: String) -> UIImage? {
        var tipoPlan = tipo
        if tipo == "Match" || tipo == "Unbox" {
            tipoPlan = "Tradicional"
        }

        // Assuming mBannersPlanes is defined and accessible here
        let banner = mBannersPlanes.filter { $0.tipo == tipoPlan && $0.oferta == oferta && $0.negocio == negocio }

        if let firstBanner = banner.first, let url = URL(string: firstBanner.urlImage) {
            if let data = try? Data(contentsOf: url), let imageBanner = UIImage(data: data) {
                return imageBanner
            }
        }

        return nil
    }

}
