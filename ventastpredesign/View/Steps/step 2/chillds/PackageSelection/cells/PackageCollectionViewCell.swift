//
//  PackageCollectionViewCell.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 06/01/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class PackageCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var planImageView: UIImageView!
    var mPlan: Plan!
    var view: BaseVentasView?

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setData(_ plan: Plan, _ view: BaseVentasView) {
        mPlan = plan
        self.view = view
        if let image = UIImage.URLimage(url: mPlan.imagenPaquete) {
            planImageView.image = image
        }
    }

    @IBAction func onClick(_ sender: Any) {
        SwiftEventBus.post("PlanSelected", sender: mPlan)
        Logger.println("\(mPlan.id)")
    }

}
