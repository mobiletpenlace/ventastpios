//
//  BannerPlan.swift
//  ventastpredesign
//
//  Created by Gustavo Tellez Diaz on 21/01/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation

struct BannerPlan {
    var id: Int = 0
    var name: String = ""
    var negocio: String = ""
    var tipo: String = ""
    var oferta: String = ""
    var urlImage: String = ""
}
