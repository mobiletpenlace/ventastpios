//
//  ImageFormat.swift
//  ventastpredesign
//
//  Created by Gustavo Tellez Diaz on 20/01/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class ImageFormat: NSObject, Mappable {
    var thumbnail: FormatInformation?
    var small: FormatInformation?
    var medium: FormatInformation?

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        small <- map["small"]
        thumbnail <- map["thumbnail"]
        medium <- map["medium"]
    }
}
