//
//  BannersImage.swift
//  ventastpredesign
//
//  Created by Gustavo Tellez Diaz on 20/01/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class BannersImage: NSObject, Mappable {

    var id: Int?
    var name: String?
    var formats: ImageFormat?

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        formats <- map["formats"]
    }
}
