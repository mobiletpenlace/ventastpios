//
//  BannersPlanesResponse.swift
//  ventastpredesign
//
//  Created by Gustavo Tellez Diaz on 20/01/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class BannersPlanesResponse: BaseResponse {

    var id: Int?
    var visible: Bool?
    var negocio: String = ""
    var tipo: String = ""
    var oferta: String = ""
    var imagen: [BannersImage] = []

    required init?(map: Map) {

    }

    override func mapping(map: Map) {
        id <- map["id"]
        visible <- map["visible"]
        negocio <- map["negocio"]
        tipo <- map["Tipo"]
        oferta <- map["Oferta"]
        imagen <- map["Imagen"]
    }
}
