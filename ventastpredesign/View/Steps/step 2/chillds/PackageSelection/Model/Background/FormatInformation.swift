//
//  FormatInformation.swift
//  ventastpredesign
//
//  Created by Gustavo Tellez Diaz on 20/01/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class FormatInformation: NSObject, Mappable {
    var url: String = ""

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        url <- map["url"]
    }
}
