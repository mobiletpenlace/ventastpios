//
//  PlanesJsonPresenter.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 18/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

protocol PlanesJsonDelegate: NSObjectProtocol {
    func OnSuccessPlanesJson(mPlanesJsonResponse: PlanesJsonResponse)
}

class PlanesJsonPresenter: BaseVentastpredesignPresenter {
    var mPlanesJsonDelegate: PlanesJsonDelegate!

    func GetPlanesJson(mTipo: String, mPlaza: String, mPlanesJsonDelegate: PlanesJsonDelegate) {
        self.mPlanesJsonDelegate = mPlanesJsonDelegate

        let planesJsonRequest = PlanesJsonRequest()
        planesJsonRequest.MInfo = InfoPlan()
        planesJsonRequest.MInfo?.Tipo = mTipo
        planesJsonRequest.MInfo?.Plaza = mPlaza
        planesJsonRequest.MLogin = Login()
        planesJsonRequest.MLogin?.Ip = "1.1.1.1"
        planesJsonRequest.MLogin?.User = "25631"
        planesJsonRequest.MLogin?.SecretWord = "Middle100$"
        RetrofitManager<PlanesJsonResponse>.init(requestUrl: ApiDefinition.API_QUERY_PLANES, delegate: self).request(requestModel: planesJsonRequest)
    }
    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if requestUrl == ApiDefinition.API_QUERY_PLANES {
           view.hideLoading()
            OnSuccessPlanesJson(planesJsonResponse: response as! PlanesJsonResponse)
        }
    }

    func OnSuccessPlanesJson(planesJsonResponse: PlanesJsonResponse) {
        if planesJsonResponse.MResult?.Result != "0" {
            Constants.Alert(title: "", body: dialogs.Error.InternService, type: type.Error, viewC: mViewController)

        } else if planesJsonResponse.MResult == nil {
            Constants.Alert(title: "", body: dialogs.Error.InternService, type: type.Error, viewC: mViewController)
        } else {
             mPlanesJsonDelegate.OnSuccessPlanesJson(mPlanesJsonResponse: planesJsonResponse)
        }
    }

}
