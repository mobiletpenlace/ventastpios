//
//  TipoPlanPresenter.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 18/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

protocol TipoPlanDelegate: NSObjectProtocol {
    func OnSuccessTipoPlanDelegate(mTipoPlanResponse: TipoPlanResponse)
}
class TipoPlanPresenter: BaseVentastpredesignPresenter {
    var mTipoPlanDelegate: TipoPlanDelegate!

    func GetTipoPlan(mTipoPlanDelegate: TipoPlanDelegate) {
        self.mTipoPlanDelegate = mTipoPlanDelegate

        let tipoPlanRequest = TipoPlanRequest()
        tipoPlanRequest.MLogin = Login()
        tipoPlanRequest.MLogin?.Ip = "1.1.1.1"
        tipoPlanRequest.MLogin?.User = "25631"
        tipoPlanRequest.MLogin?.SecretWord = "Middle100$"

        RetrofitManager<TipoPlanResponse>.init(requestUrl: ApiDefinition.API_QUERY_TYPE_PLAIN, delegate: self).request(requestModel: tipoPlanRequest)
    }
    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if requestUrl == ApiDefinition.API_QUERY_TYPE_PLAIN {
            // AlertDialog.hideOverlay()
            OnSuccessTipoPlan(tipoPlanResponse: response as! TipoPlanResponse)
        }
    }

    func OnSuccessTipoPlan(tipoPlanResponse: TipoPlanResponse) {
        mTipoPlanDelegate.OnSuccessTipoPlanDelegate(mTipoPlanResponse: tipoPlanResponse)

    }

}
