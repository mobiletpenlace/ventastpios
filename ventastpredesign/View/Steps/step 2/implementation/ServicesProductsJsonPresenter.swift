//
//  ServicesProductsJsonPresenter.swift
//  ventastpredesign
//
//  Created by Claudia Isamar Delgado Vasquez on 18/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

protocol ServicesProductsJsonDelegate: NSObjectProtocol {
    func OnSuccessServicesProductsJson(servicesProductsJsonResponse: ServicesProductsJsonResponse)

}

class ServicesProductsJsonPresenter: BaseVentastpredesignPresenter {

    var mServicesProductsJsonDelegate: ServicesProductsJsonDelegate!

    func getServicesProductsJsonPresenter(mIdPlan: String, mCiudad: String, mPlaza: String, mCluster: String, mDistrito: String, mColonia: String, mCanalVenta: String, mCP: String, mServicesProductsJsonDelegate: ServicesProductsJsonDelegate) {
        self.mServicesProductsJsonDelegate = mServicesProductsJsonDelegate
        let servicesProductsJson = ServicesProductsJsonRequest()

        servicesProductsJson.MLogin = Login()
        servicesProductsJson.MLogin?.Ip = "1.1.1.1"
        servicesProductsJson.MLogin?.User = "25631"
        servicesProductsJson.MLogin?.SecretWord = "Middle100$"
        servicesProductsJson.MInfoIdPlan = InfoIdPlan()
        servicesProductsJson.MInfoIdPlan?.IdPlan = mIdPlan
        servicesProductsJson.MInfoIdPlan?.Plaza = mPlaza
        servicesProductsJson.MInfoIdPlan?.Cluster = mCluster
        servicesProductsJson.MInfoIdPlan?.Distrito = mDistrito
        servicesProductsJson.MInfoIdPlan?.MetodoDePago = "Efectivo"
        servicesProductsJson.MInfoIdPlan?.Colonia = mColonia
        servicesProductsJson.MInfoIdPlan?.CanalVenta = mCanalVenta
        servicesProductsJson.MInfoIdPlan?.CP = mCP

    RetrofitManager<ServicesProductsJsonResponse>.init(requestUrl: ApiDefinition.API_QUERY_SERVICES_PRODUCT_DETAIL, delegate: self).request(requestModel: servicesProductsJson)
    }
    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if requestUrl == ApiDefinition.API_QUERY_SERVICES_PRODUCT_DETAIL {
            view.hideLoading()

            OnSuccessServicesProductsJson(servicesProductsJsonResponse: response as!  ServicesProductsJsonResponse)
        }

    }
    func OnSuccessServicesProductsJson(servicesProductsJsonResponse: ServicesProductsJsonResponse) {
        if servicesProductsJsonResponse.MResult?.Result != "0" {
        mServicesProductsJsonDelegate.OnSuccessServicesProductsJson(servicesProductsJsonResponse: servicesProductsJsonResponse)
        } else if servicesProductsJsonResponse.MResult?.Result == nil {
            Constants.Alert(title: "", body: dialogs.Error.InternService, type: type.Error, viewC: mViewController)
        } else {
            Constants.Alert(title: "", body: dialogs.Error.InternService, type: type.Error, viewC: mViewController)

        }
    }

}
