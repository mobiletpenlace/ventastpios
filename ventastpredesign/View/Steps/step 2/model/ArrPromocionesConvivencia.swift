//
//  ArrPromocionesConvivencia.swift
//  ventastpredesign
//
//  Created by Claudia Isamar Delgado Vasquez on 18/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class ArrPromocionesConvivencia: NSObject, Mappable {
    required init?(map: Map) {

    }
    override init() {

    }
    func mapping(map: Map) {
        Id <- map["Id"]
        Nombre <- map["Nombre"]
        Estatus <- map["Estatus"]
        PromocionPlanId <- map["PromocionPlanId"]
        PromocionExcId <- map["PromocionExcId"]
    }

    var Id: String?
    var Nombre: String?
    var Estatus: String?
    var PromocionPlanId: String?
    var PromocionExcId: String?
}
