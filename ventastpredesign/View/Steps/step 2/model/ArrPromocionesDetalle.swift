//
//  ArrPromocionesDetalle.swift
//  ventastpredesign
//
//  Created by Claudia Isamar Delgado Vasquez on 18/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class ArrPromocionesDetalle: NSObject, Mappable {
    required init?(map: Map) {

    }
    override init() {

    }
    func mapping(map: Map) {
        Id <- map["Id"]
        Nombre <- map["Nombre"]
        CAT_CanalVenta <- map["CAT_CanalVenta"]
        CAT_Ciudad <- map["CAT_Ciudad"]
        CAT_Cluster <- map["CAT_Cluster"]
        CAT_Plazo <- map["CAT_Plazo"]
        CodigoPostal <- map["CodigoPostal"]
        CAT_TipoPago <- map["CAT_TipoPago"]
        DP_Promocion <- map["DP_Promocion"]
        Estatus <- map["Estatus"]
        PromocionCodigo <- map["PromocionCodigo"]
    }

    var Id: String?
    var Nombre: String?
    var CAT_CanalVenta: String?
    var CAT_Ciudad: String?
    var CAT_Cluster: String?
    var CAT_Plazo: String?
    var CodigoPostal: String?
    var CAT_TipoPago: String?
    var DP_Promocion: String?
    var Estatus: String?
    var PromocionCodigo: String?
}
