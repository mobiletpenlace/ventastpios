//
//  InfoIdPlan.swift
//  ventastpredesign
//
//  Created by Claudia Isamar Delgado Vasquez on 18/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class InfoIdPlan: NSObject, Mappable {
    required init?(map: Map) {

    }

    func mapping(map: Map) {
        IdPlan <- map["IdPlan"]
        Plaza <- map["Plaza"]
        Cluster <- map["Cluster"]
        Colonia <- map["Colonia"]
        MetodoDePago <- map["MetodoDePago"]
        Distrito <- map["Distrito"]
        CanalVenta <- map["CanalVenta"]
        CP <- map["CP"]
    }

    override init() {

    }

    var IdPlan: String?
    var Plaza: String?
    var Cluster: String?
    var Colonia: String?
    var MetodoDePago: String?
    var Distrito: String?
    var CanalVenta: String?
    var CP: String?

}
