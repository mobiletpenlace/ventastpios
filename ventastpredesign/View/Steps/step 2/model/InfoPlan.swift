//
//  InfoPlan.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 18/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import UIKit

import ObjectMapper

class InfoPlan: NSObject, Mappable {
    required init?(map: Map) {

    }

    func mapping(map: Map) {
        Tipo <- map["Tipo"]
        Plaza <- map["Plaza"]
    }

    override init() {

    }
    var Tipo: String?
    var Plaza: String?

}
