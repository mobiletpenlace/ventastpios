//
//  ArrPromocionesPlan.swift
//  ventastpredesign
//
//  Created by Claudia Isamar Delgado Vasquez on 18/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class ArrPromocionesPlan: NSObject, Mappable {
    required init?(map: Map) {

    }
    override init() {

    }
    func mapping(map: Map) {
        Id <- map["Id"]
        Nombre <- map["Nombre"]
        Comentario <- map["Comentario"]
        Descripcion <- map["Descripcion"]
        Plan <- map["Plan"]
        PlanCambioParrilla <- map["PlanCambioParrilla"]
        Promocion <- map["Promocion"]
        Estatus <- map["Estatus"]
        Plazo <- map["Plazo"]
        EstatusPromociones <- map["EstatusPromociones"]
        ComentarioPromocion <- map["ComentarioPromocion"]
        NombrePromocion <- map["NombrePromocion"]
        CompaniaId <- map["CompaniaId"]
        EsAutomatica <- map["EsAutomatica"]
        EsPostVenta <- map["EsPostVenta"]
        EstatusPromocion <- map["EstatusPromocion"]
        EsVentaNueva <- map["EsVentaNueva"]
        FechaFin <- map["FechaFin"]
        FechaInicio <- map["FechaInicio"]
        IdPromocion <- map["IdPromocion"]
        ArrPromocionesConvivencia <- map["ArrPromocionesConvivencia"]
        ArrPromocionesDetalle <- map["ArrPromocionesDetalle"]
        ArrDP_PromocionesServicioProducto <- map["ArrDP_PromocionesServicioProducto"]
        PromocionCodigo <- map["PromocionCodigo"]
        idCodigo <- map["idCodigo"]
    }

    var Id: String?
    var Nombre: String?
    var Comentario: String?
    var Descripcion: String?
    var Plan: String?
    var PlanCambioParrilla: String?
    var Promocion: String?
    var Estatus: String?
    var Plazo: String?
    var EstatusPromociones: String?
    var ComentarioPromocion: String?
    var NombrePromocion: String?
    var CompaniaId: String?
    var EsAutomatica: String?
    var EsPostVenta: String?
    var EstatusPromocion: String?
    var EsVentaNueva: String?
    var FechaFin: String?
    var FechaInicio: String?
    var IdPromocion: String?
    var PromocionCodigo: String?
    var idCodigo: String?
    var ArrPromocionesConvivencia: [ArrPromocionesConvivencia] = []
    var ArrPromocionesDetalle: [ArrPromocionesDetalle] = []
    var ArrDP_PromocionesServicioProducto: [ArrDP_PromocionesServicioProducto] = []
}
