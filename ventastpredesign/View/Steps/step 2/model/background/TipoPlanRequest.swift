//
//  TipoPlanRequest.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 18/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class TipoPlanRequest: BaseRequest {

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        MLogin <- map["Login"]

    }

    var MLogin: Login?

}
