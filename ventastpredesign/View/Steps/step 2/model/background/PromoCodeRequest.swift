//
//  PromoCodeRequest.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 27/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class PromoCodeRequest: BaseRequest {

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        MLogin <- map["Login"]
        DP_Plan <- map["DP_Plan"]
        DP_Promocion <- map["DP_Promocion"]
        IdCodigo <- map["IdCodigo"]

    }

    var MLogin: Login?
    var DP_Plan: String?
    var DP_Promocion: String?
    var IdCodigo: String?

}
