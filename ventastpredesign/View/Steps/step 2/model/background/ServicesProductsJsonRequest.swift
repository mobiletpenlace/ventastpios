//
//  ServicesProductsJsonRequest.swift
//  ventastpredesign
//
//  Created by Claudia Isamar Delgado Vasquez on 18/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class ServicesProductsJsonRequest: BaseRequest {

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        MInfoIdPlan <- map["Info"]
        MLogin <- map["Login"]
    }
    var MInfoIdPlan: InfoIdPlan?
    var MLogin: Login?
}
