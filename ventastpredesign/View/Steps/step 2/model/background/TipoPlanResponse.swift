//
//  TipoPlanResponse.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 18/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class TipoPlanResponse: BaseResponse {

    required init?(map: Map) {

    }

    override func mapping(map: Map) {
        MResult <- map["Result"]
        MTipoPlan <- map["TipoPlan"]

    }
    override init() {

    }

    var MResult: Result?
    var MTipoPlan: [TipoPlan] = []

}
