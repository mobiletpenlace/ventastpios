//
//  PlanesJsonRequest.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 18/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class PlanesJsonRequest: BaseRequest {

    public override init() {
        super.init()
    }

    public required init?(map: Map) {
        super.init()
    }

    override func mapping(map: Map) {
        MInfo <- map["Info"]
        MLogin <- map["Login"]

    }

    var MInfo: InfoPlan?
    var MLogin: Login?

}
