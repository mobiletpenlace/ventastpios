//
//  ServicesProductsJsonResponse.swift
//  ventastpredesign
//
//  Created by Claudia Isamar Delgado Vasquez on 18/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class ServicesProductsJsonResponse: BaseResponse {
    required init?(map: Map) {

    }

    override func mapping(map: Map) {
        MResult <- map["Result"]
        MArrServiciosIncluidos <- map["ArrServiciosIncluidos"]
        MArrServiciosAdicionales <- map["ArrServiciosAdicionales"]
        MArrProductosAdicionales <- map["ArrProductosAdicionales"]
        MArrPromocionesPlan <- map["ArrPromocionesPlan"]
        MArrCostoInstalacion <- map["ArrCostoInstalacion"]
    }
    var MResult: Result?
    var MArrServiciosIncluidos: [ArrServiciosIncluidos] = []
    var MArrServiciosAdicionales: [ArrServiciosAdicionales] = []
    var MArrProductosAdicionales: [ArrProductosAdicionales] = []
    var MArrPromocionesPlan: [ArrPromocionesPlan] = []
    var MArrCostoInstalacion: [ArrCostoInstalacion] = []

}
