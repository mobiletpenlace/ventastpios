//
//  PromoCodeResponse.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 27/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class PromoCodeResponse: BaseResponse {

    required init?(map: Map) {

    }

    override func mapping(map: Map) {
        MResponse <- map["response"]
        NoUsados <- map["NoUsados"]
        idPromocion <- map["idPromocion"]
        idCodigo <- map["idCodigo"]
        Valido <- map["Valido"]

    }

    var MResponse: Response?
    var NoUsados: String?
    var idPromocion: String?
    var idCodigo: String?
    var Valido: String?

}
