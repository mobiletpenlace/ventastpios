//
//  ArrServiciosAdicionales.swift
//  ventastpredesign
//
//  Created by Claudia Isamar Delgado Vasquez on 18/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class ArrServiciosAdicionales: NSObject, Mappable {
    required init?(map: Map) {

    }
    override init() {

    }
    func mapping(map: Map) {
        Id <- map["Id"]
        Nombre <- map["Nombre"]
        CantidadEquiposAutomaticos <- map["CantidadEquiposAutomaticos"]
        Comentario <- map["Comentario"]
        MaximoAgregar <- map["MaximoAgregar"]
        Nombrecomercial <- map["Nombrecomercial"]
        NombreEditable <- map["NombreEditable"]
        PlanId <- map["PlanId"]
        Precio <- map["Precio"]
        SeActiva <- map["SeActiva"]
        SeFactura <- map["SeFactura"]
        ServicioId <- map["ServicioId"]
        SRV_Mode <- map["SRV_Mode"]
        TipoServicio <- map["TipoServicio"]
        TipoTelefonia <- map["TipoTelefonia"]
        ArrProductosIncluidos <- map["ArrProductosIncluidos"]
        EquipoATA <- map["EquipoATA"]
        PrecioATA <- map["PrecioATA"]
    }
    var Id: String?
    var Nombre: String?
    var CantidadEquiposAutomaticos: String?
    var Comentario: String?
    var MaximoAgregar: String?
    var Nombrecomercial: String?
    var NombreEditable: String?
    var PlanId: String?
    var Precio: String?
    var SeActiva: String?
    var SeFactura: String?
    var ServicioId: String?
    var SRV_Mode: String?
    var TipoServicio: String?
    var TipoTelefonia: String?
    var ArrProductosIncluidos: [ArrProductosIncluidos] = []
    var EquipoATA: String?
    var PrecioATA: String?

}
