//
//  ArrProductosConvivencia.swift
//  ventastpredesign
//
//  Created by Claudia Isamar Delgado Vasquez on 18/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class ArrProductosConvivencia: NSObject, Mappable {
    required init?(map: Map) {

    }
    override init() {

    }

    func mapping(map: Map) {
        Id <- map["Id"]
        Name <- map["Name"]
        Comentario <- map["Comentario"]
        Estatus <- map["Estatus"]
        ProductosId <- map["ProductosId"]
        ProductoExcIncId <- map["ProductoExcIncId"]
        Tipo <- map["Tipo"]
    }
    var Id: String?
    var Name: String?
    var Comentario: String?
    var Estatus: String?
    var ProductosId: String?
    var ProductoExcIncId: String?
    var Tipo: String?
}
