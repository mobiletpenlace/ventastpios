//
//  ArrProductosAdicionales.swift
//  ventastpredesign
//
//  Created by Claudia Isamar Delgado Vasquez on 18/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class ArrProductosAdicionales: NSObject, Mappable {
    required init?(map: Map) {

    }
    override init() {

    }

    func mapping(map: Map) {
        Id <- map["Id"]
        Nombre <- map["Nombre"]
        AgrupacionAddon <- map["AgrupacionAddon"]
        CantidadDN <- map["CantidadDN"]
        CantidadTroncal <- map["CantidadTroncal"]
        Comentario <- map["Comentario"]
        EsAutomaticoCiudad <- map["EsAutomaticoCiudad"]
        EsCargoUnico <- map["EsCargoUnico"]
        Estatus <- map["Estatus"]
        EsVisible <- map["EsVisible"]
        IdBrmArrear <- map["IdBrmArrear"]
        IdBrmCU <- map["IdBrmCU"]
        IdBrmForward <- map["IdBrmForward"]
        IEPS <- map["IEPS"]
        IVA <- map["IVA"]
        MaximoAgregar <- map["MaximoAgregar"]
        NombreEditable <- map["NombreEditable"]
        PlanDescuentoId <- map["PlanDescuentoId"]
        Plazo <- map["Plazo"]
        PrecioBase <- map["PrecioBase"]
        PrecioEditable <- map["PrecioEditable"]
        PrecioProntoPago <- map["PrecioProntoPago"]
        ProductoPadre <- map["ProductoPadre"]
        TipoProducto <- map["TipoProducto"]
        TipoProductoTexto <- map["TipoProductoTexto"]
        NameProducto <- map["NameProducto"]
        ProductoId <- map["ProductoId"]
        Ciudad <- map["Ciudad"]
        VelocidadSubida <- map["VelocidadSubida"]
        VelocidadBajada <- map["VelocidadBajada"]
        TieneIPDinamica <- map["TieneIPDinamica"]
        TieneIPFija <- map["TieneIPFija"]
        TieneSTBAdicional <- map["TieneSTBAdicional"]
        EsCCTV <- map["EsCCTV"]
        EsWiFi <- map["EsWiFi"]
        Cantidad <- map["Cantidad"]
        EstatusProducto <- map["EstatusProducto"]
        FechaInicio <- map["FechaInicio"]
        FechaFin <- map["FechaFin"]
        ComentarioProducto <- map["ComentarioProducto"]
        EsProntoPago <- map["EsProntoPago"]
        ArrProductosConvivencia <- map["ArrProductosConvivencia"]
    }
    var Id: String?
    var Nombre: String?
    var AgrupacionAddon: String?
    var CantidadDN: String?
    var CantidadTroncal: String?
    var Comentario: String?
    var EsAutomaticoCiudad: String?
    var EsCargoUnico: String?
    var Estatus: String?
    var EsVisible: String?
    var IdBrmArrear: String?
    var IdBrmCU: String?
    var IdBrmForward: String?
    var IEPS: String?
    var IVA: String?
    var MaximoAgregar: String?
    var NombreEditable: String?
    var PlanDescuentoId: String?
    var Plazo: String?
    var PrecioBase: String?
    var PrecioEditable: String?
    var PrecioProntoPago: String?
    var ProductoPadre: String?
    var TipoProducto: String?
    var TipoProductoTexto: String?
    var NameProducto: String?
    var ProductoId: String?
    var Ciudad: String?
    var VelocidadSubida: String?
    var VelocidadBajada: String?
    var TieneIPDinamica: String?
    var TieneIPFija: String?
    var TieneSTBAdicional: String?
    var EsCCTV: String?
    var EsWiFi: String?
    var Cantidad: String?
    var EstatusProducto: String?
    var FechaInicio: String?
    var FechaFin: String?
    var ComentarioProducto: String?
    var EsProntoPago: String?
    var ArrProductosConvivencia: [ArrProductosConvivencia] = []
}
