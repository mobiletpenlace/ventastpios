//
//  TipoPlan.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 18/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import UIKit

import ObjectMapper

class TipoPlan: NSObject, Mappable {
    required init?(map: Map) {

    }

    func mapping(map: Map) {
        Tipo <- map["Tipo"]

    }

    override init() {

    }
    var Tipo: String?

}
