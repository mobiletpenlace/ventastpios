//
//  ArrPlan.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 18/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class ArrPlan: NSObject, Mappable {
    required init?(map: Map) {

    }

    func mapping(map: Map) {

         IdPlan <- map["IdPlan"]
         NombrePlan <- map["NombrePlan"]
         TipCobertura <- map["TipCobertura"]
         TipooAprovisionamiento <- map["TipooAprovisionamiento"]
         TipoFacturacion <- map["TipoFacturacion"]
         Comentario <- map["Comentario"]
         PemiteDescuentoRenta <- map["PemiteDescuentoRenta"]
         PermiteDescuentoCargoUnico <- map["PermiteDescuentoCargoUnico"]
         SubtipoOportunidad <- map["SubtipoOportunidad"]
         TmcodeArrear <- map["TmcodeArrear"]
         TmcodeFwd <- map["TmcodeFwd"]

    }

    override init() {

    }
    var IdPlan: String?
    var NombrePlan: String?
    var TipCobertura: String?
    var TipooAprovisionamiento: String?
    var TipoFacturacion: String?
    var Comentario: String?
    var PemiteDescuentoRenta: String?
    var PermiteDescuentoCargoUnico: String?
    var SubtipoOportunidad: String?
    var TmcodeArrear: String?
    var TmcodeFwd: String?

}
