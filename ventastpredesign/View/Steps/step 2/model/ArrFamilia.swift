//
//  ArrFamilia.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 18/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class ArrFamilia: NSObject, Mappable {
    required init?(map: Map) {

    }

    func mapping(map: Map) {

        IdFamilia <- map["IdFamilia"]
        NombreFamilia <- map["NombreFamilia"]
        ArrPlan <- map["ArrPlan"]
    }

    override init() {
    }

    var IdFamilia: String?
    var NombreFamilia: String?
    var ArrPlan: [ArrPlan] = []

}
