//
//  ArrDP_PromocionesServicioProducto.swift
//  ventastpredesign
//
//  Created by Claudia Isamar Delgado Vasquez on 18/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

import ObjectMapper

class ArrDP_PromocionesServicioProducto: NSObject, Mappable {
    required init?(map: Map) {

    }
    override init() {

    }
    func mapping(map: Map) {
        Id <- map["Id"]
        Nombre <- map["Nombre"]
        AgregarServicio <- map["AgregarServicio"]
        EsCargoUnico <- map["EsCargoUnico"]
        Estatus <- map["Estatus"]
        IEPS <- map["IEPS"]
        IVA <- map["IVA"]
        MesInicio <- map["MesInicio"]
        MontoFront <- map["MontoFronterizo"]
        Monto__c <- map["Monto__c"]
        PlanServicio <- map["PlanServicio"]
        Porcentaje <- map["Porcentaje"]
        ProductoCaracteristica <- map["ProductoCaracteristica"]
        PromocionPlan <- map["PromocionPlan"]
        ServicioProducto <- map["ServicioProducto"]
        VigenciaMes <- map["VigenciaMes"]
        IdProductoC <- map["IdProductoC"]
        NameProductoC <- map["NameProductoC"]
        ProductoId <- map["ProductoId"]
        Ciudad <- map["Ciudad"]
        VelocidadSubida <- map["VelocidadSubida"]
        VelocidadBajada <- map["VelocidadBajada"]
        TieneIPDinamica <- map["TieneIPDinamica"]
        TieneIPFija <- map["TieneIPFija"]
        TieneSTBAdicional <- map["TieneSTBAdicional"]
        EsCCTV <- map["EsCCTV"]
        EsWiFi <- map["EsWiFi"]
        Cantidad <- map["Cantidad"]
        EstatusProductoC <- map["EstatusProductoC"]
        FechaInicio <- map["FechaInicio"]
        FechaFin <- map["FechaFin"]
        Comentario <- map["Nombre"]
        EsProntoPago <- map["EsProntoPago"]
        DP_ServicioProducto <- map["DP_ServicioProducto"]
    }
    var Id: String?
    var Nombre: String?
    var AgregarServicio: String?
    var EsCargoUnico: String?
    var Estatus: String?
    var IEPS: String?
    var IVA: String?
    var MesInicio: String?
    var Monto__c: String?
    var MontoFront: String?
    var PlanServicio: String?
    var Porcentaje: String?
    var ProductoCaracteristica: String?
    var PromocionPlan: String?
    var ServicioProducto: String?
    var VigenciaMes: String?
    var IdProductoC: String?
    var NameProductoC: String?
    var ProductoId: String?
    var Ciudad: String?
    var VelocidadSubida: String?
    var VelocidadBajada: String?
    var TieneIPDinamica: String?
    var TieneIPFija: String?
    var TieneSTBAdicional: String?
    var EsCCTV: String?
    var EsWiFi: String?
    var Cantidad: String?
    var EstatusProductoC: String?
    var FechaInicio: String?
    var FechaFin: String?
    var Comentario: String?
    var EsProntoPago: String?
    var DP_ServicioProducto: DP_ServicioProducto?
}
