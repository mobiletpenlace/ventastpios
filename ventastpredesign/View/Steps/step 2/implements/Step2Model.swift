//
//  Step2Model.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 03/06/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import RealmSwift

protocol Step2ModelObserver {
    func onSuccesPlanes(consultaPlanesResponse: ConsultaPlanesResponse)
    func onErrorPlanes(message: String)
}

class Step2Model: BaseModel {

    var viewModel: Step2ModelObserver?
    var server: ServerDataManager2<ConsultaPlanesResponse>?
    var url = AppDelegate.API_SALESFORCE + ApiDefinition.API_PLANES_TP

    init (observer: BaseViewModelObserver) {
        server = ServerDataManager2<ConsultaPlanesResponse>()
        super.init(observerVM: observer)
    }

    func atachModel(viewModel: Step2ModelObserver) {
        self.viewModel = viewModel
    }

    func chargePlanes(_ request: ConsultaPlanessRequest) {
        url = AppDelegate.API_SALESFORCE + ApiDefinition.API_PLANES_TP
        server?.setValues(requestUrl: url, delegate: self, headerType: .headersSalesforce)
        server?.request(requestModel: request)
    }

    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if requestUrl == url {
            let planes = response as! ConsultaPlanesResponse
            if planes.result == "0" {
                viewModel?.onSuccesPlanes(consultaPlanesResponse: planes)
            } else {
                    viewModel?.onErrorPlanes(message: "No se encontraron datos")
            }
        }
    }

    func getIdPlan() -> ObjetoModelado {
        var mObjetoModelado = ObjetoModelado()
        if RealManager.findFirst(object: ObjetoModelado.self) != nil {
            mObjetoModelado = ObjetoModelado(value: RealManager.findFirst(object: ObjetoModelado.self)!)
        }
        return mObjetoModelado
    }

}
