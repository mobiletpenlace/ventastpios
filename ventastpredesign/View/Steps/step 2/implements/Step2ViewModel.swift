//
//  Step2ViewModel.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 03/06/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import SwiftEventBus

protocol Step2Observer {
    func changeView(page: Int)
    func onSuccesChargePlanes()
    func normalFlow()
    func PrintMessage(message: String)
    func back(paso: Int)
}

class Step2ViewModel: BaseViewModel, Step2ModelObserver {

    var observer: Step2Observer?
    var model: Step2Model!
    var mSitio: Sitio?
    var mProspecto: NProspecto?
    var estimulo: String?
    var isResidencial: Bool = false
    var planes: [Plan] = []
    var Striming: [ServiciosStriming] = []
    var residenciales: [Plan] = []
    var negocio: [Plan] = []
    var prepago: [Plan] = []
    var Familia: FamilyObject!
    var planSelected: Plan!
    var idPlanSelected: String!
    var existeModelado: Bool = false
    var infoAddons: InfoAdicionalesCreacion?
    var isOfertaNueva: Bool = true

    override init(view: BaseVentasView) {
        super.init(view: view)
        model = Step2Model(observer: self)
        model.atachModel(viewModel: self)

    }

    func atachView(observer: Step2Observer) {
        self.observer = observer
        isOfertaNueva = true
        SwiftEventBus.onMainThread(self, name: "back") { [weak self] result in
            if let number = result?.object as? Int {
                self?.observer?.back(paso: number)
            } else {
                self?.observer?.PrintMessage(message: "Ocurrio un  error en la comunicacion interna")
            }
        }

        SwiftEventBus.onMainThread(self, name: "Residencial_Familly") { [weak self] _ in
            if self?.isOfertaNueva == true {
                observer.changeView(page: fase.NewPackageSelection)
            } else {
                observer.changeView(page: fase.FamilyResidencial)
            }
        }

        SwiftEventBus.onMainThread(self, name: "Negocio_Familly") { [weak self] _ in
            observer.changeView(page: fase.FamilyNegocio)
        }

        SwiftEventBus.onMainThread(self, name: "InfoPersonal2") { [weak self] _ in
            observer.changeView(page: fase.InfoPersonal2)
        }

        SwiftEventBus.onMainThread(self, name: "Residencial_Prepago_Familly") { [weak self] _ in
            observer.changeView(page: fase.FamilyPrepago)
        }

        SwiftEventBus.onMainThread(self, name: "CargaEstimulo") { [weak self] result in
            if let cadena = result?.object as? String {
                self?.estimulo = cadena
            } else {
                self?.observer?.PrintMessage(message: "Ocurrio un  error en la comunicacion interna")
            }
        }

        SwiftEventBus.onMainThread(self, name: "PackageSelection") { [weak self] result in
            if let family = result?.object as? FamilyObject {
                self?.Familia = family
                self?.observer?.changeView(page: fase.PackageSelection)
            } else {
                self?.observer?.PrintMessage(message: "Ocurrio un  error en la comunicacion interna")
            }
        }

        SwiftEventBus.onMainThread(self, name: "PlanSelected") { [weak self] result in
            if let plan = result?.object as? Plan {
                self?.planSelected =  plan
                self?.observer?.changeView(page: fase.AddonConfig)
            } else {
                self?.observer?.PrintMessage(message: "Ocurrio un  error en la comunicacion interna")
            }
        }

        SwiftEventBus.onMainThread(self, name: "Addons_cargados") { [weak self] result in
            if let info = result?.object as? InfoAdicionalesCreacion {
                self!.infoAddons = info
            } else {
                self?.observer?.PrintMessage(message: "Ocurrio un  error en la comunicacion interna")
            }
        }

        SwiftEventBus.onMainThread(self, name: "Modelado_cargado") { [weak self] result in
            if let plan = result?.object as? Plan {
                self!.existeModelado = true
                self!.planSelected = plan
            } else {
                self?.observer?.PrintMessage(message: "Ocurrio un  error en la comunicacion interna")
            }
        }
    }

    func getSitio() {
       // Intento de solucion
        if case let sitio: Sitio? = Sitio(value: RealManager.findFirst(object: Sitio.self)!) {
            mSitio = sitio
        } else {
            observer?.PrintMessage(message: "Fallo la carga del sitio")
        }
    }

    func getProspecto() {
        // Intento de solucion
        if case let prospecto: NProspecto? = NProspecto(value: RealManager.findFirst(object: NProspecto.self)!) {
            mProspecto = prospecto
        } else {
            observer?.PrintMessage(message: "Fallo la carga del prospecto")
        }
    }

    func getFamily() -> [Plan] {
        return Familia.Family
    }

    func getNombre() -> String {
        return Familia.Nombre
    }

    func getDeal() -> String {
        return Familia.Deal
    }

    func getNegocio() -> String {
        let tipoNegocio = Familia.isNegocio ? "Negocio" : "Residencial"
        return tipoNegocio
    }

    func getPlan() -> Plan {
        return planSelected
    }
    func chargePlanes() {
        getSitio()
        getProspecto()
        let request: ConsultaPlanessRequest = ConsultaPlanessRequest()
        request.canal = mProspecto?.canalEmpleado
        request.subcanal = mProspecto?.subCanalEmpleado
        request.canalFront = "SalesforceWeb"
        request.cluster = mSitio?.cluster
        request.plazaTP = mSitio?.plaza
        request.distrito = mSitio?.distrito
        if mSitio?.isEstimulo == "true" {
            request.estimuloFiscal = true
        } else {
            request.estimuloFiscal = false
        }
        model.chargePlanes(request)
    }

    func onSuccesPlanes(consultaPlanesResponse: ConsultaPlanesResponse) {
        residenciales = []
        negocio = []
        planes = consultaPlanesResponse.planesTotalplay
        Striming = consultaPlanesResponse.serviciosStriming
        for plan in planes {
            if plan.tipoPlan == "Residencial" {
                residenciales.append(plan)
            } else if plan.tipoPlan == "Micronegocios" {
                negocio.append(plan)
            }
        }
        observer?.onSuccesChargePlanes()
    }

    func onErrorPlanes(message: String) {
        view.hideLoading()
        observer?.PrintMessage(message: message)
    }

    func getStriming() -> [ServiciosStriming] {
        return Striming
    }

    func getPlanesResidencial() -> [Plan] {
        return residenciales
    }

    func getPlanesNegocio() -> [Plan] {
        return negocio
    }

    func LoadRealm() {
        if RealManager.findFirst(object: ObjetoModelado.self) != nil {
            idPlanSelected = self.model.getIdPlan().planSeleccionadoId
            for plan in residenciales {
                if plan.id == idPlanSelected {
                    isResidencial = true
                    clasificaPlanes(key: plan.familiaPaquete)
                    planSelected = plan
                }
            }
            for plan in negocio {
                if plan.id == idPlanSelected {
                    isResidencial = false
                    clasificaPlanes(key: plan.familiaPaquete)
                    planSelected = plan
                }
            }
            if planSelected == nil {
                observer?.normalFlow()
            } else {
                view.hideLoading()
                observer?.changeView(page: fase.AddonConfig)
            }
        } else {
            observer?.normalFlow()
        }
    }

    func clasificaPlanes(key: String) {
        Familia.Family = []
        var auxPlanes: [Plan] = []
        if isResidencial {
            auxPlanes = residenciales
        } else {
            auxPlanes = negocio
        }
        for plan in auxPlanes {
            if plan.familiaPaquete.uppercased().contains(string: "DOBLE") && key.uppercased().contains(string: "DOBLE") {
                if plan.familiaPaquete.uppercased().contains(string: "NETFLIX") && key.uppercased().contains(string: "NETFLIX") {
                    Familia.Family.append(plan)
                }
                if plan.familiaPaquete.uppercased().contains(string: "AMAZON") && key.uppercased().contains(string: "AMAZON") {
                    Familia.Family.append(plan)
                }
                if !plan.familiaPaquete.uppercased().contains(string: "NETFLIX") && !key.uppercased().contains(string: "NETFLIX") && !plan.familiaPaquete.uppercased().contains(string: "AMAZON") && !key.uppercased().contains(string: "AMAZON") {
                    Familia.Family.append(plan)
                }
            } else if plan.familiaPaquete.uppercased().contains(string: "TRIPLE") && key.uppercased().contains(string: "TRIPLE") {
                if plan.familiaPaquete.uppercased().contains(string: "NETFLIX") && key.uppercased().contains(string: "NETFLIX") {
                    Familia.Family.append(plan)
                }
                if plan.familiaPaquete.uppercased().contains(string: "AMAZON") && key.uppercased().contains(string: "AMAZON") {
                    Familia.Family.append(plan)
                }
                if !plan.familiaPaquete.uppercased().contains(string: "NETFLIX") && !key.uppercased().contains(string: "NETFLIX") && !plan.familiaPaquete.uppercased().contains(string: "AMAZON") && !key.uppercased().contains(string: "AMAZON") {
                    Familia.Family.append(plan)
                }
            }
        }
    }
}
