//
//  FavoriteApps.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 03/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import SwiftEventBus
import Amplitude

class FavoriteApps: BaseVentasView {

    @IBOutlet weak var netflixView: UIView!
    @IBOutlet weak var netflixButton: UIButton!
    @IBOutlet weak var primeView: UIView!
    @IBOutlet weak var primeButton: UIButton!
    @IBOutlet weak var youtubeView: UIView!
    @IBOutlet weak var youtubeButton: UIButton!

    let checkImage = UIImage(named: "Image_rede_blue_check.png")
    let myImageView: UIImageView = UIImageView()
    let myImageView2: UIImageView = UIImageView()
    let myImageView3: UIImageView = UIImageView()

    override func viewDidLoad() {
        fasePreguntas.Index = 5
        super.viewDidLoad()
        super.setView(view: view)
        configureButton(array: fasePreguntas.favoriteApps)
        SwiftEventBus.post("FavoriteApps", sender: fasePreguntas.favoriteApps)
    }

    override func viewDidAppear(_ animated: Bool) {
        // Triger to send amplitude event "Show_BestFitFlow_Question6"
        Amplitude.instance().logEvent("Show_BestFitFlow_Question6", withEventProperties: ["Question": "¿Cuáles son sus apps favoritas?"])
    }

    override func viewDidDisappear(_ animated: Bool) {
        SwiftEventBus.post("FavoriteApps", sender: fasePreguntas.favoriteApps)
    }

    func configureButton(array: String) {

        if array.contains("NETFLIX") == true {
            addCheck(viewParent: netflixView, button: netflixButton)
            netflixButton.select()
        }

        if array.contains("AMAZON") == true {
            addCheck2(viewParent: primeView, button: primeButton)
            primeButton.select()
        }

        if array.contains("YOUTUBE") == true {
            addCheck3(viewParent: youtubeView, button: youtubeButton)
            youtubeButton.select()
        }
    }

    func addCheck(viewParent: UIView, button: UIButton) {
        myImageView.contentMode = UIView.ContentMode.scaleAspectFit
        myImageView.frame.size.width = 20
        myImageView.frame.size.height = 20
        myImageView.frame.origin.y = viewParent.frame.origin.y - 5
        myImageView.frame.origin.x = button.frame.origin.x + button.width - 15
        myImageView.image = checkImage
        viewParent.addSubview(myImageView)
    }

    func addCheck2(viewParent: UIView, button: UIButton) {
        myImageView2.contentMode = UIView.ContentMode.scaleAspectFit
        myImageView2.frame.size.width = 20
        myImageView2.frame.size.height = 20
        myImageView2.frame.origin.y = viewParent.frame.origin.y - 5
        myImageView2.frame.origin.x = button.frame.origin.x + button.width - 15
        myImageView2.image = checkImage
        viewParent.addSubview(myImageView2)
    }

    func addCheck3(viewParent: UIView, button: UIButton) {
        myImageView3.contentMode = UIView.ContentMode.scaleAspectFit
        myImageView3.frame.size.width = 20
        myImageView3.frame.size.height = 20
        myImageView3.frame.origin.y = viewParent.frame.origin.y - 5
        myImageView3.frame.origin.x = button.frame.origin.x + button.width - 15
        myImageView3.image = checkImage
        viewParent.addSubview(myImageView3)
    }

    // NETFLIX
    @IBAction func netflixAction(_ sender: UIButton) {
        if self.netflixButton.isSelected == false {
            fasePreguntas.favoriteAppsArray[sender.tag] = "NETFLIX"
            addCheck(viewParent: netflixView, button: netflixButton)
           netflixButton.select()
        } else {
            fasePreguntas.favoriteAppsArray[sender.tag]  = ""
            self.netflixButton.isSelected = false
            myImageView.removeFromSuperview()
            netflixButton.deaselect()
        }
        fasePreguntas.favoriteApps = fasePreguntas.favoriteAppsArray.filter({ $0 != ""}).joined(separator: ";")
        SwiftEventBus.post("FavoriteApps", sender: fasePreguntas.favoriteApps)
    }

    // prime
    @IBAction func primeAction(_ sender: UIButton) {
        if self.primeButton.isSelected == false {
            fasePreguntas.favoriteAppsArray[sender.tag]  = "AMAZON"
            addCheck2(viewParent: primeView, button: primeButton)
            primeButton.select()
        } else {
            fasePreguntas.favoriteAppsArray[sender.tag]  = ""
            self.primeButton.isSelected = false
            myImageView2.removeFromSuperview()
            primeButton.deaselect()
        }
        fasePreguntas.favoriteApps = fasePreguntas.favoriteAppsArray.filter({ $0 != ""}).joined(separator: ";")
        SwiftEventBus.post("FavoriteApps", sender: fasePreguntas.favoriteApps)
    }
    // youtube
    @IBAction func youtubeAction(_ sender: UIButton) {
        if self.youtubeButton.isSelected == false {
            fasePreguntas.favoriteAppsArray[sender.tag]  = "YOUTUBE"
            self.youtubeButton.borderColor = UIColor(named: "Base_rede_button_dark")
            addCheck3(viewParent: youtubeView, button: youtubeButton)
            self.youtubeButton.borderWidth = 3
            self.youtubeButton.isSelected = true
        } else {
            fasePreguntas.favoriteAppsArray[sender.tag]  = ""
            self.youtubeButton.isSelected = false
            myImageView3.removeFromSuperview()
            self.youtubeButton.borderColor = UIColor(named: "Base_rede_semiclear_gray")
            self.youtubeButton.borderWidth = 2
        }
        fasePreguntas.favoriteApps = fasePreguntas.favoriteAppsArray.filter({ $0 != ""}).joined(separator: ";")
        SwiftEventBus.post("FavoriteApps", sender: fasePreguntas.favoriteApps)
    }

}
