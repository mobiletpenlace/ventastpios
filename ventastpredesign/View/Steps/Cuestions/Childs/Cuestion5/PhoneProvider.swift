//
//  PhoneProvider.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 03/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import SwiftEventBus
import Amplitude

class PhoneProvider: BaseVentasView {

    @IBOutlet weak var telmexView: UIView!
    @IBOutlet weak var telmexButton: UIButton!
    @IBOutlet weak var izziView: UIView!
    @IBOutlet weak var izziButton: UIButton!
    @IBOutlet weak var megacableView: UIView!
    @IBOutlet weak var megacableButton: UIButton!
    @IBOutlet weak var otrosView: UIView!
    @IBOutlet weak var otrosButton: UIButton!
    @IBOutlet weak var ningunoView: UIView!
    @IBOutlet weak var ningunoButton: UIButton!
    @IBOutlet weak var otherProviderTextfield: UITextField!

    let checkImage = UIImage(named: "Image_rede_blue_check.png")
    let myImageView: UIImageView = UIImageView()

    override func viewDidLoad() {
        fasePreguntas.Index = 4
        super.viewDidLoad()
        super.setView(view: view)
        otherProviderTextfield.delegate = self
    }

    override func viewDidAppear(_ animated: Bool) {
        // Triger to send amplitude event "Show_BestFitFlow_Question5"
        Amplitude.instance().logEvent("Show_BestFitFlow_Question5", withEventProperties: ["Question": "Selecciona su proveedor actual de telefonía."])
            SwiftEventBus.post("ConfigurarBoton", sender: true)
            switch fasePreguntas.phoneProvider {
            case "TELMEX":
                    configureButton(button: telmexButton, view: telmexView)
            case "IZZI":
                    configureButton(button: izziButton, view: izziView)
            case "MEGACABLE":
                    configureButton(button: megacableButton, view: megacableView)
            case "OTRO":
                    otherProviderTextfield.isHidden = false
                    otherProviderTextfield.text = fasePreguntas.phoneProviderOther
                    configureButton(button: otrosButton, view: otrosView)
            case "NINGUNO":
                    configureButton(button: ningunoButton, view: ningunoView)
            default:
                    SwiftEventBus.post("ConfigurarBoton", sender: false)
            }
    }

    func configureButton(button: UIButton, view: UIView) {
        SwiftEventBus.post("ConfigurarBoton", sender: true)
        button.borderColor = UIColor(named: "Base_rede_button_dark")
        addCheck(viewParent: view, button: button)
        button.borderWidth = 3
        button.isSelected = true
    }

    override func viewDidDisappear(_ animated: Bool) {
        SwiftEventBus.post("PhoneProvider", sender: fasePreguntas.phoneProvider)
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            if !text.isEmpty {
                SwiftEventBus.post("ConfigurarBoton", sender: true)
            } else {
                SwiftEventBus.post("ConfigurarBoton", sender: false)
            }
            return true
        }

    func addCheck(viewParent: UIView, button: UIButton) {
        myImageView.contentMode = UIView.ContentMode.scaleAspectFit
        myImageView.frame.size.width = 20
        myImageView.frame.size.height = 20
        myImageView.frame.origin.y = viewParent.frame.origin.y - 5
        myImageView.frame.origin.x = button.frame.origin.x + button.width - 15
        myImageView.image = checkImage
        viewParent.addSubview(myImageView)
    }

    // telmex
    @IBAction func telmexAction(_ sender: UIButton) {
        if self.telmexButton.isSelected == false {
            butttonPressed(button: sender.tag)
            buttonsLogic()
            fasePreguntas.phoneProvider = "TELMEX"
            addCheck(viewParent: telmexView, button: telmexButton)
            telmexButton.select()
        } else {
            fasePreguntas.phoneProvider = ""
            butttonPressed(button: 10)
            self.telmexButton.isSelected = true
            myImageView.removeFromSuperview()
          buttonsLogic()
        }
        SwiftEventBus.post("PhoneProvider", sender: fasePreguntas.phoneProvider)
    }

    // IZZI
    @IBAction func izziAction(_ sender: UIButton) {
        if self.izziButton.isSelected == false {
            butttonPressed(button: sender.tag)
            buttonsLogic()
            fasePreguntas.phoneProvider = "IZZI"
            addCheck(viewParent: izziView, button: izziButton)
            izziButton.select()
        } else {
            butttonPressed(button: 10)
            fasePreguntas.phoneProvider = ""
            self.izziButton.isSelected = true
            myImageView.removeFromSuperview()
          buttonsLogic()
        }
        SwiftEventBus.post("PhoneProvider", sender: fasePreguntas.phoneProvider)

    }

    // MEGACABLE
    @IBAction func megacableAction(_ sender: UIButton) {
        if self.megacableButton.isSelected == false {
            butttonPressed(button: sender.tag)
            buttonsLogic()
            fasePreguntas.phoneProvider = "MEGACABLE"
            addCheck(viewParent: megacableView, button: megacableButton)
            megacableButton.select()
        } else {
            fasePreguntas.phoneProvider = ""
            butttonPressed(button: 10)
            self.megacableButton.isSelected = true
            myImageView.removeFromSuperview()
          buttonsLogic()
        }
        SwiftEventBus.post("PhoneProvider", sender: fasePreguntas.phoneProvider)
    }

    // OTHER
    @IBAction func otroAction(_ sender: UIButton) {
        if self.otrosButton.isSelected == false {
            butttonPressed(button: sender.tag)
            buttonsLogic()
            fasePreguntas.phoneProvider = "OTRO"
            addCheck(viewParent: otrosView, button: otrosButton)
            otrosButton.select()
            otherProviderTextfield.isHidden = false
        } else {
            fasePreguntas.phoneProvider = ""
            butttonPressed(button: 10)
            self.otrosButton.isSelected = true
            myImageView.removeFromSuperview()
          buttonsLogic()
        }
        SwiftEventBus.post("PhoneProvider", sender: fasePreguntas.phoneProvider)
    }

    // NOTHING
    @IBAction func ningunoTVAction(_ sender: UIButton) {
        if self.ningunoButton.isSelected == false {
            butttonPressed(button: sender.tag)
            buttonsLogic()
            fasePreguntas.phoneProvider = "NINGUNO"
            addCheck(viewParent: ningunoView, button: ningunoButton)
            ningunoButton.select()
        } else {
            fasePreguntas.phoneProvider = ""
            butttonPressed(button: 10)
            self.ningunoButton.isSelected = true
            myImageView.removeFromSuperview()
          buttonsLogic()
        }
        SwiftEventBus.post("PhoneProvider", sender: fasePreguntas.phoneProvider)
    }

    func butttonPressed(button: Int) {
        if button != 10 {
            switch button {
            case 0, 1, 2, 4:
                SwiftEventBus.post("ConfigurarBoton", sender: true)
            case 3:
                if otherProviderTextfield.text == "" {
                    SwiftEventBus.post("ConfigurarBoton", sender: false)
                } else {
                    SwiftEventBus.post("ConfigurarBoton", sender: true )
                }
                default:
                    break
            }
        }
        if button == 10 {
            SwiftEventBus.post("ConfigurarBoton", sender: false)
        }
    }

    func buttonsLogic() {
        otherProviderTextfield.isHidden = true
        telmexButton.deaselect()
        telmexButton.deaselect()
        izziButton.deaselect()
        megacableButton.deaselect()
        otrosButton.deaselect()
        ningunoButton.deaselect()
    }

    @IBAction func changeTextField(_ sender: Any) {
        fasePreguntas.phoneProviderOther = otherProviderTextfield.text!
        if otherProviderTextfield.text == "" {
            SwiftEventBus.post("ConfigurarBoton", sender: false)
        } else {
            SwiftEventBus.post("ConfigurarBoton", sender: true)
        }
    }

}
