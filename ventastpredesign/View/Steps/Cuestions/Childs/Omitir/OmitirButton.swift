//
//  OmitirButton.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 07/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import SwiftEventBus

class OmitirButton: BaseVentasView {
    @IBOutlet var popUpView: UIView!
    @IBOutlet weak var cuestionsView: UIView!
    @IBOutlet weak var configurePlanView: UIView!
    @IBOutlet weak var nothingPlanView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
    }

    @IBAction func closeNothingAction(_ sender: Any) {
        Constants.Back(viewC: self)
        SwiftEventBus.post("Cambia_paso_container", sender: 3)
    }
    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }

    @IBAction func omitirUnaPreguntaAction(_ sender: Any) {
        switch fasePreguntas.Index {
        case 0:
            fasePreguntas.numberOfPeople = 2
            SwiftEventBus.post("OmitirPregunta", sender: 1)
        case 1:
            fasePreguntas.numberOfRooms = 2
            SwiftEventBus.post("OmitirPregunta", sender: 2)
        case 2:
            fasePreguntas.tvProvider = "OMITIR"
            SwiftEventBus.post("OmitirPregunta", sender: 3)
        case 3:
            fasePreguntas.internetProvider = "OMITIR"
            SwiftEventBus.post("OmitirPregunta", sender: 4)
        case 4:
            fasePreguntas.phoneProvider = "OMITIR"
            SwiftEventBus.post("OmitirPregunta", sender: 5)
        case 5:
            fasePreguntas.favoriteApps = "OMITIR"
            SwiftEventBus.post("OmitirPregunta", sender: 6)
        case 6:
            fasePreguntas.usesInternet = "OMITIR"
            let viewAlert: UIStoryboard = UIStoryboard(name: "StepContainer", bundle: nil)
            let viewAlertVC = viewAlert.instantiateViewController(withIdentifier: "StepContainer")as! StepContainer
                viewAlertVC.providesPresentationContextTransitionStyle = true
                viewAlertVC.definesPresentationContext = true
                viewAlertVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                viewAlertVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                self.present(viewAlertVC, animated: false, completion: nil)
        default:
            break
        }
        Constants.Back(viewC: self)
    }

    @IBAction func omitirTodasAction(_ sender: Any) {
        print(fasePreguntas.Index)
        fasePreguntas.numberOfPeople = 2
        fasePreguntas.numberOfRooms = 2
        fasePreguntas.tvProvider = "OMITIR"
        fasePreguntas.internetProvider = "OMITIR"
        fasePreguntas.phoneProvider = "OMITIR"
        fasePreguntas.favoriteApps = "OMITIR"
        fasePreguntas.usesInternet = "OMITIR"
        // SwiftEventBus.post("OmitirPregunta", sender: 7)
        cuestionsView.isHidden = true
        faseConfigurarPlan.flujo = 0
    }

    @IBAction func reiniciarAction(_ sender: Any) {
        SwiftEventBus.post("OmitirPregunta", sender: 0)
        Constants.Back(viewC: self)
    }

    @IBAction func configureAction(_ sender: Any) {
        SwiftEventBus.post("Cambia_paso_container", sender: 4)
        Constants.Back(viewC: self)
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        guard let location = touch?.location(in: self.view) else { return }
        if !popUpView.frame.contains(location) {
           Constants.Back(viewC: self)
        }
    }
}
