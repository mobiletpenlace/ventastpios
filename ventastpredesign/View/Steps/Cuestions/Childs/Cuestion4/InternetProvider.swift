//
//  InternetProvider.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 03/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import SwiftEventBus
import Amplitude

class InternetProvider: BaseVentasView {

    @IBOutlet weak var infinitumView: UIView!
    @IBOutlet weak var infinitumButton: UIButton!
    @IBOutlet weak var izziView: UIView!
    @IBOutlet weak var izziButton: UIButton!
    @IBOutlet weak var megacableView: UIView!
    @IBOutlet weak var megacableButton: UIButton!
    @IBOutlet weak var otrosView: UIView!
    @IBOutlet weak var otrosButton: UIButton!
    @IBOutlet weak var ningunoView: UIView!
    @IBOutlet weak var ningunoButton: UIButton!
    @IBOutlet weak var otherProviderTextfield: UITextField!

    let checkImage = UIImage(named: "Image_rede_blue_check.png")
    let myImageView: UIImageView = UIImageView()

    override func viewDidLoad() {

        fasePreguntas.Index = 3
        super.viewDidLoad()
        super.setView(view: view)
        otherProviderTextfield.delegate = self
    }

    override func viewDidAppear(_ animated: Bool) {
        // Triger to send amplitude event "Show_BestFitFlow_Question4"
        Amplitude.instance().logEvent("Show_BestFitFlow_Question4", withEventProperties: ["Question": "Selecciona su proveedor actual de  internet."])
        SwiftEventBus.post("ConfigurarBoton", sender: true)
            switch fasePreguntas.internetProvider {
            case "TELMEX":
                    configureButton(button: infinitumButton, view: infinitumView)
            case "IZZI":
                    configureButton(button: izziButton, view: izziView)
            case "MEGACABLE":
                    configureButton(button: megacableButton, view: megacableView)
            case "OTRO":
                    otherProviderTextfield.isHidden = false
                    otherProviderTextfield.text = fasePreguntas.internetProviderOther
                    configureButton(button: otrosButton, view: otrosView)
            case "NINGUNO":
                    configureButton(button: ningunoButton, view: ningunoView)
            default:
                    SwiftEventBus.post("ConfigurarBoton", sender: false)
                    break
            }
    }

    func configureButton(button: UIButton, view: UIView) {
        SwiftEventBus.post("ConfigurarBoton", sender: true)
        button.borderColor = UIColor(named: "Base_rede_button_dark")
        addCheck(viewParent: view, button: button)
        button.borderWidth = 3
        button.isSelected = true
    }

    override func viewDidDisappear(_ animated: Bool) {
        SwiftEventBus.post("InternetProvider", sender: fasePreguntas.internetProvider)
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if !text.isEmpty {
            SwiftEventBus.post("ConfigurarBoton", sender: true)
        } else {
            SwiftEventBus.post("ConfigurarBoton", sender: false)
        }
        return true
    }

    func addCheck(viewParent: UIView, button: UIButton) {
        myImageView.contentMode = UIView.ContentMode.scaleAspectFit
        myImageView.frame.size.width = 20
        myImageView.frame.size.height = 20
        myImageView.frame.origin.y = viewParent.frame.origin.y - 5
        myImageView.frame.origin.x = button.frame.origin.x + button.width - 15
        myImageView.image = checkImage
        viewParent.addSubview(myImageView)
    }

    // INFINITUM
    @IBAction func infinitumAction(_ sender: UIButton) {
        if self.infinitumButton.isSelected == false {
            butttonPressed(button: sender.tag)
            buttonsLogic()
            fasePreguntas.internetProvider = "TELMEX"
            self.infinitumButton.borderColor = UIColor(named: "Base_rede_button_dark")
            addCheck(viewParent: infinitumView, button: infinitumButton)
            self.infinitumButton.borderWidth = 3
            self.infinitumButton.isSelected = true
        } else {
            fasePreguntas.internetProvider = ""
            butttonPressed(button: 10)
            self.infinitumButton.isSelected = true
            myImageView.removeFromSuperview()
          buttonsLogic()
        }
        SwiftEventBus.post("InternetProvider", sender: fasePreguntas.internetProvider)
    }

    // IZZI
    @IBAction func izziAction(_ sender: UIButton) {
        if self.izziButton.isSelected == false {
            butttonPressed(button: sender.tag)
            buttonsLogic()
            fasePreguntas.internetProvider = "IZZI"
            self.izziButton.borderColor = UIColor(named: "Base_rede_button_dark")
            addCheck(viewParent: izziView, button: izziButton)
            self.izziButton.borderWidth = 3
            self.izziButton.isSelected = true
        } else {
            fasePreguntas.internetProvider = ""
            butttonPressed(button: 10)
            self.izziButton.isSelected = true
            myImageView.removeFromSuperview()
          buttonsLogic()
        }
        SwiftEventBus.post("InternetProvider", sender: fasePreguntas.internetProvider)
    }

    // MEGACABLE
    @IBAction func megacableAction(_ sender: UIButton) {
        if self.megacableButton.isSelected == false {
            butttonPressed(button: sender.tag)
            buttonsLogic()
            fasePreguntas.internetProvider = "MEGACABLE"
            self.megacableButton.borderColor = UIColor(named: "Base_rede_button_dark")
            addCheck(viewParent: megacableView, button: megacableButton)
            self.megacableButton.borderWidth = 3
            self.megacableButton.isSelected = true
        } else {
            fasePreguntas.internetProvider = ""
            butttonPressed(button: 10)
            self.megacableButton.isSelected = true
            myImageView.removeFromSuperview()
          buttonsLogic()
        }
        SwiftEventBus.post("InternetProvider", sender: fasePreguntas.internetProvider)
    }

    // OTHER
    @IBAction func otroAction(_ sender: UIButton) {
        if self.otrosButton.isSelected == false {
            butttonPressed(button: sender.tag)
            buttonsLogic()
            fasePreguntas.internetProvider = "OTRO"
            self.otrosButton.borderColor = UIColor(named: "Base_rede_button_dark")
            addCheck(viewParent: otrosView, button: otrosButton)
            self.otrosButton.borderWidth = 3
            self.otrosButton.isSelected = true
            otherProviderTextfield.isHidden = false
        } else {
            fasePreguntas.internetProvider = ""
            butttonPressed(button: 10)
            self.otrosButton.isSelected = true
            myImageView.removeFromSuperview()
          buttonsLogic()
        }
        SwiftEventBus.post("InternetProvider", sender: fasePreguntas.internetProvider)
    }

    // NOTHING
    @IBAction func ningunoTVAction(_ sender: UIButton) {
        if self.ningunoButton.isSelected == false {
            butttonPressed(button: sender.tag)
            buttonsLogic()
            fasePreguntas.internetProvider = "NINGUNO"
            self.ningunoButton.borderColor = UIColor(named: "Base_rede_button_dark")
            addCheck(viewParent: ningunoView, button: ningunoButton)
            self.ningunoButton.borderWidth = 3
            self.ningunoButton.isSelected = true
        } else {
            fasePreguntas.internetProvider = ""
            butttonPressed(button: 10)
            self.ningunoButton.isSelected = true
            myImageView.removeFromSuperview()
          buttonsLogic()
        }
        SwiftEventBus.post("InternetProvider", sender: fasePreguntas.internetProvider)
    }

    func butttonPressed(button: Int) {
        if button != 10 {
            switch button {
            case 0, 1, 2, 4:
                SwiftEventBus.post("ConfigurarBoton", sender: true)
            case 3:
                if otherProviderTextfield.text == "" {
                    SwiftEventBus.post("ConfigurarBoton", sender: false)
                } else {
                    SwiftEventBus.post("ConfigurarBoton", sender: true )
                }
            default:
                break
            }
        }
        if button == 10 {
            SwiftEventBus.post("ConfigurarBoton", sender: false)
        }
    }

    func buttonsLogic() {
        fasePreguntas.internetProvider = ""
        otherProviderTextfield.isHidden = true
        infinitumButton.deaselect()
        izziButton.deaselect()
        megacableButton.deaselect()
        otrosButton.deaselect()
        ningunoButton.deaselect()
    }

    @IBAction func changeTextField(_ sender: Any) {
        fasePreguntas.internetProviderOther = otherProviderTextfield.text!
        if otherProviderTextfield.text == "" {
            SwiftEventBus.post("ConfigurarBoton", sender: false)
        } else {
            SwiftEventBus.post("ConfigurarBoton", sender: true)
        }
    }

}
