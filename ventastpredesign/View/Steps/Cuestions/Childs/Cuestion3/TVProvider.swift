//
//  TVProvider.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 01/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import SwiftEventBus
import Amplitude

class TVProvider: BaseVentasView {

    @IBOutlet weak var megacableView: UIView!
    @IBOutlet weak var megacableButton: UIButton!
    @IBOutlet weak var dishView: UIView!
    @IBOutlet weak var dishButton: UIButton!
    @IBOutlet weak var vetvView: UIView!
    @IBOutlet weak var vetvButton: UIButton!
    @IBOutlet weak var skyView: UIView!
    @IBOutlet weak var skyButton: UIButton!
    @IBOutlet weak var izziView: UIView!
    @IBOutlet weak var izziTVButton: UIButton!
    @IBOutlet weak var otrosView: UIView!
    @IBOutlet weak var otrosButton: UIButton!
    @IBOutlet weak var ningunoView: UIView!
    @IBOutlet weak var ningunoButton: UIButton!
    @IBOutlet weak var otherProviderTextfield: UITextField!

    let checkImage = UIImage(named: "Image_rede_blue_check.png")
    let myImageView: UIImageView = UIImageView()
    var tvProvider = ""

    deinit {
        print("DEINIT TVPROVIDER")
    }

    override func viewDidLoad() {
        fasePreguntas.Index = 2
        super.viewDidLoad()
        super.setView(view: view)
        otherProviderTextfield.delegate = self
    }

    override func viewDidAppear(_ animated: Bool) {
        // Triger to send amplitude event "Show_BestFitFlow_Question3"s
        Amplitude.instance().logEvent("Show_BestFitFlow_Question3", withEventProperties: ["Question": "Selecciona su proveedor actual de TV de paga."])
        switch fasePreguntas.tvProvider {
        case "MEGACABLE":
            configureButton(button: megacableButton, view: megacableView)
        case "DISH":
            configureButton(button: dishButton, view: dishView)
        case "VETV":
            configureButton(button: vetvButton, view: vetvView)
        case "SKY":
            configureButton(button: skyButton, view: skyView)
        case "IZZI":
            configureButton(button: izziTVButton, view: izziView)
        case "OTRO":
            otherProviderTextfield.isHidden = false
            otherProviderTextfield.text = fasePreguntas.tvProviderOther
            configureButton(button: otrosButton, view: otrosView)
        case "NINGUNO":
            configureButton(button: ningunoButton, view: ningunoView)
        default:
            SwiftEventBus.post("ConfigurarBoton", sender: false)
        }

    }

    func configureButton(button: UIButton, view: UIView) {
        SwiftEventBus.post("ConfigurarBoton", sender: true)
        button.borderColor = UIColor(named: "Base_rede_button_dark")
        addCheck(viewParent: view, button: button)
        button.borderWidth = 3
        button.isSelected = true
    }

    override func viewDidDisappear(_ animated: Bool) {
        SwiftEventBus.post("TVProvider", sender: fasePreguntas.tvProvider)
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if !text.isEmpty {
            SwiftEventBus.post("ConfigurarBoton", sender: true)
        } else {
            SwiftEventBus.post("ConfigurarBoton", sender: false)
        }
        return true
    }

    func addCheck(viewParent: UIView, button: UIButton) {
        myImageView.contentMode = UIView.ContentMode.scaleAspectFit
        myImageView.frame.size.width = 20
        myImageView.frame.size.height = 20
        myImageView.frame.origin.y = viewParent.frame.origin.y - 5
        myImageView.frame.origin.x = button.frame.origin.x + button.width - 15
        myImageView.image = checkImage
        viewParent.addSubview(myImageView)
    }

    func selectButton (button: UIButton) {
        buttonsLogic()
        fasePreguntas.tvProvider = "MEGACABLE"
        self.megacableButton.borderColor = UIColor(named: "Base_rede_button_dark")
        addCheck(viewParent: megacableView, button: megacableButton)
        self.megacableButton.borderWidth = 3
        self.megacableButton.isSelected = true
    }

    // MEGACABLE
    @IBAction func megacableAction(_ sender: UIButton) {
        if self.megacableButton.isSelected == false {
            butttonPressed(button: sender.tag)
            buttonsLogic()
            fasePreguntas.tvProvider = "MEGACABLE"
            addCheck(viewParent: megacableView, button: megacableButton)
            megacableButton.select()
        } else {
            butttonPressed(button: 10)
            self.megacableButton.isSelected = true
            myImageView.removeFromSuperview()
          buttonsLogic()
        }
        SwiftEventBus.post("TVProvider", sender: tvProvider)
    }

    // DISH
    @IBAction func dishAction(_ sender: UIButton) {
        if self.dishButton.isSelected == false {
            butttonPressed(button: sender.tag)
            buttonsLogic()
            fasePreguntas.tvProvider = "DISH"
            addCheck(viewParent: dishView, button: dishButton)
            dishButton.select()
        } else {
            butttonPressed(button: 10)
            self.dishButton.isSelected = true
            myImageView.removeFromSuperview()
          buttonsLogic()
        }
        SwiftEventBus.post("TVProvider", sender: tvProvider)
    }

    // VETV
    @IBAction func vetvAction(_ sender: UIButton) {
        if self.vetvButton.isSelected == false {
            butttonPressed(button: sender.tag)
            buttonsLogic()
            fasePreguntas.tvProvider = "VETV"
            addCheck(viewParent: vetvView, button: vetvButton)
            vetvButton.select()
        } else {
            butttonPressed(button: 10)
            self.vetvButton.isSelected = true
            myImageView.removeFromSuperview()
          buttonsLogic()
        }
        SwiftEventBus.post("TVProvider", sender: tvProvider)
    }

    // SKY
    @IBAction func skyAction(_ sender: UIButton) {
        if self.skyButton.isSelected == false {
            butttonPressed(button: sender.tag)
            buttonsLogic()
            fasePreguntas.tvProvider = "SKY"
            addCheck(viewParent: skyView, button: skyButton)
            skyButton.select()
        } else {
            butttonPressed(button: 10)
            self.skyButton.isSelected = true
            myImageView.removeFromSuperview()
          buttonsLogic()
        }
        SwiftEventBus.post("TVProvider", sender: tvProvider)
    }

    // IZZI
    @IBAction func izziTVAction(_ sender: UIButton) {
        if self.izziTVButton.isSelected == false {
            butttonPressed(button: sender.tag)
            buttonsLogic()
            fasePreguntas.tvProvider = "IZZI"
            addCheck(viewParent: izziView, button: izziTVButton)
            izziTVButton.select()
        } else {
            butttonPressed(button: 10)
            self.izziTVButton.isSelected = true
            myImageView.removeFromSuperview()
            buttonsLogic()
        }
        SwiftEventBus.post("TVProvider", sender: tvProvider)
    }

    // OTHER
    @IBAction func otroAction(_ sender: UIButton) {
        if self.otrosButton.isSelected == false {
            if otherProviderTextfield.text != "" {
                SwiftEventBus.post("ConfigurarBoton", sender: true)
            }
            butttonPressed(button: sender.tag)
            buttonsLogic()
            fasePreguntas.tvProvider = "OTRO"
            addCheck(viewParent: otrosView, button: otrosButton)
            otrosButton.select()
            otherProviderTextfield.isHidden = false
        } else {
            butttonPressed(button: 10)
            self.otrosButton.isSelected = true
            myImageView.removeFromSuperview()
          buttonsLogic()
        }
        SwiftEventBus.post("TVProvider", sender: tvProvider)
    }

    // NOTHING
    @IBAction func ningunoTVAction(_ sender: UIButton) {
        if self.ningunoButton.isSelected == false {
            butttonPressed(button: sender.tag)
            buttonsLogic()
            addCheck(viewParent: ningunoView, button: ningunoButton)
            fasePreguntas.tvProvider = "NINGUNO"
            ningunoButton.select()
        } else {
            butttonPressed(button: 10)
            self.ningunoButton.isSelected = true
            myImageView.removeFromSuperview()
          buttonsLogic()
        }
        SwiftEventBus.post("TVProvider", sender: tvProvider)
    }

    func butttonPressed(button: Int) {
        if button != 10 {
            switch button {
            case 0, 1, 2, 3, 4, 6:
                SwiftEventBus.post("ConfigurarBoton", sender: true)
            case 5:
                if otherProviderTextfield.text == "" {
                    SwiftEventBus.post("ConfigurarBoton", sender: false)
                } else {
                    SwiftEventBus.post("ConfigurarBoton", sender: true )
                }
            default:
                break
            }
        }
        if button == 10 {
            SwiftEventBus.post("ConfigurarBoton", sender: false)
        }
    }

    func buttonsLogic() {
        tvProvider = ""
        otherProviderTextfield.isHidden = true
        megacableButton.deaselect()
        dishButton.deaselect()
        vetvButton.deaselect()
        skyButton.deaselect()
        izziTVButton.deaselect()
        otrosButton.deaselect()
        ningunoButton.deaselect()
    }

    @IBAction func changeTextField(_ sender: Any) {
        fasePreguntas.tvProviderOther = otherProviderTextfield.text!
        if otherProviderTextfield.text == "" {
            SwiftEventBus.post("ConfigurarBoton", sender: false)
        } else {
            SwiftEventBus.post("ConfigurarBoton", sender: true)
        }
    }
}
