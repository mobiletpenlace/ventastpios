//
//  InternetUses.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 03/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import SwiftEventBus
import Amplitude

class InternetUses: BaseVentasView {
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var navigationButton: UIButton!
    @IBOutlet weak var socialNetworksView: UIView!
    @IBOutlet weak var socialNetworksButton: UIButton!
    @IBOutlet weak var homeOfficeView: UIView!
    @IBOutlet weak var homeOfficeButton: UIButton!
    @IBOutlet weak var onlineClassView: UIView!
    @IBOutlet weak var onlineClassButton: UIButton!
    @IBOutlet weak var videogamesView: UIView!
    @IBOutlet weak var videogamesButton: UIButton!
    @IBOutlet weak var otherView: UIView!
    @IBOutlet weak var otherButton: UIButton!
    @IBOutlet weak var otherProviderTextfield: UITextField!

    let checkImage = UIImage(named: "Image_rede_blue_check.png")
    let myImageView: UIImageView = UIImageView()
    let myImageView2: UIImageView = UIImageView()
    let myImageView3: UIImageView = UIImageView()
    let myImageView4: UIImageView = UIImageView()
    let myImageView5: UIImageView = UIImageView()
    let myImageView6: UIImageView = UIImageView()
    var Banderas: [Bool] = [false, false, false, false, false, false]

    override func viewDidLoad() {
        fasePreguntas.Index = 6
        super.viewDidLoad()
        super.setView(view: view)
        otherProviderTextfield.delegate = self
        configureButton(array: fasePreguntas.usesInternet)
    }

    override func viewDidAppear(_ animated: Bool) {
        // Triger to send amplitude event "Show_BestFitFlow_Question7"
        Amplitude.instance().logEvent("Show_BestFitFlow_Question7", withEventProperties: ["Question": "¿Qué otros usos le da al internet?"])
        if fasePreguntas.usesInternet == "" {
            SwiftEventBus.post("ConfigurarBoton", sender: false)
        } else {
            if self.otherButton.isSelected == false {
                otherProviderTextfield.isHidden = true
            } else {
                otherProviderTextfield.isHidden = false
            }
            otherProviderTextfield.text = fasePreguntas.usesInternetOther
            configureButton(array: fasePreguntas.favoriteApps)
            SwiftEventBus.post("ConfigurarBoton", sender: true)
        }
        SwiftEventBus.post("InternetUses", sender: fasePreguntas.usesInternet)
    }

    override func viewDidDisappear(_ animated: Bool) {
        SwiftEventBus.post("InternetUses", sender: fasePreguntas.usesInternet)
    }

    func configureButton(array: String) {
        if array == "" {
            SwiftEventBus.post("ConfigurarBoton", sender: false)
        } else {
            if array.contains("NAVEGACION INTERNET") == true {
                addCheck(viewParent: navigationView, button: navigationButton)
                navigationButton.select()
            }

            if array.contains("REDES SOCIALES") == true {
                addCheck2(viewParent: socialNetworksView, button: socialNetworksButton)
                socialNetworksButton.select()
            }

            if array.contains("TRABAJO DESDE CASA") == true {
                addCheck3(viewParent: homeOfficeView, button: homeOfficeButton)
                homeOfficeButton.select()
            }

            if array.contains("CLASES EN LINEA") == true {
                addCheck4(viewParent: onlineClassView, button: onlineClassButton)
                onlineClassButton.select()
            }

            if array.contains("VIDEOJUEGOS") == true {
                addCheck5(viewParent: videogamesView, button: videogamesButton)
                videogamesButton.select()
            }

            if array.contains("OTRO") == true {
                addCheck6(viewParent: otherView, button: otherButton)
                otherButton.select()
            }
        }
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            if !text.isEmpty {
                Banderas[5] = true
                SwiftEventBus.post("ConfigurarBoton", sender: true)
            } else {
                Banderas[5] = false
                if !hasAlmostOneFlag() {
                    butttonPressed(button: 10)
                }
            }
            return true
        }

    func addCheck(viewParent: UIView, button: UIButton) {
        myImageView.contentMode = UIView.ContentMode.scaleAspectFit
        myImageView.frame.size.width = 20
        myImageView.frame.size.height = 20
        myImageView.frame.origin.y = viewParent.frame.origin.y - 5
        myImageView.frame.origin.x = button.frame.origin.x + button.width - 15
        myImageView.image = checkImage
        viewParent.addSubview(myImageView)
    }

    func addCheck2(viewParent: UIView, button: UIButton) {
        myImageView2.contentMode = UIView.ContentMode.scaleAspectFit
        myImageView2.frame.size.width = 20
        myImageView2.frame.size.height = 20
        myImageView2.frame.origin.y = viewParent.frame.origin.y - 5
        myImageView2.frame.origin.x = button.frame.origin.x + button.width - 15
        myImageView2.image = checkImage
        viewParent.addSubview(myImageView2)
    }

    func addCheck3(viewParent: UIView, button: UIButton) {
        myImageView3.contentMode = UIView.ContentMode.scaleAspectFit
        myImageView3.frame.size.width = 20
        myImageView3.frame.size.height = 20
        myImageView3.frame.origin.y = viewParent.frame.origin.y - 5
        myImageView3.frame.origin.x = button.frame.origin.x + button.width - 15
        myImageView3.image = checkImage
        viewParent.addSubview(myImageView3)
    }

    func addCheck4(viewParent: UIView, button: UIButton) {
        myImageView4.contentMode = UIView.ContentMode.scaleAspectFit
        myImageView4.frame.size.width = 20
        myImageView4.frame.size.height = 20
        myImageView4.frame.origin.y = viewParent.frame.origin.y - 5
        myImageView4.frame.origin.x = button.frame.origin.x + button.width - 15
        myImageView4.image = checkImage
        viewParent.addSubview(myImageView4)
    }

    func addCheck5(viewParent: UIView, button: UIButton) {
        myImageView5.contentMode = UIView.ContentMode.scaleAspectFit
        myImageView5.frame.size.width = 20
        myImageView5.frame.size.height = 20
        myImageView5.frame.origin.y = viewParent.frame.origin.y - 5
        myImageView5.frame.origin.x = button.frame.origin.x + button.width - 15
        myImageView5.image = checkImage
        viewParent.addSubview(myImageView5)
    }

    func addCheck6(viewParent: UIView, button: UIButton) {
        myImageView6.contentMode = UIView.ContentMode.scaleAspectFit
        myImageView6.frame.size.width = 20
        myImageView6.frame.size.height = 20
        myImageView6.frame.origin.y = viewParent.frame.origin.y - 5
        myImageView6.frame.origin.x = button.frame.origin.x + button.width - 15
        myImageView6.image = checkImage
        viewParent.addSubview(myImageView6)
    }

    // NAVIGATION
    @IBAction func navigationAction(_ sender: UIButton) {
        if self.navigationButton.isSelected == false {
            butttonPressed(button: sender.tag)
            navigationButton.select()
            fasePreguntas.usesInternetArray[sender.tag] = "NAVEGACION INTERNET"
            addCheck(viewParent: navigationView, button: navigationButton)
            Banderas[0] = true
        } else {
            Banderas[0] = false
            if !hasAlmostOneFlag() {
                butttonPressed(button: 10)
            }
            fasePreguntas.usesInternetArray[sender.tag] = ""
            myImageView.removeFromSuperview()
            navigationButton.deaselect()

        }
        fasePreguntas.usesInternet = fasePreguntas.usesInternetArray.filter({ $0 != ""}).joined(separator: ";")
        SwiftEventBus.post("InternetUses", sender: fasePreguntas.usesInternet)
    }

    // socialNetworks
    @IBAction func socialNetworksAction(_ sender: UIButton) {
        if self.socialNetworksButton.isSelected == false {
            butttonPressed(button: sender.tag)
            fasePreguntas.usesInternetArray[sender.tag] = "REDES SOCIALES"
            addCheck2(viewParent: socialNetworksView, button: socialNetworksButton)
            socialNetworksButton.select()
            Banderas[1] = true
        } else {
            Banderas[1] = false
            if !hasAlmostOneFlag() {
                butttonPressed(button: 10)
            }
            fasePreguntas.usesInternetArray[sender.tag] = ""
            myImageView2.removeFromSuperview()
            socialNetworksButton.deaselect()
        }
        fasePreguntas.usesInternet = fasePreguntas.usesInternetArray.filter({ $0 != ""}).joined(separator: ";")
        SwiftEventBus.post("InternetUses", sender: fasePreguntas.usesInternet)
    }
    // homeOffice
    @IBAction func homeOfficeAction(_ sender: UIButton) {
        if self.homeOfficeButton.isSelected == false {
            butttonPressed(button: sender.tag)
            fasePreguntas.usesInternetArray[sender.tag] = "TRABAJO DESDE CASA"
            addCheck3(viewParent: homeOfficeView, button: homeOfficeButton)
            homeOfficeButton.select()
            Banderas[2] = true
        } else {
            Banderas[2] = false
            if !hasAlmostOneFlag() {
                butttonPressed(button: 10)
            }
            fasePreguntas.usesInternetArray[sender.tag] = ""
            myImageView3.removeFromSuperview()
            homeOfficeButton.deaselect()
        }
        fasePreguntas.usesInternet = fasePreguntas.usesInternetArray.filter({ $0 != ""}).joined(separator: ";")
        SwiftEventBus.post("InternetUses", sender: fasePreguntas.usesInternet)
    }

    // homeOffice
    @IBAction func onlineClassAction(_ sender: UIButton) {
        if self.onlineClassButton.isSelected == false {
            butttonPressed(button: sender.tag)
            fasePreguntas.usesInternetArray[sender.tag] = "CLASES EN LINEA"
            addCheck4(viewParent: onlineClassView, button: onlineClassButton)
            onlineClassButton.select()
            Banderas[3] = true
        } else {
            Banderas[3] = false
            if !hasAlmostOneFlag() {
                butttonPressed(button: 10)
            }
            fasePreguntas.usesInternetArray[sender.tag] = ""
            myImageView4.removeFromSuperview()
            onlineClassButton.deaselect()
        }
        fasePreguntas.usesInternet = fasePreguntas.usesInternetArray.filter({ $0 != ""}).joined(separator: ";")
        SwiftEventBus.post("InternetUses", sender: fasePreguntas.usesInternet)
    }

// VIDEOGAMES
    @IBAction func videogamesAction(_ sender: UIButton) {
        if self.videogamesButton.isSelected == false {
            butttonPressed(button: sender.tag)
            fasePreguntas.usesInternetArray[sender.tag] = "VIDEOJUEGOS"
            addCheck5(viewParent: videogamesView, button: videogamesButton)
            videogamesButton.select()
            Banderas[4] = true
        } else {
            Banderas[4] = false
            if !hasAlmostOneFlag() {
                butttonPressed(button: 10)
            }
            fasePreguntas.usesInternetArray[sender.tag] = ""
            myImageView5.removeFromSuperview()
            videogamesButton.deaselect()
        }
        fasePreguntas.usesInternet = fasePreguntas.usesInternetArray.filter({ $0 != ""}).joined(separator: ";")
        SwiftEventBus.post("InternetUses", sender: fasePreguntas.usesInternet)
    }

    // Other
        @IBAction func otherAction(_ sender: UIButton) {
            if self.otherButton.isSelected == false {
                butttonPressed(button: sender.tag)
                fasePreguntas.usesInternetArray[sender.tag] = "OTRO"
                addCheck6(viewParent: otherView, button: otherButton)
                otherButton.select()
                otherProviderTextfield.isHidden = false
            } else {
                Banderas[5] = false
                if !hasAlmostOneFlag() {
                    butttonPressed(button: 10)
                } else {
                    SwiftEventBus.post("ConfigurarBoton", sender: true)
                }
                fasePreguntas.usesInternetArray[sender.tag] = ""
                otherProviderTextfield.isHidden = true
                myImageView6.removeFromSuperview()
                otherButton.deaselect()
            }
            fasePreguntas.usesInternet = fasePreguntas.usesInternetArray.filter({ $0 != ""}).joined(separator: ";")
            SwiftEventBus.post("InternetUses", sender: fasePreguntas.usesInternet)
        }

    func butttonPressed(button: Int) {
        if button != 10 {
            switch button {
            case 0, 1, 2, 3, 4, 6:
                if otherProviderTextfield.text == ""  && Banderas[3] == true {
                    SwiftEventBus.post("ConfigurarBoton", sender: false)
                } else {
                    SwiftEventBus.post("ConfigurarBoton", sender: true)
                }
            case 5:
                if otherProviderTextfield.text == "" {
                    SwiftEventBus.post("ConfigurarBoton", sender: false)
                } else {
                    SwiftEventBus.post("ConfigurarBoton", sender: true)
                }
            default:
                break
            }
        }
        if button == 10 {
            SwiftEventBus.post("ConfigurarBoton", sender: false)
        }
    }

    func hasAlmostOneFlag() -> Bool {
        var isBandera: Bool = false
        for bandera in Banderas {
            if bandera {
                isBandera = true
            }
        }
        return isBandera
    }
        @IBAction func changeTextField(_ sender: Any) {
        fasePreguntas.usesInternetOther = otherProviderTextfield.text!
        if otherProviderTextfield.text == "" {
            SwiftEventBus.post("ConfigurarBoton", sender: false)
        } else {
            SwiftEventBus.post("ConfigurarBoton", sender: true)
        }
    }

}
