//
//  NumberOfRooms.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 01/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import SwiftEventBus
import Amplitude

class NumberOfRooms: BaseVentasView {

    // Weak
    @IBOutlet weak var numberRoomsImage: UIImageView!
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var roomsLabel: UILabel!
    @IBOutlet weak var addButton: UIButton!

    override func viewDidLoad() {
        fasePreguntas.Index = 1
        numberLabel.text = String(fasePreguntas.numberOfRooms)
        setViewsAndButtons(value: fasePreguntas.numberOfRooms)
        super.viewDidLoad()
        super.setView(view: view)
        SwiftEventBus.post("NumberOfRooms", sender: numberLabel.text)
    }

    override func viewDidAppear(_ animated: Bool) {
        // Triger to send amplitude event "Show_BestFitFlow_Question2"
        Amplitude.instance().logEvent("Show_BestFitFlow_Question2", withEventProperties: ["Question": "¿Cuántas habitaciones / récamaras tiene su hogar?"])

    }

    override func viewDidDisappear(_ animated: Bool) {
        SwiftEventBus.post("NumberOfRooms", sender: numberLabel.text)
    }

    @IBAction func minusButton(_ sender: AnyObject) {
        fasePreguntas.numberOfRooms -= 1
        setViewsAndButtons(value: fasePreguntas.numberOfRooms)
    }

    @IBAction func addButton(_ sender: AnyObject) {
        fasePreguntas.numberOfRooms += 1
        setViewsAndButtons(value: fasePreguntas.numberOfRooms)
    }

    func setViewsAndButtons(value: Int) {
        if value != 1 {
            roomsLabel
                .text = "Habitaciones"
        } else {
            roomsLabel
                .text = "Habitacion"
        }

        switch value {
        case 1:
                enableAddButton()
                dissableSubstractButton()
        case 2, 3, 4, 5, 6, 7, 8, 9:
                enableSubstractButton()
                enableAddButton()
        case 10:
                roomsLabel.text = "o más Habitaciones"
                enableSubstractButton()
                dissableAddButton()
        default:
                Logger.println("Default")
        }
        SwiftEventBus.post("NumberOfRooms", sender: numberLabel.text)
        numberRoomsImage.image  = UIImage(named: "Image_rede_numberRooms_" + String(value))
        numberLabel.text = String(value)
    }

    func dissableSubstractButton() {
        minusButton.isEnabled = false
        minusButton.setImage(UIImage(named: "Icon_rede_Substract_Disable"), for: .disabled)
        minusButton.tintColor = UIColor.lightGray
    }

    func dissableAddButton() {
        addButton.isEnabled = false
        addButton.setImage(UIImage(named: "Icon_rede_Add_Disable"), for: .disabled)
        addButton.tintColor = UIColor.lightGray
    }

    func enableSubstractButton() {
        minusButton.isEnabled = true
        minusButton.tintColor = UIColor.purple
    }

    func enableAddButton() {
        addButton.isEnabled = true
        addButton.tintColor = UIColor.purple
    }

}
