//
//  NumberOfPeople.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 31/05/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import SwiftEventBus
import Amplitude

class NumberOfPeople: BaseVentasView {

    // Weak
    @IBOutlet weak var numberPeopleImage: UIImageView!
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var peopleLabel: UILabel!
    @IBOutlet weak var addButton: UIButton!

    override func viewDidLoad() {
        fasePreguntas.Index = 0
        numberLabel.text = String(fasePreguntas.numberOfPeople)
        setViewsAndButtons(value: fasePreguntas.numberOfPeople)
        super.viewDidLoad()
        super.setView(view: view)
        SwiftEventBus.post("NumberOfPeople", sender: numberLabel.text)
    }

    override func viewDidAppear(_ animated: Bool) {
        // Triger to send amplitude event "Show_BestFitFlow_Question1"
        Amplitude.instance().logEvent("Show_BestFitFlow_Question1", withEventProperties: ["Question": "¿Cuántas personas se conectan a internet en su hogar?"])
    }

    override func viewDidDisappear(_ animated: Bool) {
        SwiftEventBus.post("NumberOfPeople", sender: numberLabel.text)
    }

    @IBAction func minusButton(_ sender: AnyObject) {
        fasePreguntas.numberOfPeople -= 1
        setViewsAndButtons(value: fasePreguntas.numberOfPeople)
    }

    @IBAction func addButton(_ sender: AnyObject) {
        fasePreguntas.numberOfPeople += 1
        setViewsAndButtons(value: fasePreguntas.numberOfPeople)
    }

    func setViewsAndButtons(value: Int) {
        if value != 1 {
            peopleLabel
                .text = "Personas"
        } else {
            peopleLabel
                .text = "Persona"
        }

        switch value {
        case 1:
                enableAddButton()
                dissableSubstractButton()
        case 2, 3, 4, 5, 6, 7, 8, 9:
                enableSubstractButton()
                enableAddButton()
        case 10:
                peopleLabel.text = "o más Personas"
                enableSubstractButton()
                dissableAddButton()
        default:
                Logger.println("Default")
        }

        SwiftEventBus.post("NumberOfPeople", sender: numberLabel.text)
        numberPeopleImage.image  = UIImage(named: "Image_rede_numberPeople_" + String(value))
        numberLabel.text = String(value)
    }

    func dissableSubstractButton() {
        minusButton.isEnabled = false
        minusButton.setImage(UIImage(named: "Icon_rede_Substract_Disable"), for: .disabled)
        minusButton.tintColor = UIColor.lightGray
    }

    func dissableAddButton() {
        addButton.isEnabled = false
        addButton.setImage(UIImage(named: "Icon_rede_Add_Disable"), for: .disabled)
        addButton.tintColor = UIColor.lightGray
    }

    func enableSubstractButton() {
        minusButton.isEnabled = true
        minusButton.tintColor = UIColor.purple
    }

    func enableAddButton() {
        addButton.isEnabled = true
        addButton.tintColor = UIColor.purple
    }

}
