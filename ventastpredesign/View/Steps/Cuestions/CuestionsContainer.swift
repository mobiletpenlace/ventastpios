//
//  CuestionsContainer.swift
//  ventastpredesign
//
//  Created by Armando Isais Olguin Cabrera on 31/05/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus
import Amplitude

class CuestionsContainer: BaseVentasView {

    @IBOutlet weak var cuestionsContainer: UIView!
    @IBOutlet weak var numberCuestionLabel: UILabel!
    @IBOutlet weak var nextView: UIView!
    @IBOutlet weak var nextButtton: UIButton!
    @IBOutlet weak var textLabel: UILabel!

    var respuestas = Respuestas()
    var numberOfPeople = "0"
    var numberOfRooms = "0"
    var tvProvider = "0"
    var internetProvider = "0"
    var phoneProvider = "0"
    var favoriteApps = "0"
    var internetUses = "0"

    override func viewDidLoad() {
        SwiftEventBus.onMainThread(self, name: "OmitirPregunta") { [weak self] result in
            if let question = result?.object as? Int {
                self?.selectModuleCuestions(index: question)
            }
        }

        SwiftEventBus.onMainThread(self, name: "ConfigurarBoton") { [weak self] result in
            if let bool = result?.object as? Bool {
                self?.configurarBoton(bool: bool)
            }
        }

        SwiftEventBus.onMainThread(self, name: "NumberOfPeople") { [weak self] result in
            self?.respuestas.personasConectadas = (result?.object as? String)!
        }

        SwiftEventBus.onMainThread(self, name: "NumberOfRooms") { [weak self] result in
            self?.respuestas.numHabitaciones = (result?.object as? String)!
        }

        SwiftEventBus.onMainThread(self, name: "TVProvider") { [weak self] result in
            self?.respuestas.proveedorTV = (result?.object as? String)!
        }

        SwiftEventBus.onMainThread(self, name: "InternetProvider") { [weak self] result in
            self?.respuestas.proveedorInternet = (result?.object as? String)!
        }

        SwiftEventBus.onMainThread(self, name: "PhoneProvider") { [weak self] result in
            self?.respuestas.proveedorTelefonia = (result?.object as? String)!
        }

        SwiftEventBus.onMainThread(self, name: "FavoriteApps") { [weak self] result in
            self?.respuestas.appsFavoritas = (result?.object as? String)!
        }

        SwiftEventBus.onMainThread(self, name: "InternetUses") { [weak self] result in
            self?.respuestas.UsosRed = (result?.object as? String)!
        }

        super.viewDidLoad()
        super.setView(view: view)
        selectModuleCuestions(index: 0)
    }

    func configurarBoton(bool: Bool) {
        if bool == true {
            nextView.backgroundColor = UIColor(named: "Base_rede_button_dark")
            nextButtton.isUserInteractionEnabled = true
        }
        if bool == false {
            nextView.backgroundColor = UIColor.gray
            nextButtton.isUserInteractionEnabled = false
        }
    }

    @IBAction func Actionback(_ sender: Any) {

        switch fasePreguntas.Index {
        case 0:
            SwiftEventBus.post("Cambia_paso_container", sender: 2)
        case 1:
            selectModuleCuestions(index: 0)
        case 2:
            selectModuleCuestions(index: 1)
        case 3:
            selectModuleCuestions(index: 2)
        case 4:
            selectModuleCuestions(index: 3)
        case 5:
            selectModuleCuestions(index: 4)
        case 6:
            selectModuleCuestions(index: 5)
        default:
            configurarBoton(bool: true)
            selectModuleCuestions(index: 5)
        }
    }

    func selectModuleCuestions(index: Int) {
        switch index {
        case 0:
            print("entro")
            configurarBoton(bool: true)
            numberCuestionLabel.text = "Pregunta 1 / 7"
            ViewEmbedder.embed(withIdentifier: "NumberOfPeople", parent: self, container: cuestionsContainer)
        case 1:
            configurarBoton(bool: true)
            numberCuestionLabel.text = "Pregunta 2 / 7"
            ViewEmbedder.embed(withIdentifier: "NumberOfRooms", parent: self, container: cuestionsContainer)

        case 2:
            configurarBoton(bool: false)
            numberCuestionLabel.text = "Pregunta 3 / 7"
            ViewEmbedder.embed(withIdentifier: "TVProvider", parent: self, container: cuestionsContainer)

        case 3:
            configurarBoton(bool: false)
            numberCuestionLabel.text = "Pregunta 4 / 7"
            ViewEmbedder.embed(withIdentifier: "InternetProvider", parent: self, container: cuestionsContainer)
        case 4:
            configurarBoton(bool: false)
            numberCuestionLabel.text = "Pregunta 5 / 7"
            ViewEmbedder.embed(withIdentifier: "PhoneProvider", parent: self, container: cuestionsContainer)

        case 5:
            configurarBoton(bool: true)
            numberCuestionLabel.text = "Pregunta 6 / 7"
            ViewEmbedder.embed(withIdentifier: "FavoriteApps", parent: self, container: cuestionsContainer)
            textLabel.text = "Siguiente"

        case 6:
            configurarBoton(bool: false)
            numberCuestionLabel.text = "Pregunta 7 / 7"
            ViewEmbedder.embed(withIdentifier: "InternetUses", parent: self, container: cuestionsContainer)
            textLabel.text = "Ver plan sugerido"
        case 7:
            SwiftEventBus.post("Cambia_paso_container", sender: 5)
        default:
            break
        }
        fasePreguntas
            .Index = index
    }

    @IBAction func nextViewAction(_ sender: Any) {
        switch fasePreguntas.Index {
        case 0:
            Amplitude.instance().logEvent("Question_1_Answered", withEventProperties: ["Question": "¿Cuántas personas se conectan a internet en su hogar?", "Answer": fasePreguntas.numberOfPeople])
            selectModuleCuestions(index: 1)
        case 1:
            selectModuleCuestions(index: 2)
            Amplitude.instance().logEvent("Question_2_Answered", withEventProperties: ["Question": "¿Cuántas habitaciones / récamaras tiene su hogar?", "Answer": fasePreguntas.numberOfRooms])
        case 2:
            selectModuleCuestions(index: 3)
            if fasePreguntas.tvProvider == "OTRO" {
                Amplitude.instance().logEvent("Question_3_Answered", withEventProperties: ["Question": "Selecciona su proveedor actual de TV de paga.", "Answer": "OTRO:" + fasePreguntas.tvProviderOther.uppercased().removeAccents().removeSpaces()])
            } else {
                Amplitude.instance().logEvent("Question_3_Answered", withEventProperties: ["Question": "Selecciona su proveedor actual de TV de paga.", "Answer": fasePreguntas.tvProvider])
            }
        case 3:
            selectModuleCuestions(index: 4)
            if fasePreguntas.internetProvider == "OTRO" {
                Amplitude.instance().logEvent("Question_4_Answered", withEventProperties: ["Question": "Selecciona su proveedor actual de internet.", "Answer": "OTRO:" + fasePreguntas.internetProviderOther.uppercased().removeSpaces().removeAccents()])
            } else {
                Amplitude.instance().logEvent("Question_4_Answered", withEventProperties: ["Question": "Selecciona su proveedor actual de internet.", "Answer": fasePreguntas.internetProvider])
            }
        case 4:
            selectModuleCuestions(index: 5)
            if fasePreguntas.internetProvider == "OTRO" {
                Amplitude.instance().logEvent("Question_5_Answered", withEventProperties: ["Question": "Selecciona su proveedor actual de telefonía.", "Answer": "OTRO:" + fasePreguntas.phoneProviderOther.uppercased().removeSpaces().removeAccents()])
            } else {
            Amplitude.instance().logEvent("Question_5_Answered", withEventProperties: ["Question": "Selecciona su proveedor actual de telefonía.", "Answer": fasePreguntas.phoneProvider])
            }
        case 5:
            selectModuleCuestions(index: 6)
            Amplitude.instance().logEvent("Question_6_Answered", withEventProperties: ["Question": "¿Cuáles son sus apps favoritas?", "Answer": fasePreguntas.favoriteApps])
        case 6:
            selectModuleCuestions(index: 7)
            if fasePreguntas.internetProvider.contains(string: "OTRO") {
                Amplitude.instance().logEvent("Question_7_Answered", withEventProperties: ["Question": "¿Qué otros usos le da al internet?", "Answer": fasePreguntas.usesInternet + ":" + fasePreguntas.usesInternetOther.uppercased().removeSpaces().removeAccents()])
            } else {
                Amplitude.instance().logEvent("Question_7_Answered", withEventProperties: ["Question": "¿Qué otros usos le da al internet?", "Answer": fasePreguntas.usesInternet])
            }
        default:
            break
        }

    }

    @IBAction func closeAction(_ sender: Any) {
        let omitirButton: OmitirButton = UIStoryboard(name: "OmitirButton", bundle: nil).instantiateViewController(withIdentifier: "OmitirButton") as! OmitirButton
        omitirButton.providesPresentationContextTransitionStyle = true
        omitirButton.definesPresentationContext = true
        omitirButton.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        omitirButton.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        UIApplication.topViewController()?.present(omitirButton, animated: false, completion: nil)
    }

    override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
    }

}
