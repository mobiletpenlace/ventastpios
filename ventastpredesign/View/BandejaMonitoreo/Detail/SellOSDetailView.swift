//
//  SellOSDetailView.swift
//  SalesCloud
//
//  Created by Ulises Ortega on 26/10/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

@available(iOS 13.0, *)
class SellOSDetailView: UIViewController {

    deinit {
        print("DETAIL VC DEINIT")
        SwiftEventBus.unregister(self)
        ProgressHUD.dismiss()
    }

    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!

    @IBOutlet weak var contentView: UIView!

//    @IBOutlet weak var centeredView: UIView!

    // ControlDeskSection
    @IBOutlet weak var accountNumberLabel: UILabel!
    @IBOutlet weak var opportunityNumberLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var telNumLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    // ControlDeskSection RejectedOportunitySection
    @IBOutlet weak var rejectedOportunitySectionView: UIView!
    @IBOutlet weak var rejectReasonLabel: UILabel!
    @IBOutlet weak var rejectReasonDetailLabel: UILabel!
    @IBOutlet weak var rejectCommentLabel: UILabel!
    // ControlDeskSection CommentsSection
    @IBOutlet weak var commentsSectionView: UIView!
    @IBOutlet weak var commentsStack: UIStackView!
    @IBOutlet weak var stackAuxView: UIView!
    @IBOutlet weak var commentTextAndButtonView: UIView!
    @IBOutlet weak var commentTextView: UITextView!
    @IBOutlet weak var sendCommentButton: UIButton!
    @IBOutlet weak var showControlDeskCommentsButton: UIButton!

    // InstallStatusSection
    @IBOutlet weak var installStatusSectionView: UIView!
    @IBOutlet weak var osDetailSection: UIView! // Replace this refference for the installStatusSectionView and delete
    @IBOutlet weak var workOrderLabel: UILabel!
    @IBOutlet weak var closingDateLabel: UILabel!
    @IBOutlet weak var confirmationDateLabel: UILabel!
    @IBOutlet weak var confirmedLabel: UILabel!
    @IBOutlet weak var squareLabel: UILabel!
    @IBOutlet weak var districtLabel: UILabel!
    @IBOutlet weak var clusterLabel: UILabel!
    // InstallStatusCommentsSection
    @IBOutlet weak var commentsSection: UIView!
    @IBOutlet weak var commentsLabel: UILabel!
    @IBOutlet weak var showInstallStatusCommentsButton: UIButton!

    // OrderManagementSection
    @IBOutlet weak var changeDocumentsButton: UIButton!
    @IBOutlet weak var changePhoneButton: UIButton!
    @IBOutlet weak var scheduleOSButton: UIButton!
    @IBOutlet weak var detailsButton: UIButton! // TODO: Button hidden, functionality pending
    @IBOutlet weak var chatClientTechButton: UIButton! // TODO: Button hidden, functionality pending

    // SaleModelingSection
    @IBOutlet weak var SaleModelingSection: UIView! // TODO: Complete section hidden, enable when functionality works
    @IBOutlet weak var planChangeButton: UIButton! // TODO: Functionality pending
    @IBOutlet weak var modifyAddonsButton: UIButton! // TODO: Functionality pending

    // ProspectDetailSection
//    @IBOutlet weak var statusLabel: UILabel! //Chech references and delete

    // ProspectRejectSection
//    @IBOutlet weak var prospectRejectSectionView: UIView! //Check and replace references
//    @IBOutlet weak var secondSeparatorView: UIView! //Check references and delete

    // ClientInfo section
//    @IBOutlet weak var clientInfoSectionView: UIView!
//    @IBOutlet weak var thirdSeparatorView: UIView!

    // Comments Section

    let textViewPlaceHolder = "Agregar un comentario"

    var prospect: Oportunidad?
    var OSProspect: OportunidadOSResponse?

    let viewModel = ProspectDetailViewModel()
    let buttonSend = UIButton()

    var previousComments: [Comentarios] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        commentTextView.delegate = self
        configUI()
        fillProspectDetail()
        configOSProspectDetail()
        getCommentsDetails()
        ProgressHUD.show()

        SwiftEventBus.onMainThread(self, name: "ProspectCommentsLoaded", handler: { [weak self] result in
            guard let commentResponse = result?.object as? MotivosOpp else {return}
            if commentResponse.motivoRechazoMC == nil && commentResponse.detalleMotivoRechazo == nil && commentResponse.comentariosMC == nil {
                self?.rejectedOportunitySectionView.isHidden = true
//                self?.secondSeparatorView.isHidden = true
            }
            self?.rejectReasonLabel.text = commentResponse.motivoRechazoMC
            self?.rejectReasonDetailLabel.text = commentResponse.detalleMotivoRechazo
            self?.rejectCommentLabel.text = commentResponse.comentariosMC
            let motivoRechazo = commentResponse.motivoRechazoMC?.lowercased()
            if motivoRechazo == "pom ilocalizable" || motivoRechazo == "cliente ilocalizable" {
                self?.changeDocumentsButton.isHidden = true
            }
            ProgressHUD.dismiss()
        })

        SwiftEventBus.onMainThread(self, name: "FillCommentsSection", handler: { [weak self] result in
            guard let comments = result?.object as? [Comentarios] else {return}
            print("Entro al fill comments")
            self?.fillCommentSection(comments: comments)
        })

        SwiftEventBus.onMainThread(self, name: "UpdateCommentConstraint", handler: { [weak self] result in
            guard let commentView = result?.object as? ChatBubbleView else {return}
            self?.updateCommentConstraint(commentView: commentView)
        })

        SwiftEventBus.onMainThread(self, name: "UpdateComments", handler: {[weak self] _ in
            self?.getCommentsDetails()
        })

        SwiftEventBus.onMainThread(self, name: "SuccessDocumentsChanged", handler: { [weak self] _ in
            ProgressHUD.showSuccess("Documentos actualizados")
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5, execute: {
                if let motive = self?.rejectReasonLabel.text {
                    if motive.lowercased().contains(string: "documento") {
                        self?.saveSell(motivoRescate: "Actualización de documentos")
                    }
                }
            })
        })

        SwiftEventBus.onMainThread(self, name: "SuccessNumberChanged", handler: { [weak self] _ in
            ProgressHUD.showSuccess("Número actualizado")
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5, execute: {
                if let motive = self?.rejectReasonLabel.text {
                    if motive.lowercased().contains(string: "ilocalizable") {
                        self?.saveSell(motivoRescate: "Actualización de número telefónico")
                    }
                }
            })
        })

        SwiftEventBus.onMainThread(self, name: "SuccessRescheduled", handler: { [weak self] _ in
            ProgressHUD.showSuccess("Reagendado correctamente")
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5, execute: {
                SwiftEventBus.post("UpdateOS")
                self?.dismiss(animated: false)
            })
        })
    }

    @available(iOS 14, *)
    @IBAction func changeDocumentsButtonAction(_ sender: Any) {
        let vc = ChangeDocumentsView(idObject: prospect?.id ?? "", BRMAcc: prospect?.numeroCuenta ?? "")
        vc.modalPresentationStyle = .overFullScreen
        present(vc, animated: false, completion: nil)
    }

    @IBAction func changePhoneButtonAction(_ sender: Any) {
        let vc = ChangeNumberView(idObject: prospect?.id ?? "")
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: false, completion: nil)
    }

    @available(iOS 13.0, *)
    @IBAction func scheduleButtonAction(_ sender: Any) {
        let vc = ScheduleOSView()
        vc.oportunidadOS = OSProspect
        vc.modalPresentationStyle = .overFullScreen
        present(vc, animated: false)
    }

    @IBAction func showControlDeskCommentsButtonAction(_ sender: Any) {
        commentsSectionView.isHidden.toggle()
        let buttonTitle = commentsSectionView.isHidden ? "Mostrar comentarios":"Ocultar comentarios"
        showControlDeskCommentsButton.setTitle(buttonTitle, for: .normal)
    }

    @IBAction func showInstallStatusCommentsButtonAction(_ sender: Any) {
        commentsSection.isHidden.toggle()
        let buttonTitle = commentsSection.isHidden ? "Mostrar comentarios":"Ocultar comentarios"
        showInstallStatusCommentsButton.setTitle(buttonTitle, for: .normal)
    }

    @IBAction func closeButtonAction(_ sender: Any) {
        dismiss(animated: false)
    }

    @IBAction func sendCommentButtonAction(_ sender: UIButton) {
        print("ButtonClicked")
        if !commentTextView.text.isEmpty {
            sendNewComment(comment: commentTextView.text)
        }
        commentTextView.text = textViewPlaceHolder
        commentTextView.textColor = .lightGray
        sender.isEnabled = false
        commentTextView.resignFirstResponder()
    }

    func sendNewComment(comment: String) {
        viewModel.sendNewComment(idObject: prospect?.id ?? "", comment: comment)
    }

    func saveSell(motivoRescate: String) {
        ProgressHUD.show()
        viewModel.saveSell(idOpp: prospect?.id ?? "", motive: motivoRescate) { [weak self] in
            SwiftEventBus.post("UpdateProspects")
            ProgressHUD.dismiss()
            self?.dismiss(animated: true)
        }
    }

    func getCommentsDetails() {
        viewModel.getCommentsDetails(idRegistro: prospect?.id ?? "")
    }

    func fillProspectDetail() {
        guard let prospect = prospect else {
            return
        }
        accountNumberLabel.text = prospect.numeroCuenta
        nameLabel.text = prospect.nombre
        dateLabel.text = String(prospect.fecha.split(separator: "T")[0])
        // Hide num
        if prospect.celular.count > 4 {
            let string = prospect.celular
            let hiddenNum = string.replacingCharacters(in: string.startIndex..<string.index(string.endIndex, offsetBy: -4), with: "******")
            telNumLabel.text = hiddenNum
        }
        opportunityNumberLabel.text = prospect.numeroOpp
//        statusLabel.text = prospect.etapa
    }

    func configUI() {
        switch prospect?.etapa {
        case "Perdida":
            print("Show everything")
        case "Crédito":
            changeDocumentsButton.isHidden = true
        case "Ganada":
            commentTextAndButtonView.isHidden = true
//            clientInfoSectionView.isHidden = true
//            thirdSeparatorView.isHidden = true
        default:
            return
        }
        stackAuxView.isHidden = true
        commentTextView.textColor = .lightGray
        sendCommentButton.setTitle("", for: .normal)
        sendCommentButton.isEnabled = false
        closeButton.setTitle("", for: .normal)
        self.view.backgroundColor = .black.withAlphaComponent(0.8)
    }

    func configOSProspectDetail() {
        guard let OSProspect = OSProspect else {
            installStatusSectionView.isHidden = true
            scheduleOSButton.isHidden = true
            return
        }

        // Fill OS Detail Section
//        accNumLabel.text = OSProspect.OS[0].cuenta
        nameLabel.text = OSProspect.OS[0].nombre
        workOrderLabel.text = OSProspect.OS[0].idOT
        if let closingDate = OSProspect.fechacierre {
            closingDateLabel.text = String(closingDate.split(separator: "T")[0])
        }
        if let confirmationDate = OSProspect.fechacierre {
            confirmationDateLabel.text = String(confirmationDate.split(separator: "T")[0])
        }
//        confirmationDateLabel.text = OSProspect.OS[0].fechaconfirmada
        confirmedLabel.text = OSProspect.OS[0].confirmada ?? false ? "Sí" : "No"
        squareLabel.text = OSProspect.OS[0].plaza
        districtLabel.text = OSProspect.OS[0].distrito
        clusterLabel.text = OSProspect.OS[0].cluster
        // Fill Comments Section
        commentsLabel.text = OSProspect.OS[0].comentarios
        // Enable or disable schedule button
        scheduleOSButton.isEnabled = OSProspect.OS[0].estatus == "Rescate"

    }

}

// MARK: Comments section and textViewhandling
@available(iOS 13.0, *)
extension SellOSDetailView: UITextViewDelegate {

    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == .lightGray && textView.text == textViewPlaceHolder {
            textView.text = nil
            textView.textColor = .black
        }
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        if let nonBlankText = textView.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) {
            if nonBlankText.count < 1 {
                textView.textColor = .lightGray
                textView.text = textViewPlaceHolder
            }
        }
        textView.setContentOffset(.zero, animated: true)
    }

    func textViewDidChange(_ textView: UITextView) {
        if let nonBlankText = textView.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) {
            if nonBlankText.count < 1 {
                sendCommentButton.isEnabled = false
            } else {
                sendCommentButton.isEnabled = true
            }
        }
    }

    func fillCommentSection(comments: [Comentarios]) {
        print("ENTRO A LA FUNCIN PROSPE")
        let commentsReversed = comments.reversed()
        for comment in commentsReversed {
            if !previousComments.contains(where: {$0.Id == comment.Id}) {
                let commentView = ChatBubbleView()
                commentView.configure(with: Comment(title: comment.creadoPor,
                                                    comment: comment.texto,
                                                    date: comment.fechaCreacion,
                                                    id: comment.Id))
                commentView.heightAnchor.constraint(equalToConstant: 90).isActive = true
                commentsStack.addArrangedSubview(commentView)
            } else {
                print("COMMENT ALREADY EXISTS")
            }
        }
        previousComments = comments
    }

    func updateCommentConstraint(commentView: ChatBubbleView) {
        print("Entro al update constraints")
        if let constraint = commentView.constraints.last {
            constraint.isActive = false
            commentView.heightAnchor.constraint(equalToConstant: CGFloat(commentView.heightVar + 90)).isActive = true
        } else {
            commentView.heightAnchor.constraint(equalToConstant: CGFloat(commentView.heightVar + 90)).isActive = true
        }
    }
}
