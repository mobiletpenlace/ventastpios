//
//  MonitoringTrayTableViewCell.swift
//  SalesCloud
//
//  Created by Ulises Ortega on 25/09/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit

class MonitoringTrayTableViewCell: UITableViewCell {

    @IBOutlet weak var installStatusButton: UIButton!
    @IBOutlet weak var saleStatusButton: UIButton!

    var oportunidad: Oportunidad?

    var osSellTuple: (Oportunidad, OportunidadOSResponse?)?

    @IBOutlet weak var accNumberLabel: UILabel!
    @IBOutlet weak var oportunityLabel: UILabel!
    @IBOutlet weak var osLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configureCell() {
        accNumberLabel.text = osSellTuple?.0.numeroCuenta
        nameLabel.text = osSellTuple?.0.nombre
        oportunityLabel.text = osSellTuple?.0.numeroOpp
        saleStatusButton.setTitle(osSellTuple?.0.etapa, for: .normal)

        installStatusButton.setTitle(osSellTuple?.1 == nil ? "Sin estatus" : osSellTuple?.1?.OS[0].estatus, for: .normal)
        osLabel.text = osSellTuple?.1 == nil ? "Sin información" : osSellTuple?.1?.OS[0].numOS

//        accNumberLabel.text = oportunidad?.numeroCuenta
//        nameLabel.text = oportunidad?.nombre
//        oportunityLabel.text = oportunidad?.numeroOpp
//        saleStatusButton.setTitle(oportunidad?.etapa, for: .normal)
//        installStatusButton.setTitle("Sin estatus", for: .normal)

        if #available(iOS 15.0, *) {
            var config = UIButton.Configuration.filled()
            config.titleTextAttributesTransformer = UIConfigurationTextAttributesTransformer { incoming in
                var outgoing = incoming
                outgoing.font = UIFont.systemFont(ofSize: 12, weight: .bold)
                return outgoing
            }
            saleStatusButton.configuration = config
            installStatusButton.configuration = config
        }

        switch osSellTuple?.0.etapa {
        case "Perdida":
            saleStatusButton.tintColor = .systemRed
        case "Crédito":
            saleStatusButton.tintColor = .systemOrange
        case "Ganada":
            saleStatusButton.tintColor = .systemGreen
        case "Factibilidad":
            saleStatusButton.tintColor = .systemPurple
        default:
            saleStatusButton.tintColor = .systemGray
            return
        }

        switch osSellTuple?.1?.OS[0].estatus {
        case "Rescate":
            installStatusButton.tintColor = .systemRed
        case "Agendado":
            installStatusButton.tintColor = .systemBlue
        case "Confirmado":
            installStatusButton.tintColor = .systemBlue
        default:
            installStatusButton.tintColor = .systemGray
            return
        }
    }

}
