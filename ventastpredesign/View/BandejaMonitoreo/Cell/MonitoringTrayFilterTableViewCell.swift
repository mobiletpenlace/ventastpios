//
//  MonitoringTrayFilterTableViewCell.swift
//  SalesCloud
//
//  Created by Ulises Ortega on 26/09/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class MonitoringTrayFilterTableViewCell: UITableViewCell {
    @IBOutlet weak var salesCounterLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var controlTableStatusButton: UIButton!
    @IBOutlet weak var controlTableStatusButtonMenu: UIMenu!
    @IBOutlet weak var installStatusButtonMenu: UIMenu!
    @IBOutlet weak var installStatusButton: UIButton!
    @IBOutlet weak var startDatePicker: UIDatePicker!
    @IBOutlet weak var endDatePicker: UIDatePicker!

    private var installStatusArray: [String] = []

    override func awakeFromNib() {
        super.awakeFromNib()
        guard #available(iOS 14, *) else { return }
        configDatePickers()

        SwiftEventBus.onMainThread(self, name: "installStatusArrayLoaded", handler: {[weak self] result in
            print("ENTRO AL INSTALLSTATUSARRAY")
            guard let installStatusArrayResponse = result?.object as? [String] else {return}
            self?.installStatusArray = installStatusArrayResponse
            self?.configFilters()
        })

        SwiftEventBus.onMainThread(self, name: "EmployeeHeaderNameUpdate", handler: {[weak self] result in
            print("ENTRO AL EmployeeNameUpdate")
            guard let nameCounterTuple = result?.object as? (name: String, counter: Int ) else {return}
            self?.nameLabel.text = nameCounterTuple.name
            self?.salesCounterLabel.text = String(nameCounterTuple.counter)
        })

        SwiftEventBus.onMainThread(self, name: "SetInstallAndTableStatusFiltersToDefault", handler: {[weak self] _ in
            self?.configFilters()
        })
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @available(iOS 14, *)
    private func configControlTableStatusFilter() {

        let buttonClosure = { (action: UIAction) in
            SwiftEventBus.post("UpdateControlTableStatusFilter", sender: action.title)
        }

        // TODO: Check if this should be filled dinamically in the future

        controlTableStatusButton.menu = UIMenu(children: [
            UIAction(title: "Todas", handler: buttonClosure),
            UIAction(title: "Ganada", handler: buttonClosure),
            UIAction(title: "Perdida", handler: buttonClosure),
            UIAction(title: "Crédito", handler: buttonClosure),
            UIAction(title: "Factibilidad", handler: buttonClosure)
        ])
        controlTableStatusButton.showsMenuAsPrimaryAction = true
    }

    @available(iOS 14, *)
    private func configInstallStatusFilter() {

        let buttonClosure = { (action: UIAction) in
            SwiftEventBus.post("UpdateInstallStatusFilter", sender: action.title)
        }

        var actionsArray: [UIMenuElement] = [UIAction(title: "Todas", handler: buttonClosure)]
        for installStatus in installStatusArray {
            actionsArray.append(UIAction(title: installStatus, handler: buttonClosure))
        }
        installStatusButton.menu = UIMenu(children: actionsArray)
        installStatusButton.showsMenuAsPrimaryAction = true
    }

    @available(iOS 14, *)
    private func configFilters() {
        configControlTableStatusFilter()
        configInstallStatusFilter()
    }

    private func configDatePickers() {
        // TODO: Configure date pickers with mexico region and language to show calendar in spanish
        // Setting maximum date to current date
        startDatePicker.maximumDate = Date()
        endDatePicker.maximumDate = Date()

        // Set default value of start date picker to 1 months ago
        // TODO: If this date will change, make sure to also change it in the request from MonitoringTrayViewModel
        // TODO: Update logic to take this date as default in the request
        guard let startDate = Calendar.current.date(byAdding: .month, value: -1, to: Date()) else { return }
        startDatePicker.setDate(startDate, animated: true)
//        startDatePicker.date = startDate
        print("FechaTest = \(startDatePicker.date)")

        // TODO: Minumum date set still pending
    }

    @objc func controlTableStatusChanged() {
        print("Estatus mesa de control \(controlTableStatusButton.titleLabel?.text)")
    }

    @objc func installStatusChanged() {
        print("Estatus instalacion \(installStatusButton.titleLabel?.text)")
    }

    @IBAction func startDateDidChange(_ sender: UIDatePicker) {
        print("Start date changed \(sender.date)")
        SwiftEventBus.post("SetStartAndEndDate", sender: (startDatePicker.date,endDatePicker.date))
    }

    @IBAction func endDateDidChange(_ sender: UIDatePicker) {
        print("End date changed \(sender.date)")
        SwiftEventBus.post("SetStartAndEndDate", sender: (startDatePicker.date,endDatePicker.date))
    }


}
