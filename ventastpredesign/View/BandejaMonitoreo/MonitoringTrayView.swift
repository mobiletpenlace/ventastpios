//
//  MonitoringTrayView.swift
//  SalesCloud
//
//  Created by Ulises Ortega on 25/09/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class MonitoringTrayView: UIViewController {
    private let viewModel = MonitoringTrayViewModel()
    private var prospectsTotal: [Oportunidad] = []
//    var prospectsFiltered : [Oportunidad] = []
    private var prospectsOSTotal: [OportunidadOSResponse] = []
//    var prospectsOSFiltered : [OportunidadOSResponse] = []
    private let searchController = UISearchController()

    private var osSellTupleArray: [(Oportunidad, OportunidadOSResponse?)] = []
    private var osSellTupleArrayFiltered: [(Oportunidad, OportunidadOSResponse?)] = []

    private var controlTableStatusFilter = "Todas"
    private var installStatusFilter = "Todas"
    
    private var startDate: Date? = nil
    private var endDate: Date? = nil

//    private var employee : Empleado
    private var employeeID: String
    private var employeeName: String

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noContentLabel: UILabel!

    deinit {
        print("DEINIT MonitoringTray VC")
        SwiftEventBus.unregister(self)
        ProgressHUD.dismiss()
    }

    init(employeeID: String, employeeName: String) {
        self.employeeID = employeeID
        self.employeeName = employeeName
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        ProgressHUD.show(interaction: false)
        configTableView()
        configNavigationView()
        loadProspects(startDate: startDate, endDate: endDate)
        view.isUserInteractionEnabled = false

        SwiftEventBus.onMainThread(self, name: "ProspectsLoaded", handler: {[weak self] result in
            guard let prospects = result?.object as? [Oportunidad] else {return}
            self?.prospectsTotal = prospects
            self?.noContentLabel.isHidden = !prospects.isEmpty
            // Update header data
            SwiftEventBus.post("EmployeeHeaderNameUpdate", sender: (name: self?.employeeName, counter: self?.prospectsTotal.count))
//            self?.prospectsFiltered = prospects
//            ProgressHUD.dismiss()
        })

        SwiftEventBus.onMainThread(self, name: "osProspectsLoaded", handler: {[weak self] result in
            guard let self = self else {return}
            guard let prospects = result?.object as? [OportunidadOSResponse] else {return}
//            if prospects.count == 0 {
//                print("Viene en 0 la lista de instalaciones")
//                SwiftEventBus.post("installStatusArrayLoaded", sender: [])
//            } else {
                self.prospectsOSTotal = prospects
                var installStatusArray: [String] = []
                for osProspect in self.prospectsOSTotal {
                    if !installStatusArray.contains(osProspect.OS[0].estatus ?? "") {
                        installStatusArray.append(osProspect.OS[0].estatus ?? "")
                    }
                }
                SwiftEventBus.post("installStatusArrayLoaded", sender: installStatusArray)
//                self.prospectsOSFiltered = prospects

                // DELETE
                print("ProspectsTotal = \(self.prospectsTotal.count)")
//                print("ProspectsFiltered = \(self.prospectsFiltered.count)")
                print("ProspectsOSTotal = \(self.prospectsOSTotal.count)")
//                print("ProspectsOSFiltered = \(self.prospectsOSFiltered.count)")

                self.osSellTupleArray = self.viewModel.fillTupleArray(sellProspects: self.prospectsTotal,
                                                                        osProspects: self.prospectsOSTotal)
            self.osSellTupleArrayFiltered = self.osSellTupleArray
                self.tableView.reloadData()
                print("ProspectsTuple = \(self.osSellTupleArray.count)")
                print(self.osSellTupleArray)

//            }

            // TODO: Check commented code
//            //Get scopes array
//            for prospect in prospects{
//                if !(self?.scopes.contains(where: { $0 == prospect.OS[0].estatus}) ?? true) {
//                    if let scope = prospect.OS[0].estatus{
//                        self?.scopes.append(scope)
//                    }
//                }
//            }
//            self?.tableView.reloadData()
//            self?.navigationItem.searchController?.searchBar.scopeButtonTitles = self?.scopes
//            self?.navigationItem.searchController?.searchBar.showsScopeBar = true

            self.view.isUserInteractionEnabled = true
            ProgressHUD.dismiss()
        })

        SwiftEventBus.onMainThread(self, name: "ProspectsTuples_Filtered", handler: { [weak self] result in
            guard let prospects = result?.object as? [(Oportunidad, OportunidadOSResponse?)] else {return}
            if prospects.count < 1 {
                self?.noContentLabel.isHidden = false
            } else {
                self?.noContentLabel.isHidden = true
            }
            self?.osSellTupleArrayFiltered = prospects
            self?.tableView.reloadData()
        })

        SwiftEventBus.onMainThread(self, name: "UpdateControlTableStatusFilter", handler: { [weak self] result in
            guard let status = result?.object as? String else {return}
            guard let self = self else {return}
            self.controlTableStatusFilter = status
            // TODO: TEST if else with "" as text is needed
            if let text = self.searchController.searchBar.text {
                self.filterProspects(string: text, prospects: self.osSellTupleArray, selectedControlTableStatus: self.controlTableStatusFilter, selectedInstallStatus: self.installStatusFilter)
            }
        })

        SwiftEventBus.onMainThread(self, name: "UpdateInstallStatusFilter", handler: { [weak self] result in
            guard let status = result?.object as? String else {return}
            guard let self = self else {return}
            self.installStatusFilter = status
            // TODO: TEST if else with "" as text is needed
            if let text = self.searchController.searchBar.text {
                self.filterProspects(string: text, prospects: self.osSellTupleArray, selectedControlTableStatus: self.controlTableStatusFilter, selectedInstallStatus: self.installStatusFilter)
            }
        })

        SwiftEventBus.onMainThread(self, name: "UpdateProspects", handler: { [weak self] _ in
            ProgressHUD.showSuccess("Documentos actualizados")
            self?.loadProspects(startDate: self?.startDate, endDate: self?.endDate)
            self?.searchController.searchBar.text = nil
            self?.searchController.searchBar.resignFirstResponder()
        })
        
        SwiftEventBus.onMainThread(self, name: "SetStartAndEndDate", handler: { [weak self] result in
            guard let dates = result?.object as? (Date,Date) else {return}
            self?.startDate = dates.0
            self?.endDate = dates.1
            
            // Refresh prospects and filters
            ProgressHUD.show()
            self?.loadProspects(startDate: self?.startDate, endDate: self?.endDate)
            self?.searchController.searchBar.text = nil
            self?.searchController.searchBar.resignFirstResponder()
            SwiftEventBus.post("SetInstallAndTableStatusFiltersToDefault")
            self?.controlTableStatusFilter = "Todas"
            self?.installStatusFilter = "Todas"
            
        })
    }

    func configTableView() {
        // TODO: Fix this with the new celltype
        tableView.register(UINib(nibName: "MonitoringTrayTableViewCell", bundle: nil), forCellReuseIdentifier: "monitoringCell")

        // TODO: Find alternative when lower than 14.0
        if #available(iOS 14, *) {
            let view = (Bundle.main.loadNibNamed("MonitoringTrayFilterTableViewCell", owner: self, options: nil)![0] as! MonitoringTrayFilterTableViewCell)
            tableView.tableHeaderView = view
        } else {
            // Header view (Filter should'nt show)
        }
        tableView.delegate = self
        tableView.dataSource = self
    }

    func configNavigationView() {
        self.navigationController?.navigationBar.prefersLargeTitles = true
        // TODO: Change title according to displayed view
        self.title = "Monitoreo de Ventas"
        self.navigationItem.searchController = searchController
        self.navigationItem.backButtonTitle = ""
        self.navigationItem.searchController?.searchBar.delegate = self
        self.navigationItem.searchController?.searchBar.setValue("Cancelar", forKey: "cancelButtonText")
//        self.navigationItem.searchController?.searchBar.showsScopeBar = false
        self.navigationItem.hidesSearchBarWhenScrolling = false
        self.navigationItem.searchController?.searchBar.placeholder = "Buscar por nombre o cuenta"
        self.navigationController?.navigationBar.topItem?.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "Back_Arrow_Black"), style: .plain, target: self, action: #selector(backAction))
        self.navigationController?.navigationBar.topItem?.leftBarButtonItem?.tintColor = .black
    }

    @objc func backAction() {
        print("BackPressed")
        self.navigationController?.dismiss(animated: false)
        self.navigationController?.viewControllers.removeAll()
    }

    func loadProspects(startDate: Date?, endDate: Date?) {
        viewModel.loadProspects(employeeID: employeeID, startDate: startDate, endDate: endDate)
    }

    func filterProspects(string: String, prospects: [(Oportunidad, OportunidadOSResponse?)], selectedControlTableStatus: String, selectedInstallStatus: String, startDate: String? = nil, endDate: String? = nil) {
        viewModel.filterProspects(string: string, prospects: prospects, selectedControlTableStatus: selectedControlTableStatus, selectedInstallStatus: selectedInstallStatus, startDate: startDate ?? "", endDate: endDate ?? "")
    }
}

@available(iOS 13.0, *)
// TODO: Fix this
extension MonitoringTrayView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "monitoringCell", for: indexPath) as! MonitoringTrayTableViewCell
//        cell.oportunidad = prospectsFiltered[indexPath.section]
        cell.osSellTuple = osSellTupleArrayFiltered[indexPath.section]
        cell.configureCell()
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        // TODO: this is only test fix with returning array count
        return osSellTupleArrayFiltered.count
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // TODO: Open new detail view in this section
//        let selectedProspect = osSellTupleArrayFiltered[indexPath.section]
//        let vc = OSDetailView()
//        vc.OSProspect = selectedProspect.1
//        vc.modalPresentationStyle = .overFullScreen
//        present(vc, animated: false)
        let selectedProspect = osSellTupleArrayFiltered[indexPath.section]
        let vc = SellOSDetailView()
        vc.prospect = selectedProspect.0
        vc.OSProspect = selectedProspect.1
//      vc.fillProspectDetail()
        vc.modalPresentationStyle = .overFullScreen
        present(vc, animated: false)
    }
}

@available(iOS 13.0, *)
extension MonitoringTrayView: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let text = searchBar.text else {return}
        filterProspects(string: text, prospects: osSellTupleArray, selectedControlTableStatus: controlTableStatusFilter, selectedInstallStatus: installStatusFilter)
    }

    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        guard let text = searchBar.text else {return}
        filterProspects(string: text, prospects: osSellTupleArray, selectedControlTableStatus: controlTableStatusFilter, selectedInstallStatus: installStatusFilter)
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        filterProspects(string: "", prospects: osSellTupleArray, selectedControlTableStatus: controlTableStatusFilter, selectedInstallStatus: installStatusFilter)
        tableView.reloadData()
    }
}
