//
//  MonitoringTrayViewModel.swift
//  SalesCloud
//
//  Created by Ulises Ortega on 25/09/23.
//  Copyright © 2023 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import Alamofire
import SwiftyJSON
import SwiftEventBus

class MonitoringTrayViewModel {
    let model = MonitoringTrayModel()

    // TODO: Improve filter if user should be able to search using other attributes
    func filterProspects(string: String, prospects: [(Oportunidad, OportunidadOSResponse?)], selectedControlTableStatus: String, selectedInstallStatus: String, startDate: String? = nil, endDate: String? = nil) {
        let text = string.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        var newProspects: [(Oportunidad, OportunidadOSResponse?)] = []
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"

        let calendar = Calendar.current
        let startDateNormalized = startDate.flatMap { dateFormatter.date(from: $0) }.map { calendar.startOfDay(for: $0) }
        let endDateNormalized = endDate.flatMap { dateFormatter.date(from: $0) }.map { calendar.startOfDay(for: $0) }

        func isDateInRange(_ dateString: String?) -> Bool {
            guard let dateString = dateString, let date = dateFormatter.date(from: dateString) else { return false }
            let normalizedDate = calendar.startOfDay(for: date)

            if let start = startDateNormalized, normalizedDate < start { return false }
            if let end = endDateNormalized, normalizedDate > end { return false }

            return true
        }

        newProspects = prospects.filter { (oportunidad, osResponse) in
            let matchesText: Bool = {
                if text.isNumeric {
                    return oportunidad.numeroCuenta.contains(text)
                } else if text.isEmpty {
                    return true
                } else {
                    return oportunidad.nombre.lowercased().contains(text.lowercased())
                }
            }()

            let matchesStatus = (selectedControlTableStatus == "Todas" || oportunidad.etapa.lowercased() == selectedControlTableStatus.lowercased()) &&
                                (selectedInstallStatus == "Todas" || osResponse?.OS.first?.estatus?.lowercased() == selectedInstallStatus.lowercased())

            let matchesDate = isDateInRange(oportunidad.fecha)

            return matchesText && matchesStatus && matchesDate
        }

        SwiftEventBus.post("ProspectsTuples_Filtered", sender: newProspects)
    }

    func loadProspects(employeeID: String, startDate: Date?, endDate: Date? ) {
        let id = employeeID

        let format: DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            return formatter
        }()
        
        var startDateString = ""
        var endDateString = ""
        if let startDate = startDate, let endDate = endDate {
            startDateString = format.string(from: startDate)
            endDateString = format.string(from: endDate)
        } else {
            // Default startDate set to 1 month before
            // TODO: Improve this code and get this value from the datePicker also for default
            guard let startDate = Calendar.current.date(byAdding: .month, value: -1, to: Date()) else { return }
            let endDate = Date()
            startDateString = format.string(from: startDate)
            endDateString = format.string(from: endDate)
        }
        
        print("START DATE = \(startDateString) ENDATE = \(endDateString)")
        let parameters: Parameters = [
            "idEmpleado": employeeID,
            "fechafin": endDateString,
            "fechainicio": startDateString
        ]
        if ConnectionUtils.isConnectedToNetwork() {
            // Connection is active
            Alamofire.request(AppDelegate.API_SALESFORCE + ApiDefinition.API_CONSULTA_OPORTUNIDADES, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: ApiDefinition.headersToken).responseJSON {[weak self] response in
                guard let self = self else {return}
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    let oportunidadResponse = OportunidadResponse(JSONString: json.rawString() ?? "")
                    guard let oportunidades = oportunidadResponse?.oportunidades else {return}
                    let oportunidadesJSON: String = AES128.shared.decode(coded: oportunidades) ?? ""
                    print("prospects")
                    print(oportunidadesJSON)
                    guard let oportunidadesObjects: [Oportunidad] = [Oportunidad].init(JSONString: oportunidadesJSON) else {return}
                    SwiftEventBus.post("ProspectsLoaded", sender: oportunidadesObjects)
                    // Load OSProspects
                    self.loadOSProspects(employeeID: id)
                case .failure(let error):
                    print(error)
                }
            }
        } else {
            // Conection error
        }
    }

    func loadOSProspects(employeeID: String) {
            let id = employeeID
            let parameters: Parameters = [
                "accion": "ConsultarOpo",
                "idEmpleado": id
            ]
            if ConnectionUtils.isConnectedToNetwork() {
                // Connection is active
                Alamofire.request(AppDelegate.API_SALESFORCE + ApiDefinition.API_CONSULTA_OS, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: ApiDefinition.headersToken).responseJSON {[weak self] response in
                    guard let self = self else {return}
                    switch response.result {
                    case .success(let value):
                        let json = JSON(value)
                        print(json)
                        guard let oportunidades = OportunidadOSListResponse(JSONString: json.rawString() ?? "") else {return}
                        SwiftEventBus.post("osProspectsLoaded", sender: oportunidades.listainfoOS)
                    case .failure(let error):
                        print(error)
                    }
                }
            } else {
                // Conection error
            }
    }

    func fillTupleArray(sellProspects: [Oportunidad], osProspects: [OportunidadOSResponse]) -> [(Oportunidad, OportunidadOSResponse?)] {
        var tupleArray: [(Oportunidad, OportunidadOSResponse?)] = []
        var saved = false
        for sell in sellProspects {
            saved = false
            for os in osProspects {
                if sell.numeroCuenta == os.OS[0].cuenta {
                    tupleArray.append((sell, os))
                    saved = true
                }
            }
            if saved == false {
                tupleArray.append((sell, nil))
            }
        }
        return tupleArray
    }
}
