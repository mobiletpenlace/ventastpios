//
//  HumanResourcesViewController.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 9/9/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import UIKit
import Amplitude

class HumanResourcesViewController: BaseVentasView {

    deinit {
        print("DEINIT HUMANRESOURCESVC")
    }

    @IBOutlet weak var GSTalentContainer: UIView!
    @IBOutlet weak var SMBCardContainer: UIView!
    @IBOutlet weak var BienestarContainer: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: self.view)
        design()
    }

    override func viewDidAppear(_ animated: Bool) {
        Amplitude.instance().logEvent("Show_HRScreen")
    }

    @IBAction func goTalentoGS(_ sender: Any) {
        if let url = URL(string: "https://auth.socio.gs/nidp/saml2/idpsend?id=authsociogs") {
            UIApplication.shared.open(url, options: [:])
        }
    }

    @IBAction func goCardSMB(_ sender: Any) {
        if let url = URL(string: "https://tarjetasmb.com.mx/inicio") {
            UIApplication.shared.open(url, options: [:])
        }
    }

    @IBAction func goBienestar(_ sender: Any) {
        if let url = URL(string: "https://portalsocio.gs/totalplay") {
            UIApplication.shared.open(url, options: [:])
        }
    }

    @IBAction func goBack(_ sender: Any) {
        Constants.Back(viewC: self)
    }

    func design() {
        Constants.Border(view: GSTalentContainer, radius: 6, width: 1)
        Constants.Border(view: SMBCardContainer, radius: 6, width: 1)
        Constants.Border(view: BienestarContainer, radius: 6, width: 1)
        Constants.Gradient(view: self.GSTalentContainer, frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.size.width - 40, height: self.GSTalentContainer.frame.size.height), colors: [Colors.White, Colors.TransWhite, Colors.LowGray])
        Constants.Gradient(view: self.SMBCardContainer, frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.size.width - 40, height: self.SMBCardContainer.frame.size.height), colors: [Colors.White, Colors.TransWhite, Colors.LowGray])
        Constants.Gradient(view: self.BienestarContainer, frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.size.width - 40, height: self.SMBCardContainer.frame.size.height), colors: [Colors.White, Colors.TransWhite, Colors.LowGray])

    }
}
