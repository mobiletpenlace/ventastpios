//
//  StringDialogs.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 04/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

struct type {
    static let Normal = "normal"
    static let Error = "error"
    static let Success = "success"
    static let Warning = "warning"
    static let Info = "info"
}

struct family {
    static let doble = 0
    static let dobleMatch = 1
    static let triple = 2
    static let tripleMatch = 3
    static let dobleUnbox = 4
    static let tripleUnbox = 5
    static let doblePrepago = 6
    static let triplePrepago = 7
}

struct fase {
    static var bug = false
    static let ValidaCov = 0
    static let Mapa = 1
//    //NUEVAS
    static let Home = 2
    static let BestFit = 3
//    static let BestFit = 3
    // ANTIGUAS
    static let Deal = 2
    static let FamilyResidencial = 3
    static let FamilyPrepago = 12
    static let FamilyNegocio = 4
    static let PackageSelection = 5
    static let NewPackageSelection = 13
    static let AddonConfig = 6
    static let InfoPersonal = 7
    static let InfoPersonal2 = 8
    static let Documentos = 11
    static let Pago = 10
    static var gender = ""
    static var TypeHome = false

}

struct beerExtId {
    static var idModelo = ""
    static var idExternoGM = ""
    static var isSelected = false
}

struct claseAddon {
    static let Promocion = 1
    static let Servicio = 2
    static let Producto = 3
    static let HS = 4
}

struct faseSelectedPlan {
    static var firstPlan = false
    static var invertirPlanes = false
    static var plan1: LlenadoObjeto = LlenadoObjeto()
    static var plan2: LlenadoObjeto = LlenadoObjeto()
    static var planNegocioID = ""
    static var selectedPlan: LlenadoObjeto = LlenadoObjeto()
}

struct UserDefinitions {
    static var isEmbajador = Bool()
    static var isMinistro = Bool()
    static var isEmpresarial = Bool()
    static var isResidencial = Bool()
    static var isPromotor = Bool()
    static var idEmployee = ""
}

struct faseConfigurarPlan {
    static var adivinaQuien = Bool()
    static var isNegocio = Bool()
    static var ResidencialisEmpty = Bool()
    static var MicronegocioisEmpty = Bool()
    static var flujo = 0
    static var isDouble1 = Bool()
    static var isDouble2 = Bool()
    static var vengodeBack = Bool()
}

struct faseNuevoCliente {
    static var name = ""
    static var number = ""
}

struct faseComisiones {
    static var enableComissions = Bool()
    static var numEmpleado = ""
    static var fechaIGlobal = Date()
    static var fechaFGlobal = Date()
    static var nuevaFechaI = ""
    static var nuevaFechaF = ""
    static var fechaInicio = ""
    static var fechaFin = ""
    static var PagadasBoton = 1
    static var idFlujo = 4
}

struct faseReportes {
    static var searchButton = Bool()
    static var emptyReports = Bool()
}

struct fasePreguntas {
    static var editar = Bool()
    static var idCliente = ""
    static var Index = 0
    static var numberOfPeople = 2
    static var numberOfRooms = 2
    static var tvProvider = ""
    static var tvProviderOther = ""
    static var internetProvider = ""
    static var internetProviderOther = ""
    static var phoneProvider = ""
    static var phoneProviderOther = ""
    static var favoriteAppsArray = [String](repeating: "", count: 3)
    static var favoriteApps = ""
    static var usesInternetArray = [String](repeating: "", count: 6)
    static var usesInternet = ""
    static var usesInternetOther = ""
}

struct fasePostPreguntas {
    static var Index = 0
}

struct faseDataConsult {
    static var direction: Direccion = Direccion()
    static var isPaymentRiskConsulted = false
    static var isExpressAcepted = false
}

struct faseDataOK {
    static var isOK = false
    static var isSended = false
}

struct planesRecomendados {
    static var idOptimo = ""
    static var idEquivalente = ""
    static var idRecomendado = ""
    static var planesArray = [["Equivalente", "https://concepto.de/wp-content/uploads/2018/10/URL1-e1538664726127.jpg"], ["Plan Ideal", "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxIREhUTEhMVFhUXGBgWGBcWGBUXFhcZFRcaGBUYGBgYHiggGholGxgYITEiJSkrLi4uFyAzOjUvNygtLisBCgoKDg0OGxAQGysmICUvNzItLS0tLS8vLS0tLSstLS0tLS0tLS0tLS0tLS0tLS0tLS0rLS0tLS0tLS0tLS0tLf/AABEIAK8BIQMBIgACEQEDEQH/xAAcAAEAAgMBAQEAAAAAAAAAAAAABgcBBAUCAwj/xABFEAACAQIEAwUFBgIHBgcAAAABAgADEQQFEiEGMUEHEyJRYTJxgZGhFEJicrHRI1IVQ1OSosHwFzNjguLxFiRUc8LS4f/EABgBAQEBAQEAAAAAAAAAAAAAAAACAQME/8QAIhEBAQEAAgIBBQEBAAAAAAAAAAECAxESITEEEyIyUUFx/9oADAMBAAIRAxEAPwC8YiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICImpj8etLTquSxsLf5+USdlvTbmLzQyzOKVdbow2JBHkVNmHvBBuOc4GEznVV1hrq+673HuHvH6Spi1F3Il8TT/AKSp/wA0xM6qvKN2IiY0iIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiJoZnnNDDhTVqBdbaV/E3kIk7Zb034mvg8YlVdSG/mOo94kQ4rxb/aAiVCFdGpje6pVQalNvUE3/ACSs57vTNa6nabyHZ9xe1KjiyiWaiHA5E3sCj77abG85nB3Hy4hWo1fDXp6kZL3Ksmxtf2lv8ROcKuuqA4v3itQqjoSlyhI8iC4+Kzpjj/rlvk/jncGdq7MQmMNmO2s7I35gPYPqNvdJZxDm+uooHsMg0H8QNz6cipHmAZVGa8HVKBJpDWg+7zYD0vz93OZyHPKuGGkDvKPWi+1vM02+6fTl7p0mOr253fc6dfjPG18BWTMMMfBWITEId0NRRZWI6EgEX/CPOdLIc8o4tb4c6Ku7tQY73vcmkfvC/T16T3h1oY+jVpK2ujUFmU7VaDjdSVPS4G/p1ErfFZLVwjgOCCD4ai3AuDsQRyN46svplss9rg/p2p/Yv/db9olZ/wDiHHf+sq/4P/rE31/Ge/6/TERE8j2kREBERAREQEREBERAREQEREBERARE1sxrFKbsOYFxA2Z8cZiRTQub2G+3OauRYo1aKuxuTqv8GInz4je1Bh57SuvfSbfx7buBxIqoHAsD+9p95FcHnH2fL6la2o0lqNpva5BJA9L3E5/DXHoxBvUAAPMAG9M//JZX2776T9yTrtNsRXWmpdjZVFyfICc7A5/Rquaakg9NVhfrb0PvjPsUow5I3V7LccrPtf6ymOL8yqYKpQxKeyb0aq9DbxIfQg67GbnEs7rN7s11Fz8SVLUGANmbZbGxJA1W/wAMp7tFepUpUVW5RSz7cxe19uo5fOSmnxUMXhlYHUUIYH723NWHna+/WYyvCriGFM25sq35crr8xadcZ8Z7cd68r6RDhHjGth9IqMWQbBhuyjyI+8vpzEk+b4rviaqEEOUqqy7rrQ+fkV2+c5/EHBhRi1JdLfeTkG9V6A/Q/WcTLqtSgT3e4v46L7Anr+VvXrLkl9xz1qz1XJ48wT0MauKokqtZQ2oXFqi7HcciRpPzndyXiQVwErkU6uwWryRj0D+R9f8AtOrhkpYtBRCVGBY+Aqb0zY6lJOxHuvOHmfCVWib0wXpna1rsnv8ANf0+syTqtt7iz8mwiYhCrDTVG/vHI38xfr6iR7iPgy5LINFTz+6/5h19/OSDs+ytUDVAhFlFMM4bU3Ik+Le2wkwq0gwsRcTld+OvTtnjms+1HZNkzLiP4lB9ajSNIazatvaXYpzO8sjF8IoU06QykWZCBb1sPKSShhFTl53mxJ1y2/CscMnygP8Asxwf9n/ib95mT2JP3NK+3kiIkOhERAREQEREBERAREQEREBERAREhfHvFb4FkXUFDjw6UZ6jEe0AovyuOnWbmd3pOteM7TScziR9OHqH0/znvIM0GKw9OuFZda30uLMp5MCLnqDGe0g9FlPI7fMzc+tezXvPpxuB6vgceT/qqzocTn+CxHSx+s4vAj+Gp+YfVR+07nEC3ov7p01+7nn3xoVg6Jr4fFUlv41S9udjcH6CQo5bWw1QAn8jja/oel/Tr+lh8An+I480X/CT+872fcPJWU2UG/Nf8x5GdLuZ11XGcd3juIJl+du1JqD7EgkKeV/5kPlfmJyeIMH9rw9WmfaDKw8wQQf3E2szyxqB01LlL+CpyZT0uejevI/SfDvao27sO1ra7hUI6aut/QCdeo5d3/USyha2DqW3VvI30sP2/STbKM7po61ASG1KxpgFm1C1yoG5BElWecHrWQG3MXFvaRrdD5f6M5WRcLYhHVWVAuoanud1BHsjmNuh5X6yJrPS7jUqyMRhlqr4h7j1F5DeIODRVOpTpccnHI+jDqP9CTkQRPLndz8PXrE18ohwnw9VouGrFbKpsFufEetzbpfa0lFTBqW1W36+s2IjW7q9mcTM6jyiAbCeoiSsiIgIiICIiAiIgIiICImCYGYnIy3Pades9NNwovq/mN7G3pOvNss+WSy/DVx+YU6IBc21EKB1JJt8ptSAcfspqoyupOnSQDcqQSQSOnP6T4VO1KhTqJSYEm6ippDOyg2uSqjYddzy6Tp9u+Msc5yzysqxSZrPmFII761Kp7RBva3TbrvynzzdVahVUnZkYfNTaVBicVUFFqSsQrsuoDzva/yvHHx+cZy8vhXWz7tVYOyYek5CmxKqD82chb+gvPeSdotdyNYJ/BUVVJ/KybfrIkMhqVtwdCdNrkjz9BPuMiqUh4jqX+YCzIejfA73necefh5rya+V25TmSYmmKicuRB5qeoMiXaBkmIrvTakV0731H2eXIdb2+k5nZ/mLJXCE7VAVYdNS3sR8QR8ZZb0weYvOOp9vfp6M37uPbk8J4E0MLTpk3IBubW3LE7DpNrOGtTJPIb/IzSzXifDYZijMS4sCqC5W4vv0GxBn14gIfDvbcMht8RtI6vfd/wBX3PHqf4jfAL71R6p+h/aSfNxem35T+khXZlixVatpDWHd2LKVDe17N+Yk/wAXh9SkDyMvk/dHF741f8B4tBi2pahq0v4bi9lcb2lkyA8FZTUTFVHel3YGvc2BZiw32/UyfTOb9m8E6y5uaZQlYEEDfYg8jIzQ4HAqC9VzTBB0WF7X5FuZEnESZvU+F6486vdYtMLTA5CeokLIiICIiAiIgIiICIiAiIgIiQXBZwVxN3JIGu/XYXlZzdI1uZ6TqJq4jMaVNO8dwq+Z636AcyfQSM4nj+gG0ojP72VT8tz84zjWviGuTOfmphNXMMwpUF1VXCg7C/M+gHMzVyLPaWLUlLhl9pTzF+R9R6yEduVFmwtEh2CrUOoKSt9SELcje1wdvURM/l1S7/HuOU+djCvVrJcqNRFgxOnVceFd/hOvwTxxVxtSslZSi6AU1aVJ3s1lBJGxHMyDZeh+y0wRYNT2HoCV/VTOxwDkrLXSo7FtiLAWHiHzM9e8yzt4+PVl6RTLMZUGMZKtVmPjUC9kBVuijrtzO86mPyx6tRTTYeIHXe/hK7DlzuP0ku4k4T0tVrU1VdzU25sebcvjNPhXQcQocAgqbX5XG4+l5ubPHtOpfLpYWFUtQQE38C3Pn4bGVjmFLRVdG5B7/Am/7y21A02HKVxxth9FcN/MPqP9Gc+G/l07fUT8ZU1yTB0gisApuAdWx5+XpPpm+EpsjFgosD4thYdbnylKUeM8Zg6z0UOpdmUXAuG9GBHO46cps4jiXMMXZXARfN6isB693TAB+Jmfbvl8n3JM/Du5Lc4un3e6q5YnyUXt/eNh85cFJrgGVlwXgNTqFu1iDUqEe0V3Vfnvbp8ZZ6i0nnvdX9POsqj7R6avii3ePTBGlgtgXtbSdXMbEjbykuyLBVKmXIoJ/wB0VXWWJsLhbk78rc5ye0zi/L8Cw1qtbFgeGkttQvyLtY6B9T0E8UeJc4OVitSywLX5BGe/8O1+9FI2Y77aCb9Zl5J4yRWeK+Vt/wBdrgPJalAVGqkFm0gBb2Fr9Tz5yXSsuDe1/CYgiji1+yV76bNfuy3lqI8B9Gt5XMsxWBAINwdwRyM561dXuumMzM6jylMDlPjjcwpUdPe1Up6mCrrZV1MeSi53PpIX2hdp2GywGmlq2K6UlOyX61GHL8vM+nOVHwxVpZ3j9ecY3SQbJRN0V7/cVvZpjlt7R+slS8+NeOMJldPVXbVUYeCkli7+tvur+I7fpONV7SHbLxjaGX4qpc20FbADa76hcmn5MB8ps8S9mGXY1FBpd06qESpSNiFUWUEcmA9d/WQj+js/yG3cN9uwaj2LElQOmi5dNv5SRtAl2fcd42lhKOKoZXiGD+Kor2DU1/KhLXPMEgADnPtwl2qZdj7J3ncVTt3day3P4X9lvdcH0m32fccpm1NmWhVpMmzhgTTv5JUsAx9LA+kxxZ2cZfmF2qUtFU/1tLwPfzbo3xBgS8RKYbIs/wAl3wdb7dhh/VOLsq+QQnUP+Qn3Tv8ACvbDg8Swo4pWwlcnTpe5QtysHsNJ/MB5QLIiIgIiICIiAiIgIiICVPjK4p4itfzqL82lsSmeMVPfYhRtdyv95rf5zvwfNeb6j4iOcRZtiMW1mcpRQaV0mzMBzsfur7tzOdlOGCNqp0Db+YLuf+Y7mTHB5EK1XdbohCKvQkeY6gcred5MaPCyhbufgttvjO1uc1xmdaiK8N480cTSYHYkqfcyn/O0sjM6AxFFkNtxtflcbiVRbu62n+Wpp+TWlrZW+qmp8wP0k809zUV9Pe5c1XPEeDFFkp87KfQWZiRb5mTDgllOHpkAA2sbeY2N/lOB2j0tBp1bHSNSseg5FSfIc95FMDxyaKd1hmDnewpIXe59fZHvM2/lie2Z/DkvpcuYUgykeYlNPmi4Kt43VGpsQBU2uOQI8wQb7S0+Dca+IwlM1b97YhwSGa4O1yNibWke4u4brtUvTRXU7i5AI8xuPORx3xtzV8ufKTUc3h7jyriMVSWx7knSxCaad2FlOpt23tyna46oa6WsfdN/h1+l5ycn4Ur94jVWChWU6V3JsQbXPL4Sd53lwaky22IjVznU6MzWs2VTC5GuKqqdwygi687X8uv/AOyW5NwOpI1Go3pyHx0gTj8KpXpYxCzoF1GnoUFmIJsCWPI7A7CXXRFgJXLu5vpPDxzU9tLKMsWggVQBbYAchOhETy29vZJ0oHtWx2JwmcjFUaNImmtMjUofV4d2IIuPK6m40yccEdreDx1qdYjDVztpc/w2P4HPW/3TY++fbtL4MrYxlxGH0moiaGpnYuASw0ty1bkWPnzlG5xkgLMtRDTqqbG4swP4lPOGv0RxhwDgczW9enpq22rU7LUHlc8mHo15U2d5Dn2SUqiYWvUrYRgRqpjU1MHmdBBakbdVNt785yeFe0fMMqIp1f8AzGG5aXJJUf8ADfmvuNxL04Q43wWZpfD1PHa7UnstVfO69R6i4mCpuxKnlBcVK9XVjySQK9goJJ3pEmzufM7+QHWzOMezfAZldqlPu6x/rqVle/4hyf47+onP417JsDj71KYGGrm5101GhietSnyO+9xYzb7NOHsywVN0x2LFZAbUk3cqAfa7xvFY9F6fSBxeBeEM5y7EikcXTqYBRcB7sSNrKinem3uYr75aERA8ogHIAddttzzM9REBKl7XuFszxFZMThKdColIXCooGIJ2vqZvbG2wUj3X3ltRAqrhvtnoMwoZjRfCVhsSQ3d3/ECNSfEEess7A42nXQVKNRKiNuGRgyn3EbSKdp2R08RhHYYBcXXAsg8KVFvzYPcMQP5Qd9pXfZT2hYbLqbYHG03w7CoTqKnSCQos6+0p2vex+EC94mvgcdSroKlGolRG5MjBlPxE2ICIiAiIgIiICU9xnti63/uKfoplwym+0WqtPF1yzBQdBuxAG6LbnO/0/wC1/wCPN9T+s/6m/CmGA1N11N9WN5I69PacXhTdW/MT895ImW4kbv5OvHPxUXxfjaVDGVEZwGLghRcsb2I8IuZbPDg1Ul936SIcVcM1ftbVKNJT3oBZ9huoC+I8+QEm3DOCajh0RyC4HiI5fCdOTXeJ7ceLPW76eM9yo1abBLaul+R8wZX44UxTNayIPO9/oBLZnnux5SMctzHXfDnfy4fCWT/ZaRUsWJOok7dALAdBtO49MHmJ6ic7bb26ZzJOo8JSUchMsoIsZ6iY1HcHwnQpVu+Vbtq1AsSbXN9r8pIoibdW/Kc5mfgiImKa+Lqldh1kc4g4ew+NW1dLsPZqLtUX3N1HobiSDHKdj0kY4x4kTLsMa7o9TcKqoObEEjUfurYHf9YFS8Z8IVsvU1GIqYe9u82Fr8ldCbgn0uD6SAVwlMitQqGm48S6SQb/AISN1MlODxdbiLHpTxmLp4ene6ITpUA/coqdmqHzY39/KXr/ALOcs+yjCfZl7sb6v63Va2vvOer6elpvYq7gftuq09NLMVNVOXfoB3i/nUbP7xY++Xfk+b0MXSFXDVVq0z95T9COan0O85NfgXLnwq4RsMncr7I5Op6uH9rUepvvIjwl2TNl2O+0UsdUGHABCCwd/wANU+yyeoF9+nOYLSiYVr7jlMwEREBETDMACSbAbknkIGZRfblxjgagOEpUaVfELs1cj/cWt4UYe03nvYep2HntU7W9erCZc/hN1qYhTu3QrSI6fjHPp5yvMhyG1qlYb81Q9PVv2gdjsrp4mhi6DrVqUkerTUopsKiswB1Lytb0vP1FKi7POEKlaomKq3SkjB0H3qjKbgjyS459ekt2bWERExpERAREQEqjjzhsfbmrdyaz1QpUkFwulQulQfCtrA/GWvPL0wecvGvG9o5MeU6cHgvCVEoXqizk8r3sAABf1kgmFW3KZk6vd7bmdTp5ZAecyBMxMUREQEREBERAREQEREDBEifaDlDVsFVVFLMNLqALm6sCQB56byWzBF4H5GzLJVe5TwP1HIEjzHQyQ4fivNUwZwhxZs2wY3aqi2toFW9wD8SOhly8YcDUMXdx/Dq9Kqi9/Sou2r38/WQfLuy+vULjEVFpBbimUtU1no1riyemx9030xDaPEOZLgzg2xj6W67l1X+zFT2tJ8vhy2nyo5vj1wZwbYyp3bdAb6R/IHPi0HqL2+F7zzLeyyq+v7RVFO1xTNOz6j0dr2sv4dj7uv1yvsqZtf2msFI2p914h6O2q234dj6x6EB4O45zLKnWkL16BNhSckqB/wANudPbpy9Osk/EnbNjUro+HoKuHX2lqeIuT7WplPgt0t7zflOdxDwtisC1qil0J0rUQEqxPIW5qx/lPwvNbNOF8ZhaYqV6JFNxe+zBb/dqW9k++OjtbHC3arl2NplmqDD1EUs9OqQLAbko3Jx7t/SOGe1bLsbVaiKhpPqITvgFFUXsCpvYE/ymxlFLhKdO5RQCTv8AsPITVxuTJU8SWRvT2T8Onwjo7frTFYlKSNUqMqIoLMzEBQBzJJ6T87dqPam+PJwuCLLhidLMLh6/S1uYT8PM9fKQ3H5rmL0UwVWrVeiGulMm6kjl4uZA5gE2HkJ1clyZaA1NvUPXot+i/vHTXwyPIxStUqgF+YHRP+r9JbnAvAhracRi1tT5pSPN/IuOienX3c93gTgT2cTi132anSPTqGqDz8l6dfIWTDBVAFhsBtMxExpERAREQEREBERAREQEREBERAREQEREBERAREQERPliCbQPNeqOXOa1pm0zaBi0WmbTNoGJhlBBBFwdiDuCPIierTIEDg5dwhg6Fc16VEByNhzRPMop9kn/ALWnG4n7OaGIvUw9qFU72A/hOfVR7J9V+UnAEzaB+eMdk2IwtUUq1JlY7LYEh/yEe18N/SWhwLwOKJWvigDV5oh3FPyJ83+g98mzUwbXANjcXANj5jyM9ze2dNiJ4R57mNIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgJgiZiBq1Kdp5tNsiYKAwNWJs90L3nxdLQPMzEyICZiZgYmYntFgESfSIgIiICIiAiIgf/Z"], ["Plan Optimo", "https://seo.pe/wp-content/uploads/estructura-url-seo-pe.jpg"]]
}

struct pantallas {
    static let pagadas = 0
    static let scrollViewPagadas = 1
    static let detalleCuenta = 3
}

struct dialogs {

    struct Error {
        static let NoCoverage = "Favor de validar la cobertura antes de avanzar"
        static let CpInvalid = "No se ha podido validar tu informacion, consulta de nuevo el codigo postal"
        static let CpNoExist = "Nuestra base de datos no cuenta con registro de este código postal"
        static let Login = "Verifique su usuario y constraseña "
        static let InternService = "Ocurrió un error al conectar con el servidor "
        static let ConnectionInternet = "Verifique su conexión a internet "
        static let FillFields = "Favor de completar todos los campos "
        static let FillCell = "Favor de ingresar un número de celular"
        static let FillStep3 = "Favor de completar el paso 3 "
        static let FillStep4 = "Favor de llenar en el paso 4 "
        static let FillStep5 = "Favor de completar el paso 5 "
        static let FillStep12 = "Favor de completar el paso 1 para poder obtener la oferta"
        static let FillStep2 = "Favor de completar la información del paso dos para poder seleccionar esta opción"
        static let FillStep123 = "Favor de completar el paso 1 , 2 y 3 para poder posponer la venta"
        static let PreviewImage = "Error en la vista previa, vuelve a tomar la foto"
        static let SuiteInclude = "Debes seleccionar al menos un producto de la suite incluida"
        static let SearchDirection = "Seleccione alguna direccion para continuar"
        static let ErrorCP = "No se ha encontrado un Codigo postal, prueba moviendo un poco el mapa"
        static let ErrorCovergage = "La direccion no cuenta con cobertura"
        static let TermsConditionsCard = "Favor de aceptar el registro de tarjeta para cargos y la revisión de buró de crédito y firme para poder continuar"
        static let TermsConditionsAll = "Acepte todos los terminos y condiciones y firme para poder continuar."
        static let NumberEmployee = "Favor de completar el numero de empleado"
        static let OwnerIdentification = "Favor de completar la Identificación del Propietario"
        static let SelectPackage = "Favor de elegir nuevamente el paquete deseado"
        static let NoSendSale = "No fue posible enviar la venta, Verifique los datos"
        static let NoSendSale2 = "No fue posible enviar la venta , verique su conexión e intentelo de nuevo"
        static let NIP = "Ocurrio un error al verificar el NIP"
        static let Construccion = "Estamos trabajando en este flujo, disculpa las molestias"
    }

    struct Warning {
        static let ExpressSale = "Para verificar si hay disponibilidad de venta express debe completar la validación de cobertura en el paso 1"
        static let Version = "Esta versión de la Aplicación de Ventas Totalplay, ya no cuenta con soporte por favor actualice su aplicación "
        static let DeleteSale = "¿Está seguro de eliminar todos los datos de la solicitud?"
        static let maximoAlcanzado = "Se ha alcanzado el maximo permitido a agregar"
        static let deleteComplete = "Se ha eliminado"
    }

    struct Info {
        static let WithOutLead = "Por el momento no hay Leads que mostrar."
        static let WithCoverage = "Tu orden está dentro de cobertura"
        static let WithoutCoverage = "Su dirección no cuenta con cobertura"
        static let ConsultCP = "Ingrese el código postal de 5 digitos"
        static let WithoutData = "Por el momento no hay información para mostrar"
        static let NoExpressSale = "No hay disponibilidad de venta express"
        static let WaitLoadInfo = "Porfavor espera, se esta cargando la información."

    }

    struct Success {
        static let canalEmpleado = "Se guardo correctamente tu canal de venta"
     static let SaveScreenShot = "Se ha tomado una captura de pantalla revisa tu galeria"
    static let createLead = "El lead fue creado correctamente"
        static let createCommentaryLead = "El comentario fue enviado correctamente"
     static let Download = "Descarga completada"
     static let VerifyNIP = "El NIP fue enviado con exito"
     }
}

struct tvPremiumFocusParrillas {
    static let tvInfocus: [String] = [
        "fox sports premium", "nba league pass", "hbo max",
        "paquete hbo", "universal+", "paramount+"
    ]
}
