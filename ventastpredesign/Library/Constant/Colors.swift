//
//  Colors.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 11/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class Colors: NSObject {

    static let DARK_GREEN = UIColor.init(red: 78, green: 132, blue: 36)
    static let LIGHT_GREEN = UIColor.init(red: 196, green: 211, blue: 172)
    static let PRIMARY = UIColor.init(red: 140, green: 193, blue: 81)
    static let SECONDARY = UIColor.init(red: 205, green: 219, blue: 74)

    static let White = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0 ).cgColor // StartColorDegradado
    static let TransWhite = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.0 ) .cgColor // CenterColorDegradado
    static let LowGray = UIColor(red: 236/255, green: 239/255, blue: 241/255, alpha: 0.77 ) .cgColor // EndColorDegradado
    static let DarkGray = UIColor(red: 224/255, green: 224/255, blue: 224/255, alpha: 1.0 ) .cgColor // BordesView
    static let TransBlack = UIColor.black.withAlphaComponent(0.6)
    static let SelectGray = UIColor(red: 108/255, green: 36/255, blue: 251/255, alpha: 1.0 ).cgColor
    static let Purple = UIColor(red: 101/255, green: 30/255, blue: 255/255, alpha: 1.0 )
    static let Gray = UIColor.gray.cgColor
    static let Black = UIColor.black
    static let Green = UIColor(red: 189/255, green: 213/255, blue: 57/255, alpha: 1.0 )
    static let Yellow = UIColor(red: 250/255, green: 176/255, blue: 66/255, alpha: 1.0 )
    static let Pink = UIColor(red: 224/255, green: 19/255, blue: 109/255, alpha: 1.0 )
    static let DarkBlue = UIColor(netHex: 0x3A4559)

    struct GrayP {
        static let Dark = UIColor(red: 224/255, green: 224/255, blue: 224/255, alpha: 1.0 )
        static let Medium = UIColor(red: 108/255, green: 36/255, blue: 251/255, alpha: 1.0 )
        static let Light = UIColor(red: 236/255, green: 239/255, blue: 241/255, alpha: 0.77 )
    }

    static let Clear = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.0)

    static let Base_gray_lines = UIColor(red: 237/255, green: 239/255, blue: 250/255, alpha: 1.0)

    static let Base_purple_names = UIColor(red: 133/255, green: 13/255, blue: 254/255, alpha: 1.0)

}

struct color {
    static let White = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0 ) // StartColorDegradado
    static let TransWhite = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.0 )// CenterColorDegradado
    static let LowGray = UIColor(red: 236/255, green: 239/255, blue: 241/255, alpha: 0.77 )// EndColorDegradado
    static let DarkGray = UIColor(red: 224/255, green: 224/255, blue: 224/255, alpha: 1.0 ) // BordesView

}
