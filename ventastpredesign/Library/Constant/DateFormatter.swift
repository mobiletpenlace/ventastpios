//
//  DateFormatter.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 12/04/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import Foundation

struct dateFormatter {
    static let dayhzone = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    static let dayhour = "dd-MM-yyyy HH:mm"
    static let dayd = "dd-MM-yyyy"
    static let dayy = "yyyy-MM-dd"
}
