//
//  TPDocumentsShared.swift
//  SalesCloud
//
//  Created by Axl Estevez on 27/05/24.
//  Copyright © 2024 TotalPlay. All rights reserved.
//

import Foundation

public class TPDocumentsShared {

    public static let shared = TPDocumentsShared()

    var documentSelected: TPDocuments?
    var documentTypeSelected: TPDocumentType?

    private init() {}
}
