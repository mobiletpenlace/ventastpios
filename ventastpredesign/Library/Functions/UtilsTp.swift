//
//  UtilsTp.swift
//  ventastpredesign
//
//  Created by Arturo Ceron Epigmenio on 16/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import PKHUD
import UIKit

class UtilsTp: NSObject {

    static var overlay: UIView?
    static var viewController: UIViewController?
    //// valida textfile lleno
    func textFieldDidChange(textField: UITextField!) -> Bool {

        let text =  textField.text?.trim()
        var valid: Bool = true
        if (text?.count) != 0 {
            valid = true
            return valid
        } else {
            valid = false
            return valid
        }

    }

    func numbersAndLength(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String, maxChar: Int) -> Bool {
        if isOnlyNumbers(textField, shouldChangeCharactersIn: range, replacementString: string, maxChar: maxChar) {
            return length(textField, shouldChangeCharactersIn: range, replacementString: string, maxChar: maxChar)
        } else {
             return false
        }
    }
    func isOnlyNumbers(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String, maxChar: Int) -> Bool {
        let aSet = NSCharacterSet(charactersIn: "0123456789").inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltenetworking = compSepByCharInSet.joined(separator: "a")

        if numberFiltenetworking == "a" {
            return false
        } else {
            return true
        }
    }

    func length(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String, maxChar: Int) -> Bool {
        let maxLength = maxChar
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength

    }
    func textFieldMaxMin(textField: UITextField!, maxChar: Int, minChar: Int) -> Bool {

        var valid: Bool = true
        if ((textField?.text?.count) != 0 ) &&  ((textField?.text?.count)! >= minChar ) && ((textField?.text?.count)! <= maxChar ) {
            valid = true
            return valid
        } else {
            valid = false
            return valid
        }
    }

    func isValidEmail(textField: UITextField!) -> Bool {
        var valid: Bool = true
        let string = textField.text!
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let test = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        valid = test.evaluate(with: string)

        if  valid {
            return valid
        } else {
            return valid
        }
    }
    func frsit(text: String, noCaracteres: Int) -> String {
        if  text.count > 0 {
            let index: String.Index = text.index(text.startIndex, offsetBy: noCaracteres)
            let result: String = text.substring(to: index)
            return result
        } else {
            return ""
        }
    }
    func generateA(name: String, lastName: String, motherLastName: String ) -> String {
        let resultA: String = lastName.uppercased()
        var primerletra = ""
        var SegundaLetra = ""
        var first = true
        var hasTwoLetter = true
        for letra in resultA {
            if hasTwoLetter {
                if first {
                    primerletra = String("\(letra)")
                    first = false
                } else if letra == "A" || letra == "E" || letra == "I" || letra == "O" || letra == "U" {
                    SegundaLetra = String("\(letra)")
                    hasTwoLetter = false
                }
            }

        }
        let ab = primerletra + SegundaLetra
        let resultB: String = frsit(text: motherLastName, noCaracteres: 1)
        let resultC: String  = frsit(text: name, noCaracteres: 1)
        return (ab + resultB + resultC )
    }
    func validaRFC(RFC: String) -> String {
          let cast = ["BUEI", "BUEY", "CACA", "CACO", "CAGA", "CAGO", "CAKA", "CAKO", "COGE", "COJA", "COJE", "COJI", "COJO", "CULO", "FETO", "GUEY", "JOTO", "KACA", "KACO", "KAGA", "KAGO", "KOGE", "KOJO", "KAKA", "KULO", "MAME", "MAMO", "MEAR", "MEAS", "MEON", "MION", "MOCO", "MULA", "PEDA", "PEDO", "PENE", "PUTA", "PUTO", "QULO", "RATA", "RUIN" ]
        if  cast.contains(RFC) {
            let stepArfc = RFC.prefix(3)
            return stepArfc + "X"

        } else {
            return  RFC
        }
    }

    func primerosCuatroCaracteresRFC(name: String, lastName: String, motherLastName: String) -> String {
        // Eliminar acentos y llevar a mayúsculas
        eliminarAcentosYSimbolos(s: name)
        eliminarAcentosYSimbolos(s: lastName)
        eliminarAcentosYSimbolos(s: motherLastName)

        print(name)
        print(lastName)
        print(motherLastName)
//
        let compoundWord = name.components(separatedBy: [" "])
        _ = compoundWord[0]
        let nameB = compoundWord[1]
        let stepA = generateA(name: nameB, lastName: lastName, motherLastName: motherLastName)
        let RFCValid = validaRFC(RFC: stepA)
        return RFCValid
    }

    func eliminarAcentosYSimbolos(s: String) -> String {

        s.trim().uppercased()
        s.replacingOccurrences(of: "Ñ", with: "&")
        // s = Normalizer.normalize(s.replaceAll("[Ññ]","&"), Normalizer.Form.NFD);
//        s = s.replaceAll("[^&A-Za-z ]", "");
//
        return s
    }

    func RFCA(name: String, lastName: String, motherLastName: String) -> String {
        if  name.count > 2 &&  lastName.count > 2 && motherLastName.count > 2 {
            if lastName.contains("’") {
                return("")
            } else {
                if  name.contains(" ") {
                    let compoundWord = name.components(separatedBy: [" "])
                    let nameA = compoundWord[0]
                    let nameB = compoundWord[1]

                    if (nameA == "MARIA" ) || (nameA  == "JOSE") {
                        let stepA = generateA(name: nameB, lastName: lastName, motherLastName: motherLastName)
                        let RFCValid = validaRFC(RFC: stepA)
                        print(RFCValid)
                        return(RFCValid )
                    } else {
                        let stepA = generateA(name: nameA, lastName: lastName, motherLastName: motherLastName)
                        let RFCValid = validaRFC(RFC: stepA)
                        print(RFCValid)
                        return(RFCValid )

                    }
                } else {
                    let stepA = generateA(name: name, lastName: lastName, motherLastName: motherLastName)
                    let RFCValid = validaRFC(RFC: stepA)
                    print(RFCValid)
                    return(RFCValid )
                }
            }
        } else if  name.count > 2 &&  lastName.count <= 2 && motherLastName.count > 2 && lastName.count > 0 {

            if lastName.contains("’") {
                return("")
            } else {
                let resultA: String = frsit(text: lastName, noCaracteres: 1)
                let resultB: String = frsit(text: motherLastName, noCaracteres: 1)
                let resultC: String = frsit(text: name, noCaracteres: 2)

                print(resultA)
                print(resultB)
                print(resultC)

                let preRFC = resultA + resultB + resultC
                let RFCValid = validaRFC(RFC: preRFC)
                print(RFCValid)
              return (RFCValid )
            }
        } else {
           return ""
        }
    }

    func generatRFC (name: String, lastName: String, motherLastName: String, date: String ) -> String {
        if date.count > 0 {
             let RFCX = RFCA(name: name.trim(), lastName: lastName.trim(), motherLastName: motherLastName.trim())
            if  RFCX.count == 4 {
                let compoundWord = date.components(separatedBy: ["/"])
                let dia = compoundWord[0]
                let mes = compoundWord[1]
                let añoA = compoundWord[2]
                let año  = añoA.suffix(2)

                return RFCX + año + mes + dia
            } else {
               return ""
            }
        } else {
            return ""
        }

    }

    func isValidRFC(textField: UITextField!) -> Bool {
        var valid: Bool = true
        let string = textField.text!
         let rfcRegEx = "[A-Z,Ñ,&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9]?[A-Z,0-9]?[0-9,A-Z]?"
        let test = NSPredicate(format: "SELF MATCHES %@", rfcRegEx)
        valid = test.evaluate(with: string)
        if  valid {
            return valid
        } else {
            return valid
        }
    }

}
