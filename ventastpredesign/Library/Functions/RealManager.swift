//
//  RealManager.swift
//  ventastpredesign
//
//  Created by Arturo Ceron Epigmenio on 09/05/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import RealmSwift

class RealManager: NSObject {

    class func mReal() -> Realm {
        return try! Realm()
    }

    class func findFirst<T: Object>(object: T.Type) -> T? {
        return try! Realm().objects(object).first
    }

    class func deleteAll<T: Object>(object: T.Type) {
        let realm = try! Realm()
        try! realm.write({ () -> Void in
            realm.delete(realm.objects(object))
        })
    }

    class  func deleteFrom<T: Object>(object: T.Type, field: String, equalsTo: String) {
        let realm = try! Realm()
        try! realm.write({ () -> Void in
            realm.delete(realm.objects(object).filter("\(field) == '\(equalsTo)'"))
        })
    }

    class func insert<T: Object>(object: T) -> T! {
        let realm = try! Realm()
        realm.beginWrite()
        realm.add(object, update: false)
        try! realm.commitWrite()
        return object
    }

    class func insertCollection<T: Object>(objects: [T]) {
        let realm = try! Realm()
        try! realm.write({ () -> Void in
            objects.forEach({ object in
                realm.add(object)
            })
        })
    }

    class func update<T: Object>(object: T) -> T! {
        let realm = try! Realm()
        realm.beginWrite()
        realm.add(object, update: true)
        try! realm.commitWrite()
        return object
    }

    class  func updateLine(updatingCode: @escaping () -> Void) {
        let realm = try! Realm()
        try! realm.write {
            updatingCode()
        }
    }

    class func getSorted<T: Object>(object: T.Type, field: String, ascending: Bool) -> [T] {
        let count  = try! Realm().objects(object).count
        var results: [T] = []
        if count > 0 {
            let realmResults = try! Realm().objects(object).sorted(byKeyPath: field, ascending: ascending)
            realmResults.forEach { element in
                results.append(element)
            }
        }
        return results
    }

    class   func getCount<T: Object>(object: T.Type) -> Int? {
        return try! Realm().objects(object).count
    }

    class func getAll<T: Object>(object: T.Type) -> [T] {
        let realm = try! Realm()
        let realmResults = realm.objects(object)
        var results = [T]()
        realmResults.forEach { element in
            results.append(element)
        }
        return results
    }

    class func getFromFirst<T: Object>(object: T.Type, field: String, equalsTo: String) -> T? {
        let count  = try! Realm().objects(object).count
        var realmResults: T?
        if count > 0 {
            realmResults = try! Realm().objects(object).filter("\(field) == '\(equalsTo)'").first
        }
        return realmResults

    }

    class func getFromAll<T: Object>(object: T.Type, field: String, equalsTo: String) -> [T]? {
        let count  = try! Realm().objects(object).count
        var results: [T] = []
        if count > 0 {
            let realmResults = try! Realm().objects(object).filter("\(field) == '\(equalsTo)'")
            realmResults.forEach { element in
                results.append(element)
            }
        }
        return results
    }

}
