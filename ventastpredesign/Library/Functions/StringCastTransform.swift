//
//  StringCastTransform.swift
//  ventastp
//
//  Created by Ulises Ortega on 09/11/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

// SPECIAL TRANSFORM
// Transforms recieveng double from JSON into string, and leaves the string value when transformed into a JSON
import ObjectMapper

open class StringCastTransform: TransformType {

    public typealias Object = String
    public typealias JSON = String

    public func transformToJSON(_ value: String?) -> String? {
        return value
    }

    public func transformFromJSON(_ value: Any?) -> String? {
        // Returns string value (add aditional casts and return methods if needed)

        if let doubleValue = value as? Double {
            return String(doubleValue)
        }
        if let boolValue = value as? Bool {
            return String(boolValue)
        }
        if let stringValue = value as? String {
            return stringValue
        }
        return nil
    }

}
