//
//  ConfigTextfieldMaterial.swift
//  ventastpredesign
//
//  Created by Marisol Huerta O. on 14/06/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import MaterialComponents.MaterialTextControls_FilledTextFields

extension MDCFilledTextField {
    func setFilledTP(colorBackground: UIColor = UIColor.clear) {
        self.setFilledBackgroundColor(colorBackground, for: .normal)
        self.setFilledBackgroundColor(colorBackground, for: .disabled)
        self.setFilledBackgroundColor(colorBackground, for: .editing)
    }

    func setTextTP(colorTxt: UIColor = UIColor(named: "Base_rede_blue_textfields_wait")!) {
        self.setTextColor(colorTxt, for: .normal)
        self.setTextColor(colorTxt, for: .editing)
        self.setNormalLabelColor(UIColor(named: "Base_rede_ligth_gray_titles")!, for: .normal)
        self.setNormalLabelColor(UIColor(named: "Base_rede_ligth_gray_titles")!, for: .editing)
        self.setFloatingLabelColor(UIColor(named: "Base_rede_ligth_gray_titles")!, for: .normal)
        self.setFloatingLabelColor(UIColor(named: "Base_rede_ligth_gray_titles")!, for: .editing)

        self.setUnderlineColor(UIColor(named: "Base_rede_ligth_gray_titles")!, for: .normal)
        self.setUnderlineColor(UIColor(named: "Base_rede_ligth_gray_titles")!, for: .editing)
    }

}
