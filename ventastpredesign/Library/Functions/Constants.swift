//
//  Constants.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 08/04/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import Foundation
import AVKit
import Photos
import Lottie
import UIKit

class Constants: NSObject {

    public static var spinnerView: WaitingView!

    public static func LoadStoryBoard(name: String, viewC: UIViewController) {
        let viewV = UIStoryboard(name: name, bundle: nil)
        let controller = viewV.instantiateViewController(withIdentifier: name)
        viewC.modalPresentationStyle = .fullScreen
        viewC.present(controller, animated: false, completion: {})
    }

    public static func LoadStoryBoard(storyboard: String, identifier: String, viewC: UIViewController) {
        let viewV = UIStoryboard(name: storyboard, bundle: nil)
        let controller = viewV.instantiateViewController(withIdentifier: identifier)
        viewC.modalPresentationStyle = .fullScreen
        viewC.present(controller, animated: false, completion: {})
    }

    public static func LoadStoryBoardBoot(storyboard: String, identifier: String, viewC: UIViewController) {
        let viewV = UIStoryboard(name: storyboard, bundle: nil)
        let controller = viewV.instantiateViewController(withIdentifier: identifier)
        controller.providesPresentationContextTransitionStyle = true
        controller.definesPresentationContext = true
        controller.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        UIApplication.topViewController()?.present(controller, animated: false, completion: nil)
    }

    public static func Gradient(view: UIView, frame: CGRect, colors: [CGColor]) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = frame
        gradientLayer.colors = colors
        view.clipsToBounds = true
        view.layer.insertSublayer(gradientLayer, at: 0)
    }

    public static func Back(viewC: UIViewController) {
        viewC.navigationController?.popViewController(animated: true)
        viewC.dismiss(animated: false, completion: nil )
    }

    public static func Border(view: UIView, radius: CGFloat, width: CGFloat) {
        view.layer.cornerRadius = radius
        view.layer.borderWidth = width
        view.layer.borderColor = Colors.DarkGray
    }

    public static func BorderCustom(view: UIView, radius: CGFloat, width: CGFloat, masked: CACornerMask ) {
        view.layer.cornerRadius = radius
        view.layer.borderWidth = width
        view.layer.borderColor = Colors.DarkGray
        view.clipsToBounds = true
        view.layer.maskedCorners = masked
    }

    public static func GetDate(formater: String, numDay: Int, numMonth: Int) -> String {
        let date = Date()
        let formatter = DateFormatter()
        let calendar = Calendar.current
        var component = DateComponents()
        var dateC: Date?
        var dateP: String?
        formatter.dateFormat = formater
        component.day = numDay
        component.month = numMonth
        dateC = calendar.date(byAdding: component, to: date)
        dateP = formatter.string(from: dateC!)
        return dateP!
    }

    public static func DateString(formater: String, day: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = formater
        let dateString = formatter.string(from: day)
        return dateString

    }

    public static func Alert(title: String, body: String, type: String, viewC: UIViewController, dimissParent: Bool = false, dismissDouble: Bool = false) {
        let alert = UIAlertController(title: title, message: body, preferredStyle: UIAlertControllerStyle.alert)
        if type == "normal" {
            alert.title = title
        } else {
            alert.title = type.uppercased() + "\n\n"
            let viewImage = ImageAlert(type: type)
            alert.view.addSubview(viewImage)
        }

//        alert.view.backgroundColor = UIColor.white
//        alert.view.layer.cornerRadius = 8
      //  alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
            if dimissParent {
                if dismissDouble {
                    viewC.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: false, completion: nil)
                } else {
                    viewC.presentingViewController?.dismiss(animated: false, completion: nil)
                }
            } else {
                alert.dismiss(animated: false, completion: nil)
            }
        }))
        viewC.present(alert, animated: true, completion: nil)
    }

    private static func ImageAlert(type: String) -> UIView {
        let imgTitle = UIImage(named: "Img_" + type)
        let imgViewTitle = UIImageView(frame: CGRect(x: 20, y: 15, width: 25, height: 25))
        imgViewTitle.image = imgTitle
        return imgViewTitle
    }

    public static func Shadow(color: CGColor, opacity: Float, offSet: CGSize, radius: CGFloat, view: UIView) {
        view.layer.masksToBounds = false
        view.layer.shadowColor = color
        view.layer.shadowOpacity = opacity
        view.layer.shadowOffset = offSet
        view.layer.shadowRadius = radius
    }

    public static func OptionCombo(origen: String, array: [String], viewC: UIViewController) {
        if array.count > 0 {

            let viewAlert: UIStoryboard = UIStoryboard(name: storyBoard.Option, bundle: nil)
        let controller = viewAlert.instantiateViewController(withIdentifier: storyBoard.Option)as! OptionComboViewController
        controller.origin = origen
            controller.pickerValues = array.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
        controller.providesPresentationContextTransitionStyle = true
        controller.definesPresentationContext = true
        controller.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            viewC.present(controller, animated: false, completion: {})
        }
    }

    public static func OptionComboDate(origen: String, viewC: UIViewController) {

            let viewAlert: UIStoryboard = UIStoryboard(name: "OptionDateComboView", bundle: nil)
        let controller = viewAlert.instantiateViewController(withIdentifier: "OptionDateComboView")as! OptionDateComboView
        controller.origin = origen
        controller.providesPresentationContextTransitionStyle = true
        controller.definesPresentationContext = true
        controller.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            viewC.present(controller, animated: false, completion: {})

    }

    public static func OptionComboStreet(data: [Any] = [], id: String = "", header: TypeOfHeader, defaultOption: String? = nil, delegate: OptionsAlertViewDelegate, viewC: UIViewController) {

            let viewAlert: UIStoryboard = UIStoryboard(name: "AlertSelectView", bundle: nil)
        let controller = viewAlert.instantiateViewController(withIdentifier: "AlertSelectView")as! AlertSelectView

        controller.typeOfAlert = header
        controller.dataPicker = data
        controller.id = id
        controller.defaultOption = defaultOption
        controller.optionsAlertViewDelegate = delegate

        controller.providesPresentationContextTransitionStyle = true
        controller.definesPresentationContext = true
        controller.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            viewC.present(controller, animated: false, completion: {})

    }

    public static func AlertSuccesStreet(typeAlert: TypeAlert, dismissParent: Bool = false, viewC: UIViewController) {

        let viewAlert: UIStoryboard = UIStoryboard(name: "AlertSuccessView", bundle: nil)
    let controller = viewAlert.instantiateViewController(withIdentifier: "AlertSuccessView")as! AlertSuccessView
        controller.typeAlert = typeAlert
        controller.providesPresentationContextTransitionStyle = true
        controller.definesPresentationContext = true
        controller.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        viewC.present(controller, animated: false, completion: {})

        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double((Int64)(2.0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {() -> Void in

           controller.dismiss(animated: true, completion: {() -> Void in
               if dismissParent {
                   viewC.dismiss(animated: false, completion: nil)
               }
        })
        })
    }

    public static func DownLoadVideo(nameVideo: String, URLVideo: String, ViewC: UIViewController) {

        let urlV = URL(string: URLVideo)

        DispatchQueue.global(qos: .background).async {
            if let urlData = NSData(contentsOf: urlV!) {
                let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
                let filePath="\(documentsPath)/\(nameVideo).mp4"
                DispatchQueue.main.async {
                    urlData.write(toFile: filePath, atomically: true)
                    PHPhotoLibrary.shared().performChanges({
                        PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: URL(fileURLWithPath: filePath))
                    }) { completed, _ in
                        if completed {
                            Constants.Alert(title: "", body: dialogs.Success.Download, type: "success", viewC: ViewC)
                        }
                    }
                }
            }
        }
    }

    public static func PlayVideo(URLVIdeo: String, ViewC: UIViewController) {

    }

    public static func showSpinnerView(title: String, view: UIView) {
        spinnerView = WaitingView(frame: CGRect(
            x: 0,
            y: 0,
            width: view.bounds.width,
            height: view.bounds.height
        ))
        spinnerView.titleLabel!.text = title
        view.addSubview(spinnerView)
    }

    public static func removeSpinnerView() {
        spinnerView.removeFromSuperview()
    }

    public static func showErrorGenericMessage(viewCPresent: UIViewController) {
        Self.Alert(title: "Error",
                        body: "Ha ocurrido un error inesperado",
                        type: type.Error,
                        viewC: viewCPresent
        )
    }
}
