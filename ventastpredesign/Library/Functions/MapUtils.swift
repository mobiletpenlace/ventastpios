//
//  MapUtils.swift
//  ventastp
//
//  Created by Marisol Huerta O. on 20/08/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class MapUtils {

    class func getCoordinate(latitud: Double, longitud: Double) -> CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: latitud, longitude: longitud)
    }

    class func addAnotation(coordinate: CLLocationCoordinate2D, title: String = "", mapView: MKMapView) {
        var pointAnnotation: MKPointAnnotation!
        var pinAnnotationView: MKPinAnnotationView!
        pointAnnotation = MKPointAnnotation()
        pointAnnotation.coordinate = coordinate
        pointAnnotation.title = title
        pinAnnotationView = MKPinAnnotationView(annotation: pointAnnotation, reuseIdentifier: nil)
        mapView.centerCoordinate = pointAnnotation.coordinate
        mapView.addAnnotation(pinAnnotationView.annotation!)
    }

    class func removeAnnotations(mapView: MKMapView) {
        if mapView.annotations.count > 0 {
            mapView.removeAnnotations(mapView.annotations)
        }
    }

    class func addRegion(coordinate: CLLocationCoordinate2D, spanValue: Double = 0.015, mapView: MKMapView) {
        let span = MKCoordinateSpan(latitudeDelta: spanValue, longitudeDelta: spanValue)
        let region = MKCoordinateRegion(center: coordinate, span: span)
        mapView.setRegion(region, animated: true)
    }

//    class func zoomAllAnnotationss(mapView: MKMapView){
//        var zoomRect            = MKMapRect
//        for annotation in mapView.annotations {
//            let annotationPoint = MKMapPointForCoordinate(annotation.coordinate)
//            let pointRect       = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.01, 0.01);
//            zoomRect            = zoomRect.MKMapRectUnion(pointRect)
//        }
//        mapView.setVisibleMapRect(zoomRect, edgePadding: UIEdgeInsets(top: 100, left: 100, bottom: 100, right: 100), animated: true)
//    }

    class func addRegionMeters(coordinate: CLLocationCoordinate2D, meters: Double = 1400.0, mapView: MKMapView) {
        let region = MKCoordinateRegionMakeWithDistance(coordinate, meters, meters)
        mapView.setRegion(region, animated: true)
    }

    class func addCircleRadio(coordinate: CLLocationCoordinate2D, radius: Double = 400, mapView: MKMapView) {
        var circle: MKCircle!
        circle = MKCircle(center: coordinate, radius: 400)
        mapView.add(circle)
    }

    class func showRouteTwoPoint(source: CLLocationCoordinate2D, destination: CLLocationCoordinate2D, mapView: MKMapView) {

        addAnotation(coordinate: source, mapView: mapView)
        addAnotation(coordinate: destination, mapView: mapView)

        let request = MKDirections.Request()
        request.source = MKMapItem(placemark: MKPlacemark(coordinate: source, addressDictionary: nil))

        request.destination = MKMapItem(placemark: MKPlacemark(coordinate: destination, addressDictionary: nil))
        request.requestsAlternateRoutes = true
        request.transportType = .automobile

        let directions = MKDirections(request: request)

        directions.calculate { [unowned mapView] response, _ in
            guard let unwrappedResponse = response else { return }

            if unwrappedResponse.routes.count > 0 {
                mapView.add(unwrappedResponse.routes[0].polyline)
                mapView.setVisibleMapRect(unwrappedResponse.routes[0].polyline.boundingMapRect, animated: false)
            }
        }
    }

}
