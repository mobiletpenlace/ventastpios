//
//  ExtensionsView.swift
//  StreetTpIOS
//
//  Created by Juan Reynaldo Escobar Miron on 9/25/19.
//  Copyright © 2019 Juan Reynaldo Escobar Miron. All rights reserved.
//

import UIKit
import Foundation
import MaterialComponents.MaterialTextControls_FilledTextAreas
import MaterialComponents.MaterialTextControls_FilledTextFields

@IBDesignable

extension UILabel {
    func halfTextColorChange (fullText: String, changeText: String ) {
        let strNumber: NSString = fullText as NSString
        let range = (strNumber).range(of: changeText)
        let attribute = NSMutableAttributedString.init(string: fullText)
        attribute.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor(named: "Base_rede_ligth_gray_titles"), range: range)
        self.attributedText = attribute
    }
}

extension UIView {
    public static var primerColor: UIColor!

    @IBInspectable
    /// Border color of view; also inspectable from Storyboard.
    public var borderColor: UIColor? {
        get {
            guard let color = layer.borderColor else {
                return nil
            }
            return UIColor(cgColor: color)
        }
        set {
            guard let color = newValue else {
                layer.borderColor = nil
                return
            }
            layer.borderColor = color.cgColor
        }
    }

    @IBInspectable
    /// Border width of view; also inspectable from Storyboard.
    public var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }

    @IBInspectable
    // Corner radious of view
    public var radius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }

    @IBInspectable
    // Corner radious of view
    public var CornerLeftView: Bool {
        get {
            return true
        }
        set {
            if newValue {
                layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
            }

        }
    }

    @IBInspectable
    // Corner radious of view
    public var CornerBootomView: Bool {
        get {
            return true
        }
        set {
            if newValue {
                layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner ]
            }

        }
    }

    @IBInspectable
    // Corner radious of view
    public var CornerTopView: Bool {
        get {
            return true
        }
        set {
            if newValue {
                layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner ]
            }

        }
    }

    @IBInspectable
        // Corner radious of view
    var coorner: CACornerMask {
            get {
                return layer.maskedCorners
                   }
                   set {
                       layer.maskedCorners = newValue
                   }
        }
//    

    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }

    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }

    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }

    @IBInspectable
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }

}

// MARK: - Properties

public extension UIView {

    /// Size of view.
    var size: CGSize {
        get {
            return self.frame.size
        }
        set {
            self.width = newValue.width
            self.height = newValue.height
        }
    }

    /// Width of view.
    var width: CGFloat {
        get {
            return self.frame.size.width
        }
        set {
            self.frame.size.width = newValue
        }
    }

    /// Height of view.
    var height: CGFloat {
        get {
            return self.frame.size.height
        }
        set {
            self.frame.size.height = newValue
        }
    }
}

extension MDCFilledTextField {

    func setFloatingLabel(message: String) {
        self.label.text = message
    }

    @IBInspectable
    public var mdcBackgroundColor: UIColor? {
        get {
            guard let color = layer.borderColor else {
                return nil
            }
            return UIColor(cgColor: color)
        }
        set {
            guard let color = newValue else {
                layer.borderColor = nil
                return
            }
            self.setFilledTP(colorBackground: color)
        }

    }

    @IBInspectable
    public var mdcTextColor: UIColor? {
        get {
            guard let color = layer.borderColor else {
                return nil
            }
            return UIColor(cgColor: color)
        }
        set {
            guard let color = newValue else {
                layer.borderColor = nil
                return
            }
            self.setTextColor(color, for: .normal)
            self.setTextColor(color, for: .editing)
        }

    }

    @IBInspectable
    public var mcdLabelColor: UIColor? {
        get {
            guard let color = layer.borderColor else {
                return nil
            }
            return UIColor(cgColor: color)
        }
        set {
            guard let color = newValue else {
                layer.borderColor = nil
                return
            }
            self.setNormalLabelColor(color, for: .normal)
            self.setNormalLabelColor(color, for: .editing)
        }
    }

    @IBInspectable
    public var mcdFloatingColor: UIColor? {
        get {
            guard let color = layer.borderColor else {
                return nil
            }
            return UIColor(cgColor: color)
        }
        set {
            guard let color = newValue else {
                layer.borderColor = nil
                return
            }
            self.setFloatingLabelColor(color, for: .normal)
            self.setFloatingLabelColor(color, for: .editing)
        }
    }

    func setColorsDidBeingEditing(textColor: UIColor, labelColor: UIColor, floatingColor: UIColor) {
        self.setTextColor(textColor, for: .normal)
        self.setTextColor(textColor, for: .editing)
        self.setNormalLabelColor(labelColor, for: .normal)
        self.setNormalLabelColor(labelColor, for: .editing)
        self.setFloatingLabelColor(floatingColor, for: .normal)
        self.setFloatingLabelColor(floatingColor, for: .editing)
    }

    func normalTheme(type: Int) {
        if type == 1 {
            self.leadingAssistiveLabel.isHidden = true
        } else {
            self.leadingAssistiveLabel.isHidden = false
            self.setLeadingAssistiveLabelColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .normal)
            self.leadingAssistiveLabel.text = "*Obligatorio"
        }

        self.setFilledBackgroundColor(UIColor.clear, for: .normal)
        self.setFilledBackgroundColor(UIColor.clear, for: .editing)
        self.setTextColor(UIColor(named: "Base_rede_ligth_gray_titles")!, for: .normal)
        self.setNormalLabelColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .normal)
        self.setFloatingLabelColor(UIColor(named: "Base_rede_blue_textfields_wait")!, for: .normal)

        self.setUnderlineColor(UIColor(named: "Base_rede_ligth_gray_titles")!, for: .normal)
        self.setUnderlineColor(UIColor(named: "Base_rede_ligth_gray_titles")!, for: .editing)

//        self.leadingAssistiveLabel.isHidden = true
//        self.trailingAssistiveLabel.isHidden = true
    }

    func errorTheme(message: String) {
        self.setTextColor(UIColor.red, for: .normal)
        self.setNormalLabelColor(UIColor.red, for: .normal)
        self.setFloatingLabelColor(UIColor.red, for: .normal)
        self.setLeadingAssistiveLabelColor(UIColor.red, for: .normal)
        self.leadingAssistiveLabel.text = message
    }

    func clearColorFill() {
        self.setFilledBackgroundColor(UIColor.clear, for: .normal)
        self.setFilledBackgroundColor(UIColor.clear, for: .disabled)
        self.setFilledBackgroundColor(UIColor.clear, for: .editing)
    }
}
extension UIView {

    func addCheck(viewParent: UIView, button: UIButton) {
        let checkImage = UIImage(named: "Image_rede_blue_check.png")
        let myImageView: UIImageView = UIImageView()
            myImageView.contentMode = UIView.ContentMode.scaleAspectFit
            myImageView.frame.size.width = 20
            myImageView.frame.size.height = 20
            myImageView.frame.origin.y = viewParent.frame.origin.y - 5
            myImageView.frame.origin.x = button.frame.origin.x + button.width - 15
            myImageView.image = checkImage
            myImageView.tag = 4
            viewParent.addSubview(myImageView)
            button.borderColor = UIColor(named: "Base_rede_button_dark")
            button.borderWidth = 3
            button.isSelected = true
    }

    func removeSubview(view: UIView) {
        if let viewWithTag = view.viewWithTag(4) {
            viewWithTag.removeFromSuperview()
        } else {
        }
    }

    func showToast(controller: UIViewController, message: String, seconds: Double) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.view.backgroundColor = UIColor.black
        alert.view.alpha = 0.6
        alert.view.layer.cornerRadius = 15

        controller.present(alert, animated: true)

        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds) {
            alert.dismiss(animated: true)
        }
    }

    // OUTPUT 2
    func setShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }

}

extension UIButton {
    func deaselect() {
        self.isSelected = false
        self.borderColor = UIColor(named: "Base_rede_semiclear_gray")
        self.borderWidth = 2
    }

    func select() {
        self.isSelected = true
        self.borderColor = UIColor(named: "Base_rede_button_dark")
        self.borderWidth = 3
    }

}

extension Double {

    var asLocaleCurrency: String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.groupingSeparator = ","
        formatter.groupingSize = 3
        formatter.usesGroupingSeparator = true
        formatter.decimalSeparator = "."
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        formatter.locale = Locale.current
        return  "$ " + formatter.string(from: self as NSNumber)!
    }

}

extension UIViewController {
    func showToast2(message: String, font: UIFont, time: Double) {
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 150, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.font = font
        toastLabel.textAlignment = .center
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: time, delay: 0.1, options: .curveEaseOut, animations: {
             toastLabel.alpha = 0.0
        }, completion: {(_) in
            toastLabel.removeFromSuperview()
        })
    }
}
