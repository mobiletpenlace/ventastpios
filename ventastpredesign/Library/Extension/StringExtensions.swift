//
//  StringExtensions.swift
//  VentasTotalPlayiOS
//
//  Created by Jorge Hdez Villa on 23/01/17.
//  Copyright © 2017 TotalPlay. All rights reserved.
//

import UIKit

class StringExtensions: NSObject {

}

extension String {

    var length: Int {
        return self.count
    }

    subscript (i: Int) -> String {
        return self[Range(i ..< i + 1)]
    }

    func substring(from: Int) -> String {
        return self[Range(min(from, length) ..< length)]
    }

    func substring(to: Int) -> String {
        return self[Range(0 ..< max(0, to))]
    }

    subscript (r: Range<Int>) -> String {
        let range = Range(uncheckedBounds: (lower: max(0, min(length, r.lowerBound)),
                                            upper: min(length, max(0, r.upperBound))))
        let start = index(startIndex, offsetBy: range.lowerBound)
        let end = index(start, offsetBy: range.upperBound - range.lowerBound)
        return self[Range(uncheckedBounds: (0, 1))]
    }

    var boolValue: Bool {
        return NSString(string: self).boolValue
    }

    var doubleValue: Double {
        return NSString(string: self).doubleValue
    }

    var intValue: Int {
        return Int(NSString(string: self).intValue)
    }

    func contains(string: String) -> Bool {
        return (self.range(of: string) != nil)
    }

    func replace(search: String, replace: String) -> String {
        return self.replacingOccurrences(of: search, with: replace)
    }

    func compareIgnoreCase(string: String) -> Bool {
        return self.caseInsensitiveCompare(string) == ComparisonResult.orderedSame
    }

    func clean() -> String {
        return self.folding(options: .diacriticInsensitive, locale: .current)
    }

    func split(character: String) -> [String] {
        return self.components(separatedBy: character)
    }
}

extension String {

    func removeAccents() -> String {
        return self.folding(options: .diacriticInsensitive, locale: .current)
    }

    func removeBackslash() -> String {
        return self.replacingOccurrences(of: "\"", with: "")
    }

    func removeExtraSpaces() -> String {
        return self.replacingOccurrences(of: "[\\s\n]+", with: " ", options: .regularExpression, range: nil).trimmingCharacters(in: .whitespacesAndNewlines)
    }

    func removeSpaces() -> String {
        return self.filter {!$0.isWhitespace}
    }

    func isValidEmail() -> Bool {
            // here, `try!` will always succeed because the pattern is valid
            let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
            return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
        }

    func matches(regex: String) -> Bool {
        return NSPredicate(format: "SELF MATCHES %@", regex).evaluate(with: self)
    }

    var isNumeric: Bool {
        return !isEmpty && rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
    }

}

extension NSLayoutConstraint {
    /**
     Change multiplier constraint
     
     - parameter multiplier: CGFloat
     - returns: NSLayoutConstraint
     */
    func setMultiplier(multiplier: CGFloat) -> NSLayoutConstraint {

        NSLayoutConstraint.deactivate([self])

        let newConstraint = NSLayoutConstraint(
            item: firstItem as Any,
            attribute: firstAttribute,
            relatedBy: relation,
            toItem: secondItem,
            attribute: secondAttribute,
            multiplier: multiplier,
            constant: constant)

        newConstraint.priority = priority
        newConstraint.shouldBeArchived = self.shouldBeArchived
        newConstraint.identifier = self.identifier

        NSLayoutConstraint.activate([newConstraint])
        return newConstraint
    }
}
