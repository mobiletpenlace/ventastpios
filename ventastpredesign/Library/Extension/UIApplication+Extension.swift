//
//  UIApplication+Extension.swift
//  ModuloPagoIOS
//
//  Created by Marisol Huerta O. on 02/09/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import Foundation
import UIKit

extension UIApplication {

    class func topViewController(viewController: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = viewController as? UINavigationController {
            return topViewController(viewController: nav.visibleViewController)
        }
        if let tab = viewController as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(viewController: selected)
            }
        }
        if let presented = viewController?.presentedViewController {
            return topViewController(viewController: presented)
        }
        return viewController
    }
}
