//
//  ArrayExtension.swift
//  ventastp
//
//  Created by Armando Isais Olguin Cabrera on 05/10/21.
//  Copyright © 2021 TotalPlay. All rights reserved.
//

import UIKit
import Foundation

extension Array where Element: Hashable {
    func removingDuplicates() -> [Element] {
        var addedDict = [Element: Bool]()

        return filter {
            addedDict.updateValue(true, forKey: $0) == nil
        }
    }

    mutating func removeDuplicates() {
        self = self.removingDuplicates()
    }
}
