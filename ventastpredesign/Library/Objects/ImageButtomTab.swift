//
//  ImageButtomTab.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 10/11/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import Foundation
import UIKit

class ImageButtonTab {
    var Button: UIButton!
    var Line: UIView!
    var image: UIImageView!
    var controller: UIViewController!
    var imageOn: UIImage!
    var imageOff: UIImage!
    var viewAlert: UIStoryboard!
    var mController: UIViewController
    var Title: String

    init(_ button: UIButton, _ line: UIView, _ nombre: String, _ imageOn: UIImage, _ imageOff: UIImage, _ title: String, _ imageView: UIImageView) {
        self.Button = button
        self.Line = line
        self.image = imageView
        viewAlert = UIStoryboard(name: nombre, bundle: nil)
        mController = viewAlert!.instantiateViewController(withIdentifier: nombre)
        self.controller = mController
        self.imageOn = imageOn
        self.imageOff = imageOff
        self.Title = title
    }

    func on() {
        image.image = imageOn
        Line.isHidden = false
    }

    func off() {
        image.image = imageOff
        Line.isHidden = true
    }
}
