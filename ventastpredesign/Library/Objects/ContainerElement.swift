//
//  ContainerElement.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 09/01/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import Foundation
import UIKit

class ContainerElement {
    var position: Int!
    var referenceStoryboard: UIStoryboard!
    var referenceViewController: UIViewController!

    init(_ storyboard: String, _ storyboardId: String, _ position: Int) {
        self.position = position
        self.referenceStoryboard = UIStoryboard(name: storyboard, bundle: nil)
        self.referenceViewController = referenceStoryboard!.instantiateViewController(withIdentifier: storyboardId)
    }

}
