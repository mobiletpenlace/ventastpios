//
//  DocumentsManagerConstants.swift
//  SalesCloud
//
//  Created by Axl Estevez on 27/05/24.
//  Copyright © 2024 TotalPlay. All rights reserved.
//

import Foundation

public enum TPDocuments: Int {
    case ID_CARD_FRONT      = 1
    case ID_CARD_BACK       = 2
    case RESIDENCY_PROOF    = 3
    case RFC_DOCUMENT       = 4
    case BANK_DOCUMENT      = 5
    // MARK: Afiliacion de comercios
    case ACTA_CONSTITUTIVA          = 6
    case COMPROBANTE_DE_DOMICILIO   = 7
    case RFC                        = 8
    case PODER_REPRESENTANTE_LEGAL  = 9
    case ID_REPRESENTANTE_LEGAL     = 10
    case ESTADO_DE_CUENTA           = 11
}

public enum TPDocumentsValues {
    static let id_card_front    = 1
    static let id_card_back     = 2
    static let residency_prood  = 3
    static let rfc_document     = 4
    static let bank_document    = 5
    //MARK: Afiliación de comercios
    static let acta_constitutiva            = 6
    static let comprobante_de_domicilio     = 7
    static let RFC                          = 8
    static let poder_representante_legal    = 9
    static let id_representante_legal       = 10
    static let estado_de_cuenta             = 11
}
