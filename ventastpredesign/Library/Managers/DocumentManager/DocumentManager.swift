//
//  DocumentManager.swift
//  SalesCloud
//
//  Created by Axl Estevez on 14/05/24.
//  Copyright © 2024 TotalPlay. All rights reserved.
//

import UIKit

public class DocumentManager {

    static let sharedInstance = DocumentManager()
    var typeDocumentSourceView: TPSourceTypeDocumentView?

    // MARK: String Constants utils
    let mediaSourceManager  = ViewsManagerConstants.mediaSourceViewController
    let titleSpinner        = NSLocalizedString("Validando Información", comment: "")

    func showMediaSourceView(delegate: TPMediaDocumentsSourceProtocol, viewC: UIViewController) {
        let view = UIStoryboard(name: mediaSourceManager, bundle: nil)
        let controller = view.instantiateViewController(withIdentifier: mediaSourceManager) as! MediaSourceViewController
        controller.delegate = delegate
        controller.modalPresentationStyle = .overFullScreen
        viewC.present(controller, animated: true)
    }

    func showTypeSourceMedia(view: UIView, delegate: TPSourceTypeDocumentProtocol) {
        typeDocumentSourceView = TPSourceTypeDocumentView(
            frame: CGRect(
                x: 0,
                y: 0,
                width: view.bounds.width,
                height: view.bounds.height
            )
        )
        typeDocumentSourceView!.manager = delegate
        view.addSubview(typeDocumentSourceView!)
    }

    func hideTypeSourceMedia() {
        typeDocumentSourceView!.removeFromSuperview()
    }

    func openFinder(view: UIViewController, pickerDelegate: UIDocumentPickerDelegate) {
        Constants.showSpinnerView(title: titleSpinner, view: view.view)
        let documentPicker = UIDocumentPickerViewController(documentTypes: ["public.content"], in: .import)
        documentPicker.delegate = pickerDelegate
        view.present(documentPicker, animated: true)
    }
}
