//
//  RetrifitManager.swift
//  EstrategiaDigital
//
//  Created by Jorge Hdez Villa on 09/08/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
// 0101220657

import UIKit
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

open  class BaseRetrofitManager<Res: BaseResponse>: NSObject {

    open var requestUrl: String
    open weak var delegate: AlamofireResponseDelegate!
    open var request: Alamofire.Request?
    open var url: String = ""

    public init(requestUrl: String, delegate: AlamofireResponseDelegate) {
        self.delegate = delegate
        self.requestUrl = requestUrl
    }

    open func getJsonDebug(requestUrl: String) -> String {
        return ""
    }

    open func getDebugEnabled() -> Bool {
        return false
    }

    open var Manager: Alamofire.SessionManager = {
        // Create the server trust policies
        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            "https://189.203.181.233": .disableEvaluation,
            "189.203.181.233": .disableEvaluation
        ]

        // Create custom manager
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        let manager = Alamofire.SessionManager(
            configuration: URLSessionConfiguration.default,
            serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
        )

        return manager
    }()

    open func request(requestModel: BaseRequest) {

        if getDebugEnabled() {
            let json: String = getJsonDebug(requestUrl: self.requestUrl)
            if json != "" {
                let response = Res(JSONString: json)
                self.delegate.onSuccessLoadResponse(requestUrl: self.requestUrl, response: response!)
            }
            return
        }

        if ConnectionUtils.isConnectedToNetwork() {
            delegate.onRequestWs()

            print("///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////")
            print("Logg --> \(AppDelegate.API_TOTAL + requestUrl)")

            let params  = Mapper().toJSON(requestModel)
            print("///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////")

            print("Logg -->  Request = \(String(describing: params))")
            print("///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////")
            if self.requestUrl == "/ventasmovil/Cobertura" {
                url = "https://mss.totalplay.com.mx/ventasmovil/Cobertura"
            } else if self.requestUrl == "/ventasmovil/LoginUsuarioVentasAPP" {
                url = "https://msstest.totalplay.com.mx:444/ventasmovil/LoginUsuarioVentasAPP"
            } else {
                url = AppDelegate.API_TOTAL + self.requestUrl
            }
            print("url: " + url)
            if self.requestUrl == "/ocr/obtener_datos" {
                url = "https://ine.nubarium.com/ocr/obtener_datos"
                request = Manager.request(url, method: HTTPMethod.post, parameters: params, encoding: JSONEncoding.default, headers: ApiDefinition.headersINE).responseObject {(response: DataResponse<Res>) in
                    switch response.result {
                    case .success:
                         print("Logg --> Response\(Mapper().toJSON(response.result.value! as Res))")
                        self.delegate.onSuccessLoadResponse(requestUrl: self.requestUrl, response: response.result.value! as Res)
                    case .failure:
                        self.delegate.onErrorLoadResponse(requestUrl: self.requestUrl, messageError: "")
                    }
                }
            } else {
                request = Manager.request(url, method: HTTPMethod.post, parameters: params, encoding: JSONEncoding.default, headers: ApiDefinition.CUSTOM_HEADERS).responseObject {(response: DataResponse<Res>) in
                    switch response.result {
                    case .success:
                         print("Logg --> Response\(Mapper().toJSON(response.result.value! as Res))")
                        self.delegate.onSuccessLoadResponse(requestUrl: self.requestUrl, response: response.result.value! as Res)
                    case .failure:
                        self.delegate.onErrorLoadResponse(requestUrl: self.requestUrl, messageError: "")
                    }
                }
            }

        } else {
            delegate.onErrorConnection()
        }
    }

    open func requestRealm(requestModel: BaseRealmRequest) {
        if getDebugEnabled() {
            let json: String = getJsonDebug(requestUrl: self.requestUrl)
            if json != "" {
                let response = Res(JSONString: json)
                self.delegate.onSuccessLoadResponse(requestUrl: self.requestUrl, response: response!)
            }
            return
        }
        if ConnectionUtils.isConnectedToNetwork() {
            delegate.onRequestWs()

            print("///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////")
            print("Logg --> \(AppDelegate.API_TOTAL + requestUrl)")

            let params  = Mapper().toJSON(requestModel)
            print("///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////")

            print("Logg -->  Request = \(String(describing: params))")
            print("///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////")
            request = Manager.request(AppDelegate.API_TOTAL + self.requestUrl, method: HTTPMethod.post, parameters: params, encoding: JSONEncoding.default, headers: ApiDefinition.CUSTOM_HEADERS).responseObject {(response: DataResponse<Res>) in
                switch response.result {
                case .success:
                    print("Logg --> Response\(Mapper().toJSON(response.result.value! as Res))")
                    self.delegate.onSuccessLoadResponse(requestUrl: self.requestUrl, response: response.result.value! as Res)
                case .failure:
                    self.delegate.onErrorLoadResponse(requestUrl: self.requestUrl, messageError: "")
                }

            }
        } else {
            delegate.onErrorConnection()
        }
    }

}

public protocol AlamofireResponseDelegate: NSObjectProtocol {

    func onRequestWs()

    func onSuccessLoadResponse(requestUrl: String, response: BaseResponse)

    func onSuccessLoadResponse(requestUrl: String, response: [BaseResponse])

    func onErrorLoadResponse(requestUrl: String, messageError: String)

    func onErrorConnection()

}
