//
//  BaseViewModel.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 20/02/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

protocol BaseViewModelObserver: NSObjectProtocol {
    func onRequestWs()

    func onSuccessLoadResponse(requestUrl: String, response: BaseResponse)

    func onSuccessLoadResponse(requestUrl: String, response: [BaseResponse])

    func onErrorLoadResponse(requestUrl: String, messageError: String)

    func onErrorConnection()
}

class BaseViewModel: NSObject, BaseViewModelObserver {

    weak var view: BaseVentasView!

    init(view: BaseVentasView) {
        self.view = view
    }

    func onRequestWs() {
        if !(TPDataDocumentsShared.shared.isRedesingFlow ?? false) {
            view.showLoading(message: "")
        }
    }

    func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
    }

    func onSuccessLoadResponse(requestUrl: String, response: [BaseResponse]) {
    }

    func onErrorLoadResponse(requestUrl: String, messageError: String) {
        view.hideLoading()
    }

    func onErrorConnection() {
        view.hideLoading()
    }

}
