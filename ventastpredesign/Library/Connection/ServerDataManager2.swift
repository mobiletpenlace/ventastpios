//
//  ServerDataManager.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 16/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import SwiftJWT
import SwiftyJSON

class ServerDataManager2<Res: BaseResponse> {

    open var requestUrl: String!
    open var delegate: AlamofireResponseDelegate!
    // open var headersSalesforce: Bool = false
    open var headerType: headers!

    public enum headers {
        case headersSalesforce
        case headersMiddle
        case headersFfmap
        case headersReportes
        case headersStrapi
        case headersOkta
        case headersSystemsTP
        case headersSystemToken
        case headersCariaiToken
        case headersCloudToken
        case headersCloudEvents
        case headersSendPDF
        case getHeadersFlyersSystemsTP
        case getQR
        case getFlyer
        case sendFlyer
        case headersApiService
    }

    open var request: Alamofire.Request?
    let jsonEncoder = JSONEncoder()
    let jsonDecoder = JSONDecoder()
    var urlLoginSalesforce = AppDelegate.API_SALESFORCE +  ApiDefinition.API_TOKEN_SALESFORCE
    var urlLoginSystemsTP = AppDelegate.API_SYSTEMSTP + ApiDefinition.API_SYSTEMASTP_TOKEN
    var urlLoginCloud = AppDelegate.API_CLOUD_TOKEN
    // static var TOKEN = ""    let urlLoginSalesforce = "https://cs3.salesforce.com/services/oauth2/token"

    var manager: Alamofire.SessionManager = {
        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            "https://189.203.181.233": .disableEvaluation,
            "189.203.181.233": .disableEvaluation,
            "https://cs3.salesforce.com": .disableEvaluation,
            "cs3.salesforce.com": .disableEvaluation
        ]
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        let manager = Alamofire.SessionManager(
            configuration: URLSessionConfiguration.default,
            serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
        )
        return manager
    }()

    func objToJSON<A: Codable>(model: A) -> String {
        let jsonData = try? jsonEncoder.encode(model)
        let jsonString = String(data: jsonData!, encoding: .utf8)
        return jsonString ?? ""
    }

    func getHeaders() -> [String: String] {
        let headers = [
            "Accept": "application/json; charset=UTF-8",
            "Content-Type": "application/json; charset=UTF-8",
            "Authorization": "Bearer \(AppDelegate.TOKEN)"
        ]
        return headers
    }

    func getHeadersMiddle() -> [String: String] {
        let headers = [
            "Accept": "application/json; charset=UTF-8",
            "Content-Type": "application/json; charset=UTF-8",
            "Authorization": "Basic ZmZtYXBwOjRnZW5kNG1pM250bw=="
            // "Authorization": "Basic ZmZtYXBwOjRnZW5kNG1pM250bw== "
        ]
        return headers
    }

    func getHeadersFfmapp() -> [String: String] {
        let headers = [
            "Accept": "application/json",
            "Content-Type": "application/json",
            "Authorization": "Basic ZmZtYXBwOjRnZW5kNG1pM250bw== "
        ]
        return headers
    }

    func getHeadersToken() -> [String: String] {
        let headers = [
            "Accept": "application/json; charset=UTF-8",
            "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
            "Authorization": "Bearer 00DQ000000EeC6l!AQgAQE2n9BVsr_26jbsRuv2wMm_7Rqfxo.MRlcocahx2r3WtOITNK0VZgeiEC6wqv73EP7_C2Lhf3hwnUNm5tlXps0.pLkqm"
        ]
        return headers
    }

    func getHeadersTokenReportes() -> [String: String] {
        let headers = [
            "Accept": "*/*",
            "Content-Type": "application/json"
        ]
        return headers
    }

    func getHeadersSystemsTP() -> [String: String] {
        let model = AppDelegate.TOKEN_SYSTEM_USER + ":" + AppDelegate.TOKEN_SYSTEM_PASS
        let modelBase = model.toBase64()!
        let headers = [
            "Version": "1.0.0.0",
            "Authorization": "Basic " + modelBase
        ]
        return headers
    }

    func getHeadersAllStatesSystemsTP() -> [String: String] {
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(statics.TOKEN_SYSTEMS_TP)"
        ]
        return headers
    }

    func getHeadersFlyersSystemsTP() -> [String: String] {
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(statics.TOKEN_SYSTEMS_TP)"
            //            "version": "3.0.0.38"
        ]
        return headers
    }

    func getQR() -> [String: String] {
        let headers = [
            "Content-Type": "image/png",
            "Accept": "application/json",
            "Authorization": "Bearer \(statics.TOKEN_SYSTEMS_TP)",
            "version": "3.0.0.39"
        ]
        return headers
    }

    func getFlyer() -> [String: String] {
        let headers = [
            "Content-Type": "image/png",
            "Accept": "application/pdf",
            "Authorization": "Bearer \(statics.TOKEN_SYSTEMS_TP)",
            "version": "3.0.0.39"
        ]
        return headers
    }

    func getHeaderPaternidad() -> [String: String] {
        let headersApiService = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": "Bearer " +  statics.TOKEN_SERVICE_API,
            "id-Acceso": statics.ID_ACCESO
        ]
        return headersApiService
    }

    func getHeadersReportes() -> [String: String] {
        let headers = [
            "Accept": "*/*",
            "Authorization": "Bearer \(statics.TOKEN_REPORTES)"
        ]
        return headers
    }

    func getHeadersStrapi() -> [String: String] {
        let headers = [
            "Accept": "application/json; charset=UTF-8",
            "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
        ]
        return headers
    }

    func postFfmapp<A: Codable>(model: A, url path: String, closure: @escaping (Data?) -> Void) {
        post(model: model, url: path, headers: getHeadersFfmapp(), closure: closure)
    }

    func postMiddle<A: Codable>(model: A, url path: String, closure: @escaping (Data?) -> Void) {
        post(model: model, url: path, headers: getHeadersMiddle(), closure: closure)
    }

    func postToken(model: String, url path: String, closure: @escaping (Data?) -> Void) {
        postToken(model: model, url: path, headers: getHeadersToken(), closure: closure)
    }

    func postTokenSystemsTP(model: String, url path: String, closure: @escaping (Data?) -> Void) {
        postToken(model: model, url: path, headers: getHeadersSystemsTP(), closure: closure)
    }

    func postTokenSystemsTPGetAllStates(model: String, url path: String, closure: @escaping (Data?) -> Void) {
        postToken(model: model, url: path, headers: getHeadersAllStatesSystemsTP(), closure: closure)
    }

    func post<A: Codable>(model: A, url path: String, closure: @escaping (Data?) -> Void) {
        post(model: model, url: path, headers: getHeaders(), closure: closure)
    }

    func post(url path: String, closure: @escaping (Data?) -> Void) {
        post(url: path, headers: getHeaders(), closure: closure)
    }

    func postMiddle(url path: String, closure: @escaping (Data?) -> Void) {
        post(url: path, headers: getHeadersMiddle(), closure: closure)
    }
    func postFfmapp(url path: String, closure: @escaping (Data?) -> Void) {
        post(url: path, headers: getHeadersFfmapp(), closure: closure)
    }

    private func postToken(model: String, url path: String, headers: [String: String], closure: @escaping (Data?) -> Void) {
        if ConnectionUtils.isConnectedToNetwork() {
            Logger.blockSeparator()
            Logger.println(path)
            var request = createRequest(url: path, headers: headers)
            request.httpBody = model.data(using: .utf8)
            requestManager(request, closure: closure)
        } else {
            closure(nil)
            Logger.println("no hay conexion a internet.")
        }
    }

    private func post<A: Codable>(model: A, url path: String, headers: [String: String], closure: @escaping (Data?) -> Void) {
        if ConnectionUtils.isConnectedToNetwork() {
            do {
                Logger.blockSeparator()
                Logger.println(path)
                Logger.blockSeparator()
                Logger.println("request value: \(objToJSON(model: model))")
                var request = createRequest(url: path, headers: headers)
                request.httpBody = try jsonEncoder.encode(model)
                requestManager(request, closure: closure)
            } catch {
                Logger.println("--> hubo un error en el metodo post")
                closure(nil)
            }
        } else {
            closure(nil)
            Logger.println("no hay conexion a internet.")
        }
    }

    private func post(url path: String, headers: [String: String], closure: @escaping (Data?) -> Void) {
        if ConnectionUtils.isConnectedToNetwork() {
            let request = createRequest(url: path, headers: headers)
            requestManager(request, closure: closure)
        } else {
            closure(nil)
            Logger.println("no hay conexion a internet.")
        }
    }

    private func createRequest(url path: String, headers: [String: String]) -> URLRequest {
        var request = URLRequest(url: URL(string: path)!)
        request.httpMethod = HTTPMethod.post.rawValue
        for header in headers {
            request.setValue(header.value, forHTTPHeaderField: header.key)
        }
        return request
    }

    private func requestManager(_ request: URLRequest, closure: @escaping (Data?) -> Void) {
        manager.request(request).responseJSON {response in
            switch response.result {
            case .success:
                Logger.blockSeparator()
                Logger.println("response status:  \(String(describing: response.response?.statusCode))")
                closure(response.data)
            case .failure(let error):
                Logger.blockSeparator()
                Logger.println("error: \(error)")
                closure(nil)
            }
            if let result = response.result.value {
                if let JSON = result as? NSDictionary {
                    Logger.blockSeparator()
                    Logger.println("response: \(JSON)")
                }
            }
        }
    }

    func get(url path: String, parameters: Parameters?, closure: @escaping (Data?) -> Void) {
        manager.request(path, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            switch response.result {
            case .success:
                closure(response.data)
            case .failure(let error):
                Logger.println("failure get method service: \(error)")
            }
            //            if let result = response.result.value {
            //                let JSON = result as! NSDictionary
            //                print("json: \(JSON)")
            //            }
        }
    }

    open func requestStrappi() {
        if ConnectionUtils.isConnectedToNetwork() {
            delegate.onRequestWs()
            Logger.blockSeparator()
            let header = getHeaderToRequest()
            request = manager.request(self.requestUrl, method: HTTPMethod.get, encoding: JSONEncoding.default, headers: header).responseObject { [self] (response: DataResponse<Res>) in
                switch response.result {
                case .success:
                    let object = Mapper().toJSON(response.result.value! as Res)
                    let errorUnauthorized = object["error"] as? String ?? ""
                    if headerType == headers.headersReportes &&  errorUnauthorized != "" {
                        Logger.println("error: El token ha expirado")
                        self.getTokenReportes { (result, _) in
                            if result {
                                self.requestStrappi()
                            }
                        }
                    } else {
                        Logger.println("Response (\(self.requestUrl!)) = " + Mapper().toJSONString(response.result.value! as Res, prettyPrint: true)!)
                        Logger.blockSeparator()
                        self.delegate.onSuccessLoadResponse(requestUrl: self.requestUrl!, response: response.result.value! as Res)
                    }
                case .failure:
                    let statusCode = response.response?.statusCode ?? -1
                    if statusCode == 401 {
                        Logger.println("error")
                        self.getToken {(res, _) in
                            if res {
                                self.requestStrappi()
                            }
                        }
                    } else {
                        self.delegate.onErrorLoadResponse(requestUrl: self.requestUrl!, messageError: "status \(statusCode)")
                    }
                }
            }
        } else {
            delegate.onErrorConnection()
        }
    }

    func getHeaderToRequest() -> [String: String] {
        switch headerType {
        case .headersStrapi:
            return getHeadersStrapi()
        case .headersReportes:
            return getHeadersReportes()
        default:
            return [:]
        }
    }

    open func requestArrayLiteral() {
        if ConnectionUtils.isConnectedToNetwork() {
            delegate.onRequestWs()
            Logger.blockSeparator()
            let header = getHeaderToRequest()
            request = manager.request(self.requestUrl, method: HTTPMethod.get, encoding: JSONEncoding.default, headers: header).responseArray { [self](response: DataResponse<[Res]>) in
                switch response.result {
                case .success:
                    let object = response.result.value!
                    Logger.println("Response (\(self.requestUrl!)) = \(Mapper().toJSONString(response.result.value! as [Res], prettyPrint: true)!)")
                    self.delegate.onSuccessLoadResponse(requestUrl: self.requestUrl!, response: object as [Res])
                    Logger.blockSeparator()
                case .failure:
                    self.delegate.onErrorLoadResponse(requestUrl: self.requestUrl!, messageError: "status \(response.response?.statusCode ?? -1)")

                }
            }
        } else {
            delegate.onErrorConnection()
        }
    }
    open func request(requestModel: AlfaRequest) {
        petition(requestModel: requestModel)
    }

    func setValues(requestUrl: String, delegate: AlamofireResponseDelegate, headerType: headers) {
        // func setValues(requestUrl: String, delegate : AlamofireResponseDelegate, headersSalesforce : Bool){
        self.delegate = delegate
        self.requestUrl = requestUrl
        // self.headersSalesforce = headersSalesforce
        self.headerType = headerType
    }

    func petition(requestModel: AlfaRequest) {
        if ConnectionUtils.isConnectedToNetwork() {
            delegate.onRequestWs()
            Logger.blockSeparator()
            let params = Mapper().toJSON(requestModel)
            Logger.println(String("Request (\(requestUrl!))=" + Mapper().toJSONString(requestModel, prettyPrint: true)!))
            Logger.blockSeparator()
            // Switch Case para elegir tipo de header
            switch headerType {
            case .headersOkta:
                Alamofire.request(self.requestUrl, method: HTTPMethod.get, parameters: nil, encoding: JSONEncoding.default, headers: ApiDefinition.headersOkta).responseJSON { response in
                    print("RESPONSE JSON OKTA: \(response)")
                    switch response.result {
                    case .success(let value):
                        print("VALUE OKTA: \(value)")
                        let json = JSON(value)
                        let responseValue = OktaUserResponse(JSONString: json.rawString() ?? "")
                        // print("JSON: \(json)")
                        self.delegate.onSuccessLoadResponse(requestUrl: self.requestUrl, response: responseValue!)
                        // TODO: Add code after getting response data
                    case .failure(let error):
                        print("Error: \(error)")
                    }
                }

            case .headersSalesforce:
                request = manager.request(self.requestUrl, method: HTTPMethod.post, parameters: params, encoding: JSONEncoding.default, headers: ApiDefinition.headersToken).responseObject {(response: DataResponse<Res>) in
                    Logger.println("\(ApiDefinition.headersToken)")
                    self.processResponse(requestModel: requestModel, response: response)
                }
            case .headersMiddle:
                request = manager.request(self.requestUrl, method: HTTPMethod.post, parameters: params, encoding: JSONEncoding.default, headers: ApiDefinition.headersMiddle).responseObject {(response: DataResponse<Res>) in
                    Logger.println("\(ApiDefinition.headersMiddle)")
                    self.processResponse(requestModel: requestModel, response: response)
                }

            case .headersFfmap:
                request = manager.request(self.requestUrl, method: HTTPMethod.post, parameters: params, encoding: JSONEncoding.default, headers: ApiDefinition.headersFfmapp).responseObject {(response: DataResponse<Res>) in
                    Logger.println("\(ApiDefinition.headersFfmapp)")
                    self.processResponse(requestModel: requestModel, response: response)
                }

            case .headersSystemsTP:
                Alamofire.request(self.requestUrl, method: HTTPMethod.post, parameters: nil, encoding: JSONEncoding.default, headers: ApiDefinition.oAuthSystemToken).responseJSON { response in
                    Logger.blockSeparator()
                    Logger.println("Response \(String(describing: self.requestUrl))")
                    switch response.result {
                    case .success(let value):
                        Logger.println("\(value)")
                        Logger.blockSeparator()
                        let json = JSON(value)
                        let responseValue = TokenSystemResponse(JSONString: json.rawString() ?? "")
                        statics.TOKEN_SYSTEMS_TP =  responseValue?.mAccess_token ?? ""
                        print("Token SistemasTP: \(statics.TOKEN_SYSTEMS_TP)")
                        self.delegate.onSuccessLoadResponse(requestUrl: self.requestUrl, response: responseValue!)
                        // TODO: Add code after getting response data
                    case .failure(let error):
                        print("Error: \(error)")
                    }
                }
            case .headersSystemToken:
                Alamofire.request(self.requestUrl, method: HTTPMethod.get, parameters: nil, encoding: JSONEncoding.default, headers: ApiDefinition.headersSystemToken).responseJSON { response in
                    Logger.blockSeparator()
                    Logger.println("Response \(String(describing: self.requestUrl))")
                    print(ApiDefinition.headersSystemToken)
                    switch response.result {
                    case .success(let value):
                        Logger.println("\(value)")
                        Logger.blockSeparator()
                        let json = JSON(value)
                        let responseValue = GetAllStatesResponse(JSONString: json.rawString() ?? "")
                        self.delegate.onSuccessLoadResponse(requestUrl: self.requestUrl, response: responseValue!)
                        // TODO: Add code after getting response data
                    case .failure(let error):
                        print("Error: \(error)")
                    }
                }
            case .headersCariaiToken:
                Alamofire.request(self.requestUrl, method: HTTPMethod.post, parameters: params, encoding: JSONEncoding.default, headers: ApiDefinition.headersCariaiToken).responseJSON { response in
                    Logger.blockSeparator()
                    Logger.println("Response \(String(describing: self.requestUrl))")
                    switch response.result {
                    case .success(let value):
                        Logger.println("\(value)")
                        print(ApiDefinition.headersCariaiToken)
                        Logger.blockSeparator()
                        let json = JSON(value)
                        let responseValue = CariaiResponse(JSONString: json.rawString() ?? "")
                        statics.TOKEN_CARIAI =  responseValue?.mCariSec ?? ""
                        self.delegate.onSuccessLoadResponse(requestUrl: self.requestUrl, response: responseValue!)
                        // TODO: Add code after getting response data
                    case .failure(let error):
                        print("Error: \(error)")
                    }
                }

            case .headersCloudToken:
                request = manager.request(self.requestUrl, method: HTTPMethod.post, parameters: params, encoding: JSONEncoding.default, headers: ApiDefinition.headersCloudToken).responseObject {(response: DataResponse<Res>) in
                    Logger.println("\(ApiDefinition.headersCloudToken)")
                    self.processResponse(requestModel: requestModel, response: response)
                }

            case .headersApiService:
                request = manager.request(self.requestUrl, method: HTTPMethod.post, parameters: params, encoding: JSONEncoding.default, headers: getHeaderPaternidad()).responseObject {(response: DataResponse<Res>) in
                    Logger.println("\(self.getHeaderPaternidad())")
                    self.processResponse(requestModel: requestModel, response: response)
                }

            case .headersCloudEvents:
                request = manager.request(self.requestUrl, method: HTTPMethod.post, parameters: params, encoding: JSONEncoding.default, headers: ApiDefinition.headersCloudEvent).responseObject {(response: DataResponse<Res>) in
                    Logger.println("\(ApiDefinition.headersCloudToken)")
                    self.processResponse(requestModel: requestModel, response: response)
                }

            case .sendFlyer:
                Alamofire.request(self.requestUrl, method: HTTPMethod.post, parameters: params, encoding: JSONEncoding.default, headers: ApiDefinition.headersSendPDF).responseJSON { response in
                    Logger.blockSeparator()
                    Logger.println("Response \(String(describing: self.requestUrl))")
                    switch response.result {
                    case .success(let value):
                        Logger.println("\(value)")
                        print(ApiDefinition.headersCariaiToken)
                        Logger.blockSeparator()
                        let json = JSON(value)
                        let responseValue = CariaiResponse(JSONString: json.rawString() ?? "")
                        self.delegate.onSuccessLoadResponse(requestUrl: self.requestUrl, response: responseValue!)
                    case .failure(let error):
                        print("Error: \(error)")
                    }
                }

            case .headersSendPDF:
                Alamofire.request(self.requestUrl, method: HTTPMethod.post, parameters: params, encoding: JSONEncoding.default, headers: ApiDefinition.headersSendPDF).responseJSON { response in
                    Logger.blockSeparator()
                    Logger.println("Response \(String(describing: self.requestUrl))")
                    switch response.result {
                    case .success(let value):
                        Logger.println("\(value)")
                        print(ApiDefinition.headersSendPDF)
                        Logger.blockSeparator()
                        let json = JSON(value)
                        print(json)
                        dump(json)
                        let responseValue = SendHSMResponse(JSONString: json.rawString() ?? "")
                        self.delegate.onSuccessLoadResponse(requestUrl: self.requestUrl, response: responseValue!)
                        // TODO: Add code after getting response data
                    case .failure(let error):
                        print("Error: \(error)")
                    }
                }

            case .getHeadersFlyersSystemsTP:
                Alamofire.request(self.requestUrl, method: HTTPMethod.post, parameters: params, encoding: JSONEncoding.default, headers: ApiDefinition.headersFlyers).responseJSON { [weak self] response in
                    guard let self = self else {return}
                    switch response.result {
                    case .success(let value):

                        let json = JSON(value)
                        var flyersResponseArray: [GetFlyersResponse] = []
                        if json.count > 0 {
                            for i in 0...json.count-1 {
                                if let responseValue = GetFlyersResponse(JSONString: json[i].rawString() ?? "") {
                                    // Validate isVisible field
                                    flyersResponseArray.append(responseValue)
                                }
                            }
                        }
                        print("\(flyersResponseArray)")
                        Logger.blockSeparator()
                        self.delegate.onSuccessLoadResponse(requestUrl: self.requestUrl, response: flyersResponseArray)
                    case .failure(let error):
                        print(error)
                    }
                }
            case .getQR:
                Alamofire.request(self.requestUrl, method: HTTPMethod.get, parameters: nil, encoding: JSONEncoding.default, headers: ApiDefinition.headersGetQR).responseString {(response: DataResponse<String>) in
                    switch response.result {
                    case .success(let value):
                        let json = JSON(value)
                        print(response)
                        print(json)
                        dump(json)
                        let responseValue = QRResponse(JSONString: json.rawString() ?? "")
                        let imageQR = response.result.value!
                        statics.Qr = imageQR
                        self.delegate.onErrorConnection()

                    case .failure:
                        Logger.println("Error: \(response.response?.statusCode ?? -1)")
                        //                            callBack((false, ""))
                    }
                }

            default:
                delegate.onErrorConnection()
            }
        } else {
            delegate.onErrorConnection()
        }
    }

    func processResponse(requestModel: AlfaRequest, response: DataResponse<Res>) {
        switch response.result {
        case .success:

            let object = Mapper().toJSON(response.result.value! as Res)
            Logger.println("Response (\(requestUrl)) = " + Mapper().toJSONString(response.result.value! as Res, prettyPrint: true)!)
            if let error = object["error_message"] {
                Logger.println("error: El token ha expirado")
                self.getToken {(res, token) in
                    if res {
                        statics.TOKEN = token
                        self.request(requestModel: requestModel)
                    }
                }
            } else {
                self.delegate.onSuccessLoadResponse(requestUrl: self.requestUrl!, response: response.result.value! as Res)
                Logger.blockSeparator()
            }

        case .failure:
            let statusCode = response.response?.statusCode ?? -1
            if statusCode == 401 {
                self.getToken {(res, _) in
                    if res {
                        self.request(requestModel: requestModel)
                    }
                }
                Logger.println("error")
            } else {
                self.delegate.onErrorLoadResponse(requestUrl: self.requestUrl!, messageError: "status \(response.response?.statusCode ?? -1)")
            }
        }
    }

    func getToken(_ callBack: @escaping ((Bool, String)) -> Void) {
        var token: String = ""
        urlLoginSalesforce = AppDelegate.API_SALESFORCE +  ApiDefinition.API_TOKEN_SALESFORCE
        var model = AppDelegate.GRANT
        Logger.println(model)
        self.postToken(model: model, url: urlLoginSalesforce) { responseData in
            guard let data = responseData else {
                callBack((false, ""))
                return
            }
            do {
                let decoder = JSONDecoder()
                let res = try decoder.decode(TokenResponse.self, from: data)
                token = res.access_token ?? ""
                AppDelegate.TOKEN = token
                statics.TOKEN = token
                ApiDefinition.headersToken = [
                    "version": "3.0.0.10",
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + statics.TOKEN
                ]
                callBack((true, token))
            } catch let err {
                Logger.println("Fallo la consulta")
                callBack((false, ""))
            }
        }
    }

    func getTokenFlyers(_ callBack: @escaping ((Bool, String)) -> Void) {
        var token: String = ""
        let model = AppDelegate.TOKEN_SYSTEM_USER + ":" + AppDelegate.TOKEN_SYSTEM_PASS
        let modelBase = model.toBase64()
        Logger.println(model)
        Logger.println(modelBase!)
        self.postTokenSystemsTP(model: modelBase!, url: urlLoginSystemsTP) { responseData in
            guard let data = responseData else {
                callBack((false, ""))
                return
            }
            do {
                let decoder = JSONDecoder()
                let res = try decoder.decode(TokenResponse.self, from: data)
                token = res.access_token ?? ""
                AppDelegate.TOKEN = token
                statics.TOKEN = token
                ApiDefinition.headersToken = [
                    "version": "3.0.0.10",
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + statics.TOKEN
                ]
                callBack((true, token))
            } catch let err {
                Logger.println("Fallo la consulta")
                callBack((false, ""))
            }
        }
    }

    func getTokenCol(_ callBack: @escaping ((Bool, String)) -> Void) {
        var tokenSystemsTP: String = ""
        let model = "grant_type=password&client_id=3MVG9KI2HHAq33RxdFES5KXE7Sz8lu.FC0.Lk53x64FHLI9V1B11UAhBcz8bX584F0ugq1aNlMNMKvWS2Tung&client_secret=8884931604809527571&username=appcolombia@totalplay.com.mx&password=totalplay123456"
        self.postToken(model: model, url: urlLoginSalesforce) { responseData in
            guard let data = responseData else {
                callBack((false, ""))
                return
            }
            do {
                let decoder = JSONDecoder()
                let res = try decoder.decode(TokenResponse.self, from: data)
                tokenSystemsTP = res.access_token ?? ""
                AppDelegate.TOKEN = tokenSystemsTP
                statics.TOKEN = tokenSystemsTP
                callBack((true, tokenSystemsTP))
            } catch let err {
                Logger.println("Fallo la consulta")
                callBack((false, ""))
            }
        }
    }

    func getTokenReportes(_ callBack: @escaping ((Bool, String)) -> Void) {
        if ConnectionUtils.isConnectedToNetwork() {
            let tokenRequest = TokenReportRequest()
            let params = Mapper().toJSON(tokenRequest)
            let requestUrl = ApiDefinition.API_TOKEN_REPORTES

            request = manager.request(requestUrl, method: HTTPMethod.post, parameters: params, encoding: JSONEncoding.default, headers: getHeadersTokenReportes()).responseString {(response: DataResponse<String>) in

                switch response.result {
                case .success:
                    let token = response.result.value!
                    statics.TOKEN_REPORTES = token
                    Logger.println("Token para reportes generado: \(token)")
                    callBack((true, token))

                case .failure:
                    Logger.println("Error: \(response.response?.statusCode ?? -1)")
                    callBack((false, ""))
                }
            }
        } else {
            callBack((false, ""))
            Logger.println("no hay conexion a internet.")
        }
    }

    func getQR(_ callBack: @escaping ((Bool, String)) -> Void) {
        if ConnectionUtils.isConnectedToNetwork() {
            let tokenRequest = TokenReportRequest()
            let params = Mapper().toJSON(tokenRequest)
            let requestUrl = ApiDefinition.API_TOKEN_REPORTES

            request = manager.request(requestUrl, method: HTTPMethod.post, parameters: params, encoding: JSONEncoding.default, headers: getHeadersTokenReportes()).responseString {(response: DataResponse<String>) in

                switch response.result {
                case .success:
                    let token = response.result.value!
                    statics.TOKEN_REPORTES = token
                    Logger.println("Token para reportes generado: \(token)")
                    callBack((true, token))

                case .failure:
                    Logger.println("Error: \(response.response?.statusCode ?? -1)")
                    callBack((false, ""))
                }
            }
        } else {
            callBack((false, ""))
            Logger.println("no hay conexion a internet.")
        }
    }

}
