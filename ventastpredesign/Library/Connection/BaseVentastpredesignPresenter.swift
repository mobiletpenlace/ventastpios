//
//  BaseVentastpredesignPresenter.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 04/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import Alamofire

class BaseVentastpredesignPresenter: BasePresenter, AlamofireResponseDelegate {

    weak var view: BaseVentasView!

    init(view: BaseVentasView) {
        super.init(viewController: view)
        self.view = view
    }

    override func viewDidLoad() {
    }

    func onRequestWs() {
       // view.showLoading()
    }

    func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {

    }

    func onSuccessLoadResponse(requestUrl: String, response: [BaseResponse]) {
    }

    func onErrorLoadResponse(requestUrl: String, messageError: String) {
        view.hideLoading()
        if messageError == "" {
            Constants.Alert(title: "", body: dialogs.Error.InternService, type: type.Error, viewC: mViewController)
        } else {
            Constants.Alert(title: "Ha ocurrido un error", body: messageError, type: type.Normal, viewC: mViewController)
        }
    }

    func onErrorConnection() {
         view.hideLoading()
         Constants.Alert(title: "", body: dialogs.Error.ConnectionInternet, type: type.Error, viewC: mViewController)
    }
}
