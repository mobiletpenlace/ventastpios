//
//  BaseModel.swift
//  ventastpredesign
//
//  Created by Juan Reynaldo Escobar Miron on 20/02/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import Foundation

class BaseModel: NSObject, AlamofireResponseDelegate {

    weak var observerVM: BaseViewModelObserver?

    init(observerVM: BaseViewModelObserver) {
        self.observerVM = observerVM
    }

    func onRequestWs() {
        observerVM?.onRequestWs()

    }

    func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        observerVM?.onSuccessLoadResponse(requestUrl: requestUrl, response: response)
    }

    func onSuccessLoadResponse(requestUrl: String, response: [BaseResponse]) {
        observerVM?.onSuccessLoadResponse(requestUrl: requestUrl, response: response)
    }

    func onErrorLoadResponse(requestUrl: String, messageError: String) {
        observerVM?.onErrorLoadResponse(requestUrl: requestUrl, messageError: messageError)
    }

    func onErrorConnection() {
        observerVM?.onErrorConnection()
    }
}
