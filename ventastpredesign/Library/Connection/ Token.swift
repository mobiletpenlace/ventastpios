//
//   Token.swift
//  ventastpredesign
//
//  Created by apple on 1/7/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import Foundation
import Alamofire

class Token {
    var grant_type: String?
    var client_id: String?
    var client_secret: String?
    var username: String?
    var secretWord: String?

    required init() {
    }
}
