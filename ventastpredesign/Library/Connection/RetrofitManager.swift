//
//  RetrofitManager.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 04/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class RetrofitManager<Res: BaseResponse>: BaseRetrofitManager<Res> {

    override func getDebugEnabled() -> Bool {
        return false
    }
    override func getJsonDebug(requestUrl: String) -> String {
        let json = ""
        return json
    }

}
