//
//  ServerData.swift
//  VentasTPEmpresarial
//
//  Created by mhuertao on 09/03/22.
//  Copyright © 2022 TotalPlay. All rights reserved.
//

import UIKit

class ServerData<Res: BaseResponse>: BaseServerDataManager<Res> {

    override func getDebugEnabled() -> Bool {
        return false
    }

    override func getJsonDebug(requestUrl: String) -> String {
        var json = ""
        return json
    }

}
