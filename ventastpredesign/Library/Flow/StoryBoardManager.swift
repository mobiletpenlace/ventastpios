//
//  StoryBoardManager.swift
//  ventastpredesign
//
//  Created by Marisol Huerta Ortega on 12/04/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import Foundation

struct storyBoard {

    static let LoginProfile             = "LoginPerfilViewController"
    static let Profile                  = "ProfileViewController"
    static let MySell                   = "MySellViewController"
    static let Bienestar                = "BienestarViewController"
    static let Prospects                = "ProspectsTrayViewController"
    static let Different                = "Different2ViewController"
    static let Notification             = "NotificationViewController"
    static let Login                    = "LoginViewController"
    static let LoginVC                  = "LoginVC"
    static let SecretWord               = "VerifySecretWordViewController"
    static let EditProfile              = "EditedProfileViewController"
    static let Home                     = "HomeViewController"
    static let Video                    = "VideosViewControllerNew"
    static let SellDetail               = "SellApprovedViewController"
    static let Map                      = "MapValidateCoverageViewController"
    static let MicroResi                = "PackageViewController"
    static let Option                   = "OptionComboViewController"
    static let ServiceProduct           = "ServicePromoViewController"
    static let HumanResources           = "HumanResourcesViewController"
    static let Reports                  = "ReportsViewController"
    static let Crearlead                = "CreateLeadView"
    static let DigitalTray              = "BandejaVentaView"
    static let Indicadores              = "EstatusLeadView"
    static let CanalEmpleado            = "CanalEmpleadoView"
    static let Start                    = "StartSaleViewController"
    static let ExitSale                 = "EndSaleViewController"
    static let MySales                  = "MySalesRedesign"
    static let FlyersView               = "FlyersView"
    static let NewClientPopUp           = "NewClientPopUpViewController"
    static let ReportsNew               = "ReportCoachView"
    static let AfiliacionComercios      = "AfiliacionComerciosView"

    struct Provisional {
        static let postPreguntas        = "StepContainer"
    }
    struct newSell {
        static let popUP                = "TypePropertyViewController"
    }

    struct Street {
        static let homePromotor         = "EventsCalendarView"
        static let detailEvent          = "DetailEventHostesView"
        static let reasonMissing        = "MotivoFaltaView"
        static let newEventSuper        = "createEventSupervisorView"
        static let homeSupervisor       = "EventCalendarSupervisorView"
        static let createEventSuper     = "createEventSupervisorView"
        static let statusLeadSuper      = "ListPromotorView"
        static let createLead           = "CreateLeadView"
        static let statusleadPromotor   = "EstatusLeadView"
        static let newEventCoord        = "NewEventView"
        static let planningCoord        = "PlanificacionView"
        static let consultEventCoord    = "ConsultEventView"
    }

    struct Step {
        static let Validate             = "ValidateCoverageViewController"
        static let Captured             = "CaptureDataViewController"
        static let Package              = "Package2ViewController"
        static let Document             = "LoadDocumentViewController"
        static let Payment              = "PaymentViewController"
        static let Reference            = "ReferenceViewController"
    }

    struct Comissions {
        static let Welcome              = "CommissionsViewController"
        static let NewConsult           = "NewConsultComissionsViewController"
        static let ConsultDetails       = "ComissionsDetailsViewController"
    }

    struct Ticket {
        static let SendTicketInfo       = "SendTicketInfoViewController"
        static let TicketDetail         = "TicketDetailsViewController"
    }

    struct ComissionsEmbajador {
        static let WelcomeMinistro      = "HomeComissionsView"
        static let WelcomeEmbajador     = "HomeComissionsEmbajadorView"
    }

    struct QR {
        static let GenerateQR           = "GenerateQR"
    }

    struct ExistingAccount {
        static let ExistingAccount      = "ExistingAccountsViewController"
    }

    struct Diferenciadores {
        static let Welcome              = "Differentiators"
        static let Residential          = "ResidentialViewController"
        static let Business             = "BusinessmenViewController"
    }

    struct IdealPlan {
        static let Ideal                = "IdealPlanViewController"
        static let Experience           = "ExperienceTPViewController"
        static let RecommendedPlan      = "RecommendedPlan"
    }
    struct Flyers {
        static let PopUpMailView        = "PopUpMailView"
    }
}
