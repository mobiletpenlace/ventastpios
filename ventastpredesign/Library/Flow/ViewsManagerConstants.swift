//
//  ViewsManagerConstants.swift
//  SalesCloud
//
//  Created by Axl Estevez on 24/05/24.
//  Copyright © 2024 TotalPlay. All rights reserved.
//

import Foundation

public enum ViewsManagerConstants {

    static let mediaSourceViewController    = "MediaSourceViewController"

}
