/* 
  Localizable.strings
  

  Created by Armando Isais Olguin Cabrera on 07/12/22.
  
*/
/*HomeScreen*/

enum LocalizedText: String {
  case helloWorld = "Hola Mundo!"
}

func Localized(key: LocalizedText, comment: String = "") -> String {
    return NSLocalizedString(key.rawValue, value: key.rawValue, comment: comment)
}
